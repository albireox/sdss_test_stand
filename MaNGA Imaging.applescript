tell application "Terminal"
	
	close every window
	
	count windows
	
	activate
	
	set tab1 to do script "date"
	set wid1 to id of front window
	set bounds of window id wid1 to {670, 0, 770, 300}
	set custom title of window id wid1 to "Input Camera"
	set number of rows of window id wid1 to 16
	set number of columns of window id wid1 to 132
	set background color of window id wid1 to "orange"
	do script "ds9 -title input -port 0 -colorbar no -geometry 594x579 -view panner no -view magnifier no -view info no -zoom 0.5 &" in window id wid1
	delay 4
	do script "gige_loop -uid 5013587 -ds9name input" in window id wid1
	delay 1
	
	set tab2 to do script "date"
	set wid2 to id of front window
	set bounds of window id wid2 to {670, 300, 770, 600}
	set custom title of window id wid2 to "Output Camera"
	set number of rows of window id wid2 to 16
	set number of columns of window id wid2 to 132
	set background color of window id wid2 to "yellow"
	do script "ds9 -title output -port 0 -colorbar no -geometry 594x579 -view panner no -view magnifier no -view info no -zoom to fit&" in window id wid2
	delay 4
	do script "gige_loop -uid 5027623 -ds9name output" in window id wid2
	delay 1
	
	set tab3 to do script "date"
	set wid3 to id of front window
	set bounds of window id wid3 to {670, 600, 770, 900}
	set custom title of window id wid3 to "python"
	set number of rows of window id wid3 to 24
	set number of columns of window id wid3 to 132
	set background color of window id wid3 to "green"
	do script "caffeinate python ~/git/MaNGA/manga/ADE_Trunk/python/ADErun.py" in window id wid3
	delay 4
	
	count windows
	
end tell

tell application "System Events"
	tell application "Python" to activate
	delay 4
	tell application "Python"
		get bounds of front window
		set bounds of window 1 to {1190, 20, 1600, 665}
		get bounds of front window
	end tell
end tell