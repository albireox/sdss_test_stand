#! /bin/bash

#echo "Checking out new code..."
source ~/.bash_profile
(cd ~/git/MaNGA;
git pull bucket imaging)

#echo "Compiling ftdi..."
(cd ~/git/MaNGA/manga/ADE_trunk/c/ftdi/lib;
make;
cd ../;
make install;
cd lib;
make clean;
cd ../;
make clean)

#echo "Compiling apt..."
cd ~/git/MaNGA/manga/ADE_trunk/c/apt/lib
make
cd ../
make install
cd lib
make clean
cd ../
make clean

#echo "Compiling gige..."
cd ~/git/MaNGA/manga/ADE_trunk/c/gige/lib
make
cd ../
make install
cd lib
make clean
cd ../
make clean

#echo "Compiling manga..."
cd ~/git/MaNGA/manga/ADE_trunk/c/manga/lib
make
cd ../
make install
cd lib
make clean
cd ../
make clean

#echo "Code updated!"
