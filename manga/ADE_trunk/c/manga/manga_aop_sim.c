/* file: $RCSfile: manga_aop_sim.c,v $
** rcsid: $Id: manga_aop_sim.c,v 1.2 2013/06/13 15:45:31 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_aop_sim.c,v 1.2 2013/06/13 15:45:31 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_aop_sim.c,v $ - simulates the aop thing.
** We generate a simulation to show things are working.
**
** the input lines are of the form:

tag1 tag1 index x1 y1 x2 y2

** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "manga.h"

/**
 * Generate a simulation of measurements
 * We simulate an ifu if pp=0, else a plug plate.
 */

static int
do_aop_sim(FILE *fp, char *tag, AOP aop, int rank, int pp)
{
	FIBER_AB ab_list[MAX_FIBERS];
	FIBER_XY xy_list[MAX_FIBERS];
	FIBER_XY im_list[MAX_FIBERS];
	int i;

	(void)fprintf(stderr, "%s: model:    pp: %8d\n", tag, pp);
	(void)fprintf(stderr, "%s: model:  rank: %8d\n", tag, rank);
	(void)fprintf(stderr, "%s: model:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(stderr, "%s: model: theta: %12.6f deg\n", tag, aop.theta*(180/M_PI));
	(void)fprintf(stderr, "%s: model:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(stderr, "%s: model:    sy: %12.6f\n", tag, aop.sy);

	// build the coordinate lists
	int nf = manga_fiber_count(rank);
	if (pp) {
		nf = MAX_PP;
	}

	(void)fprintf(stderr, "%s: ab_list: %3s %8s %8s\n", tag, "i", "a", "b");
	(void)fprintf(stderr, "%s: xy_list: %3s %8s %8s\n", tag, "i", "x", "y");
	(void)fprintf(stderr, "%s: im_list: %3s %8s %8s\n", tag, "i", "x", "y");
	(void)fprintf(stdout, "%s: aop_data: %3s %8s %8s %8s %8s\n", tag, "i", "x1", "y1", "x2", "y2");
	for (i = 0; i < nf; i++) {
		if (pp) {
			xy_list[i] = manga_fiber_pp(i);
		} else {
			ab_list[i] = manga_fiber_ab(i);
			xy_list[i] = manga_fiber_ab2xy(ab_list[i]);
		}
		// rotate it
		im_list[i].x = cos(aop.theta) * xy_list[i].x - sin(aop.theta) * xy_list[i].y;
		im_list[i].y = sin(aop.theta) * xy_list[i].x + cos(aop.theta) * xy_list[i].y;
		// scale it
		im_list[i].x *= aop.c;
		im_list[i].y *= aop.c;
		// translate it
		im_list[i].x += aop.sx;
		im_list[i].y += aop.sy;
		// add in some noise
		im_list[i].x += (drand48() - 0.5) * 1;	// 1-ish pixels
		im_list[i].y += (drand48() - 0.5) * 1;	// 1-ish pixels
		if (!pp) {
			(void)fprintf(stderr, "%s: ab_list: %3d %8.3f %8.3f\n",
					tag, i, ab_list[i].a, ab_list[i].b);
		}
		(void)fprintf(stderr, "%s: xy_list: %3d %8.3f %8.3f\n",
				tag, i, xy_list[i].x, xy_list[i].y);
		(void)fprintf(stderr, "%s: im_list: %3d %8.3f %8.3f\n",
				tag, i, im_list[i].x, im_list[i].y);
		(void)fprintf(stderr, "%s: aop_data: %3d %8.3f %8.3f %8.3f %8.3f\n",
				tag, i, xy_list[i].x, xy_list[i].y, im_list[i].x, im_list[i].y);
	}

	// show the simulation
	for (i = 0; i < nf; i++) {
		(void)fprintf(fp, "%s: %s %3d %.3f %.3f %.3f %.3f\n", tag, (pp?"pp":"ifu"),
				i,
				xy_list[i].x, xy_list[i].y,
				im_list[i].x, im_list[i].y);
	}

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int pp = 0;		// indicates a plug plate simulation
	int rank = 6;
	AOP aop;

	// start with a typical transformation
	aop.c = 26;
	aop.theta = 20 * (M_PI/180);
	aop.sx = 836;
	aop.sy = 612;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", tag);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-pp (simulate plug-plate)\n");
			(void)fprintf(stderr, "\t-ifu (simulate ifu)\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-rank n (%d)\n", rank);
			(void)fprintf(stderr, "\t-c <scale, pix> (%12.6f)\n", aop.c);
			(void)fprintf(stderr, "\t-theta <rotation, deg> (%12.6f)\n", aop.theta*(180/M_PI));
			(void)fprintf(stderr, "\t-sx <translation, pix> (%12.6f)\n", aop.sx);
			(void)fprintf(stderr, "\t-sy <translation, pix> (%12.6f)\n", aop.sy);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "ifu", arglen) == 0) {
			pp = 0;
			// reset the model to reflect the typical ifu values
			aop.c = 26;
			aop.theta = 20 * (M_PI/180);
			aop.sx = 836;
			aop.sy = 612;

		} else if (strncmp(argptr, "pp", arglen) == 0) {
			pp++;
			// reset the model to reflect the typical plug plate values
			aop.c = 1.01;
			aop.theta = 1 * (M_PI/180);
			aop.sx = 16;
			aop.sy = 0;

		} else if (strncmp(argptr, "rank", arglen) == 0) {
			rank = atoi(argv[++argnum]);
			rank = RANGE(rank,0,MAX_RANK);

		} else if (strncmp(argptr, "c", arglen) == 0) {
			aop.c = atof(argv[++argnum]);

		} else if (strncmp(argptr, "theta", arglen) == 0) {
			aop.theta = (M_PI/180)*atof(argv[++argnum]);

		} else if (strncmp(argptr, "sx", arglen) == 0) {
			aop.sx = atof(argv[++argnum]);

		} else if (strncmp(argptr, "sy", arglen) == 0) {
			aop.sy = atof(argv[++argnum]);

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(-1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s:  rank: %8d\n", tag, rank);
		(void)fprintf(stderr, "%s:    pp: %8d\n", tag, pp);
		(void)fprintf(stderr, "%s:     c: %12.6f\n", tag, aop.c);
		(void)fprintf(stderr, "%s: theta: %12.6f deg\n", tag, aop.theta*(180/M_PI));
		(void)fprintf(stderr, "%s:    sx: %12.6f\n", tag, aop.sx);
		(void)fprintf(stderr, "%s:    sy: %12.6f\n", tag, aop.sy);
	}

	do_aop_sim(stdout, tag, aop, rank, pp);

	return(0);
}
