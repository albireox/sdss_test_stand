/* file: $RCSfile: manga_sim.c,v $
** rcsid: $Id: manga_sim.c,v 1.5 2013/03/13 03:00:07 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_sim.c,v 1.5 2013/03/13 03:00:07 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_sim.c,v $ - simulates a manga fiber bundle.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "manga.h"

// storage for the largest image we can recognize
static int image[MAX_IMAGE_SIZE];

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int nx = NX; int ny = NY, depth = 0;
	int seed = 0;
	int rcode;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", tag);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\t-nx int (%d)\n", nx);
			(void)fprintf(stderr, "\t-ny int (%d)\n", ny);
			(void)fprintf(stderr, "\t-depth [0,1,2] (%d)\n", depth);
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-seed int (%d)\n", seed);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "nx", arglen) == 0) {
			nx = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "ny", arglen) == 0) {
			ny = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "depth", arglen) == 0) {
			depth = atoi(argv[++argnum]);
			depth = RANGE(depth,0,2);

		} else if (strncmp(argptr, "seed", arglen) == 0) {
			seed = atoi(argv[++argnum]);

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(-1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: nx %d ny %d depth %d seed %d\n", tag, nx, ny, depth, seed);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: zero image %d bytes\n", tag, MAX_IMAGE_SIZE);
	}
	(void)memset(image, 0, MAX_IMAGE_SIZE);

	if (verbose) {
		(void)fprintf(stderr, "%s: simulate image nx %d ny %d seed %d\n", tag, nx, ny, seed);
	}
	rcode = manga_im_sim(image, nx, ny, seed);
	if (rcode != 0) {
		return(-1);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: write image nx %d ny %d depth %d\n", tag, nx, ny, depth);
	}
	rcode = manga_im_write_raw(stdout, image, nx, ny, depth, rcsid);
	if (rcode != 0) {
		return(-1);
	}

	return(0);
}
