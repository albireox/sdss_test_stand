/* file: $RCSfile: manga.h,v $
** rcsid: $Id: manga.h,v 1.7 2013/06/13 15:43:45 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
** *******************************************************************
** $RCSfile: manga.h,v $ - manga fiber-finding software
** *******************************************************************
*/

#ifndef MANGA_H

#include <stdio.h>

#ifdef MIN
#undef MIN
#endif
#define MIN(a,b)	((a)<(b)?(a):(b))
#ifdef MAX
#undef MAX
#endif
#define MAX(a,b)	((a)>(b)?(a):(b))
#define RANGE(x,a,b)	(MIN(MAX((x),(a)),(b)))

// the default image size for uw-manga
#define NX	(1624)
#define NY	(1234)

// pixel size in microns
#define PX	(4.4)
#define PY	(4.4)

/**
 * Here we describe the file sizes we can manage.
 * Given nx, ny, depth (bytes per pixel),
 * we can compute the size of a raw image,
 * or a properly-written FITS file.
 * We assume the FITS file to have:
 * - a 2880-byte header (exactly 36 cards)
 * - a data section
 * - an optional rounding out of the data section to a multiple of 2880 bytes.
 * If this rounding is absent,
 * which plagues many of the fits files I have created,
 * we call it a "frag",
 * but recognize it anyway.
 */

#define IM_SIZE_RAW(nx, ny, bpp)	(nx*ny*bpp)
#define IM_SIZE_FRAG(nx, ny, bpp)	(2880 + IM_SIZE_RAW(nx, ny, bpp))
#define IM_SIZE_FITS(nx, ny, bpp)	(2880 + ((IM_SIZE_RAW(nx, ny, bpp)+2880-1)/2880)*2880)

/**
 * The largest image we will process: a 2048x1536 32-bit image in FITS format.
 * This is 36*80=2880 header bytes + 2048*1536*4 data bytes,
 * rounded up to the next multiple of 2880.
 */

#define MAX_IMAGE_SIZE	(IM_SIZE_FITS(2048,1536,4))

// this macro gives the largest unsigned value for the given depth (e.g. 255 for depth=0)
#define MAX_IMAGE_VALUE(depth)	((1<<8*(1<<depth))-1)

// the default fiber radius in image pixels
#define R0	(21)

/**
 * Fiber AB coordinates out to rank=7 (169 fibers)
 */

#define MAX_RANK	(7)
#define MAX_FIBERS	(169)
#define MAX_PP		(35)

typedef struct s_fiber_xy {
	int f;	// fiber number
	double x;
	double y;
} FIBER_XY;

typedef struct s_fiber_ab {
	int f;	// fiber number
	double a;
	double b;
} FIBER_AB;

typedef struct s_fiber {
	int f;	// fiber number
	double xc;
	double yc;
	int peak;
	int flux;
	int f1;
	int f2;
	double z;
} FIBER;

/**
 * The solution for the Absolute Orientation Problem,
 * wherein we map a perfect (r=1) fiber bundle
 * onto an observed set of fibers in the Manta camera.
 */

typedef struct s_aop {
	double c;		// scale factor
	double theta;	// rotation
	double sx;		// x-offset
	double sy;		// y-offset
} AOP;

/* EXTERN_START */
extern AOP manga_im_aop(FIBER_XY x_list[], FIBER_XY y_list[], int nf);
extern FIBER_AB manga_fiber_ab(int f);
extern FIBER_AB manga_fiber_lookup(double x, double y);
extern FIBER_AB manga_fiber_xy2ab(FIBER_XY fiber_xy);
extern FIBER_XY manga_fiber_ab2xy(FIBER_AB fiber_ab);
extern FIBER_XY manga_fiber_pp(int h);
extern double manga_fiber_distance(double x, double y, FIBER_XY fiber_xy);
extern int manga_fiber_count(int rank);
extern int manga_fiber_spectrum(double *shist, int *nhist, int nh, int rank);
extern int manga_im_blobs(int *xform, int *image, int nx, int ny);
extern int manga_im_floor(int *xform, int *image, int nx, int ny);
extern int manga_im_hough(int *xform, int *image, int nx, int ny, int r0);
extern int manga_im_hough_line(int *xform, int *image, int nx, int ny);
extern int manga_im_ifu(AOP *paop, FIBER_XY xy_list[], FIBER_XY im_list[], int nfibers);
extern int manga_im_read_new(int *image, int *pnx, int *pny, int *pdepth, FILE *fp);
extern int manga_im_show(FILE *fp, int *image, int nx, int ny);
extern int manga_im_sim(int *image, int nx, int ny, int seed);
extern int manga_im_sim_line(int *image, int nx, int ny, int seed);
extern int manga_im_sobel(int *xform, int *image, int nx, int ny);
extern int manga_im_sort(int *xform, int *image, int nx, int ny);
extern int manga_im_spikes(FIBER fiberbuf[], int fmax, int *image, int nx, int ny);
extern int manga_im_spot(double *xc, double *yc, int *image, int nx, int ny, int tilesize);
extern int manga_im_stats(int *min, int *max, double *mean, double *sigma, int *image, int nx, int ny);
extern int manga_im_write_fits(FILE *fp, int *image, int nx, int ny, int depth, char *history);
extern int manga_im_write_raw(FILE *fp, int *image, int nx, int ny, int depth, char *history);
extern int svdcmp(double **a, int nRows, int nCols, double *w, double **v);
extern void svdbksb(double **u, double *w, double **v, int nRows, int nCols, double *b, double *x);
/* EXTERN_STOP */

#define MANGA_H
#endif
