#! /bin/sh
# file: $RCSfile: manga-clean.sh,v $
# rcsid: $Id: manga-clean.sh,v 1.5 2013/06/13 15:44:47 jwp Exp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# clean up the re-creatable files made by manga-motor-cal-2
#
# the options are:
#	-p prefix
#	-r
#
# ********************************************************************

echo $(basename $0): process $$ pwd $(pwd) argc $# argv $*

# we start by assuming the usual "manta" filename prefix
prefix="manta"
recurse="";	# no directory descent

while getopts p:r opt
do
	case "$opt" in
		p) prefix=$OPTARG;;
		r) recurse="-r";;
		\?) ;;
		esac
done
shift $((OPTIND-1))

echo $(basename $0): process $$ pwd $(pwd) prefix $prefix recurse $recurse

for name in $*
do
	if [ -d $name ]
	then
	(
		cd $name;
		if [ $recurse ]
		then
			manga-clean -p $prefix -r *;
		fi
		manga-clean -p $prefix
	)
	fi
done

echo $(basename $0): process $$ pwd $(pwd) prefix $prefix done with recursion

for suffix in fits
do
	for tag in copy sobel hough blobs
	do
		echo $(basename $0): check .$tag.$suffix
		rm -vf $prefix.*.copy.$suffix
		rm -vf $prefix.*.sobel.$suffix
		rm -vf $prefix.*.hough.$suffix
		rm -vf $prefix.*.blobs.$suffix
	done
done
for tag in spot sobel hough blobs spikes ifu aop
do
	echo $(basename $0): check .$tag.out
	rm -vf $prefix.*.$tag.out
	echo $(basename $0): check .$tag.err
	rm -vf $prefix.*.$tag.err
done
rm -vf $prefix.*.spikes.out.png
rm -vf $prefix.*.spikes.ifu.out.png
rm -vf manga_pp_motor_cal.*.log.txt
rm -vf manga_pp_motor_cal.*.log.aop.out
rm -vf manga_pp_motor_cal.*.log.aop.err
rm -vf manga_ifu_motor_cal.*.log.txt
rm -vf manga_ifu_motor_cal.*.log.aop.out
rm -vf manga_ifu_motor_cal.*.log.aop.err

# remove fits files that have a .raw parent
shopt -s nullglob
for f in *.fits
do
	g=$(basename $f .fits)
	if [ -f $g.raw ]
	then
		echo $(basename $0): $(pwd)/$f can go
		rm -f $f
	else
		echo $(basename $0): $(pwd)/$f must stay
	fi
done
