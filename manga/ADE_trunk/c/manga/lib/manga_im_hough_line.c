/* file: $RCSfile: manga_im_hough_line.c,v $
** rcsid: $Id: manga_im_hough_line.c,v 1.4 2013/04/16 19:04:44 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_hough_line.c,v 1.4 2013/04/16 19:04:44 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_hough_line.c,v $ - hough linear accumulator
** See http://en.wikipedia.org/wiki/Hough_transform
** *******************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_hough_line(int *xform, int *image, int nx, int ny)
{
	char *tag = "manga_im_hough_line";
	int a, b;		// indexes into the xform
	int x, y;		// indexes into the image

	(void)fprintf(stderr, "%s: transform %dx%d\n", tag, nx, ny);

	(void)memset(xform, 0, nx*ny*sizeof(int));

	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			int v = image[y*nx+x];
			if (v > 0) {
				for (a = 0; a < nx; a++) {
					double theta = (double)a/nx * (2*M_PI);
					b = x*cos(theta) + y*sin(theta);
					if (b < 0) {
						continue;
					} else if (b >= ny) {
						continue;
					}
					xform[b*nx+a] += v;
				}
			}
		}
	}

	return(0);
}
