/* file: $RCSfile: manga_im_aop.c,v $
** rcsid: $Id: manga_im_aop.c,v 1.6 2013/06/13 15:39:37 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_aop.c,v 1.6 2013/06/13 15:39:37 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_aop.c,v $ - solve the Absolute Orientation Problem
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "manga.h"

#undef DEBUG

/**
 * This solves the Absolute Orientation Problem
 * presented by
 * Shinji Umeyama
 * in
 * IEEE Transactions on Pattern Analysis and Machine Intelligence, Vol 13, No. 4, April 1991.
 *
 * The transformation is given by y = c * R(x) + t
 * where R is a rotation, c is a scale factor, and t is a translation.
 */

AOP
manga_im_aop(FIBER_XY x_list[], FIBER_XY y_list[], int nf)
{
	char *tag = "manga_im_aop";
	int i, j, k;
	AOP aop;	// the final solution

	(void)fprintf(stderr, "%s: do %d pairs\n", tag, nf);

	// compute the elements of the fit

	// compute the mean vectors

	double mu_x1 = 0;
	double mu_x2 = 0;
	for (i = 0; i < nf; i++) {
		mu_x1 += x_list[i].x;
		mu_x2 += x_list[i].y;
	}
	mu_x1 /= (double)nf;
	mu_x2 /= (double)nf;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: mu_x1 %8.3f mu_x2 %8.3f\n", tag, mu_x1, mu_x2);
#endif

	double mu_y1 = 0;
	double mu_y2 = 0;
	for (i = 0; i < nf; i++) {
		mu_y1 += y_list[i].x;
		mu_y2 += y_list[i].y;
	}
	mu_y1 /= (double)nf;
	mu_y2 /= (double)nf;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: mu_y1 %8.3f mu_y2 %8.3f\n", tag, mu_y1, mu_y2);
#endif

	// compute the variances about the means

	double sigma_x2 = 0;
	for (i = 0; i < nf; i++) {
		sigma_x2 += (x_list[i].x - mu_x1)*(x_list[i].x - mu_x1) + (x_list[i].y - mu_x2)*(x_list[i].y - mu_x2);
	}
	sigma_x2 /= (double)nf;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: sigma_x %12.3f\n", tag, sigma_x2);
#endif

	double sigma_y2 = 0;
	for (i = 0; i < nf; i++) {
		sigma_y2 += (y_list[i].x - mu_y1)*(y_list[i].x - mu_y1) + (y_list[i].y - mu_y2)*(y_list[i].y - mu_y2);
	}
	sigma_y2 /= (double)nf;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: sigma_y %12.3f\n", tag, sigma_y2);
#endif

	// compute the covariance matrix

	double Sigma[2][2] = { { 0, 0}, {0, 0} };
	for (i = 0; i < nf; i++) {
		Sigma[0][0] += (y_list[i].x - mu_y1) * (x_list[i].x - mu_x1);
		Sigma[0][1] += (y_list[i].x - mu_y1) * (x_list[i].y - mu_x2);
		Sigma[1][0] += (y_list[i].y - mu_y2) * (x_list[i].x - mu_x1);
		Sigma[1][1] += (y_list[i].y - mu_y2) * (x_list[i].y - mu_x2);
	}
	Sigma[0][0] /= (double)nf;
	Sigma[0][1] /= (double)nf;
	Sigma[1][0] /= (double)nf;
	Sigma[1][1] /= (double)nf;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: Sigma %12.3f %12.3f\n", tag, Sigma[0][0], Sigma[0][1]);
	(void)fprintf(stderr, "%s: Sigma %12.3f %12.3f\n", tag, Sigma[1][0], Sigma[1][1]);
#endif

	// get ready for the Singular Value Decomposition
	//double U[2][2], W[2], V[2][2];
	double *U[2], *W, *V[2];
	U[0] = malloc(2*sizeof(double));
	U[1] = malloc(2*sizeof(double));
	W = malloc(2*sizeof(double));
	V[0] = malloc(2*sizeof(double));
	V[1] = malloc(2*sizeof(double));
	U[0][0] = Sigma[0][0];
	U[0][1] = Sigma[0][1];
	U[1][0] = Sigma[1][0];
	U[1][1] = Sigma[1][1];

#ifdef DEBUG
	(void)fprintf(stderr, "%s: call svdcmp\n", tag);
#endif
	(void)svdcmp(U, 2, 2, W, V);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: done\n", tag);
#endif

#ifdef DEBUG
	(void)fprintf(stderr, "%s: U %12.3f %12.3f\n", tag, U[0][0], U[0][1]);
	(void)fprintf(stderr, "%s: U %12.3f %12.3f\n", tag, U[1][0], U[1][1]);
	(void)fprintf(stderr, "%s: W %12.3f %12.3f\n", tag, W[0], W[1]);
	(void)fprintf(stderr, "%s: V %12.3f %12.3f\n", tag, V[0][0], V[0][1]);
	(void)fprintf(stderr, "%s: V %12.3f %12.3f\n", tag, V[1][0], V[1][1]);
#endif

	// check the results a la Numerical Recipes, 1986, ISBN 0 521 30811 9, as discussed on p. 54:
	// Eq. 2.9.1: A = U * W * VT
	// Eq. 2.9.4: UT * U = VT * V = I

#ifdef DEBUG
	// Sigma = U * W * VT
	(void)fprintf(stderr, "%s: check: Sigma = U * W * V\n", tag);
	{
		double A[2][2];
		A[0][0] = 0;
		A[0][1] = 0;
		A[1][0] = 0;
		A[1][1] = 0;
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 2; j++) {
				for (k = 0; k < 2; k++) {
					A[i][j] += U[i][k]*W[i]*V[j][k];
				}
			}
		}
		(void)fprintf(stderr, "%s: Sigma %12.3f %12.3f\n", tag, A[0][0], A[0][1]);
		(void)fprintf(stderr, "%s: Sigma %12.3f %12.3f\n", tag, A[1][0], A[1][1]);
	}
#endif

#ifdef DEBUG
	// I = UT * U
	(void)fprintf(stderr, "%s: check: I = UT * U\n", tag);
	{
		double A[2][2];
		A[0][0] = 0;
		A[0][1] = 0;
		A[1][0] = 0;
		A[1][1] = 0;
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 2; j++) {
				for (k = 0; k < 2; k++) {
					A[i][j] += U[k][i]*U[k][j];
				}
			}
		}
		(void)fprintf(stderr, "%s: I %12.3f %12.3f\n", tag, A[0][0], A[0][1]);
		(void)fprintf(stderr, "%s: I %12.3f %12.3f\n", tag, A[1][0], A[1][1]);
	}
#endif

#ifdef DEBUG
	// I = VT * V
	(void)fprintf(stderr, "%s: check: I = VT * V\n", tag);
	{
		double A[2][2];
		A[0][0] = 0;
		A[0][1] = 0;
		A[1][0] = 0;
		A[1][1] = 0;
		for (i = 0; i < 2; i++) {
			for (j = 0; j < 2; j++) {
				for (k = 0; k < 2; k++) {
					A[i][j] += V[k][i]*V[k][j];
				}
			}
		}
		(void)fprintf(stderr, "%s: I %12.3f %12.3f\n", tag, A[0][0], A[0][1]);
		(void)fprintf(stderr, "%s: I %12.3f %12.3f\n", tag, A[1][0], A[1][1]);
	}
#endif

	// extract the model parameters

	// R = U * VT
#ifdef DEBUG
	(void)fprintf(stderr, "%s: R = U * VT\n", tag);
#endif

	double R[2][2];
	R[0][0] = 0;
	R[0][1] = 0;
	R[1][0] = 0;
	R[1][1] = 0;
	for (i = 0; i < 2; i++) {
		for (j = 0; j < 2; j++) {
			for (k = 0; k < 2; k++) {
				R[i][j] += U[i][k]*V[j][k];
			}
		}
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: R %12.3f %12.3f\n", tag, R[0][0], R[0][1]);
	(void)fprintf(stderr, "%s: R %12.3f %12.3f\n", tag, R[1][0], R[1][1]);
#endif

	// the scale factor
	aop.c = (1 / sigma_x2) * (W[0] + W[1]);

	// the angle of rotation
	aop.theta = asin(R[1][0]);
	//theta *= -1;

	// the translation
	aop.sx = mu_y1 - aop.c * (cos(aop.theta)*mu_x1 - sin(aop.theta)*mu_x2);
	aop.sy = mu_y2 - aop.c * (sin(aop.theta)*mu_x1 + cos(aop.theta)*mu_x2);

	(void)fprintf(stderr, "%s: aop: final:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(stderr, "%s: aop: final: theta: %12.6f deg\n", tag, aop.theta * (180/M_PI));
	(void)fprintf(stderr, "%s: aop: final:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(stderr, "%s: aop: final:    sy: %12.6f\n", tag, aop.sy);

#ifdef NOPE
	// show the residuals
	(void)fprintf(stderr, "%s: residuals: %3s %5s %8s %8s %5s %8s %8s %8s %8s %8s\n",
				tag, "ix", "xy.f", "xy.x", "xy.y", "im.f", "im.x", "im.y", "dx", "dy", "dr");
	int ix;
	for (ix = 0; ix < nf; ix++) {
		// un-translate it
		y_list[ix].x -= aop.sx;
		y_list[ix].y -= aop.sy;
		// un-scale it
		y_list[ix].x /= aop.c;
		y_list[ix].y /= aop.c;
		// un-rotate it
		double x = cos(-aop.theta) * y_list[ix].x - sin(-aop.theta) * y_list[ix].y;
		double y = sin(-aop.theta) * y_list[ix].x + cos(-aop.theta) * y_list[ix].y;
		y_list[ix].x = x;
		y_list[ix].y = y;
		double dx = y_list[ix].x - x_list[ix].x;
		double dy = y_list[ix].y - x_list[ix].y;
		double dr = sqrt(dx*dx + dy*dy);
		(void)fprintf(stderr, "%s: residuals: %3d %5d %8.3f %8.3f %5d %8.3f %8.3f %8.3f %8.3f %8.3f\n",
				tag, ix,
				x_list[ix].f, x_list[ix].x, x_list[ix].y,
				y_list[ix].f, y_list[ix].x, y_list[ix].y,
				dx, dy, dr);
	}
#endif

	return(aop);
}
