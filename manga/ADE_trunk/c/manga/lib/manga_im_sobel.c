/* file: $RCSfile: manga_im_sobel.c,v $
** rcsid: $Id: manga_im_sobel.c,v 1.5 2013/04/16 19:06:43 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_sobel.c,v 1.5 2013/04/16 19:06:43 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_sobel.c,v $ - sobel 2D gradient transform
** See http://en.wikipedia.org/wiki/Sobel_operator
** *******************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

// the X convolution kernel
static int GX[3][3] = {
	{-1, 0, 1},
	{-2, 0, 2},
	{-1, 0, 1}
};

// the Y convolution kernel
static int GY[3][3] = {
	{-1, -2, -1},
	{ 0,  0,  0},
	{ 1,  2,  1}
};

// storage for the x and y gradient convolutions
static int *gx;
static int *gy;

int
manga_im_sobel(int *xform, int *image, int nx, int ny)
{
	char *tag = "manga_im_sobel";
	int i, j;	// indexes into the kernel
	int x, y;	// indexes into the image

	(void)fprintf(stderr, "%s: transform %dx%d\n", tag, nx, ny);

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gx memory\n", tag);
#endif
	gx = (int *)malloc(nx*ny*sizeof(int));
	if (gx == NULL) {
		perror(tag);
		return(-1);
	}
	(void)memset(gx, 0, nx*ny*sizeof(int));

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gy memory\n", tag);
#endif
	gy = (int *)malloc(nx*ny*sizeof(int));
	if (gy == NULL) {
		perror(tag);
		return(-1);
	}
	(void)memset(gy, 0, nx*ny*sizeof(int));

	/**
	 * Do the X gradient
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gx xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GX[j][i]*image[(y+j-1)*nx+(x+i-1)];
				}
			}
			gx[y*nx+x] = v;
		}
	}

	/**
	 * Do the Y gradient
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gy xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GY[j][i]*image[(y+j-1)*nx+(x+i-1)];
				}
			}
			gy[y*nx+x] = v;
		}
	}

	/**
	 * Compute the amplitude of the two gradients
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: compute the amplitude\n", tag);
#endif
	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			int vx = gx[y*nx+x];
			int vy = gy[y*nx+x];
			double vv = vx*vx + vy*vy;
			int v = (int)(sqrt(vv) + 0.5);
			xform[y*nx+x] = v;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: free gx and gy memory\n", tag);
#endif
	free(gx);
	free(gy);

	return(0);
}
