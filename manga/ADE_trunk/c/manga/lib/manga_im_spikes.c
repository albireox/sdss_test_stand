
/**
 * *******************************************************************
 * Copyright Jeffrey W Percival
 * *******************************************************************
 * Department of Astronomy
 * University of Wisconsin-Madison
 * 5518 Sterling Hall
 * 475 N Charter St
 * Madison, WI 53706 USA
 * *******************************************************************
 * Do not use this software without permission.
 * Do not use this software without attribution.
 * Do not remove or alter any of the lines above.
 * *******************************************************************
 */

/**
 * *******************************************************************
 * Fills a list with detected fibers.
 * After doing the Sobel, Hough, and convolutional sharpening transformations,
 * we have two kinds of detections: actual fibers, and false positives
 * caused by the "echo circles" of false votes (see one of the .blobs images).
 * Sometimes the false positives have higher peaks and fluxes
 * than the solutions for the fainter fibers.
 *
 * The false positives arise from the intersection of echo circles.
 * Imagine two intersecting circles, like in a Venn Diagram.
 * At the two crossing points, the circles add to produce a local flux spike.
 * We want to reject these spikes,
 * and will do so by looking at their local structure.
 * The false positives have extended structure from the adjacent circles.
 * They do not fall off in all directions like a Gaussian.
 * We will measure the flux in an annulus around the spike;
 * too much flux in the annulus, and we reject the spike.
 * Examining a few images gives us a hueristic:
 * The ratio of the annulus flux to the total flux
 * appears to be much less than about 0.25 for Gaussian-like spikes,
 * and much greater than 0.25 for the false positives.
 * We will parameterize this value (ZCRIT) so we can change it as needed.
 *
 * Another issue is that even after sharpening,
 * the Hough peaks may be cratered, multi-peaked, and so on.
 * Peak finding sometimes finds two peaks in close proximity,
 * representing the same fiber.
 * We try to exclude these contaminators,
 * and as a final measure when adding a new spike,
 * we check the list to see if the new spike
 * is too near any spike in the list,
 * and leave only the brighter of any pair we find.
 * we use the "Keep Clear Distance" (KCD) for this.
 *
 * *******************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "manga.h"

#define DEBUG

#define NSIGMA	(20)
#define ZCRIT	(0.10)

// the Keep Clear Distance in pixels
#define KCD	(10)

// sort by fiber number (order of detection)
static int
comp_f(const FIBER *v1, const FIBER *v2)
{
	if (v1->f < v2->f) {
		return(-1);
	} else if (v1->f > v2->f) {
		return(1);
	}
	return(0);
}

// sort by x-coordinate
static int
comp_x(const FIBER *v1, const FIBER *v2)
{
	if (v1->xc < v2->xc) {
		return(-1);
	} else if (v1->xc > v2->xc) {
		return(1);
	}
	return(0);
}

// sort by y-coordinate
static int
comp_y(const FIBER *v1, const FIBER *v2)
{
	if (v1->yc < v2->yc) {
		return(-1);
	} else if (v1->yc > v2->yc) {
		return(1);
	}
	return(0);
}

/**
 * Finds spikes in an image.
 * @return the number of found spikes.
 */

int
manga_im_spikes(FIBER fiberbuf[], int fmax, int *image, int nx, int ny)
{
	char *tag = "manga_im_spikes";
	FIBER fiber;
	double mean, sigma;
	int ix;
	int min, max;
	int nfibers = 0;
	int x, y;	// loop indices

	(void)fprintf(stderr, "%s: find fibers %dx%d\n", tag, nx, ny);

#ifdef DEBUG
	(void)fprintf(stderr, "%s: compute the stats\n", tag);
#endif
	(void)manga_im_stats(&min, &max, &mean, &sigma, image, nx, ny);
	(void)fprintf(stderr, "%s: min %d max %d mean %f sigma %f\n", tag, min, max, mean, sigma);

#ifdef DEBUG
	(void)fprintf(stderr, "%s: set the floor of the image\n", tag);
#endif
	(void)manga_im_floor(image, image, nx, ny);
	(void)manga_im_stats(&min, &max, &mean, &sigma, image, nx, ny);
	(void)fprintf(stderr, "%s: min %d max %d mean %f sigma %f\n", tag, min, max, mean, sigma);

	/**
	 * Scan the image looking for targets
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: search the image\n", tag);
#endif
#define SCAN_IMAGE

	// we avoid the edges to allow for the annulus testing
	for (y = 32; y < ny-32; y++) {

		for (x = 32; x < nx-32; x++) {

			int v, peak;

			v = image[y*nx+x];

			// it must be significant
			if (v < mean + NSIGMA*sigma) {
				continue;
			}

			/* we must be brighter than our neighbors */
			if (image[(y-1)*nx + x-1] >= v) continue;
			if (image[(y-1)*nx + x  ] >= v) continue;
			if (image[(y-1)*nx + x+1] >= v) continue;

			if (image[(y  )*nx + x-1] >= v) continue;
			//if (image[(y  )*nx + x  ] >= v) continue;
			if (image[(y  )*nx + x+1] >= v) continue;

			if (image[(y+1)*nx + x-1] >= v) continue;
			if (image[(y+1)*nx + x  ] >= v) continue;
			if (image[(y+1)*nx + x+1] >= v) continue;

			// we have a local maximum
			peak = v;
#ifdef DEBUG
			(void)fprintf(stderr, "%s: trigger: x %4d y %4d peak %8d nfibers %3d\n",
					tag, x, y, peak, nfibers);
#endif
			/**
			 * Compute two fluxes: in a large box and a smaller one.
			 * Centrally peaked objects will have similar fluxes;
			 * extended objects will not,
			 * as they have structure away from the peak.
			 */

			int w1 = 11;
			int w2 = 9;

			int i, j;
			int f1 = 0;	// larger box
			for (j = -w1/2; j <= w1/2; j++) {
				for (i = -w1/2; i <= w1/2; i++) {
					f1 += image[(y+j)*nx + x+i] - mean;
				}
			}

			int f2 = 0;	// smaller box
			for (j = -w2/2; j <= w2/2; j++) {
				for (i = -w2/2; i <= w2/2; i++) {
					f2 += image[(y+j)*nx + x+i] - mean;
				}
			}

			// the significance of the peak
			double nsig = (peak - mean) / sigma;

			// z is a measure of how much flux is in the annulus
			double z = (f1 - f2)/ (double)f1;

			// if the list is full, we must be brighter than the faintest

			if (nfibers == fmax) {
				if (f2 <= fiberbuf[nfibers-1].flux) {
#ifdef DEBUG
					(void)fprintf(stderr, "%s: reject: too faint: x %4d y %4d p %8d f1 %8d f2 %8d z %8.3f nsig %8.3f\n",
						tag, x, y, peak, f1, f2, z, nsig);
#endif
					continue;
				}
			}

#ifdef DEBUG
			(void)fprintf(stderr, "%s: trial: %3d: x %4d y %4d p %8d f1 %8d f2 %8d z %8.3f nsig %8.3f\n",
				tag, nfibers, x, y, peak, f1, f2, z, nsig);
#endif

			if (z > ZCRIT) {
				// reject this as a false positive
#ifdef DEBUG
				(void)fprintf(stderr, "%s: reject: false positive: %3d: x %4d y %4d p %8d f1 %8d f2 %8d z %8.3f nsig %8.3f\n",
					tag, nfibers, x, y, peak, f1, f2, z, nsig);
#endif
				continue;
			}

#ifdef DEBUG
			(void)fprintf(stderr, "%s: finalist: %3d: x %4d y %4d p %8d f1 %8d f2 %8d z %8.3f nsig %8.3f\n",
							tag, nfibers, x, y, peak, f1, f2, z, nsig);
#endif

			fiber.f = nfibers;
			fiber.xc = x;
			fiber.yc = y;
			fiber.peak = peak;
			fiber.flux = f2;
			fiber.f1 = f1;
			fiber.f2 = f2;
			fiber.z = z;

			// keep a brightness-sorted list
			for (ix = 0; ix < nfibers; ix++) {
				// how far is the new one from this list entry?
				double x1 = fiberbuf[ix].xc;
				double y1 = fiberbuf[ix].yc;
				double x2 = fiber.xc;
				double y2 = fiber.yc;
				double d2 = (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1);
#ifdef DEBUG
				(void)fprintf(stderr, "%s: pair test: x %8.3f y %8.3f peak %8d flux %8d d %f\n",
							tag, fiberbuf[ix].xc, fiberbuf[ix].yc, fiberbuf[ix].peak, fiberbuf[ix].flux, sqrt(d2));
#endif
				if (d2 < KCD*KCD) {	// avoid the square root overhead
					// a close pair; keep the brighter one
					if (fiber.flux > fiberbuf[ix].flux) {
						// replace, not swap
#ifdef DEBUG
						(void)fprintf(stderr, "%s: pair loser: x %8.3f y %8.3f peak %8d flux %8d\n",
								tag, fiberbuf[ix].xc, fiberbuf[ix].yc, fiberbuf[ix].peak, fiberbuf[ix].flux);
#endif
						fiberbuf[ix] = fiber;
						/**
						 * now, we want to get out of this loop, so we "continue",
						 * but we don't want to append this fiber to the end,
						 * as if it were merely the least brightest in the list.
						 * So we mark it by setting its flux to zero,
						 * and test that condition before we append.
						 */
						fiber.flux = 0;
						continue;
					}
				} else if (fiber.flux > fiberbuf[ix].flux) {
					// swap, not replace
#ifdef DEBUG
					(void)fprintf(stderr, "%s: swap in at fiber %d\n", tag, ix);
#endif
					FIBER tmp = fiberbuf[ix];
					fiberbuf[ix] = fiber;
					fiber = tmp;
				}
			}
			if (fiber.flux == 0) {
				// must have been a loser in the pair test
			} else if (nfibers < fmax) {
				// there's room at the end...
#ifdef DEBUG
				(void)fprintf(stderr, "%s: append this fiber\n", tag);
#endif
				fiberbuf[nfibers] = fiber;
				nfibers++;
			} else {
				// not bright enough
#ifdef DEBUG
				(void)fprintf(stderr, "%s: dim loser: f %2d x %8.3f y %8.3f peak %8d flux %8d\n",
							tag, fiber.f, fiber.xc, fiber.yc, fiber.peak, fiber.flux);
#endif
			}
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: prelim: %3s %3s %8s %8s %8s %8s %8s\n",
				tag, "ix", "f", "xc", "yc", "peak", "flux", "z");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: prelim: %3d %3d %8.3f %8.3f %8d %8d %8.3f\n",
				tag, ix,
				fiberbuf[ix].f,
				fiberbuf[ix].xc,
				fiberbuf[ix].yc,
				fiberbuf[ix].peak,
				fiberbuf[ix].flux,
				fiberbuf[ix].z);
	}
#endif

#define FIND_CENTROIDS

	// now do centroids
	for (ix = 0; ix < nfibers; ix++) {
		int xp = fiberbuf[ix].xc + 0.5;	// recover the integer value from the stored double
		int yp = fiberbuf[ix].yc + 0.5;	// recover the integer value from the stored double
		int peak = fiberbuf[ix].peak;
		int flux = fiberbuf[ix].flux;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: refine: ix %3d xp %4d yp %4d peak %8d flux %8d\n",
				tag, ix, xp, yp, peak, flux);
#endif

		// compute the x-centroid in a 3x5 box
		double xflux = 0;
		flux = 0;
		for (y = yp-1; y <= yp+1; y++) {
			for (x = xp-2; x <= xp+2; x++) {
				int v = image[y*nx+x];
				xflux += x * (double)v;
				flux += v;
			}
		}
		double xc = (double)xflux / flux;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: refine: ix %3d xflux %8.3f flux %8d xc %8.3f\n",
				tag, ix, xflux, flux, xc);
#endif

		// compute the y-centroid in a 5x3 box
		double yflux = 0;
		flux = 0;
		for (y = yp-2; y <= yp+2; y++) {
			for (x = xp-1; x <= xp+1; x++) {
				int v = image[y*nx+x];
				yflux += y * (double)v;
				flux += v;
			}
		}
		double yc = (double)yflux / flux;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: refine: ix %3d yflux %8.3f flux %8d yc %8.3f\n",
				tag, ix, yflux, flux, yc);
#endif

		fiberbuf[ix].xc = xc;
		fiberbuf[ix].yc = yc;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: centroid: ix %3d xflux %8.3f yflux %8.3f xc %8.3f yc %8.3f\n",
			tag, ix, xflux, yflux, xc, yc);
#endif

	}

	// show the centroided positions
	(void)fprintf(stderr, "%s: final: %3s %3s %8s %8s %8s %8s %8s\n",
			tag, "ix", "f", "xc", "yc", "peak", "flux", "z");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: final: %3d %3d %8.3f %8.3f %8d %8d %8.3f\n",
			tag, ix,
			fiberbuf[ix].f,
			fiberbuf[ix].xc,
			fiberbuf[ix].yc,
			fiberbuf[ix].peak,
			fiberbuf[ix].flux,
			fiberbuf[ix].z);
	}

#define SORT_X

	// sort by x-coordinate
	qsort(fiberbuf, nfibers, sizeof(FIBER), (int (*)(const void *, const void *))comp_x);
	(void)fprintf(stderr, "%s: sorted-x: %3s %3s %8s %8s %8s %8s %8s\n",
					tag, "ix", "f", "xs", "ys", "peak", "flux", "z");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: sorted-x: %3d %3d %8.3f %8.3f %8d %8d %8.3f\n",
			tag, ix,
			fiberbuf[ix].f,
			fiberbuf[ix].xc,
			fiberbuf[ix].yc,
			fiberbuf[ix].peak,
			fiberbuf[ix].flux,
			fiberbuf[ix].z);
	}

#define SORT_Y

	// sort by x-coordinate
	qsort(fiberbuf, nfibers, sizeof(FIBER), (int (*)(const void *, const void *))comp_y);
	(void)fprintf(stderr, "%s: sorted-y: %3s %3s %8s %8s %8s %8s %8s\n",
					tag, "ix", "f", "xs", "ys", "peak", "flux", "z");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: sorted-y: %3d %3d %8.3f %8.3f %8d %8d %8.3f\n",
			tag, ix,
			fiberbuf[ix].f,
			fiberbuf[ix].xc,
			fiberbuf[ix].yc,
			fiberbuf[ix].peak,
			fiberbuf[ix].flux,
			fiberbuf[ix].z);
	}

#define SORT_F

	// sort by fiber number (order of detection)
	qsort(fiberbuf, nfibers, sizeof(FIBER), (int (*)(const void *, const void *))comp_f);
	(void)fprintf(stderr, "%s: sorted-f: %3s %3s %8s %8s %8s %8s %8s\n",
					tag, "ix", "f", "xs", "ys", "peak", "flux", "z");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: sorted-f: %3d %3d %8.3f %8.3f %8d %8d %8.3f\n",
			tag, ix,
			fiberbuf[ix].f,
			fiberbuf[ix].xc,
			fiberbuf[ix].yc,
			fiberbuf[ix].peak,
			fiberbuf[ix].flux,
			fiberbuf[ix].z);
	}

	return(nfibers);
}
