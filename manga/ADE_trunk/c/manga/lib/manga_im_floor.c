/* file: $RCSfile: manga_im_floor.c,v $
** rcsid: $Id: manga_im_floor.c,v 1.5 2013/04/16 19:03:57 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_floor.c,v 1.5 2013/04/16 19:03:57 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_floor.c,v $ - sets the floor of an image to zero.
** The xform space can be the same as the image space.
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_floor(int *xform, int *image, int nx, int ny)
{
	char *tag = "manga_im_floor";
	int x, y;

	(void)fprintf(stderr, "%s: do floor %dx%d\n", tag, nx, ny);

	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			int v = image[y*nx+x];
			if (v >= 0) {
				xform[y*nx+x] = v;
			} else {
				xform[y*nx+x] = 0;
			}
		}
	}

	return(0);
}
