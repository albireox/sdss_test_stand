/* file: $RCSfile: manga_im_spot.c,v $
** rcsid: $Id: manga_im_spot.c,v 1.1 2013/04/16 19:08:20 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_spot.c,v 1.1 2013/04/16 19:08:20 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_spot.c,v $ - finds the brightest spot in an image.
** We do a very simple thing:
** we slide a tile of some size over the image,
** and find the location where the sum over the tile is largest.
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_spot(double *xc, double *yc, int *image, int nx, int ny, int tilesize)
{
	char *tag = "manga_im_spot";
	int flux, xmax, ymax;
	int x, y, dx, dy;

	(void)fprintf(stderr, "%s: do spot %dx%d tilesize %d\n", tag, nx, ny, tilesize);

	int ds = tilesize;	// name shortcut

	flux = 0;
	xmax = 0;
	ymax = 0;
	for (y = ds/2; y < ny-ds/2; y++) {
		for (x = ds/2; x < nx-ds/2; x++) {
			int sum = 0;
			for (dy = -ds/2; dy < ds/2; dy++) {
				for (dx = -ds/2; dx < ds/2; dx++) {
					int v = image[(y+dy)*nx+(x+dx)];
					sum += v;
				}
			}
			if (sum > flux) {
				flux = sum;
				xmax = x;
				ymax = y;
#ifdef DEBUG
				(void)fprintf(stderr, "%s: xmax %3d ymax %3d flux %6d\n", tag, xmax, ymax, flux);
#endif
			} else if (sum == flux) {
#ifdef DEBUG
				(void)fprintf(stderr, "%s: xmax %3d ymax %3d flux %6d (same)\n", tag, xmax, ymax, flux);
#endif
			}
		}
	}

	// find the centroid in the winning tile
	flux = 0;
	int xsum = 0;
	int ysum = 0;
	for (dy = -ds/2; dy < ds/2; dy++) {
		for (dx = -ds/2; dx < ds/2; dx++) {
			x = xmax + dx;
			y = ymax + dy;
			int v = image[y*nx+x];
			xsum += v * dx;
			ysum += v * dy;
			flux += v;
		}
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: xsum %6d ysum %6d flux %6d\n", tag, xsum, ysum, flux);
#endif

	*xc = xmax + (double)xsum/flux;
	*yc = ymax + (double)ysum/flux;
	(void)fprintf(stderr, "%s: tilesize %d xc %f yc %f\n", tag, tilesize, *xc, *yc);

	return(0);
}
