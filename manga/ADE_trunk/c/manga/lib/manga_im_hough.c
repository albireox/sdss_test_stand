/* file: $RCSfile: manga_im_hough.c,v $
** rcsid: $Id: manga_im_hough.c,v 1.5 2013/04/16 19:04:15 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_hough.c,v 1.5 2013/04/16 19:04:15 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_hough.c,v $ - hough circular accumulator
** See http://en.wikipedia.org/wiki/Hough_transform
** We expect to have been given a Sobel transform of the image.
** This seems to be a little insensitive to the radius;
** varying the radius value around the "true" value
** still produces spikes in the transform space,
** but affects the local of structure around them.
** The centroid seems to be pretty reproduceable
** despite the variation is the radius used in the transform.
** Any change in the centroid for a given solution
** seems to be matched by a similar change in all the others.
** That is, the solutions may move a bit,
** but they move as a group.
** *******************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_hough(int *xform, int *image, int nx, int ny, int r0)
{
	char *tag = "manga_im_hough";
	int x, y;	// indexes into the image
	int a, b;	// indexes into the accumulator space

	(void)fprintf(stderr, "%s: transform %dx%d r0 %d\n", tag, nx, ny, r0);

	(void)memset(xform, 0, nx*ny*sizeof(int));

	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			int v = image[y*nx+x];
			if (v > 0) {
				for (a = x-r0; a <= x+r0; a++) {
					if (a < 0) {
						continue;
					} else if (a >= nx) {
						continue;
					}
					b = (int)(y - sqrt((double)r0*r0 - (x-a)*(x-a)) + 0.5);
					if (b < 0) {
						continue;
					} else if (b >= ny) {
						continue;
					}
					xform[b*nx+a] += v;
					b = (int)(y + sqrt((double)r0*r0 - (x-a)*(x-a)) + 0.5);
					if (b < 0) {
						continue;
					} else if (b >= ny) {
						continue;
					}
					xform[b*nx+a] += v;
				}
			}
		}
	}

	return(0);
}
