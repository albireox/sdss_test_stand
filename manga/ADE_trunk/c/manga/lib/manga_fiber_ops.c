/* file: $RCSfile: manga_fiber_ops.c,v $
** rcsid: $Id: manga_fiber_ops.c,v 1.5 2013/04/16 19:00:59 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_fiber_ops.c,v 1.5 2013/04/16 19:00:59 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_fiber_ops.c,v $ - various manga utilities
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include <stdlib.h>

#include "manga.h"

#define DEBUG

/**
 * The list of fiber AB coordinates for a hexagon of rank 7.
 * I could not find an algebraic expression to do this,
 * so I resort to a list.
 */

static FIBER_AB fiber_ab_list[MAX_FIBERS] = {
	{   0,   0,   0 },
	{   1,   1,   0 },
	{   2,   0,   1 },
	{   3,  -1,   1 },
	{   4,  -1,   0 },
	{   5,   0,  -1 },
	{   6,   1,  -1 },
	{   7,   2,   0 },
	{   8,   1,   1 },
	{   9,   0,   2 },
	{  10,  -1,   2 },
	{  11,  -2,   2 },
	{  12,  -2,   1 },
	{  13,  -2,   0 },
	{  14,  -1,  -1 },
	{  15,   0,  -2 },
	{  16,   1,  -2 },
	{  17,   2,  -2 },
	{  18,   2,  -1 },
	{  19,   3,   0 },
	{  20,   2,   1 },
	{  21,   1,   2 },
	{  22,   0,   3 },
	{  23,  -1,   3 },
	{  24,  -2,   3 },
	{  25,  -3,   3 },
	{  26,  -3,   2 },
	{  27,  -3,   1 },
	{  28,  -3,   0 },
	{  29,  -2,  -1 },
	{  30,  -1,  -2 },
	{  31,   0,  -3 },
	{  32,   1,  -3 },
	{  33,   2,  -3 },
	{  34,   3,  -3 },
	{  35,   3,  -2 },
	{  36,   3,  -1 },
	{  37,   4,   0 },
	{  38,   3,   1 },
	{  39,   2,   2 },
	{  40,   1,   3 },
	{  41,   0,   4 },
	{  42,  -1,   4 },
	{  43,  -2,   4 },
	{  44,  -3,   4 },
	{  45,  -4,   4 },
	{  46,  -4,   3 },
	{  47,  -4,   2 },
	{  48,  -4,   1 },
	{  49,  -4,   0 },
	{  50,  -3,  -1 },
	{  51,  -2,  -2 },
	{  52,  -1,  -3 },
	{  53,   0,  -4 },
	{  54,   1,  -4 },
	{  55,   2,  -4 },
	{  56,   3,  -4 },
	{  57,   4,  -4 },
	{  58,   4,  -3 },
	{  59,   4,  -2 },
	{  60,   4,  -1 },
	{  61,   5,   0 },
	{  62,   4,   1 },
	{  63,   3,   2 },
	{  64,   2,   3 },
	{  65,   1,   4 },
	{  66,   0,   5 },
	{  67,  -1,   5 },
	{  68,  -2,   5 },
	{  69,  -3,   5 },
	{  70,  -4,   5 },
	{  71,  -5,   5 },
	{  72,  -5,   4 },
	{  73,  -5,   3 },
	{  74,  -5,   2 },
	{  75,  -5,   1 },
	{  76,  -5,   0 },
	{  77,  -4,  -1 },
	{  78,  -3,  -2 },
	{  79,  -2,  -3 },
	{  80,  -1,  -4 },
	{  81,   0,  -5 },
	{  82,   1,  -5 },
	{  83,   2,  -5 },
	{  84,   3,  -5 },
	{  85,   4,  -5 },
	{  86,   5,  -5 },
	{  87,   5,  -4 },
	{  88,   5,  -3 },
	{  89,   5,  -2 },
	{  90,   5,  -1 },
	{  91,   6,   0 },
	{  92,   5,   1 },
	{  93,   4,   2 },
	{  94,   3,   3 },
	{  95,   2,   4 },
	{  96,   1,   5 },
	{  97,   0,   6 },
	{  98,  -1,   6 },
	{  99,  -2,   6 },
	{ 100,  -3,   6 },
	{ 101,  -4,   6 },
	{ 102,  -5,   6 },
	{ 103,  -6,   6 },
	{ 104,  -6,   5 },
	{ 105,  -6,   4 },
	{ 106,  -6,   3 },
	{ 107,  -6,   2 },
	{ 108,  -6,   1 },
	{ 109,  -6,   0 },
	{ 110,  -5,  -1 },
	{ 111,  -4,  -2 },
	{ 112,  -3,  -3 },
	{ 113,  -2,  -4 },
	{ 114,  -1,  -5 },
	{ 115,   0,  -6 },
	{ 116,   1,  -6 },
	{ 117,   2,  -6 },
	{ 118,   3,  -6 },
	{ 119,   4,  -6 },
	{ 120,   5,  -6 },
	{ 121,   6,  -6 },
	{ 122,   6,  -5 },
	{ 123,   6,  -4 },
	{ 124,   6,  -3 },
	{ 125,   6,  -2 },
	{ 126,   6,  -1 },
	{ 127,   7,   0 },
	{ 128,   6,   1 },
	{ 129,   5,   2 },
	{ 130,   4,   3 },
	{ 131,   3,   4 },
	{ 132,   2,   5 },
	{ 133,   1,   6 },
	{ 134,   0,   7 },
	{ 135,  -1,   7 },
	{ 136,  -2,   7 },
	{ 137,  -3,   7 },
	{ 138,  -4,   7 },
	{ 139,  -5,   7 },
	{ 140,  -6,   7 },
	{ 141,  -7,   7 },
	{ 142,  -7,   6 },
	{ 143,  -7,   5 },
	{ 144,  -7,   4 },
	{ 145,  -7,   3 },
	{ 146,  -7,   2 },
	{ 147,  -7,   1 },
	{ 148,  -7,   0 },
	{ 149,  -6,  -1 },
	{ 150,  -5,  -2 },
	{ 151,  -4,  -3 },
	{ 152,  -3,  -4 },
	{ 153,  -2,  -5 },
	{ 154,  -1,  -6 },
	{ 155,   0,  -7 },
	{ 156,   1,  -7 },
	{ 157,   2,  -7 },
	{ 158,   3,  -7 },
	{ 159,   4,  -7 },
	{ 160,   5,  -7 },
	{ 161,   6,  -7 },
	{ 162,   7,  -7 },
	{ 163,   7,  -6 },
	{ 164,   7,  -5 },
	{ 165,   7,  -4 },
	{ 166,   7,  -3 },
	{ 167,   7,  -2 },
	{ 168,   7,  -1 }
};

/**
 * Returns the number of fibers in a bundle of the given rank.
 */

int
manga_fiber_count(int rank)
{
	int n;

	n = 3*rank * (rank+1) + 1;

	return(n);
}

/**
 * Returns the AB coordinates of fiber "f".
 * Coerce the fiber number to a hexagon of rank <= MAX_RANK.
 */

FIBER_AB
manga_fiber_ab(int f)
{
	FIBER_AB fiber_ab;

	f = RANGE(f,0,MAX_FIBERS-1);

	fiber_ab = fiber_ab_list[f];

	return(fiber_ab);
}

/**
 * Converts fiber AB coordinates to XY coordinates.
 */

FIBER_XY
manga_fiber_ab2xy(FIBER_AB fiber_ab)
{
	FIBER_XY fiber_xy;

	fiber_xy.f = fiber_ab.f;
	fiber_xy.x = 2 * fiber_ab.a + fiber_ab.b;
	fiber_xy.y = fiber_ab.b * sqrt(3);

	return(fiber_xy);
}

/**
 * Converts fiber XY coordinates to AB coordinates.
 */

FIBER_AB
manga_fiber_xy2ab(FIBER_XY fiber_xy)
{
	FIBER_AB fiber_ab;

	fiber_ab.f = fiber_xy.f;
	fiber_ab.a = 0.5 * (fiber_xy.x - fiber_xy.y/sqrt(3));
	fiber_ab.b = fiber_xy.y / sqrt(3);

	return(fiber_ab);
}

/**
 * Returns the Plug Plate coordinates of fiber "f".
 */

FIBER_XY
manga_fiber_pp(int h)
{
	FIBER_XY fiber_xy;

	h = RANGE(h,0,MAX_PP-1);

	fiber_xy.f = h;
	fiber_xy.x = 4 * (h % 7);
	fiber_xy.y = 16 - 4 * (h / 7);

	return(fiber_xy);
}

/**
 * Finds the distance from the given (x, y) position to the given fiber.
 */

double
manga_fiber_distance(double x, double y, FIBER_XY fiber_xy)
{
	double dx, dy, d;

	dx = x - fiber_xy.x;
	dy = y - fiber_xy.y;

	d = sqrt(dx*dx + dy*dy);

	return(d);
}

/**
 * Looks up (in the fiber AB list) the fiber closest to the given (x, y) position
 */

FIBER_AB
manga_fiber_lookup(double x, double y)
{
	FIBER_AB fiber_ab, fiber_ab_best;
	FIBER_XY fiber_xy;
	int i;
	double d, dmin;

	// start with the first one
	fiber_ab_best = fiber_ab_list[0];
	fiber_xy = manga_fiber_ab2xy(fiber_ab_best);
	dmin = manga_fiber_distance(x, y, fiber_xy);

	for (i = 1; i < MAX_FIBERS; i++) {
		fiber_ab = fiber_ab_list[i];
		fiber_xy = manga_fiber_ab2xy(fiber_ab);
		d = manga_fiber_distance(x, y, fiber_xy);
		if (d < dmin) {
			dmin = d;
			fiber_ab_best = fiber_ab;
		}
	}

	return(fiber_ab_best);
}

/**
 * Forms a histogram of all possible fiber separations.
 * A fiber bundle of rank N will have width = 4N.
 * That is, separations as large as 4N will exist.
 * We will spread the given "nh" histogram slots
 * across the 4N span of the bundle.
 */

int
manga_fiber_spectrum(double *shist, int *nhist, int nh, int rank)
{
	char *tag = "manga_fiber_spectrum";
	int i, j;
	int nf;
	int n = 0;

	// initialize the results arrays
	for (i = 0; i < nh; i++) {
		double s = ((double)i / (nh-1)) * (4.0*rank);
		shist[i] = s;
		nhist[i] = 0;
	}

	rank = RANGE(rank,0,MAX_RANK);
	nf = manga_fiber_count(rank);
	(void)fprintf(stderr, "%s: rank %d count %3d\n", tag, rank, nf);

	// look at every pair of fibers. this is a n(n-1)/2 kind of problem.
	(void)fprintf(stderr, "%s: %6s %3s %3s %3s %3s %12s %4s %12s\n", tag, "n", "i", "j", "da", "db", "flix", "ix", "s");
	for (i = 0; i < nf; i++) {
		FIBER_AB fiber_ab1 = manga_fiber_ab(i);
		FIBER_XY fiber_xy1 = manga_fiber_ab2xy(fiber_ab1);
		for (j = i+1; j < nf; j++) {
			FIBER_AB fiber_ab2 = manga_fiber_ab(j);
			FIBER_XY fiber_xy2 = manga_fiber_ab2xy(fiber_ab2);
			double s = manga_fiber_distance(fiber_xy2.x, fiber_xy2.y, fiber_xy1);
			double flix = s/(4*rank) * (nh-1);	// floating-point index
			int ix = (int)(flix + 0.500001); // perturb it away from powers of 2
			(void)fprintf(stderr, "%s: %6d %3d %3d %3g %3g %12.6f %4d %12.6f\n",
					tag, n++, i, j,
					fiber_ab2.a - fiber_ab1.a,
					fiber_ab2.b - fiber_ab1.b,
					flix, ix, s);
			nhist[ix]++;
		}
	}
	return(0);
}
