/* file: $RCSfile: manga_im_ifu.c,v $
 ** rcsid: $Id: manga_im_ifu.c,v 1.15 2013/08/16 19:48:26 jwp Exp $
 ** Copyright Jeffrey W Percival
 ** *******************************************************************
 ** Space Astronomy Laboratory
 ** University of Wisconsin
 ** 1150 University Avenue
 ** Madison, WI 53706 USA
 ** *******************************************************************
 ** Do not use this software without permission.
 ** Do not use this software without attribution.
 ** Do not remove or alter any of the lines above.
 ** *******************************************************************
 */

static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_ifu.c,v 1.15 2013/08/16 19:48:26 jwp Exp $";

/*
 ** *******************************************************************
 ** $RCSfile: manga_im_ifu.c,v $ - finds the IFU in a list of fibers.
 ** Given a list of fibers,
 ** assume they represent an IFU
 ** and do the Absolute Orientation Problem (AOP)
 ** to figure out the rotation, scaling, and translation
 ** between IFU space and the image space.
 ** We do this in two steps:
 ** First, we do it crudely:
 ** 1. guess the scale factor from the histogram of fiber separations
 ** 2. guess the translation by averaging the fiber coordinates
 ** 3. guess the rotation by looking at the relationship of adjacent fibers
 ** Once this is done,
 ** we build the lists mapping IFU fibers onto image fibers,
 ** and pass the whole problem to the AOP solver,
 ** which performs a formal least-squares analysis on the data.
 **
 ** Which way do we want the transform to go? Which way is forward?
 ** Do we want to start in hexagon AB space and transform out to image space,
 ** or do we want to start with an image detection and transform back into AB space?
 ** The rotation matrix is orthonormal,
 ** hence easily invertible,
 ** and the translation is trivial,
 ** and the scale factor will either be larger than 1 (AB to image)
 ** or smaller than 1 (image to AB).
 ** Let's choose to keep c > 1,
 ** and call AB-to-image the forward transformation.
 ** In the Umeyama formulation above,
 ** that means that x represents AB-space and y the image space.
 ** It also means that the translation "t" is in units of image pixels,
 ** not AB-space units.
 **
 ** Final note on the translation:
 ** the AOP solver doesn't know we're doing hexagons.
 ** It is doing the mapping abstractly,
 ** so the translation is just what comes out of the fit.
 ** we choose here that the translation should represent the central fiber.
 **
 ** *******************************************************************
 */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "manga.h"

#define DEBUG

// from Numerical Recipes...
void correl(float data1[], float data2[], unsigned long n, float ans[]);

/**
 * Given a histogram of found pixel separations,
 * which looks like a hydrogen emission line spectrum
 * with spikes several pixels wide,
 * crawl along the separation axis finding spikes,
 * and measure their centroided positions.
 *
 * Returns the centroided position of the first spike,
 * which is the r=2 separation of adjacent fibers.
 * Scaling by this value allows us to transform the camera-pixel scale
 * back onto the unit grid of the a-b hexagon axes.
 */

/* the states for the sep_hist state machine */
#define S_INIT	(0)
#define S_SCAN	(1)
#define S_CENT	(2)
#define S_DONE	(3)

static double
sep_hist(int *shist, int nhist)
{
	char *tag = "manga_im_ifu: sep_hist";
	int s = 0;			// separation
	int done = 0;
	int state = S_INIT;
	int n, sum_n, sum_sn;
	double s0 = 0;		// the fundamental r=2 (nearest neighbor) separation
	double c;			// the current separation

	while (!done) {
		switch (state) {
		case S_INIT:
		default:
			sum_n = 0;
			sum_sn = 0;
			state = S_SCAN;
			break;
		case S_SCAN:
			if (s < nhist) {
				if (shist[s] > 0) {
					state = S_CENT;
				} else {
					s++; // stay in this state
				}
			} else {
				// no more data
				done++;
			}
			break;
		case S_CENT:
			n = shist[s];
			sum_n += n;
			sum_sn += s * n;
			s++;
			if (s < nhist) {
				if (shist[s] > 0) {
					// stay in this state
				} else {
					state = S_DONE;
				}
			} else {
				state = S_DONE;
			}
			break;
		case S_DONE:
			c = (double) sum_sn / sum_n;
			(void)fprintf(stderr, "%s: sum_sn %6d sum_n %3d c %12.6f\n", tag, sum_sn, sum_n, c);
			if ((sum_n > 10) && (s0 == 0)) {
				s0 = c;
			}
			state = S_INIT;
			break;
		}
	}
	(void)fprintf(stderr, "%s: s0 %8.3f\n", tag, s0);

	return(s0);
}

// sort by fiber number
static int
comp_f(const FIBER_XY *v1, const FIBER_XY *v2)
{
	if (v1->f < v2->f) {
		return(-1);
	} else if (v1->f > v2->f) {
		return(1);
	}
	return(0);
}

// sort by x-coordinate
static int
comp_x(const FIBER_XY *v1, const FIBER_XY *v2)
{
	if (v1->x < v2->x) {
		return(-1);
	} else if (v1->x > v2->x) {
		return(1);
	}
	return(0);
}

// sort by y-coordinate
static int
comp_y(const FIBER_XY *v1, const FIBER_XY *v2)
{
	if (v1->y < v2->y) {
		return(-1);
	} else if (v1->y > v2->y) {
		return(1);
	}
	return(0);
}

// radius histogram in pixels
#define NSHIST	(2048)
static int shist[NSHIST];

/**
 * Finds fibers in an image.
 * @return the number of found fibers
 */

int
manga_im_ifu(AOP *paop, FIBER_XY xy_list[], FIBER_XY im_list[], int nfibers)
{
	char *tag = "manga_im_ifu";
	AOP aop;
	int ix, jx;		// loop indexes
	int nh;			// number of histogram elements
	int rank = 6;	// assume the largest ifu; will shrink as needed
	int s;			// separation in pixels
	double xs_save = 0, ys_save = 0;
	int sum_save = 0;

#define GUESS_THETA

	/**
	 * We want to guess the rotation angle of the bundle.
	 * We might use the outermost two fibers in the central row,
	 * but vignetting and other lighting effects
	 * sometimes render these unusable.
	 * If we sort the fibers by y-coordinate,
	 * then adjacent fibers are slightly off-horizontal
	 * and will differ slightly in y.
	 * Their dy/dx is an estimate of tan(theta).
	 * We accumulate many of these estimates over a bundle,
	 * and find the commonest (the mode) of the estimates
	 * to guess theta.
	 */
	double dx = 0;
	double dy = 0;
#define MODEBUFLEN	(90)
	int modebuf[MODEBUFLEN] = {0};

#define SORT_Y

	qsort(im_list, nfibers, sizeof(FIBER_XY), (int (*)(const void *, const void *))comp_y);
	(void)fprintf(stderr, "%s: sorted-y: %3s %5s %8s %8s %8s %8s %8s %3s\n", tag,
			"ix", "im.f", "im.x", "im.y", "dx", "dy", "dy/dx", "bin");
	for (ix = 0; ix < nfibers; ix++) {
		/**
		 * bin is the index into a histogram of tan(theta).
		 * We choose a bin width of 0.035,
		 * which is about 2 degrees.
		 * Finer binning risks flattening out the peak;
		 * coarser binning decreases precision in the guess.
		 */
		int bin = 0;
		double dydx = 0;
		if (ix > 0) {
			dx = im_list[ix].x - im_list[ix-1].x;
			dy = im_list[ix].y - im_list[ix-1].y;
			dydx = dy/dx;
			bin = (int)((dydx) / 0.035 + 0.5);
		}
		(void)fprintf(stderr, "%s: sorted-y: %3d %5d %8.3f %8.3f %8.3f %8.3f %8.3f %3d\n",
				tag, ix, im_list[ix].f, im_list[ix].x, im_list[ix].y, dx, dy, dydx, bin);
		// we want the mode of the values of n
		modebuf[MODEBUFLEN/2 + bin]++; // offset by len/2 to make the buffer symmetric about zero
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

	int peak = 0;
	int peakix = 0;
	for (ix = 0; ix < MODEBUFLEN; ix++) {
		(void)fprintf(stderr, "%s: modebuf[%3d] = %2d\n", tag, ix - MODEBUFLEN/2, modebuf[ix]);
		if (modebuf[ix] > peak) {
			peak = modebuf[ix];
			peakix = ix - MODEBUFLEN/2;
		}
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
	(void)fprintf(stderr, "%s: peakix %3d peak %3d\n", tag, peakix, peak);
	// do a rough centroid
	{
		int x1 = peakix - 1;
		int x2 = peakix + 0;
		int x3 = peakix + 1;
		double w1 = modebuf[x1 + MODEBUFLEN/2];
		double w2 = modebuf[x2 + MODEBUFLEN/2];
		double w3 = modebuf[x3 + MODEBUFLEN/2];
		(void)fprintf(stderr, "%s: x1 %8d x2 %8d x3 %8d\n", tag, x1, x2, x3);
		(void)fprintf(stderr, "%s: w1 %8.3f w2 %8.3f w3 %8.3f\n", tag, w1, w2, w3);
		double pbar = (x1*w1 + x2*w2 + x3*w3) / (w1+w2+w3);
		(void)fprintf(stderr, "%s: pbar %12.6f\n", tag, pbar);
		aop.theta = atan(pbar * 0.035);
	}
	(void)fprintf(stderr, "%s: theta %12.6f deg\n", tag, aop.theta*(180/M_PI));

#ifdef NOPE
	// sort by x-coordinate
#define SORT_X
	qsort(im_list, nfibers, sizeof(FIBER_XY), (int(*)(const void *, const void *))comp_x);
	(void)fprintf(stderr, "%s: sorted-x: %3s %5s %8s %8s\n", tag, "ix", "im.f", "im.x", "im.y");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: sorted-x: %3d %5d %8.3f %8.3f\n", tag,
				ix, im_list[ix].f, im_list[ix].x, im_list[ix].y);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
#endif

#ifdef NOPE
	// sort by f-coordinate
#define SORT_F
	qsort(im_list, nfibers, sizeof(FIBER_XY), (int(*)(const void *, const void *))comp_f);
	(void)fprintf(stderr, "%s: sorted-f: %3s %5s %8s %8s\n", tag, "i", "im.f", "im.x", "im.y");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: sorted-f: %3d %5d %8.3f %8.3f\n", tag,
				ix, im_list[ix].f, im_list[ix].x, im_list[ix].y);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
#endif

#define SEPARATION_HISTOGRAM

	// do the histogram of fiber separations (in pixels)
	for (s = 0; s < NSHIST; s++) {
		shist[s] = 0;
	}
	nh = 0;
	(void)fprintf(stderr, "%s: hist: %3s %3s %8s %8s %8s %3s\n", tag, "ix", "jx", "dx", "dy", "sep", "nh");
	for (ix = 0; ix < nfibers; ix++) {
		for (jx = ix + 1; jx < nfibers; jx++) {
			double dx = im_list[jx].x - im_list[ix].x;
			double dy = im_list[jx].y - im_list[ix].y;
			double sep = sqrt(dx*dx + dy*dy);
			(void)fprintf(stderr, "%s: hist: %3d %3d %8.3f %8.3f %8.3f %3d\n", tag, ix, jx, dx, dy, sep, nh);
			int s = (int)(sep + 0.5);
			s = RANGE(s,0,NSHIST-1);
			shist[s]++;
			nh = MAX(s+1, nh);
		}
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

	(void)fprintf(stderr, "%s: shist1: %4s %4s\n", tag, "s", "n");
	for (s = 0; s < nh; s++) {
		(void)fprintf(stderr, "%s: shist1: %4d %4d\n", tag, s, shist[s]);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

#define GUESS_SCALE

	// find the fundamental (s=2r) separation
	double s0 = sep_hist(shist, NSHIST);
	aop.c = s0 / 2;
	(void)fprintf(stderr, "%s: scale: s0 %8.3f c %12.6f\n", tag, s0, aop.c);
	// scale the separation axis onto the unit (r=1) grid
	(void)fprintf(stderr, "%s: shist2: %8s %4s\n", tag, "s", "n");
	for (s = 0; s < nh; s++) {
		(void)fprintf(stderr, "%s: shist2: %8.3f %4d\n", tag, s/aop.c, shist[s]);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

#define GUESS_RANK

	double diameter = nh / aop.c;
	double epsilon = 0.1;	// quantization error
	(void)fprintf(stderr, "%s: diameter %8.3f epsilon %8.3f\n", tag, diameter, epsilon);
	rank = 1;	// default
    if (diameter >  4+epsilon) rank = 2;
	if (diameter >  8+epsilon) rank = 3;
	if (diameter > 12+epsilon) rank = 4;
	if (diameter > 16+epsilon) rank = 5;
	if (diameter > 20+epsilon) rank = 6;
	int mfc = manga_fiber_count(rank);
	(void)fprintf(stderr, "%s: rank: %d mfc: %d\n", tag, rank, mfc);

#define GUESS_TRANSLATION

	// compute the average of fiber positions to estimate the center of the bundle
	double x_sum = 0;
	double y_sum = 0;
	for (ix = 0; ix < nfibers; ix++) {
		x_sum += im_list[ix].x;
		y_sum += im_list[ix].y;
	}
	aop.sx = x_sum / nfibers;
	aop.sy = y_sum / nfibers;
	(void)fprintf(stderr, "%s: translation: sx %8.3f sy %8.3f\n", tag, aop.sx, aop.sy);

	(void)fprintf(stderr, "%s: aop: guess:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(stderr, "%s: aop: guess: theta: %12.6f deg\n", tag, aop.theta * (180/M_PI));
	(void)fprintf(stderr, "%s: aop: guess:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(stderr, "%s: aop: guess:    sy: %12.6f\n", tag, aop.sy);

	/**
	 * OK.
	 * The worst guess is the translation, (sx, sy).
	 * We took the average of all the fibers,
	 * but missing fibers
	 * (as in the C-Tech Rank-4 test IFU with 30 out of 61 fibers)
	 * may mess up the average.
	 * Before we can do the mapping and snapping,
	 * we need to improve our guess.
	 * Map the back onto the ideal grid using our guess,
	 * but understand that the x- and y-coordinates will have some unknown offset.
	 * Do a correlation with the ideal hexagon to derive a lag.
	 */

	FIBER_XY sample[MAX_FIBERS];
	(void)fprintf(stderr, "%s: sample: %3s %8s %8s %8s\n", tag, "ix", "sample.x", "sample.y", "sample.r");
	for (ix = 0; ix < nfibers; ix++) {
		// set up some scratch variables
		double x = im_list[ix].x;
		double y = im_list[ix].y;
		// un-translate it with our crude guess, to center it on zero as best we can
		x -= aop.sx;
		y -= aop.sy;
		// un-scale it
		x /= aop.c;
		y /= aop.c;
		// un-rotate it
		sample[ix].x = cos(-aop.theta) * x - sin(-aop.theta) * y;
		sample[ix].y = sin(-aop.theta) * x + cos(-aop.theta) * y;
		x = sample[ix].x;
		y = sample[ix].y;
		double r = sqrt(x*x + y*y);
		(void)fprintf(stderr, "%s: sample: %3d %8.3f %8.3f %8.3f\n", tag, ix, x, y, r);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

#define ONE_D_CORRELATION
#ifdef NOPE
	// create two histograms (sample vs. model) for each axis.
	// we will spread +/-12 radii into NCHIST bins
#define NCHIST	(256)
	float mx[NCHIST] = {0};	// model x
	float my[NCHIST] = {0};	// model y
	float sx[NCHIST] = {0};	// sample x
	float sy[NCHIST] = {0};	// sample y
	// load the model
	(void)fprintf(stderr, "%s: model: %3s %8s %8s\n", tag, "ix", "model.x", "model.y");
	for (ix = 0; ix < mfc; ix++) {
		FIBER_AB ab = manga_fiber_ab(ix);
		FIBER_XY model = manga_fiber_ab2xy(ab);
		(void)fprintf(stderr, "%s: model: %3d %8.3f %8.3f\n", tag, ix, model.x, model.y);
		int xbin = (int)(((model.x/12) * NCHIST/2) + NCHIST/2 + 0.5);
		int ybin = (int)(((model.y/12) * NCHIST/2) + NCHIST/2 + 0.5);
		mx[xbin] = 1.0;
		my[ybin] = 1.0;
	}
	// load the sample
	for (ix = 0; ix < nfibers; ix++) {
		int xbin = (int)(((sample[ix].x/12) * NCHIST/2) + NCHIST/2 + 0.5);
		int ybin = (int)(((sample[ix].y/12) * NCHIST/2) + NCHIST/2 + 0.5);
		sx[xbin] = 1.0;
		sy[ybin] = 1.0;
	}
	(void)fprintf(stderr, "%s: correlation: %3s %8s %8s %8s %8s %8s\n",
				tag, "ix", "r", "mx", "my", "sx", "sy");
	for (ix = 0; ix < NCHIST; ix++) {
		double r = 12 * (ix - NCHIST/2) / (double)(NCHIST/2);
		(void)fprintf(stderr, "%s: correlation: %3d %8.3f %8.3f %8.3f %8.3f %8.3f\n",
				tag, ix, r, mx[ix], my[ix], sx[ix], sy[ix]);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

	// prepare for the correlation itself
	float ans[2*NCHIST] = {0};
#define X_CORRELATION
	correl(mx, sx, NCHIST, ans);
	(void)fprintf(stderr, "%s: x-corr: %3s %8s %8s %8s %8s\n", tag, "ix", "r", "mx", "sx", "ans");
	for (ix = NCHIST/2; ix < NCHIST; ix++) {
		double r = 12 * (ix - NCHIST) / (double)(NCHIST/2);
		(void)fprintf(stderr, "%s: x-corr: %3d %8.3f %8.3f %8.3f %8.3f\n", tag, ix, r, mx[ix], sx[ix], ans[ix]);
	}
	for (ix = 0; ix < NCHIST/2; ix++) {
		double r = 12 * (ix) / (double)(NCHIST/2);
		(void)fprintf(stderr, "%s: x-corr: %3d %8.3f %8.3f %8.3f %8.3f\n", tag, ix, r, mx[ix], sx[ix], ans[ix]);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
#define Y_CORRELATION
	correl(my, sy, NCHIST, ans);
	(void)fprintf(stderr, "%s: y-corr: %3s %8s %8s %8s %8s\n", tag, "ix", "r", "my", "sy", "ans");
	for (ix = NCHIST/2; ix < NCHIST; ix++) {
		double r = 12 * (ix - NCHIST) / (double)(NCHIST/2);
		(void)fprintf(stderr, "%s: y-corr: %3d %8.3f %8.3f %8.3f %8.3f\n", tag, ix, r, my[ix], sy[ix], ans[ix]);
	}
	for (ix = 0; ix < NCHIST/2; ix++) {
		double r = 12 * (ix) / (double)(NCHIST/2);
		(void)fprintf(stderr, "%s: y-corr: %3d %8.3f %8.3f %8.3f %8.3f\n", tag, ix, r, my[ix], sy[ix], ans[ix]);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
#endif

#define TWO_D_CORRELATION
#ifdef NOPE
	/**
	 * that didn't work so well.
	 * try a limited but real 2-d correlation.
	 * we'll use the roughly centered sample from the 1-d correlation attempt,
	 * and shuffle it around by a few mm looking for a good match.
	 */
	xs_save = 0;
	ys_save = 0;
	sum_save = 0;
	double xs, ys;
	double r = 2;	// half-width of search in mm
	for (xs = -r; xs < r; xs += 0.010) {
		for (ys = -r; ys < r; ys += 0.010) {
			int sum = 0;
			//(void)fprintf(stderr, "%s: align trial.xs %8.3f trial.ys %8.3f\n", tag, xs, ys);
			/**
			 * at this offset,
			 * are there any fibers in the sample that overlap with the model?
			 * run through each sample fiber and test it against the model.
			 * the sample fibers have some uncertainty in their positions;
			 * use a 2-pixel blur circle.
			 */
			for (ix = 0; ix < mfc; ix++) {
				FIBER_AB ab = manga_fiber_ab(ix);
				FIBER_XY model = manga_fiber_ab2xy(ab);
				//(void)fprintf(stderr, "%s: match  model fiber %3d x %8.3f y %8.3f\n", tag, ix, model.x, model.y);
				for (jx = 0; jx < nfibers; jx++) {
					double dx = model.x - (sample[jx].x - xs);
					double dy = model.y - (sample[jx].y - ys);
					double dr = sqrt(dx*dx + dy*dy);
/*					(void)fprintf(stderr, "%s: match sample fiber %3d x %8.3f y %8.3f dx %8.3f dy %8.3f dr %8.3f\n",
								tag, jx, sample[jx].x, sample[jx].y, dx, dy, dr);*/
					if (dr <= 0.100) {
						sum += 1;
						(void)fprintf(stderr, "%s: hit sum %3d ix %3d jx %3d dx %8.3f dy %8.3f dr %8.3f xs %8.3f ys %8.3f\n",
								tag, sum, ix, jx, dx, dy, dr, xs, ys);
					}
				}
			}
			if (sum > sum_save) {
				(void)fprintf(stderr, "%s: good sum %3d xs %8.3f ys %8.3f\n", tag, sum, xs, ys);
				sum_save = sum;
				xs_save = xs;
				ys_save = ys;
			}
		}
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
	(void)fprintf(stderr, "%s: corr sum_save %3d xs_save %8.3f ys_save %8.3f\n", tag, sum_save, xs_save, ys_save);
#endif

#define TRY_ALL_OVERLAYS
	/**
	 * Instead of differentially shuffling the sample over the model by a few mm,
	 * which assumes we are approximately aligned,
	 * instead try all possible overlays.
	 * That is,
	 * pick a fiber in the sample,
	 * and place it over each fiber in the model.
	 * Add up the hits and choose the best overlay.
	 * This should match the previous method for shift errors of a few mm;
	 * for larger shifts,
	 * this method will succeed where the differential method fails.
	 * Which sample fiber should we pick?
	 * Subtle thing learned by JWP on 06-Aug-2013:
	 * the sample coordinates above claim to be unrotated
	 * by the guessed aop.theta.
	 * But our coarseness in the MODETAB histogram is about 2 degrees,
	 * so for small (but nonzero) rotations
	 * the guessed theta can be much closer to zero than the actual rotation.
	 * So the unrotation is illusory,
	 * and the sample coordinates still have residual rotation.
	 * So when we overlay the "unrotated" sample
	 * onto the model,
	 * the errors near the edge of the IFU can be larger
	 * than just a few tenths of radius.
	 * So, finally, to pick a sample fiber for this test,
	 * pick the one closest to the origin,
	 * not an edge one, which might have a larger rotation error.
	 * sample[0] is a poor choice,
	 * as the y-sorted array will make sample[0] one of the edge fibers.
	 * Note that the edge fibers will still have errors from the model
	 * in excess of a few tenths of radius,
	 * but at least we'll have a lot more close hits for the interior ones.
	 */

	int picked = 0;
	double best_r = 1e9;	// should be good enough!

	for (ix = 0; ix < nfibers; ix++) {
		double x = sample[ix].x;
		double y = sample[ix].y;
		double r = sqrt(x*x + y*y);
		(void)fprintf(stderr, "%s: picker: %3d x %8.3f y %8.3f r %8.3f\n", tag, ix, x, y, r);
		if (r < best_r) {
			(void)fprintf(stderr, "%s: picker: %3d x %8.3f y %8.3f r %8.3f new best\n", tag, ix, x, y, r);
			best_r = r;
			picked = ix;
		}
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
	(void)fprintf(stderr, "%s: best pick %3d\n", tag, picked);

	// we're going to use sample fiber 0 and try all possible overlays
	xs_save = 0;
	ys_save = 0;
	sum_save = 0;
	int f;
	for (f = 0; f < mfc; f++) {
		// retrieve the model fiber
		FIBER_AB ab = manga_fiber_ab(f);
		FIBER_XY model = manga_fiber_ab2xy(ab);
		// align sample fiber 0 to this model fiber
		double xs = sample[picked].x - model.x;
		double ys = sample[picked].y - model.y;
		double rs = sqrt(xs*xs + ys*ys);
		(void)fprintf(stderr, "%s: align sample fiber %3d sample.x %8.3f sample.y %8.3f\n",
				tag, picked, sample[picked].x, sample[picked].y);
		(void)fprintf(stderr, "%s: align  model fiber %3d  model.x %8.3f  model.y %8.3f\n",
				tag, f, model.x, model.y);
		(void)fprintf(stderr, "%s: align  trial shift %3d trial.xs %8.3f trial.ys %8.3f trial.rs %8.3f\n",
				tag, f, xs, ys, rs);
		int sum = 0;
		/**
		 * at this offset,
		 * are there any fibers in the sample that overlap with the model?
		 * run through each sample fiber and test it against the model.
		 * the sample fibers have some uncertainty in their positions (see above);
		 * be generous (R=1), and let the votes carry the day.
		 */
		for (ix = 0; ix < mfc; ix++) {
			FIBER_AB ab = manga_fiber_ab(ix);
			FIBER_XY model = manga_fiber_ab2xy(ab);
			//(void)fprintf(stderr, "%s:  model ix %3d  model.x %8.3f  model.y %8.3f\n", tag, ix, model.x, model.y);
			for (jx = 0; jx < nfibers; jx++) {
				double dx = model.x - (sample[jx].x - xs);
				double dy = model.y - (sample[jx].y - ys);
				double dr = sqrt(dx*dx + dy*dy);
/*				(void)fprintf(stderr, "%s: sample jx %3d sample.x %8.3f sample.y %8.3f dx %8.3f dy %8.3f dr %8.3f\n",
										tag, jx, sample[jx].x, sample[jx].y, dx, dy, dr);*/
				if (dr <= 1.000) {
					sum += 1;
/*					(void)fprintf(stderr, "%s: new hit sum %3d ix %3d jx %3d dx %8.3f dy %8.3f dr %8.3f xs %8.3f ys %8.3f\n",
							tag, sum, ix, jx, dx, dy, dr, xs, ys);*/
				}
			}
		}
		(void)fprintf(stderr, "%s: overlay hit sum %3d\n", tag, sum);
		if (sum > sum_save) {
			(void)fprintf(stderr, "%s: new best hit sum %3d sum_save %3d xs %8.3f ys %8.3f\n", tag, sum, sum_save, xs, ys);
			sum_save = sum;
			xs_save = xs;
			ys_save = ys;
		}
	}
	(void)fprintf(stderr, "%s: eol\n", tag);
	(void)fprintf(stderr, "%s: corr sum_save %3d xs_save %8.3f ys_save %8.3f\n", tag, sum_save, xs_save, ys_save);

#define MAP_BACK

	FIBER_AB ab_list[MAX_FIBERS];
	// map the fibers back onto the ideal grid using our guess aop
	(void)fprintf(stderr, "%s: ready to map\n", tag); // mark this point in the output
	for (ix = 0; ix < nfibers; ix++) {
		// set up some scratch variables
		double x = im_list[ix].x;
		double y = im_list[ix].y;
		// un-translate it
		x -= aop.sx;
		y -= aop.sy;
		// un-scale it
		x /= aop.c;
		y /= aop.c;
		// un-rotate it
		xy_list[ix].x = cos(-aop.theta) * x - sin(-aop.theta) * y;
		xy_list[ix].y = sin(-aop.theta) * x + cos(-aop.theta) * y;
		// apply our correlation correction
		xy_list[ix].x -= xs_save;
		xy_list[ix].y -= ys_save;
		ab_list[ix] = manga_fiber_lookup(xy_list[ix].x, xy_list[ix].y);
		// go back to xy-space with the ab point.
		xy_list[ix] = manga_fiber_ab2xy(ab_list[ix]);
		im_list[ix].f = ab_list[ix].f; // the proper fiber number (spiral numbering)
	}

#define SORT_F

	qsort(ab_list, nfibers, sizeof(FIBER_XY), (int(*)(const void *, const void *))comp_f);
	qsort(xy_list, nfibers, sizeof(FIBER_XY), (int(*)(const void *, const void *))comp_f);
	qsort(im_list, nfibers, sizeof(FIBER_XY), (int(*)(const void *, const void *))comp_f);

	(void)fprintf(stderr, "%s: mapped: %3s %5s %8s %8s %8s %8s %8s %8s\n",
			tag, "ix", "ab.f", "ab.a", "ab.b", "xy.x", "xy.y", "im.x", "im.y");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: mapped: %3d %5d %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n",
				tag, ix, ab_list[ix].f, ab_list[ix].a, ab_list[ix].b,
				xy_list[ix].x, xy_list[ix].y, im_list[ix].x, im_list[ix].y);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

#define DO_AOP

	// now we have the desired data for the AOP
	aop = manga_im_aop(xy_list, im_list, nfibers);

	// override the translation with the position of fiber 0
	//aop.sx = im_list[0].x;
	//aop.sy = im_list[0].y;
	//(void)fprintf(stderr, "%s: new sx %8.3f sy %8.3f\n", tag, aop.sx, aop.sy);

	(void)fprintf(stderr, "%s: aop: final:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(stderr, "%s: aop: final: theta: %12.6f deg\n", tag, aop.theta * (180/M_PI));
	(void)fprintf(stderr, "%s: aop: final:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(stderr, "%s: aop: final:    sy: %12.6f\n", tag, aop.sy);

	/**
	 * Now we have what may be a partially populated list of fibers,
	 * if some were not found.
	 * Start with a default list of fibers,
	 * and replace the fibers in the default list with the ones we found.
	 * Unfound fibers will retain their default values.
	 */

#define BUILD_TEMPLATE

	FIBER_XY base_xy[MAX_FIBERS];
	FIBER_XY base_im[MAX_FIBERS];
	(void)fprintf(stderr, "%s: template: %3s %3s %8s %8s %8s %8s\n",
			tag, "ix", "f", "xy.x", "xy.y", "im.x", "im.y");
	for (ix = 0; ix < mfc; ix++) {
		FIBER_AB ab = manga_fiber_ab(ix);
		base_xy[ix] = manga_fiber_ab2xy(ab);
		// rotate it
		base_im[ix].x = cos(aop.theta) * base_xy[ix].x - sin(aop.theta) * base_xy[ix].y;
		base_im[ix].y = sin(aop.theta) * base_xy[ix].x + cos(aop.theta) * base_xy[ix].y;
		// scale it
		base_im[ix].x *= aop.c;
		base_im[ix].y *= aop.c;
		// translate it
		base_im[ix].x += aop.sx;
		base_im[ix].y += aop.sy;
		(void)fprintf(stderr, "%s: template: %3d %3d %8.3f %8.3f %8.3f %8.3f\n", tag, ix,
			base_xy[ix].f, base_xy[ix].x, base_xy[ix].y, base_im[ix].x, base_im[ix].y);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

#define FILL_IN_FIBERS

	// now fill in the found fibers
	int max_f = 0;
	for (ix = 0; ix < nfibers; ix++) {
		int f = xy_list[ix].f;
		if (f > max_f) {
			max_f = f;
		}
		base_xy[f] = xy_list[ix];
		base_im[f] = im_list[ix];
	}
	(void)fprintf(stderr, "%s: max_f %d\n", tag, max_f);

	// copy back into the caller's lists
	for (ix = 0; ix < mfc; ix++) {
		xy_list[ix] = base_xy[ix];
		im_list[ix] = base_im[ix];
	}

#define DO_RESIDUALS

	// show the residuals
	(void)fprintf(stderr, "%s: residuals: %3s %8s %8s %8s %8s %8s %8s %8s %8s\n",
			tag, "ix", "model.f", "model.x", "model.y", "image.x", "image.y", "dx", "dy", "dr");
	for (ix = 0; ix <= max_f; ix++) {
		// start with the model coordinates
		double x = xy_list[ix].x;
		double y = xy_list[ix].y;
		// apply the model to move into pixel space
		double tmpx = aop.c * (cos(aop.theta) * x - sin(aop.theta) * y) + aop.sx;
		double tmpy = aop.c * (sin(aop.theta) * x + cos(aop.theta) * y) + aop.sy;
		x = tmpx;
		y = tmpy;
		double dx = im_list[ix].x - x;
		double dy = im_list[ix].y - y;
		double dr = sqrt(dx*dx + dy*dy);
		(void)fprintf(stderr, "%s: residuals: %3d %8d %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n",
				tag, ix,
				xy_list[ix].f,
				x, y,
				im_list[ix].x, im_list[ix].y,
				dx, dy, dr);
	}
	(void)fprintf(stderr, "%s: eol\n", tag);

	*paop = aop;
	return(mfc);
}
