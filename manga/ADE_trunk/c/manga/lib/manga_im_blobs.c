/* file: $RCSfile: manga_im_blobs.c,v $
** rcsid: $Id: manga_im_blobs.c,v 1.5 2013/04/16 19:02:44 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_blobs.c,v 1.5 2013/04/16 19:02:44 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_blobs.c,v $ - blob finder
** We do a convolution with a gaussian kernal,
** then compute the laplacian.
** See "Laplacian of Gaussian" at http://en.wikipedia.org/wiki/Blob_detection
** *******************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

// the gaussian convolution kernel
#define NK	(11)
static double K[NK][NK];

// the X derivative convolution kernel
static int GX[3][3] = {
	{-1, 0, 1},
	{-2, 0, 2},
	{-1, 0, 1}
};

// the Y derivative convolution kernel
static int GY[3][3] = {
	{-1, -2, -1},
	{ 0,  0,  0},
	{ 1,  2,  1}
};

static int *gx, *gxx;
static int *gy, *gyy;

int
manga_im_blobs(int *xform, int *image, int nx, int ny)
{
	char *tag = "manga_im_blobs";
	double mean, sigma;
	int i, j;	// indexes into the kernel
	int min, max;
	int x, y;	// indexes into the image

	(void)fprintf(stderr, "%s: transform %dx%d\n", tag, nx, ny);

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gx memory\n", tag);
#endif
	gx = (int *)malloc(nx*ny*sizeof(int));
	if (gx == NULL) {
		perror(tag);
		return(-1);
	}
	(void)memset(gx, 0, nx*ny*sizeof(int));

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gxx memory\n", tag);
#endif
		gxx = (int *)malloc(nx*ny*sizeof(int));
		if (gxx == NULL) {
			perror(tag);
			return(-1);
		}
		(void)memset(gxx, 0, nx*ny*sizeof(int));

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gy memory\n", tag);
#endif
	gy = (int *)malloc(nx*ny*sizeof(int));
	if (gy == NULL) {
		perror(tag);
		return(-1);
	}
	(void)memset(gy, 0, nx*ny*sizeof(int));

#ifdef DEBUG
	(void)fprintf(stderr, "%s: allocate gyy memory\n", tag);
#endif
	gyy = (int *)malloc(nx*ny*sizeof(int));
	if (gyy == NULL) {
		perror(tag);
		return(-1);
	}
	(void)memset(gyy, 0, nx*ny*sizeof(int));

	/**
	 * Compute the gaussian kernel
	 */

	double t = (NK*NK)/2.0;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: t %f\n", tag, t);
#endif
	for (j = 0; j < NK; j++) {
		for (i = 0; i < NK; i++) {
			double r2 = (i-NK/2)*(i-NK/2) + (j-NK/2)*(j-NK/2);
			//(void)fprintf(stderr, "%s: r2 %f\n", tag, r2);
			double f = (1/(2*M_PI*t))*exp(-r2/(2*t));
			//(void)fprintf(stderr, "%s: f %f\n", tag, f);
			K[j][i] = f;
		}
	}

#ifdef DEBUG
	for (j = 0; j < NK; j++) {
		(void)fprintf(stderr, "%2d:", j);
		for (i = 0; i < NK; i++) {
			(void)fprintf(stderr, " %9.6f", K[j][i]);
		}
		(void)fprintf(stderr, "\n");
	}
#endif

	/**
	 * Do the convolution
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the convolution\n", tag);
#endif
	for (y = NK/2; y < ny-NK/2; y++) {
		for (x = NK/2; x < nx-NK/2; x++) {
			double v = 0;
			for (j = 0; j < NK; j++) {
				for  (i = 0; i < NK; i++) {
					v += K[j][i]*image[(y+j-NK/2)*nx+(x+i-NK/2)];
				}
			}
			xform[y*nx+x] = (int)(v + 0.5);
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the laplacian\n", tag);
#endif

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gx xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GX[j][i]*image[(y+j-1)*nx+(x+i-1)];
				}
			}
			gx[y*nx+x] = v;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gxx xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GX[j][i]*gx[(y+j-1)*nx+(x+i-1)];
				}
			}
			gxx[y*nx+x] = v;
		}
	}

	/**
	 * Do the Y gradient
	 */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gy xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GY[j][i]*image[(y+j-1)*nx+(x+i-1)];
				}
			}
			gy[y*nx+x] = v;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: do the gyy xform\n", tag);
#endif
	for (y = 1; y < ny-1; y++) {
		for (x = 1; x < nx-1; x++) {
			int v = 0;
			for (j = 0; j < 3; j++) {
				for  (i = 0; i < 3; i++) {
					v += GY[j][i]*gy[(y+j-1)*nx+(x+i-1)];
				}
			}
			gyy[y*nx+x] = v;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: load the laplacian\n", tag);
#endif
	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			int v = gxx[y*nx+x] + gyy[y*nx+x];
			v = -v;	// we negate v to make bright blobs positive, dark ones negative
			xform[y*nx+x] = v;
		}
	}

	(void)manga_im_stats(&min, &max, &mean, &sigma, xform, nx, ny);
	(void)fprintf(stderr, "%s: min %d max %d mean %f sigma %f\n", tag, min, max, mean, sigma);

	free(gx);
	free(gxx);
	free(gy);
	free(gyy);

	return(0);
}
