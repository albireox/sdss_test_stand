/* file: $RCSfile: manga_im_sim.c,v $
** rcsid: $Id: manga_im_sim.c,v 1.8 2013/06/13 15:40:26 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_sim.c,v 1.8 2013/06/13 15:40:26 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_sim.c,v $ - simulate a manga fiber bundle
** *******************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "manga.h"

#undef DEBUG

/**
 * Places a  fiber into the image at the given coordinates.
 */

static int
place(int *image, int nx, int ny, double xc, double yc, double r0)
{
	char *tag = "manga_im_sim: place";
	double rng;
	int i, j;	// pixel offsets
	int x, y;	// pixel coordinates

	(void)fprintf(stderr, "%s: xc %8.3f yc %8.3f r0 %8.3f\n", tag, xc, yc, r0);

	// offset from the corner to the center of the chip
	x = xc + nx/2;
	y = yc + ny/2;

	// create a bullseye pixel at the center
	//image[y*nx + x] = 255;

	double p = drand48() * 255;	// peak brightness of this fiber
	p = MAX(p, 150);			// we require some minimum quality
	double e = 0.9 * p;
	double c = 12;
	double s1 = 0.7;	// fractional radius of the core
	double s2 = 0.8;	// radius at which the edge rolloff ends

#ifdef DEBUG
	(void)fprintf(stderr, "%s: x %4d y %4d p %f e %f c %f s1 %f s2 %f\n",
			tag, x, y, p, e, c, s1, s2);
#endif

	for (i = x-r0; i <= x+r0; i++) {
		for (j = y-r0; j <= y+r0; j++) {
			double r = sqrt((i-x)*(i-x) + (j-y)*(j-y));
			double f = r/r0;
			int v = image[j*nx+i];
			// we build up the fiber profile in steps
			int dv;
			if (f < s1) {
				// within the fiber, form a peaky plateau
				dv = p + (e - p)/(s1) * f;
			} else if (f < s2) {
				// roll-off at the shoulder of the fiber
				dv = e + (c - e)/(s2 - s1) * (f - s1);
			} else if (f < 1) {
				// the cladding
				dv = c;
			} else {
				dv = 0;
			}
#ifdef DEBUG
			(void)fprintf(stderr, "%s: x %4d y %4d r %8.3f f %8.3f v %3d dv %3d\n",
					tag, x, y, r, f, v, dv);
#endif
			v += dv;
			image[j*nx+i] = RANGE(v, 0, 255);
		}
	}

	// add a blemish to 10% of the fibers
	rng = drand48();
#ifdef DEBUG
	(void)fprintf(stderr, "%s: blemish rng %8.3f\n", tag, rng);
#endif
	if (rng < 0.1) {
		// perturb the center
		x += (drand48() - 0.5) * 0.8 * r0;
		y += (drand48() - 0.5) * 0.8 * r0;
		r0 = r0 * drand48()/2;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: blemish x %4d y %4d r0 %8.3f\n", tag, x, y, r0);
#endif
		e = image[y*nx+x];
		int b = drand48() * e;
		for (i = x-r0; i <= x+r0; i++) {
			for (j = y-r0; j <= y+r0; j++) {
				double r = sqrt((i-x)*(i-x) + (j-y)*(j-y));
				double f = r/r0;
				int v = image[j*nx+i];
				int dv = 0;
				if (f < s1) {
					// we're completely inside the blemish
					dv = b - e;
				} else if (f < s2) {
					// roll-off at the shoulder of the blemish
					dv = b - e + (e - b)/(s2 - s1) * (f - s1);
				} else {
					dv = 0;
				}
				v += dv;
				image[j*nx+i] = RANGE(v, 0, 255);
			}
		}

	}

	return(0);
} 

// a non-negative seed means add a test pattern

int
manga_im_sim(int *image, int nx, int ny, int seed)
{
	char *tag = "manga_im_sim";
	double r0 = 28;			// fiber pcaking radius in pixels, including cladding
	double theta = 8 * (M_PI/180);		// bundle rotation
	int i;
	int nf;
	int rank = 2;
	int x, y;

	(void)fprintf(stderr, "%s: simulate image %dx%d seed %d\n", tag, nx, ny, seed);
	(void)fprintf(stderr, "%s: pixel size %.3fx%.3f microns\n", tag, PX, PY);
	(void)fprintf(stderr, "%s: sensor size %.3fx%.3f microns\n", tag, nx*PX, ny*PY);
	(void)fprintf(stderr, "%s: bundle params rank %d r0 %f pixels theta %f deg\n", tag, rank, r0, theta*(180/M_PI));

	// seed the RNG
	if (seed >= 0) {
		srand48(seed);
	}

	nf = manga_fiber_count(rank);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: rank %d nf %3d\n", tag, rank, nf);
#endif

#ifdef DEBUG
	(void)fprintf(stderr, "%s: add a pedestal\n", tag);
#endif
	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			image[y*nx + x] += x % 8;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: sprinkle in some noise\n", tag);
#endif
	if (seed >= 0) {
		for (y = 0; y < ny; y++) {
			for (x = 0; x < nx; x++) {
				image[y*nx + x] += drand48() * 5;
			}
		}
	}

	/* plot a fiber core at each location */
	for (i = 0; i < nf; i++) {
		FIBER_AB fiber_ab = manga_fiber_ab(i);
		FIBER_XY fiber_xy = manga_fiber_ab2xy(fiber_ab);
#ifdef DEBUG
		(void)fprintf(stderr, "%s: fiber %3d a %8.3f b %8.3f x %8.3f y %8.3f\n", tag, i,
				fiber_ab.a,
				fiber_ab.b,
				fiber_xy.x,
				fiber_xy.y);
#endif

		/* add in some rotation */
		double xx = fiber_xy.x * cos(theta) - fiber_xy.y * sin(theta);
		double yy = fiber_xy.x * sin(theta) + fiber_xy.y * cos(theta);
#ifdef DEBUG
		(void)fprintf(stderr, "%s: fiber %3d  rotate xx %8.3f yy %8.3f\n", tag, i, xx, yy);
#endif

		/* randomly perturb the fiber up to 10% of its radius */
		double dx = (drand48() - 0.5) / 10;
		double dy = (drand48() - 0.5) / 10;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: fiber %3d perturb dx %8.3f dy %8.3f\n", tag, i, dx, dy);
#endif

		/* compute the final position in microns */
		double xc = r0 * (xx + dx);
		double yc = r0 * (yy + dy);
#ifdef DEBUG
		(void)fprintf(stderr, "%s: fiber %3d   final xc %8.3f yc %8.3f\n", tag, i, xc, yc);
#endif
		place(image, nx, ny, xc, yc, r0);
	}

	return(0);
}
