/* file: $RCSfile: manga_im_sort.c,v $
** rcsid: $Id: manga_im_sort.c,v 1.4 2013/04/16 19:07:42 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_sort.c,v 1.4 2013/04/16 19:07:42 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_sort.c,v $ - sorts a vector of pixels
** *******************************************************************
*/

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

static int
comp(const int *v1, const int *v2)
{
	if (*v1 < *v2) {
		return(1);
	} else if (*v1 > *v2) {
		return(-1);
	}
	return(0);
}

int
manga_im_sort(int *xform, int *image, int nx, int ny)
{
	char *tag = "manga_im_sort";

	(void)fprintf(stderr, "%s: sort %dx%d\n", tag, nx, ny);

	(void)memcpy(xform, image, nx*ny*sizeof(int));

	qsort(xform, nx*ny, sizeof(int), (int (*)(const void *, const void *))comp);

	return(0);
}
