/* file: $RCSfile: manga_im_io.c,v $
** rcsid: $Id: manga_im_io.c,v 1.4 2013/05/14 16:52:04 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_io.c,v 1.4 2013/05/14 16:52:04 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_io.c,v $ - provides general read/write support.
** When reading we probe the image size to decide if it's raw or FITS,
** and its shape (nx, ny, bitpix).
** We try to recognize a large variety of sizes and shapes.
** *******************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include "manga.h"

#undef DEBUG

// we read the image bytes into this buffer, and then reconstruct the image data into the caller's buffer
static char imbuf[MAX_IMAGE_SIZE];

/**
 * This next one is static
 * because it's a helper for the manga_im_read2() function,
 * which reads in the data and decides whether to call this one.
 * The image data must already have been read into imbuf.
 */

static int
manga_im_read_fits(int *image, int *pnx, int *pny, int *pdepth, char *imbuf, int n_got)
{
	char *tag = "manga_im_fits_read";
	double b_scale = 1;
	double b_zero = 0;
	int bitpix = 0;				// bits per pixel
	int bpp;					// bytes per pixel
	int done = 0;				// true when header is done
	int end = 0;				// true if END card has been seen
	int ix = 0;					// index into imbuf
	int naxis = 0;				// # of axes
	int naxis1 = 0;				// length of axis 1
	int naxis2 = 0;				// length of axis 2
	int ncards = 0;				// number of header cards
	int rpv;					// raw pixel value
	int rpv_min = 0;			// raw pixel value minimum
	int rpv_max = 0;			// raw pixel value maximum
	int x, y;

	// do a fits sanity check
	if (strncmp(imbuf+ix, "SIMPLE  =", 9) != 0) {
		(void)fprintf(stderr, "%s: bad FITS file; no SIMPLE keyword\n", tag);
		return(1);
	}

	while (!done) {

		ix += 80;
		ncards++;
#ifdef DEBUG
		(void)fprintf(stderr, "%s: ix %d ncards %d\n", tag, ix, ncards);
#endif

		if (strncmp(imbuf+ix, "BITPIX  =", 9) == 0) {
			bitpix = atoi(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: bitpix %d\n", tag, bitpix);
#endif
		} else if (strncmp(imbuf+ix, "NAXIS   =", 9) == 0) {
			naxis = atoi(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: naxis %d\n", tag, naxis);
#endif
		} else if (strncmp(imbuf+ix, "NAXIS1  =", 9) == 0) {
			naxis1 = atoi(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: naxis1 %d\n", tag, naxis1);
#endif
		} else if (strncmp(imbuf+ix, "NAXIS2  =", 9) == 0) {
			naxis2 = atoi(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: naxis2 %d\n", tag, naxis2);
#endif
		} else if (strncmp(imbuf+ix, "BSCALE  =", 9) == 0) {
			b_scale = atof(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: bscale %.15g\n", tag, b_scale);
#endif
		} else if (strncmp(imbuf+ix, "BZERO   =", 9) == 0) {
			b_zero = atof(imbuf+ix+9);
#ifdef DEBUG
			(void)fprintf(stderr, "%s: bzero %.15g\n", tag, b_zero);
#endif
		} else if (strncmp(imbuf+ix, "END", 3) == 0) {
			end++;
#ifdef DEBUG
			(void)fprintf(stderr, "%s: end\n", tag);
#endif
		}

		if (((ncards % 36) == 0) && (end)) {
			done++;
		}
	}

	*pnx = naxis1;
	*pny = naxis2;
	switch (bitpix) {
	case 8:
		*pdepth = 0;
		break;
	case 16:
		*pdepth = 1;
		break;
	case 32:
		*pdepth = 2;
		break;
	default:
		(void)fprintf(stderr, "%s: bitpix error (%d)\n", tag, bitpix);
		return(1);
	}

	bpp = bitpix / 8;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: ix %d bpp %d\n", tag, ix, bpp);
#endif

	for (y = 0; y < naxis2; y++) {
		for (x = 0; x < naxis1; x++) {

			rpv = 0;
			switch (bpp) {
			case 1:
			default:
				// unsigned 8-bit integer
				rpv |= (imbuf[ix++] & 0xff);
				break;
			case 2:
				// signed 16-bit integer, msb first
				rpv |= (imbuf[ix++] & 0xff);
				if (rpv & 0x80) {
					// the msb had a sign bit; sign-extend the current value
					rpv |= ~(0xff);
				}
				rpv <<= 8;
				rpv |= (imbuf[ix++] & 0xff);
				break;
			case 4:
				// signed 32-bit integer, msb first
				rpv |= (imbuf[ix++] & 0xff);
				if (rpv & 0x80) {
					// the msb had a sign bit; sign-extend the current value
					rpv |= ~(0xff);
				}
				rpv <<= 8;
				rpv |= (imbuf[ix++] & 0xff);
				rpv <<= 8;
				rpv |= (imbuf[ix++] & 0xff);
				rpv <<= 8;
				rpv |= (imbuf[ix++] & 0xff);
				break;
			}
			image[y*naxis1+x] = rpv;

			/* track the minmax */
			if (ix == 0) {
				rpv_min = rpv_max = rpv;
			} else if (rpv < rpv_min) {
				rpv_min = rpv;
			} else if (rpv > rpv_max) {
				rpv_max = rpv;
			}
			image[ix] = rpv;
		}
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: rpv min %d max %d range %d\n", tag, rpv_min, rpv_max, (rpv_max - rpv_min));
#endif

	if ((b_scale == 1) && (b_zero != 0)) {
		int spv;						/* scaled pixel value */
		int spv_min = 0;				/* scaled pixel value minimum */
		int spv_max = 0;				/* scaled pixel value maximum */

#ifdef DEBUG
		(void)fprintf(stderr, "%s: apply bzero %.15g\n", tag, b_zero);
#endif
		for (ix = 0; ix < (naxis1 * naxis2); ix++) {
			rpv = image[ix];

			spv = rpv + b_zero;

			/* track the minmax */
			if (ix == 0) {
				spv_min = spv_max = spv;
			} else if (spv < spv_min) {
				spv_min = spv;
			} else if (spv > spv_max) {
				spv_max = spv;
			}

			image[ix] = spv;
		}

#ifdef DEBUG
		(void)fprintf(stderr, "%s: spv min %d max %d range %d\n",
				tag, spv_min, spv_max, (spv_max - spv_min));
#endif
	}

	return(0);
}

int
manga_im_read_new(int *image, int *pnx, int *pny, int *pdepth, FILE *fp)
{
	char *tag = "manga_im_read_new";
	int nx = 0, ny = 0, depth = 0;	// image is 2^depth bytes
	int fits = 0;		// fits or raw image?
	int n_ask, n_got;
	int rcode = 0;		// return code

#ifdef DEBUG
	(void)fprintf(stderr, "%s: read the whole image\n", tag);
#endif
	n_ask = MAX_IMAGE_SIZE;
	n_got = fread(imbuf, 1, n_ask, fp);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: n_ask %d n_got %d\n", tag, n_ask, n_got);
#endif

	switch (n_got) {
	case IM_SIZE_RAW(640,480,1):
		nx = 640; ny = 480; depth = 0; break;
	case IM_SIZE_RAW(640,480,2):
		nx = 640; ny = 480; depth = 1; break;
	case IM_SIZE_RAW(640,480,4):
		nx = 640; ny = 480; depth = 2; break;
	case IM_SIZE_FITS(640,480,1):
	case IM_SIZE_FITS(640,480,2):
	case IM_SIZE_FITS(640,480,4):
	case IM_SIZE_FRAG(640,480,1):
	case IM_SIZE_FRAG(640,480,2):
	case IM_SIZE_FRAG(640,480,4):
		fits++; break;
	case IM_SIZE_RAW(910,525,1):
		nx = 910; ny = 525; depth = 0; break;
	case IM_SIZE_RAW(910,525,2):
		nx = 910; ny = 525; depth = 1; break;
	case IM_SIZE_RAW(910,525,4):
		nx = 910; ny = 525; depth = 2; break;
	case IM_SIZE_FITS(910,525,1):
	case IM_SIZE_FITS(910,525,2):
	case IM_SIZE_FITS(910,525,4):
	case IM_SIZE_FRAG(910,525,1):
	case IM_SIZE_FRAG(910,525,2):
	case IM_SIZE_FRAG(910,525,4):
		fits++; break;
	case IM_SIZE_RAW(1624,1234,1):
		nx = 1624; ny = 1234; depth = 0; break;
	case IM_SIZE_RAW(1624,1234,2):
		nx = 1624; ny = 1234; depth = 1; break;
	case IM_SIZE_RAW(1624,1234,4):
		nx = 1624; ny = 1234; depth = 2; break;
	case IM_SIZE_FITS(1624,1234,1):
	case IM_SIZE_FITS(1624,1234,2):
	case IM_SIZE_FITS(1624,1234,4):
	case IM_SIZE_FRAG(1624,1234,1):
	case IM_SIZE_FRAG(1624,1234,2):
	case IM_SIZE_FRAG(1624,1234,4):
		fits++; break;
	case IM_SIZE_RAW(2048,1536,1):
		nx = 2048; ny = 1536; depth = 0; break;
	case IM_SIZE_RAW(2048,1536,2):
		nx = 2048; ny = 1536; depth = 1; break;
	case IM_SIZE_RAW(2048,1536,4):
		nx = 2048; ny = 1536; depth = 2; break;
	case IM_SIZE_FITS(2048,1536,1):
	case IM_SIZE_FITS(2048,1536,2):
	case IM_SIZE_FITS(2048,1536,4):
	case IM_SIZE_FRAG(2048,1536,1):
	case IM_SIZE_FRAG(2048,1536,2):
	case IM_SIZE_FRAG(2048,1536,4):
		fits++; break;
	default:
		break;
	}

	if (fits) {
#ifdef DEBUG
		(void)fprintf(stderr, "%s: read fits image\n", tag);
#endif
		rcode = manga_im_read_fits(image, &nx, &ny, &depth, imbuf, n_got);
#ifdef DEBUG
		(void)fprintf(stderr, "%s: got fits image %dx%d depth %d\n", tag, nx, ny, depth);
#endif
	} else {
		int ix = 0;			// index into the data buffer
		int rpv;			// raw pixel value
		int x, y;			// pixel coordinates

#ifdef DEBUG
		(void)fprintf(stderr, "%s: read raw image %dx%d depth %d\n", tag, nx, ny, depth);
#endif
		if (nx == 0) {
			(void)fprintf(stderr, "%s: image size (%d) not recognized\n", tag, n_got);
			return(1);
		} else if (n_got != nx*ny*(1<<depth)) {
			(void)fprintf(stderr, "%s: size/shape error (%d!=%dx%dx(1<<%d))\n",
					tag, n_got, nx, ny, depth);
			return(1);
		}

		for (y = 0; y < ny; y++) {
			for (x = 0; x < nx; x++) {
				rpv = 0;
				switch (depth) {
				case 0:
				default:
					rpv |= (imbuf[ix++] & 0xff) << 0;
					break;
				case 1:
					rpv |= (imbuf[ix++] & 0xff) << 8;
					rpv |= (imbuf[ix++] & 0xff) << 0;
					break;
				case 2:
					rpv |= (imbuf[ix++] & 0xff) << 24;
					rpv |= (imbuf[ix++] & 0xff) << 16;
					rpv |= (imbuf[ix++] & 0xff) << 8;
					rpv |= (imbuf[ix++] & 0xff) << 0;
					break;
				}
				image[y*nx+x] = rpv;
			}
		}
	}

	*pnx = nx;
	*pny = ny;
	*pdepth = depth;

	return(rcode);
}

/**
 * Writes a raw file.
 * Just pixel data, not headers or anything.
 * We clip the image data to fit the requested "depth".
 * The number of bytes per pixel is 2**depth:
 * depth 0 = 8-bit unsigned integers
 * depth 1 = 16-bit signed integers
 * depth 2 = 32-bit signed integers
 */

int
manga_im_write_raw(FILE *fp, int *image, int nx, int ny, int depth, char *history)
{
	char *tag = "manga_im_write_raw";
	int x, y;
	int i = 0;
	int n_ask, n_got;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: write image %dx%d depth %d\n", tag, nx, ny, depth);
#endif
#ifdef NOPE
	(void)fprintf(stderr, "%s: -(1<<15)   %d\n", tag, -(1<<15));
	(void)fprintf(stderr, "%s:  (1<<15)-1 %d\n", tag, (1<<15)-1);
	(void)fprintf(stderr, "%s: -(1<<31)   %d\n", tag, -(1<<31));
	(void)fprintf(stderr, "%s:  (1<<31)-1 %d\n", tag, (1<<31)-1);
#endif

	for (y = 0; y < ny; y++) {
		for (x = 0; x < nx; x++) {
			char byte[4];
			int v = image[i++];

			n_ask = 0;
			switch (depth) {
			case 0:
			default:
				v = RANGE(v,0,255);
				byte[n_ask++] = (v >> 0) & 0xff;
				break;
			case 1:
				v = RANGE(v,-(1<<15),(1<<15)-1);
				byte[n_ask++] = (v >>  8) & 0xff;
				byte[n_ask++] = (v >>  0) & 0xff;
				break;
			case 2:
				v = RANGE(v,-(1<<31),(1<<31)-1);
				byte[n_ask++] = (v >> 24) & 0xff;
				byte[n_ask++] = (v >> 16) & 0xff;
				byte[n_ask++] = (v >>  8) & 0xff;
				byte[n_ask++] = (v >>  0) & 0xff;
				break;
			}

			n_got = fwrite((void *)byte, 1, n_ask, fp);
			if (n_got != n_ask) {
				(void)fprintf(stderr, "%s: bad write\n", tag);
				return(-1);
			}
		}
	}

	return(0);
}



int
manga_im_write_fits(FILE *fp, int *image, int nx, int ny, int depth, char *history)
{
	char *tag = "manga_im_write_fits";
	char buf[BUFSIZ];			// fits card data
	char byte[4];				// for pixel bytes
	double mean, sigma;			// image stats
	int b_scale = 1;			// real = (tape * b_scale) + b_zero
	int b_zero = 0;				// real = (tape * b_scale) + b_zero
	int ix;						// image index
	int min, max;				// image extrema
	int nh = 0;					// number of header cards
	int nout = 0;				// number of data bytes written
	int rpv;					// raw pixel value
	int rpv_max;				// raw pixel value maximum
	int rpv_min;				// raw pixel value minimum
	size_t n_ask, n_got;
	unsigned int range;			/* total numerical range */

#ifdef DEBUG
	(void)fprintf(stderr, "%s: image %dx%d depth %d\n", tag, nx, ny, depth);
#endif

	/* get the min and max */
	manga_im_stats(&min, &max, &mean, &sigma, image, nx, ny);
	range = max - min;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: min %d max %d range %u\n", tag, min, max, range);
#endif

	/* enforce the depth */
	if (range < 256) {
		depth = 0;
		b_scale = 1;
		if (min < 0) {
			b_zero = min;
		} else if (max > 255) {
			b_zero = max - 255;
		} else {
			b_zero = 0;
		}
	} else if (range < 65536) {
		depth = 1;
		b_scale = 1;
		if (min < -32768) {
			b_zero = min + 32768;
		} else if (max > 32767) {
			b_zero = max - 32767;
		} else {
			b_zero = 0;
		}
	} else {
		depth = 2;
		b_scale = 1;
		b_zero = 0;
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: depth %d b_scale %d b_zero %d\n", tag, depth, b_scale, b_zero);
#endif

	/********************/
	/* write the header */
	/********************/

	(void)fprintf(fp, "%10.10s%20.20s%-50.50s", "SIMPLE  = ", "T", "");
	nh++;

	switch (depth) {
	case 0:
	default:
		(void)fprintf(fp, "%10.10s%20d%-50.50s", "BITPIX  = ", 8, " /8-BIT UNSIGNED INTEGERS");
		break;
	case 1:
		(void)fprintf(fp, "%10.10s%20d%-50.50s", "BITPIX  = ", 16, " /16-BIT SIGNED INTEGERS");
		break;
	case 2:
		(void)fprintf(fp, "%10.10s%20d%-50.50s", "BITPIX  = ", 32, " /32-BIT SIGNED INTEGERS");
		break;
	}
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS   = ", 2, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS1  = ", nx, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS2  = ", ny, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "BSCALE  = ", b_scale, " /REAL = (TAPE * BSCALE) + BZERO");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "BZERO   = ", b_zero, " /REAL = (TAPE * BSCALE) + BZERO");
	nh++;

	if (depth == 0) {
		(void)fprintf(fp, "%10.10s%-20.20s%-50.50s", "DATATYPE= ", "'INTEGER*1'", "");
	} else if (depth == 1) {
		(void)fprintf(fp, "%10.10s%-20.20s%-50.50s", "DATATYPE= ", "'INTEGER*2'", "");
	} else {
		(void)fprintf(fp, "%10.10s%-20.20s%-50.50s", "DATATYPE= ", "'INTEGER*4'", "");
	}
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "DATAMIN = ", min, " /TRUE IMAGE MINIMUM");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "DATAMAX = ", max, " /TRUE IMAGE MAXIMUM");
	nh++;

	// from the caller
	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", history);
	nh++;

	// our own history
	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", "$Id: manga_im_io.c,v 1.4 2013/05/14 16:52:04 jwp Exp $");
	nh++;

	(void)fprintf(fp, "%-80.80s", "END");
	nh++;

	/* round out the header record */
#ifdef DEBUG
	(void)fprintf(stderr, "%s: %d cards out, rounding...\n", tag, nh);
#endif
	memset(buf, ' ', 80);
	for (ix = nh; ix%36 != 0; ix++) {
		n_ask = 80;
		n_got = fwrite((void *)buf, 1, 80, fp);
		if (n_ask != n_got) {
			(void)fprintf(stderr, "%s: bad write on card %d\n", tag, ix);
		}
	}

	/* write out the data */
	rpv_min = rpv_max = image[0] - b_zero;
	for (ix = 0; ix < (nx*ny); ix++) {

		rpv = image[ix] - b_zero;

		/* track the minmax */
		if (rpv < rpv_min) {
			rpv_min = rpv;
		} else if (rpv > rpv_max) {
			rpv_max = rpv;
		}

		n_ask = 0;
		switch (depth) {
		case 0:
		default:
			byte[n_ask++] = (rpv >> 0) & 0xff;
			break;
		case 1:
			byte[n_ask++] = (rpv >> 8) & 0xff;
			byte[n_ask++] = (rpv >> 0) & 0xff;
			break;
		case 2:
			byte[n_ask++] = (rpv >> 24) & 0xff;
			byte[n_ask++] = (rpv >> 16) & 0xff;
			byte[n_ask++] = (rpv >>  8) & 0xff;
			byte[n_ask++] = (rpv >>  0) & 0xff;
			break;
		}

		n_got = fwrite((void *)byte, 1, n_ask, fp);
		if (n_ask != n_got) {
			(void)fprintf(stderr, "%s: bad write on pix %d\n", tag, ix);
			return(1);
		}
		nout += n_got;
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: rpv min %d max %d range %d\n", tag, rpv_min, rpv_max, (rpv_max - rpv_min));
#endif

	// fill out the file to the next multiple of 2880
#ifdef DEBUG
	(void)fprintf(stderr, "%s: %d bytes out, rounding...\n", tag, nout);
#endif
	byte[0] = 0;
	while (nout%2880 != 0) {
		n_ask = 1;
		n_got = fwrite((void *)byte, 1, n_ask, fp);
		nout += n_got;
	}

	if (fflush(fp)) {
		(void)fprintf(stderr, "%s: can't flush output stream\n", tag);
		return(1);
	}

	return(0);
}
