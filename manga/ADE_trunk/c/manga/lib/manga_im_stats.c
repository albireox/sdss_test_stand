/* file: $RCSfile: manga_im_stats.c,v $
** rcsid: $Id: manga_im_stats.c,v 1.5 2013/04/16 19:12:39 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_stats.c,v 1.5 2013/04/16 19:12:39 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_stats.c,v $ - compute statistics on a vector of pixels
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_stats(int *min, int *max, double *mean, double *sigma, int *image, int nx, int ny)
{
	char *tag = "manga_im_stats";
	double sum_v = 0, sum_vv = 0;
	double var;
	int i;
	int npix;

	(void)fprintf(stderr, "%s: do stats %dx%d\n", tag, nx, ny);

	npix = nx*ny;
	*min = *max = image[0];
	for (i = 0; i < npix; i++) {
		int v = image[i];
		if (v < *min) {
			*min = v;
		} else if (v > *max) {
			*max = v;
		}
		sum_v += (double)v;
		sum_vv += (double)v * v;
	}

	*mean = sum_v / npix;
	var = (sum_vv - (sum_v*sum_v)/npix)/(npix-1);
	*sigma = sqrt(var);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: min %d max %d mean %f var %f sigma %f\n", tag, *min, *max, *mean, var, *sigma);
#endif

	return(0);
}
