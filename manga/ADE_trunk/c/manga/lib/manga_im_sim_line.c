/* file: $RCSfile: manga_im_sim_line.c,v $
** rcsid: $Id: manga_im_sim_line.c,v 1.7 2013/06/13 15:40:26 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_sim_line.c,v 1.7 2013/06/13 15:40:26 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_sim_line.c,v $ - simulate a manga fiber bundle
** *******************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "manga.h"

#undef DEBUG

int
manga_im_sim_line(int *image, int nx, int ny, int seed)
{
	char *tag = "manga_im_sim_line";
	double r, theta;
	int x, y;
	
	r = nx / 2;
	theta = 45 * (M_PI/180);

	(void)fprintf(stderr, "%s: simulate image %dx%d seed %d\n", tag, nx, ny, seed);
	(void)fprintf(stderr, "%s: pixel size %.3fx%.3f mm\n", tag, PX, PY);
	(void)fprintf(stderr, "%s: sensor size %.3fx%.3f mm\n", tag, nx*PX, ny*PY);
	(void)fprintf(stderr, "%s: bundle params r %f mm theta %f deg\n", tag, r, theta*(180/M_PI));
	
	(void)memset(image, 0, nx*ny*sizeof(int));

	for (x = 0; x < nx; x++) {
		if (sin(theta) == 0) {
			continue;
		}
		y = (r - x*cos(theta)) / sin(theta);
		if (y < 0) {
			continue;
		} else if (y >= ny) {
			continue;
		} else {
			image[y*nx+x] += 16;
		}
	}

	return(0);
}
