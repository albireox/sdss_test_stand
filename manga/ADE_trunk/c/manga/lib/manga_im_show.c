/* file: $RCSfile: manga_im_show.c,v $
** rcsid: $Id: manga_im_show.c,v 1.3 2013/03/13 02:59:24 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_im_show.c,v 1.3 2013/03/13 02:59:24 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: manga_im_show.c,v $ - show image pixels
** *******************************************************************
*/

#include <string.h>
#include <math.h>
#include "manga.h"

#undef DEBUG

int
manga_im_show(FILE *fp, int *image, int nx, int ny)
{
	char *tag = "manga_im_show";
	int i;

	(void)fprintf(stderr, "%s: show image %dx%d\n", tag, nx, ny);

	for (i = 0; i < 256; i++) {
		if ((i%8) == 0) {
			(void)fprintf(stderr, "%s: image[%3d]:", tag, i);
		}
		(void)fprintf(stderr, " %8d", image[i]);
		if ((i%8) == 7) {
			(void)fprintf(stderr, "\n");
		}
	}

	return(0);
}
