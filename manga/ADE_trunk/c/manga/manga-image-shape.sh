#! /bin/sh
# file: $RCSfile: manga-image-shape.sh,v $
# rcsid: $Id: manga-image-shape.sh,v 1.2 2013/01/10 03:11:35 jwp Exp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# given a file size,
# we guess the file shape (NX, NY, bitpix) by looking at the file size in bytes.
# add more tests in the "if" statement below to accomodate more shapes.
# ********************************************************************

# set defaults
NX=1624
NY=1234
depth=0
bitpix=8

if [ $# -lt 1 ]
then
	echo "Usage: $(basename $0) filename.raw"
	exit 1
fi

for f in $*
do

	# does the file exist

	if [ ! -f $f ]
	then
		echo "$(basename $0): file $f not found"
		exit 1
	fi

	# detect the image shape and depth

	size=$(ls -l $f | awk '{print $5}')
	# echo $(basename $0): size $size
	if [ $size -eq 2004016 ]
	then
		NX=1624
		NY=1234
		depth=0
		bitpix=8
	elif [ $size -eq 8016064 ]
	then
		NX=1624
		NY=1234
		depth=2
		bitpix=32
	elif [ $size -eq 3145728 ]
	then
		NX=2048
		NY=1536
		depth=0
		bitpix=8
	elif [ $size -eq 12582912 ]
	then
		NX=2048
		NY=1536
		depth=2
		bitpix=32
	else
		echo $(basename $0): bad file size: $size
		exit 1
	fi

	echo $f NX $NX NY $NY depth $depth bitpix $bitpix

done

exit 0
