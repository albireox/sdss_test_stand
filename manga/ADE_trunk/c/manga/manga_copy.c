/* file: $RCSfile: manga_copy.c,v $
** rcsid: $Id: manga_copy.c,v 1.5 2013/03/13 03:00:07 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_copy.c,v 1.5 2013/03/13 03:00:07 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_copy.c,v $ - copies an image using our I/O routines.
** By default we copy stdin to stdout.
** If we detect command-line file arguments,
** we switch to file io.
** We accept either raw or FITS format on input; we auto-detect.
** We write fits files.
** We try to be clever with file names:
** foo.raw will end up as foo.fits,
** but foo.fits will and up as foo.copy.fits.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "manga.h"

// storage for the largest image we can recognize
static int image[MAX_IMAGE_SIZE];

static int verbose = 0;
static int fits = 1;

static void
usage(FILE *fp, char *tag)
{
	(void)fprintf(fp, "Usage: %s [-help] [-verbose] [file ...]\n", tag);
	return;
}

static int
do_copy(char *tag, FILE *fp2, FILE *fp1)
{
	int rcode;
	int nx, ny, depth;

	if (verbose) {
		(void)fprintf(stderr, "%s: zero image %d bytes\n", tag, MAX_IMAGE_SIZE);
	}
	(void)memset(image, 0, MAX_IMAGE_SIZE);

	if (verbose) {
		(void)fprintf(stderr, "%s: read image\n", tag);
	}
	rcode = manga_im_read_new(image, &nx, &ny, &depth, fp1);
	if (rcode != 0) {
		return(-1);
	}
	if (verbose) {
		(void)fprintf(stderr, "%s: found image nx %d ny %d depth %d\n", tag, nx, ny, depth);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: write image nx %d ny %d depth %d\n", tag, nx, ny, depth);
	}
	if (fits) {
		rcode = manga_im_write_fits(fp2, image, nx, ny, depth, rcsid);
	} else {
		rcode = manga_im_write_raw(fp2, image, nx, ny, depth, rcsid);
	}
	if (rcode != 0) {
		return(-1);
	}

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen;
	int rcode;
	char *filenames[BUFSIZ];
	int nfiles = 0;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strncmp(argptr, "help", arglen) == 0) {
			usage(stderr, tag);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else {
			// must be a filename
			filenames[nfiles++] = argptr;
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: nfiles %d\n", tag, nfiles);
	}

	if (nfiles > 0) {
		// we have filenames
		int f;
		for (f = 0; f < nfiles; f++) {
			char *f1, *f2, *p, buf[BUFSIZ];
			// open the source file
			f1 = filenames[f];
			FILE *fp1 = fopen(f1, "r");
			if (fp1 == NULL) {
				perror(f1);
				continue;
			}
			// build the destination filename
			f2 = strcpy(buf, f1);
			//p = strrchr(f2, '/');
			//if (p != NULL) {
			//	f2 = p+1;
			//}
			p = strrchr(f2, '.');
			if (p != NULL) {
				// is our desired extension already in use?
				if (strcmp(p, ".fits") == 0) {
					*p = 0;
					(void)strcat(f2, ".copy");
				} else {
					*p = 0;
				}
			}
			(void)strcat(f2, ".fits");
			// open the destination file
			FILE *fp2 = fopen(f2, "w");
			if (fp2 == NULL) {
				perror(f2);
				continue;
			}
			// perform the operation
			(void)fprintf(stderr, "%s: %s <-- %s\n", tag, f2, f1);
			rcode = do_copy(tag, fp2, fp1);
			(void)fclose(fp1);
			(void)fclose(fp2);
		}
	} else {
		// use stdin/stdout
		(void)fprintf(stderr, "%s: stdout <-- stdin\n", tag);
		rcode = do_copy(tag, stdout, stdin);
	}

	return(0);
}
