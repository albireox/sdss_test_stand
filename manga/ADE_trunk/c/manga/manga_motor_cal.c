/* file: $RCSfile: manga_motor_cal.c,v $
** rcsid: $Id: manga_motor_cal.c,v 1.4 2013/06/13 15:47:24 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_motor_cal.c,v 1.4 2013/06/13 15:47:24 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_motor_cal.c,v $ - use 2 image positions to calibrate the motor stages
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "manga.h"

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	double mx1 = 0; double my1 = 0;
	double mx2 = 2; double my2 = 1;
	double sx1 = 0; double sy1 = 0;
	double sx2 = 1; double sy2 = 2;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", tag);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\t-mx1 (motor x, 1st position) (%8.3f)\n", mx1);
			(void)fprintf(stderr, "\t-my1 (motor y, 1st position) (%8.3f)\n", my1);
			(void)fprintf(stderr, "\t-mx2 (motor x, 2nd position) (%8.3f)\n", mx2);
			(void)fprintf(stderr, "\t-my2 (motor y, 2nd position) (%8.3f)\n", my2);
			(void)fprintf(stderr, "\t-sx1 (image x, 1st position) (%8.3f)\n", sx1);
			(void)fprintf(stderr, "\t-sy1 (image y, 1st position) (%8.3f)\n", sy1);
			(void)fprintf(stderr, "\t-sx2 (image x, 2st position) (%8.3f)\n", sx2);
			(void)fprintf(stderr, "\t-sy2 (image y, 2st position) (%8.3f)\n", sy2);
			(void)fprintf(stderr, "\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "mx1", arglen) == 0) {
			mx1 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "my1", arglen) == 0) {
			my1 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "mx2", arglen) == 0) {
			mx2 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "my2", arglen) == 0) {
			my2 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "sx1", arglen) == 0) {
			sx1 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "sy1", arglen) == 0) {
			sy1 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "sx2", arglen) == 0) {
			sx2 = atof(argv[++argnum]);
		} else if (strncmp(argptr, "sy2", arglen) == 0) {
			sy2 = atof(argv[++argnum]);

		} else {
			(void)fprintf(stderr, "%s: bad arg(%s)\n", tag, argptr);
			return(-1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: mx1 %8.3f my1 %8.3f\n", tag, mx1, my1);
		(void)fprintf(stderr, "%s: mx2 %8.3f my2 %8.3f\n", tag, mx2, my2);
		(void)fprintf(stderr, "%s: sx1 %8.3f sy1 %8.3f\n", tag, sx1, sy1);
		(void)fprintf(stderr, "%s: sx2 %8.3f sy2 %8.3f\n", tag, sx2, sy2);
	}

	// derive the motor vectors
	double mx = (mx2 - mx1);
	double my = (my2 - my1);
	double mlen = sqrt(mx*mx + my*my);
	double mx_hat = mx / mlen;
	double my_hat = my / mlen;
	if (verbose) {
		(void)fprintf(stderr, "%s: mx %8.3f my %8.3f\n", tag, mx, my);
		(void)fprintf(stderr, "%s: mlen %8.3f\n", tag, mlen);
		(void)fprintf(stderr, "%s: mx_hat %8.3f my_hat %8.3f\n", tag, mx_hat, my_hat);
	}

	// derive the sensor vectors
	double sx = (sx2 - sx1);
	double sy = (sy2 - sy1);
	double slen = sqrt(sx*sx + sy*sy);
	double sx_hat = sx / slen;
	double sy_hat = sy / slen;
	if (verbose) {
		(void)fprintf(stderr, "%s: sx %8.3f sy %8.3f\n", tag, sx, sy);
		(void)fprintf(stderr, "%s: slen %8.3f\n", tag, slen);
		(void)fprintf(stderr, "%s: sx_hat %8.3f sy_hat %8.3f\n", tag, sx_hat, sy_hat);
	}

	// S-to-M scale
	double k = mlen / slen;
	if (verbose) {
		(void)fprintf(stderr, "%s: k %12.8f mm/pix 1/k %12.8f pix/mm\n", tag, k, 1/k);
	}

	// dot product gives the angle between M and S
	double cos_alpha = (mx_hat*sx_hat + my_hat*sy_hat);
	double alpha = acos(cos_alpha);
	if (verbose) {
		(void)fprintf(stderr, "%s: cos(alpha) %12.8f\n", tag, cos_alpha);
		(void)fprintf(stderr, "%s: alpha %12.8f deg\n", tag, alpha*(180/M_PI));
	}

	// we can form J from either of the two measurements
	double Jx1 = mx1 - k * (cos(alpha)*sx1 - sin(alpha)*sy1);
	double Jy1 = my1 - k * (sin(alpha)*sx1 + cos(alpha)*sy1);
	if (verbose) {
		(void)fprintf(stderr, "%s: Jx1 %8.3f Jy1 %8.3f\n", tag, Jx1, Jy1);
	}

	double Jx2 = mx2 - k * (cos(alpha)*sx2 - sin(alpha)*sy2);
	double Jy2 = my2 - k * (sin(alpha)*sx2 + cos(alpha)*sy2);
	if (verbose) {
		(void)fprintf(stderr, "%s: Jx2 %8.3f Jy2 %8.3f\n", tag, Jx2, Jy2);
	}

	double Jx = (Jx1 + Jx2) / 2;
	double Jy = (Jy1 + Jy2) / 2;
	if (verbose) {
		(void)fprintf(stderr, "%s: Jx %8.3f Jy %8.3f\n", tag, Jx, Jy);
	}

	// re-form m from measurement 1
	mx1 = k * (cos(alpha) * sx1 - sin(alpha)*sy1) + Jx;
	my1 = k * (sin(alpha) * sx1 + cos(alpha)*sy1) + Jy;
	if (verbose) {
		(void)fprintf(stderr, "%s: derived mx1 %8.3f my1 %8.3f\n", tag, mx1, my1);
	}

	mx2 = k * (cos(alpha) * sx2 - sin(alpha)*sy2) + Jx;
	my2 = k * (sin(alpha) * sx2 + cos(alpha)*sy2) + Jy;
	if (verbose) {
		(void)fprintf(stderr, "%s: derived mx2 %8.3f my2 %8.3f\n", tag, mx2, my2);
	}

	(void)fprintf(stdout, "%s: solution: k %12.8f mm/pix alpha %12.8f deg Jx %8.3f mm Jy %8.3f mm\n",
		tag, k, alpha*(180/M_PI), Jx, Jy);

	return(0);
}
