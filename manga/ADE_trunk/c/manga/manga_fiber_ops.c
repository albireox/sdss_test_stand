/* file: $RCSfile: manga_fiber_ops.c,v $
** rcsid: $Id: manga_fiber_ops.c,v 1.5 2013/03/13 03:00:07 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_fiber_ops.c,v 1.5 2013/03/13 03:00:07 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_fiber_ops.c,v $ - perform various manga-like things
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "manga.h"

// the histogram of fiber separations
#define NH	(2048)
static double shist[NH];	// the separation values
static int nhist[NH];		// counts per separation

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int rcode;	// return code
	int rank = 6;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", tag);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-rank n (%d)\n", rank);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "rank", arglen) == 0) {
			rank = atoi(argv[++argnum]);
			rank = RANGE(rank,0,MAX_RANK);

		} else {
			(void)fprintf(stderr, "%s: bad arg(%s)\n", tag, argptr);
			return(-1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: rank %d\n", tag, rank);
	}

	// show bundle sizes
	{
		int rank;
		for (rank = 0; rank <= 12; rank++) {
			(void)fprintf(stdout, "%s: fiber bundle rank %d: %3d\n", tag, rank, manga_fiber_count(rank));
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: look at the separation spectrum\n", tag);
	}
	rcode = manga_fiber_spectrum(shist, nhist, NH, rank);
	if (rcode != 0) {
		return(-1);
	}
	int i;
	(void)fprintf(stdout, "%s: shist: %4s %8s %4s\n", tag, "i", "s", "n");
	for (i = 0; i < NH; i++) {
		(void)fprintf(stdout, "%s: shist: %4d %8.3f %4d\n", tag, i, shist[i], nhist[i]);
	}

	return(0);
}
