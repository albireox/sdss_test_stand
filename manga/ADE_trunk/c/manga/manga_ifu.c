/* file: $RCSfile: manga_ifu.c,v $
** rcsid: $Id: manga_ifu.c,v 1.10 2013/08/16 19:49:23 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_ifu.c,v 1.10 2013/08/16 19:49:23 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_ifu.c,v $ - finds an ifu in a list of detected fibers.
** By default we process stdin to stdout.
** If we detect command-line file arguments,
** we switch to file io.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "manga.h"

#define DEBUG

static int verbose = 0;

static void
usage(FILE *fp, char *tag)
{
	(void)fprintf(fp, "Usage: %s [-help] [-verbose] [file ...]\n", tag);
	return;
}

static int
do_ifu(char *tag, FILE *fp2, FILE *fp1)
{
	AOP aop;
	FIBER fiberbuf[MAX_FIBERS];
	FIBER_XY im_list[MAX_FIBERS];
	FIBER_XY xy_list[MAX_FIBERS];
	int ix;
	int nfibers = 0;
	char buf[BUFSIZ];

	if (verbose) {
		(void)fprintf(stderr, "%s: read fiber list\n", tag);
	}

	(void)fgets(buf, BUFSIZ, fp1);	// skip the header row

	while (fgets(buf, BUFSIZ, fp1) != NULL) {
		char *p = index(buf, '\n');
		if (p != NULL) {
			*p = 0;
		}
		(void)fprintf(stderr, "%s: (%s)\n", tag, buf);

		int n = sscanf(buf, "%*s %*d %d %lf %lf %d %d",
				&(fiberbuf[nfibers].f),
				&(fiberbuf[nfibers].xc),
				&(fiberbuf[nfibers].yc),
				&(fiberbuf[nfibers].peak),
				&(fiberbuf[nfibers].flux));

		if (n != 5) {
			(void)fprintf(stderr, "%s: bad line (%s): expected 5, got %d\n", tag, buf, n);
			exit(1);
		}
		nfibers++;
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: fiberbuf: %3s %3s %8s %8s %8s %8s\n", tag, "ix", "f", "xc", "yc", "peak", "flux");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: fiberbuf: %3d %3d %8.3f %8.3f %8d %8d\n",
			tag, ix,
			fiberbuf[ix].f,
			fiberbuf[ix].xc,
			fiberbuf[ix].yc,
			fiberbuf[ix].peak,
			fiberbuf[ix].flux);
	}
#endif

	/**
	 * Now that we've read the data as supplied by the spike finder,
	 * we drop all the flux-related info and just deal with delta functions.
	 */

	for (ix = 0; ix < nfibers; ix++) {
		im_list[ix].f = fiberbuf[ix].f;
		im_list[ix].x = fiberbuf[ix].xc;
		im_list[ix].y = fiberbuf[ix].yc;
	}

#ifdef DEBUG
	(void)fprintf(stderr, "%s: im_list: %3s %3s %8s %8s\n", tag, "ix", "f", "im.x", "im.y");
	for (ix = 0; ix < nfibers; ix++) {
		(void)fprintf(stderr, "%s: im_list: %3d %3d %8.3f %8.3f\n",
			tag, ix,
			im_list[ix].f,
			im_list[ix].x,
			im_list[ix].y);
	}
#endif

	nfibers = manga_im_ifu(&aop, xy_list, im_list, nfibers);

	(void)fprintf(fp2, "%s: solution: aop:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(fp2, "%s: solution: aop: theta: %12.6f deg\n", tag, aop.theta*(180/M_PI));
	(void)fprintf(fp2, "%s: solution: aop:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(fp2, "%s: solution: aop:    sy: %12.6f\n", tag, aop.sy);

	// show the residuals
	(void)fprintf(fp2, "%s: residuals: %3s %8s %8s %8s %8s %8s %8s %8s %8s\n",
			tag, "ix", "model.f", "model.x", "model.y", "image.x", "image.y", "dx", "dy", "dr");
	for (ix = 0; ix < nfibers; ix++) {
		// start with the model coordinates
		double x = xy_list[ix].x;
		double y = xy_list[ix].y;
		// apply the model to move into pixel space
		double tmpx = aop.c * (cos(aop.theta) * x - sin(aop.theta) * y) + aop.sx;
		double tmpy = aop.c * (sin(aop.theta) * x + cos(aop.theta) * y) + aop.sy;
		x = tmpx;
		y = tmpy;
		double dx = im_list[ix].x - x;
		double dy = im_list[ix].y - y;
		double dr = sqrt(dx*dx + dy*dy);
		(void)fprintf(fp2, "%s: residuals: %3d %8d %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f\n",
				tag, ix,
				xy_list[ix].f,
				x, y,
				im_list[ix].x, im_list[ix].y,
				dx, dy, dr);
	}


	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen;
	int rcode;
	char *filenames[BUFSIZ];
	int nfiles = 0;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strncmp(argptr, "help", arglen) == 0) {
			usage(stderr, tag);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else {
			// must be a filename
			filenames[nfiles++] = argptr;
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: nfiles %d\n", tag, nfiles);
	}

	if (nfiles > 0) {
		// we have filenames
		int f;
		for (f = 0; f < nfiles; f++) {
			char *f1, *f2, *p, buf[BUFSIZ];
			// open the source file
			f1 = filenames[f];
			FILE *fp1 = fopen(f1, "r");
			if (fp1 == NULL) {
				perror(f1);
				continue;
			}
			// build the destination filename
			f2 = strcpy(buf, f1);
//			p = strrchr(f2, '/');
//			if (p != NULL) {
//				f2 = p+1;
//			}
			p = strrchr(f2, '.');
			if (p != NULL) {
				*p = 0;
			}
			(void)strcat(f2, ".ifu.out");
			// open the destination file
			FILE *fp2 = fopen(f2, "w");
			if (fp2 == NULL) {
				perror(f2);
				continue;
			}
			// perform the operation
			(void)fprintf(stderr, "%s: %s <-- %s\n", tag, f2, f1);
			rcode = do_ifu(tag, fp2, fp1);
			(void)fclose(fp1);
			(void)fclose(fp2);
		}
	} else {
		// use stdin/stdout
		(void)fprintf(stderr, "%s: stdout <-- stdin\n", tag);
		rcode = do_ifu(tag, stdout, stdin);
	}

	return(0);
}
