/* file: $RCSfile: manga_stats.c,v $
** rcsid: $Id: manga_stats.c,v 1.7 2013/04/17 23:01:38 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_stats.c,v 1.7 2013/04/17 23:01:38 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_stats.c,v $ - do stats on an image.
** By default we process stdin to stdout.
** If we detect command-line file arguments,
** we switch to file io.
** We accept either raw or FITS format on input; we auto-detect.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "manga.h"

// storage for the largest image we can recognize
static int image[MAX_IMAGE_SIZE];

static int verbose = 0;

static void
usage(FILE *fp, char *tag)
{
	(void)fprintf(fp, "Usage: %s [-help] [-verbose] [file ...]\n", tag);
	return;
}

static int
do_stats(char *tag, FILE *fp2, FILE *fp1)
{
	int rcode;
	int nx, ny, depth;
	int min, max;
	double mean, sigma;

	if (verbose) {
		(void)fprintf(stderr, "%s: zero image %d bytes\n", tag, MAX_IMAGE_SIZE);
	}
	(void)memset(image, 0, MAX_IMAGE_SIZE);

	if (verbose) {
		(void)fprintf(stderr, "%s: read image\n", tag);
	}
	rcode = manga_im_read_new(image, &nx, &ny, &depth, fp1);
	if (rcode != 0) {
		return(-1);
	}
	if (verbose) {
		(void)fprintf(stderr, "%s: found image nx %d ny %d depth %d\n", tag, nx, ny, depth);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: perform stats nx %d ny %d depth %d\n", tag, nx, ny, depth);
	}
	rcode = manga_im_stats(&min, &max, &mean, &sigma, image, nx, ny);
	if (rcode != 0) {
		return(-1);
	}

	(void)fprintf(fp2, "%s: min %d max %d mean %f sigma %f\n", tag, min, max, mean, sigma);

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen;
	int rcode;
	char *filenames[BUFSIZ];
	int nfiles = 0;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strncmp(argptr, "help", arglen) == 0) {
			usage(stderr, tag);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else {
			// must be a filename
			filenames[nfiles++] = argptr;
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: nfiles %d\n", tag, nfiles);
	}

	if (nfiles > 0) {
		// we have filenames
		int f;
		for (f = 0; f < nfiles; f++) {
			char *f1, *f2, *p, buf[BUFSIZ];
			// open the source file
			f1 = filenames[f];
			FILE *fp1 = fopen(f1, "r");
			if (fp1 == NULL) {
				perror(f1);
				continue;
			}
			// build the destination filename
			f2 = strcpy(buf, f1);
			p = strrchr(f2, '/');
			if (p != NULL) {
				f2 = p+1;
			}
			p = strrchr(f2, '.');
			if (p != NULL) {
				//*p = 0;
			}
			(void)strcat(f2, ".stats.out");
			// open the destination file
			FILE *fp2 = fopen(f2, "w");
			if (fp2 == NULL) {
				perror(f2);
				continue;
			}
			// perform the operation
			(void)fprintf(stderr, "%s: %s <-- %s\n", tag, f2, f1);
			rcode = do_stats(tag, fp2, fp1);
			(void)fclose(fp1);
			(void)fclose(fp2);
		}
	} else {
		// use stdin/stdout
		(void)fprintf(stderr, "%s: stdout <-- stdin\n", tag);
		rcode = do_stats(tag, stdout, stdin);
	}

	return(0);
}
