#! /bin/sh
# file: $RCSfile: manga-raw2ds9.sh,v $
# rcsid: $Id: manga-raw2ds9.sh,v 1.2 2013/01/10 03:09:52 jwp Exp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# show manta .raw files in ds9
# ********************************************************************

# build a spaces-proof path to the current files
echo PWD $PWD
p=$(echo $PWD | sed -e 's/ /\\ /g')
echo p $p

xpaget ds9 frame
xpaget ds9 scale
xpaget ds9 zoom
xpaget ds9 tile
xpaget ds9 tile mode
xpaget ds9 tile grid mode
xpaget ds9 tile grid layout

xpaset -p ds9 frame delete all
xpaset -p ds9 orient none
xpaset -p ds9 tile yes
#xpaset -p ds9 scale linear
#xpaset -p ds9 zoom to fit

xpaset -p ds9 preserve scale yes
xpaset -p ds9 preserve pan no
xpaset -p ds9 preserve regions no

for f in $*
do
	# does the file exist

	if [ ! -f $f ]
	then
		echo "$(basename $0): file $f not found"
		continue;
	fi

	# detect the image shape and depth
	# the next output format is:
	#	foo.raw NX 1624 NY 1234 depth 2 bitpix 32
	NX=$(manga-image-shape $f | awk '{print $3}')
	NY=$(manga-image-shape $f | awk '{print $5}')
	bitpix=$(manga-image-shape $f | awk '{print $9}')

	echo xpaset -p ds9 file new array $p/$f[xdim=$NX,ydim=$NY,bitpix=$bitpix,arch=littlendian]
	xpaset -p ds9 file new array $p/$f[xdim=$NX,ydim=$NY,bitpix=$bitpix,arch=littlendian]
done
