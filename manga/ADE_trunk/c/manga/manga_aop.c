/* file: $RCSfile: manga_aop.c,v $
** rcsid: $Id: manga_aop.c,v 1.5 2013/06/13 15:45:12 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: manga_aop.c,v 1.5 2013/06/13 15:45:12 jwp Exp $";
/*
** *******************************************************************
** $RCSfile: manga_aop.c,v $ - perform the aop thing.
** By default we process stdin to stdout.
** If we detect command-line file arguments,
** we switch to file io.
** We accept either raw or FITS format on input; we auto-detect.
** We write text files files.
**
** the input lines are of the form:

tag1 tag1 index x1 y1 x2 y2

** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "manga.h"

static void
usage(FILE *fp, char *tag)
{
	(void)fprintf(fp, "Usage: %s [-help] [-verbose] [file ...]\n", tag);
	return;
}

static int
do_aop(char *tag, FILE *fp2, FILE *fp1)
{
	FIBER_XY xy_list[MAX_FIBERS];
	FIBER_XY im_list[MAX_FIBERS];
	int nf = 0;
	AOP aop;
	char buf[BUFSIZ];

	(void)fgets(buf, BUFSIZ, fp1);	// skip the header row

	while (fgets(buf, BUFSIZ, fp1) != NULL) {
		char *p = index(buf, '\n');
		if (p != NULL) {
			*p = 0;
		}
		(void)fprintf(stderr, "%s: (%s)\n", tag, buf);

		int n = sscanf(buf, "%*s %*s %*d %lf %lf %lf %lf",
				&(xy_list[nf].x),
				&(xy_list[nf].y),
				&(im_list[nf].x),
				&(im_list[nf].y));

		if (n != 4) {
			(void)fprintf(stderr, "%s: bad line (%s): expected 4, got %d\n", tag, buf, n);
			exit(1);
		}
		xy_list[nf].f = nf;
		im_list[nf].f = nf;
		nf++;
	}
	int ix;
	(void)fprintf(stderr, "%s: data: %3s %12s %12s %12s %12s\n", tag, "ix", "x1", "y1", "x2", "y2");
	for (ix = 0; ix < nf; ix++) {
		(void)fprintf(stderr, "%s: data: %3d %12.6f %12.6f %12.6f %12.6f\n",
				tag, ix, xy_list[ix].x, xy_list[ix].y, im_list[ix].x, im_list[ix].y);
	}

	// compute the fit
	aop = manga_im_aop(xy_list, im_list, nf);

	(void)fprintf(fp2, "%s: solution: aop:     c: %12.6f\n", tag, aop.c);
	(void)fprintf(fp2, "%s: solution: aop: theta: %12.6f deg\n", tag, aop.theta*(180/M_PI));
	(void)fprintf(fp2, "%s: solution: aop:    sx: %12.6f\n", tag, aop.sx);
	(void)fprintf(fp2, "%s: solution: aop:    sy: %12.6f\n", tag, aop.sy);

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int rcode;
	char *filenames[BUFSIZ];
	int nfiles = 0;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			usage(stderr, tag);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else {
			// must be a filename
			filenames[nfiles++] = argptr;
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: nfiles %d\n", tag, nfiles);
	}

	if (nfiles > 0) {
		// we have filenames
		int f;
		for (f = 0; f < nfiles; f++) {
			char *f1, *f2, *p, buf[BUFSIZ];
			// open the source file
			f1 = filenames[f];
			FILE *fp1 = fopen(f1, "r");
			if (fp1 == NULL) {
				perror(f1);
				continue;
			}
			// build the destination filename
			f2 = strcpy(buf, f1);
			//p = strrchr(f2, '/');
			//if (p != NULL) {
			//	f2 = p+1;
			//}
			p = strrchr(f2, '.');
			if (p != NULL) {
				*p = 0;
			}
			(void)strcat(f2, ".aop.out");
			// open the destination file
			FILE *fp2 = fopen(f2, "w");
			if (fp2 == NULL) {
				perror(f2);
				continue;
			}
			// perform the operation
			(void)fprintf(stderr, "%s: %s <-- %s\n", tag, f2, f1);
			rcode = do_aop(tag, fp2, fp1);
			(void)fclose(fp1);
			(void)fclose(fp2);
		}
	} else {
		// use stdin/stdout
		(void)fprintf(stderr, "%s: stdout <-- stdin\n", tag);
		rcode = do_aop(tag, stdout, stdin);
	}

	return(0);
}
