/* file: $RCSfile: apt_move_home.c,v $
** rcsid: $Id: apt_move_home.c,v 1.2 2013/08/01 20:45:00 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_move_home.c,v 1.2 2013/08/01 20:45:00 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: apt_move_home.c,v $ - Thorlabs APT motion control program
** Execute a homing sequence.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "apt.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0xfaf0;
static char *description = "APT DC Motor Controller";
static char *serialnumber = NULL;
static int baud = 115200;

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	char *axis = "n/a";
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int block = 0;		// block on the finish
	int rcode;
	int teststand = TS2;
	int timeout = 60;	// time for reply, in seconds

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description <string> (%s)\n", description);
			(void)fprintf(stderr, "\t-serialnumber <string> (no default)\n");
			(void)fprintf(stderr, "\t-sn <string> (no default)\n");
			(void)fprintf(stderr, "\t-ts1 | -ts2 (select test stand, default is ts2)\n");
			(void)fprintf(stderr, "\t-x1 | -y1 | -x2 | -y2 | -z2 (select stage by function)\n");
			(void)fprintf(stderr, "\t-timeout <seconds> (%d)\n", timeout);
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-block (wait until finished)\n");
			(void)fprintf(stderr, "\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose = 1;
		} else if (strncmp(argptr, "vv", arglen) == 0) {
			verbose = 2;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			axis = description;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			axis = serialnumber;
		} else if (strncmp(argptr, "sn", arglen) == 0) {
			serialnumber = argv[++argnum];
			axis = serialnumber;

		} else if (strncmp(argptr, "ts1", arglen) == 0) {
			teststand = TS1;
		} else if (strncmp(argptr, "ts2", arglen) == 0) {
			teststand = TS2;

		} else if (strncmp(argptr, "x1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X1 : TS2_X1);
			axis = argptr;
		} else if (strncmp(argptr, "y1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y1 : TS2_Y1);
			axis = argptr;
		} else if (strncmp(argptr, "x2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X2 : TS2_X2);
			axis = argptr;
		} else if (strncmp(argptr, "y2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y2 : TS2_Y2);
			axis = argptr;
		} else if (strncmp(argptr, "z2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Z2 : TS2_Z2);
			axis = argptr;

		} else if (strncmp(argptr, "timeout", arglen) == 0) {
			timeout = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "block", arglen) == 0) {
			block++;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(1);
		}
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: test stand %d description %s serialnumber %s timeout %d\n",
				tag, teststand, description, serialnumber, timeout);
		(void)fprintf(stdout, "%s: block %d\n", tag, block);
	}

	if (serialnumber != NULL) {
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	} else {
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	if (verbose) {
		FT_DEVICE_LIST_INFO_NODE devInfo;
		ftStatus = ftdi_get_device_info(&devInfo, tag, ftHandle);
		(void)fprintf(stderr, "%s:         Type=0x%04x\n", tag, devInfo.Type);
		(void)fprintf(stderr, "%s:           ID=0x%08x\n", tag, devInfo.ID);
		(void)fprintf(stderr, "%s: SerialNumber=%s\n", tag, devInfo.SerialNumber);
		(void)fprintf(stderr, "%s:  Description=%s\n", tag, devInfo.Description);
	}

	rcode = aptInitialize(ftHandle, P_MOD_CHAN1);
	if (rcode != 0) {
		return(-1);
	}

	rcode = aptMotMoveHome(ftHandle, P_MOD_CHAN1);
	if (rcode != 0) {
		return(-1);
	}

	if (block) {
		// wait for the move to finish
		rcode = apt_block(axis, ftHandle, timeout);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (1) {
		POSITION_COUNTER position_counter;
		if (verbose) {
			(void)fprintf(stdout, "%s: get position counter\n", tag);
		}
		rcode = aptMotGetPosCounter(&position_counter, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		(void)fprintf(stdout, "%s: get position counter rcode (%d)\n", tag, rcode);
#endif
		if (rcode == 0) {
			(void)aptPositionCounterFormat(stdout, axis, position_counter);
		}
	}

	if (verbose) {
		ENCODER_COUNTER encoder_counter;
		if (verbose) {
			(void)fprintf(stdout, "%s: get encoder counter\n", tag);
		}
		rcode = aptMotGetEncCounter(&encoder_counter, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		(void)fprintf(stdout, "%s: get encoder counter rcode (%d)\n", tag, rcode);
#endif
		if (rcode == 0) {
			(void)aptEncoderCounterFormat(stdout, axis, encoder_counter);
		}
	}

#ifdef DEBUG
	{
		BYTE pkt[APT_PKT_MAX]; int npkt;
		(void)fprintf(stdout, "%s: check for stray bytes\n", tag);
		npkt = aptRead(ftHandle, pkt, APT_PKT_MAX);
		if (npkt > 0) {
			fprintf(stdout, "%s: found %d bytes\n", tag, npkt);
			(void)aptFormat(stdout, argv[0], pkt, npkt);
		}
	}
#endif

	if (verbose) {
		(void)fprintf(stdout, "%s: close device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
