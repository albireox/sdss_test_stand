/* file: $RCSfile: apt_query.c,v $
** rcsid: $Id: apt_query.c,v 1.3 2013/08/01 20:45:00 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_query.c,v 1.3 2013/08/01 20:45:00 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: apt_query.c,v $ - Thorlabs APT motion control demo program
** Open the device & query the status
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "apt.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0xfaf0;
static char *description = "APT DC Motor Controller";
static char *serialnumber = NULL;
static int baud = 115200;

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	char *axis = "n/a";
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int params_flags = 0;
	int rcode;
	int teststand = TS2;
	int timeout = 60;	// time for reply, in seconds

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description <string> (%s)\n", description);
			(void)fprintf(stderr, "\t-serialnumber <string> (no default)\n");
			(void)fprintf(stderr, "\t-sn <string> (no default)\n");
			(void)fprintf(stderr, "\t-ts1 | -ts2 (select test stand, default is ts2)\n");
			(void)fprintf(stderr, "\t-x1 | -y1 | -x2 | -y2 | -z2 (select stage by function)\n");
			(void)fprintf(stderr, "\t-timeout <seconds> (%d)\n", timeout);
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-hw|ch|pos|enc|vel|jog|gen|rel|abs|home|limit|pid|av|pot|button|status|all\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			axis = description;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			axis = serialnumber;
		} else if (strncmp(argptr, "sn", arglen) == 0) {
			serialnumber = argv[++argnum];
			axis = serialnumber;

		} else if (strncmp(argptr, "ts1", arglen) == 0) {
			teststand = TS1;
		} else if (strncmp(argptr, "ts2", arglen) == 0) {
			teststand = TS2;

		} else if (strncmp(argptr, "x1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X1 : TS2_X1);
			axis = argptr;
		} else if (strncmp(argptr, "y1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y1 : TS2_Y1);
			axis = argptr;
		} else if (strncmp(argptr, "x2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X2 : TS2_X2);
			axis = argptr;
		} else if (strncmp(argptr, "y2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y2 : TS2_Y2);
			axis = argptr;
		} else if (strncmp(argptr, "z2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Z2 : TS2_Z2);
			axis = argptr;

		} else if (strncmp(argptr, "timeout", arglen) == 0) {
			timeout = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "hw", arglen) == 0) {
			params_flags ^= PARAMS_HW;
		} else if (strncmp(argptr, "ch", arglen) == 0) {
			params_flags ^= PARAMS_CH;
		} else if (strncmp(argptr, "pos", arglen) == 0) {
			params_flags ^= PARAMS_POS;
		} else if (strncmp(argptr, "enc", arglen) == 0) {
			params_flags ^= PARAMS_ENC;
		} else if (strncmp(argptr, "vel", arglen) == 0) {
			params_flags ^= PARAMS_VEL;
		} else if (strncmp(argptr, "jog", arglen) == 0) {
			params_flags ^= PARAMS_JOG;
		} else if (strncmp(argptr, "gen", arglen) == 0) {
			params_flags ^= PARAMS_GEN;
		} else if (strncmp(argptr, "rel", arglen) == 0) {
			params_flags ^= PARAMS_REL;
		} else if (strncmp(argptr, "abs", arglen) == 0) {
			params_flags ^= PARAMS_ABS;
		} else if (strncmp(argptr, "home", arglen) == 0) {
			params_flags ^= PARAMS_HOME;
		} else if (strncmp(argptr, "limit", arglen) == 0) {
			params_flags ^= PARAMS_LIMIT;
		} else if (strncmp(argptr, "pid", arglen) == 0) {
			params_flags ^= PARAMS_PID;
		} else if (strncmp(argptr, "av", arglen) == 0) {
			params_flags ^= PARAMS_AV;
		} else if (strncmp(argptr, "pot", arglen) == 0) {
			params_flags ^= PARAMS_POT;
		} else if (strncmp(argptr, "buttons", arglen) == 0) {
			params_flags ^= PARAMS_BUTTON;
		} else if (strncmp(argptr, "status", arglen) == 0) {
			params_flags ^= PARAMS_STATUS;
		} else if (strncmp(argptr, "all", arglen) == 0) {
			params_flags = 0xffff;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(1);
		}
	}

	// if the user gave no query params, do the position
	if (params_flags == 0) {
		params_flags |= PARAMS_POS;
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: test stand %d description %s serialnumber %s timeout %d\n",
				tag, teststand, description, serialnumber, timeout);
		(void)fprintf(stdout, "%s: params_flags 0x%04x\n", tag, params_flags);
	}

	if (serialnumber != NULL) {
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	} else {
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	if (verbose) {
		FT_DEVICE_LIST_INFO_NODE devInfo;
		ftStatus = ftdi_get_device_info(&devInfo, tag, ftHandle);
		(void)fprintf(stdout, "%s:         Type=0x%04x\n", tag, devInfo.Type);
		(void)fprintf(stdout, "%s:           ID=0x%08x\n", tag, devInfo.ID);
		(void)fprintf(stdout, "%s: SerialNumber=%s\n", tag, devInfo.SerialNumber);
		(void)fprintf(stdout, "%s:  Description=%s\n", tag, devInfo.Description);
	}

	if (params_flags & PARAMS_HW) {
		HW_INFO hw_info;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s hw info\n", tag, serialnumber);
		}
		rcode = aptHwGetInfo(&hw_info, ftHandle);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: hw_info rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptHwInfoFormat(stdout, axis, hw_info);
		}
	}

	if (params_flags & PARAMS_CH) {
		CHANNEL_ENABLE_STATE channel_enable_state;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s channel enable state\n", tag, serialnumber);
		}
		rcode = aptModGetChannelEnableState(&channel_enable_state, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get channel enable state rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptChannelEnableStateFormat(stdout, axis, channel_enable_state);
		}
	}

	if (params_flags & PARAMS_POS) {
		POSITION_COUNTER position_counter;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s position counter\n", tag, serialnumber);
		}
		rcode = aptMotGetPosCounter(&position_counter, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get position counter rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptPositionCounterFormat(stdout, axis, position_counter);
		}
	}

	if (params_flags & PARAMS_ENC) {
		ENCODER_COUNTER encoder_counter;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s encoder counter\n", tag, serialnumber);
		}
		rcode = aptMotGetEncCounter(&encoder_counter, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get encoder counter rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptEncoderCounterFormat(stdout, axis, encoder_counter);
		}
	}

	if (params_flags & PARAMS_VEL) {
		VEL_PARAMS vel_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s vel params\n", tag, serialnumber);
		}
		rcode = aptMotGetVelParams(&vel_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get vel params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptVelParamsFormat(stdout, axis, vel_params);
		}
	}

	if (params_flags & PARAMS_JOG) {
		JOG_PARAMS jog_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s jog params\n", tag, serialnumber);
		}
		rcode = aptMotGetJogParams(&jog_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get jog params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptJogParamsFormat(stdout, axis, jog_params);
		}
	}

	if (params_flags & PARAMS_GEN) {
		GEN_MOVE_PARAMS gen_move_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s gen_move params\n", tag, serialnumber);
		}
		rcode = aptMotGetGenMoveParams(&gen_move_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get gen_move params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptGenMoveParamsFormat(stdout, axis, gen_move_params);
		}
	}

	if (params_flags & PARAMS_REL) {
		MOVE_REL_PARAMS move_rel_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s move rel params\n", tag, serialnumber);
		}
		rcode = aptMotGetMoveRelParams(&move_rel_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get move rel params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptMoveRelParamsFormat(stdout, axis, move_rel_params);
		}
	}

	if (params_flags & PARAMS_ABS) {
		MOVE_ABS_PARAMS move_abs_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s move abs params\n", tag, serialnumber);
		}
		rcode = aptMotGetMoveAbsParams(&move_abs_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get move abs params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptMoveAbsParamsFormat(stdout, axis, move_abs_params);
		}
	}

	if (params_flags & PARAMS_HOME) {
		HOME_PARAMS home_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s home params\n", tag, serialnumber);
		}
		rcode = aptMotGetHomeParams(&home_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get home params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptHomeParamsFormat(stdout, axis, home_params);
		}
	}

	if (params_flags & PARAMS_LIMIT) {
		LIMIT_SWITCH_PARAMS limit_switch_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s limit switch params\n", tag, serialnumber);
		}
		rcode = aptMotGetLimitSwitchParams(&limit_switch_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get limit switch params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptLimitSwitchParamsFormat(stdout, axis, limit_switch_params);
		}
	}

	if (params_flags & PARAMS_PID) {
		DC_PID_PARAMS dc_pid_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s dc pid params\n", tag, serialnumber);
		}
		rcode = aptMotGetDcPidParams(&dc_pid_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get dc pid params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptDcPidParamsFormat(stdout, axis, dc_pid_params);
		}
	}

	if (params_flags & PARAMS_AV) {
		AV_MODES av_modes;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s av modes\n", tag, serialnumber);
		}
		rcode = aptMotGetAvModes(&av_modes, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get av modes rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptAvModesFormat(stdout, axis, av_modes);
		}
	}

	if (params_flags & PARAMS_POT) {
		POT_PARAMS pot_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s pot params\n", tag, serialnumber);
		}
		rcode = aptMotGetPotParams(&pot_params, ftHandle, P_MOD_CHAN1);
		if (verbose) {
			(void)fprintf(stdout, "%s: get pot params rcode (%d)\n", tag, rcode);
		}
		if (rcode == 0) {
			aptPotParamsFormat(stdout, axis, pot_params);
		}
	}

	if (params_flags & PARAMS_BUTTON) {
		BUTTON_PARAMS button_params;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s button params\n", tag, serialnumber);
		}
		rcode = aptMotGetButtonParams(&button_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get button params rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptButtonParamsFormat(stdout, axis, button_params);
		}
	}

	if (params_flags & PARAMS_STATUS) {
		STATUS_BITS status_bits;
		if (verbose) {
			(void)fprintf(stdout, "%s: get %s status bits\n", tag, serialnumber);
		}
		rcode = aptMotGetStatusBits(&status_bits, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: get status bits rcode (%d)\n", tag, rcode);
		}
#endif
		if (rcode == 0) {
			aptStatusBitsFormat(stdout, axis, status_bits);
		}
	}

#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stdout, "%s: check for stray bytes\n", tag);
		npkt = aptRead(ftHandle, pkt, APT_PKT_MAX);
		if (npkt > 0) {
			fprintf(stdout, "%s: found %d bytes\n", tag, npkt);
			(void)aptFormat(stdout, argv[0], pkt, npkt);
		}
	}
#endif

	if (verbose) {
		(void)fprintf(stdout, "%s: close device %s\n", tag, serialnumber);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
