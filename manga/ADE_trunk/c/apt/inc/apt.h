/* file: $RCSfile: apt.h,v $
** rcsid: $Id: apt.h,v 1.2 2013/08/01 20:36:38 jwp Exp mabadmin $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
** *******************************************************************
** $RCSfile: apt.h,v $ - thorlabs APT motion control header file
** These things are taken from
** Thorlabs APT Controllers Host-Controller Communications Protocol Issue 3 Date: 01-June-2012
** *******************************************************************
*/

#ifndef APT_H

#include <stdio.h>
#include <sys/time.h>
#include "ftdi.h"

/******************************/
/* global verbosity indicator */
/******************************/
extern int verbose;

/**************************/
/* global debug indicator */
/**************************/
extern int debug;

/**********************/
/* special data types */
/**********************/

/****************/
/* misc. macros */
/****************/
#define TVTIME(tv)	(tv.tv_sec+(tv.tv_usec/1e6))
#define TVDIFF(tv1,tv2)	(TVTIME(tv1)-TVTIME(tv2))
#ifdef MIN
#undef MIN
#endif
#define MIN(a,b)	((a)<(b)?(a):(b))
#ifdef MAX
#undef MAX
#endif
#define MAX(a,b)	((a)>(b)?(a):(b))
#define RANGE(x,a,b)	(MIN(MAX((x),(a)),(b)))

#define APT_PKT_MAX	(128)

#define APT_HOST	(0x01)
#define APT_DEST	(0x50)

/* APT destination flag (page 9) */
#define APT_DATA	(0x80)

// map devices onto our axes

#define TS1	(0)
#define TS2 (1)

//#define TS1_X1	"83836888"
#define TS1_X1	"83844231"
#define TS1_Y1	"83839474"
#define TS1_X2	"83839464"
#define TS1_Y2	"83839445"
#define TS1_Z2	"83845995"

#define TS2_X1	"83842661"
#define TS2_Y1	"83840945"
#define TS2_X2	"83842739"
#define TS2_Y2	"83841096"
#define TS2_Z2	"83856416"

// NB: (param1, param2) should be treated as a union with (ndata).
typedef struct apt_pkt {
	int opcode;
	int param1;
	int param2;
	int ndata;
	int dest;
	int host;
	BYTE data[APT_PKT_MAX];
} APT_PKT;

/* source and destination bytes (page 10) */
#define HOST	(0x01)
#define RACK	(0x11)
#define BAY0	(0x21)
#define BAY1	(0x22)
#define BAY2	(0x23)
#define BAY3	(0x24)
#define BAY4	(0x25)
#define BAY5	(0x26)
#define BAY6	(0x27)
#define BAY7	(0x28)
#define BAY8	(0x29)
#define BAY9	(0x2A)
#define USB		(0X50)

/* APT typdedefs (page 12) */
typedef short	word;
typedef long	dword;

// Hardware type idents.
#define P_HW_RACK_USB			(0x01)
#define P_HW_RACK_ETHERNET		(0x02)
#define P_HW_MOTOR				(0x10)
#define P_HW_PIEZO				(0x12)
#define P_HW_POWERMETER			(0x13)
#define P_HW_NANOTRAK			(0x14)
#define P_HW_CTRL6MOTOR3CHAN	(0x20)
#define P_HW_CTRL6PIEZO3CHAN	(0x22)

// Channel idents.
#define P_MOD_CHAN1				(0x01)
#define P_MOD_CHAN2				(0x02)
#define P_MOD_CHAN3				(0x04)
#define P_MOD_CHAN4				(0x08)

#define P_MOD_CHAN_ENABLE		(0x1)
#define P_MOD_CHAN_DISABLE		(0x2)

// Hardware limit switch mode definitions.
#define P_MOT_SWITCHIGNORE		(0x01)	//	Ignore switch or switch not present.
#define P_MOT_SWITCHMAKES		(0x02)	//	Switch makes on contact.
#define P_MOT_SWITCHBREAKS		(0x03)	//	Switch breaks on contact.
#define P_MOT_SWITCHMAKES_HOME	(0x04)	//	Switch makes on contact.
#define P_MOT_SWITCHBREAKS_HOME	(0x05)	//	Switch breaks on contact.
#define P_MOT_SWITCHINDEX_HOME	(0x06)
// KAD 06-12-05 - Set upper bit to swap CW and CCW limit switches in code.
// Both wCWHardLimit and wCCWHardLimit structure members will have the upper bit
// set when limit switches have been physically swapped.
#define P_MOT_SWITCHSWAPPED		(0x80)	//	bitwise OR'd with one of the settings above...

// Jog direction definitions.
#define P_MOT_JOG_FWD			(0x01)
#define P_MOT_JOG_REV			(0x02)

// Jog mode definitions.
#define P_MOT_JOGCONTINUOUS		(0x01)	//	Move continuously while jog signal present.
#define P_MOT_JOGSINGLESTEP		(0x02)	//	Move one jog step (lJogStepSize) when jog

// wDirection definitions.
#define P_MOT_CW				(0x01)	//	Clockwise.
#define P_MOT_CCW				(0x02)	//	Counterclockwise.
// wLimSwitch definitions.
#define P_MOT_CWHARD			(0x01)	//	Clockwise hardware switch.
#define P_MOT_CCWHARD			(0x04)	//	Counterclockwise hardware switch.

// Stop mode definitions.
#define P_MOT_STOP_IMMEDIATE	(0x01)	//	stops the move immediately
#define P_MOT_STOP_PROFILED		(0x02)	//	stops the move in a controlled deceleration.

// software limit switch mode
#define P_MOT_LIMIT_IGNORE			(0x01)	//	Ignore Limit
#define P_MOT_LIMIT_STOP_IMMEDIATE	(0x02)	//	Stop Immediate at Limit
#define P_MOT_LIMIT_STOP_PROFILED	(0x03)	//	Profiled Stop at limit
#define P_MOT_LIMIT_ROTATION_STAGE	(0x80)	//	Rotation Stage Limit (bitwise OR'd with one of the settings above)

// Button operating modes.
#define P_MOT_BUTTON_JOG		(0x01)	//	Buttons used for jogging (using the jog settings described earlier).
#define P_MOT_BUTTON_POSITION	(0x02)	//	Buttons used for preset positions.

// AV LED modes
#define P_MOT_AV_MODE_IDENT		(0x01)	//	blinks in response to the IDENT msg
#define P_MOT_AV_MODE_LIMIT		(0x02)	//	blinks when in a limit
#define P_MOT_AV_MODE_MOVING	(0x08)	//	blinks when moving

// Motor specific status bit locations.
#define P_MOT_SB_CWHARDLIMIT	(0x00000001)	//	CW hardware limit switch (0 - no contact, 1 - contact).
#define P_MOT_SB_CCWHARDLIMIT	(0x00000002)	//	CCW hardware limit switch (0 - no contact, 1 - contact).
#define P_MOT_SB_CWSOFTLIMIT	(0x00000004)	//	CW software limit switch (0 - no contact, 1 - contact).
#define P_MOT_SB_CCWSOFTLIMIT	(0x00000008)	//	CCW software limit switch (0 - no contact, 1 - contact).
#define P_MOT_SB_INMOTIONCW		(0x00000010)	//	Moving clockwise (1 - moving, 0 - stationary).
#define P_MOT_SB_INMOTIONCCW	(0x00000020)	//	Moving counterclockwise (1 - moving, 0 - stationary).
#define P_MOT_SB_JOGGINGCW		(0x00000040)	//	Jogging clockwise (1 - moving, 0 - stationary).
#define P_MOT_SB_JOGGINGCCW		(0x00000080)	//	Jogging counterclockwise (1 - moving, 0 - stationary).
#define P_MOT_SB_CONNECTED		(0x00000100)	//	Motor connected (1 - connected, 0 - not connected).
#define P_MOT_SB_HOMING			(0x00000200)	//	Motor homing (1 - homing, 0 - not homing).
#define P_MOT_SB_HOMED			(0x00000400)	//	Motor homed (1 - homed, 0 - not homed).
#define P_MOT_SB_UNUSED_11		(0x00000800)
#define P_MOT_SB_INTERLOCK		(0x00001000)	//	Interlock state (1 - enabled, 0 - disabled)
#define P_MOT_SB_UNUSED_13		(0x00002000)
#define P_MOT_SB_UNUSED_14		(0x00004000)
#define P_MOT_SB_UNUSED_15		(0x00008000)

// dc status bit mask meaning
#define P_MOT_DC_LIMIT_FWD		(0x00000001)	//	forward hardware limit switch is active
#define P_MOT_DC_LIMIT_REV		(0x00000002)	//	reverse hardware limit switch is active
#define P_MOT_DC_UNUSED_02		(0x00000004)
#define P_MOT_DC_UNUSED_03		(0x00000008)
#define P_MOT_DC_MOVING_FWD		(0x00000010)	//	in motion, moving forward
#define P_MOT_DC_MOVING_REV		(0x00000020)	//	in motion, moving reverse
#define P_MOT_DC_JOGGING_FWD	(0x00000040)	//	in motion, jogging forward
#define P_MOT_DC_JOGGING_REV	(0x00000080)	//	in motion, jogging reverse
#define P_MOT_DC_UNUSED_08		(0x00000100)
#define P_MOT_DC_HOMING			(0x00000200)	//	in motion, homing
#define P_MOT_DC_HOMED			(0x00000400)	//	homed (homing has been completed)
#define P_MOT_DC_UNUSED_11		(0x00000800)
#define P_MOT_DC_TRACKING		(0x00001000)	//	tracking
#define P_MOT_DC_SETTLED		(0x00002000)	//	settled
#define P_MOT_DC_MOTION_ERROR	(0x00004000)	//	motion error (excessive position error)
#define P_MOT_DC_UNUSED_15		(0x00008000)
#define P_MOT_DC_CURRENT_LIMIT	(0x01000000)	//	motor current limit reached
#define P_MOT_DC_CHANNEL_ENABLED	(0x80000000)	//	channel is enabled

#define P_MOT_PID_FILTER_KP		(0x01)
#define P_MOT_PID_FILTER_KI		(0x02)
#define P_MOT_PID_FILTER_KD		(0x04)
#define P_MOT_PID_FILTER_ILIMIT	(0x08)

// encoder constant (counts/mm)
#define Ke	(67*512.0)

/**
 * From Section 7 (page 13) of the APT Communications Protocol Rev 3:
 *
 * In general,
 * the messages used in the communication protocol can be divided into 5 main groups:
 * generic commands,
 * move parameter setup commands,
 * move initiating commands,
 * status update message related commands
 * and error messages.
 *
 * The commands listed below are a subset of all the commands available
 * but they should enable any type of movement to be controlled.
 * There are other commands available to perform other functions
 * (for example for setting digital outputs)
 * but these are not relevant for most applications that control movement.
 * Some of these will nevertheless be listed here for completeness.
 *
 * Where the same command has the SET/REQ/GET versions,
 * the summary below only shows the SET version.
 * The detailed description of the command lists the other versions.
 */

#ifdef NOPE
/*********************/
/* Generic commands: */
/*********************/
#define MGMSG_MOD_IDENTIFY						(0x0223)	//	Identify
#define MGMSG_HW_REQ_INFO						(0x0005)	//	Hardware information
#define MGMSG_MOD_SET_CHANENABLESTATE			(0x0210)	//	Enable channel
/**********************************/
/* Move parameter setup commands: */
/**********************************/
#define MGMSG_MOT_SET_POSCOUNTER				(0x0410)	//	Set position counter
#define MGMSG_MOT_SET_VELPARAMS					(0x0414)	//	Set velocity parameters
#define MGMSG_MOT_SET_JOGPARAMS					(0x0416)	//	Set jogging parameters
#define MGMSG_MOT_SET_GENMOVEPARAMS				(0x043A)	//	Set general move parameters
#define MGMSG_MOT_SET_MOVERELPARAMS				(0x0445)	//	Set relative move parameters
#define MGMSG_MOT_SET_MOVEABSPARAMS				(0x0450)	//	Set absolute move parameters
#define MGMSG_MOT_SET_HOMEPARAMS				(0x0442)	//	Set parameters for homing
/*****************************/
/* Move initiating commands: */
/*****************************/
#define MGMSG_MOT_MOVE_HOME						(0x0443)	//	Initiate homing
#define MGMSG_MOT_MOVE_RELATIVE					(0x0448)	//	Move relative
#define MGMSG_MOT_MOVE_ABSOLUTE					(0x0453)	//	Move absolute
#define MGMSG_MOT_MOVE_JOG						(0x046A)	//	Jog
#define MGMSG_MOT_MOVE_STOP						(0x0465)	//	Stop movement
/*******************************************/
/* Status update message related commands: */
/*******************************************/
#define MGMSG_HW_START_UPDATEMSGS				(0x0011)	//	Start sending update messages
#define MGMSG_HW_STOP_UPDATEMSGS				(0x0011)	//	Stop sending update messages
#define MGMSG_MOT_REQ_DCSTATUSUPDATE			(0x0490)	//	Send status update
#define MGMSG_MOT_REQ_STATUSBITS				(0x0429)	//	Send status bits only
/*******************/
/* Error messages: */
/*******************/
#define MGMSG_HW_RESPONSE						(0x0080)	//	Short error message
#define MGMSG_HW_RICHRESPONSE					(0x0081)	//	Verbose error message
#endif

/**
 * Here follows the complete list of opcodes;
 * We have commented out the ones not relevant to the TDC001 controller.
 * This was taken from page 136 of the APT Communications Protocol, Rev 3.
 */

/***********************************/
/* Generic System Control Messages */
/***********************************/
#define MGMSG_MOD_IDENTIFY						(0x0223)
#define MGMSG_MOD_SET_CHANENABLESTATE			(0x0210)
#define MGMSG_MOD_REQ_CHANENABLESTATE			(0x0211)
#define MGMSG_MOD_GET_CHANENABLESTATE			(0x0212)
#define MGMSG_HW_DISCONNECT						(0x0002)
#define MGMSG_HW_RESPONSE						(0x0080)
#define MGMSG_HW_RICHRESPONSE					(0x0081)
#define MGMSG_HW_START_UPDATEMSGS				(0x0011)
#define MGMSG_HW_STOP_UPDATEMSGS				(0x0012)
#define MGMSG_HW_REQ_INFO						(0x0005)
#define MGMSG_HW_GET_INFO						(0x0006)
#define MGMSG_RACK_REQ_BAYUSED					(0x0060)
#define MGMSG_RACK_GET_BAYUSED					(0x0061)
#define MGMSG_HUB_REQ_BAYUSED					(0x0065)
#define MGMSG_HUB_GET_BAYUSED					(0x0066)
#define MGMSG_RACK_REQ_STATUSBITS				(0x0226)
#define MGMSG_RACK_GET_STATUSBITS				(0x0227)
#define MGMSG_RACK_SET_DIGOUTPUTS				(0x0228)
#define MGMSG_RACK_REQ_DIGOUTPUTS				(0x0229)
#define MGMSG_RACK_GET_DIGOUTPUTS				(0x0230)
/**************************/
/* Motor Control Messages */
/**************************/
#define MGMSG_MOT_SET_POSCOUNTER				(0x0410)
#define MGMSG_MOT_REQ_POSCOUNTER				(0x0411)
#define MGMSG_MOT_GET_POSCOUNTER				(0x0412)
#define MGMSG_MOT_SET_ENCCOUNTER				(0x0409)
#define MGMSG_MOT_REQ_ENCCOUNTER				(0x040A)
#define MGMSG_MOT_GET_ENCCOUNTER				(0x040B)
#define MGMSG_MOT_SET_VELPARAMS					(0x0413)
#define MGMSG_MOT_REQ_VELPARAMS					(0x0414)
#define MGMSG_MOT_GET_VELPARAMS					(0x0415)
#define MGMSG_MOT_SET_JOGPARAMS					(0x0416)
#define MGMSG_MOT_REQ_JOGPARAMS					(0x0417)
#define MGMSG_MOT_GET_JOGPARAMS					(0x0418)
#define MGMSG_MOT_SET_GENMOVEPARAMS				(0x043A)
#define MGMSG_MOT_REQ_GENMOVEPARAMS				(0x043B)
#define MGMSG_MOT_GET_GENMOVEPARAMS				(0x043C)
#define MGMSG_MOT_SET_MOVERELPARAMS				(0x0445)
#define MGMSG_MOT_REQ_MOVERELPARAMS				(0x0446)
#define MGMSG_MOT_GET_MOVERELPARAMS				(0x0447)
#define MGMSG_MOT_SET_MOVEABSPARAMS				(0x0450)
#define MGMSG_MOT_REQ_MOVEABSPARAMS				(0x0451)
#define MGMSG_MOT_GET_MOVEABSPARAMS				(0x0452)
#define MGMSG_MOT_SET_HOMEPARAMS				(0x0440)
#define MGMSG_MOT_REQ_HOMEPARAMS				(0x0441)
#define MGMSG_MOT_GET_HOMEPARAMS				(0x0442)
#define MGMSG_MOT_SET_LIMSWITCHPARAMS			(0x0423)
#define MGMSG_MOT_REQ_LIMSWITCHPARAMS			(0x0424)
#define MGMSG_MOT_GET_LIMSWITCHPARAMS			(0x0425)
#define MGMSG_MOT_MOVE_HOME						(0x0443)
#define MGMSG_MOT_MOVE_HOMED					(0x0444)
#define MGMSG_MOT_MOVE_RELATIVE					(0x0448)
#define MGMSG_MOT_MOVE_COMPLETED				(0x0464)
#define MGMSG_MOT_MOVE_ABSOLUTE					(0x0453)
#define MGMSG_MOT_MOVE_JOG						(0x046A)
#define MGMSG_MOT_MOVE_VELOCITY					(0x0457)
#define MGMSG_MOT_MOVE_STOP						(0x0465)
#define MGMSG_MOT_MOVE_STOPPED					(0x0466)
#define MGMSG_MOT_SET_DCPIDPARAMS				(0x04A0)
#define MGMSG_MOT_REQ_DCPIDPARAMS				(0x04A1)
#define MGMSG_MOT_GET_DCPIDPARAMS				(0x04A2)
#define MGMSG_MOT_SET_AVMODES					(0x04B3)
#define MGMSG_MOT_REQ_AVMODES					(0x04B4)
#define MGMSG_MOT_GET_AVMODES					(0x04B5)
#define MGMSG_MOT_SET_POTPARAMS					(0x04B0)
#define MGMSG_MOT_REQ_POTPARAMS					(0x04B1)
#define MGMSG_MOT_GET_POTPARAMS					(0x04B2)
#define MGMSG_MOT_SET_BUTTONPARAMS				(0x04B6)
#define MGMSG_MOT_REQ_BUTTONPARAMS				(0x04B7)
#define MGMSG_MOT_GET_BUTTONPARAMS				(0x04B8)
#define MGMSG_MOT_SET_EEPROMPARAMS				(0x04B9)
#define MGMSG_MOT_SET_PMDPOSITIONLOOPPARAMS		(0x04D7)
#define MGMSG_MOT_REQ_PMDPOSITIONLOOPPARAMS		(0x04D8)
#define MGMSG_MOT_GET_PMDPOSITIONLOOPPARAMS		(0x04D9)
#define MGMSG_MOT_SET_PMDMOTOROUTPUTPARAMS		(0x04DA)
#define MGMSG_MOT_REQ_PMDMOTOROUTPUTPARAMS		(0x04DB)
#define MGMSG_MOT_GET_PMDMOTOROUTPUTPARAMS		(0x04DC)
#define MGMSG_MOT_SET_PMDTRACKSETTLEPARAMS		(0x04E0)
#define MGMSG_MOT_REQ_PMDTRACKSETTLEPARAMS		(0x04E1)
#define MGMSG_MOT_GET_PMDTRACKSETTLEPARAMS		(0x04E2)
#define MGMSG_MOT_SET_PMDPROFILEMODEPARAMS		(0x04E3)
#define MGMSG_MOT_REQ_PMDPROFILEMODEPARAMS		(0x04E4)
#define MGMSG_MOT_GET_PMDPROFILEMODEPARAMS		(0x04E5)
#define MGMSG_MOT_SET_PMDCURRENTLOOPPARAMS		(0x04D4)
#define MGMSG_MOT_REQ_PMDCURRENTLOOPPARAMS		(0x04D5)
#define MGMSG_MOT_GET_PMDCURRENTLOOPPARAMS		(0x04D6)
#define MGMSG_MOT_SET_PMDSETTLEDCURRENTLOOPPARAMS	(0x04E9)
#define MGMSG_MOT_REQ_PMDSETTLEDCURRENTLOOPPARAMS	(0x04EA)
#define MGMSG_MOT_GET_PMDSETTLEDCURRENTLOOPPARAMS	(0x04EB)
#define MGMSG_MOT_SET_PMDSTAGEAXISPARAMS		(0x04F0)
#define MGMSG_MOT_REQ_PMDSTAGEAXISPARAMS		(0x04F1)
#define MGMSG_MOT_GET_PMDSTAGEAXISPARAMS		(0x04F2)
#define MGMSG_MOT_REQ_STATUSUPDATE				(0x0480)
#define MGMSG_MOT_GET_STATUSUPDATE				(0x0481)
#define MGMSG_MOT_REQ_DCSTATUSUPDATE			(0x0490)
#define MGMSG_MOT_GET_DCSTATUSUPDATE			(0x0491)
#define MGMSG_MOT_ACK_DCSTATUSUPDATE			(0x0492)
#define MGMSG_MOT_REQ_STATUSBITS				(0x0429)
#define MGMSG_MOT_GET_STATUSBITS				(0x042A)
#define MGMSG_MOT_SUSPEND_ENDOFMOVEMSGS			(0x046B)
#define MGMSG_MOT_RESUME_ENDOFMOVEMSGS			(0x046C)
/*****************************/
/* Solenoid Control Messages */
/*****************************/
#define MGMSG_MOT_SET_SOL_OPERATINGMODE			(0x04C0)
#define MGMSG_MOT_REQ_SOL_OPERATINGMODE			(0x04C1)
#define MGMSG_MOT_GET_SOL_OPERATINGMODE			(0x04C2)
#define MGMSG_MOT_SET_SOL_CYCLEPARAMS			(0x04C3)
#define MGMSG_MOT_REQ_SOL_CYCLEPARAMS			(0x04C4)
#define MGMSG_MOT_GET_SOL_CYCLEPARAMS			(0x04C5)
#define MGMSG_MOT_SET_SOL_INTERLOCKMODE			(0x04C6)
#define MGMSG_MOT_REQ_SOL_INTERLOCKMODE			(0x04C7)
#define MGMSG_MOT_GET_SOL_INTERLOCKMODE			(0x04C8)
#define MGMSG_MOT_SET_SOL_STATE					(0x04CB)
#define MGMSG_MOT_REQ_SOL_STATE					(0x04CC)
#define MGMSG_MOT_GET_SOL_STATE					(0x04CD)
/**************************/
/* Piezo Control Messages */
/**************************/
#define MGMSG_PZ_SET_POSCONTROLMODE				(0x0640)
#define MGMSG_PZ_REQ_POSCONTROLMODE				(0x0641)
#define MGMSG_PZ_GET_POSCONTROLMODE				(0x0642)
#define MGMSG_PZ_SET_OUTPUTVOLTS				(0x0643)
#define MGMSG_PZ_REQ_OUTPUTVOLTS				(0x0644)
#define MGMSG_PZ_GET_OUTPUTVOLTS				(0x0645)
#define MGMSG_PZ_SET_OUTPUTPOS					(0x0646)
#define MGMSG_PZ_REQ_OUTPUTPOS					(0x0647)
#define MGMSG_PZ_GET_OUTPUTPOS					(0x0648)
#define MGMSG_PZ_SET_INPUTVOLTSSRC				(0x0652)
#define MGMSG_PZ_REQ_INPUTVOLTSSRC				(0x0653)
#define MGMSG_PZ_GET_INPUTVOLTSSRC				(0x0654)
#define MGMSG_PZ_SET_PICONSTS					(0x0655)
#define MGMSG_PZ_REQ_PICONSTS					(0x0656)
#define MGMSG_PZ_GET_PICONSTS					(0x0657)
#define MGMSG_PZ_REQ_PZSTATUSBITS				(0x065B)
#define MGMSG_PZ_GET_PZSTATUSBITS				(0x065C)
#define MGMSG_PZ_GET_PZSTATUSUPDATE				(0x0661)
#define MGMSG_PZ_ACK_PZSTATUSUPDATE				(0x0662)
#define MGMSG_PZ_SET_OUTPUTLUT					(0x0700)
#define MGMSG_PZ_REQ_OUTPUTLUT					(0x0701)
#define MGMSG_PZ_GET_OUTPUTLUT					(0x0702)
#define MGMSG_PZ_SET_OUTPUTLUTPARAMS			(0x0703)
#define MGMSG_PZ_REQ_OUTPUTLUTPARAMS			(0x0704)
#define MGMSG_PZ_GET_OUTPUTLUTPARAMS			(0x0705)
#define MGMSG_PZ_START_LUTOUTPUT				(0x0706)
#define MGMSG_PZ_STOP_LUTOUTPUT					(0x0707)
#define MGMSG_PZ_SET_EEPROMPARAMS				(0x07D0)
#define MGMSG_PZ_SET_TPZ_DISPSETTINGS			(0x07D1)
#define MGMSG_PZ_REQ_TPZ_DISPSETTINGS			(0x07D2)
#define MGMSG_PZ_GET_TPZ_DISPSETTINGS			(0x07D3)
#define MGMSG_PZ_SET_TPZ_IOSETTINGS				(0x07D4)
#define MGMSG_PZ_REQ_TPZ_IOSETTINGS				(0x07D5)
#define MGMSG_PZ_GET_TPZ_IOSETTINGS				(0x07D6)
#define MGMSG_PZ_SET_ZERO						(0x0658)
#define MGMSG_PZ_REQ_MAXTRAVEL					(0x0650)
#define MGMSG_PZ_GET_MAXTRAVEL					(0x0651)
#define MGMSG_PZ_SET_IOSETTINGS					(0x0670)
#define MGMSG_PZ_REQ_IOSETTINGS					(0x0671)
#define MGMSG_PZ_GET_IOSETTINGS					(0x0672)
#define MGMSG_PZ_SET_OUTPUTMAXVOLTS				(0x0680)
#define MGMSG_PZ_REQ_OUTPUTMAXVOLTS				(0x0681)
#define MGMSG_PZ_GET_OUTPUTMAXVOLTS				(0x0682)
#define MGMSG_PZ_SET_TPZ_SLEWRATES				(0x0683)
#define MGMSG_PZ_REQ_TPZ_SLEWRATES				(0x0684)
#define MGMSG_PZ_GET_TPZ_SLEWRATES				(0x0685)
#define MGMSG_MOT_SET_PZSTAGEPARAMDEFAULTS		(0x0686)
#define MGMSG_PZ_SET_LUTVALUETYPE				(0x0708)
#define MGMSG_PZ_SET_TSG_IOSETTINGS				(0x07DA)
#define MGMSG_PZ_REQ_TSG_IOSETTINGS				(0x07DB)
#define MGMSG_PZ_GET_TSG_IOSETTINGS				(0x07DC)
#define MGMSG_PZ_REQ_TSG_READING				(0x07DD)
#define MGMSG_PZ_GET_TSG_READING				(0x07DE)

// enumerate the various Set items
#define PARAMS_HW		(0x0001)
#define PARAMS_CH		(0x0002)
#define PARAMS_POS		(0x0004)
#define PARAMS_ENC		(0x0008)
#define PARAMS_VEL		(0x0010)
#define PARAMS_JOG		(0x0020)
#define PARAMS_GEN		(0x0040)
#define PARAMS_REL		(0x0080)
#define PARAMS_ABS		(0x0100)
#define PARAMS_HOME		(0x0200)
#define PARAMS_LIMIT	(0x0400)
#define PARAMS_PID		(0x0800)
#define PARAMS_AV		(0x1000)
#define PARAMS_POT		(0x2000)
#define PARAMS_BUTTON	(0x4000)
#define PARAMS_STATUS	(0x8000)

// structs for parameters

typedef struct channel_enable_state {
	int channel;
	int enable_state;
} CHANNEL_ENABLE_STATE;

typedef struct hw_info {
	int serial_number;
	char model_number[8];
	int hw_type;
	int sw_version;
	char notes[64];
	int num_channels;
} HW_INFO;

typedef struct position_counter {
	int channel;
	int position;
} POSITION_COUNTER;

typedef struct encoder_counter {
	int channel;
	int encoder;
} ENCODER_COUNTER;

typedef struct vel_params {
	int channel;
	int min_vel;
	int acc;
	int max_vel;
} VEL_PARAMS;

typedef struct jog_params {
	int channel;
	int mode;
	int step_size;
	int min_vel;
	int acc;
	int max_vel;
	int stop_mode;
} JOG_PARAMS;

typedef struct gen_move_params {
	int channel;
	int backlash_distance;
} GEN_MOVE_PARAMS;

typedef struct move_rel_params {
	int channel;
	int relative_distance;
} MOVE_REL_PARAMS;

typedef struct move_abs_params {
	int channel;
	int absolute_position;
} MOVE_ABS_PARAMS;

typedef struct home_params {
	int channel;
	int direction;
	int limit_switch;
	int vel;
	int offset_distance;
} HOME_PARAMS;

typedef struct limit_switch_params {
	int channel;
	int cw_hard_limit;
	int ccw_hard_limit;
	int cw_soft_limit;
	int ccw_soft_limit;
	int soft_limit_mode;
} LIMIT_SWITCH_PARAMS;

typedef struct dc_pid_params {
	int channel;
	int Kp;
	int Ki;
	int Kd;
	int ILimit;
	int filter_control;
} DC_PID_PARAMS;

typedef struct av_modes {
	int channel;
	int mode_bits;
} AV_MODES;

typedef struct pot_params {
	int channel;
	int wnd_0;
	int vel_1;
	int wnd_1;
	int vel_2;
	int wnd_2;
	int vel_3;
	int wnd_3;
	int vel_4;
} POT_PARAMS;

typedef struct button_params {
	int channel;
	int mode;
	int pos_1;
	int pos_2;
	int timeout;
	int not_used;
} BUTTON_PARAMS;

typedef struct dc_status_update {
	int channel;
	int position;
	int velocity;
	int reserved;
	int status_bits;
} DC_STATUS_UPDATE;

typedef struct status_bits {
	int channel;
	int status_bits;
} STATUS_BITS;


// macros and place-holders for messages

#define aptModIdentify(ftHandle)				aptCmdSend(ftHandle, MGMSG_MOD_IDENTIFY, 1)
// aptModSetChannelEnableState
#define aptModReqChannelEnableState(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOD_REQ_CHANENABLESTATE, channel)
// aptModGetChannelEnableState

// aptHwResponse
#define aptHwStartUpdateMsgs(ftHandle)			aptCmdSend(ftHandle, MGMSG_HW_START_UPDATE_MSGS, 1)
#define aptHwStopUpdateMsgs(ftHandle)			aptCmdSend(ftHandle, MGMSG_HW_STOP_UPDATE_MSGS, 1)
#define aptHwReqInfo(ftHandle)					aptCmdSend(ftHandle, MGMSG_HW_REQ_INFO, 1)
// aptHwGetInfo

// aptMotSetPosCounter
#define aptMotReqPosCounter(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_POSCOUNTER, channel)
// aptMotGetPosCounter

// aptMotSetEncCounter
#define aptMotReqEncCounter(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_ENCCOUNTER, channel)
// aptMotGetEncCounter

// aptMotSetVelParams
#define aptMotReqVelParams(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_VELPARAMS, channel)
// aptMotGetVelParams

// aptMotSetJogParams
#define aptMotReqJogParams(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_JOGPARAMS, channel)
// aptMotGetJogParams

// aptMotSetGenMoveParams
#define aptMotReqGenMoveParams(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_GENMOVEPARAMS, channel)
// aptMotGetGenMoveParams

// aptMotSetMoveRelParams
#define aptMotReqMoveRelParams(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_MOVERELPARAMS, channel)
// aptMotGetMoveRelParams

// aptMotSetMoveAbsParams
#define aptMotReqMoveAbsParams(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_MOVEABSPARAMS, channel)
// aptMotGetMoveAbsParams

// aptMotSetHomeParams
#define aptMotReqHomeParams(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_HOMEPARAMS, channel)
// aptMotGetHomeParams

// aptMotSetLimitSwitchParams
#define aptMotReqLimitSwitchParams(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_LIMSWITCHPARAMS, channel)
// aptMotGetLimitSwitchParams


#define aptMotMoveHome(ftHandle, channel)			aptCmdSend(ftHandle, MGMSG_MOT_MOVE_HOME, channel)
// aptMotMoveHomed
#define aptMotMoveRelative(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_MOVE_RELATIVE, channel)
// aptMotMoveCompleted
#define aptMotMoveAbsolute(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_MOVE_ABSOLUTE, channel)
// aptMotMoveJog
// aptMotMoveVelocity
// aptMotMoveStop
// aptMotMoveStopped

// aptMotSetDcPidParams
#define aptMotReqDcPidParams(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_DCPIDPARAMS, channel)
// aptMotGetDcPidParams

// aptMotSetAvModes
#define aptMotReqAvModes(ftHandle, channel)			aptCmdSend(ftHandle, MGMSG_MOT_REQ_AVMODES, channel)
// aptMotGetAvModes

// aptMotSetPotParams
#define aptMotReqPotParams(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_POTPARAMS, channel)
// aptMotGetPoParams

// aptMotSetButtonParams
#define aptMotReqButtonParams(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_BUTTONPARAMS, channel)
// aptMotGetButtonParams

#define aptMotReqDcStatusUpdate(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_REQ_DCSTATUSUPDATE, channel)
#define aptMotAckDcStatusUpdate(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_ACK_DCSTATUSUPDATE, channel)

#define aptMotReqStatusBits(ftHandle, channel)		aptCmdSend(ftHandle, MGMSG_MOT_REQ_STATUSBITS, channel)
// aptMotGetStatusBits

#define aptMotSuspendEndOfMoveMsgs(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_SUSPEND_ENDOFMOVEMSGS, channel)
#define aptMotResumeEndOfMoveMsgs(ftHandle, channel)	aptCmdSend(ftHandle, MGMSG_MOT_RESUME_ENDOFMOVEMSGS, channel)

/* EXTERN_START */
extern APT_PKT aptNewCmd(int opcode, int channel);
extern char *aptButtonParamsModeString(int mode);
extern char *aptChannelEnableStateString(int enable_state);
extern char *aptJogParamsModeString(int mode);
extern char *aptJogParamsStopModeString(int stop_mode);
extern char *aptLimitParamsHardLimitOpString(int hard_limit);
extern char *aptLimitParamsSoftLimitModeString(int soft_limit_mode);
extern char *apt_opcode_name(int opcode);
extern int aptAvModesDecode(AV_MODES *av_modes, BYTE *pkt, int pmax);
extern int aptAvModesEncode(BYTE *pkt, int pmax, AV_MODES av_modes);
extern int aptAvModesFormat(FILE *fp, char *tag, AV_MODES av_modes);
extern int aptButtonParamsDecode(BUTTON_PARAMS *button_params, BYTE *pkt, int pmax);
extern int aptButtonParamsEncode(BYTE *pkt, int pmax, BUTTON_PARAMS button_params);
extern int aptButtonParamsFormat(FILE *fp, char *tag, BUTTON_PARAMS button_params);
extern int aptChannelEnableStateFormat(FILE *fp, char *tag, CHANNEL_ENABLE_STATE channel_enable_state);
extern int aptCmdSend(FT_HANDLE ftHandle, int opcode, int channel);
extern int aptDcPidParamsDecode(DC_PID_PARAMS *dc_pid_params, BYTE *pkt, int pmax);
extern int aptDcPidParamsEncode(BYTE *pkt, int pmax, DC_PID_PARAMS dc_pid_params);
extern int aptDcPidParamsFormat(FILE *fp, char *tag, DC_PID_PARAMS dc_pid_params);
extern int aptDcStatusUpdateDecode(DC_STATUS_UPDATE *dc_status_update, BYTE *pkt, int pmax);
extern int aptDcStatusUpdateFormat(FILE *fp, char *tag, DC_STATUS_UPDATE dc_status_update);
extern int aptDecodeS1(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptDecodeS2(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptDecodeS4(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptDecodeU1(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptDecodeU2(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptDecodeU4(int *pival, BYTE *apt_pkt, int ix, int pmax);
extern int aptEncodeS1(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncodeS2(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncodeS4(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncodeU1(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncodeU2(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncodeU4(BYTE *apt_pkt, int ix, int pmax, int ival);
extern int aptEncoderCounterDecode(ENCODER_COUNTER *encoder_counter, BYTE *pkt, int pmax);
extern int aptEncoderCounterEncode(BYTE *pkt, int pmax, ENCODER_COUNTER encoder_counter);
extern int aptEncoderCounterFormat(FILE *fp, char *tag, ENCODER_COUNTER encoder_counter);
extern int aptFormat(FILE *fp, char *tag, BYTE *pkt, int npkt);
extern int aptGenMoveParamsDecode(GEN_MOVE_PARAMS *gen_move_params, BYTE *pkt, int pmax);
extern int aptGenMoveParamsEncode(BYTE *pkt, int pmax, GEN_MOVE_PARAMS gen_move_params);
extern int aptGenMoveParamsFormat(FILE *fp, char *tag, GEN_MOVE_PARAMS gen_move_params);
extern int aptHomeParamsDecode(HOME_PARAMS *home_params, BYTE *pkt, int pmax);
extern int aptHomeParamsEncode(BYTE *pkt, int pmax, HOME_PARAMS home_params);
extern int aptHomeParamsFormat(FILE *fp, char *tag, HOME_PARAMS home_params);
extern int aptHwGetInfo(HW_INFO *hw_info, FT_HANDLE ftHandle);
extern int aptHwInfoDecode(HW_INFO *hw_info, BYTE *pkt, int pmax);
extern int aptHwInfoFormat(FILE *fp, char *tag, HW_INFO hw_info);
extern int aptInitialize(FT_HANDLE ftHandle, int channel);
extern int aptJogParamsDecode(JOG_PARAMS *jog_params, BYTE *pkt, int pmax);
extern int aptJogParamsEncode(BYTE *pkt, int pmax, JOG_PARAMS jog_params);
extern int aptJogParamsFormat(FILE *fp, char *tag, JOG_PARAMS jog_params);
extern int aptLimitSwitchParamsDecode(LIMIT_SWITCH_PARAMS *limit_switch_params, BYTE *pkt, int pmax);
extern int aptLimitSwitchParamsEncode(BYTE *pkt, int pmax, LIMIT_SWITCH_PARAMS limit_switch_params);
extern int aptLimitSwitchParamsFormat(FILE *fp, char *tag, LIMIT_SWITCH_PARAMS limit_switch_params);
extern int aptModGetChannelEnableState(CHANNEL_ENABLE_STATE *channel_enable_state, FT_HANDLE ftHandle, int channel);
extern int aptModSetChannelEnableState(FT_HANDLE ftHandle, CHANNEL_ENABLE_STATE channel_enable_state);
extern int aptMotGetAvModes(AV_MODES *av_modes, FT_HANDLE ftHandle, int channel);
extern int aptMotGetButtonParams(BUTTON_PARAMS *button_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetDcPidParams(DC_PID_PARAMS *dc_pid_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetDcStatusUpdate(DC_STATUS_UPDATE *dc_status_update, FT_HANDLE ftHandle, int channel);
extern int aptMotGetEncCounter(ENCODER_COUNTER *encoder_counter, FT_HANDLE ftHandle, int channel);
extern int aptMotGetGenMoveParams(GEN_MOVE_PARAMS *gen_move_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetHomeParams(HOME_PARAMS *home_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetJogParams(JOG_PARAMS *jog_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetLimitSwitchParams(LIMIT_SWITCH_PARAMS *limit_switch_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetMoveAbsParams(MOVE_ABS_PARAMS *move_abs_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetMoveRelParams(MOVE_REL_PARAMS *move_rel_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetPosCounter(POSITION_COUNTER *position_counter, FT_HANDLE ftHandle, int channel);
extern int aptMotGetPotParams(POT_PARAMS *pot_params, FT_HANDLE ftHandle, int channel);
extern int aptMotGetStatusBits(STATUS_BITS *status_bits, FT_HANDLE ftHandle, int channel);
extern int aptMotGetVelParams(VEL_PARAMS *vel_params, FT_HANDLE ftHandle, int channel);
extern int aptMotMoveJog(FT_HANDLE ftHandle, int channel, int direction);
extern int aptMotMoveStop(FT_HANDLE ftHandle, int channel, int stop_mode);
extern int aptMotMoveVelocity(FT_HANDLE ftHandle, int channel, int direction);
extern int aptMotSetAvModes(FT_HANDLE ftHandle, AV_MODES av_modes);
extern int aptMotSetButtonParams(FT_HANDLE ftHandle, BUTTON_PARAMS button_params);
extern int aptMotSetDcPidParams(FT_HANDLE ftHandle, DC_PID_PARAMS dc_pid_params);
extern int aptMotSetEncCounter(FT_HANDLE ftHandle, ENCODER_COUNTER encoder_counter);
extern int aptMotSetGenMoveParams(FT_HANDLE ftHandle, GEN_MOVE_PARAMS gen_move_params);
extern int aptMotSetHomeParams(FT_HANDLE ftHandle, HOME_PARAMS home_params);
extern int aptMotSetJogParams(FT_HANDLE ftHandle, JOG_PARAMS jog_params);
extern int aptMotSetLimitSwitchParams(FT_HANDLE ftHandle, LIMIT_SWITCH_PARAMS limit_switch_params);
extern int aptMotSetMoveAbsParams(FT_HANDLE ftHandle, MOVE_ABS_PARAMS move_abs_params);
extern int aptMotSetMoveRelParams(FT_HANDLE ftHandle, MOVE_REL_PARAMS move_rel_params);
extern int aptMotSetPosCounter(FT_HANDLE ftHandle, POSITION_COUNTER position_counter);
extern int aptMotSetPotParams(FT_HANDLE ftHandle, POT_PARAMS pot_params);
extern int aptMotSetVelParams(FT_HANDLE ftHandle, VEL_PARAMS vel_params);
extern int aptMoveAbsParamsDecode(MOVE_ABS_PARAMS *move_abs_params, BYTE *pkt, int pmax);
extern int aptMoveAbsParamsEncode(BYTE *pkt, int pmax, MOVE_ABS_PARAMS move_abs_params);
extern int aptMoveAbsParamsFormat(FILE *fp, char *tag, MOVE_ABS_PARAMS move_abs_params);
extern int aptMoveRelParamsDecode(MOVE_REL_PARAMS *move_rel_params, BYTE *pkt, int pmax);
extern int aptMoveRelParamsEncode(BYTE *pkt, int pmax, MOVE_REL_PARAMS move_rel_params);
extern int aptMoveRelParamsFormat(FILE *fp, char *tag, MOVE_REL_PARAMS move_rel_params);
extern int aptPktDecode(APT_PKT *apt_pkt, BYTE *pkt, int pmax);
extern int aptPktEncode(BYTE *pkt, int pmax, APT_PKT apt_pkt);
extern int aptPktFormat(FILE *fp, char *tag, APT_PKT apt_pkt);
extern int aptPktHeaderDecode(APT_PKT *apt_pkt, BYTE *pkt, int pmax);
extern int aptPktRead(APT_PKT *apt_pkt, FT_HANDLE ftHandle);
extern int aptPktReadOld(APT_PKT *apt_pkt, FT_HANDLE ftHandle);
extern int aptPktWrite(FT_HANDLE ftHandle, APT_PKT apt_pkt);
extern int aptPositionCounterDecode(POSITION_COUNTER *position_counter, BYTE *pkt, int pmax);
extern int aptPositionCounterEncode(BYTE *pkt, int pmax, POSITION_COUNTER position_counter);
extern int aptPositionCounterFormat(FILE *fp, char *tag, POSITION_COUNTER position_counter);
extern int aptPotParamsDecode(POT_PARAMS *pot_params, BYTE *pkt, int pmax);
extern int aptPotParamsEncode(BYTE *pkt, int pmax, POT_PARAMS pot_params);
extern int aptPotParamsFormat(FILE *fp, char *tag, POT_PARAMS pot_params);
extern int aptRead(FT_HANDLE ftHandle, BYTE *pkt, int npkt);
extern int aptStatusBitsDecode(STATUS_BITS *status_bits, BYTE *pkt, int pmax);
extern int aptStatusBitsFormat(FILE *fp, char *tag, STATUS_BITS status_bits);
extern int aptVelParamsDecode(VEL_PARAMS *vel_params, BYTE *pkt, int pmax);
extern int aptVelParamsEncode(BYTE *pkt, int pmax, VEL_PARAMS vel_params);
extern int aptVelParamsFormat(FILE *fp, char *tag, VEL_PARAMS vel_params);
extern int aptWrite(FT_HANDLE ftHandle, BYTE *pkt, int npkt);
extern int apt_block(char *tag, FT_HANDLE ftHandle, int timeout);
extern int apt_opcode_check(int opcode);
extern void aptHubNoop(void);
extern void aptMotSolNoop(void);
extern void aptPzNoop(void);
extern void aptRackNoop(void);
/* EXTERN_STOP */

#define APT_H
#endif
