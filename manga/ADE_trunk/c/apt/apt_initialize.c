/* file: $RCSfile: apt_initialize.c,v $
** rcsid: $Id: apt_initialize.c,v 1.2 2013/08/01 20:45:00 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_initialize.c,v 1.2 2013/08/01 20:45:00 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: apt_initialize.c,v $ - Thorlabs APT motion control program
** Initialize the programmable items in the drive.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "apt.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0xfaf0;
static char *description = "APT DC Motor Controller";
static char *serialnumber = NULL;
static int baud = 115200;

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	char *axis = "n/a";
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int params_flags = 0;
	int rcode;
	int teststand = TS2;
	int timeout = 60;	// time for reply, in seconds

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description <string> (%s)\n", description);
			(void)fprintf(stderr, "\t-serialnumber <string> (no default)\n");
			(void)fprintf(stderr, "\t-sn <string> (no default)\n");
			(void)fprintf(stderr, "\t-ts1 | -ts2 (select test stand, default is ts2)\n");
			(void)fprintf(stderr, "\t-x1 | -y1 | -x2 | -y2 | -z2 (select stage by function)\n");
			(void)fprintf(stderr, "\t-timeout <seconds> (%d)\n", timeout);
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-hw|ch|pos|enc|vel|jog|gen|rel|abs|home|limit|pid|av|pot|button|status|all\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
		} else if (strncmp(argptr, "sn", arglen) == 0) {
			serialnumber = argv[++argnum];

		} else if (strncmp(argptr, "ts1", arglen) == 0) {
			teststand = TS1;
		} else if (strncmp(argptr, "ts2", arglen) == 0) {
			teststand = TS2;

		} else if (strncmp(argptr, "x1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X1 : TS2_X1);
			axis = argptr;
		} else if (strncmp(argptr, "y1", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y1 : TS2_Y1);
			axis = argptr;
		} else if (strncmp(argptr, "x2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_X2 : TS2_X2);
			axis = argptr;
		} else if (strncmp(argptr, "y2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Y2 : TS2_Y2);
			axis = argptr;
		} else if (strncmp(argptr, "z2", arglen) == 0) {
			serialnumber = (teststand == TS1 ? TS1_Z2 : TS2_Z2);
			axis = argptr;

		} else if (strncmp(argptr, "timeout", arglen) == 0) {
			timeout = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "hw", arglen) == 0) {
			params_flags ^= PARAMS_HW;
		} else if (strncmp(argptr, "ch", arglen) == 0) {
			params_flags ^= PARAMS_CH;
		} else if (strncmp(argptr, "pos", arglen) == 0) {
			params_flags ^= PARAMS_POS;
		} else if (strncmp(argptr, "enc", arglen) == 0) {
			params_flags ^= PARAMS_ENC;
		} else if (strncmp(argptr, "vel", arglen) == 0) {
			params_flags ^= PARAMS_VEL;
		} else if (strncmp(argptr, "jog", arglen) == 0) {
			params_flags ^= PARAMS_JOG;
		} else if (strncmp(argptr, "gen", arglen) == 0) {
			params_flags ^= PARAMS_GEN;
		} else if (strncmp(argptr, "rel", arglen) == 0) {
			params_flags ^= PARAMS_REL;
		} else if (strncmp(argptr, "abs", arglen) == 0) {
			params_flags ^= PARAMS_ABS;
		} else if (strncmp(argptr, "home", arglen) == 0) {
			params_flags ^= PARAMS_HOME;
		} else if (strncmp(argptr, "limit", arglen) == 0) {
			params_flags ^= PARAMS_LIMIT;
		} else if (strncmp(argptr, "pid", arglen) == 0) {
			params_flags ^= PARAMS_PID;
		} else if (strncmp(argptr, "av", arglen) == 0) {
			params_flags ^= PARAMS_AV;
		} else if (strncmp(argptr, "pot", arglen) == 0) {
			params_flags ^= PARAMS_POT;
		} else if (strncmp(argptr, "buttons", arglen) == 0) {
			params_flags ^= PARAMS_BUTTON;
		} else if (strncmp(argptr, "status", arglen) == 0) {
			params_flags ^= PARAMS_STATUS;
		} else if (strncmp(argptr, "all", arglen) == 0) {
			params_flags = 0xffff;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(1);
		}
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: test stand %d description %s serialnumber %s timeout %d\n",
				tag, teststand, description, serialnumber, timeout);
	}

	if (serialnumber != NULL) {
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	} else {
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	if (verbose) {
		FT_DEVICE_LIST_INFO_NODE devInfo;
		ftStatus = ftdi_get_device_info(&devInfo, tag, ftHandle);
		(void)fprintf(stderr, "%s:         Type=0x%04x\n", tag, devInfo.Type);
		(void)fprintf(stderr, "%s:           ID=0x%08x\n", tag, devInfo.ID);
		(void)fprintf(stderr, "%s: SerialNumber=%s\n", tag, devInfo.SerialNumber);
		(void)fprintf(stderr, "%s:  Description=%s\n", tag, devInfo.Description);
	}

	rcode = aptInitialize(ftHandle, P_MOD_CHAN1);
	if (rcode != 0) {
		return(-1);
	}

	if (params_flags & PARAMS_HW) {
		// nothing to set here
	}

	if (params_flags & PARAMS_CH) {
		CHANNEL_ENABLE_STATE channel_enable_state;
		(void)fprintf(stdout, "%s: get channel enable state\n", tag);
		rcode = aptModGetChannelEnableState(&channel_enable_state, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get channel enable state rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//channel_enable_state.channel = P_MOD_CHAN1;
		//channel_enable_state.enable_state = P_MOD_CHAN_ENABLE;
		(void)fprintf(stdout, "%s: set channel enable state\n", tag);
		rcode = aptModSetChannelEnableState(ftHandle, channel_enable_state);
		(void)fprintf(stdout, "%s: set channel enable state rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_POS) {
		POSITION_COUNTER position_counter;
		(void)fprintf(stdout, "%s: get position counter\n", tag);
		rcode = aptMotGetPosCounter(&position_counter, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get position counter rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//position_counter.channel = P_MOD_CHAN1;
		//position_counter.position = 0 * Ke;	// mm
		(void)fprintf(stdout, "%s: set position counter\n", tag);
		rcode = aptMotSetPosCounter(ftHandle, position_counter);
		(void)fprintf(stdout, "%s: set position counter rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_ENC) {
		ENCODER_COUNTER encoder_counter;
		(void)fprintf(stdout, "%s: get encoder counter\n", tag);
		rcode = aptMotGetEncCounter(&encoder_counter, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get encoder counter rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//encoder_counter.channel = P_MOD_CHAN1;
		//encoder_counter.encoder = 0 * Ke;	// mm
		(void)fprintf(stdout, "%s: set encoder counter\n", tag);
		rcode = aptMotSetEncCounter(ftHandle, encoder_counter);
		(void)fprintf(stdout, "%s: set encoder counter rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_VEL) {
		VEL_PARAMS vel_params;
		(void)fprintf(stdout, "%s: get vel params\n", tag);
		rcode = aptMotGetVelParams(&vel_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get vel params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//vel_params.channel = P_MOD_CHAN1;
		vel_params.min_vel	= 0.000 * Ke;	// mm/s
		vel_params.acc		= 0.010 * Ke;	// mm/s^2
		vel_params.max_vel	= 10.000 * 1342180;	// mm/s
		(void)fprintf(stdout, "%s: set vel params\n", tag);
		rcode = aptMotSetVelParams(ftHandle, vel_params);
		(void)fprintf(stdout, "%s: set vel params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_JOG) {
		JOG_PARAMS jog_params;

		if (verbose) {
			(void)fprintf(stdout, "%s: get jog params\n", tag);
		}
		rcode = aptMotGetJogParams(&jog_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		(void)fprintf(stdout, "%s: get jog params rcode (%d)\n", tag, rcode);
#endif
		if (rcode != 0) {
			return(-1);
		}
		if (verbose) {
			(void)aptJogParamsFormat(stdout, tag, jog_params);
		}

		// change parameters here
		//jog_params.channel = P_MOD_CHAN1;
		jog_params.mode = P_MOT_JOGCONTINUOUS;
		jog_params.mode = P_MOT_JOGSINGLESTEP;
		jog_params.step_size	= 1.000 * Ke;	// mm
		jog_params.step_size	= 0.010 * Ke;	// mm
		jog_params.min_vel		= 0.010 * Ke;	// mm/s
		jog_params.max_vel		= 1.000 * Ke;	// mm/s
		jog_params.stop_mode = P_MOT_STOP_PROFILED;
		jog_params.stop_mode = P_MOT_STOP_IMMEDIATE;

		if (verbose) {
			(void)fprintf(stdout, "%s: set jog params\n", tag);
		}
		rcode = aptMotSetJogParams(ftHandle, jog_params);
#ifdef DEBUG
		(void)fprintf(stdout, "%s: set jog params rcode (%d)\n", tag, rcode);
#endif
		if (rcode != 0) {
			return(-1);
		}

		if (verbose) {
			(void)fprintf(stdout, "%s: get jog params\n", tag);
		}
		rcode = aptMotGetJogParams(&jog_params, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		(void)fprintf(stdout, "%s: get jog params rcode (%d)\n", tag, rcode);
#endif
		if (rcode != 0) {
			return(-1);
		}
		if (verbose) {
			(void)aptJogParamsFormat(stdout, tag, jog_params);
		}
	}

	if (params_flags & PARAMS_GEN) {
		GEN_MOVE_PARAMS gen_move_params;
		(void)fprintf(stdout, "%s: get gen_move params\n", tag);
		rcode = aptMotGetGenMoveParams(&gen_move_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get gen_move params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//gen_move_params.channel = P_MOD_CHAN1;
		gen_move_params.backlash_distance = 0.010 * Ke;	// mm
		(void)fprintf(stdout, "%s: set gen_move params\n", tag);
		rcode = aptMotSetGenMoveParams(ftHandle, gen_move_params);
		(void)fprintf(stdout, "%s: set gen_move params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_REL) {
		MOVE_REL_PARAMS move_rel_params;
		(void)fprintf(stdout, "%s: get move rel params\n", tag);
		rcode = aptMotGetMoveRelParams(&move_rel_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get move rel params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//move_rel_params.channel = P_MOD_CHAN1;
		move_rel_params.relative_distance = 1.000 * Ke;	// mm
		(void)fprintf(stdout, "%s: set move rel params\n", tag);
		rcode = aptMotSetMoveRelParams(ftHandle, move_rel_params);
		(void)fprintf(stdout, "%s: set move rel params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_ABS) {
		MOVE_ABS_PARAMS move_abs_params;
		(void)fprintf(stdout, "%s: get move abs params\n", tag);
		rcode = aptMotGetMoveAbsParams(&move_abs_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get move abs params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//move_abs_params.channel = P_MOD_CHAN1;
		move_abs_params.absolute_position = 0.000 * Ke;	// mm
		(void)fprintf(stdout, "%s: set move abs params\n", tag);
		rcode = aptMotSetMoveAbsParams(ftHandle, move_abs_params);
		(void)fprintf(stdout, "%s: set move abs params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_HOME) {
		HOME_PARAMS home_params;
		(void)fprintf(stdout, "%s: get home params\n", tag);
		rcode = aptMotGetHomeParams(&home_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get home params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//home_params.channel = P_MOD_CHAN1;
		//home_params.direction = 0;		// ignored
		//home_params.limit_switch = 0;		// ignored
		home_params.vel = 20.000 * Ke;		// mm/s
		//home_params.offset_distance = 0;	// ignored
		(void)fprintf(stdout, "%s: set home params\n", tag);
		rcode = aptMotSetHomeParams(ftHandle, home_params);
		(void)fprintf(stdout, "%s: set home params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_LIMIT) {
		LIMIT_SWITCH_PARAMS limit_switch_params;
		(void)fprintf(stdout, "%s: get limit switch params\n", tag);
		rcode = aptMotGetLimitSwitchParams(&limit_switch_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get limit switch params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//limit_switch_params.channel = P_MOD_CHAN1;
		limit_switch_params.cw_hard_limit = P_MOT_SWITCHMAKES;
		limit_switch_params.ccw_hard_limit = P_MOT_SWITCHMAKES;
		limit_switch_params.cw_soft_limit = 40 * 134218;
		limit_switch_params.ccw_soft_limit = 30 * 134218;
		limit_switch_params.soft_limit_mode = P_MOT_LIMIT_STOP_PROFILED;
		(void)fprintf(stdout, "%s: set limit switch params\n", tag);
		rcode = aptMotSetLimitSwitchParams(ftHandle, limit_switch_params);
		(void)fprintf(stdout, "%s: set limit switch params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_PID) {
		DC_PID_PARAMS dc_pid_params;
		(void)fprintf(stdout, "%s: get dc pid params\n", tag);
		rcode = aptMotGetDcPidParams(&dc_pid_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get dc pid params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//dc_pid_params.channel = P_MOD_CHAN1;
		//dc_pid_params.Kp = 0;
		//dc_pid_params.Ki = 0;
		//dc_pid_params.Kd = 0;
		//dc_pid_params.ILimit = 0;
		dc_pid_params.filter_control = 0;
		dc_pid_params.filter_control |= P_MOT_PID_FILTER_KP;
		dc_pid_params.filter_control |= P_MOT_PID_FILTER_KI;
		dc_pid_params.filter_control |= P_MOT_PID_FILTER_KD;
		dc_pid_params.filter_control |= P_MOT_PID_FILTER_ILIMIT;
		(void)fprintf(stdout, "%s: set dc pid params\n", tag);
		rcode = aptMotSetDcPidParams(ftHandle, dc_pid_params);
		(void)fprintf(stdout, "%s: set dc pid params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_AV) {
		AV_MODES av_modes;
		(void)fprintf(stdout, "%s: get av modes\n", tag);
		rcode = aptMotGetAvModes(&av_modes, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get av modes rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//av_modes.channel = P_MOD_CHAN1;
		av_modes.mode_bits = 0;
		av_modes.mode_bits |= P_MOT_AV_MODE_IDENT;
		av_modes.mode_bits |= P_MOT_AV_MODE_LIMIT;
		av_modes.mode_bits |= P_MOT_AV_MODE_MOVING;
		(void)fprintf(stdout, "%s: set av modes\n", tag);
		rcode = aptMotSetAvModes(ftHandle, av_modes);
		(void)fprintf(stdout, "%s: set av modes rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_POT) {
		POT_PARAMS pot_params;
		(void)fprintf(stdout, "%s: get pot params\n", tag);
		rcode = aptMotGetPotParams(&pot_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get pot params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//pot_params.channel = P_MOD_CHAN1;
		//pot_params.wnd_0 = 0 * Ke;	// mm
		//pot_params.vel_1 = 0 * Ke;	// mm/ss
		//pot_params.wnd_1 = 0 * Ke;	// mm
		//pot_params.vel_2 = 0 * Ke;	// mm/ss
		//pot_params.wnd_2 = 0 * Ke;	// mm
		//pot_params.vel_3 = 0 * Ke;	// mm/ss
		//pot_params.wnd_3 = 0 * Ke;	// mm
		//pot_params.vel_4 = 0 * Ke;	// mm/ss
		(void)fprintf(stdout, "%s: set pot params\n", tag);
		rcode = aptMotSetPotParams(ftHandle, pot_params);
		(void)fprintf(stdout, "%s: set pot params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_BUTTON) {
		BUTTON_PARAMS button_params;
		(void)fprintf(stdout, "%s: get button params\n", tag);
		rcode = aptMotGetButtonParams(&button_params, ftHandle, P_MOD_CHAN1);
		(void)fprintf(stdout, "%s: get button params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
		// change parameters here
		//button_params.channel = P_MOD_CHAN1;
		button_params.pos_1 = 1.00 * Ke;	// mm
		button_params.pos_2 = 5.00 * Ke;	// mm
		button_params.timeout = 1000;	// ms
		(void)fprintf(stdout, "%s: set button params\n", tag);
		rcode = aptMotSetButtonParams(ftHandle, button_params);
		(void)fprintf(stdout, "%s: set button params rcode (%d)\n", tag, rcode);
		if (rcode != 0) {
			return(-1);
		}
	}

	if (params_flags & PARAMS_STATUS) {
		// nothing to set here
	}

	if (params_flags & PARAMS_STATUS) {
		// nothing to set here
	}

#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stdout, "%s: check for stray bytes\n", tag);
		npkt = aptRead(ftHandle, pkt, APT_PKT_MAX);
		if (npkt > 0) {
			fprintf(stdout, "%s: found %d bytes\n", tag, npkt);
			(void)aptFormat(stdout, argv[0], pkt, npkt);
		}
	}
#endif

	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
