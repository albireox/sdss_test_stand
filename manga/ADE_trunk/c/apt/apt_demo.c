/* file: $RCSfile: apt_demo.c,v $
** rcsid: $Id: apt_demo.c,v 1.3 2013/08/01 20:45:00 jwp Exp SDSSfiber $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_demo.c,v 1.3 2013/08/01 20:45:00 jwp Exp SDSSfiber $";
/*
** *******************************************************************
** $RCSfile: apt_demo.c,v $ - Thorlabs APT motion control demo program
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "apt.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0xfaf0;
static char *description = "APT DC Motor Controller";
static int baud = 115200;

#define MYBUFFERSIZE	(4096)
static BYTE gtx[MYBUFFERSIZE];		// global transmit buffer
static BYTE grx[MYBUFFERSIZE];		// global receive buffer

/**
 * Writes a string to the device.
 * We sleep for some time before querying the device.
 * The sleep time is passed in as milliseconds,
 * as this is what we see in the Sleep() calls
 * in the rfid1demoDlg.cpp code.
 * We convert to microseconds for the Unix usleep() call.
 */

static int
apt_write(FT_HANDLE ftHandle, BYTE *rx, BYTE *tx, int ntx, int delay)
{
	char *tag = "apt_write";
	FT_STATUS ftStatus;
	DWORD BytesWritten, AmountInRxQueue, BytesReturned;

	(void)fprintf(stdout, "%s: ntx: %d delay: %d\n", tag, ntx, delay);
	(void)aptFormat(stdout, "apt_write: write: ", tx, ntx);

	(void)fprintf(stdout, "%s: ask %d bytes out\n", tag, ntx);
	ftStatus = FT_Write(ftHandle, tx, ntx, &BytesWritten);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't write bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: got %d bytes out\n", tag, (int)BytesWritten);

	usleep(1000*delay);

	ftStatus = FT_GetQueueStatus(ftHandle, &AmountInRxQueue);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't get queue status (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: expect %d bytes in\n", tag, (int)AmountInRxQueue);

	ftStatus = FT_Read(ftHandle, rx, AmountInRxQueue, &BytesReturned);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: got %d bytes in\n", tag, (int)BytesReturned);

	(void)aptFormat(stdout, "apt_write: read: ", rx, AmountInRxQueue);

	return(BytesReturned);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int npkt;

	// Open the APT controller

	(void)fprintf(stdout, "%s: open the controller\n", tag);
	ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	if (ftHandle == NULL) {
		return(-1);
	}

	// Interact with the APT controller

	(void)fprintf(stdout, "%s: MOD_IDENTIFY\n", tag);
	npkt = 0;
	npkt = aptEncodeU2(gtx, npkt, MYBUFFERSIZE, MGMSG_MOD_IDENTIFY);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, 0);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, 0);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_DEST);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_HOST);
	npkt = apt_write(ftHandle, grx, gtx, npkt, 1000);
	(void)fprintf(stdout, "%s: apt_write returned %d\n", tag, npkt);

	(void)fprintf(stdout, "%s: HW_REQ_INFO\n", tag);
	npkt = 0;
	npkt = aptEncodeU2(gtx, npkt, MYBUFFERSIZE, MGMSG_HW_REQ_INFO);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, 0);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, 0);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_DEST);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_HOST);
	npkt = apt_write(ftHandle, grx, gtx, npkt, 1000);
	(void)fprintf(stdout, "%s: apt_write returned %d\n", tag, npkt);

	(void)fprintf(stdout, "%s: MOT_MOVE_HOME\n", tag);
	npkt = 0;
	npkt = aptEncodeU2(gtx, npkt, MYBUFFERSIZE, MGMSG_MOT_MOVE_HOME);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, P_MOD_CHAN1);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, 0);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_DEST);
	npkt = aptEncodeU1(gtx, npkt, MYBUFFERSIZE, APT_HOST);
	npkt = apt_write(ftHandle, grx, gtx, npkt, 1000);
	(void)fprintf(stdout, "%s: apt_write returned %d\n", tag, npkt);

	// Close the APT controller

	(void)fprintf(stdout, "%s: close the controller\n", tag);
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
