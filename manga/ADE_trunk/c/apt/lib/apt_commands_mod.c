/* file: $RCSfile: apt_commands_mod.c,v $
** rcsid: $Id: apt_commands_mod.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_commands_mod.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_commands_mod.c,v $ - Thorlabs APT Command Library
** *******************************************************************
*/

#include "apt.h"

int
aptModGetChannelEnableState(CHANNEL_ENABLE_STATE *channel_enable_state, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptModGetChannelEnableState";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptModReqChannelEnableState(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOD_GET_CHANENABLESTATE) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	channel_enable_state->channel = apt_pkt.param1;
	channel_enable_state->enable_state = apt_pkt.param2;

	return(0);
}

int
aptModSetChannelEnableState(FT_HANDLE ftHandle, CHANNEL_ENABLE_STATE channel_enable_state)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOD_SET_CHANENABLESTATE, channel_enable_state.channel);
	//apt_pkt.param1 = channel_enable_state.channel;
	apt_pkt.param2 = channel_enable_state.enable_state;

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}
