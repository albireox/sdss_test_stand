/* file: $RCSfile: apt_commands_rack.c,v $
** rcsid: $Id: apt_commands_rack.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_commands_rack.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_commands_rack.c,v $ - Thorlabs APT Command Library
** *******************************************************************
*/

#include "apt.h"

void
aptRackNoop(void)
{
	return;
}
