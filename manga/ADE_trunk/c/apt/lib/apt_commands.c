/* file: $RCSfile: apt_commands.c,v $
** rcsid: $Id: apt_commands.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_commands.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_commands.c,v $ - Thorlabs APT Command Library
** *******************************************************************
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "apt.h"

#undef DEBUG

/**
 * Initializes a new APT command to the given opcode and channel.
 */

APT_PKT
aptNewCmd(int opcode, int channel)
{
	APT_PKT apt_pkt;

	apt_pkt.opcode = opcode;
	apt_pkt.param1 = channel;
	apt_pkt.param2 = 0;
	apt_pkt.ndata = 0;
	apt_pkt.dest = APT_DEST;
	apt_pkt.host = APT_HOST;
	memset(apt_pkt.data, '0', APT_PKT_MAX);

	return(apt_pkt);
}

/**
 * Reads the requested number of bytes from the device queue.
 * We read any bytes, even if fewer than 6 rare waiting,
 * to keep the queue clear for the next try.
 */

int
aptRead(FT_HANDLE ftHandle, BYTE *pkt, int npkt)
{
	char *tag = "aptRead";
	FT_STATUS ftStatus;
	DWORD n_ask, n_got;
	DWORD AmountInRxQueue;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: read %d bytes\n", tag, npkt);
#endif

	ftStatus = FT_GetQueueStatus(ftHandle, &AmountInRxQueue);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't get queue status (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: AmountInRxQueue: %d\n", tag, AmountInRxQueue);
#endif

	n_ask = npkt;
	ftStatus = FT_Read(ftHandle, pkt, n_ask, &n_got);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read queue (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: n_got: %d\n", tag, n_got);
#endif

#ifdef DEBUG
	(void)aptFormat(stderr, tag, pkt, n_got);
#endif

	return(n_got);
}

/**
 * Writes bytes into the device queue.
 */

int
aptWrite(FT_HANDLE ftHandle, BYTE *pkt, int npkt)
{
	char *tag = "aptWrite";
	FT_STATUS ftStatus = FT_OK;
	DWORD n_ask, n_got;

#ifdef DEBUG
	(void)aptFormat(stderr, tag, pkt, npkt);
#endif

	n_ask = npkt;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ask %d bytes out\n", tag, n_ask);
#endif
	ftStatus = FT_Write(ftHandle, pkt, npkt, &n_got);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't write bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: got %d bytes out\n", tag, n_got);
#endif

	return(n_got);
}

/**
 * Shows the packet in human-readable form.
 */

int
aptFormat(FILE *fp, char *tag, BYTE *pkt, int npkt)
{
	int i;

	for (i = 0; i < npkt; i++) {
		(void)fprintf(fp, "%s: %2d 0x%02x", tag, i, pkt[i]);
		if (isprint(pkt[i])) {
			(void)fprintf(fp, " %c", pkt[i]);
		}
		(void)fprintf(fp, "\n");
	}

	return(0);
}

/**
 * Reads a packet (header + data) from the device.
 */

int
aptPktReadOld(APT_PKT *apt_pkt, FT_HANDLE ftHandle)
{
	char *tag = "aptPktRead";
	BYTE pkt[APT_PKT_MAX];
	int npkt;
	int rcode;

	// read the mandatory 6-byte header
#ifdef DEBUG
	(void)fprintf(stderr, "%s: read header\n", tag);
#endif
	npkt = aptRead(ftHandle, pkt, 6);
	if (npkt == 0) {
		(void)fprintf(stderr, "%s: timeout\n", tag);
		return(-1);
	} else if (npkt != 6) {
		(void)fprintf(stderr, "%s: too few bytes for header (%d != 6)\n", tag, npkt);
		return(-1);
	}
#ifdef DEBUG
	aptFormat(stderr, tag, pkt, npkt);
#endif

	rcode = aptPktHeaderDecode(apt_pkt, pkt, 6);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: opcode 0x%04x (%s) ndata %d\n",
		tag,
		apt_pkt->opcode,
		apt_opcode_name(apt_pkt->opcode),
		apt_pkt->ndata);
#endif

	if (apt_pkt->ndata > 0) {
		// read the data
		npkt = aptRead(ftHandle, apt_pkt->data, apt_pkt->ndata);
		if (npkt != apt_pkt->ndata) {
			(void)fprintf(stderr, "%s: too few bytes for data (%d != %d)\n", tag, npkt, apt_pkt->ndata);
			return(-1);
		}
	}

	return(0);
}

/**
 * Reads a packet (header + data) from the device.
 * We scan for a valid opcode,
 * then read the rest of the header.
 */

int
aptPktRead(APT_PKT *apt_pkt, FT_HANDLE ftHandle)
{
	char *tag = "aptPktRead";
	BYTE pkt[APT_PKT_MAX];
	int npkt;
	int rcode;
	int done = 0;

	// scan for an opcode
#ifdef DEBUG
	(void)fprintf(stderr, "%s: scan for an opcode\n", tag);
#endif
	while (!done) {
		npkt = aptRead(ftHandle, pkt, 2);
		if (npkt == 0) {
#ifdef DEBUG
			(void)fprintf(stderr, "%s: timeout\n", tag);
#endif
			return(-1);
		} else if (npkt != 2) {
			(void)fprintf(stderr, "%s: too few bytes for opcode (%d != 2)\n", tag, npkt);
			return(-1);
		}
#ifdef DEBUG
		aptFormat(stderr, tag, pkt, npkt);
#endif
		npkt = 0;
		npkt = aptDecodeU2(&apt_pkt->opcode, pkt, npkt, 2);
		if (apt_opcode_check(apt_pkt->opcode)) {
			(void)fprintf(stderr, "aptPktHeaderDecode: bad opcode 0x%04x (%s)\n",
					apt_pkt->opcode, apt_opcode_name(apt_pkt->opcode));
			// loop
		} else {
#ifdef DEBUG
			(void)fprintf(stderr, "aptPktHeaderDecode: got opcode 0x%04x (%s)\n",
					apt_pkt->opcode, apt_opcode_name(apt_pkt->opcode));
#endif
			done++;
		}
	}

	// read the rest of the mandatory 6-byte header
#ifdef DEBUG
	(void)fprintf(stderr, "%s: read the rest of the header\n", tag);
#endif
	npkt = aptRead(ftHandle, pkt+2, 4);
	if (npkt == 0) {
		(void)fprintf(stderr, "%s: timeout\n", tag);
		return(-1);
	} else if (npkt != 4) {
		(void)fprintf(stderr, "%s: too few bytes for header (%d != 4)\n", tag, npkt);
		return(-1);
	}
	npkt += 2;	// allow for the existing opcode
#ifdef DEBUG
	aptFormat(stderr, tag, pkt, npkt);
#endif

	rcode = aptPktHeaderDecode(apt_pkt, pkt, 6);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: opcode 0x%04x (%s) ndata %d\n",
			tag,
			apt_pkt->opcode,
			apt_opcode_name(apt_pkt->opcode),
			apt_pkt->ndata);
#endif

	if (apt_pkt->ndata > 0) {
		// read the data
		npkt = aptRead(ftHandle, apt_pkt->data, apt_pkt->ndata);
		if (npkt != apt_pkt->ndata) {
			(void)fprintf(stderr, "%s: too few bytes for opcode 0x%04x (%s) data (%d != %d)\n",
					tag, apt_pkt->opcode, apt_opcode_name(apt_pkt->opcode), npkt, apt_pkt->ndata);
			return(-1);
		}
	}

	return(0);
}

/**
 * Writes the packet (header + data) to the device.
 */

int
aptPktWrite(FT_HANDLE ftHandle, APT_PKT apt_pkt)
{
	char *tag = "aptPktWrite";
	BYTE pkt[APT_PKT_MAX];
	int n_ask, n_got;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: write opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
#endif

	n_ask = aptPktEncode(pkt, APT_PKT_MAX, apt_pkt);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ask %d bytes out\n", tag, n_ask);
#endif

	n_got = aptWrite(ftHandle, pkt, n_ask);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: got %d bytes out\n", tag, n_got);
#endif

	if (n_got != n_ask) {
		(void)fprintf(stderr, "%s: can't write apt_pkt\n", tag);
		return(-1);
	}

	return(0);
}

/**
 * Sends a generic APT no-arguments command.
 */

int
aptCmdSend(FT_HANDLE ftHandle, int opcode, int channel)
{
	char *tag = "aptCmdSend";
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(opcode, channel);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	if (rcode != 0) {
		(void)fprintf(stderr, "%s: can't send command 0x%04x (%s)\n", tag, opcode, apt_opcode_name(opcode));
		return(-1);
	}

	return(0);
}

/**
 * Initializes our APT motor controller.
 */

int
aptInitialize(FT_HANDLE ftHandle, int channel)
{
	int rcode;

	rcode = aptMotSuspendEndOfMoveMsgs(ftHandle, channel);
#ifdef DEBUG
	(void)fprintf(stdout, "%s: suspend end of move msgs rcode (%d)\n", tag, rcode);
#endif
	if (rcode != 0) {
		return(-1);
	}

	return(rcode);
}
