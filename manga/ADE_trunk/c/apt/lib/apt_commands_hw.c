/* file: $RCSfile: apt_commands_hw.c,v $
** rcsid: $Id: apt_commands_hw.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_commands_hw.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_commands_hw.c,v $ - Thorlabs APT Command Library
** *******************************************************************
*/

#include "apt.h"

int
aptHwGetInfo(HW_INFO *hw_info, FT_HANDLE ftHandle)
{
	char *tag = "aptHwGetInfo";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptHwReqInfo(ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_HW_GET_INFO) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 84) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptHwInfoDecode(hw_info, apt_pkt.data, apt_pkt.ndata);

	return(0);
}
