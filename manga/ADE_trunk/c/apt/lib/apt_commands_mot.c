/* file: $RCSfile: apt_commands_mot.c,v $
** rcsid: $Id: apt_commands_mot.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_commands_mot.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_commands_mot.c,v $ - Thorlabs APT Command Library
** *******************************************************************
*/

#include <unistd.h>

#include "apt.h"

/**
 * Gets the active position counter.
 */

int
aptMotGetPosCounter(POSITION_COUNTER *position_counter, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetPosCounter";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqPosCounter(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_POSCOUNTER) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptPositionCounterDecode(position_counter, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the active position counter.
 */

int
aptMotSetPosCounter(FT_HANDLE ftHandle, POSITION_COUNTER position_counter)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_POSCOUNTER, position_counter.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptPositionCounterEncode(apt_pkt.data, APT_PKT_MAX, position_counter);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the active encoder counter.
 */

int
aptMotGetEncCounter(ENCODER_COUNTER *encoder_counter, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetEncCounter";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqEncCounter(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_ENCCOUNTER) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptEncoderCounterDecode(encoder_counter, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the active encoder counter.
 */

int
aptMotSetEncCounter(FT_HANDLE ftHandle, ENCODER_COUNTER encoder_counter)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_ENCCOUNTER, encoder_counter.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptEncoderCounterEncode(apt_pkt.data, APT_PKT_MAX, encoder_counter);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the trapezoidal velocity parameters.
 */

int
aptMotGetVelParams(VEL_PARAMS *vel_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetVelParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqVelParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_VELPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 14) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptVelParamsDecode(vel_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the trapezoidal velocity parameters.
 */

int
aptMotSetVelParams(FT_HANDLE ftHandle, VEL_PARAMS vel_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_VELPARAMS, vel_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptVelParamsEncode(apt_pkt.data, APT_PKT_MAX, vel_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the jog parameters.
 */

int
aptMotGetJogParams(JOG_PARAMS *jog_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetJogParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqJogParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_JOGPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 22) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptJogParamsDecode(jog_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the jog parameters.
 */

int
aptMotSetJogParams(FT_HANDLE ftHandle, JOG_PARAMS jog_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_JOGPARAMS, jog_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptJogParamsEncode(apt_pkt.data, APT_PKT_MAX, jog_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the general move parameters.
 */

int
aptMotGetGenMoveParams(GEN_MOVE_PARAMS *gen_move_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetGenMoveParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqGenMoveParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_GENMOVEPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptGenMoveParamsDecode(gen_move_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the general move parameters.
 */

int
aptMotSetGenMoveParams(FT_HANDLE ftHandle, GEN_MOVE_PARAMS gen_move_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_GENMOVEPARAMS, gen_move_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptGenMoveParamsEncode(apt_pkt.data, APT_PKT_MAX, gen_move_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the relative move parameters.
 */

int
aptMotGetMoveRelParams(MOVE_REL_PARAMS *move_rel_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetMoveRelParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqMoveRelParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_MOVERELPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptMoveRelParamsDecode(move_rel_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the relative move parameters.
 */

int
aptMotSetMoveRelParams(FT_HANDLE ftHandle, MOVE_REL_PARAMS move_rel_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_MOVERELPARAMS, move_rel_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptMoveRelParamsEncode(apt_pkt.data, APT_PKT_MAX, move_rel_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the absolute move parameters.
 */

int
aptMotGetMoveAbsParams(MOVE_ABS_PARAMS *move_abs_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetMoveAbsParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqMoveAbsParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_MOVEABSPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptMoveAbsParamsDecode(move_abs_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the absolute move parameters.
 */

int
aptMotSetMoveAbsParams(FT_HANDLE ftHandle, MOVE_ABS_PARAMS move_abs_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_MOVEABSPARAMS, move_abs_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptMoveAbsParamsEncode(apt_pkt.data, APT_PKT_MAX, move_abs_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the home parameters.
 */

int
aptMotGetHomeParams(HOME_PARAMS *home_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetHomeParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqHomeParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_HOMEPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 14) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptHomeParamsDecode(home_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the home parameters.
 */

int
aptMotSetHomeParams(FT_HANDLE ftHandle, HOME_PARAMS home_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_HOMEPARAMS, home_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptHomeParamsEncode(apt_pkt.data, APT_PKT_MAX, home_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the limit switch parameters.
 */

int
aptMotGetLimitSwitchParams(LIMIT_SWITCH_PARAMS *limit_switch_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetLimitSwitchParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqLimitSwitchParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_LIMSWITCHPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 16) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptLimitSwitchParamsDecode(limit_switch_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the limit switch parameters.
 */

int
aptMotSetLimitSwitchParams(FT_HANDLE ftHandle, LIMIT_SWITCH_PARAMS limit_switch_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_LIMSWITCHPARAMS, limit_switch_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptLimitSwitchParamsEncode(apt_pkt.data, APT_PKT_MAX, limit_switch_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

int
aptMotMoveJog(FT_HANDLE ftHandle, int channel, int direction)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_MOVE_JOG, channel);

	apt_pkt.param2 = direction;

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

int
aptMotMoveVelocity(FT_HANDLE ftHandle, int channel, int direction)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_MOVE_VELOCITY, channel);

	apt_pkt.param2 = direction;

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

int
aptMotMoveStop(FT_HANDLE ftHandle, int channel, int stop_mode)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_MOVE_STOP, channel);

	apt_pkt.param2 = stop_mode;

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the DC PID parameters.
 */

int
aptMotGetDcPidParams(DC_PID_PARAMS *dc_pid_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetDcPidParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqDcPidParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_DCPIDPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 20) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptDcPidParamsDecode(dc_pid_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the DC PID parameters.
 */

int
aptMotSetDcPidParams(FT_HANDLE ftHandle, DC_PID_PARAMS dc_pid_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_DCPIDPARAMS, dc_pid_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptDcPidParamsEncode(apt_pkt.data, APT_PKT_MAX, dc_pid_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the AV modes.
 */

int
aptMotGetAvModes(AV_MODES *av_modes, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetAvModes";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqAvModes(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_AVMODES) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 4) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptAvModesDecode(av_modes, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the AV modes.
 */

int
aptMotSetAvModes(FT_HANDLE ftHandle, AV_MODES av_modes)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_AVMODES, av_modes.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptAvModesEncode(apt_pkt.data, APT_PKT_MAX, av_modes);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the potentiometer parameters.
 */

int
aptMotGetPotParams(POT_PARAMS *pot_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetPotParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqPotParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_POTPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 26) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptPotParamsDecode(pot_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the potentiometer parameters.
 */

int
aptMotSetPotParams(FT_HANDLE ftHandle, POT_PARAMS pot_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_POTPARAMS, pot_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptPotParamsEncode(apt_pkt.data, APT_PKT_MAX, pot_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the button parameters.
 */

int
aptMotGetButtonParams(BUTTON_PARAMS *button_params, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetButtonParams";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqButtonParams(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_BUTTONPARAMS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 16) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptButtonParamsDecode(button_params, apt_pkt.data, apt_pkt.ndata);

	return(0);
}

/**
 * Sets the button parameters.
 */

int
aptMotSetButtonParams(FT_HANDLE ftHandle, BUTTON_PARAMS button_params)
{
	APT_PKT apt_pkt;
	int rcode;

	apt_pkt = aptNewCmd(MGMSG_MOT_SET_BUTTONPARAMS, button_params.channel);

	apt_pkt.dest |= APT_DATA;
	apt_pkt.ndata = aptButtonParamsEncode(apt_pkt.data, APT_PKT_MAX, button_params);

	rcode = aptPktWrite(ftHandle, apt_pkt);

	return(rcode);
}

/**
 * Gets the dc status update.
 */

int
aptMotGetDcStatusUpdate(DC_STATUS_UPDATE *dc_status_update, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetDcStatusUpdate";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqDcStatusUpdate(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	(void)fprintf(stderr, "%s: sleep\n", tag);
	sleep(1);

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_DCSTATUSUPDATE) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 14) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptDcStatusUpdateDecode(dc_status_update, apt_pkt.data, apt_pkt.ndata);

	return(0);
}


/**
 * Gets the status bits.
 */

int
aptMotGetStatusBits(STATUS_BITS *status_bits, FT_HANDLE ftHandle, int channel)
{
	char *tag = "aptMotGetStatusBits";
	APT_PKT apt_pkt;
	int rcode;

	rcode = aptMotReqStatusBits(ftHandle, channel);

	if (rcode != 0) {
		return(-1);
	}

	rcode = aptPktRead(&apt_pkt, ftHandle);

	if (rcode != 0) {
		return(-1);
	}

	if (apt_pkt.opcode != MGMSG_MOT_GET_STATUSBITS) {
		(void)fprintf(stderr, "%s: bad opcode 0x%04x (%s)\n", tag, apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
		return(-1);
	}

	if (apt_pkt.ndata != 6) {
		(void)fprintf(stderr, "%s: bad data length (%d)\n", tag, apt_pkt.ndata);
		return(-1);
	}

	(void)aptStatusBitsDecode(status_bits, apt_pkt.data, apt_pkt.ndata);

	return(0);
}
