/* file: $RCSfile: apt_block.c,v $
** rcsid: $Id: apt_block.c,v 1.2 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_block.c,v 1.2 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_block.c,v $ - block on a move, return when it's done
** *******************************************************************
*/

#include <time.h>
#include "apt.h"

#undef DEBUG

int
apt_block(char *tag, FT_HANDLE ftHandle, int timeout)
{
	APT_PKT apt_pkt;
	STATUS_BITS status_bits;
	int done = 0;
	int rcode = -1;
	time_t t0, t; int dt;

	t0 = time(NULL);

	while (!done) {

		t = time(NULL);
		dt = t - t0;

		// first check for a move completed message
		rcode = aptPktRead(&apt_pkt, ftHandle);
#ifdef DEBUG
		(void)fprintf(stderr, "apt_block: %s: check move rcode %d\n", tag, rcode);
#endif
		if (rcode == 0) {
			int opcode = apt_pkt.opcode;
			if (apt_opcode_check(opcode) == 0) {
#ifdef DEBUG
				(void)aptPktFormat(stderr, apt_pkt);
#endif
				switch (opcode) {
				case MGMSG_MOT_MOVE_HOMED:
				case MGMSG_MOT_MOVE_COMPLETED:
				case MGMSG_MOT_MOVE_STOPPED:
					(void)fprintf(stdout, "apt_block: %s: opcode 0x%04x (%s)\n", tag, opcode, apt_opcode_name(opcode));
					rcode = 0;
					done++;
					break;
				default:
					(void)fprintf(stderr, "apt_block: %s: unexpected opcode 0x%04x (%s)\n", tag, opcode, apt_opcode_name(opcode));
					break;
				}
			} else {
				(void)fprintf(stderr, "apt_block: %s: bad opcode 0x%04x (%s)\n", tag, opcode, apt_opcode_name(opcode));
			}
		}
		// check the motion status flags
		rcode = aptMotGetStatusBits(&status_bits, ftHandle, P_MOD_CHAN1);
#ifdef DEBUG
		(void)fprintf(stderr, "apt_block: %s: check status rcode %d\n", tag, rcode);
#endif
		if (rcode == 0) {
			if (status_bits.status_bits & P_MOT_DC_HOMING) {
				(void)fprintf(stderr, "apt_block: %s: still homing after %d s\n", tag, dt);
			} else if (status_bits.status_bits & P_MOT_DC_MOVING_FWD) {
				(void)fprintf(stderr, "apt_block: %s: still moving fwd after %d s\n", tag, dt);
			} else if (status_bits.status_bits & P_MOT_DC_MOVING_REV) {
				(void)fprintf(stderr, "apt_block: %s: still moving rev after %d s\n", tag, dt);
			} else {
				(void)fprintf(stderr, "apt_block: %s: motion has stopped after %d s\n", tag, dt);
				done++;
			}
		}
		t = time(NULL);
		dt = t - t0;
#ifdef DEBUG
		(void)fprintf(stdout, "apt_block: %s: timeout %d of %d\n", tag, dt, timeout);
#endif
		if (dt > timeout) {
			(void)fprintf(stderr, "apt_block: %s: move block timed out\n", tag);
			rcode = -1;
			done++;
		}
	}

	return(rcode);
}
