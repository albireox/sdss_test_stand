/* file: $RCSfile: apt_codec.c,v $
** rcsid: $Id: apt_codec.c,v 1.3 2013/08/01 20:43:02 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: apt_codec.c,v 1.3 2013/08/01 20:43:02 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: apt_codec.c,v $ - encode and decode numbers
** *******************************************************************
*/

#include <string.h>
#include "apt.h"

#undef DEBUG

/*----------------------------------------------------------------------*/

/* encode an unsigned 1-byte value */
int
aptEncodeU1(BYTE *apt_pkt, int ix, int pmax, int ival)
{

	if (ix < pmax) {
		ival = RANGE(ival, 0, 0xff);
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
	}

	return(ix);
}

int
aptDecodeU1(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-1) {
		ival = apt_pkt[ix++];
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

/* encode an unsigned 2-byte value */
int
aptEncodeU2(BYTE *apt_pkt, int ix, int pmax, int ival)
{

	if (ix <= pmax-2) {
		ival = RANGE(ival, 0, 0xffff);
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >>  8) & 0xff);
	}

	return(ix);
}

int
aptDecodeU2(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-2) {
		ival |= apt_pkt[ix++];
		ival |= apt_pkt[ix++] <<  8;
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

/* encode an unsigned 4-byte value */
int
aptEncodeU4(BYTE *apt_pkt, int ix, int pmax, int ival)
{

	if (ix <= pmax-4) {
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >>  8) & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >> 16) & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >> 24) & 0xff);
	}

	return(ix);
}

int
aptDecodeU4(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-4) {
		ival |= apt_pkt[ix++];
		ival |= apt_pkt[ix++] <<  8;
		ival |= apt_pkt[ix++] << 16;
		ival |= apt_pkt[ix++] << 24;
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

/* encode a signed 1-byte value */
int
aptEncodeS1(BYTE *apt_pkt, int ix, int pmax, int ival)
{

	if (ix <= pmax-1) {
		ival = RANGE(ival, 0x80, 0x7f);
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
	}

	return(ix);
}

int
aptDecodeS1(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-1) {
		ival = apt_pkt[ix++];
		if (ival & 0x80) {
			ival -= 0x100;
		}
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

/* encode a signed 2-byte value */
int
aptEncodeS2(BYTE *apt_pkt, int ix, int pmax, int ival)
{

	if (ix <= pmax-2) {
		ival = RANGE(ival, 0x8000, 0x7fff);
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >>  8) & 0xff);
	}

	return(ix);
}

int
aptDecodeS2(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-2) {
		ival |= apt_pkt[ix++];
		ival |= apt_pkt[ix++] <<  8;
		if (ival & 0x8000) {
			ival -= 0x10000;
		}
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

/* encode a signed 4-byte value */
int
aptEncodeS4(BYTE *apt_pkt, int ix, int pmax, int ival)
{
	if (ix <= pmax-4) {
		apt_pkt[ix++] = (BYTE)(ival & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >>  8) & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >> 16) & 0xff);
		apt_pkt[ix++] = (BYTE)((ival >> 24) & 0xff);
	}

	return(ix);
}

int
aptDecodeS4(int *pival, BYTE *apt_pkt, int ix, int pmax)
{
	int ival = 0;

	if (ix <= pmax-4) {
		ival |= apt_pkt[ix++];
		ival |= apt_pkt[ix++] <<  8;
		ival |= apt_pkt[ix++] << 16;
		ival |= apt_pkt[ix++] << 24;
	}

	*pival = ival;

	return(ix);
}

/*----------------------------------------------------------------------*/

int
aptPktHeaderDecode(APT_PKT *apt_pkt, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&apt_pkt->opcode, pkt, npkt, pmax);
	if (apt_opcode_check(apt_pkt->opcode)) {
		(void)fprintf(stderr, "aptPktHeaderDecode: bad opcode 0x%04x (%s)\n", apt_pkt->opcode, apt_opcode_name(apt_pkt->opcode));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "aptPktHeaderDecode: decode opcode 0x%04x (%s)\n", apt_pkt->opcode, apt_opcode_name(apt_pkt->opcode));
#endif
	if (pkt[4] & APT_DATA) {
		npkt = aptDecodeU2(&apt_pkt->ndata, pkt, npkt, pmax);
		apt_pkt->param1 = 0;
		apt_pkt->param2 = 0;
		npkt = aptDecodeU1(&apt_pkt->dest, pkt, npkt, pmax);
		apt_pkt->dest &= (~APT_DATA);	// nuke the data flag
		npkt = aptDecodeU1(&apt_pkt->host, pkt, npkt, pmax);
	} else {
		npkt = aptDecodeU1(&apt_pkt->param1, pkt, npkt, pmax);
		npkt = aptDecodeU1(&apt_pkt->param2, pkt, npkt, pmax);
		apt_pkt->ndata = 0;
		npkt = aptDecodeU1(&apt_pkt->dest, pkt, npkt, pmax);
		npkt = aptDecodeU1(&apt_pkt->host, pkt, npkt, pmax);
	}

	// a fix for the APT TDC001 firmware bug
	if (apt_pkt->opcode == MGMSG_MOD_GET_CHANENABLESTATE) {
		(void)fprintf(stderr, "aptPktHeaderDecode: ask for errant byte\n");
		apt_pkt->ndata = 1;	// it emits an errant byte
	}

#ifdef DEBUG
	(void)fprintf(stderr, "aptPktHeaderDecode: %d bytes\n", npkt);
#endif

	return(0);
}


int
aptPktDecode(APT_PKT *apt_pkt, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptPktHeaderDecode(apt_pkt, pkt, pmax);
	if (apt_pkt->ndata > 0) {
		(void)memcpy(apt_pkt->data, pkt+npkt, apt_pkt->ndata);
		npkt += apt_pkt->ndata;
	}

	return(npkt);
}

int
aptPktEncode(BYTE *pkt, int pmax, APT_PKT apt_pkt)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, apt_pkt.opcode);
	if (apt_pkt.ndata > 0) {
		npkt = aptEncodeU2(pkt, npkt, pmax, apt_pkt.ndata);
		npkt = aptEncodeU1(pkt, npkt, pmax, APT_DEST | APT_DATA);
		npkt = aptEncodeU1(pkt, npkt, pmax, APT_HOST);
		(void)memcpy(pkt+npkt, apt_pkt.data, apt_pkt.ndata);
		npkt += apt_pkt.ndata;
	} else {
		npkt = aptEncodeU1(pkt, npkt, pmax, apt_pkt.param1);
		npkt = aptEncodeU1(pkt, npkt, pmax, apt_pkt.param2);
		npkt = aptEncodeU1(pkt, npkt, pmax, APT_DEST);
		npkt = aptEncodeU1(pkt, npkt, pmax, APT_HOST);
	}

	return(npkt);
}

int
aptPktFormat(FILE *fp, char *tag, APT_PKT apt_pkt)
{
	(void)fprintf(fp, "apt_pkt.opcode: 0x%04x (%s)\n", apt_pkt.opcode, apt_opcode_name(apt_pkt.opcode));
	if (apt_pkt.ndata > 0) {
		(void)fprintf(fp, " apt_pkt.ndata: %8d\n", apt_pkt.ndata);
		(void)aptFormat(fp, "aptPktFormat", apt_pkt.data, apt_pkt.ndata);
	} else {
		(void)fprintf(fp, "apt_pkt.param1: %8d\n", apt_pkt.param1);
		(void)fprintf(fp, "apt_pkt.param2: %8d\n", apt_pkt.param2);
	}
	(void)fprintf(fp, "  apt_pkt.dest: 0x%02x\n", apt_pkt.dest);
	(void)fprintf(fp, "  apt_pkt.host: 0x%02x\n", apt_pkt.host);

	return(0);
}

char *
aptChannelEnableStateString(int enable_state)
{
	char *p;
	switch (enable_state) {
	case P_MOD_CHAN_ENABLE:
		p = "Enabled";
		break;
	case P_MOD_CHAN_DISABLE:
		p = "Disabled";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

int
aptChannelEnableStateFormat(FILE *fp, char *tag, CHANNEL_ENABLE_STATE channel_enable_state)
{
	(void)fprintf(fp, "%40s: %8d\n", "channel_enable_state.channel", channel_enable_state.channel);
	(void)fprintf(fp, "%40s: %8d (%s)\n", "channel_enable_state.enable_state",
			channel_enable_state.enable_state,
			aptChannelEnableStateString(channel_enable_state.enable_state));

	return(0);
}

int
aptHwInfoDecode(HW_INFO *hw_info, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU4(&hw_info->serial_number, pkt, npkt, pmax);
	(void)memcpy(hw_info->model_number, pkt+npkt, 8);
	npkt += 8;
	npkt = aptDecodeU2(&hw_info->hw_type, pkt, npkt, pmax);
	npkt = aptDecodeU4(&hw_info->sw_version, pkt, npkt, pmax);
	(void)memcpy(hw_info->notes, pkt+npkt, 64);
	npkt += 64;
	npkt = aptDecodeU2(&hw_info->num_channels, pkt, npkt, pmax);

	return(npkt);
}

int
aptHwInfoFormat(FILE *fp, char *tag, HW_INFO hw_info)
{
	(void)fprintf(fp, "%40s: 0x%08x\n", "hw_info.serial_number", hw_info.serial_number);
	(void)fprintf(fp, "%40s: %8.8s\n", "hw_info.model_number", hw_info.model_number);
	(void)fprintf(fp, "%40s: %8d\n", "hw_info.hw_type", hw_info.hw_type);
	(void)fprintf(fp, "%40s: 0x%08x\n", "hw_info.software_version", hw_info.sw_version);
	(void)fprintf(fp, "%40s: %64.64s\n", "hw_info.notes", hw_info.notes);
	(void)fprintf(fp, "%40s: %8d\n", "hw_info.num_channels", hw_info.num_channels);

	return(0);
}

int
aptPositionCounterDecode(POSITION_COUNTER *position_counter, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&position_counter->channel, pkt, npkt, pmax);
	npkt = aptDecodeS4(&position_counter->position, pkt, npkt, pmax);

	return(npkt);
}

int
aptPositionCounterEncode(BYTE *pkt, int pmax, POSITION_COUNTER position_counter)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, position_counter.channel);
	npkt = aptEncodeS4(pkt, npkt, pmax, position_counter.position);

	return(npkt);
}

int
aptPositionCounterFormat(FILE *fp, char *tag, POSITION_COUNTER position_counter)
{
	(void)fprintf(fp, "%40s: channel %d position %8d (%8.3f mm)\n",
			tag,
			position_counter.channel,
			position_counter.position,
			position_counter.position/Ke);

	return(0);
}

int
aptEncoderCounterDecode(ENCODER_COUNTER *encoder_counter, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&encoder_counter->channel, pkt, npkt, pmax);
	npkt = aptDecodeS4(&encoder_counter->encoder, pkt, npkt, pmax);

	return(npkt);
}

int
aptEncoderCounterEncode(BYTE *pkt, int pmax, ENCODER_COUNTER encoder_counter)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, encoder_counter.channel);
	npkt = aptEncodeS4(pkt, npkt, pmax, encoder_counter.encoder);

	return(npkt);
}

int
aptEncoderCounterFormat(FILE *fp, char *tag, ENCODER_COUNTER encoder_counter)
{
	(void)fprintf(fp, "%40s: channel %d  encoder %8d (%8.3f mm)\n",
			"encoder_counter",
			encoder_counter.channel,
			encoder_counter.encoder,
			encoder_counter.encoder/Ke);

	return(0);
}

int
aptVelParamsDecode(VEL_PARAMS *vel_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&vel_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&vel_params->min_vel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&vel_params->acc,     pkt, npkt, pmax);
	npkt = aptDecodeU4(&vel_params->max_vel, pkt, npkt, pmax);

	return(npkt);
}

int
aptVelParamsEncode(BYTE *pkt, int pmax, VEL_PARAMS vel_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, vel_params.channel);
	npkt = aptEncodeU4(pkt, npkt, pmax, vel_params.min_vel);
	npkt = aptEncodeU4(pkt, npkt, pmax, vel_params.acc);
	npkt = aptEncodeU4(pkt, npkt, pmax, vel_params.max_vel);

	return(npkt);
}

int
aptVelParamsFormat(FILE *fp, char *tag, VEL_PARAMS vel_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "vel_params.channel", vel_params.channel);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n", "vel_params.min_vel", vel_params.min_vel, vel_params.min_vel/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s^2)\n", "vel_params.acc", vel_params.acc, vel_params.acc/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n", "vel_params.max_vel", vel_params.max_vel, vel_params.max_vel/1342180.0

	);

	return(0);
}

int
aptJogParamsDecode(JOG_PARAMS *jog_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&jog_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&jog_params->mode, pkt, npkt, pmax);
	npkt = aptDecodeU4(&jog_params->step_size, pkt, npkt, pmax);
	npkt = aptDecodeU4(&jog_params->min_vel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&jog_params->acc, pkt, npkt, pmax);
	npkt = aptDecodeU4(&jog_params->max_vel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&jog_params->stop_mode, pkt, npkt, pmax);

	return(npkt);
}

int
aptJogParamsEncode(BYTE *pkt, int pmax, JOG_PARAMS jog_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, jog_params.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, jog_params.mode);
	npkt = aptEncodeU4(pkt, npkt, pmax, jog_params.step_size);
	npkt = aptEncodeU4(pkt, npkt, pmax, jog_params.min_vel);
	npkt = aptEncodeU4(pkt, npkt, pmax, jog_params.acc);
	npkt = aptEncodeU4(pkt, npkt, pmax, jog_params.max_vel);
	npkt = aptEncodeU2(pkt, npkt, pmax, jog_params.stop_mode);

	return(npkt);
}

char *
aptJogParamsModeString(int mode)
{
	char *p;

	switch (mode) {
	case P_MOT_JOGCONTINUOUS:
		p = "Continuous";
		break;
	case P_MOT_JOGSINGLESTEP:
		p = "SingleStep";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

char *
aptJogParamsStopModeString(int stop_mode)
{
	char *p;

	switch (stop_mode) {
	case P_MOT_STOP_IMMEDIATE:
		p = "Immediate";
		break;
	case P_MOT_STOP_PROFILED:
		p = "Profiled";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

int
aptJogParamsFormat(FILE *fp, char *tag, JOG_PARAMS jog_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "jog_params.channel", jog_params.channel);
	(void)fprintf(fp, "%40s: %8d (%s)\n", "jog_params.mode",
			jog_params.mode,
			aptJogParamsModeString(jog_params.mode));
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "jog_params.step_size",
			jog_params.step_size,
			jog_params.step_size/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s^2)\n", "jog_params.min_vel",
			jog_params.acc,
			jog_params.acc/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n", "jog_params.max_vel",
			jog_params.max_vel,
			jog_params.max_vel/Ke);
	(void)fprintf(fp, "%40s: %8d (%s)\n", "jog_params.stop_mode",
			jog_params.stop_mode,
			aptJogParamsStopModeString(jog_params.stop_mode));

	return(0);
}

int
aptGenMoveParamsDecode(GEN_MOVE_PARAMS *gen_move_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&gen_move_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeS4(&gen_move_params->backlash_distance, pkt, npkt, pmax);

	return(npkt);
}

int
aptGenMoveParamsEncode(BYTE *pkt, int pmax, GEN_MOVE_PARAMS gen_move_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, gen_move_params.channel);
	npkt = aptEncodeS4(pkt, npkt, pmax, gen_move_params.backlash_distance);

	return(npkt);
}

int
aptGenMoveParamsFormat(FILE *fp, char *tag, GEN_MOVE_PARAMS gen_move_params)
{
	(void)fprintf(fp, "%40s: channel %d position %8d (%8.3f mm)\n",
			"gen_move_param",
			gen_move_params.channel,
			gen_move_params.backlash_distance,
			gen_move_params.backlash_distance/Ke);

	return(0);
}

int
aptMoveRelParamsDecode(MOVE_REL_PARAMS *move_rel_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&move_rel_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeS4(&move_rel_params->relative_distance, pkt, npkt, pmax);

	return(npkt);
}

int
aptMoveRelParamsEncode(BYTE *pkt, int pmax, MOVE_REL_PARAMS move_rel_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, move_rel_params.channel);
	npkt = aptEncodeS4(pkt, npkt, pmax, move_rel_params.relative_distance);

	return(npkt);
}

int
aptMoveRelParamsFormat(FILE *fp, char *tag, MOVE_REL_PARAMS move_rel_params)
{
	(void)fprintf(fp, "%40s: channel %d position %8d (%8.3f mm)\n",
			"move_rel_params",
			move_rel_params.channel,
			move_rel_params.relative_distance,
			move_rel_params.relative_distance/Ke);

	return(0);
}

int
aptMoveAbsParamsDecode(MOVE_ABS_PARAMS *move_abs_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&move_abs_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeS4(&move_abs_params->absolute_position, pkt, npkt, pmax);

	return(npkt);
}

int
aptMoveAbsParamsEncode(BYTE *pkt, int pmax, MOVE_ABS_PARAMS move_abs_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, move_abs_params.channel);
	npkt = aptEncodeS4(pkt, npkt, pmax, move_abs_params.absolute_position);

	return(npkt);
}

int
aptMoveAbsParamsFormat(FILE *fp, char *tag, MOVE_ABS_PARAMS move_abs_params)
{
	(void)fprintf(fp, "%40s: channel %d position %8d (%8.3f mm)\n",
			"move_abs_params",
			move_abs_params.channel,
			move_abs_params.absolute_position,
			move_abs_params.absolute_position/Ke);

	return(0);
}

int
aptHomeParamsDecode(HOME_PARAMS *home_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&home_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&home_params->direction, pkt, npkt, pmax);
	npkt = aptDecodeU2(&home_params->limit_switch, pkt, npkt, pmax);
	npkt = aptDecodeU4(&home_params->vel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&home_params->offset_distance, pkt, npkt, pmax);

	return(npkt);
}

int
aptHomeParamsEncode(BYTE *pkt, int pmax, HOME_PARAMS home_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, home_params.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, home_params.direction);
	npkt = aptEncodeU2(pkt, npkt, pmax, home_params.limit_switch);
	npkt = aptEncodeU4(pkt, npkt, pmax, home_params.vel);
	npkt = aptEncodeU4(pkt, npkt, pmax, home_params.offset_distance);

	return(npkt);
}

int
aptHomeParamsFormat(FILE *fp, char *tag, HOME_PARAMS home_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "home_params.channel", home_params.channel);
	(void)fprintf(fp, "%40s: %8d (Ignored, Always Forward)\n", "home_params.direction", home_params.direction);
	(void)fprintf(fp, "%40s: %8d (Ignored, Not Used)\n", "home_params.limit_switch", home_params.limit_switch);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n", "home_params.vel",
			home_params.vel,
			home_params.vel/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "home_params.offset_distance",
			home_params.offset_distance,
			home_params.offset_distance/Ke);

	return(0);
}

int
aptLimitSwitchParamsDecode(LIMIT_SWITCH_PARAMS *limit_switch_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&limit_switch_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&limit_switch_params->cw_hard_limit, pkt, npkt, pmax);
	npkt = aptDecodeU2(&limit_switch_params->ccw_hard_limit, pkt, npkt, pmax);
	npkt = aptDecodeU4(&limit_switch_params->cw_soft_limit, pkt, npkt, pmax);
	npkt = aptDecodeU4(&limit_switch_params->ccw_soft_limit, pkt, npkt, pmax);
	npkt = aptDecodeU2(&limit_switch_params->soft_limit_mode, pkt, npkt, pmax);

	return(npkt);
}

int
aptLimitSwitchParamsEncode(BYTE *pkt, int pmax, LIMIT_SWITCH_PARAMS limit_switch_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, limit_switch_params.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, limit_switch_params.cw_hard_limit);
	npkt = aptEncodeU2(pkt, npkt, pmax, limit_switch_params.ccw_hard_limit);
	npkt = aptEncodeU4(pkt, npkt, pmax, limit_switch_params.cw_soft_limit);
	npkt = aptEncodeU4(pkt, npkt, pmax, limit_switch_params.ccw_soft_limit);
	npkt = aptEncodeU2(pkt, npkt, pmax, limit_switch_params.soft_limit_mode);

	return(npkt);
}

char *
aptLimitParamsHardLimitOpString(int hard_limit)
{
	char *p;

	switch (hard_limit) {
	case P_MOT_SWITCHIGNORE:
		p = "Ignore";
		break;
	case P_MOT_SWITCHMAKES:
		p = "Switch Makes";
		break;
	case P_MOT_SWITCHBREAKS:
		p = "Switch Breaks";
		break;
	case P_MOT_SWITCHMAKES_HOME:
		p = "Switch Makes (Home)";
		break;
	case P_MOT_SWITCHBREAKS_HOME:
		p = "Switch Breaks (Home)";
		break;
	case P_MOT_SWITCHINDEX_HOME:
		p = "Index (Home)";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

char *
aptLimitParamsSoftLimitModeString(int soft_limit_mode)
{
	char *p;

	switch (soft_limit_mode) {
	case P_MOT_LIMIT_IGNORE:
		p = "Ignore";
		break;
	case P_MOT_LIMIT_STOP_IMMEDIATE:
		p = "Immediate";
		break;
	case P_MOT_LIMIT_STOP_PROFILED:
		p = "Profiled";
		break;
	case P_MOT_LIMIT_ROTATION_STAGE:
		p = "Rotation Stage";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

int
aptLimitSwitchParamsFormat(FILE *fp, char *tag, LIMIT_SWITCH_PARAMS limit_switch_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "limit_switch_params.channel", limit_switch_params.channel);
	(void)fprintf(fp, "%40s: 0x%02x (%s)\n", "limit_switch_params.cw_hard_limit",
			limit_switch_params.cw_hard_limit,
			aptLimitParamsHardLimitOpString(limit_switch_params.cw_hard_limit));
	(void)fprintf(fp, "%40s: 0x%02x (%s)\n", "limit_switch_params.ccw_hard_limit",
			limit_switch_params.ccw_hard_limit,
			aptLimitParamsHardLimitOpString(limit_switch_params.ccw_hard_limit));
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "limit_switch_params.cw_soft_limit",
			limit_switch_params.cw_soft_limit,
			limit_switch_params.cw_soft_limit/134218.0);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "limit_switch_params.ccw_soft_limit",
			limit_switch_params.ccw_soft_limit,
			limit_switch_params.ccw_soft_limit/134218.0);
	(void)fprintf(fp, "%40s: %8d (%s)\n", "limit_switch_params.soft_limit_mode",
			limit_switch_params.soft_limit_mode,
			aptLimitParamsSoftLimitModeString(limit_switch_params.soft_limit_mode));

	return(0);
}

int
aptDcPidParamsDecode(DC_PID_PARAMS *dc_pid_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&dc_pid_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_pid_params->Kp, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_pid_params->Ki, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_pid_params->Kd, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_pid_params->ILimit, pkt, npkt, pmax);
	npkt = aptDecodeU2(&dc_pid_params->filter_control, pkt, npkt, pmax);

	return(npkt);
}

int
aptDcPidParamsEncode(BYTE *pkt, int pmax, DC_PID_PARAMS dc_pid_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, dc_pid_params.channel);
	npkt = aptEncodeU4(pkt, npkt, pmax, dc_pid_params.Kp);
	npkt = aptEncodeU4(pkt, npkt, pmax, dc_pid_params.Ki);
	npkt = aptEncodeU4(pkt, npkt, pmax, dc_pid_params.Kd);
	npkt = aptEncodeU4(pkt, npkt, pmax, dc_pid_params.ILimit);
	npkt = aptEncodeU2(pkt, npkt, pmax, dc_pid_params.filter_control);

	return(npkt);
}

int
aptDcPidParamsFormat(FILE *fp, char *tag, DC_PID_PARAMS dc_pid_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "dc_pid_params.channel", dc_pid_params.channel);
	(void)fprintf(fp, "%40s: %8d\n", "dc_pid_params.Kp", dc_pid_params.Kp);
	(void)fprintf(fp, "%40s: %8d\n", "dc_pid_params.Ki", dc_pid_params.Ki);
	(void)fprintf(fp, "%40s: %8d\n", "dc_pid_params.Kd", dc_pid_params.Kd);
	(void)fprintf(fp, "%40s: %8d\n", "dc_pid_params.ILimit", dc_pid_params.ILimit);
	(void)fprintf(fp, "%40s: 0x%02x\n", "dc_pid_params.filter_control", dc_pid_params.filter_control);
	(void)fprintf(fp, "%40s: %d\n", "dc_pid_params.filter_control.Kp",	(dc_pid_params.filter_control&P_MOT_PID_FILTER_KP)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "dc_pid_params.filter_control.Ki",	(dc_pid_params.filter_control&P_MOT_PID_FILTER_KI)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "dc_pid_params.filter_control.Kd",	(dc_pid_params.filter_control&P_MOT_PID_FILTER_KD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "dc_pid_params.filter_control.ILimit",	(dc_pid_params.filter_control&P_MOT_PID_FILTER_ILIMIT)?1:0);


	return(0);
}

int
aptAvModesDecode(AV_MODES *av_modes, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&av_modes->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&av_modes->mode_bits, pkt, npkt, pmax);

	return(npkt);
}

int
aptAvModesEncode(BYTE *pkt, int pmax, AV_MODES av_modes)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, av_modes.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, av_modes.mode_bits);

	return(npkt);
}

int
aptAvModesFormat(FILE *fp, char *tag, AV_MODES av_modes)
{
	(void)fprintf(fp, "%40s: %d\n", "av_modes.channel", av_modes.channel);
	(void)fprintf(fp, "%40s: 0x%02x\n", "av_modes.mode_bits", av_modes.mode_bits);
	(void)fprintf(fp, "%40s: %d\n", "av_modes.mode_bits.ident",		(av_modes.mode_bits&P_MOT_AV_MODE_IDENT)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "av_modes.mode_bits.limit",		(av_modes.mode_bits&P_MOT_AV_MODE_LIMIT)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "av_modes.mode_bits.moving",	(av_modes.mode_bits&P_MOT_AV_MODE_MOVING)?1:0);

	return(0);
}

int
aptPotParamsDecode(POT_PARAMS *pot_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&pot_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&pot_params->wnd_0, pkt, npkt, pmax);
	npkt = aptDecodeU4(&pot_params->vel_1, pkt, npkt, pmax);
	npkt = aptDecodeU2(&pot_params->wnd_1, pkt, npkt, pmax);
	npkt = aptDecodeU4(&pot_params->vel_2, pkt, npkt, pmax);
	npkt = aptDecodeU2(&pot_params->wnd_2, pkt, npkt, pmax);
	npkt = aptDecodeU4(&pot_params->vel_3, pkt, npkt, pmax);
	npkt = aptDecodeU2(&pot_params->wnd_3, pkt, npkt, pmax);
	npkt = aptDecodeU4(&pot_params->vel_4, pkt, npkt, pmax);


	return(npkt);
}

int
aptPotParamsEncode(BYTE *pkt, int pmax, POT_PARAMS pot_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, pot_params.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, pot_params.wnd_0);
	npkt = aptEncodeU4(pkt, npkt, pmax, pot_params.vel_1);
	npkt = aptEncodeU2(pkt, npkt, pmax, pot_params.wnd_1);
	npkt = aptEncodeU4(pkt, npkt, pmax, pot_params.vel_2);
	npkt = aptEncodeU2(pkt, npkt, pmax, pot_params.wnd_2);
	npkt = aptEncodeU4(pkt, npkt, pmax, pot_params.vel_3);
	npkt = aptEncodeU2(pkt, npkt, pmax, pot_params.wnd_3);
	npkt = aptEncodeU4(pkt, npkt, pmax, pot_params.vel_4);

	return(npkt);
}

int
aptPotParamsFormat(FILE *fp, char *tag, POT_PARAMS pot_params)
{
	(void)fprintf(fp, "%40s: %8d\n",				"pot_params.channel", pot_params.channel);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n",		"pot_params.wnd_0", pot_params.wnd_0, pot_params.wnd_0/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n",	"pot_params.vel_1", pot_params.vel_1, pot_params.vel_1/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n",		"pot_params.wnd_1", pot_params.wnd_1, pot_params.wnd_1/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n",	"pot_params.vel_2", pot_params.vel_2, pot_params.vel_2/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n",		"pot_params.wnd_2", pot_params.wnd_2, pot_params.wnd_2/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n",	"pot_params.vel_3", pot_params.vel_3, pot_params.vel_3/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n",		"pot_params.wnd_3", pot_params.wnd_3, pot_params.wnd_3/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n",	"pot_params.vel_4", pot_params.vel_4, pot_params.vel_4/Ke);

	return(0);
}

int
aptButtonParamsDecode(BUTTON_PARAMS *button_params, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&button_params->channel, pkt, npkt, pmax);
	npkt = aptDecodeU2(&button_params->mode, pkt, npkt, pmax);
	npkt = aptDecodeU4(&button_params->pos_1, pkt, npkt, pmax);
	npkt = aptDecodeU4(&button_params->pos_2, pkt, npkt, pmax);
	npkt = aptDecodeU2(&button_params->timeout, pkt, npkt, pmax);
	npkt = aptDecodeU2(&button_params->not_used, pkt, npkt, pmax);

	return(npkt);
}

int
aptButtonParamsEncode(BYTE *pkt, int pmax, BUTTON_PARAMS button_params)
{
	int npkt = 0;

	npkt = aptEncodeU2(pkt, npkt, pmax, button_params.channel);
	npkt = aptEncodeU2(pkt, npkt, pmax, button_params.mode);
	npkt = aptEncodeU4(pkt, npkt, pmax, button_params.pos_1);
	npkt = aptEncodeU4(pkt, npkt, pmax, button_params.pos_2);
	npkt = aptEncodeU2(pkt, npkt, pmax, button_params.timeout);
	npkt = aptEncodeU2(pkt, npkt, pmax, button_params.not_used);

	return(npkt);
}

char *
aptButtonParamsModeString(int mode)
{
	char *p;

	switch (mode) {
	case P_MOT_BUTTON_JOG:
		p = "Jog";
		break;
	case P_MOT_BUTTON_POSITION:
		p = "Position";
		break;
	default:
		p = "Unknown";
		break;
	}
	return(p);
}

int
aptButtonParamsFormat(FILE *fp, char *tag, BUTTON_PARAMS button_params)
{
	(void)fprintf(fp, "%40s: %8d\n", "button_params.channel", button_params.channel);
	(void)fprintf(fp, "%40s: %8d (%s)\n", "button_params.mode",
			button_params.mode,
			aptButtonParamsModeString(button_params.mode));
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "button_params.pos_1", button_params.pos_1, button_params.pos_1/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "button_params.pos_2", button_params.pos_2, button_params.pos_2/Ke);
	(void)fprintf(fp, "%40s: %8d\n", "button_params.timeout",  button_params.timeout);
	(void)fprintf(fp, "%40s: %8d\n", "button_params.not_used", button_params.not_used);

	return(0);
}

int
aptDcStatusUpdateDecode(DC_STATUS_UPDATE *dc_status_update, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&dc_status_update->channel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_status_update->position, pkt, npkt, pmax);
	npkt = aptDecodeU2(&dc_status_update->velocity, pkt, npkt, pmax);
	npkt = aptDecodeU2(&dc_status_update->reserved, pkt, npkt, pmax);
	npkt = aptDecodeU4(&dc_status_update->status_bits, pkt, npkt, pmax);

	return(npkt);
}

int
aptDcStatusUpdateFormat(FILE *fp, char *tag, DC_STATUS_UPDATE dc_status_update)
{
	(void)fprintf(fp, "%40s: %8d\n", "dc_status_update.channel", dc_status_update.channel);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm)\n", "dc_status_update.position",
			dc_status_update.position,
			dc_status_update.position/Ke);
	(void)fprintf(fp, "%40s: %8d (%8.3f mm/s)\n", "dc_status_update.velocity",
			dc_status_update.velocity,
			dc_status_update.velocity/Ke);
	(void)fprintf(fp, "%40s: %8d\n", "dc_status_update.reserved",	dc_status_update.reserved);
	(void)fprintf(fp, "%40s: 0x%08x\n", "dc_status_update.status_bits", dc_status_update.status_bits);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.limit_fwd",		(dc_status_update.status_bits&P_MOT_DC_LIMIT_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.limit_rev",		(dc_status_update.status_bits&P_MOT_DC_LIMIT_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.moving_fwd",		(dc_status_update.status_bits&P_MOT_DC_MOVING_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.moving_rev",		(dc_status_update.status_bits&P_MOT_DC_MOVING_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.jogging_fwd",		(dc_status_update.status_bits&P_MOT_DC_JOGGING_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.jogging_rev",		(dc_status_update.status_bits&P_MOT_DC_JOGGING_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.homing",			(dc_status_update.status_bits&P_MOT_DC_HOMING)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.homed",			(dc_status_update.status_bits&P_MOT_DC_HOMED)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.tracking",			(dc_status_update.status_bits&P_MOT_DC_TRACKING)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.settled",			(dc_status_update.status_bits&P_MOT_DC_SETTLED)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.motion_error",		(dc_status_update.status_bits&P_MOT_DC_MOTION_ERROR)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.current_limit",	(dc_status_update.status_bits&P_MOT_DC_CURRENT_LIMIT)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.channel_enabled",	(dc_status_update.status_bits&P_MOT_DC_CHANNEL_ENABLED)?1:0);

	return(0);
}

int
aptStatusBitsDecode(STATUS_BITS *status_bits, BYTE *pkt, int pmax)
{
	int npkt = 0;

	npkt = aptDecodeU2(&status_bits->channel, pkt, npkt, pmax);
	npkt = aptDecodeU4(&status_bits->status_bits, pkt, npkt, pmax);

	return(npkt);
}

int
aptStatusBitsFormat(FILE *fp, char *tag, STATUS_BITS status_bits)
{
	(void)fprintf(fp, "%40s: %d\n", "status_bits.channel",			status_bits.channel);
	(void)fprintf(fp, "%40s: 0x%08x\n", "status_bits.status_bits",	status_bits.status_bits);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.limit_fwd",		(status_bits.status_bits&P_MOT_DC_LIMIT_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.limit_rev",		(status_bits.status_bits&P_MOT_DC_LIMIT_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.moving_fwd",		(status_bits.status_bits&P_MOT_DC_MOVING_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.moving_rev",		(status_bits.status_bits&P_MOT_DC_MOVING_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.jogging_fwd",		(status_bits.status_bits&P_MOT_DC_JOGGING_FWD)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.jogging_rev",		(status_bits.status_bits&P_MOT_DC_JOGGING_REV)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.homing",			(status_bits.status_bits&P_MOT_DC_HOMING)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.homed",			(status_bits.status_bits&P_MOT_DC_HOMED)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.tracking",			(status_bits.status_bits&P_MOT_DC_TRACKING)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.settled",			(status_bits.status_bits&P_MOT_DC_SETTLED)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.motion_error",		(status_bits.status_bits&P_MOT_DC_MOTION_ERROR)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.current_limit",	(status_bits.status_bits&P_MOT_DC_CURRENT_LIMIT)?1:0);
	(void)fprintf(fp, "%40s: %d\n", "status_bits.channel_enabled",	(status_bits.status_bits&P_MOT_DC_CHANNEL_ENABLED)?1:0);

	return(0);
}
