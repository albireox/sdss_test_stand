//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by APTDLLClient.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_APTDLLCLIENT_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDC_BTNSTARTTHREAD              1000
#define IDC_INITDLL                     1003
#define IDC_MG17LOGGERCTRL              1006
#define IDC_CLEANDLL                    1007
#define IDC_BTNGETNUMHWUNITSEX          1008
#define IDC_EDTHWTYPE                   1009
#define IDC_EDTNUMHWUNITS               1010
#define IDC_BTNINITHWDEVICE             1011
#define IDC_EDTHWSERIALNUM              1012
#define IDC_BTNGETVELPARAMS             1013
#define IDC_EDTMINVEL                   1014
#define IDC_EDTACCN                     1015
#define IDC_BTNSETVELPARAMS             1016
#define IDC_BTNGETSTATUSBITS            1017
#define IDC_EDTMAXVEL1                  1018
#define IDC_EDTSTATUSBITS               1019
#define IDC_BTNHOME                     1020
#define IDC_BTNMOVERELEX                1021
#define IDC_EDTRELDIST                  1022
#define IDC_BTNGETPOS                   1023
#define IDC_EDTPOSITION                 1024
#define IDC_BTNMOVEABSEX                1025
#define IDC_EDTABSPOS                   1026
#define IDC_BTNIDENTIFY                 1027
#define IDC_BTNDISABLEHWCHAN            1028
#define IDC_BTNENABLEHWCHAN             1029
#define IDC_BTNGETHOMEPARAMS            1030
#define IDC_EDTHOMEDIRECTION            1031
#define IDC_EDTHOMELIMSWITCH            1032
#define IDC_EDTHOMEVEL                  1033
#define IDC_BTNSETHOMEPARAMS            1034
#define IDC_EDTZEROOFFSET               1035
#define IDC_BTNGETHWSERNUMEX            1036
#define IDC_BTNMOVEVELOCITY             1037
#define IDC_EDTINDEX                    1038
#define IDC_BTNSTOPPROFILED             1039
#define IDC_BTNGETHWINFO                1040
#define IDC_EDTMODEL                    1041
#define IDC_EDTSWVER                    1042
#define IDC_EDTHWNOTES                  1043
#define IDC_EDTVELDIRECTION             1044
#define IDC_BTNSETCHAN                  1046
#define IDC_EDTCHANID                   1047
#define IDC_BTNDISABLEEVENTDLG          1048
#define IDC_BTNENABLEEVENTDLG           1049
#define IDC_BTNGETBLASHDIST             1052
#define IDC_EDTBLASHDIST                1053
#define IDC_BTNSETBLASHDIST             1054
#define IDC_BTNGETMOTORPARAMS           1055
#define IDC_EDTSTEPSPERREV              1056
#define IDC_EDTGEARBOXRATIO             1057
#define IDC_BTNSETMOTORPARAMS           1058
#define IDC_BTNGETSTAGEAXISINFO         1059
#define IDC_EDTSTAGEMINPOS              1060
#define IDC_EDTSTAGEMAXPOS              1061
#define IDC_EDTSTAGEUNITS               1062
#define IDC_BTNSETSTAGEAXISINFO         1063
#define IDC_EDTSTAGEPITCH               1064
#define IDC_BTNGETHWLIMSWITCHES         1065
#define IDC_EDTREVLIMSWITCH             1066
#define IDC_EDTFWDLIMSWITCH             1067
#define IDC_BTNSETHWLIMSWITCHES         1068
#define IDC_BTNGETDCPIDPARAMS           1069
#define IDC_EDTDCPIDPROP                1070
#define IDC_EDTDCPIDINT                 1071
#define IDC_EDTDCPIDDERIV               1072
#define IDC_BTNSETDCPIDPARAMS           1073
#define IDC_EDTDCPIDINTLIMIT            1074
#define IDC_BTNGETVELPARAMLIMITS        1075
#define IDC_EDTMAXVELLIM                1076
#define IDC_EDTACCN2                    1077
#define IDC_EDTMAXACCNLIM               1077

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1050
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
