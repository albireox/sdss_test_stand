/* file: $RCSfile: ftdi.c,v $
** rcsid: $Id: ftdi.c,v 1.1 2013/01/13 20:39:20 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: ftdi.c,v 1.1 2013/01/13 20:39:20 jwp Exp jwp $";

/*
** *******************************************************************
** $RCSfile: ftdi.c,v $ - ftdi usb support
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>		// for usleep
#include <ctype.h>
#include <string.h>

#include "ftdi.h"

#undef DEBUG

#ifdef NOPE
/**
 * This is a typical initialization sequence.
 * This is taken from the Thorlabs APT Host-Controller Protocol document (Issue 3).
 */

// Set baud rate to 115200.
ftStatus = FT_SetBaudRate(m_hFTDevice, (ULONG)uBaudRate);
// 8 data bits, 1 stop bit, no parity
ftStatus = FT_SetDataCharacteristics(m_hFTDevice, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE);
// Pre purge dwell 50ms.
Sleep(uPrePurgeDwell);
// Purge the device.
ftStatus = FT_Purge(m_hFTDevice, FT_PURGE_RX | FT_PURGE_TX);
// Post purge dwell 50ms.
Sleep(uPostPurgeDwell);
// Reset device.
ftStatus = FT_ResetDevice(m_hFTDevice);
// Set flow control to RTS/CTS.
ftStatus = FT_SetFlowControl(m_hFTDevice, FT_FLOW_RTS_CTS, 0, 0);
// Set RTS.
ftStatus = FT_SetRts(m_hFTDevice);
#endif

/**
 * Initializes an open FTDI device to desired default values.
 */

FT_HANDLE
ftdi_initialize(FT_HANDLE ftHandle, char *tag, int baud)
{
	FT_STATUS ftStatus;

	// Set baud rate
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set baud rate %d\n", tag, baud);
#endif
	ftStatus = FT_SetBaudRate(ftHandle, (ULONG)baud);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set baud rate to %d\n", tag, baud);
		return(NULL);
	}

	// 8 data bits, 1 stop bit, no parity
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set data characteristics\n", tag);
#endif
	ftStatus = FT_SetDataCharacteristics(ftHandle, FT_BITS_8, FT_STOP_BITS_1, FT_PARITY_NONE);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set data characteristics\n", tag);
		return(NULL);
	}

	// Pre-purge dwell 50ms.
#ifdef DEBUG
	(void)fprintf(stderr, "%s: pre-purge dwell\n", tag);
#endif
	usleep(50000);

	// Purge the device.
#ifdef NOPE
#ifdef DEBUG
	(void)fprintf(stderr, "%s: purge the device"
#endif
	ftStatus = FT_Purge(ftHandle, FT_PURGE_RX | FT_PURGE_TX);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't purge\n", tag);
		return(NULL);
	}
#endif

	// Post-purge dwell 50ms.
#ifdef DEBUG
	(void)fprintf(stderr, "%s: post-purge dwell\n", tag);
#endif
	usleep(50000);

	// Reset device.
#ifdef NOPE
#ifdef DEBUG
	(void)fprintf(stderr, "%s: reset the device\n", tag);
#endif
	ftStatus = FT_ResetDevice(ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't reset device\n", tag);
		return(NULL);
	}
#endif

	// Set flow control to RTS/CTS.
#ifdef NOPE
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set flow control\n", tag);
#endif
	ftStatus = FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0, 0);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set flow control\n", tag);
		return(NULL);
	}
#endif

	// Set RTS.
#ifdef NOPE
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set RTS\n", tag);
#endif
	ftStatus = FT_SetRts(ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set RTS\n", tag);
		return(NULL);
	}
#endif

	// Set timeouts.
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set timeouts\n", tag);
#endif
	ftStatus = FT_SetTimeouts(ftHandle, 100, 0);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set timeouts\n", tag);
		return(NULL);
	}

	return(ftHandle);
}

/**
 * Opens the FTDI device by Device Description.
 */

FT_HANDLE
ftdi_open_by_description(char *tag, int vid, int pid, char *description, int baud)
{
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	DWORD iNewVID, iNewPID;

	// load our vid/pid values
	iNewVID = vid;
	iNewPID = pid;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set new vid 0x%04x pid 0x%04x\n", tag, (int)iNewVID, (int)iNewPID);
#endif
	ftStatus = FT_SetVIDPID(iNewVID, iNewPID);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set VID/PID (%s)\n", tag, ftdi_status(ftStatus));
		return(NULL);
	}

	// open a device that matches our description
#ifdef DEBUG
	(void)fprintf(stderr, "%s: open device \"%s\"\n", tag, description);
#endif
	ftStatus = FT_OpenEx(description, FT_OPEN_BY_DESCRIPTION, &ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't open device \"%s\"\n", tag, description);
		return(NULL);
	}
#ifdef DEBUG
	(void)fprintf(stdout, "%s: opened device \"%s\"\n", tag, description);
#endif

	ftHandle = ftdi_initialize(ftHandle, tag, baud);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't initialize device \"%s\"\n", tag, description);
		return(NULL);
	}

	return(ftHandle);
}

/**
 * Opens the FTDI device by Serial Number.
 */

FT_HANDLE
ftdi_open_by_serialnumber(char *tag, int vid, int pid, char *serialnumber, int baud)
{
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	DWORD iNewVID, iNewPID;

	// load our vid/pid values
	iNewVID = vid;
	iNewPID = pid;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: set new vid 0x%04x pid 0x%04x\n", tag, (int)iNewVID, (int)iNewPID);
#endif
	ftStatus = FT_SetVIDPID(iNewVID, iNewPID);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set VID/PID (%s)\n", tag, ftdi_status(ftStatus));
		return(NULL);
	}

	// open a device that matches our description
#ifdef DEBUG
	(void)fprintf(stderr, "%s: open device \"%s\"\n", tag, serialnumber);
#endif
	ftStatus = FT_OpenEx(serialnumber, FT_OPEN_BY_SERIAL_NUMBER, &ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't open device \"%s\"\n", tag, serialnumber);
		return(NULL);
	}
#ifdef DEBUG
	(void)fprintf(stdout, "%s: opened device \"%s\"\n", tag, serialnumber);
#endif

	ftHandle = ftdi_initialize(ftHandle, tag, baud);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't initialize device \"%s\"\n", tag, serialnumber);
		return(NULL);
	}

	return(ftHandle);
}

/**
 * Closes the FTDI device.
 */

FT_STATUS
ftdi_close(FT_HANDLE ftHandle, char *tag)
{
	FT_STATUS ftStatus;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: close device\n", tag);
#endif
	ftStatus = FT_Close(ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close device\n", tag);
		return(ftStatus);
	}

	return(ftStatus);
}

/**
 * Programs the EEPROM with suer-supplied description and serial number.
 */

FT_STATUS
ftdi_ee_program(FT_HANDLE ftHandle, char *tag, char *newdescription, char *newserialnumber)
{
	char Manufacturer[BUFSIZ];
	char ManufacturerId[BUFSIZ];
	char Description[BUFSIZ];
	char SerialNumber[BUFSIZ];
	FT_STATUS ftStatus;
	FT_PROGRAM_DATA ftData;

	// prepare storage for the strings coming from the EEPROM
	ftData.Signature1 = 0x00000000;
	ftData.Signature2 = 0xffffffff;
	ftData.Version = 0x00000005;
	ftData.Manufacturer = Manufacturer;
	ftData.ManufacturerId = ManufacturerId;
	ftData.Description = Description;
	ftData.SerialNumber = SerialNumber;

	// read the existing data from the EEPROM
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_ee_program: read the device\n", tag);
#endif
	ftStatus = FT_EE_Read(ftHandle, &ftData);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read eeprom (%s)\n", tag, ftdi_status(ftStatus));
		return(ftStatus);
	}
	(void)ftdi_ee_format(stderr, "old", ftData);

	// update our two fields of interest
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_ee_program: update the fields of interest\n", tag);
#endif
	if (newdescription != NULL) {
		(void)strcpy(ftData.Description, newdescription);
		ftData.Description[63] = 0;	// ensure null termination
	}
	if (newserialnumber != NULL) {
		(void)strcpy(ftData.SerialNumber, newserialnumber);
		ftData.SerialNumber[63] = 0;	// ensure null termination
	}
	(void)ftdi_ee_format(stderr, "new", ftData);

	// write the new data into the EEPROM
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_ee_program: program the device\n", tag);
#endif
	ftStatus = FT_EE_Program(ftHandle, &ftData);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't program eeprom (%s)\n", tag, ftdi_status(ftStatus));
		return(ftStatus);
	}

	// verify the write
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_ee_program: read the device\n", tag);
#endif
	ftStatus = FT_EE_Read(ftHandle, &ftData);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read eeprom (%s)\n", tag, ftdi_status(ftStatus));
		return(ftStatus);
	}
	(void)ftdi_ee_format(stderr, "verify", ftData);

	return(ftStatus);
}

/**
 * Reads the EEPROM.
 */

FT_STATUS
ftdi_ee_read(FT_PROGRAM_DATA *pftData, FT_HANDLE ftHandle, char *tag)
{
	static char Manufacturer[BUFSIZ];
	static char ManufacturerId[BUFSIZ];
	static char Description[BUFSIZ];
	static char SerialNumber[BUFSIZ];
	FT_STATUS ftStatus;

	// prepare storage for the strings coming from the EEPROM
	pftData->Signature1 = 0x00000000;
	pftData->Signature2 = 0xffffffff;
	pftData->Version = 0x00000005;
	pftData->Manufacturer = Manufacturer;
	pftData->ManufacturerId = ManufacturerId;
	pftData->Description = Description;
	pftData->SerialNumber = SerialNumber;

	// read the existing data from the EEPROM
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_ee_read: read the device\n", tag);
#endif
	ftStatus = FT_EE_Read(ftHandle, pftData);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read eeprom (%s)\n", tag, ftdi_status(ftStatus));
		return(ftStatus);
	}

	return(ftStatus);
}

/**
 * Gets the device USB device info from the FTDI chip.
 */

FT_STATUS
ftdi_get_device_info(FT_DEVICE_LIST_INFO_NODE *devInfo, char *tag, FT_HANDLE ftHandle)
{
	FT_STATUS ftStatus;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: ftdi_get_device_info: get device info\n", tag);
#endif
	ftStatus = FT_GetDeviceInfo(ftHandle, &devInfo->Type, &devInfo->ID, devInfo->SerialNumber, devInfo->Description, NULL);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: ftdi_get_device_info: can't get device info (%s)\n",
				tag, ftdi_status(ftStatus));
		return(ftStatus);
	}
	// manage the fields not returned by the call
	devInfo->Flags = 0;
	devInfo->LocId= 0;
	devInfo->ftHandle = ftHandle;
#ifdef DEBUG
	(void)fprintf(stderr, "         Type=0x%04x\n", devInfo->Type);
	(void)fprintf(stderr, "           ID=0x%08x\n", devInfo->ID);
	(void)fprintf(stderr, " SerialNumber=%s\n", devInfo->SerialNumber);
	(void)fprintf(stderr, "  Description=%s\n", devInfo->Description);
#endif

	return(ftStatus);
}

/**
 * Returns a text string for the given status code.
 */

char *
ftdi_status(FT_STATUS ftStatus)
{
	char *ft_status_name = "unknown";

	switch (ftStatus) {
	case FT_OK:
		ft_status_name = "FTDI OK";
		break;
	case FT_INVALID_HANDLE:
		ft_status_name = "FTDI Invalid Handle";
		break;
	case FT_DEVICE_NOT_FOUND:
		ft_status_name = "FTDI Device Not Found";
		break;
	case FT_DEVICE_NOT_OPENED:
		ft_status_name = "FTDI Device Not Opened";
		break;
	case FT_IO_ERROR:
		ft_status_name = "FTDI IO Error";
		break;
	case FT_INSUFFICIENT_RESOURCES:
		ft_status_name = "FTDI Insufficient Resources";
		break;
	case FT_INVALID_PARAMETER:
		ft_status_name = "FTDI Invalid Parameter";
		break;
	case FT_INVALID_BAUD_RATE:
		ft_status_name = "FTDI Invalie Baud Rate";
		break;
	case FT_DEVICE_NOT_OPENED_FOR_ERASE:
		ft_status_name = "FTDI Device Not Opened for Erase";
		break;
	case FT_DEVICE_NOT_OPENED_FOR_WRITE:
		ft_status_name = "FTDI Device Not Opened for Write";
		break;
	case FT_FAILED_TO_WRITE_DEVICE:
		ft_status_name = "FTDI Failed to Write Device";
		break;
	case FT_EEPROM_READ_FAILED:
		ft_status_name = "FTDI EEPROM Read Failed";
		break;
	case FT_EEPROM_WRITE_FAILED:
		ft_status_name = "FTDI EEPROM Write Failed";
		break;
	case FT_EEPROM_ERASE_FAILED:
		ft_status_name = "FTDI EEPROM Erase Failed";
		break;
	case FT_EEPROM_NOT_PRESENT:
		ft_status_name = "FTDI EEPROM Not Present";
		break;
	case FT_EEPROM_NOT_PROGRAMMED:
		ft_status_name = "FTDI EEPROM Not Programmed";
		break;
	case FT_INVALID_ARGS:
		ft_status_name = "FTDI Invalid Args";
		break;
	case FT_NOT_SUPPORTED:
		ft_status_name = "FTDI Not Supported";
		break;
	case FT_OTHER_ERROR:
		ft_status_name = "FTDI Other Error";
		break;
	default:
		ft_status_name = "unknown error";
		break;
	}

	return(ft_status_name);
}

/**
 * Shows the ftdi EE program data in human-readable form.
 */

int
ftdi_ee_format(FILE *fp, char *tag, FT_PROGRAM_DATA ftData)
{
	(void)fprintf(fp, "%s: eeprom: Signature1: 0x%08x\n", tag, ftData.Signature1);
	(void)fprintf(fp, "%s: eeprom: Signature2: 0x%08x\n", tag, ftData.Signature2);
	(void)fprintf(fp, "%s: eeprom:    Version: 0x%08x\n", tag, ftData.Version);
	(void)fprintf(fp, "%s: eeprom:   VendorID: 0x%08x\n", tag, ftData.VendorId);
	(void)fprintf(fp, "%s: eeprom:  ProductID: 0x%08x\n", tag, ftData.ProductId);
	(void)fprintf(fp, "%s: eeprom:   Manufacturer: %s\n", tag, ftData.Manufacturer);
	(void)fprintf(fp, "%s: eeprom: ManufacturerId: %s\n", tag, ftData.ManufacturerId);
	(void)fprintf(fp, "%s: eeprom:    Description: %s\n", tag, ftData.Description);
	(void)fprintf(fp, "%s: eeprom:   SerialNumber: %s\n", tag, ftData.SerialNumber);
	(void)fprintf(fp, "%s: eeprom:      MaxPower: %d\n", tag, ftData.MaxPower);
	(void)fprintf(fp, "%s: eeprom:           PnP: %d\n", tag, ftData.PnP);
	(void)fprintf(fp, "%s: eeprom:   SelfPowered: %d\n", tag, ftData.SelfPowered);
	(void)fprintf(fp, "%s: eeprom:  RemoteWakeup: %d\n", tag, ftData.RemoteWakeup);

	return(0);
}

/**
 * Shows the ftdi packet in human-readable form.
 * Here, "ftdi packet" doesn't mean anything special;
 * USB devices may impose semantics on these packets
 * according to their own functionality,
 * but at the ftdi level, it's just a bunch of bytes.
 */

int
ftdi_pkt_format(FILE *fp, char *tag, BYTE *pkt, int npkt)
{
	int i;

	for (i = 0; i < npkt; i++) {
		(void)fprintf(fp, "%s: %2d 0x%02x", tag, i, pkt[i]);
		if (isprint(pkt[i])) {
			(void)fprintf(fp, " %c", pkt[i]);
		}
		(void)fprintf(fp, "\n");
	}

	return(0);
}

/**
 * Reads the requested number of bytes from the device queue.
 */

int
ftdi_pkt_read(FT_HANDLE ftHandle, char *tag, BYTE *pkt, int npkt)
{
	FT_STATUS ftStatus;
	DWORD n_ask, n_got;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: read %d bytes\n", tag, npkt);
#endif

#ifdef DEBUG
	DWORD AmountInRxQueue;
	ftStatus = FT_GetQueueStatus(ftHandle, &AmountInRxQueue);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't get queue status (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stderr, "%s: AmountInRxQueue: %d\n", tag, AmountInRxQueue);
#endif

	n_ask = npkt;
	ftStatus = FT_Read(ftHandle, pkt, n_ask, &n_got);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read queue (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: n_got: %d\n", tag, n_got);
#endif

#ifdef DEBUG
	(void)ftdi_pkt_format(stderr, tag, pkt, n_got);
#endif

	return(n_got);
}

/**
 * Writes bytes into the device queue.
 */

int
ftdi_pkt_write(FT_HANDLE ftHandle, char *tag, BYTE *pkt, int npkt)
{
	FT_STATUS ftStatus = FT_OK;
	DWORD n_ask, n_got;

#ifdef DEBUG
	(void)ftdi_pkt_format(stderr, tag, pkt, npkt);
#endif

	n_ask = npkt;
#ifdef DEBUG
	(void)fprintf(stderr, "%s: ask %d bytes out\n", tag, n_ask);
#endif
	ftStatus = FT_Write(ftHandle, pkt, npkt, &n_got);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't write bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
#ifdef DEBUG
	(void)fprintf(stderr, "%s: got %d bytes out\n", tag, n_got);
#endif

	return(n_got);
}

/**
 ** Sleeps until he next seconds multiple of s.
 ** This isn't just an -s second sleep;
 ** if you're already partway into the next second,
 ** this will just wait the sub-second fraction needed
 ** to get you to the next 1-second tick mark.
 **
 ** This isn't any FTDI function,
 ** but this file is a convenient place to put it for now.
 */

int
ftdi_sleep(int s)
{
	struct timeval tv;

	(void)gettimeofday(&tv, NULL);
#ifdef DEBUG
	(void)fprintf(stdout, "ftdi_sleep: timeval %d %d\n", (int)tv.tv_sec, (int)tv.tv_usec);
#endif

	// how many seconds to the next multiple?
	int t1 = (int)tv.tv_sec % s;
#ifdef DEBUG
	(void)fprintf(stdout, "ftdi_sleep: t1 %d\n", t1);
#endif

	// how many microseconds to the next second?
	int t2 = 1000000 - (int)tv.tv_usec;
#ifdef DEBUG
	(void)fprintf(stdout, "ftdi_sleep: t2 %d\n", t2);
#endif

	useconds_t t = 1000000 * t1 + t2;
	(void)usleep(t);

	return(0);
}
