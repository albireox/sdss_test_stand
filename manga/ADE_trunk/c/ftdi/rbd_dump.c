/* file: $RCSfile$
** rcsid: $Id$
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid = "$Id$";
/*
** *******************************************************************
** $RCSfile: apt_demo.c,v $ - RBD 9103 Picoammeter demo program
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "FT232R USB UART";
static int baud = 57600;

static char buf[BUFSIZ];

/**
 * Shows the packet in human-readable form.
 */

int
rbdFormat(FILE *fp, char *tag, char *pkt, int npkt)
{
	int i;

	for (i = 0; i < npkt; i++) {
		(void)fprintf(fp, "%s: %2d 0x%02x", tag, i, pkt[i]);
		if (isprint(pkt[i])) {
			(void)fprintf(fp, " %c", pkt[i]);
		}
		(void)fprintf(fp, "\n");
	}

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int done = 0;
	DWORD n_ask, n_got;

	(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
			tag, vid, pid, description, baud);
	ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	if (ftHandle == NULL) {
		return(-1);
	}

	// Interact with the RBD device
	(void)fprintf(stdout, "%s: start reading the device\n", tag);
	while (!done) {
		n_ask = BUFSIZ;
		ftStatus = FT_Read(ftHandle, buf, n_ask, &n_got);
		if (!FT_SUCCESS(ftStatus)) {
			(void)fprintf(stderr, "%s: can't read (%s)\n", tag, ftdi_status(ftStatus));
			return(-1);
		}
		(void)fprintf(stdout, "%s: got %d bytes\n", tag, n_got);
		if (n_got > 0) {
#ifdef DEBUG
			rbdFormat(stdout, tag, buf, n_got);
#endif
			(void)fprintf(stdout, "%s: %s\n", tag, buf);
		}
	}

	(void)fprintf(stdout, "%s: close the device\n", tag);
	ftStatus = ftdi_close(tag, ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
