/* file: $RCSfile$
** rcsid: $Id$
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id$";
/*
** *******************************************************************
** $RCSfile$ - Program the FTDI EEPROM with a new description and/or serial number.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ftdi.h"

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "DLP-IO8";
static char *serialnumber = NULL;
static int baud = 115200;

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	FT_HANDLE ftHandle; FT_STATUS ftStatus;

	char *newdescription = NULL;
	char *newserialnumber = NULL;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description string)\n");
			(void)fprintf(stderr, "\t-serialnumber string)\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-vid #\n");
			(void)fprintf(stderr, "\t-pid #\n");
			(void)fprintf(stderr, "\t-apt (load vid/pid for thorlabs apt motion controller)\n");
			(void)fprintf(stderr, "\t-dlp (load vid/pid for dlp-i08-g data acq module)\n");
			(void)fprintf(stderr, "\t-motic (load vid/pid for moticam 2300)\n");
			(void)fprintf(stderr, "\t-rbd (load vid/pid for rbd 9103 picoammeter)\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-newdescription string)\n");
			(void)fprintf(stderr, "\t-newserialnumber string)\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			serialnumber = NULL;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			description = NULL;

		} else if (strncmp(argptr, "vid", arglen) == 0) {
			vid = strtol(argv[++argnum], NULL, 0);

		} else if (strncmp(argptr, "pid", arglen) == 0) {
			pid = strtol(argv[++argnum], NULL, 0);

		} else if (strncmp(argptr, "apt", arglen) == 0) {
			vid = 0x0403;
			pid = 0xfaf0;

		} else if (strncmp(argptr, "dlp", arglen) == 0) {
			vid = 0x0403;
			pid = 0x6001;

		} else if (strncmp(argptr, "motic", arglen) == 0) {
			vid = 0x0634;
			pid = 0x1003;

		} else if (strncmp(argptr, "rbd", arglen) == 0) {
			vid = 0x0403;
			pid = 0x6001;

		} else if (strncmp(argptr, "newdescription", arglen) == 0) {
			newdescription = argv[++argnum];

		} else if (strncmp(argptr, "newserialnumber", arglen) == 0) {
			newserialnumber = argv[++argnum];

		} else {
			;
		}
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: vid 0x%04x pid 0x%04x\n", tag, vid, pid);
		(void)fprintf(stdout, "%s: description %s\n", tag, description);
		(void)fprintf(stdout, "%s: serialnumber %s\n", tag, serialnumber);
		(void)fprintf(stdout, "%s: newdescription %s\n", tag, newdescription);
		(void)fprintf(stdout, "%s: newserialnumber %s\n", tag, newserialnumber);
	}

	if (description != NULL) {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
					tag, vid, pid, description, baud);
		}
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	} else {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x serialnumber (%s) baud %d\n",
					tag, vid, pid, serialnumber, baud);
		}
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: program the device\n", tag);
	}
	ftStatus = ftdi_ee_program(ftHandle, tag, newdescription, newserialnumber);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't program\n", tag);
		return(-1);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: close the device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close\n", tag);
		return(-1);
	}

	return(0);
}
