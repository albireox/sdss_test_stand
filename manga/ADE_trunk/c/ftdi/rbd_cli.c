/* file: $RCSfile: rbd_cli.c,v $
** rcsid: $Id: rbd_cli.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: rbd_cli.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: rbd_cli.c,v $ - RBD 9103 Picoammeter demo program
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "FT232R USB UART";
static char *serialnumber = NULL;
static int baud = 57600;

// the &M menu request cmd appears to be the longest, at 1092 bytes.
#define RBD_MAX	(2048)
static char buf[RBD_MAX];

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int done = 0;
	int n_ask, n_got;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description string)\n");
			(void)fprintf(stderr, "\t-serialnumber string)\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			serialnumber = NULL;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			description = NULL;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", argv[0], argptr);
			return(1);
		}
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: vid 0x%04x pid 0x%04x\n", tag, vid, pid);
		(void)fprintf(stdout, "%s: description %s\n", tag, description);
		(void)fprintf(stdout, "%s: serialnumber %s\n", tag, serialnumber);
	}

	if (description != NULL) {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
					tag, vid, pid, description, baud);
		}
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	} else {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x serialnumber (%s) baud %d\n",
					tag, vid, pid, serialnumber, baud);
		}
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	// Interact with the RBD device
	(void)fprintf(stdout, "%s: start reading commands\n", tag);
	while (!done) {
		// read a cmd from the user
		(void)fprintf(stdout, "cmd (&M for menu): ");
		char *p = fgets(buf, RBD_MAX, stdin);
		if (p == NULL) {
			(void)fprintf(stdout, "%s: EOF\n", tag);
			done++;
		} else {
			n_ask = strlen(buf);
			if (n_ask > 0) {
				// nuke the newline
				buf[--n_ask] = 0;
				if (n_ask == 0) {
					// user typed a newline; substitute a current reading
					(void)strcpy(buf, "&S");
					n_ask = strlen(buf);
				}
				// write & read the device
				if (verbose) {
					(void)fprintf(stdout, "%s: send cmd (%s)\n", tag, buf);
				}
				(void)strcat(buf, "\r\n");
				n_ask = strlen(buf);
				n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
				if (verbose) {
					(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
				}
#endif
				if (n_got != n_ask) {
					(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
				} else {
					sleep(1);
					n_ask = RBD_MAX;
					n_got = ftdi_pkt_read(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
					if (verbose) {
						(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
					}
#endif
					if (n_got > 0) {
						buf[--n_got] = 0;
						(void)fprintf(stdout, "%s: %s\n", tag, buf);
					}
				}
			} else {
				done++;
			}
		}
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: close the device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close\n", tag);
		return(-1);
	}

	return(0);
}
