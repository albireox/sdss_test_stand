/* file: $RCSfile: rbd_query.c,v $
** rcsid: $Id: rbd_query.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: rbd_query.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: rbd_query.c,v $ - query a single sample
** a sample (prefixed by this pgm) looks like this:
** rbd_query: 1354547826 530397 &S=,Range=200nA,+036.26,nA
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "FT232R USB UART";
static char *serialnumber = NULL;
static int baud = 57600;

#define RBD_MAX	(10240)
static char buf[RBD_MAX];

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int n_ask, n_got;
	int i, n = 1;		// number of samples

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description string)\n");
			(void)fprintf(stderr, "\t-serialnumber string)\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-loop (go forever)\n");
			(void)fprintf(stderr, "\t-n <#samples> (%d)\n", n);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			serialnumber = NULL;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			description = NULL;

		} else if (strcmp(argptr, "loop") == 0) {
			n = 86400;
		} else if (strcmp(argptr, "n") == 0) {
			n = atoi(argv[++argnum]);
			n = RANGE(n, 1, 86400);

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", argv[0], argptr);
			return(1);
		}
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: vid 0x%04x pid 0x%04x\n", tag, vid, pid);
		(void)fprintf(stdout, "%s: description %s\n", tag, description);
		(void)fprintf(stdout, "%s: serialnumber %s\n", tag, serialnumber);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: n %d\n", tag, n);
	}

	if (description != NULL) {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
					tag, vid, pid, description, baud);
		}
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	} else {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x serialnumber (%s) baud %d\n",
					tag, vid, pid, serialnumber, baud);
		}
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	// turn off automatic readings
	(void)strcpy(buf, "&I0000\r\n");
	n_ask = strlen(buf);
	n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
	}
#endif
	if (n_got != n_ask) {
		(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
		return(1);
	}

	// select the input current filter
	(void)strcpy(buf, "&F064\r\n");	// 64-samples, the maximum allowed
	n_ask = strlen(buf);
	n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
	}
#endif
	if (n_got != n_ask) {
		(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
		return(1);
	}

	// set the range
	(void)strcpy(buf, "&R3\r\n");	// 200 nA
	n_ask = strlen(buf);
	n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
	}
#endif
	if (n_got != n_ask) {
		(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
		return(1);
	}

	// flush the input
	n_ask = RBD_MAX;
	n_got = ftdi_pkt_read(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
	if (verbose) {
		(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
	}
#endif

	for (i = 0; i < n; i++) {
		// ask for a sample
		(void)strcpy(buf, "&S\r\n");
		n_ask = strlen(buf);
		n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		if (n_got != n_ask) {
			(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
			return(1);
		}

		// read the reply
		if (verbose) {
			(void)fprintf(stderr, "%s: read the reply\n", tag);
		}
		n_ask = RBD_MAX;
		n_got = ftdi_pkt_read(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		if (n_got > 0) {
			// nuke the cr
			char *p = strchr(buf, '\r');
			if (p != NULL) {
				*p = 0;
			}
			// scan it back to check for correct response
			{
				float f;
				n_got = sscanf(buf, "&S*,Range=200nA,+%6f,nA", &f);
				if (n_got != 1) {
					(void)fprintf(stderr, "%s: format error: (%s)\n", tag, buf);
					return(1);
				} else if (strcmp(buf+23, ",nA") != 0) {
					(void)fprintf(stderr, "%s: format error: ,nA not present: (%s)\n", tag, buf);
					return(1);
				} else if (f < 0) {
					(void)fprintf(stderr, "%s: bad conversion: current %f < 0\n", tag, f);
					return(1);
				} else if (f > 200) {
					(void)fprintf(stderr, "%s: bad conversion: current %f > 200 nA\n", tag, f);
					return(1);
				}
			}
			struct timeval tv;
			(void)gettimeofday(&tv,  NULL);
			(void)fprintf(stdout, "%s: %d %6d %s\n", tag, (int)tv.tv_sec, (int)tv.tv_usec, buf);
		} else {
			(void)fprintf(stderr, "%s: bad read (ask %d got %d)\n", tag, n_ask, n_got);
			return(1);
		}

		if (n > 1) {
			//sleep(1);
			ftdi_sleep(1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: close the device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
