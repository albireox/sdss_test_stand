/* file: $RCSfile: keithley.c,v $
** rcsid: $Id: keithley.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: keithley.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: keithley.c,v $ - query a single sample from the Keithley 6485 Picoammeter.
** This is a serial device, not a FTDI USB device,
** but we have it here because we cloned the rbd_query program.
** a sample (prefixed by this pgm) looks like this:
** keithley: 1354733857 168 +3.609316E-12A,+1.022175E+04,+0.000000E+00
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include <termios.h>

#include "ftdi.h"

#define DEBUG

static char *dev = "/dev/cu.USA49W4b112P2.2";

#ifdef NOPE
static struct termios old_settings;
static struct termios new_settings;
#endif

static int baud = 9600;

static char buf[BUFSIZ];

static char *dev_init[] = {
	"SYST:ZCH OFF",			// disable zero check
	"SENS:CURR:NPLC 1",		// set meas rate to # power line cycles (p4-5)
	"CONF:CURR",			// I'm not sure; the choices are CURR and DC (p12-2)
	"CURR:RANGE:AUTO OFF",	// disable autoranging
	"CURR:RANGE 2E-7",		// set range to 0.2 uA (p4-4)
	"STAT:QUEUE:NEXT?",		// get last error code
};
static int n_init = (sizeof(dev_init)/sizeof(char *));

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int fd;		// file descriptor
	int init = 0;
	int n_ask, n_got;
	int i, n = 1;		// number of samples

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "-dev <dev> (%s)\n", dev);
			(void)fprintf(stderr, "\t-init\n");
			(void)fprintf(stderr, "\t-n <#samples> (%d)\n", n);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "dev", arglen) == 0) {
			dev = argv[++argnum];

		} else if (strncmp(argptr, "init", arglen) == 0) {
			init++;

		} else if (strcmp(argptr, "n") == 0) {
			n = atoi(argv[++argnum]);
			n = RANGE(n, 1, 86400);

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", argv[0], argptr);
			return(1);
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: dev %s\n", tag, dev);
		(void)fprintf(stderr, "%s: init %d\n", tag, init);
		(void)fprintf(stderr, "%s: n_init %d\n", tag, n_init);
		(void)fprintf(stderr, "%s: n %d\n", tag, n);
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: open the device (%s) baud %d\n", tag, dev, baud);
	}
	fd = open(dev, O_RDWR|O_NONBLOCK);
	if (verbose) {
		(void)fprintf(stderr, "%s: fd: %d\n", tag, fd);
	}
	if (fd < 0) {
		perror(dev);
		return(1);
	}

#ifdef NOPE
	tcgetattr(fd, &old_settings);
	if (verbose) {
		(void)fprintf(stderr, "%s: ispeed %d\n", tag, (int)cfgetispeed(&old_settings));
		(void)fprintf(stderr, "%s: ospeed %d\n", tag, (int)cfgetospeed(&old_settings));
		(void)fprintf(stderr, "%s: c_iflag 0x%08x\n", tag, (int)old_settings.c_iflag);
		(void)fprintf(stderr, "%s: c_oflag 0x%08x\n", tag, (int)old_settings.c_oflag);
		(void)fprintf(stderr, "%s: c_cflag 0x%08x\n", tag, (int)old_settings.c_cflag);
		(void)fprintf(stderr, "%s: c_cflag CSIZE  0x%08x\n", tag, (int)(old_settings.c_cflag & CSIZE));
		(void)fprintf(stderr, "%s: c_cflag CSTOPB 0x%08x\n", tag, (int)(old_settings.c_cflag & CSTOPB));
		(void)fprintf(stderr, "%s: c_cflag CREAD  0x%08x\n", tag, (int)(old_settings.c_cflag & CREAD));
		(void)fprintf(stderr, "%s: c_cflag PARENB 0x%08x\n", tag, (int)(old_settings.c_cflag & PARENB));
		(void)fprintf(stderr, "%s: c_cflag PARODD 0x%08x\n", tag, (int)(old_settings.c_cflag & PARODD));
		(void)fprintf(stderr, "%s: c_cflag HUPCL  0x%08x\n", tag, (int)(old_settings.c_cflag & HUPCL));
		(void)fprintf(stderr, "%s: c_cflag CLOCAL 0x%08x\n", tag, (int)(old_settings.c_cflag & CLOCAL));
		(void)fprintf(stderr, "%s: c_lflag 0x%08x\n", tag, (int)old_settings.c_lflag);
	}
	new_settings = old_settings;
	new_settings.c_iflag |= (IXON | IXOFF);
	tcsetattr(fd, TCSANOW, &new_settings);
#endif

	if (init) {
		if (verbose) {
			(void)fprintf(stderr, "%s: initialize the device\n", tag);
		}
		for (i = 0; i < n_init; i++) {
			strcpy(buf, dev_init[i]);
			strcat(buf, "\n");
#ifdef DEBUG
			if (verbose) {
				(void)fprintf(stderr, "%s: send: %s\n", tag, buf);
			}
#endif
			n_ask = strlen(buf);
			n_got = write(fd, buf, n_ask);
#ifdef DEBUG
			if (verbose) {
				(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
			}
#endif
			if (n_got != n_ask) {
				(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
				perror(dev);
			}

			usleep(200000);

#ifdef DEBUG
			if (verbose){
				(void)fprintf(stderr, "%s: read:\n", tag);
			}
#endif
			n_ask = BUFSIZ;
			n_got = read(fd, buf, n_ask);
#ifdef DEBUG
			if (verbose) {
				(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
			}
#endif
			if (n_got < 0) {
				perror(dev);
			} else {
				(void)fprintf(stderr, "%s: reply: %s\n", tag, buf);
			}
		}
	}

	if (verbose) {
		(void)fprintf(stderr, "%s: query the device\n", tag);
	}

	for (i = 0; i < n; i++) {
		// ask for a sample
		(void)strcpy(buf, "READ?\n");
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: send %s\n", tag, buf);
		}
#endif
		n_ask = strlen(buf);
		n_got = write(fd, buf, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		if (n_got != n_ask) {
			(void)fprintf(stderr, "%s: bad write (ask %d got %d)\n", tag, n_ask, n_got);
			perror(dev);
		}

		usleep(200000);

		// read the reply
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: read the reply\n", tag);
		}
#endif
		n_ask = BUFSIZ;
		n_got = read(fd, buf, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stderr, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		if (n_got < 0) {
			perror(dev);
		} else {
			// nuke the cr
			char *p = strchr(buf, '\r');
			if (p != NULL) {
				*p = 0;
			}
			struct timeval tv;
			(void)gettimeofday(&tv,  NULL);
			(void)fprintf(stdout, "%s: %d %6d %s\n", tag, (int)tv.tv_sec, (int)tv.tv_usec, buf);
		}

		if (n > 1) {
			//sleep(1);
			ftdi_sleep(1);
		}
	}

#ifdef NOPE
	tcsetattr(fd, TCSANOW, &old_settings);
#endif

	if (verbose) {
		(void)fprintf(stderr, "%s: close the device\n", tag);
	}
	if (close(fd) < 0) {
		perror(dev);
		return(1);
	}

	return(0);
}
