/* file: $RCSfile$
** rcsid: $Id$
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid = "$Id$";
/*
** *******************************************************************
** $RCSfile: apt_demo.c,v $ - RBD 9103 Picoammeter demo program
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "FT232R USB UART";
static int baud = 57600;

static BYTE gtx[BUFSIZ];		// global transmit buffer
static BYTE grx[BUFSIZ];		// global receive buffer

/**
 * Writes a string to the device.
 * We sleep for some time before querying the device.
 * The sleep time is passed in as milliseconds,
 * as this is what we see in the Sleep() calls
 * in the rfid1demoDlg.cpp code.
 * We convert to microseconds for the Unix usleep() call.
 */

/**
 * Shows the packet in human-readable form.
 */

int
rbdFormat(FILE *fp, char *tag, BYTE *pkt, int npkt)
{
	int i;

	for (i = 0; i < npkt; i++) {
		(void)fprintf(fp, "%s: %2d 0x%02x", tag, i, pkt[i]);
		if (isprint(pkt[i])) {
			(void)fprintf(fp, " %c", pkt[i]);
		}
		(void)fprintf(fp, "\n");
	}

	return(0);
}

static int
rbd_write(FT_HANDLE ftHandle, BYTE *rx, BYTE *tx, int ntx, int delay)
{
	char *tag = "rbd_write";
	FT_STATUS ftStatus;
	DWORD BytesWritten, AmountInRxQueue, BytesReturned;

	(void)fprintf(stdout, "%s: ntx: %d delay: %d\n", tag, ntx, delay);
#ifdef DEBUG
	(void)rbdFormat(stdout, "rbd_demo: write: ", tx, ntx);
#endif

	(void)fprintf(stdout, "%s: ask %d bytes out\n", tag, ntx);
	ftStatus = FT_Write(ftHandle, tx, ntx, &BytesWritten);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't write bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: got %d bytes out\n", tag, (int)BytesWritten);

	usleep(1000*delay);

	ftStatus = FT_GetQueueStatus(ftHandle, &AmountInRxQueue);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't get queue status (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: expect %d bytes in\n", tag, (int)AmountInRxQueue);

	ftStatus = FT_Read(ftHandle, rx, AmountInRxQueue, &BytesReturned);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't read bytes (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: got %d bytes in\n", tag, (int)BytesReturned);
	*(rx+BytesReturned) = 0;

#ifdef DEBUG
	(void)rbdFormat(stdout, "apt_write: read: ", rx, AmountInRxQueue);
#endif

	return(BytesReturned);
}


int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int done = 0;
	int n_ask, n_got;

	(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
			tag, vid, pid, description, baud);
	ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	if (ftHandle == NULL) {
		return(-1);
	}

	// Interact with the RBD device
	(void)fprintf(stdout, "%s: start reading commands\n", tag);
	while (!done) {
		// read a cmd from the user
		(void)fprintf(stdout, "cmd: ");
		char *p = fgets((char *)gtx, BUFSIZ, stdin);
		if (p == NULL) {
			(void)fprintf(stdout, "%s: EOF\n", tag);
			done++;
		} else {
			n_ask = strlen((char *)gtx);
			(void)fprintf(stdout, "%s: n_ask %d\n", tag, n_ask);
			if (n_ask > 0) {
				// write & read the device
				n_got = rbd_write(ftHandle, grx, gtx, n_ask, 1000);
				if (n_got > 0) {
					(void)fprintf(stdout, "%s: %s\n", tag, grx);
				} else {
					done++;
				}
			} else {
				done++;
			}
		}
	}

	(void)fprintf(stdout, "%s: close the device\n", tag);
	ftStatus = ftdi_close(tag, ftHandle);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close\n", tag);
		return(-1);
	}

	return(0);
}
