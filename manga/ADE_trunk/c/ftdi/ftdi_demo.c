/* file: $RCSfile$
** rcsid: $Id$
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid = "$Id$";
/*
** *******************************************************************
** $RCSfile$ - FTDI demo program - query the bus and open visible devices.
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>
//#include <ctype.h>
#include <string.h>
#include "ftdi.h"

#define MAX_DEVICES	(32)

static FT_DEVICE_LIST_INFO_NODE devInfo[MAX_DEVICES];

static DWORD iOldVID, iOldPID;

typedef struct s_dlist {
	int vid;
	int pid;
} DLIST;

#define DLISTMAX	(16)

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	DWORD numDevs;
	DWORD iNewVID, iNewPID;
	DWORD Type, ID; char SerialNumber[BUFSIZ], Description[BUFSIZ];
	DWORD AmountInRxQueue, AmountInTxQueue, EventStatus;
	DLIST dlist[DLISTMAX];
	int ndlist = 0;
	int i;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\t-device 0xpid,0xvid [...]\n");
			(void)fprintf(stderr, "\t-apt (load vid/pid for thorlabs apt motion controller)\n");
			(void)fprintf(stderr, "\t-dlp (load vid/pid for dlp-i08-g data acq module)\n");
			(void)fprintf(stderr, "\t-motic (load vid/pid for moticam 2300)\n");
			(void)fprintf(stderr, "\t-rbd (load vid/pid for rbd 9103 picoammeter)\n");
			(void)fprintf(stderr, "\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "device", arglen) == 0) {
			(void)sscanf(argv[++argnum], "0x%x,0x%x", &iNewVID, &iNewPID);
			(void)fprintf(stdout, "%s: add vid 0x%04x pid 0x%04x\n", tag, (int)iNewVID, (int)iNewPID);
			dlist[ndlist].vid = iNewVID;
			dlist[ndlist].pid = iNewPID;
			ndlist++;

		} else if (strncmp(argptr, "apt", arglen) == 0) {
			dlist[ndlist].vid = 0x0403;
			dlist[ndlist].pid = 0xfaf0;
			ndlist++;

		} else if (strncmp(argptr, "dlp", arglen) == 0) {
			dlist[ndlist].vid = 0x0403;
			dlist[ndlist].pid = 0x6001;
			ndlist++;

		} else if (strncmp(argptr, "motic", arglen) == 0) {
			dlist[ndlist].vid = 0x0634;
			dlist[ndlist].pid = 0x1003;
			ndlist++;

		} else if (strncmp(argptr, "rbd", arglen) == 0) {
			dlist[ndlist].vid = 0x0403;
			dlist[ndlist].pid = 0x6001;
			ndlist++;

		} else {
			;
		}
	}

	// cache the defaults
	(void)fprintf(stdout, "%s: cache the default VID/PID\n", tag);
	ftStatus = FT_GetVIDPID(&iOldVID, &iOldPID);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't get VID/PID (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}
	(void)fprintf(stdout, "%s: old vid 0x%04x pid 0x%04x\n", tag, (int)iOldVID, (int)iOldPID);

	for (i = 0; i < ndlist; i++) {
		// try each value
		iNewVID = dlist[i].vid;
		iNewPID = dlist[i].pid;
		(void)fprintf(stdout, "%s: load custom vid 0x%04x pid 0x%04x\n", tag, (int)iNewVID, (int)iNewPID);
		ftStatus = FT_SetVIDPID(iNewVID, iNewPID);
		if (!FT_SUCCESS(ftStatus)) {
			(void)fprintf(stderr, "%s: can't set VID/PID (%s)\n", tag, ftdi_status(ftStatus));
			return(-1);
		}

		(void)fprintf(stdout, "%s: create the device info list\n", tag);
		ftStatus = FT_CreateDeviceInfoList(&numDevs);
		if (!FT_SUCCESS(ftStatus)) {
			(void)fprintf(stderr, "%s: can't create device info list (%s)\n", tag, ftdi_status(ftStatus));
			return(-1);
		}
		(void)fprintf(stdout, "%s: number of devices: %d\n", tag, numDevs);

		(void)fprintf(stdout, "%s: get the device info list\n", tag);
		ftStatus = FT_GetDeviceInfoList(devInfo, &numDevs);
		if (!FT_SUCCESS(ftStatus)) {
			(void)fprintf(stderr, "%s: can't get device info list (%s)\n", tag, ftdi_status(ftStatus));
			return(-1);
		}
		(void)fprintf(stdout, "%s: number of devices: %d\n", tag, numDevs);

		for (i = 0; i < numDevs; i++) {
			(void)fprintf(stdout, "Dev %d:\n", i);
			(void)fprintf(stdout, "        Flags=0x%04x\n", devInfo[i].Flags);
			(void)fprintf(stdout, "              Device is %s\n", (devInfo[i].Flags&0x0001)?"Open":"Closed");
			(void)fprintf(stdout, "              Device is %s speed\n", (devInfo[i].Flags&0x0002)?"High":"Full");
			(void)fprintf(stdout, "         Type=0x%04x\n", devInfo[i].Type);
			(void)fprintf(stdout, "           ID=0x%08x\n", devInfo[i].ID);
			(void)fprintf(stdout, "        LocId=0x%04x\n", devInfo[i].LocId);
			(void)fprintf(stdout, " SerialNumber=%s\n", devInfo[i].SerialNumber);
			(void)fprintf(stdout, "  Description=%s\n", devInfo[i].Description);
			(void)fprintf(stdout, "     ftHandle=0x%08x\n", (int)devInfo[i].ftHandle);

			(void)fprintf(stdout, "%s: open device %d\n", tag, i);
			ftStatus = FT_Open(i, &ftHandle);
			if (!FT_SUCCESS(ftStatus)) {
				(void)fprintf(stderr, "%s: can't open device (%s)\n", tag, ftdi_status(ftStatus));
				continue;
			}
			(void)fprintf(stdout, "%s: opened device handle 0x%08x\n", tag, (int)ftHandle);

			(void)fprintf(stdout, "%s: get device info\n", tag);
			ftStatus = FT_GetDeviceInfo(ftHandle, &Type, &ID, SerialNumber, Description, NULL);
			if (!FT_SUCCESS(ftStatus)) {
				(void)fprintf(stderr, "%s: can't get device info (%s)\n", tag, ftdi_status(ftStatus));
				continue;
			}
			(void)fprintf(stdout, "         Type=0x%04x\n", Type);
			(void)fprintf(stdout, "           ID=0x%08x\n", ID);
			(void)fprintf(stdout, " SerialNumber=%s\n", SerialNumber);
			(void)fprintf(stdout, "  Description=%s\n", Description);

			(void)fprintf(stdout, "%s: get device status\n", tag);
			ftStatus = FT_GetStatus(ftHandle, &AmountInRxQueue, &AmountInTxQueue, &EventStatus);
			if (!FT_SUCCESS(ftStatus)) {
				(void)fprintf(stderr, "%s: can't get status (%s)\n", tag, ftdi_status(ftStatus));
				continue;
			}
			(void)fprintf(stdout, "%s: AmountInRxQueue: %d\n", tag, AmountInRxQueue);
			(void)fprintf(stdout, "%s: AmountInTxQueue: %d\n", tag, AmountInTxQueue);
			(void)fprintf(stdout, "%s: EventStatus: 0x%0x8x\n", tag, EventStatus);

			(void)fprintf(stdout, "%s: close device\n", tag);
			ftStatus = FT_Close(ftHandle);
			if (!FT_SUCCESS(ftStatus)) {
				(void)fprintf(stderr, "%s: can't close device (%s)\n", tag, ftdi_status(ftStatus));
				continue;
			}
		}
	}

	(void)fprintf(stdout, "%s: restore the default VID/PID\n", tag);
	ftStatus = FT_SetVIDPID(iOldVID, iOldPID);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't set VID/PID (%s)\n", tag, ftdi_status(ftStatus));
		return(-1);
	}

	return(0);
}
