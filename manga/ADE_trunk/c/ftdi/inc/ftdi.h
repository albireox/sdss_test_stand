/* file: $RCSfile: ftdi.h,v $
** rcsid: $Id: ftdi.h,v 1.1 2013/01/13 20:41:03 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
** *******************************************************************
** $RCSfile: ftdi.h,v $ - FTDI wrappers
** *******************************************************************
*/

#ifndef FTDI_H

#include <stdio.h>
#include <sys/time.h>
#include "ftd2xx.h"

/****************/
/* misc. macros */
/****************/
#define TVTIME(tv)	(tv.tv_sec+(tv.tv_usec/1e6))
#define TVDIFF(tv1,tv2)	(TVTIME(tv1)-TVTIME(tv2))
#ifdef MIN
#undef MIN
#endif
#define MIN(a,b)	((a)<(b)?(a):(b))
#ifdef MAX
#undef MAX
#endif
#define MAX(a,b)	((a)>(b)?(a):(b))
#define RANGE(x,a,b)	(MIN(MAX((x),(a)),(b)))

/**********************/
/* special data types */
/**********************/

#undef WINAPI
#define WINAPI
typedef char TCHAR;

// FTD error codes
#define FT_OK							(0)
#define FT_INVALID_HANDLE				(1)
#define FT_DEVICE_NOT_FOUND				(2)
#define FT_DEVICE_NOT_OPENED			(3)
#define FT_IO_ERROR						(4)
#define FT_INSUFFICIENT_RESOURCES		(5)
#define FT_INVALID_PARAMETER			(6)
#define FT_INVALID_BAUD_RATE			(7)
#define FT_DEVICE_NOT_OPENED_FOR_ERASE	(8)
#define FT_DEVICE_NOT_OPENED_FOR_WRITE	(9)
#define FT_FAILED_TO_WRITE_DEVICE		(10)
#define FT_EEPROM_READ_FAILED			(11)
#define FT_EEPROM_WRITE_FAILED			(12)
#define FT_EEPROM_ERASE_FAILED			(13)
#define FT_EEPROM_NOT_PRESENT			(14)
#define FT_EEPROM_NOT_PROGRAMMED		(15)
#define FT_INVALID_ARGS					(16)
#define FT_NOT_SUPPORTED				(17)
#define FT_OTHER_ERROR					(18)

/* EXTERN_START */
extern FT_HANDLE ftdi_initialize(FT_HANDLE ftHandle, char *tag, int baud);
extern FT_HANDLE ftdi_open_by_description(char *tag, int vid, int pid, char *description, int baud);
extern FT_HANDLE ftdi_open_by_serialnumber(char *tag, int vid, int pid, char *serialnumber, int baud);
extern FT_STATUS ftdi_close(FT_HANDLE ftHandle, char *tag);
extern FT_STATUS ftdi_ee_program(FT_HANDLE ftHandle, char *tag, char *newdescription, char *newserialnumber);
extern FT_STATUS ftdi_ee_read(FT_PROGRAM_DATA *pftData, FT_HANDLE ftHandle, char *tag);
extern FT_STATUS ftdi_get_device_info(FT_DEVICE_LIST_INFO_NODE *devInfo, char *tag, FT_HANDLE ftHandle);
extern char *ftdi_status(FT_STATUS ftStatus);
extern int ftdi_ee_format(FILE *fp, char *tag, FT_PROGRAM_DATA ftData);
extern int ftdi_pkt_format(FILE *fp, char *tag, BYTE *pkt, int npkt);
extern int ftdi_pkt_read(FT_HANDLE ftHandle, char *tag, BYTE *pkt, int npkt);
extern int ftdi_pkt_write(FT_HANDLE ftHandle, char *tag, BYTE *pkt, int npkt);
extern int ftdi_sleep(int s);
/* EXTERN_STOP */

#define FTDI_H
#endif
