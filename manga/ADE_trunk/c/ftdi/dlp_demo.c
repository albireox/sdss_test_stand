/* file: $RCSfile: dlp_demo.c,v $
** rcsid: $Id: dlp_demo.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: dlp_demo.c,v 1.1 2013/01/13 20:44:19 jwp Exp jwp $";
/*
** *******************************************************************
** $RCSfile: dlp_demo.c,v $ - DLP-I08-G demo program. Blinks channel 1.
** *******************************************************************
*/

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "DLP-IO8";
static char *serialnumber = NULL;
static int baud = 115200;

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	int i;

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description string)\n");
			(void)fprintf(stderr, "\t-serialnumber string)\n");
			(void)fprintf(stderr, "\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			serialnumber = NULL;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			description = NULL;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", argv[0], argptr);
			return(1);
		}
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: vid 0x%04x pid 0x%04x\n", tag, vid, pid);
		(void)fprintf(stdout, "%s: description %s\n", tag, description);
		(void)fprintf(stdout, "%s: serialnumber %s\n", tag, serialnumber);
	}

	if (description != NULL) {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
					tag, vid, pid, description, baud);
		}
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	} else {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x serialnumber (%s) baud %d\n",
					tag, vid, pid, serialnumber, baud);
		}
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	for (i = 0; i < 8; i++) {
		int n_ask, n_got;
		char *on = "1";
		char *off = "Q";

		if (verbose) {
			(void)fprintf(stdout, "%s: channel 1 on %d\n", tag, i);
		}
		n_ask = strlen(on);
		n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)on, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		sleep(1);

		if (verbose) {
			(void)fprintf(stdout, "%s: channel 1 off %d\n", tag, i);
		}
		n_ask = strlen(off);
		n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)off, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		sleep(1);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: close the device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close the device\n", tag);
		return(-1);
	}

	return(0);
}
