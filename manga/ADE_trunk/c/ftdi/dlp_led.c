/* file: $RCSfile$
** rcsid: $Id$
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id$";
/*
** *******************************************************************
** $RCSfile:$ - DLP-I08-G Controller
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ftdi.h"

#undef DEBUG

static int vid = 0x0403;
static int pid = 0x6001;
static char *description = "DLP-IO8";
static char *serialnumber = NULL;
static int baud = 115200;

static char *cmdnames[BUFSIZ];

int
main(int argc, char *argv[])
{
	FT_HANDLE ftHandle; FT_STATUS ftStatus;
	char buf[BUFSIZ];
	char cmdbuf[BUFSIZ]; int ncmds = 0;
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen; int verbose = 0;
	int ix;
	int sleeptime = 0;

	for (ix = 0; ix < 256; ix++) {
		cmdnames[ix] = "n/a";
	}

	cmdnames['1'] = "bit 1 high";
	cmdnames['Q'] = "bit 1 low";
	cmdnames['A'] = "bit 1 read digital";
	cmdnames['Z'] = "bit 1 read analog";
	cmdnames['9'] = "bit 1 read temperature";

	cmdnames['2'] = "bit 2 high";
	cmdnames['W'] = "bit 2 low";
	cmdnames['S'] = "bit 2 read digital";
	cmdnames['X'] = "bit 2 read analog";
	cmdnames['0'] = "bit 2 read temperature";

	cmdnames['3'] = "bit 3 high";
	cmdnames['E'] = "bit 3 low";
	cmdnames['D'] = "bit 3 read digital";
	cmdnames['C'] = "bit 3 read analog";
	cmdnames['-'] = "bit 3 read temperature";

	cmdnames['4'] = "bit 4 high";
	cmdnames['R'] = "bit 4 low";
	cmdnames['F'] = "bit 4 read digital";
	cmdnames['V'] = "bit 4 read analog";
	cmdnames['='] = "bit 4 read temperature";

	cmdnames['5'] = "bit 5 high";
	cmdnames['T'] = "bit 5 low";
	cmdnames['G'] = "bit 5 read digital";
	cmdnames['B'] = "bit 5 read analog";
	cmdnames['O'] = "bit 5 read temperature";

	cmdnames['6'] = "bit 6 high";
	cmdnames['Y'] = "bit 6 low";
	cmdnames['H'] = "bit 6 read digital";
	cmdnames['N'] = "bit 6 read analog";
	cmdnames['P'] = "bit 6 read temperature";

	cmdnames['7'] = "bit 7 high";
	cmdnames['U'] = "bit 7 low";
	cmdnames['J'] = "bit 7 read digital";
	cmdnames['M'] = "bit 7 read analog";
	cmdnames['['] = "bit 7 read temperature";

	cmdnames['8'] = "bit 8 high";
	cmdnames['I'] = "bit 8 low";
	cmdnames['K'] = "bit 8 read digital";
	cmdnames[','] = "bit 8 read analog";
	cmdnames[']'] = "bit 8 read temperature";

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			(void)fprintf(stderr, "Usage: %s\n", argv[0]);
			(void)fprintf(stderr, "\t-verbose\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-description string)\n");
			(void)fprintf(stderr, "\t-serialnumber string)\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-[1-8][+-] ...\n");
			(void)fprintf(stderr, "\t-even[+-]\n");
			(void)fprintf(stderr, "\t-odd[+-]\n");
			(void)fprintf(stderr, "\t-all[+-]\n");
			(void)fprintf(stderr, "\n");
			(void)fprintf(stderr, "\t-sleep nsecs\n");
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "description", arglen) == 0) {
			description = argv[++argnum];
			serialnumber = NULL;

		} else if (strncmp(argptr, "serialnumber", arglen) == 0) {
			serialnumber = argv[++argnum];
			description = NULL;

		} else if (strncmp(argptr, "sleep", arglen) == 0) {
			sleeptime = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "1+", arglen) == 0) {
			cmdbuf[ncmds++] = '1';

		} else if (strncmp(argptr, "1-", arglen) == 0) {
			cmdbuf[ncmds++] = 'Q';

		} else if (strncmp(argptr, "1r", arglen) == 0) {
			cmdbuf[ncmds++] = 'A';

		} else if (strncmp(argptr, "1a", arglen) == 0) {
			cmdbuf[ncmds++] = 'Z';

		} else if (strncmp(argptr, "1t", arglen) == 0) {
			cmdbuf[ncmds++] = '9';

		} else if (strncmp(argptr, "2+", arglen) == 0) {
			cmdbuf[ncmds++] = '2';

		} else if (strncmp(argptr, "2-", arglen) == 0) {
			cmdbuf[ncmds++] = 'W';

		} else if (strncmp(argptr, "2r", arglen) == 0) {
			cmdbuf[ncmds++] = 'S';

		} else if (strncmp(argptr, "2a", arglen) == 0) {
			cmdbuf[ncmds++] = 'X';

		} else if (strncmp(argptr, "2t", arglen) == 0) {
			cmdbuf[ncmds++] = '0';

		} else if (strncmp(argptr, "3+", arglen) == 0) {
			cmdbuf[ncmds++] = '3';

		} else if (strncmp(argptr, "3-", arglen) == 0) {
			cmdbuf[ncmds++] = 'E';

		} else if (strncmp(argptr, "3r", arglen) == 0) {
			cmdbuf[ncmds++] = 'D';

		} else if (strncmp(argptr, "3a", arglen) == 0) {
			cmdbuf[ncmds++] = 'C';

		} else if (strncmp(argptr, "3t", arglen) == 0) {
			cmdbuf[ncmds++] = '-';

		} else if (strncmp(argptr, "4+", arglen) == 0) {
			cmdbuf[ncmds++] = '4';

		} else if (strncmp(argptr, "4-", arglen) == 0) {
			cmdbuf[ncmds++] = 'R';

		} else if (strncmp(argptr, "4r", arglen) == 0) {
			cmdbuf[ncmds++] = 'F';

		} else if (strncmp(argptr, "4a", arglen) == 0) {
			cmdbuf[ncmds++] = 'V';

		} else if (strncmp(argptr, "4t", arglen) == 0) {
			cmdbuf[ncmds++] = '=';

		} else if (strncmp(argptr, "5+", arglen) == 0) {
			cmdbuf[ncmds++] = '5';

		} else if (strncmp(argptr, "5-", arglen) == 0) {
			cmdbuf[ncmds++] = 'T';

		} else if (strncmp(argptr, "5r", arglen) == 0) {
			cmdbuf[ncmds++] = 'G';

		} else if (strncmp(argptr, "5a", arglen) == 0) {
			cmdbuf[ncmds++] = 'B';

		} else if (strncmp(argptr, "5t", arglen) == 0) {
			cmdbuf[ncmds++] = 'O';

		} else if (strncmp(argptr, "6+", arglen) == 0) {
			cmdbuf[ncmds++] = '6';

		} else if (strncmp(argptr, "6-", arglen) == 0) {
			cmdbuf[ncmds++] = 'Y';

		} else if (strncmp(argptr, "6r", arglen) == 0) {
			cmdbuf[ncmds++] = 'H';

		} else if (strncmp(argptr, "6a", arglen) == 0) {
			cmdbuf[ncmds++] = 'N';

		} else if (strncmp(argptr, "6t", arglen) == 0) {
			cmdbuf[ncmds++] = 'P';

		} else if (strncmp(argptr, "7+", arglen) == 0) {
			cmdbuf[ncmds++] = '7';

		} else if (strncmp(argptr, "7-", arglen) == 0) {
			cmdbuf[ncmds++] = 'U';

		} else if (strncmp(argptr, "7r", arglen) == 0) {
			cmdbuf[ncmds++] = 'J';

		} else if (strncmp(argptr, "7a", arglen) == 0) {
			cmdbuf[ncmds++] = 'M';

		} else if (strncmp(argptr, "7t", arglen) == 0) {
			cmdbuf[ncmds++] = '[';

		} else if (strncmp(argptr, "8+", arglen) == 0) {
			cmdbuf[ncmds++] = '8';

		} else if (strncmp(argptr, "8-", arglen) == 0) {
			cmdbuf[ncmds++] = 'I';

		} else if (strncmp(argptr, "8r", arglen) == 0) {
			cmdbuf[ncmds++] = 'K';

		} else if (strncmp(argptr, "8a", arglen) == 0) {
			cmdbuf[ncmds++] = ',';

		} else if (strncmp(argptr, "8t", arglen) == 0) {
			cmdbuf[ncmds++] = ']';

		} else if (strncmp(argptr, "even+", arglen) == 0) {
			cmdbuf[ncmds++] = '2';
			cmdbuf[ncmds++] = '4';
			cmdbuf[ncmds++] = '6';
			cmdbuf[ncmds++] = '8';

		} else if (strncmp(argptr, "even-", arglen) == 0) {
			cmdbuf[ncmds++] = 'W';
			cmdbuf[ncmds++] = 'R';
			cmdbuf[ncmds++] = 'Y';
			cmdbuf[ncmds++] = 'I';

		} else if (strncmp(argptr, "odd+", arglen) == 0) {
			cmdbuf[ncmds++] = '1';
			cmdbuf[ncmds++] = '3';
			cmdbuf[ncmds++] = '3';
			cmdbuf[ncmds++] = '7';

		} else if (strncmp(argptr, "odd-", arglen) == 0) {
			cmdbuf[ncmds++] = 'Q';
			cmdbuf[ncmds++] = 'E';
			cmdbuf[ncmds++] = 'T';
			cmdbuf[ncmds++] = 'U';

		} else if (strncmp(argptr, "all+", arglen) == 0) {
			cmdbuf[ncmds++] = '1';
			cmdbuf[ncmds++] = '2';
			cmdbuf[ncmds++] = '3';
			cmdbuf[ncmds++] = '4';
			cmdbuf[ncmds++] = '5';
			cmdbuf[ncmds++] = '6';
			cmdbuf[ncmds++] = '7';
			cmdbuf[ncmds++] = '8';

		} else if (strncmp(argptr, "all-", arglen) == 0) {
			cmdbuf[ncmds++] = 'Q';
			cmdbuf[ncmds++] = 'W';
			cmdbuf[ncmds++] = 'E';
			cmdbuf[ncmds++] = 'R';
			cmdbuf[ncmds++] = 'T';
			cmdbuf[ncmds++] = 'Y';
			cmdbuf[ncmds++] = 'U';
			cmdbuf[ncmds++] = 'I';

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", argv[0], argptr);
			return(1);
		}
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: vid 0x%04x pid 0x%04x\n", tag, vid, pid);
		(void)fprintf(stdout, "%s: description %s\n", tag, description);
		(void)fprintf(stdout, "%s: serialnumber %s\n", tag, serialnumber);
	}

	if (verbose) {
		for (ix = 0; ix < ncmds; ix++) {
			int cmd = cmdbuf[ix];
			(void)fprintf(stderr, "%s: cmd %2d: %c (0x%02x) (%s)\n", tag, ix, cmd, cmd, cmdnames[cmd]);
		}
	}

	if (description != NULL) {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x description (%s) baud %d\n",
					tag, vid, pid, description, baud);
		}
		ftHandle = ftdi_open_by_description(tag, vid, pid, description, baud);
	} else {
		if (verbose) {
			(void)fprintf(stdout, "%s: open the device vid 0x%04x pid 0x%0x serialnumber (%s) baud %d\n",
					tag, vid, pid, serialnumber, baud);
		}
		ftHandle = ftdi_open_by_serialnumber(tag, vid, pid, serialnumber, baud);
	}
	if (ftHandle == NULL) {
		return(-1);
	}

	for (ix = 0; ix < ncmds; ix++) {
		int cmd = cmdbuf[ix];
		int n_ask, n_got;
		if (verbose) {
			(void)fprintf(stderr, "%s: cmd %c (0x%02x) (%s)\n", tag, cmd, cmd, cmdnames[cmd]);
		}
		n_ask = 1;
		n_got = ftdi_pkt_write(ftHandle, tag, (BYTE *)&cmd, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		if (n_got != n_ask) {
			(void)fprintf(stderr, "%s: can't write bytes (ask %d != got %d)\n", tag, n_ask, n_got);
			return(-1);
		}
		n_ask = 32;
		n_got = ftdi_pkt_read(ftHandle, tag, (BYTE *)buf, n_ask);
#ifdef DEBUG
		if (verbose) {
			(void)fprintf(stdout, "%s: ask %d got %d\n", tag, n_ask, n_got);
		}
#endif
		while (n_got-- > 0) {
			(void)fprintf(stderr, "%s: read got 0x%02x\n", tag, buf[0]);
		}
		sleep(sleeptime);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: close the device\n", tag);
	}
	ftStatus = ftdi_close(ftHandle, tag);
	if (!FT_SUCCESS(ftStatus)) {
		(void)fprintf(stderr, "%s: can't close\n", tag);
		return(-1);
	}

	return(0);
}
