/**
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/**
 * *******************************************************************
 * GIGE Camera control program
 *
 * Getting the right camera:
 * We can't rely on the order of discovery.
 * We specify the camera serial number (uid),
 * and we tell it the ds9 name to use (input or output).
 * If we don't specify anything,
 * we use camera 0 (first found) and assume it is "input".
 * *******************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

#include <termios.h>

//#include "PvApi.h"

#include "gige.h"

#undef DEBUG

static struct termios old_settings;
static struct termios new_settings;

// for the command line options and keystroke commands
static char *filename_tag = "uwisc";
static char fname_buf[BUFSIZ];
static double frameRate = 10.0;
static int beep = 0;	// beep when a frame is in
static int binning = 1;	// pixel binning in the camera
static int cameraIndex = 0;	// first one found
static char *ds9name = "input"; // name of xpa access point
static int ds9 = 1;		// show images in ds9
static int etime = 5;	// in ms
static int execute = 0;	// execute frame processing code
static int fits = 0;	// save into FITS format
static int gain = 0;	// in db
static int maxtries = 16;	// number of attempts to find a camera
static int new_etime = 1;
static int new_gain = 1;
static int quit = 0;	// quit after the next frame
static int save = 0;	// save the next frame
static int status = 1;	// query the camera status
static int tsvhi = 0;	// time stamp value lo
static int tsvlo = 0;	// time stamp value hi
static unsigned long uid = 0;		// camera Unique ID, 0 => don't care
static int verbose = 0;	// be more wordy about things

// these next values are how much to diddle the setting via the arrow keys
static int gbump = 6.0206;	// factor of 2, 20*log10(2)

static int
show_help(FILE *fp, char *tag)
{
	(void)fprintf(fp, "Usage: %s\n", tag);
	(void)fprintf(fp, "\t-verbose\n");
	(void)fprintf(fp, "\n");
	(void)fprintf(fp, "\t-binning (%d)\n", binning);
	(void)fprintf(fp, "\t-camera (%d)\n", cameraIndex);
	(void)fprintf(fp, "\t-ds9name (%s)\n", ds9name);
	(void)fprintf(fp, "\t-etime (%d ms)\n", etime);
	(void)fprintf(fp, "\t-gain (%d db)\n", gain);
	(void)fprintf(fp, "\t-tag (filename tag, default=%s)\n", filename_tag);
	(void)fprintf(fp, "\t-uid (%lu) (-camera takes precedence)\n", uid);
	(void)fprintf(fp, "\n");
	(void)fprintf(fp, "\t-fits (save images in fits format)\n");
	(void)fprintf(fp, "\t-raw (save images in raw format - default)\n");
	(void)fprintf(fp, "\t-save (save first image)\n");
	(void)fprintf(fp, "\t-quit (quit after first image)\n");
	(void)fprintf(fp, "\n");
	(void)fprintf(fp, "Keystrokes while looping:\n");
	(void)fprintf(fp, "\tarrow keys:\n");
	(void)fprintf(fp, "\t\t   up: increase exposure time (1 ms)\n");
	(void)fprintf(fp, "\t\t down: decrease exposure time (1 ms)\n");
	(void)fprintf(fp, "\t\tright: increase gain (6 db)\n");
	(void)fprintf(fp, "\t\t left: decrease gain (6 db)\n");
	(void)fprintf(fp, "\tu: increase exposure time  (10 ms)\n");
	(void)fprintf(fp, "\td: decrease exposure time  (10 ms)\n");
	(void)fprintf(fp, "\tU: increase exposure time (100 ms)\n");
	(void)fprintf(fp, "\tD: decrease exposure time (100 ms)\n");
	(void)fprintf(fp, "\t0: return to default exposure time\n");
	(void)fprintf(fp, "\t1-9: select 1-9 seconds\n");
	(void)fprintf(fp, "\tB: turn on beeping\n");
	(void)fprintf(fp, "\tb: toggle beeping\n");
	(void)fprintf(fp, "\tf: save images in fits format\n");
	(void)fprintf(fp, "\th: show this help info\n");
	(void)fprintf(fp, "\tI: connect to ds9\n");
	(void)fprintf(fp, "\ti: toggle connection to ds9\n");
	(void)fprintf(fp, "\tp: pause keystrokes until next image\n");
	(void)fprintf(fp, "\tq: quit after next image\n");
	(void)fprintf(fp, "\tr: save images in raw format\n");
	(void)fprintf(fp, "\ts: save next image to disk\n");
	(void)fprintf(fp, "\tV: turn on verbosity\n");
	(void)fprintf(fp, "\tv: toggle verbosity\n");
	(void)fprintf(fp, "\tx: execute frame processing code\n");
	(void)fprintf(fp, "\t?: show current settings\n");
	(void)fprintf(fp, "\tj: query camera settings\n");

	return(0);
}

// these must be greater than 255
#define FLAG	(0x1100)

#define KEY_0	(FLAG|0x00)
#define KEY_1	(FLAG|0x01)
#define KEY_2	(FLAG|0x02)
#define KEY_3	(FLAG|0x03)
#define KEY_4	(FLAG|0x04)
#define KEY_5	(FLAG|0x05)
#define KEY_6	(FLAG|0x06)
#define KEY_7	(FLAG|0x07)
#define KEY_8	(FLAG|0x08)
#define KEY_9	(FLAG|0x09)

#define KEY_B	(FLAG|0x21)
#define KEY_D	(FLAG|0x22)
#define KEY_I	(FLAG|0x23)
#define KEY_U	(FLAG|0x24)
#define KEY_V	(FLAG|0x25)

#define KEY_b	(FLAG|0x50)
#define KEY_d	(FLAG|0x51)
#define KEY_f	(FLAG|0x52)
#define KEY_h	(FLAG|0x53)
#define KEY_i	(FLAG|0x54)
#define KEY_j	(FLAG|0x55)
#define KEY_p	(FLAG|0x56)
#define KEY_q	(FLAG|0x57)
#define KEY_r	(FLAG|0x58)
#define KEY_s	(FLAG|0x59)
#define KEY_u	(FLAG|0x60)
#define KEY_v	(FLAG|0x61)
#define KEY_x	(FLAG|0x62)

#define ARROW_U	(FLAG|0x41)
#define ARROW_D	(FLAG|0x42)
#define ARROW_R	(FLAG|0x43)
#define ARROW_L	(FLAG|0x44)

#define KEY_query	(FLAG|0x45)	// question mark

/**
 * We monitor the file /tmp/gige_loop.cmds.txt for instructions from other programs.
 * We use the file's modification time to see if anything new is there.
 * Here we cache the most recent mtime.
 */
static time_t last_mtime = 0;

/**
 * Checks for commands from the keyboard or from the command file.
 * Processes each found command.
 * Returns when all are done,
 * or when certain commands are seen.
 */

// our circular buffer
static char cmdbuf[BUFSIZ];
static int head = 0;
static int tail = 0;

/**
 * Reads commands from the command file
 * if it's newer than the previously read file.
 */

static int
read_commands(char *tag)
{
	char fname[BUFSIZ];
	int rcode;
	struct stat statbuf;

	// build the command file name
	(void)sprintf(fname, "/tmp/gige_loop.cmds.%s.txt", ds9name);

#ifdef DEBUG
	(void)fprintf(stdout, "%s: stat %s\n", tag, fname);
#endif

	rcode = stat(fname, &statbuf);
	if (rcode != 0) {
		//perror(fname);
		// no cmd file; quietly move on.
	} else {
#ifdef DEBUG
		(void)fprintf(stdout, "%s: size %d\n", tag, (int)statbuf.st_size);
		(void)fprintf(stdout, "%s: old %24.24s new %24.24s dt %ld\n", tag,
				ctime(&statbuf.st_mtimespec.tv_sec),
				ctime(&last_mtime),
				(last_mtime - statbuf.st_mtimespec.tv_sec));
#endif
		if (statbuf.st_mtimespec.tv_sec > last_mtime) {
#ifdef DEBUG
			(void)fprintf(stdout, "%s: new cmd file!\n", tag);
#endif
			int fd;
			fd = open(fname, O_RDONLY);
			if (fd < 0) {
				perror(fname);
			} else {
				char tmpbuf[BUFSIZ];
				rcode = read(fd, tmpbuf, BUFSIZ);
				if (rcode < 0) {
					perror(fname);
				} else if (rcode == 0) {
					(void)fprintf(stdout, "%s: empty command file!\n", tag);
				} else {
					// nuke any eol
					char *p = index(tmpbuf, '\n');
					if (p != NULL) {
						*p = 0;
					} else {
						tmpbuf[rcode] = 0;
					}
					rcode = strlen(tmpbuf);
					if (rcode > 0) {
						(void)fprintf(stdout, "%s: %d new commands: (%s)\n", tag, rcode, tmpbuf);
						// copy the cmds into our circular buffer
						int i;
						for (i = 0; i < rcode; i++) {
							cmdbuf[tail] = tmpbuf[i];
							tail = (tail + 1) % BUFSIZ;
						}
					} else {
						(void)fprintf(stdout, "%s: null command string!\n", tag);
					}
				}
				(void)close(fd);
				// truncate the file to signal its ingestion
				fd = open(fname, O_TRUNC);
				(void)close(fd);
			}
			last_mtime = statbuf.st_mtimespec.tv_sec;
		}
	}

	return(0);
}

static int
kpoll(char *tag)
{
	int rcode;
	int ch = 0;
	int done = 0;

	// check the command file
	(void)read_commands(tag);

	while (!done) {

		// check the keyboard
		{
			char tmpbuf[BUFSIZ];
			rcode = read(fileno(stdin), tmpbuf, 1);
			if (rcode < 0) {
				perror("stdin");
			} else if (rcode == 0) {
				;
			} else {
				// put it into the cmd buffer
				cmdbuf[tail] = tmpbuf[0];
				tail = (tail + 1) % BUFSIZ;
			}

#ifdef DEBUG
			(void)fprintf(stdout, "%s: head %d tail %d\n", tag, head, tail);
#endif
			if (head == tail) {
				ch = 0;
				done++;
			} else {
				ch = cmdbuf[head];
				head = (head + 1) % BUFSIZ;
			}

			switch (ch) {
			case 0x1b:
				// it's the start of an arrow keystroke
				rcode = read(fileno(stdin), cmdbuf, 1);
				ch = (int)cmdbuf[0] & 0xff;
#ifdef DEBUG
				(void)fprintf(stdout, "%s: ch2 0x%04x\n", tag, ch);
#endif
				if (ch == 0x5b) {
					rcode = read(fileno(stdin), cmdbuf, 1);
					ch = (int)cmdbuf[0] & 0xff;
#ifdef DEBUG
					(void)fprintf(stdout, "%s: ch3 0x%04x\n", tag, ch);
#endif
					switch (ch) {
					case 0x041:
						ch = ARROW_U;
						etime += 1;
						new_etime++;
						break;
					case 0x042:
						ch = ARROW_D;
						etime -= 1;
						new_etime++;
						break;
					case 0x043:
						ch = ARROW_R;
						gain += gbump;
						new_gain++;
						break;
					case 0x044:
						ch = ARROW_L;
						gain -= gbump;
						new_gain++;
						break;
					default:
						// pass it through
						break;
					}
				}
				break;
            case '<':
                ch = ARROW_D; 
                etime -= 1;
                new_etime++;
                break;
            case '>':
                ch = ARROW_U;
                etime += 1;
                new_etime++;
                break;
            case '0':
				ch = KEY_0;
				etime = 5;
				new_etime++;
				break;
			case '1':
				ch = KEY_1;
				etime = 1000;
				new_etime++;
				break;
			case '2':
				ch = KEY_2;
				etime = 2000;
				new_etime++;
				break;
			case '3':
				ch = KEY_3;
				etime = 3000;
				new_etime++;
				break;
			case '4':
				ch = KEY_4;
				etime = 4000;
				new_etime++;
				break;
			case '5':
				ch = KEY_5;
				etime = 5000;
				new_etime++;
				break;
			case '6':
				ch = KEY_6;
				etime = 6000;
				new_etime++;
				break;
			case '7':
				ch = KEY_7;
				etime = 7000;
				new_etime++;
				break;
			case '8':
				ch = KEY_8;
				etime = 8000;
				new_etime++;
				break;
			case '9':
				ch = KEY_9;
				etime = 9000;
				new_etime++;
				break;
			case 'D':
				ch = KEY_D;
				etime -= 100;
				new_etime++;
				break;
			case 'U':
				ch = KEY_U;
				etime += 100;
				new_etime++;
				break;
			case 'B':
				ch = KEY_B;
				beep = 1;
				(void)fprintf(stdout, "%s: beep %s\n", tag, beep?"on":"off");
				break;
			case 'b':
				ch = KEY_b;
				beep = (beep == 0);		// toggle it
				(void)fprintf(stdout, "%s: beep %s\n", tag, beep?"on":"off");
				break;
			case 'd':
				ch = KEY_d;
				etime -= 10;
				new_etime++;
				break;
			case 'f':
				ch = KEY_f;
				fits++;
				(void)fprintf(stdout, "%s: save images in fits format\n", tag);
				break;
			case 'h':
				ch = KEY_h;
				show_help(stdout, tag);
				break;
			case 'I':
				ch = KEY_I;
				ds9 = 1;
				(void)fprintf(stdout, "%s: ds9 %s\n", tag, ds9?"on":"off");
				break;
			case 'i':
				ch = KEY_i;
				ds9 = (ds9 == 0);		// toggle it
				(void)fprintf(stdout, "%s: ds9 %s\n", tag, ds9?"on":"off");
				break;
			case 'j':
				ch = KEY_j;
				status++;
				// this is a blocking command, so we bail out of keystroke processing for this frame
				done++;
				break;
			case 'p':
				ch = KEY_p;
				// this is a blocking command, so we bail out of keystroke processing for this frame
				done++;
				break;
			case 'q':
				ch = KEY_q;
				quit++;
				// this is a blocking command, so we bail out of keystroke processing for this frame
				done++;
				break;
			case 'r':
				ch = KEY_r;
				fits = 0;
				(void)fprintf(stdout, "%s: save images in raw format\n", tag);
				break;
			case 's':
				ch = KEY_s;
				save++;
				// this is a blocking command, so we bail out of keystroke processing for this frame
				done++;
				break;
			case 'u':
				ch = KEY_u;
				etime += 10;
				new_etime++;
				break;
			case 'V':
				ch = KEY_V;
				verbose = 1;
				(void)fprintf(stdout, "%s: verbosity %s\n", tag, verbose?"on":"off");
				break;
			case 'v':
				ch = KEY_v;
				verbose = (verbose == 0);		// toggle it
				(void)fprintf(stdout, "%s: verbosity %s\n", tag, verbose?"on":"off");
				break;
			case 'x':
				ch = KEY_x;
				execute = (execute == 0);		// toggle it
				break;
			case '?':
				ch = KEY_query;
				(void)fprintf(stdout, "%s:    filename tag: %s\n", tag, filename_tag);
				(void)fprintf(stdout, "%s:    image format: %s\n", tag, (fits?"fits":"raw"));
				(void)fprintf(stdout, "%s:    image viewer: %s\n", tag, (ds9?"on":"off"));
				(void)fprintf(stdout, "%s:         binning: %dx%d pixels\n", tag, binning, binning);
				(void)fprintf(stdout, "%s:           etime: %d ms\n", tag, etime);
				(void)fprintf(stdout, "%s:            gain: %d db\n", tag, gain);
				(void)fprintf(stdout, "%s: last saved file: %s\n", tag, fname_buf);
				break;
			default:
				// pass it through
				break;
			}
		}
	}

	return(0);
}

static void
gige_fname(char buf[], char *tag)
{
	time_t t;
	struct tm *p;

	t = time(NULL);
	p = localtime(&t);
	(void)sprintf(buf, "manta.%04d%02d%02d.%02d%02d%02d.%s.%s",
		p->tm_year+1900,
		p->tm_mon+1,
		p->tm_mday,
		p->tm_hour,
		p->tm_min,
		p->tm_sec,
		tag, (fits?"fits":"raw"));
	return;
}

static int
query_stuff(char *tag, tPvHandle Camera)
{
	AVT_INFO Info;
	AVT_IMAGE_MODE ImageMode;
	AVT_ACQUISITION Acquisition;
	AVT_IMAGE_FORMAT ImageFormat;
	AVT_CONTROLS Controls;
	AVT_GIGE Gige;
	AVT_STATS Stats;

	gigeGetInfo(&Info, tag, Camera);
	gigeFmtInfo(stdout, tag, Info);

	gigeGetImageMode(&ImageMode, tag, Camera);
	gigeFmtImageMode(stdout, tag, ImageMode);

	gigeGetAcquisition(&Acquisition, tag, Camera);
	gigeFmtAcquisition(stdout, tag, Acquisition);

	gigeGetImageFormat(&ImageFormat, tag, Camera);
	gigeFmtImageFormat(stdout, tag, ImageFormat);

	gigeGetControls(&Controls, tag, Camera);
	gigeFmtControls(stdout, tag, Controls);

	gigeGetGige(&Gige, tag, Camera);
	gigeFmtGige(stdout, tag, Gige);

	gigeGetStats(&Stats, tag, Camera);
	gigeFmtStats(stdout, tag, Stats);

	return(0);
}

/**
 * exit the program,
 * but not before restoring the keyboard to its cooked state.
 */
static void
bailout(int rcode)
{
	tcsetattr(0, TCSANOW, &old_settings);
	exit(rcode);
}

/**
 * Process the USR1 and USR2 signals.
 */
static void
sigfun(int s)
{
    (void)fprintf(stdout, "signal (%d)\n", s);

	if (s == SIGUSR1) {
		(void)read_commands("signal SIGUSR1");
	} else if (s == SIGUSR2) {
		quit++;
	}

    return;
}

// Manta G-201B (MaNGA)
#define NX	(1624)
#define NY	(1234)
// Manta G-146B (Star Tracker 5000)
//#define NX	(1388)
//#define NY	(1038)
static int nx = NX;
static int ny = NY;
static int w = NX;	// ROI width
static int h = NY;	// ROI height
static char iBuffer[NX*NY + BUFSIZ];	// hardwire this for the larger format
static tPvFrame PvFrame;

static int
processFrame(char *tag, char *buf, int w, int h)
{
	int x, y;

	int sum_v = 0;
	int n = 0;
	int i = 0;

	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			int v = (int)(buf[i++]) & 0xff;
			sum_v += v;
			n++;
		}
	}
	int vbar = (sum_v / n);
	(void)fprintf(stdout, "%s: n %d vbar %d\n", tag, n, vbar);

	return(0);
}

int
main(int argc, char *argv[])
{
	char *tag = argv[0];
	int argnum; char *argptr; size_t arglen;

	// init the start time
	{
		int rcode;
		struct timeval tv;
		rcode = gettimeofday(&tv, NULL);
		if (rcode != 0){
			perror("gige_loop");
			last_mtime = 0;
		} else {
			last_mtime = tv.tv_sec;
			(void)fprintf(stdout, "%s: start time %24.24s\n", tag, ctime(&tv.tv_sec));
		}
	}

	char *pshmem;
	char *tstr;	// time string
	int key = 666;
	int nframes = 0;
	int rcode;
	int shmid;
	time_t clock;
	tPvCameraInfoEx PvCameraInfoEx[10];
	tPvErr PvErr;
	tPvHandle Camera;
	tPvUint32 numCameras = 0;

	// init the file name buf in case it's needed by the '?' command
	(void)strcpy(fname_buf, "no file saved yet");

	for (argnum = 1; argnum < argc; argnum++) {
		argptr = argv[argnum];
		if (*argptr == '-') argptr++;
		arglen = strlen(argptr);

		if (strcmp(argptr, "help") == 0) {
			show_help(stderr, argv[0]);
			return(0);

		} else if (strncmp(argptr, "verbose", arglen) == 0) {
			verbose++;

		} else if (strncmp(argptr, "binning", arglen) == 0) {
			binning = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "camera", arglen) == 0) {
			cameraIndex = atoi(argv[++argnum]);
			uid = 0;

		} else if (strncmp(argptr, "ds9name", arglen) == 0) {
			ds9name = argv[++argnum];

		} else if (strncmp(argptr, "etime", arglen) == 0) {
			etime = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "gain", arglen) == 0) {
			gain = atoi(argv[++argnum]);

		} else if (strncmp(argptr, "tag", arglen) == 0) {
			filename_tag = argv[++argnum];

		} else if (strncmp(argptr, "uid", arglen) == 0) {
			uid = (unsigned long)atoi(argv[++argnum]);
			cameraIndex = -1;

		} else if (strncmp(argptr, "tsvhi", arglen) == 0) {
			tsvhi = strtol(argv[++argnum], NULL, 0);

		} else if (strncmp(argptr, "tsvlo", arglen) == 0) {
			tsvlo = strtol(argv[++argnum], NULL, 0);

		} else if (strncmp(argptr, "beep", arglen) == 0) {
			beep++;

		} else if (strncmp(argptr, "fits", arglen) == 0) {
			fits++;

		} else if (strncmp(argptr, "raw", arglen) == 0) {
			fits = 0;

		} else if (strncmp(argptr, "i", arglen) == 0) {
			ds9 = (ds9 == 0);	// toggle it

		} else if (strncmp(argptr, "save", arglen) == 0) {
			save++;

		} else if (strncmp(argptr, "quit", arglen) == 0) {
			quit++;

		} else {
			(void)fprintf(stderr, "%s: bad arg (%s)\n", tag, argptr);
			return(1);
		}
	}
    
	if (verbose) {
		(void)fprintf(stdout, "%s: USER %s\n", tag, getenv("USER"));
		(void)fprintf(stdout, "%s: HOME %s\n", tag, getenv("HOME"));
		if (uid == 0) {
			(void)fprintf(stdout, "%s: camera %d\n", tag, cameraIndex);
		}
        (void)fprintf(stdout, "%s: ds9name %s\n", tag, ds9name);
		(void)fprintf(stdout, "%s: binning %d\n", tag, binning);
		(void)fprintf(stdout, "%s: etime %d ms\n", tag, etime);
		(void)fprintf(stdout, "%s: gain %d db\n", tag, gain);
		(void)fprintf(stdout, "%s: tag %s\n", tag, filename_tag);
		if (uid != 0) {
			(void)fprintf(stdout, "%s: uid %lu\n", tag, uid);
		}
		if ((tsvhi != 0) || (tsvlo != 0)) {
			// only show them if the user has diddled them
			(void)fprintf(stdout, "%s: tsvhi %8d (0x%08x)\n", tag, tsvhi, tsvhi);
			(void)fprintf(stdout, "%s: tsvlo %8d (0x%08x)\n", tag, tsvlo, tsvlo);
		}
		(void)fprintf(stdout, "%s: save %d\n", tag, save);
		(void)fprintf(stdout, "%s: quit %d\n", tag, quit);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: attach signal handlers\n", tag);
	}
	signal(SIGUSR1, sigfun);
	signal(SIGUSR2, sigfun);

	if (verbose) {
		(void)fprintf(stdout, "%s: set tty raw mode\n", tag);
	}
	tcgetattr(0, &old_settings);
	new_settings = old_settings;
	new_settings.c_lflag &= ~ICANON;
	new_settings.c_lflag &= ~ECHO;
	new_settings.c_cc[VMIN] = 0;
	new_settings.c_cc[VTIME] = 0;
	tcsetattr(0, TCSANOW, &new_settings);

#define START_UP_PVAPI

	// show the PvAPI Version
	if (verbose) {
		tPvUint32 Major;
		tPvUint32 Minor;
		PvVersion(&Major, &Minor);
		(void)fprintf(stdout, "%s: major %lu minor %lu\n", tag, Major, Minor);
	}

	// initialize PvAPI
	if (verbose) {
		(void)fprintf(stdout, "%s: initialize PvAPI\n", tag);
	}
	PvErr = PvInitialize();
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvInitialize: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// find the connected cameras
	if (verbose) {
		(void)fprintf(stdout, "%s: find the connected cameras\n", tag);
	}
	int ntries = 0;
	while (numCameras == 0) {
		(void)kpoll(tag);
		if (quit || (ntries++ >= maxtries)) {
			bailout(1);
		}
		numCameras = PvCameraListEx(PvCameraInfoEx, 10, NULL, sizeof(tPvCameraInfoEx));
		(void)fprintf(stdout, "%s: numCameras %lu (try %d of %d)\n", tag, numCameras, ntries, maxtries);
		sleep(1);
	}

	/**
	 * When iterating over this list,
	 * we check the user's preferred UID.
	 * If UID > 0, then we use it to identify the right camera
	 */

	// print a list of the connected cameras

	if (verbose) {
		int i;
		for (i = 0; i < numCameras; i++) {
			(void)fprintf(stdout, "%s: Camera %d --------------------------------------------------\n", tag, i);
			gigeFmtPvCameraInfoEx(stdout, tag, PvCameraInfoEx[i]);
			(void)fprintf(stdout, "%s: ------------------------------------------------------------\n", tag);
		}
	}

	// scan the list for the desired camera
	if (uid > 0) {
		cameraIndex = -1;
		int i;
		for (i = 0; i < numCameras; i++) {
			(void)fprintf(stdout, "%s: Camera %d UniqueId %lu\n", tag, i, PvCameraInfoEx[i].UniqueId);
			if (PvCameraInfoEx[i].UniqueId == uid) {
				cameraIndex = i;
			}
		}
		if (cameraIndex == -1) {
			(void)fprintf(stderr, "%s: camera uid %lu not found\n", tag, uid);
			bailout(1);
		}
	}

	if (cameraIndex >= numCameras) {
		(void)fprintf(stderr, "%s: camera index %d not found\n", tag, cameraIndex);
		bailout(1);
	}

	int frameBuffer = 0;
	if (strcmp(ds9name, "output") == 0) {
		frameBuffer = 1;
	}

    key += frameBuffer;

#define ATTACH_SHARED_MEMORY

	if (verbose) {
		(void)fprintf(stdout, "%s: attach ds9 shared memory\n", tag);
	}
	shmid = shmget(key, NX*NY, (int)(0666|IPC_CREAT));
	if (shmid < 0) {
		(void)fprintf(stderr, "%s: errno %d\n", tag, errno);
		perror("shmget");
		bailout(1);
	}
	if (verbose) {
		(void)fprintf(stdout, "%s: key %d size %d shmid %d\n", tag, key, NX*NY, shmid);
	}
	pshmem = shmat(shmid, 0, 0);
	if (pshmem == (char *)-1) {
		perror("shmat");
		(void)fprintf(stderr, "%s: errno %d\n", tag, errno);
		bailout(1);
	}

#define INITIALIZE_DS9

	if (ds9) {
		// init some stuff in ds9
		char buf[BUFSIZ];
		sprintf(buf, "xpaset -p %s preserve pan yes", ds9name);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);
		sprintf(buf, "xpaset -p %s preserve regions yes", ds9name);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);
		sprintf(buf, "xpaset -p %s preserve scale yes", ds9name);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);

		sprintf(buf, "xpaset -p %s frame %d", ds9name, frameBuffer);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);

		sprintf(buf, "xpaset -p %s shm array shmid %d \'[xdim=%d,ydim=%d,bitpix=8]\'",
				ds9name, shmid, w, h);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);
		sprintf(buf, "xpaset -p %s frame center", ds9name);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);
		sprintf(buf, "xpaset -p %s zoom to fit", ds9name);
		(void)fprintf(stdout, "%s: %s\n", tag, buf);
		system(buf);
	}

	// open the camera.
	if (verbose) {
		(void)fprintf(stdout, "%s: open camera index %d\n", tag, cameraIndex);
	}
	PvErr = PvCameraOpen(PvCameraInfoEx[cameraIndex].UniqueId, ePvAccessMaster, &Camera);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvCameraOpen: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// adjust the ethernet packet size
	if (verbose) {
		(void)fprintf(stdout, "%s: adjust the ethernet packet size\n", tag);
	}
	PvErr = PvCaptureAdjustPacketSize(Camera, 1500);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvCaptureAdjustPacketSize: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: query the camera attributes\n", tag);
		rcode = gigeGetAttributeList(stdout, tag, Camera);
		if (rcode != 0) {
			bailout(1);
		}
	}

	// adjust the sensor size
	if (verbose) {
		(void)fprintf(stdout, "%s: query the sensor size\n", tag);
	}
	{
		AVT_INFO_SENSOR InfoSensor;
		rcode = gigeGetInfoSensor(&InfoSensor, tag, Camera);
		if (rcode != 0) {
			bailout(1);
		}
		nx = InfoSensor.SensorWidth;
		ny = InfoSensor.SensorHeight;
		w = (nx / binning) & ~1;
		h = (ny / binning) & ~1;
		(void)fprintf(stdout, "%s: sensor: %dx%d pixels\n", tag, nx, ny);
	}

	// start driver stream
	if (verbose) {
		(void)fprintf(stdout, "%s: start a driver stream\n", tag);
	}
	PvErr = PvCaptureStart(Camera);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvCaptureStart: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// queue frame
	if (verbose) {
		(void)fprintf(stdout, "%s: queue an image frame\n", tag);
	}
	PvFrame.ImageBuffer = (void *)iBuffer;
	PvFrame.ImageBufferSize = (tPvUint32)(nx*ny);
	PvErr = PvCaptureQueueFrame(Camera, &PvFrame, NULL);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvCaptureQueueFrame: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

#define SET_UP_THE_CAMERA

	if (verbose) {
		(void)fprintf(stdout, "%s: set the binning: %dx%d\n", tag, binning, binning);
	}
	rcode = gigeSetBinning(Camera, tag, binning);
	if (rcode != 0) {
		bailout(1);
	}

	int acquisitionMode;
	acquisitionMode = ACQUISITION_MODE_SINGLE_FRAME;
	acquisitionMode = ACQUISITION_MODE_CONTINUOUS;
	if (verbose) {
		(void)fprintf(stdout, "%s: set AcquisitionMode: %d\n", tag, acquisitionMode);
	}
	rcode = gigeSetAcquisitionMode(Camera, tag, acquisitionMode);
	if (rcode != 0) {
		bailout(1);
	}

	int frameStartTriggerMode;
	frameStartTriggerMode = FRAME_START_TRIGGER_MODE_FIXED_RATE;
	frameStartTriggerMode = FRAME_START_TRIGGER_MODE_SOFTWARE;
	frameStartTriggerMode = FRAME_START_TRIGGER_MODE_FREE_RUN;
	if (verbose) {
		(void)fprintf(stdout, "%s: set FrameStartTriggerMode: %d\n", tag, frameStartTriggerMode);
	}
	rcode = gigeSetFrameStartTriggerMode(Camera, tag, frameStartTriggerMode);
	if (rcode != 0) {
		bailout(1);
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: set the frame rate: %g Hz\n", tag, frameRate);
	}
	//rcode = gigeSetFrameRate(Camera, tag, frameRate);
	if (rcode != 0) {
		bailout(1);
	}

	if ((tsvhi != 0) || (tsvlo != 0)) {
		(void)fprintf(stdout, "%s: set the time stamp value hi %d (0x%08x) lo %d (0x%08x)\n",
				tag, tsvhi, tsvhi, tsvlo, tsvlo);
		rcode = gigeSetTimeStampValue(Camera, tag, tsvhi, tsvlo);
		if (rcode != 0) {
			bailout(1);
		}
	}

	if (verbose) {
		(void)fprintf(stdout, "%s: start the acquisition\n", tag);
	}
	PvErr = PvCommandRun(Camera, "AcquisitionStart");
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: AcquisitionStart: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

#define START_THE_FRAME_LOOP

	// turn off verbose mode; the user can re=enable it via keyboard command
	verbose = 0;

	/**
	 * we do a do-{}-while instead of a while{}
	 * so we can ask for 1 good image for a save-then-quit
	 */

	do {

		// poll the keyboard
		(void)kpoll(tag);

		if (status) {
			int rcode = query_stuff(tag, Camera);
			if (rcode) {
				bailout(1);
			}
			status = 0;
		}

		if (new_etime) {

			etime = RANGE(etime, 1, 60000);
			(void)fprintf(stdout, "%s: set new etime %d ms\n", tag, etime);

			rcode = gigeSetExposureValue(Camera, tag, etime);
			if (rcode != 0) {
				bailout(1);
			}

			new_etime = 0;
		}

		if (new_gain) {

			gain = RANGE(gain, 0, 32);
			(void)fprintf(stdout, "%s: set new gain %d db\n", tag, gain);

			rcode = gigeSetGainValue(Camera, tag, gain);
			if (rcode != 0) {
				bailout(1);
			}

			new_gain = 0;

		}

#define WAIT_FOR_FRAME

		// wait for frame to return to host
		if (verbose) {
			(void)fprintf(stdout, "%s: wait for frame %d\n", tag, nframes);
		}
		PvErr = PvCaptureWaitForFrameDone(Camera, &PvFrame, PVINFINITE);
		if (PvErr != ePvErrSuccess) {
			(void)fprintf(stderr, "%s: CaptureWaitForFrameDone: %s\n", tag, gigePvErrName(PvErr));
			bailout(1);
		}

		clock = time(NULL);
		tstr = ctime(&clock);
		*(tstr+24) = 0;	// nuke the newline at the end

		// announce the arrival
		if (beep) {
			(void)fprintf(stdout, "");
		}

		// update the terminal window
		(void)gigeFmtPvFrame(stdout, tstr, PvFrame);

		// do something with the image
		if (execute) {
			processFrame(tag, iBuffer, w, h);
		}

#define SAVE_THE_IMAGE

		if (save) {
			gige_fname(fname_buf, filename_tag);
			(void)fprintf(stdout, "%s: save image to %s\n", tag, fname_buf);
			{
				// cache the file name
				char fname[BUFSIZ];
				// build the command file name
				(void)sprintf(fname, "/tmp/gige_loop.save.%s.txt", ds9name);
				FILE *fp = fopen(fname, "w");
				(void)fwrite(fname_buf, 1, strlen(fname_buf), fp);
				(void)fwrite("\n", 1, strlen("\n"), fp);
				(void)fclose(fp);
			}
			{
				// write the image
				FILE *fp = fopen(fname_buf, "w");
				if (fits) {
					(void)gigeFitsWrite(fp, tag, iBuffer, w, h, ds9name);
				} else {
					(void)fwrite(iBuffer, 1, w*h, fp);
				}
				(void)fclose(fp);
			}
			save = 0;
		}

#define UPDATE_DS9

		(void)memcpy(pshmem, iBuffer, w*h);
		if (ds9) {
			char buf[BUFSIZ];
			/**
			 * We want to write to a frame without showing it,
			 * in case the user is looking at some other frame.
			 * So we'll query the current frame,
			 * write to the one we want,
			 * then re-select the previous one.
			 */
//			sprintf(buf, "xpaget %s frame > /tmp/foo", ds9name);
//			if (verbose) {
//				(void)fprintf(stdout, "%s: %s\n", tag, buf);
//			}
//			system(buf);
//
//			sprintf(buf, "xpaset -p %s frame %d", ds9name, cameraIndex);
//			if (verbose) {
//				(void)fprintf(stdout, "%s: %s\n", tag, buf);
//			}
//			system(buf);

			sprintf(buf, "xpaset -p %s shm array shmid %d \'[xdim=%d,ydim=%d,bitpix=8]\'",
					ds9name, shmid, w, h);
			if (verbose) {
				(void)fprintf(stdout, "%s: %s\n", tag, buf);
			}
			system(buf);

			// restore the previous one
//			{
//				FILE *fp;
//				fp = fopen("/tmp/foo", "r");
//				if (fgets(buf, BUFSIZ, fp) != NULL) {
//					int f = atoi(buf);
//					// make sure the frame number is reasonable
//					if (f >= 0) {
//						if (f <= 9) {
//							sprintf(buf, "xpaset -p %s frame %d", ds9name, f);
//							if (verbose) {
//								(void)fprintf(stdout, "%s: %s\n", tag, buf);
//							}
//							system(buf);
//						}
//					}
//				}
//				fclose(fp);
//			}
		}

		nframes++;

#define RE_QUEUE_THE_FRAME
        
        if (verbose) {
            (void)fprintf(stdout, "%s: queue a new image frame\n", tag);
        }
        PvErr = PvCaptureQueueFrame(Camera, &PvFrame, NULL);
        if (PvErr != ePvErrSuccess) {
            (void)fprintf(stderr, "%s: PvCaptureQueueFrame: %s\n", tag, gigePvErrName(PvErr));
            bailout(1);
        }

	} while (!quit);

#define CLEAN_UP_BEFORE_ENDING

	// stop camera receiving frame triggers
	if (verbose) {
		(void)fprintf(stdout, "%s: stop frame triggers\n", tag);
	}
	PvErr = PvCommandRun(Camera, "AcquisitionStop");
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: AcquisitionStop: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// stop driver stream
	if (verbose) {
		(void)fprintf(stdout, "%s: end the driver stream\n", tag);
	}
	PvErr = PvCaptureEnd(Camera);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: PvCaptureEnd: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// close the camera.
	if (verbose) {
		(void)fprintf(stdout, "%s: close the camera\n", tag);
	}
	PvErr = PvCameraClose(Camera);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: CameraClose: %s\n", tag, gigePvErrName(PvErr));
		bailout(1);
	}

	// uninitialize PvAPI
	if (verbose) {
		(void)fprintf(stdout, "%s: uninitialize PvAPI\n", tag);
	}
	PvUnInitialize();

	tcsetattr(0, TCSANOW, &old_settings);

	return(0);
}
