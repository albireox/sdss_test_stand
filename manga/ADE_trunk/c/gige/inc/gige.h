/* file: $RCSfile: gige.h,v $
** rcsid: $Id: gige.h,v 1.3 2013/08/16 19:44:44 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/

/*
** *******************************************************************
** $RCSfile: gige.h,v $ - GIGE camera header file
** *******************************************************************
*/

#ifndef GIGE_H

#include <stdio.h>
#include <sys/time.h>
#include "PvApi.h"

/******************************/
/* global verbosity indicator */
/******************************/
//extern int verbose;

/**************************/
/* global debug indicator */
/**************************/
//extern int debug;

/**********************/
/* special data types */
/**********************/

/****************/
/* misc. macros */
/****************/
#define TVTIME(tv)	(tv.tv_sec+(tv.tv_usec/1e6))
#define TVDIFF(tv1,tv2)	(TVTIME(tv1)-TVTIME(tv2))
#ifdef MIN
#undef MIN
#endif
#define MIN(a,b)	((a)<(b)?(a):(b))
#ifdef MAX
#undef MAX
#endif
#define MAX(a,b)	((a)>(b)?(a):(b))
#define RANGE(x,a,b)	(MIN(MAX((x),(a)),(b)))

#define AVT_STRLEN	(BUFSIZ)

#define ACQUISITION_MODE_CONTINUOUS		(0)
#define ACQUISITION_MODE_SINGLE_FRAME	(1)

#define FRAME_START_TRIGGER_MODE_FIXED_RATE	(0)
#define FRAME_START_TRIGGER_MODE_FREE_RUN	(1)
#define FRAME_START_TRIGGER_MODE_SOFTWARE	(2)

// Info -----------------------------------------------------------------------

typedef struct {
	unsigned long FirmwareVerBuild;
	unsigned long FirmwareVerMajor;
	unsigned long FirmwareVerMinor;
} AVT_INFO_FIRMWARE;

typedef struct {
	unsigned long PartClass;
	unsigned long PartNumber;
	char PartRevision[AVT_STRLEN];
	char PartVersion[AVT_STRLEN];
	char SerialNumber[AVT_STRLEN];
} AVT_INFO_PART;

typedef struct {
	unsigned long SensorBits;
	unsigned long SensorHeight;
	char SensorType[AVT_STRLEN];
	unsigned long SensorWidth;
} AVT_INFO_SENSOR;

typedef struct {
	char CameraName[AVT_STRLEN];
	char DeviceFirmwareVersion[AVT_STRLEN];
	char DeviceModelName[AVT_STRLEN];
	char DevicePartNumber[AVT_STRLEN];
	char DeviceSerialNumber[AVT_STRLEN];
	char DeviceVendorName[AVT_STRLEN];
	AVT_INFO_FIRMWARE InfoFirmware;
	AVT_INFO_PART InfoPart;
	AVT_INFO_SENSOR InfoSensor;
	unsigned long UniqueId;
} AVT_INFO;

// ImageMode ------------------------------------------------------------------

typedef struct {
	unsigned long BinningX;
	unsigned long BinningY;
	unsigned long DecimationHorizontal;
	unsigned long DecimationVertical;
} AVT_IMAGE_MODE;

// Acquisition ----------------------------------------------------------------

typedef struct {
	unsigned long FrameStartTriggerDelay;
	char FrameStartTriggerEvent[AVT_STRLEN];
	char FrameStartTriggerMode[AVT_STRLEN];
	char FrameStartTriggerOverlap[AVT_STRLEN];
} AVT_ACQUISITION_TRIGGER_FRAME_START;

typedef struct {
	float FrameRate;
	AVT_ACQUISITION_TRIGGER_FRAME_START FrameStart;
} AVT_ACQUISITION_TRIGGER;

typedef struct {
	AVT_ACQUISITION_TRIGGER Trigger;
	unsigned long AcquisitionFrameCount;
	char AcquisitionMode[AVT_STRLEN];
	unsigned long RecorderPreEventCount;
} AVT_ACQUISITION;

// ImageFormat ----------------------------------------------------------------

typedef struct {
	unsigned long Height;
	unsigned long RegionX;
	unsigned long RegionY;
	unsigned long Width;
} AVT_IMAGE_FORMAT_ROI;

typedef struct {
	AVT_IMAGE_FORMAT_ROI ROI;
	char PixelFormat[AVT_STRLEN];
	unsigned long TotalBytesPerFrame;
} AVT_IMAGE_FORMAT;

// Controls -------------------------------------------------------------------

typedef struct {
	char ExposureMode[AVT_STRLEN];
	unsigned long ExposureValue;
} AVT_CONTROLS_EXPOSURE;

typedef struct {
	char GainMode[AVT_STRLEN];
	unsigned long GainValue;
} AVT_CONTROLS_GAIN;

typedef struct {
	AVT_CONTROLS_EXPOSURE Exposure;
	AVT_CONTROLS_GAIN Gain;
} AVT_CONTROLS;

// GigE -----------------------------------------------------------------------

typedef struct {
	char DeviceEthAddress[AVT_STRLEN];
	char HostEthAddress[AVT_STRLEN];
} AVT_GIGE_ETHERNET;

typedef struct {
	char DeviceIPAddress[AVT_STRLEN];
	char HostIPAddress[AVT_STRLEN];
} AVT_GIGE_IP;

typedef struct {
	unsigned long TimeStampFrequency;
	unsigned long TimeStampValueHi;
	unsigned long TimeStampValueLo;
} AVT_GIGE_TIMESTAMP;

typedef struct {
	char BandwidthControlMode[AVT_STRLEN];
	unsigned long NonImagePayloadSize;
	unsigned long PayloadSize;
	AVT_GIGE_ETHERNET Ethernet;
	AVT_GIGE_IP IP;
	unsigned long HeartbeatInterval;
	unsigned long HeartbeatTimeout;
	unsigned long PacketSize;
	unsigned long StreamBytesPerSecond;
	AVT_GIGE_TIMESTAMP Timestamp;
} AVT_GIGE;

// Stats ----------------------------------------------------------------------

typedef struct {
	char StatDriverType[AVT_STRLEN];
	char StatFilterVersion[AVT_STRLEN];
	float StatFrameRate;
	unsigned long StatFramesCompleted;
	unsigned long StatFramesDropped;
	unsigned long StatPacketsErroneous;
	unsigned long StatPacketsMissed;
	unsigned long StatPacketsReceived;
	unsigned long StatPacketsRequested;
	unsigned long StatPacketsResent;
} AVT_STATS;

// ----------------------------------------------------------------------------

/* EXTERN_START */
extern char *gigePvErrName(tPvErr PvErr);
extern int gigeFitsWrite(FILE *fp, char *tag, char *image, int nx, int ny, char *history);
extern int gigeFmtAcquisition(FILE *fp, char *tag, AVT_ACQUISITION Acquisition);
extern int gigeFmtAcquisitionTrigger(FILE *fp, char *tag, AVT_ACQUISITION_TRIGGER Trigger);
extern int gigeFmtAcquisitionTriggerFrameStart(FILE *fp, char *tag, AVT_ACQUISITION_TRIGGER_FRAME_START FrameStart);
extern int gigeFmtControls(FILE *fp, char *tag, AVT_CONTROLS Controls);
extern int gigeFmtControlsExposure(FILE *fp, char *tag, AVT_CONTROLS_EXPOSURE Exposure);
extern int gigeFmtControlsGain(FILE *fp, char *tag, AVT_CONTROLS_GAIN Gain);
extern int gigeFmtGige(FILE *fp, char *tag, AVT_GIGE Gige);
extern int gigeFmtGigeEthernet(FILE *fp, char *tag, AVT_GIGE_ETHERNET Ethernet);
extern int gigeFmtGigeIP(FILE *fp, char *tag, AVT_GIGE_IP IP);
extern int gigeFmtGigeTimestamp(FILE *fp, char *tag, AVT_GIGE_TIMESTAMP Timestamp);
extern int gigeFmtImageFormat(FILE *fp, char *tag, AVT_IMAGE_FORMAT ImageFormat);
extern int gigeFmtImageFormatROI(FILE *fp, char *tag, AVT_IMAGE_FORMAT_ROI ROI);
extern int gigeFmtImageMode(FILE *fp, char *tag, AVT_IMAGE_MODE ImageMode);
extern int gigeFmtInfo(FILE *fp, char *tag, AVT_INFO Info);
extern int gigeFmtInfoFirmware(FILE *fp, char *tag, AVT_INFO_FIRMWARE InfoFirmware);
extern int gigeFmtInfoPart(FILE *fp, char *tag, AVT_INFO_PART InfoPart);
extern int gigeFmtInfoSensor(FILE *fp, char *tag, AVT_INFO_SENSOR InfoSensor);
extern int gigeFmtPvCameraInfoEx(FILE *fp, char *tag, tPvCameraInfoEx PvCameraInfoEx);
extern int gigeFmtPvFrame(FILE *fp, char *tag, tPvFrame PvFrame);
extern int gigeFmtPvFrameVerbose(FILE *fp, char *tag, tPvFrame PvFrame);
extern int gigeFmtPvIpSettings(FILE *fp, char *tag, tPvIpSettings PvIpSettings);
extern int gigeFmtStats(FILE *fp, char *tag, AVT_STATS Stats);
extern int gigeGetAcquisition(AVT_ACQUISITION *Acquisition, char *tag, tPvHandle Camera);
extern int gigeGetAcquisitionTrigger(AVT_ACQUISITION_TRIGGER *AcquisitionTrigger, char *tag, tPvHandle Camera);
extern int gigeGetAcquisitionTriggerFrameStart(AVT_ACQUISITION_TRIGGER_FRAME_START *FrameStart, char *tag, tPvHandle Camera);
extern int gigeGetAttributeList(FILE *fp, char *tag, tPvHandle Camera);
extern int gigeGetControls(AVT_CONTROLS *Controls, char *tag, tPvHandle Camera);
extern int gigeGetControlsExposure(AVT_CONTROLS_EXPOSURE *Exposure, char *tag, tPvHandle Camera);
extern int gigeGetControlsGain(AVT_CONTROLS_GAIN *Gain, char *tag, tPvHandle Camera);
extern int gigeGetGige(AVT_GIGE *Gige, char *tag, tPvHandle Camera);
extern int gigeGetGigeEthernet(AVT_GIGE_ETHERNET *Ethernet, char *tag, tPvHandle Camera);
extern int gigeGetGigeIP(AVT_GIGE_IP *IP, char *tag, tPvHandle Camera);
extern int gigeGetGigeTimestamp(AVT_GIGE_TIMESTAMP *Timestamp, char *tag, tPvHandle Camera);
extern int gigeGetImageFormat(AVT_IMAGE_FORMAT *ImageFormat, char *tag, tPvHandle Camera);
extern int gigeGetImageFormatROI(AVT_IMAGE_FORMAT_ROI *ImageFormatROI, char *tag, tPvHandle Camera);
extern int gigeGetImageMode(AVT_IMAGE_MODE *ImageMode, char *tag, tPvHandle Camera);
extern int gigeGetInfo(AVT_INFO *Info, char *tag, tPvHandle Camera);
extern int gigeGetInfoFirmware(AVT_INFO_FIRMWARE *InfoFirmware, char *tag, tPvHandle Camera);
extern int gigeGetInfoPart(AVT_INFO_PART *InfoPart, char *tag, tPvHandle Camera);
extern int gigeGetInfoSensor(AVT_INFO_SENSOR *InfoSensor, char *tag, tPvHandle Camera);
extern int gigeGetStats(AVT_STATS *Stats, char *tag, tPvHandle Camera);
extern int gigeSetAcquisitionMode(tPvHandle Camera, char *tag, int acquisitionMode);
extern int gigeSetBinning(tPvHandle Camera, char *tag, int binning);
extern int gigeSetExposureValue(tPvHandle Camera, char *tag, int exposureValue);
extern int gigeSetFrameRate(tPvHandle Camera, char *tag, double frameRate);
extern int gigeSetFrameStartTriggerMode(tPvHandle Camera, char *tag, int frameStartTriggerMode);
extern int gigeSetGainValue(tPvHandle Camera, char *tag, int gainValue);
extern int gigeSetROI(tPvHandle Camera, char *tag, int width, int height, int x, int y);
extern int gigeSetTimeStampValue(tPvHandle Camera, char *tag, int valueHi, int valueLo);
/* EXTERN_STOP */

#define GIGE_H
#endif
