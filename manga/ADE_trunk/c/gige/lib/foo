/* file: $RCSfile: gige.c,v $
** rcsid: $Id: gige.c,v 1.1 2012/12/26 13:00:08 jwp Exp mabadmin $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: gige.c,v 1.1 2012/12/26 13:00:08 jwp Exp mabadmin $";

/*
** *******************************************************************
** $RCSfile: gige.c,v $ - gige support
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

#include "PvApi.h"

#include "gige.h"

#define DEBUG

// generic boolean function
static char *
bool(unsigned long ul)
{
	char *b = "false";
	if (ul) {
		b = "true";
	}
	return(b);
}

#ifdef NOPE
//
// Error codes, returned by most functions:
//
typedef enum
{
    ePvErrSuccess       = 0,        // No error
    ePvErrCameraFault   = 1,        // Unexpected camera fault
    ePvErrInternalFault = 2,        // Unexpected fault in PvApi or driver
    ePvErrBadHandle     = 3,        // Camera handle is invalid
    ePvErrBadParameter  = 4,        // Bad parameter to API call
    ePvErrBadSequence   = 5,        // Sequence of API calls is incorrect
    ePvErrNotFound      = 6,        // Camera or attribute not found
    ePvErrAccessDenied  = 7,        // Camera cannot be opened in the specified mode
    ePvErrUnplugged     = 8,        // Camera was unplugged
    ePvErrInvalidSetup  = 9,        // Setup is invalid (an attribute is invalid)
    ePvErrResources     = 10,       // System/network resources or memory not available
    ePvErrBandwidth     = 11,       // 1394 bandwidth not available
    ePvErrQueueFull     = 12,       // Too many frames on queue
    ePvErrBufferTooSmall= 13,       // Frame buffer is too small
    ePvErrCancelled     = 14,       // Frame cancelled by user
    ePvErrDataLost      = 15,       // The data for the frame was lost
    ePvErrDataMissing   = 16,       // Some data in the frame is missing
    ePvErrTimeout       = 17,       // Timeout during wait
    ePvErrOutOfRange    = 18,       // Attribute value is out of the expected range
    ePvErrWrongType     = 19,       // Attribute is not this type (wrong access function)
    ePvErrForbidden     = 20,       // Attribute write forbidden at this time
    ePvErrUnavailable   = 21,       // Attribute is not available at this time
    ePvErrFirewall      = 22,       // A firewall is blocking the traffic (Windows only)
    __ePvErr_force_32   = 0xFFFFFFFF

} tPvErr;
#endif

/**
 * Returns a text string for the given status code.
 */

char *
gige_status(tPvErr pvErr)
{
	char *status_name = "unknown";

	switch (pvErr) {
	case ePvErrSuccess:
		status_name = "No error";
		break;
	case ePvErrCameraFault:
		status_name = "Unexpected camera fault";
		break;
	case ePvErrInternalFault:
		status_name = "Unexpected fault in PvApi or driver";
		break;
	case ePvErrBadHandle:
		status_name = "Camera handle is invalid";
		break;
	case ePvErrBadParameter:
		status_name = "Bad parameter to API call";
		break;
	case ePvErrBadSequence:
		status_name = "Sequence of API calls is incorrect";
		break;
	case ePvErrNotFound:
		status_name = "Camera or attribute not found";
		break;
	case ePvErrAccessDenied:
		status_name = "Camera cannot be opened in the specified mode";
		break;
	case ePvErrUnplugged:
		status_name = "Camera was unplugged";
		break;
	case ePvErrInvalidSetup:
		status_name = "Setup is invalid (an attribute is invalid)";
		break;
	case ePvErrResources:
		status_name = "System/network resources or memory not available";
		break;
	case ePvErrBandwidth:
		status_name = "1394 bandwidth not available";
		break;
	case ePvErrQueueFull:
		status_name = "Too many frames on queue";
		break;
	case ePvErrBufferTooSmall:
		status_name = "Frame buffer is too small";
		break;
	case ePvErrCancelled:
		status_name = "Frame cancelled by user";
		break;
	case ePvErrDataLost:
		status_name = "The data for the frame was lost";
		break;
	case ePvErrDataMissing:
		status_name = "Some data in the frame is missing";
		break;
	case ePvErrTimeout:
		status_name = "Timeout during wait";
		break;
	case ePvErrOutOfRange:
		status_name = "Attribute value is out of the expected range";
		break;
	case ePvErrWrongType:
		status_name = "Attribute is not this type (wrong access function)";
		break;
	case ePvErrForbidden:
		status_name = "Attribute write forbidden at this time";
		break;
	case ePvErrUnavailable:
		status_name = "Attribute is not available at this time";
		break;
	case ePvErrFirewall:
		status_name = "A firewall is blocking the traffic (Windows only)";
		break;
	default:
		status_name = "Unknown error";
		break;
	}

	return(status_name);
}

#ifdef NOPE
typedef struct
{
    // IP configuration mode: persistent, DHCP & AutoIp, or AutoIp only.
    tPvIpConfig         ConfigMode;
    // IP configuration mode supported by the camera
    unsigned long       ConfigModeSupport;

    // Current IP configuration.  Ignored for PvCameraIpSettingsChange().  All
    // values are in network byte order (i.e. big endian).
    unsigned long       CurrentIpAddress;
    unsigned long       CurrentIpSubnet;
    unsigned long       CurrentIpGateway;

    // Persistent IP configuration.  See "ConfigMode" to enable persistent IP
    // settings.  All values are in network byte order.
    unsigned long       PersistentIpAddr;
    unsigned long       PersistentIpSubnet;
    unsigned long       PersistentIpGateway;

    unsigned long       _reserved1[8];

} tPvIpSettings;
#endif

int
gige_ip_status(FILE *fp, char *tag, tPvIpSettings pvIpSettings)
{
	struct in_addr addr;

	(void)fprintf(fp, "%s: ConfigMode %u\n", tag, pvIpSettings.ConfigMode);
	(void)fprintf(fp, "%s: ConfigModeSupport %lu\n", tag, pvIpSettings.ConfigModeSupport);

	addr.s_addr = pvIpSettings.CurrentIpAddress;
	(void)fprintf(fp, "%s:    CurrentIpAddress %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = pvIpSettings.CurrentIpSubnet;
	(void)fprintf(fp, "%s:     CurrentIpSubnet %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = pvIpSettings.CurrentIpGateway;
	(void)fprintf(fp, "%s:    CurrentIpGateway %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = pvIpSettings.PersistentIpAddr;
	(void)fprintf(fp, "%s:    PersistentIpAddr %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = pvIpSettings.PersistentIpSubnet;
	(void)fprintf(fp, "%s:  PersistentIpSubnet %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = pvIpSettings.PersistentIpGateway;
	(void)fprintf(fp, "%s: PersistentIpGateway %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	return(0);
}

#ifdef NOPE
{
    unsigned long       StructVer;	         // Version of this structure
    //---- Version 1 ----
    unsigned long       UniqueId;            // Unique value for each camera
    char                CameraName[32];      // People-friendly camera name (usually part name)
    char                ModelName[32];       // Name of camera part
    char                PartNumber[32];      // Manufacturer's part number
    char                SerialNumber[32];    // Camera's serial number
    char                FirmwareVersion[32]; // Camera's firmware version
    unsigned long       PermittedAccess;     // A combination of tPvAccessFlags
    unsigned long       InterfaceId;         // Unique value for each interface or bus
    tPvInterface        InterfaceType;       // Interface type; see tPvInterface

} tPvCameraInfoEx;
#endif

int
gige_camera_status(FILE *fp, char *tag, tPvCameraInfoEx info)
{
	(void)fprintf(fp, "%s: UniqueID %lu\n", tag, info.UniqueId);
	(void)fprintf(fp, "%s: CameraName %s\n", tag, info.CameraName);
	(void)fprintf(fp, "%s: ModelName %s\n", tag, info.ModelName);
	(void)fprintf(fp, "%s: PartNumber %s\n", tag, info.PartNumber);
	(void)fprintf(fp, "%s: SerialNumber %s\n", tag, info.SerialNumber);
	(void)fprintf(fp, "%s: FirmwareVersion %s\n", tag, info.FirmwareVersion);
	(void)fprintf(fp, "%s: PermittedAccess %lu\n", tag, info.PermittedAccess);
	(void)fprintf(fp, "%s: PermittedAccess: Monitor: %s\n", tag,
			bool(info.PermittedAccess & ePvAccessMonitor));
	(void)fprintf(fp, "%s: PermittedAccess: Master: %s\n", tag,
			bool(info.PermittedAccess & ePvAccessMaster));
	(void)fprintf(fp, "%s: InterfaceId %lu\n", tag, info.InterfaceId);
	(void)fprintf(fp, "%s: InterfaceType %u\n", tag, info.InterfaceType);
	(void)fprintf(fp, "%s: InterfaceType: Firewire: %s\n", tag,
			bool(info.InterfaceType & ePvInterfaceFirewire));
	(void)fprintf(fp, "%s: InterfaceType: Ethernet: %s\n", tag,
			bool(info.InterfaceType & ePvInterfaceEthernet));

	return(0);
}

#ifdef NOPE

//----- Out -----

tPvErr              Status;             // Status of this frame

unsigned long       ImageSize;          // Image size, in bytes
unsigned long       AncillarySize;      // Ancillary data size, in bytes

unsigned long       Width;              // Image width
unsigned long       Height;             // Image height
unsigned long       RegionX;            // Start of readout region (left)
unsigned long       RegionY;            // Start of readout region (top)
tPvImageFormat      Format;             // Image format
unsigned long       BitDepth;           // Number of significant bits
tPvBayerPattern     BayerPattern;       // Bayer pattern, if bayer format

unsigned long       FrameCount;         // Frame counter. Uses 16bit GigEVision BlockID. Rolls at 65535.
unsigned long       TimestampLo;        // Time stamp, lower 32-bits
unsigned long       TimestampHi;        // Time stamp, upper 32-bits

unsigned long       _reserved2[32];

#endif

#ifdef NOPE
int
gige_frame_status(FILE *fp, char *tag, tPvFrame Frame)
{
	(void)fprintf(fp, "%s: Status: (%d) %s\n", tag, Frame.Status, gige_status(Frame.Status));
	(void)fprintf(fp, "%s: ImageSize: %lu\n", tag, Frame.ImageSize);
	(void)fprintf(fp, "%s: AncillarySize: %lu\n", tag, Frame.AncillarySize);
	(void)fprintf(fp, "%s: Width: %lu\n", tag, Frame.Width);
	(void)fprintf(fp, "%s: Height: %lu\n", tag, Frame.Height);
	(void)fprintf(fp, "%s: RegionX: %lu\n", tag, Frame.RegionX);
	(void)fprintf(fp, "%s: RegionY: %lu\n", tag, Frame.RegionY);
	(void)fprintf(fp, "%s: Format: %d\n", tag, Frame.Format);
	(void)fprintf(fp, "%s: BitDepth: %lu\n", tag, Frame.BitDepth);
	(void)fprintf(fp, "%s: BayerPattern: %d\n", tag, Frame.BayerPattern);
	(void)fprintf(fp, "%s: FrameCount: %lu\n", tag, Frame.FrameCount);
	(void)fprintf(fp, "%s: TimestampLo: %lu\n", tag, Frame.TimestampLo);
	(void)fprintf(fp, "%s: TimestampHi: %lu\n", tag, Frame.TimestampHi);

	return(0);
}
#else
int
gige_frame_status(FILE *fp, char *tag, tPvFrame Frame)
{
	double dt;

	if (Frame.Status != ePvErrSuccess) {
		(void)fprintf(fp, "%s: Status: (%d) %s\n", tag, Frame.Status, gige_status(Frame.Status));
	}
	dt = ((double)Frame.TimestampHi * pow(2, 32) + (double)Frame.TimestampLo) / 36e6;
	(void)fprintf(fp, "%s: FrameCount %6lu Timestamp %3lu %10lu (%12.3f) ImageSize %lu Geometry %lux%lu+%lu+%lu-%lu\n",
			tag,
			Frame.FrameCount,
			Frame.TimestampHi, Frame.TimestampLo, dt,
			Frame.ImageSize,
			Frame.Width, Frame.Height, Frame.RegionX, Frame.RegionY, Frame.BitDepth);

	return(0);
}
#endif

/**
 * Writes a FITS file from a raw Manta image.
 * We do it without any frills: no bscale or bzero, 1-byte depth...
 */

int
gige_fits_write(FILE *fp, char *tag, char *image, int nx, int ny, char *history)
{
	char buf[BUFSIZ];			// fits card data
	int ix;						// image index
	int nh = 0;					// number of header cards
	size_t n_ask, n_got;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: image nh %d nx %d ny %d\n", tag, nh, nx, ny);
#endif

	/********************/
	/* write the header */
	/********************/

	(void)fprintf(fp, "%10.10s%20.20s%-50.50s", "SIMPLE  = ", "T", "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "BITPIX  = ", 8, " /8-BIT UNSIGNED INTEGERS");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS   = ", 2, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS1  = ", nx, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS2  = ", ny, "");
	nh++;

	(void)fprintf(fp, "%10.10s%-20.20s%-50.50s", "DATATYPE= ", "'INTEGER*1'", "");
	nh++;

	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", history);
	nh++;

	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", "Written by $RCSfile: gige.c,v $)");
	nh++;

	(void)fprintf(fp, "%-80.80s", "END");
	nh++;

	/* round out the header record */
#ifdef DEBUG
	(void)fprintf(stderr, "%s: %d cards out, rounding...\n", tag, nh);
#endif
	memset(buf, ' ', 80);
	for (ix = nh; ix%36 != 0; ix++) {
		n_ask = 80;
		n_got = fwrite((void *)buf, 1, 80, fp);
		if (n_ask != n_got) {
			(void)fprintf(stderr, "%s: bad write on card %d\n", tag, ix);
		}
	}

	// write out the data
	int balance = 2880 - ((nx*ny) % 2880);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: balance %d\n", tag, balance);
#endif
	(void)fwrite(image, 1, nx*ny + balance, fp);

	if (fflush(fp)) {
		(void)fprintf(stderr, "%s: can't flush output stream\n", tag);
		return(1);
	}

	return(0);
}

