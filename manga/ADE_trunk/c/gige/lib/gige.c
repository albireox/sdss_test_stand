/* file: $RCSfile: gige.c,v $
** rcsid: $Id: gige.c,v 1.4 2013/10/31 19:30:10 jwp Exp $
** Copyright Jeffrey W Percival
** *******************************************************************
** Space Astronomy Laboratory
** University of Wisconsin
** 1150 University Avenue
** Madison, WI 53706 USA
** *******************************************************************
** Do not use this software without permission.
** Do not use this software without attribution.
** Do not remove or alter any of the lines above.
** *******************************************************************
*/
static char *rcsid __attribute__ ((unused)) = "$Id: gige.c,v 1.4 2013/10/31 19:30:10 jwp Exp $";

/*
** *******************************************************************
** $RCSfile: gige.c,v $ - gige support
** *******************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#if defined(_LINUX) || defined(_QNX) || defined(_OSX)
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <arpa/inet.h>
#endif

#include "PvApi.h"

#include "gige.h"

#define DEBUG

// generic boolean function
static char *
bool(unsigned long ul)
{
	char *b = "false";
	if (ul) {
		b = "true";
	}
	return(b);
}


/**
 * Writes a FITS file from a raw Manta image.
 * We do it without any frills: no bscale or bzero, 1-byte depth...
 */

int
gigeFitsWrite(FILE *fp, char *tag, char *image, int nx, int ny, char *history)
{
	char buf[BUFSIZ];			// fits card data
	int ix;						// image index
	int nh = 0;					// number of header cards
	size_t n_ask, n_got;

#ifdef DEBUG
	(void)fprintf(stderr, "%s: image nh %d nx %d ny %d\n", tag, nh, nx, ny);
#endif

	/********************/
	/* write the header */
	/********************/

	(void)fprintf(fp, "%10.10s%20.20s%-50.50s", "SIMPLE  = ", "T", "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "BITPIX  = ", 8, " /8-BIT UNSIGNED INTEGERS");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS   = ", 2, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS1  = ", nx, "");
	nh++;

	(void)fprintf(fp, "%10.10s%20d%-50.50s", "NAXIS2  = ", ny, "");
	nh++;

	(void)fprintf(fp, "%10.10s%-20.20s%-50.50s", "DATATYPE= ", "'INTEGER*1'", "");
	nh++;

	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", history);
	nh++;

	(void)fprintf(fp, "%-10.10s%-70.70s", "HISTORY", "Written by $RCSfile: gige.c,v $)");
	nh++;

	(void)fprintf(fp, "%-80.80s", "END");
	nh++;

	/* round out the header record */
#ifdef DEBUG
	(void)fprintf(stderr, "%s: %d cards out, rounding...\n", tag, nh);
#endif
	memset(buf, ' ', 80);
	for (ix = nh; ix%36 != 0; ix++) {
		n_ask = 80;
		n_got = fwrite((void *)buf, 1, 80, fp);
		if (n_ask != n_got) {
			(void)fprintf(stderr, "%s: bad write on card %d\n", tag, ix);
		}
	}

	// write out the data
	int balance = 2880 - ((nx*ny) % 2880);
#ifdef DEBUG
	(void)fprintf(stderr, "%s: balance %d\n", tag, balance);
#endif
	(void)fwrite(image, 1, nx*ny + balance, fp);

	if (fflush(fp)) {
		(void)fprintf(stderr, "%s: can't flush output stream\n", tag);
		return(1);
	}

	return(0);
}


/**
 * Returns a text string for the given status code.
 */

char *
gigePvErrName(tPvErr PvErr)
{
	char *errorName = "unknown";

	switch (PvErr) {
	case ePvErrSuccess:
		errorName = "No error";
		break;
	case ePvErrCameraFault:
		errorName = "Unexpected camera fault";
		break;
	case ePvErrInternalFault:
		errorName = "Unexpected fault in PvApi or driver";
		break;
	case ePvErrBadHandle:
		errorName = "Camera handle is invalid";
		break;
	case ePvErrBadParameter:
		errorName = "Bad parameter to API call";
		break;
	case ePvErrBadSequence:
		errorName = "Sequence of API calls is incorrect";
		break;
	case ePvErrNotFound:
		errorName = "Camera or attribute not found";
		break;
	case ePvErrAccessDenied:
		errorName = "Camera cannot be opened in the specified mode";
		break;
	case ePvErrUnplugged:
		errorName = "Camera was unplugged";
		break;
	case ePvErrInvalidSetup:
		errorName = "Setup is invalid (an attribute is invalid)";
		break;
	case ePvErrResources:
		errorName = "System/network resources or memory not available";
		break;
	case ePvErrBandwidth:
		errorName = "1394 bandwidth not available";
		break;
	case ePvErrQueueFull:
		errorName = "Too many frames on queue";
		break;
	case ePvErrBufferTooSmall:
		errorName = "Frame buffer is too small";
		break;
	case ePvErrCancelled:
		errorName = "Frame cancelled by user";
		break;
	case ePvErrDataLost:
		errorName = "The data for the frame was lost";
		break;
	case ePvErrDataMissing:
		errorName = "Some data in the frame is missing";
		break;
	case ePvErrTimeout:
		errorName = "Timeout during wait";
		break;
	case ePvErrOutOfRange:
		errorName = "Attribute value is out of the expected range";
		break;
	case ePvErrWrongType:
		errorName = "Attribute is not this type (wrong access function)";
		break;
	case ePvErrForbidden:
		errorName = "Attribute write forbidden at this time";
		break;
	case ePvErrUnavailable:
		errorName = "Attribute is not available at this time";
		break;
	case ePvErrFirewall:
		errorName = "A firewall is blocking the traffic (Windows only)";
		break;
	default:
		errorName = "Unknown error";
		break;
	}

	return(errorName);
}

int
gigeFmtPvCameraInfoEx(FILE *fp, char *tag, tPvCameraInfoEx PvCameraInfoEx)
{
	(void)fprintf(fp, "%s: UniqueID:        %lu\n", tag, PvCameraInfoEx.UniqueId);
	(void)fprintf(fp, "%s: CameraName:      %s\n", tag, PvCameraInfoEx.CameraName);
	(void)fprintf(fp, "%s: ModelName:       %s\n", tag, PvCameraInfoEx.ModelName);
	(void)fprintf(fp, "%s: PartNumber:      %s\n", tag, PvCameraInfoEx.PartNumber);
	(void)fprintf(fp, "%s: SerialNumber:    %s\n", tag, PvCameraInfoEx.SerialNumber);
	(void)fprintf(fp, "%s: FirmwareVersion: %s\n", tag, PvCameraInfoEx.FirmwareVersion);
	(void)fprintf(fp, "%s: PermittedAccess: %lu\n", tag, PvCameraInfoEx.PermittedAccess);
	(void)fprintf(fp, "%s: PermittedAccess: Monitor: %s\n", tag,
			bool(PvCameraInfoEx.PermittedAccess & ePvAccessMonitor));
	(void)fprintf(fp, "%s: PermittedAccess: Master: %s\n", tag,
			bool(PvCameraInfoEx.PermittedAccess & ePvAccessMaster));
	(void)fprintf(fp, "%s: InterfaceId:     %lu\n", tag, PvCameraInfoEx.InterfaceId);
	(void)fprintf(fp, "%s: InterfaceType:   %u\n", tag, PvCameraInfoEx.InterfaceType);
	(void)fprintf(fp, "%s: InterfaceType:   Firewire: %s\n", tag,
			bool(PvCameraInfoEx.InterfaceType & ePvInterfaceFirewire));
	(void)fprintf(fp, "%s: InterfaceType:   Ethernet: %s\n", tag,
			bool(PvCameraInfoEx.InterfaceType & ePvInterfaceEthernet));

	return(0);
}

int
gigeFmtPvIpSettings(FILE *fp, char *tag, tPvIpSettings PvIpSettings)
{
	struct in_addr addr;

	(void)fprintf(fp, "%s: ConfigMode %u\n", tag, PvIpSettings.ConfigMode);
	(void)fprintf(fp, "%s: ConfigModeSupport %lu\n", tag, PvIpSettings.ConfigModeSupport);

	addr.s_addr = PvIpSettings.CurrentIpAddress;
	(void)fprintf(fp, "%s:    CurrentIpAddress %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = PvIpSettings.CurrentIpSubnet;
	(void)fprintf(fp, "%s:     CurrentIpSubnet %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = PvIpSettings.CurrentIpGateway;
	(void)fprintf(fp, "%s:    CurrentIpGateway %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = PvIpSettings.PersistentIpAddr;
	(void)fprintf(fp, "%s:    PersistentIpAddr %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = PvIpSettings.PersistentIpSubnet;
	(void)fprintf(fp, "%s:  PersistentIpSubnet %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	addr.s_addr = PvIpSettings.PersistentIpGateway;
	(void)fprintf(fp, "%s: PersistentIpGateway %u (%s)\n", tag, addr.s_addr, inet_ntoa(addr));

	return(0);
}

int
gigeFmtPvFrame(FILE *fp, char *tag, tPvFrame PvFrame)
{
	double dt;

	if (PvFrame.Status != ePvErrSuccess) {
		(void)fprintf(fp, "%s: Status: (%d) %s\n", tag, PvFrame.Status, gigePvErrName(PvFrame.Status));
	}
	dt = ((double)PvFrame.TimestampHi * pow(2, 32) + (double)PvFrame.TimestampLo) / 36e6;
	(void)fprintf(fp, "%s: FrameCount %6lu Timestamp %3lu %10lu (%12.3f) ImageSize %lu Geometry %lux%lu+%lu+%lu-%lu\n",
			tag,
			PvFrame.FrameCount,
			PvFrame.TimestampHi, PvFrame.TimestampLo, dt,
			PvFrame.ImageSize,
			PvFrame.Width, PvFrame.Height, PvFrame.RegionX, PvFrame.RegionY, PvFrame.BitDepth);

	return(0);
}

int
gigeFmtPvFrameVerbose(FILE *fp, char *tag, tPvFrame PvFrame)
{
	(void)fprintf(fp, "%s: Status:        (%d) %s\n", tag, PvFrame.Status, gigePvErrName(PvFrame.Status));
	(void)fprintf(fp, "%s: ImageSize:     %lu\n", tag, PvFrame.ImageSize);
	(void)fprintf(fp, "%s: AncillarySize: %lu\n", tag, PvFrame.AncillarySize);
	(void)fprintf(fp, "%s: Width:         %lu\n", tag, PvFrame.Width);
	(void)fprintf(fp, "%s: Height:        %lu\n", tag, PvFrame.Height);
	(void)fprintf(fp, "%s: RegionX:       %lu\n", tag, PvFrame.RegionX);
	(void)fprintf(fp, "%s: RegionY:       %lu\n", tag, PvFrame.RegionY);
	(void)fprintf(fp, "%s: Format:        %d\n",  tag, PvFrame.Format);
	(void)fprintf(fp, "%s: BitDepth:      %lu\n", tag, PvFrame.BitDepth);
	(void)fprintf(fp, "%s: BayerPattern:  %d\n",  tag, PvFrame.BayerPattern);
	(void)fprintf(fp, "%s: FrameCount:    %lu\n", tag, PvFrame.FrameCount);
	(void)fprintf(fp, "%s: TimestampLo:   %lu\n", tag, PvFrame.TimestampLo);
	(void)fprintf(fp, "%s: TimestampHi:   %lu\n", tag, PvFrame.TimestampHi);

	return(0);
}

#define AVT_GETTERS

int
gigeGetAttributeList(FILE * fp, char *tag, tPvHandle Camera)
{
	int rcode = 0;
	tPvAttrListPtr ListPtr;
	tPvErr PvErr;
	unsigned long Length;

	PvErr = PvAttrList(Camera, &ListPtr, &Length);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, "PvAttrList", gigePvErrName(PvErr));
		rcode += 1;
	}
	{
		int i;
		for (i = 0; i < Length; i++) {
			const char *attributeName = ListPtr[i];
			(void)fprintf(fp, "%s: Attribute %s\n", tag, attributeName);
		}
	}

	return(rcode);
}

int
gigeGetInfoFirmware(AVT_INFO_FIRMWARE *InfoFirmware, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	p = "FirmwareVerBuild";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoFirmware->FirmwareVerBuild = Value;

	p = "FirmwareVerMajor";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoFirmware->FirmwareVerMajor = Value;

	p = "FirmwareVerMinor";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoFirmware->FirmwareVerMinor = Value;

	return(rcode);
}

int
gigeGetInfoPart(AVT_INFO_PART *InfoPart, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "PartClass";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoPart->PartClass = Value;

	p = "PartNumber";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoPart->PartNumber = Value;

	p = "PartRevision";
	PvErr = PvAttrStringGet(Camera, p, InfoPart->PartRevision, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "PartVersion";
	PvErr = PvAttrStringGet(Camera, p, InfoPart->PartVersion, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "SerialNumber";
	//PvErr = PvAttrStringGet(Camera, p, InfoPart->SerialNumber, AVT_STRLEN, &Size);
	(void)strcpy(InfoPart->SerialNumber, "Symbol Not Found");
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	return(rcode);
}

int
gigeGetInfoSensor(AVT_INFO_SENSOR *InfoSensor, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "SensorBits";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoSensor->SensorBits = Value;

	p = "SensorHeight";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoSensor->SensorHeight = Value;

	p = "SensorType";
	PvErr = PvAttrEnumGet(Camera, p, InfoSensor->SensorType, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "SensorWidth";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	InfoSensor->SensorWidth = Value;

	return(rcode);
}

int
gigeGetInfo(AVT_INFO *Info, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "CameraName";
	PvErr = PvAttrStringGet(Camera, p, Info->CameraName, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "DeviceFirmwareVersion";
	PvErr = PvAttrStringGet(Camera, p, Info->DeviceFirmwareVersion, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "DeviceModelName";
	PvErr = PvAttrStringGet(Camera, p, Info->DeviceModelName, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "DevicePartNumber";
	PvErr = PvAttrStringGet(Camera, p, Info->DevicePartNumber, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	PvErr = PvAttrStringGet(Camera, "DeviceSerialNumber", Info->DeviceSerialNumber, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	PvErr = PvAttrStringGet(Camera, "DeviceVendorName", Info->DeviceVendorName, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	rcode += gigeGetInfoFirmware(&Info->InfoFirmware, tag, Camera);

	rcode += gigeGetInfoPart(&Info->InfoPart, tag, Camera);

	rcode += gigeGetInfoSensor(&Info->InfoSensor, tag, Camera);

	p = "UniqueId";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Info->UniqueId = Value;

	return(rcode);
}

int
gigeGetImageMode(AVT_IMAGE_MODE *ImageMode, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	p = "BinningX";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageMode->BinningX = Value;

	p = "BinningY";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageMode->BinningY = Value;

	p = "DecimationHorizontal";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageMode->DecimationHorizontal = Value;

	p = "DecimationVertical";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageMode->DecimationVertical = Value;

	return(rcode);
}

int
gigeGetAcquisitionTriggerFrameStart(AVT_ACQUISITION_TRIGGER_FRAME_START *FrameStart, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "FrameStartTriggerDelay";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	FrameStart->FrameStartTriggerDelay = Value;

	p = "FrameStartTriggerEvent";
	PvErr = PvAttrEnumGet(Camera, p, FrameStart->FrameStartTriggerEvent, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "FrameStartTriggerMode";
	PvErr = PvAttrEnumGet(Camera, p, FrameStart->FrameStartTriggerMode, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "FrameStartTriggerOverlap";
	PvErr = PvAttrEnumGet(Camera, p, FrameStart->FrameStartTriggerOverlap, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	return(rcode);
}

int
gigeGetAcquisitionTrigger(AVT_ACQUISITION_TRIGGER *AcquisitionTrigger, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvFloat32 Value;

	p = "FrameRate";
	PvErr = PvAttrFloat32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	AcquisitionTrigger->FrameRate = Value;

	rcode += gigeGetAcquisitionTriggerFrameStart(&AcquisitionTrigger->FrameStart, tag, Camera);

	return(rcode);
}

int
gigeGetAcquisition(AVT_ACQUISITION *Acquisition, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	rcode += gigeGetAcquisitionTrigger(&Acquisition->Trigger, tag, Camera);

	p = "AcquisitionFrameCount";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Acquisition->AcquisitionFrameCount = Value;

	p = "AcquisitionMode";
	PvErr = PvAttrEnumGet(Camera, p, Acquisition->AcquisitionMode, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "RecorderPreEventCount";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Acquisition->RecorderPreEventCount = Value;

	return(rcode);
}

int
gigeGetImageFormatROI(AVT_IMAGE_FORMAT_ROI *ImageFormatROI, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	p = "Height";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageFormatROI->Height = Value;

	p = "RegionX";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageFormatROI->RegionX = Value;

	p = "RegionY";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageFormatROI->RegionY = Value;

	p = "Width";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageFormatROI->Width = Value;

	return(rcode);
}

int
gigeGetImageFormat(AVT_IMAGE_FORMAT *ImageFormat, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	rcode += gigeGetImageFormatROI(&ImageFormat->ROI, tag, Camera);

	p = "PixelFormat";
	PvErr = PvAttrEnumGet(Camera, p, ImageFormat->PixelFormat, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "TotalBytesPerFrame";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	ImageFormat->TotalBytesPerFrame = Value;

	return(rcode);
}

int
gigeGetControlsExposure(AVT_CONTROLS_EXPOSURE *Exposure, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "ExposureMode";
	PvErr = PvAttrEnumGet(Camera, p, Exposure->ExposureMode, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "ExposureValue";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Exposure->ExposureValue = Value;

	return(rcode);
}

int
gigeGetControlsGain(AVT_CONTROLS_GAIN *Gain, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "GainMode";
	PvErr = PvAttrEnumGet(Camera, p, Gain->GainMode, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "GainValue";
	PvErr = PvAttrUint32Get(Camera, p , &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gain->GainValue = Value;

	return(rcode);
}

int
gigeGetControls(AVT_CONTROLS *Controls, char *tag, tPvHandle Camera)
{
	int rcode = 0;

	rcode += gigeGetControlsExposure(&Controls->Exposure, tag, Camera);
	rcode += gigeGetControlsGain(&Controls->Gain, tag, Camera);

	return(rcode);
}

int
gigeGetGigeEthernet(AVT_GIGE_ETHERNET *Ethernet, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;

	p = "DeviceEthAddress";
	PvErr = PvAttrStringGet(Camera, p, Ethernet->DeviceEthAddress, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "HostEthAddress";
	PvErr = PvAttrStringGet(Camera, p, Ethernet->HostEthAddress, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	return(rcode);
}

int
gigeGetGigeIP(AVT_GIGE_IP *IP, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;

	p = "DeviceIPAddress";
	PvErr = PvAttrStringGet(Camera, p, IP->DeviceIPAddress, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "HostIPAddress";
	PvErr = PvAttrStringGet(Camera, p, IP->HostIPAddress, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	return(rcode);
}

int
gigeGetGigeTimestamp(AVT_GIGE_TIMESTAMP *Timestamp, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	p = "TimeStampFrequency";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Timestamp->TimeStampFrequency = Value;

	p = "TimeStampValueHi";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Timestamp->TimeStampValueHi = Value;

	p = "TimeStampValueLo";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Timestamp->TimeStampValueLo = Value;

	return(rcode);
}

int
gigeGetGige(AVT_GIGE *Gige, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "BandwidthControlMode";
	PvErr = PvAttrEnumGet(Camera, p, Gige->BandwidthControlMode, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "NonImagePayloadSize";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->NonImagePayloadSize = Value;

	p = "PayloadSize";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->PayloadSize = Value;

	rcode += gigeGetGigeEthernet(&Gige->Ethernet, tag, Camera);
	rcode += gigeGetGigeIP(&Gige->IP, tag, Camera);

	p = "HeartbeatInterval";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->HeartbeatInterval = Value;

	p = "HeartbeatTimeout";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->HeartbeatTimeout = Value;

	p = "PacketSize";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->PacketSize = Value;

	p = "StreamBytesPerSecond";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Gige->StreamBytesPerSecond = Value;

	rcode += gigeGetGigeTimestamp(&Gige->Timestamp, tag, Camera);

	return(rcode);
}

int
gigeGetStats(AVT_STATS *Stats, char *tag, tPvHandle Camera)
{
	char *p;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Size;
	tPvUint32 Value;

	p = "StatDriverType";
	PvErr = PvAttrEnumGet(Camera, p, Stats->StatDriverType, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "StatFilterVersion";
	PvErr = PvAttrStringGet(Camera, p, Stats->StatFilterVersion, AVT_STRLEN, &Size);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "StatFrameRate";
	PvErr = PvAttrFloat32Get(Camera, "StatFrameRate", &Stats->StatFrameRate);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}

	p = "StatFramesCompleted";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatFramesCompleted = Value;

	p = "StatFramesDropped";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatFramesDropped = Value;

	p = "StatPacketsErroneous";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatPacketsErroneous = Value;

	p = "StatPacketsMissed";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatPacketsMissed = Value;

	p = "StatPacketsReceived";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatPacketsReceived = Value;

	p = "StatPacketsRequested";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatPacketsRequested = Value;

	p = "StatPacketsResent";
	PvErr = PvAttrUint32Get(Camera, p, &Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, p, gigePvErrName(PvErr));
		rcode += 1;
	}
	Stats->StatPacketsResent = Value;

	return(rcode);
}

#define FORMATTERS

int
gigeFmtInfoFirmware(FILE *fp, char *tag, AVT_INFO_FIRMWARE InfoFirmware)
{

	(void)fprintf(fp, "%s:         Info:               Firmware:   FirmwareVerBuild: %lu\n", tag, InfoFirmware.FirmwareVerBuild);
	(void)fprintf(fp, "%s:         Info:               Firmware:   FirmwareVerMajor: %lu\n", tag, InfoFirmware.FirmwareVerMajor);
	(void)fprintf(fp, "%s:         Info:               Firmware:   FirmwareVerMinor: %lu\n", tag, InfoFirmware.FirmwareVerMinor);

	return(0);
}

int
gigeFmtInfoPart(FILE *fp, char *tag, AVT_INFO_PART InfoPart)
{

	(void)fprintf(fp, "%s:         Info:                   Part:          PartClass: %lu\n", tag, InfoPart.PartClass);
	(void)fprintf(fp, "%s:         Info:                   Part:         PartNumber: %lu\n", tag, InfoPart.PartNumber);
	(void)fprintf(fp, "%s:         Info:                   Part:       PartRevision: %s\n", tag, InfoPart.PartRevision);
	(void)fprintf(fp, "%s:         Info:                   Part:        PartVersion: %s\n", tag, InfoPart.PartVersion);
	(void)fprintf(fp, "%s:         Info:                   Part:       SerialNumber: %s\n", tag, InfoPart.SerialNumber);

	return(0);
}

int
gigeFmtInfoSensor(FILE *fp, char *tag, AVT_INFO_SENSOR InfoSensor)
{

	(void)fprintf(fp, "%s:         Info:                 Sensor:         SensorBits: %lu\n", tag, InfoSensor.SensorBits);
	(void)fprintf(fp, "%s:         Info:                 Sensor:       SensorHeight: %lu\n", tag, InfoSensor.SensorHeight);
	(void)fprintf(fp, "%s:         Info:                 Sensor:         SensorType: %s\n", tag, InfoSensor.SensorType);
	(void)fprintf(fp, "%s:         Info:                 Sensor:        SensorWidth: %lu\n", tag, InfoSensor.SensorWidth);

	return(0);
}

int
gigeFmtInfo(FILE *fp, char *tag, AVT_INFO Info)
{
	(void)fprintf(fp, "%s:         Info: ---------------------------------------------------------\n", tag);
	(void)fprintf(fp, "%s:         Info:             CameraName: %s\n", tag, Info.CameraName);
	(void)fprintf(fp, "%s:         Info:  DeviceFirmwareVersion: %s\n", tag, Info.DeviceFirmwareVersion);
	(void)fprintf(fp, "%s:         Info:        DeviceModelName: %s\n", tag, Info.DeviceModelName);
	(void)fprintf(fp, "%s:         Info:       DevicePartNumber: %s\n", tag, Info.DevicePartNumber);
	(void)fprintf(fp, "%s:         Info:     DeviceSerialNumber: %s\n", tag, Info.DeviceSerialNumber);
	(void)fprintf(fp, "%s:         Info:       DeviceVendorName: %s\n", tag, Info.DeviceVendorName);
	(void)gigeFmtInfoFirmware(fp, tag, Info.InfoFirmware);
	(void)gigeFmtInfoPart(fp, tag, Info.InfoPart);
	(void)gigeFmtInfoSensor(fp, tag, Info.InfoSensor);
	(void)fprintf(fp, "%s:         Info:               UniqueID: %lu\n", tag, Info.UniqueId);

	return(0);
}

int
gigeFmtImageMode(FILE *fp, char *tag, AVT_IMAGE_MODE ImageMode)
{
	(void)fprintf(fp, "%s:    ImageMode:               BinningX: %lu\n", tag, ImageMode.BinningX);
	(void)fprintf(fp, "%s:    ImageMode:               BinningY: %lu\n", tag, ImageMode.BinningY);
	(void)fprintf(fp, "%s:    ImageMode:   DecimationHorizontal: %lu\n", tag, ImageMode.DecimationHorizontal);
	(void)fprintf(fp, "%s:    ImageMode:     DecimationVertical: %lu\n", tag, ImageMode.DecimationVertical);

	return(0);
}

int
gigeFmtAcquisitionTriggerFrameStart(FILE *fp, char *tag, AVT_ACQUISITION_TRIGGER_FRAME_START FrameStart)
{

	(void)fprintf(fp, "%s:  Acquisition:                Trigger:         FrameStart:   FrameStartTriggerDelay: %lu\n", tag, FrameStart.FrameStartTriggerDelay);
	(void)fprintf(fp, "%s:  Acquisition:                Trigger:         FrameStart:   FrameStartTriggerEvent: %s\n", tag, FrameStart.FrameStartTriggerEvent);
	(void)fprintf(fp, "%s:  Acquisition:                Trigger:         FrameStart:    FrameStartTriggerMode: %s\n", tag, FrameStart.FrameStartTriggerMode);
	(void)fprintf(fp, "%s:  Acquisition:                Trigger:         FrameStart: FrameStartTriggerOverlap: %s\n", tag, FrameStart.FrameStartTriggerOverlap);

	return(0);
}

int
gigeFmtAcquisitionTrigger(FILE *fp, char *tag, AVT_ACQUISITION_TRIGGER Trigger)
{
	(void)fprintf(fp, "%s:  Acquisition:                Trigger:          FrameRate: %f\n", tag, Trigger.FrameRate);
	gigeFmtAcquisitionTriggerFrameStart(fp, tag, Trigger.FrameStart);

	return(0);
}

int
gigeFmtAcquisition(FILE *fp, char *tag, AVT_ACQUISITION Acquisition)
{
	gigeFmtAcquisitionTrigger(fp, tag, Acquisition.Trigger);
	(void)fprintf(fp, "%s:  Acquisition:  AcquisitionFrameCount: %lu\n", tag, Acquisition.AcquisitionFrameCount);
	(void)fprintf(fp, "%s:  Acquisition:        AcquisitionMode: %s\n", tag, Acquisition.AcquisitionMode);
	(void)fprintf(fp, "%s:  Acquisition:  RecorderPreEventCount: %lu\n", tag, Acquisition.RecorderPreEventCount);

	return(0);
}

int
gigeFmtImageFormatROI(FILE *fp, char *tag, AVT_IMAGE_FORMAT_ROI ROI)
{
	(void)fprintf(fp, "%s:  ImageFormat:                    ROI:             Height: %lu\n", tag, ROI.Height);
	(void)fprintf(fp, "%s:  ImageFormat:                    ROI:            RegionX: %lu\n", tag, ROI.RegionX);
	(void)fprintf(fp, "%s:  ImageFormat:                    ROI:            RegionY: %lu\n", tag, ROI.RegionY);
	(void)fprintf(fp, "%s:  ImageFormat:                    ROI:              Width: %lu\n", tag, ROI.Width);

	return(0);
}

int
gigeFmtImageFormat(FILE *fp, char *tag, AVT_IMAGE_FORMAT ImageFormat)
{
	gigeFmtImageFormatROI(fp, tag, ImageFormat.ROI);
	(void)fprintf(fp, "%s:  ImageFormat:            PixelFormat: %s\n", tag, ImageFormat.PixelFormat);
	(void)fprintf(fp, "%s:  ImageFormat:     TotalBytesPerFrame: %lu\n", tag, ImageFormat.TotalBytesPerFrame);

	return(0);
}

int
gigeFmtControlsExposure(FILE *fp, char *tag, AVT_CONTROLS_EXPOSURE Exposure)
{
	(void)fprintf(fp, "%s:     Controls:               Exposure:       ExposureMode: %s\n", tag, Exposure.ExposureMode);
	(void)fprintf(fp, "%s:     Controls:               Exposure:      ExposureValue: %lu\n", tag, Exposure.ExposureValue);

	return(0);
}

int
gigeFmtControlsGain(FILE *fp, char *tag, AVT_CONTROLS_GAIN Gain)
{
	(void)fprintf(fp, "%s:     Controls:                   Gain:           GainMode: %s\n", tag, Gain.GainMode);
	(void)fprintf(fp, "%s:     Controls:                   Gain:          GainValue: %lu\n", tag, Gain.GainValue);

	return(0);
}

int
gigeFmtControls(FILE *fp, char *tag, AVT_CONTROLS Controls)
{
	gigeFmtControlsExposure(fp, tag, Controls.Exposure);
	gigeFmtControlsGain(fp, tag, Controls.Gain);

	return(0);
}

int
gigeFmtGigeEthernet(FILE *fp, char *tag, AVT_GIGE_ETHERNET Ethernet)
{
	(void)fprintf(fp, "%s:         Gige:               Ethernet:   DeviceEthAddress: %s\n", tag, Ethernet.DeviceEthAddress);
	(void)fprintf(fp, "%s:         Gige:               Ethernet:     HostEthAddress: %s\n", tag, Ethernet.HostEthAddress);

	return(0);
}

int
gigeFmtGigeIP(FILE *fp, char *tag, AVT_GIGE_IP IP)
{
	(void)fprintf(fp, "%s:         Gige:                     IP:    DeviceIPAddress: %s\n", tag, IP.DeviceIPAddress);
	(void)fprintf(fp, "%s:         Gige:                     IP:      HostIPAddress: %s\n", tag, IP.HostIPAddress);

	return(0);
}

int
gigeFmtGigeTimestamp(FILE *fp, char *tag, AVT_GIGE_TIMESTAMP Timestamp)
{
	(void)fprintf(fp, "%s:         Gige:              Timestamp: TimeStampFrequency: %lu\n", tag, Timestamp.TimeStampFrequency);
	(void)fprintf(fp, "%s:         Gige:              Timestamp:   TimeStampValueHi: %lu\n", tag, Timestamp.TimeStampValueHi);
	(void)fprintf(fp, "%s:         Gige:              Timestamp:   TimeStampValueLo: %lu\n", tag, Timestamp.TimeStampValueLo);

	return(0);
}

int
gigeFmtGige(FILE *fp, char *tag, AVT_GIGE Gige)
{
	(void)fprintf(fp, "%s:         Gige:   BandwidthControlMode: %s\n", tag, Gige.BandwidthControlMode);
	(void)fprintf(fp, "%s:         Gige:    NonImagePayloadSize: %lu\n", tag, Gige.NonImagePayloadSize);
	gigeFmtGigeEthernet(fp, tag, Gige.Ethernet);
	gigeFmtGigeIP(fp, tag, Gige.IP);
	(void)fprintf(fp, "%s:         Gige:      HeartbeatInterval: %lu\n", tag, Gige.HeartbeatInterval);
	(void)fprintf(fp, "%s:         Gige:       HeartbeatTimeout: %lu\n", tag, Gige.HeartbeatTimeout);
	(void)fprintf(fp, "%s:         Gige:             PacketSize: %lu\n", tag, Gige.PacketSize);
	(void)fprintf(fp, "%s:         Gige:   StreamBytesPerSecond: %lu\n", tag, Gige.StreamBytesPerSecond);
	gigeFmtGigeTimestamp(fp, tag, Gige.Timestamp);

	return(0);
}

int
gigeFmtStats(FILE *fp, char *tag, AVT_STATS Stats)
{
	(void)fprintf(fp, "%s:        Stats:         StatDriverType: %s\n", tag, Stats.StatDriverType);
	(void)fprintf(fp, "%s:        Stats:      StatFilterVersion: %s\n", tag, Stats.StatFilterVersion);
	(void)fprintf(fp, "%s:        Stats:          StatFrameRate: %f\n", tag, Stats.StatFrameRate);
	(void)fprintf(fp, "%s:        Stats:    StatFramesCompleted: %lu\n", tag, Stats.StatFramesCompleted);
	(void)fprintf(fp, "%s:        Stats:      StatFramesDropped: %lu\n", tag, Stats.StatFramesDropped);
	(void)fprintf(fp, "%s:        Stats:   StatPacketsErroneous: %lu\n", tag, Stats.StatPacketsErroneous);
	(void)fprintf(fp, "%s:        Stats:      StatPacketsMissed: %lu\n", tag, Stats.StatPacketsMissed);
	(void)fprintf(fp, "%s:        Stats:    StatPacketsReceived: %lu\n", tag, Stats.StatPacketsReceived);
	(void)fprintf(fp, "%s:        Stats:   StatPacketsRequested: %lu\n", tag, Stats.StatPacketsRequested);
	(void)fprintf(fp, "%s:        Stats:      StatPacketsResent: %lu\n", tag, Stats.StatPacketsResent);

	return(0);
}

#define SETTERS

#ifdef NOPE
PvInitialize
PvCameraListEx
PvCameraOpen
PvCameraCount
PvVersion
PvCaptureAdjustPacketSize
PvCaptureStart
PvCaptureQueueFrame
PvCommandRun - TimeStampReset
PvCommandRun - AcquisitionStart
	PvCommandRun - FrameStartTriggerSoftware
	PvCommandRun - TimeStampValueLatch
	PvCaptureWaitForFrameDone
	PvCaptureQueueFrame
	PvCaptureQueueClear
	PvCommandRun - AcquisitionAbort
PvCommandRun - AcquisitionStop
PvCaptureEnd
PvCameraClose
PvUnInitialize
#endif

int
gigeSetAcquisitionMode(tPvHandle Camera, char *tag, int acquisitionMode)
{
	char *Name = "AcquisitionMode";
	char *Value;
	int rcode = 0;
	tPvErr PvErr;

	if (acquisitionMode == ACQUISITION_MODE_CONTINUOUS) {
		Value = "Continuous";
	} else if (acquisitionMode == ACQUISITION_MODE_SINGLE_FRAME) {
		Value = "SingleFrame";
	} else {
		(void)fprintf(stderr, "%s: %s: Bad value (%d)\n", tag, Name, acquisitionMode);
		rcode++;
		return(rcode);
	}

	PvErr = PvAttrEnumSet(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetBinning(tPvHandle Camera, char *tag, int binning)
{
	char *Name;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;
	AVT_INFO_SENSOR InfoSensor;
	AVT_IMAGE_FORMAT_ROI ROI;

	Name = "BinningX";
	Value = (tPvUint32)binning;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	Name = "BinningY";
	Value = (tPvUint32)binning;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	rcode = gigeGetInfoSensor(&InfoSensor, tag, Camera);
	if (rcode != 0) {
		return(1);
	}
	gigeFmtInfoSensor(stdout, tag, InfoSensor);

	rcode = gigeGetImageFormatROI(&ROI, tag, Camera);
	if (rcode != 0) {
		return(1);
	}
	gigeFmtImageFormatROI(stdout, tag, ROI);

	int w = InfoSensor.SensorWidth/binning;
	int h = InfoSensor.SensorHeight/binning;
	// ensure even-ness
	w &= ~(int)1;
	h &= ~(int)1;
	(void)fprintf(stdout, "%s: new roi: %dx%d pixels\n", tag, w, h);
	rcode = gigeSetROI(Camera, tag, w, h, 0, 0);

	return(rcode);
}

int
gigeSetExposureValue(tPvHandle Camera, char *tag, int exposureValue)
{
	char *Name = "ExposureValue";
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	Value = (tPvUint32)(exposureValue * 1000);
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetFrameRate(tPvHandle Camera, char *tag, double frameRate)
{
	char *Name = "FrameRate";
	int rcode = 0;
	tPvErr PvErr;
	tPvFloat32 Value;

	Value = (tPvFloat32)frameRate;
	PvErr = PvAttrFloat32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetFrameStartTriggerMode(tPvHandle Camera, char *tag, int frameStartTriggerMode)
{
	char *Name = "FrameStartTriggerMode";
	char *Value;
	int rcode = 0;
	tPvErr PvErr;

	if (frameStartTriggerMode == FRAME_START_TRIGGER_MODE_FIXED_RATE) {
		Value = "FixedRate";
	} else if (frameStartTriggerMode == FRAME_START_TRIGGER_MODE_FREE_RUN) {
		Value = "Freerun";
	} else if (frameStartTriggerMode == FRAME_START_TRIGGER_MODE_SOFTWARE) {
		Value = "Software";
	} else {
		(void)fprintf(stderr, "%s: %s: Bad value (%d)\n", tag, Name, frameStartTriggerMode);
		rcode++;
		return(rcode);
	}

	PvErr = PvAttrEnumSet(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetGainValue(tPvHandle Camera, char *tag, int gainValue)
{
	char *Name = "GainValue";
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	Value = (tPvUint32)gainValue;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetROI(tPvHandle Camera, char *tag, int width, int height, int x, int y)
{
	char *Name;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	Name = "Width";
	Value = (tPvUint32)width;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	Name = "Height";
	Value = (tPvUint32)height;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	Name = "RegionX";
	Value = (tPvUint32)x;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	Name = "RegionY";
	Value = (tPvUint32)y;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}

int
gigeSetTimeStampValue(tPvHandle Camera, char *tag, int valueHi, int valueLo)
{
	char *Name;
	int rcode = 0;
	tPvErr PvErr;
	tPvUint32 Value;

	Name = "TimeStampValueHi";
	Value = (tPvUint32)valueHi;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	Name = "TimeStampValueLo";
	Value = (tPvUint32)valueLo;
	PvErr = PvAttrUint32Set(Camera, Name, Value);
	if (PvErr != ePvErrSuccess) {
		(void)fprintf(stderr, "%s: %s: %s\n", tag, Name, gigePvErrName(PvErr));
		rcode++;
	}

	return(rcode);
}
