'''SOJ51'''
import numpy as np
import scipy.interpolate as spi
import manga_devices
import glob
from datetime import datetime
import os
import sys
import ConfigParser
import math
import pyfits
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import time
#from pyraf import iraf
from yanny import yanny
import Imager
#import ADEUtils as ADE
#import SDSS_fratio as SFR
from matplotlib.backends.backend_pdf import PdfPages as PDF
glob = glob.glob

md = manga_devices.MangaDevices()

def motor_test(do_metro=False,do_test=True):
    
    if do_metro is not False:
        md.proc_pp_motor_cal()
        print >> sys.stderr, 'Put in Spot and move stage' 
        _ = raw_input()
        md.proc_spot_cal()
        print >> sys.stderr, 'Take out Spot'
        _ = raw_input()
        for i in range(do_metro):
            md.proc_pp_metrology()
    
    if not do_test:
        return
    
    file_list = glob(os.getcwd()+"/manga_pp_metrology*.map")
    
    big_list = []
    
    f = open('PP_motor_test_results.txt','w')
    f.write('# Results generated on {}\n#\n'.format(datetime.now().isoformat(' ')))
    f.write('# {:<13}\n'.format('Used files:'))
    
    for metro_file in file_list:

        f.write('# {:>}\n'.format(metro_file))
        
        mx2, my2 = np.loadtxt(metro_file,usecols=(6,7),skiprows=1,unpack=True)
        
        big_list.append(np.array([mx2, my2]).T)
        
    results = np.dstack(big_list)
    
    means = np.mean(results,axis=2)
    stds = np.std(results,axis=2)
    

    f.write('#\n# {:6}{:^20}{:^20}\n'.format('','mean','std'))
    f.write('# {:6}{:^10}{:^10}{:^10}{:^10}\n#\n'.format('fiber','x','y','x','y'))
    for i in range(means.shape[0]):
        f.write('  {:6}{: 10.4f}{: 10.4f}{: 10.4f}{: 10.4f}\n'.format(i+1,means[i,0],means[i,1],stds[i,0],stds[i,1]))
    f.write('\n------\n')
    f.write('# {:6}{: 30.4f}{: 10.4f}\n'.format('mean',*np.mean(stds,axis=0)))
    f.write('# {:6}{: 30.4f}{: 10.4f}\n'.format('std',*np.std(stds,axis=0)))
         
    f.close()
     
    return big_list, results

def IFU_test(do_metro=False,do_test=True,offset=0.1,gross=False):
    
    config = ConfigParser.RawConfigParser()
    config.read("manga.cfg")
    section = "IFU_MOTOR_CAL"
    k = config.getfloat(section, "k")
    alpha = config.getfloat(section, "alpha")
    Jx = config.getfloat(section, "Jx")
    Jy = config.getfloat(section, "Jy")
    alpha_r = np.radians(alpha)
    
    CX = 26.75
    CY = 12.50
    
    AX = CX - offset/2.0
    AY = CY + offset/2.0
    
    BX = CX + offset/2.0
    BY = CY - offset/2.0
    
    if do_metro is not False:
        #md.proc_ifu_motor_cal()
        for i in range(do_metro):
            md.proc_apt_move_absolute('x1',AX)
            md.proc_apt_move_absolute('y1',AY)
            md.proc_apt_move_block('x1')
            md.proc_apt_move_block('y1')
            md.proc_ifu_metrology(output='A_{}.ifu.txt'.format(i))
            
            if gross:
                md.proc_apt_move_absolute('x1',CX + gross)
                md.proc_apt_move_absolute('y1',CY + gross)
                md.proc_apt_move_block('x1')
                md.proc_apt_move_block('y1')
            
            md.proc_apt_move_absolute('x1',BX)
            md.proc_apt_move_absolute('y1',BY)
            md.proc_apt_move_block('x1')
            md.proc_apt_move_block('y1')
            md.proc_ifu_metrology(output='B_{}.ifu.txt'.format(i))

            
    if not do_test:
        return
    
    A_file_list = glob(os.getcwd()+"/A*.ifu.txt")
    B_file_list = glob(os.getcwd()+"/B*.ifu.txt")
    
    big_list = []
    
    if gross:
        f = open('IFU_motor_test_results_{}_{}.txt'.format(int(offset*1000),int(gross)),'w')
    else:
        f = open('IFU_motor_test_results_{}.txt'.format(int(offset*1000)),'w')
    f.write('# Results generated on {}\n#\n'.format(datetime.now().isoformat(' ')))
    f.write('# {:<13}\n'.format('Used files:'))
    
    for A_metro_file, B_metro_file in zip(A_file_list,B_file_list):

        f.write('# A: {:>}\n# B: {:>}\n#\n'.format(A_metro_file,B_metro_file))
        #e.write('# A: {:>}\n# B: {:>}\n#\n'.format(A_metro_file,B_metro_file))

        Aidx, Asx, Asy = np.loadtxt(A_metro_file,usecols=(3,8,9),skiprows=1,unpack=True)
        Bidx, Bsx, Bsy = np.loadtxt(B_metro_file,usecols=(3,8,9),skiprows=1,unpack=True)
        
        Asidx = np.argsort(Aidx)
        Bsidx = np.argsort(Bidx)
        
        Assx = Asx[Asidx]
        Assy = Asy[Asidx]
        Bssx = Bsx[Bsidx]
        Bssy = Bsy[Bsidx]
        
        dsx = Assx - Bssx
        dsy = Assy - Bssy
        
        Amx = k * (np.cos(alpha_r) * Assx - np.sin(alpha_r) * Assy)
        Amy = k * (np.sin(alpha_r) * Assx + np.cos(alpha_r) * Assy)
        Bmx = k * (np.cos(alpha_r) * Bssx - np.sin(alpha_r) * Bssy)
        Bmy = k * (np.sin(alpha_r) * Bssx + np.cos(alpha_r) * Bssy)

        
        dmx = Amx - Bmx
        dmy = Amy - Bmy
        
        
        print "len: {}, m: {}, s: {}".format(Asidx.size, np.mean(Aidx[Asidx] - Bidx[Bsidx]), np.std(Aidx[Asidx] - Bidx[Bsidx]))
        
        big_list.append(np.array([dmx, dmy]).T)
        
    results = np.dstack(big_list)
    
    means = np.mean(results,axis=0).T
    stds = np.std(results,axis=0).T

    f.write('#\n# {:6}{:^30}{:^30}\n'.format('','mean','std'))
    f.write('# {:^6}{:>15}{:>15}{:>15}{:>15}\n#\n'.format('meas','dx','dy','dx','dy'))
    for i in range(means.shape[0]):
        f.write('  {:6}{: 15.9f}{: 15.9f}{: 15.9f}{: 15.9f}\n'.format(i+1,means[i,0],means[i,1],stds[i,0],stds[i,1]))
    f.write('\n------\n')
    f.write('# {:6}{: 15.9f}{: 15.9f}{: 15.9f}{: 15.9f}\n'.format('mean',np.mean(means,axis=0)[0],
                                                                  np.mean(means,axis=0)[1],
                                                                  np.mean(stds,axis=0)[0],
                                                                  np.mean(stds,axis=0)[1]))
    f.write('# {:6}{: 15.9f}{: 15.9f}{: 15.9f}{: 15.9f}\n'.format('std',np.std(means,axis=0)[0],
                                                                  np.std(means,axis=0)[1],
                                                                  np.std(stds,axis=0)[0],
                                                                  np.std(stds,axis=0)[1]))
         
    f.close()
     
    return big_list, results

def centering_test(num):
    
    config = ConfigParser.RawConfigParser()
    config.read("manga.cfg")
    section = "PP_MOTOR_CAL"
    k = config.getfloat(section, "k")
    alpha = config.getfloat(section, "alpha")
    Jx = config.getfloat(section, "Jx")
    Jy = config.getfloat(section, "Jy")
    alpha_r = np.radians(alpha)
    
    f = open('cent_test_results.txt','w')
    f.write('# Results generated on {}\n#\n'.format(datetime.now().isoformat(' ')))
    f.write('# {:6}{:^10}{:^10}{:^15}{:^15}{:^15}{:^15}\n#\n'.format('meas','x','y','mx','my','cum_std','cum_std'))
    
    x = np.array([])
    y = np.array([])
    mx = np.array([])
    my = np.array([])
    
    for i in range(num):
        
        imagefile = md.proc_gige_loop_save_image()
        sx, sy = md.proc_manga_spot(imagefile)
        mmx = k * (np.cos(alpha_r) * sx - np.sin(alpha_r) * sy)
        mmy = k * (np.sin(alpha_r) * sx + np.cos(alpha_r) * sy)
        x = np.append(x,sx)
        y = np.append(y,sy)
        mx = np.append(mx, mmx)
        my = np.append(my, mmy)
        f.write('{:8}{: 10.4f}{: 10.4f}{: 15.9f}{: 15.9f}{: 15.9f}{: 15.9f}\n'.\
                format(i+1,sx,sy,mmx,mmy,np.std(mx),np.std(my)))
        
        
    f.write('\n--------\n')
    f.write('# {:6}{: 10.4f}{: 10.4f}{: 15.9f}{: 15.9f}\n'.format('mean',np.mean(x),np.mean(y),
                                                                  np.mean(mx),np.mean(my)))
    f.write('# {:6}{: 10.4f}{: 10.4f}{: 15.9f}{: 15.9f}\n'.format('std',np.std(x),np.std(y),
                                                                  np.std(mx),np.std(my)))
    
    f.close()
    
    
   
def plot_results():
    
    offset = np.array([50,100,120,200,500,300,20,10,7])
    sig_dx = np.array([40.5,27.1,166,489,240,30.1,16.4,22.1,54.6])/1000.0
    sig_dy = np.array([10.9,33.7,20.7,99.2,78.7,22.2,36.9,8.32,9.69])/1000.0
    mean_sigx = np.array([324,477,491,667,1660,1050,346,378,346])/1000.0
    mean_sigy = np.array([211,361,443,673,1630,989,114,178,177])/1000.0
    
    offset = np.sqrt(offset**2 + offset**2)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(offset, sig_dx, 'bx',label='$\sigma_{\overline{\Delta x}}$')
    ax.plot(offset, sig_dy, 'bo',label='$\sigma_{\overline{\Delta y}}$')
    ax.plot(offset, mean_sigx, 'gx',label='$\overline{\sigma_{\Delta x}}$')
    ax.plot(offset, mean_sigy, 'go',label='$\overline{\sigma_{\Delta y}}$')
    
    #ax.set_title(datetime.now().isoformat(' '))
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(7,1000)
    ax.set_xlabel('$\Delta r$ [$\mu\mathrm{m}$]')
    ax.set_ylabel('[$\mu\mathrm{m}$]')
    ax.legend(loc=0,scatterpoints=1,numpoints=1)
    
    fig.show()
    
def plot_gross():
    
    gross = np.array([4,2,1,0.5,5,3])
    sig_dx = np.array([461,181,66.0,57.2,1610,533])/1000.0
    sig_dy = np.array([765,192,262,54.0,945,400])/1000.0
    mean_sigx = np.array([422,454,344,384,413,443])/1000.0
    mean_sigy = np.array([352,361,373,343,339,338])/1000.0
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(gross, sig_dx, 'bx',label='$\sigma_{\overline{\Delta x}}$')
    ax.plot(gross, sig_dy, 'bo',label='$\sigma_{\overline{\Delta y}}$')
    ax.plot(gross, mean_sigx, 'gx',label='$\overline{\sigma_{\Delta x}}$')
    ax.plot(gross, mean_sigy, 'go',label='$\overline{\sigma_{\Delta y}}$')
    
    ax.set_title('$\Delta\mathrm{r}=0.100\sqrt{2}\mu\mathrm{m}$')#\n'+datetime.now().isoformat(' '))
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(0.3,6)
    ax.set_xlabel('Gross offset [mm]')
    ax.set_ylabel('[$\mu\mathrm{m}$]')
    ax.legend(loc=0,scatterpoints=1,numpoints=1)
    
    fig.show()
    
def z_test():
    
    currents = np.array([])
    for i in range(10):
        currents = np.append(currents,md.proc_rbd_query())
        
    mean = round(np.mean(currents),3)
    std = round(np.std(currents),3)
    
    return mean, std

def take_z():
    
    fig = plt.figure(1)
    fig.clf()
    #ax = fig.add_subplot(111)
    #ax.plot([],[])
    i = 0
    
    plt.ion()
    plt.show()
    while True:
        i += 1
        print i
        reading = md.proc_rbd_query()
        #ax.lines[0].set_xdata(np.append(ax.lines[0].get_xdata(), i))
        #ax.lines[0].set_ydata(np.append(ax.lines[0].get_ydata(),i**2))
        #print ax.lines[0].get_xdata()
        plt.scatter(i,reading)
        plt.draw()
        time.sleep(0.05)
    return

def make_2d(output):
    
    x = np.arange(12.5,17.5,0.25)
    y = np.arange(0,5,0.25)
    z = np.zeros((x.size,y.size))
    
    for i in range(x.size):
        md.proc_apt_move_absolute('x2', x[i])
        md.proc_apt_move_block('x2')
        for j in range(y.size):
            md.proc_apt_move_absolute('y2', y[j])
            md.proc_apt_move_block('y2')
            reading = md.proc_rbd_query()
            z[j,i] = reading
        
    
    HDU = pyfits.PrimaryHDU(z)
    HDU.header.update({'CRVAL1':12.5,'CRPIX1':0,'CDELT1':0.25,'CTYPE1':'linear',
                       'CRVAL2':0,'CRPIX2':0,'CDELT2':0.25,'CTYPE2':'linear',
                       'ZPOS':output.split('.fits')[0].split('_')[0]})
    HDU.writeto(output,clobber=True)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.imshow(z,extent=[12.5,17.5,5,0],cmap=cm.gray)
    ax.set_title(output)
    fig.show()
    return z
    
def make_1d(output):
    
    x = np.arange(11.8,16.8,0.1)
    #x = np.arange(0,5,0.1)
    z = np.zeros((x.size,))
    
    md.proc_apt_move_absolute('y2', 2.93)
    md.proc_apt_move_block('y2')
#      
#     md.proc_apt_move_absolute('x2',14.923)
#     md.proc_apt_move_block('x2')

    for i in range(x.size):
        md.proc_apt_move_absolute('x2', x[i])
        md.proc_apt_move_block('x2')
        reading = md.proc_rbd_query()
        z[i] = reading
        
    
    HDU = pyfits.PrimaryHDU(z)
    HDU.header.update({'CRVAL1':11.8,'CRPIX1':0,'CDELT1':0.1,'CTYPE1':'linear',
                       'ZPOS':output.split('.fits')[0].split('_')[0]})
    HDU.writeto(output)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(x,z)
    ax.set_title(output)
    fig.show()
    return z
    
def anal2d(searchstr,threshold=0.5):
    
    delta = 0.01
    
    filelist = glob(searchstr)
    
    zpos = np.array([])
    brightness = np.array([])
    width = np.array([])
    xwidtharr = np.array([])
    ywidtharr = np.array([])
    
    fig0 = plt.figure()
    ax0 = fig0.add_subplot(111)
    
    for filename in filelist:
        
        print filename
        
        try:
            z = float(filename.split('.fits')[0].split('_')[0])
        except ValueError:
            continue
        
        hdu = pyfits.open(filename)[0]
        data = hdu.data
        
        centxpx, centypx = centroid(data)
        print centxpx, centypx
        
        x = np.arange(data.shape[0])
        y = np.arange(data.shape[1])
        f = spi.interp2d(x, y, data)
        interpx = np.arange(0,data.shape[0],delta)
        interpy = np.arange(0,data.shape[1],delta)
        
        interpdata = f(interpx,interpy)
        interpcentxpx, interpcentypx = centroid(interpdata)
        
        cent_brightness = f(centxpx,centypx)[0]
        
        interp_bright = interpdata[interpcentxpx,interpcentypx]
        
        xvec = interpdata[interpcentxpx,:]
        yvec = interpdata[:,interpcentypx]
        xidx = np.where(xvec > cent_brightness*threshold)[0]
        yidx = np.where(yvec > cent_brightness*threshold)[0]
    
#         xcum = np.cumsum(xvec)
#         ycum = np.cumsum(yvec)
#         
#         xcum /= xcum.max()
#         ycum /= ycum.max()
#         
#         x1 = np.interp(0.25,xcum,interpx)
#         x2 = np.interp(0.75,xcum,interpx)
#         
#         y1 = np.interp(0.25,ycum,interpy)
#         y2 = np.interp(0.75,ycum,interpy)
#         fig0 = plt.figure()
#         ax0 = fig0.add_subplot(111)
#         ax0.plot(np.arange(xvec.size),xvec)
#         ax0.axvline(x=xidx[-1])
#         ax0.axvline(x=xidx[0])
#         fig0.show()
#         raw_input('')
#         xwidth = x2 - x1
#         ywidth = y2 - y1
        
        ax0.plot(interpx,xvec,label=z)
        
        xwidth = xidx[-1] - xidx[0]
        ywidth = yidx[-1] - yidx[0]
        
        xwidthmm = xwidth * hdu.header['CDELT1'] * delta
        ywidthmm = ywidth * hdu.header['CDELT2'] * delta
         
        widthpx = 0.5 * (xwidth + ywidth)
        widthmm = 0.5 * (xwidthmm + ywidthmm)
    
        zpos = np.append(zpos, z)
        brightness = np.append(brightness, cent_brightness)
        width = np.append(width, widthmm)
        xwidtharr = np.append(xwidtharr, xwidthmm)
        ywidtharr = np.append(ywidtharr, ywidthmm)
        
        #pyfits.PrimaryHDU(interpdata).writeto(filename.split('.fits')[0]+'_interp.fits',clobber=True)
        
        print filename, widthmm
        
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    
    ax1.plot(zpos,brightness,'.')
    ax2.plot(zpos,width,'.',label='avg')
    ax2.plot(zpos,xwidtharr,'.',label='x')
    ax2.plot(zpos,ywidtharr,'.',label='y')
    ax2.legend(loc=0)
    
    ax1.set_xlabel('Z stage position [mm]')
    ax1.set_ylabel('Central brightness [nA]')
    
    ax2.set_xlabel('Z stage position [mm]')
    ax2.set_ylabel('{:3.2f} width [mm]'.format(threshold))
    
    ax0.legend(loc=0)
    fig0.show()
    
    fig1.show()
    fig2.show()
    return

def anal1d(searchstr,threshold=0.5,title=None):
    
    filelist = glob(searchstr)
    
    delta = 0.01
    
    zpos = np.array([])
    brightness = np.array([])
    width = np.array([])
    kurt = np.array([])
    biglist = []
    
    fig0 = plt.figure()
    ax0 = fig0.add_subplot(111)
    
    for filename in filelist:
        
        print filename
        
        try:
            z = float(filename.split('.fits')[0].split('_')[0])
        except ValueError:
            z = 1
        
        hdu = pyfits.open(filename)[0]
        data = hdu.data
        x = np.arange(data.size)
        biglist.append(data)
        
        centpx = np.sum(data * x)/np.sum(data)
        cent_brightness = np.interp(centpx,x,data)

        interpx = np.arange(0,data.size,delta)
        interpdata = np.interp(interpx,x,data)
        
        """Compute some moments"""
        norm_data = interpdata/np.sum(interpdata)
        mu = np.sum(interpx * norm_data)
        print "Center is {}".format(mu* hdu.header['CDELT1'] + hdu.header['CRVAL1'])
        mu_2 = np.sum(((interpx - mu)**2) * norm_data)
        mu_4 = ((np.sum(((interpx - mu)**4) * norm_data)) / mu_2**2) - 3.0
        
        xidx = np.where(interpdata > cent_brightness*threshold)[0]
        x2 = xidx[-1]
        x1 = xidx[0]
        xwidth = x2 - x1
#          
#         cum = np.cumsum(data)
#         cum /= cum.max()
#         x1 = np.interp(threshold/2.0,cum,x)
#         x2 = np.interp(1.0 - threshold/2.0,cum,x)
#         xwidth = x2 - x1
#         
#         xidx = np.where(np.arange(data.size) > cent_brightness*threshold)[0]
#         
#         xwidth = xidx[-1] - xidx[0]
        
        widthmm = xwidth * hdu.header['CDELT1'] * delta
    
        zpos = np.append(zpos, z)
        brightness = np.append(brightness, cent_brightness)
        width = np.append(width, mu_2)
        kurt = np.append(kurt,mu_4)
        
        line = ax0.plot(np.arange(interpdata.size),interpdata,label=z)[0]
#         ax0.axvline(x=x1,color=line.get_color())
#         ax0.axvline(x=x2,color=line.get_color())
        
        #pyfits.PrimaryHDU(interpdata).writeto(filename.split('.fits')[0]+'_interp.fits',clobber=True)
        
        print filename, widthmm
        
        
    '''Fit a parabola to \mu_4'''
    mu4_func = np.poly1d(np.polyfit(zpos,kurt,3))
    func_z = np.linspace(zpos.min(),zpos.max(),100)
    func_kurt = mu4_func(func_z)
    focus = func_z[np.where(func_kurt == func_kurt.min())[0]][0]
    print np.where(func_kurt == func_kurt.min())[0]
    print focus
    print func_z[np.where(func_kurt == func_kurt.min())[0]]
    
    '''figure out variation in brightness'''
    uncertainty_range_idx = np.where((zpos >= focus - 0.468) & (zpos <= focus + 0.468))
    brightness_var = np.std(brightness[uncertainty_range_idx])
    brightness_percent = brightness_var/np.mean(brightness[uncertainty_range_idx])
    print uncertainty_range_idx
    print "Brightness variation: {:}\n which is {:5.2f}%".format(brightness_var,brightness_percent*100.)
    
    ax0.legend(loc=0)
    fig0.show()
    
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111,frameon=False,xticks=[],yticks=[])
    image = np.vstack(biglist)
    ax4.imshow(image[::-1],cmap=cm.gray)
    fig4.show()
    
    ax1.errorbar(zpos,brightness,ls='.',yerr=0.004)
    ax2.plot(zpos,width,'.')
    ax3.plot(zpos,kurt,'.')
    ax3.plot(func_z,func_kurt,':')
    
    ax1.set_xlabel('Z stage position [mm]')
    ax1.set_ylabel('Central brightness [nA]')
    
    ax2.set_xlabel('Z stage position [mm]')
    ax2.set_ylabel('{:3.2f} width [mm]'.format(threshold))
    
    ax3.set_xlabel('Z stage position [mm]')
    ax3.set_ylabel('$\mu_4$')
    
    if title:
        ax1.set_title(title)
        ax3.set_title(title+'\nFocus at {:5.3f}mm'.format(focus))
    else:
        ax1.set_title(datetime.now().isoformat(' '))
        ax3.set_title(datetime.now().isoformat(' ')+'\nFocus at {:5.3f}mm'.format(focus))
    fig1.show()
    #fig2.show()
    fig3.show()
    return
    
def centroid(image):
    '''takes in an array and returns a list containing the
    center of mass of that array
    
    Originally from ADEUtils.py
    '''

    '''just to make sure we don't mess up the original data somehow'''
    data = np.copy(image) * 1.0

    size = data.shape
    totalMass = np.sum(data)
    
    xcom = np.sum(np.sum(data,1) * 1.*np.arange(size[0]))/totalMass
    ycom = np.sum(np.sum(data,0) * 1.*np.arange(size[1]))/totalMass
    
    return (xcom,ycom)

def lamp_test(output):
    '''a simple warm-up test of the lamp output
    '''
    
    readings = np.array([])
    times = np.array([])
    
    while True:
        
        try:
            readings = np.append(readings,md.proc_rbd_query())
            times = np.append(times,time.time())
            time.sleep(1)
        except KeyboardInterrupt:
            break
    
    f = open(output,'w')
    f.write('# Generated on {}\n'.format(datetime.now().isoformat(' ')))
    f.write('#{:>20}{:>10}\n'.format('Time','Reading'))
    
    for i in range(times.size):
        f.write('{:20.10f}{:10.4f}\n'.format(times[i],readings[i]))
        
    f.close()
    return

def plot_lamp(datafile):
    '''Plots the results of lamp_test
    '''
    
    longtime, readings = np.loadtxt(datafile,unpack=True)
    
    time = (longtime - longtime.min())/60.
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(time,readings,'.')
    ax.set_xlabel('Time [min]')
    ax.set_ylabel('Picoammeter reading [nA]')
    
    fig.show()
    return ax

def plot_z(data_file,title=''):
    
    z, f, fe, s, se = np.loadtxt(data_file,unpack=True)
    
    tput = f/s 
    tpute = np.sqrt((fe/s)**2 + (se*f/s**2)**2)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.errorbar(z, f, ls='.', yerr=fe)
    ax.set_xlabel('Z-stage [mm]')
    ax.set_ylabel('Ammeter reading [nA]')
    ax.set_title('Fiber')
    
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.errorbar(z, s, ls='.', yerr=se)
    ax2.set_xlabel('Z-stage [mm]')
    ax2.set_ylabel('Ammeter reading [nA]')
    ax2.set_title('Source')
    
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(111)
    ax3.errorbar(z, tput, ls='.', yerr=tpute)
    ax3.set_xlabel('Z-stage [mm]')
    ax3.set_ylabel('Throughput')
    ax3.set_title(title)
    
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(111)
    ax4.errorbar(z, f, ls='.', yerr=fe, label='fiber')
    ax4.errorbar(z, s, ls='.', yerr=se, label='source')
    ax4.set_xlabel('Z-stage [mm]')
    ax4.set_ylabel('Ammeter reading [nA]')
    ax4.legend(loc=0,numpoints=1)
    ax4.set_title(title)
    
    fig.show()
    fig2.show()
    fig3.show()
    fig4.show()
    
    return z[np.where(tput == tput.max())]


def angle_data(zpos):
    '''Collects CCD images of fibers in a single slitblock. 
    Right now it is calibrated for production 37 fiber harness
    '''
    
    step = 0.177
    startfiber = 1
    
    md.set_ifu_map('production-fiber-map-rank-3.map')
    
    '''get direct measurements'''
    raw_input('Setup direct and hit enter')
    name = 'direct_{:05.2f}.fits'.format(zpos)
    rawimage = md.proc_gige_loop_save_image()
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))
    
    '''get the sky fibers'''
    raw_input('setup fibers')
    md.proc_apt_move_absolute('x1',14.5)
    md.proc_apt_move_absolute('y1',22.13)
    md.proc_apt_move_block('x1')
    md.proc_apt_move_block('y1')
    '''assumes the user starts at the lowest-numbered fiber in the block'''
    startpos = md.proc_apt_query('x2')
    name = 'fiber-1_{:05.2f}.fits'.format(zpos)
    rawimage = md.proc_gige_loop_save_image()
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))
    
    
    for num in range(37):
        
        print "measuring fiber {:}".format(num+startfiber)
        md.proc_ifu_fiber_acquire(3, num+startfiber)
        
        name = 'fiber{:}_{:05.2f}.fits'.format(num+startfiber,zpos)
        rawimage = md.proc_gige_loop_save_image()
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        
        xpos = float(md.proc_apt_query('x2'))
        md.proc_apt_move_absolute('x2',xpos + step)
        
        
    '''sky #2'''
    md.proc_apt_move_absolute('x1',21.49)
    md.proc_apt_move_absolute('y1',22.2)
    md.proc_apt_move_block('x1')
    md.proc_apt_move_block('y1')
    name = 'fiber-2_{:05.2f}.fits'.format(zpos)
    rawimage = md.proc_gige_loop_save_image()
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))
    
    '''now go back to the start in case we want to do it again (i.e. for another zpos)'''
    #md.proc_ifu_fiber_acquire(3, startfiber)
    md.proc_apt_move_absolute('x2',startpos)
    
    raw_input('Setup direct and hit enter')
    name = 'direct2_{:05.2f}.fits'.format(zpos)
    rawimage = md.proc_gige_loop_save_image()
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))
    
    return
    
def angle_test(searchstr,axtup=None,label=None,outfig='test.pdf',tol=1.):
    '''Given a list of fits files this function computes the center of the profile in each image
    and then plots x and y center as a function of z stage position'''
    
    file_list = glob(searchstr)
    output = searchstr.split('*')[0].split('_')[0]+'_centers.txt'
    
    pp = PDF(outfig)
    zlist = np.array([])
    xlist = np.array([])
    ylist = np.array([])
    flist = np.array([])
    
    t1 = time.time()
    for image in file_list:
        
        print image
        fig = plt.figure()
        z = float(image.split('_')[2][0:4])
        data = pyfits.open(image)[0].data.T
 
        x, y = centroid(data)
#        x, y = iso_cent(data,fig=fig,tol=tol)
        fig.suptitle('{}\n({},{})'.format(image,x,y))        
        pp.savefig(fig)
        zlist = np.append(zlist,z)
        xlist = np.append(xlist,x)
        ylist = np.append(ylist,y)
        flist = np.append(flist,np.sum(data))
    
    delta_t = time.time() - t1
    print "Finding centers took {} seconds".format(delta_t)
    pp.close()
    '''write out the centers'''
    f = open(output,'w')
    f.write('# File {} generated on {}\n#\n'.format(output,datetime.now().isoformat(' ')) +
            '# {:<10} - Reading on Z stage micrometer [mm]\n'.format('zpos') +
            '# {:<10} - X center of beam profile (actually Y) [px]\n'.format('x') +
            '# {:<10} - Y center of beam profile (actually X) [px]\n'.format('y') +
            '# {:<10} - Total counts in image [ADU]\n'.format('ADU') +
            '#{:>10}{:>10}{:>10}{:>10}\n'.format('zpos','x','y','ADU') +
            '#{:>10}{:>10}{:>10}{:>10}\n'.format(*range(4)) +
            '#'+'-'*40+'\n')
    for i in range(zlist.size):
        f.write('{:11.3f}{:10.4f}{:10.4f}{:10.2e}\n'.format(zlist[i],xlist[i],ylist[i],flist[i]))
    f.close()
            

    xlist *= 0.0044 #in mm
    ylist *= 0.0044

    '''fit a line'''
    xfit = ADE.polyclip(zlist,xlist,1)
    yfit = ADE.polyclip(zlist,ylist,1)
    # xfit = np.poly1d(np.polyfit(zlist,xlist,1))
    # yfit = np.poly1d(np.polyfit(zlist,ylist,1))
    
    ###Compute the error on the slopes
    see_x = (np.sum((xlist - xfit(zlist))**2)/(xlist.size - 2))**0.5
    slope_err_x = see_x * (1/(np.sum((zlist - np.mean(zlist))**2)))**0.5
    see_y = (np.sum((ylist - yfit(zlist))**2)/(ylist.size - 2))**0.5
    slope_err_y = see_y * (1/(np.sum((zlist - np.mean(zlist))**2)))**0.5

    if not axtup:
        x_ax = plt.figure().add_subplot(111)
        y_ax = plt.figure().add_subplot(111)
        f_ax = plt.figure().add_subplot(111)
    else:
        x_ax, y_ax = axtup
        f_ax = plt.figure().add_subplot(111)
        
#     xpoints = x_ax.plot(zlist,xlist - xfit.c[1],'.',label=label)[0]
#     ypoints = y_ax.plot(zlist,ylist - yfit.c[1],'.',label=label)[0]
#     x_ax.plot(zlist,xfit(zlist) - xfit.c[1],':',color=xpoints.get_color())
#     y_ax.plot(zlist,yfit(zlist) - yfit.c[1],':',color=ypoints.get_color())
    xpoints = x_ax.plot(zlist,xlist,'.',label=label)[0]
    ypoints = y_ax.plot(zlist,ylist,'.',label=label)[0]
    x_ax.plot(zlist,xfit(zlist),':',color=xpoints.get_color())
    y_ax.plot(zlist,yfit(zlist),':',color=ypoints.get_color())
    
    x_ax.set_ylabel('X center - b [mm]')
    x_ax.set_xlabel('Z position [mm]')
    #x_ax.set_xlim(10,15)
    if x_ax.get_title() == '':
        x_ax.set_title('{:}: x = {:5.5f} z + {:5.2f}'.format(label,*xfit.c))
    else:
        x_ax.set_title(x_ax.get_title()+'\n{:}: x = {:5.5f} z + {:5.2f}'.format(label,*xfit.c))
    x_ax.legend(loc=0,scatterpoints=1,numpoints=1)
    y_ax.set_ylabel('Y center - b [mm]')
    y_ax.set_xlabel('Z position [mm]')
    #y_ax.set_xlim(10,15)
    if y_ax.get_title() == '':
        y_ax.set_title('{:}: y = {:5.5f} z + {:5.2f}'.format(label,*yfit.c))
    else:
        y_ax.set_title(y_ax.get_title()+'\n{:}: y = {:5.5f} z + {:5.2f}'.format(label,*yfit.c))
    y_ax.legend(loc=0,scatterpoints=1,numpoints=1)
    
    f_ax.plot(zlist,flist)
#     x_ax.get_figure().show()
#     plt.draw()
#     y_ax.get_figure().show()
#     
#     plt.draw()
    
    return (x_ax,y_ax,f_ax,xfit,yfit)
#    return (x_ax,y_ax,xfit,yfit,slope_err_x,slope_err_y)

def A30_script(output):
    '''Analysis script for running angle_test on block A30 in MIFU003
    '''

    numlist = np.arange(30) + 38
    xax, yax, srcx, srcy = angle_test('source*.fits',label='Source')

    fitlist = []
    
    for fiber in numlist:
        
        name = 'fiber{:}'.format(fiber)
        print name
        xax, yax, xfit, yfit = angle_test(name+'*.fits',axtup=(xax,yax),label=name)
        fitlist.append((xfit,yfit))

    xax.get_figure().show()
    yax.get_figure().show()

    centeroutput = output.split('.txt')[0]+'_centers.txt'
    center_helper(centeroutput)

    f = open(output,'w')
    f.write('# File generated on {}\n'.format(datetime.now().isoformat(' ')))
    f.write('# {:<10} - Fiber number\n'.format('fibnum') +
            '# {:<10} - Slope of x = mx z + bx\n'.format('mx') +
            '# {:<10} - Intercept of x = mx z + bx [mm]\n'.format('bx') +
            '# {:<10} - Slope of y = my z + by\n'.format('my') +
            '# {:<10} - Intercept of y = my z + by [mm]\n'.format('by'))
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format('fibum','mx','bx','my','by'))
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format(*range(5)))
    f.write('#'+'-'*50+'\n')
    f.write('{0:>11}{1[0]:10.5f}{1[1]:10.2f}{2[0]:10.5f}{2[1]:10.2f}\n'.format(0,*[srcx.c,srcy.c]))
    for i, (x,y) in enumerate(fitlist):
            f.write('{0:11}{1[0]:10.5f}{1[1]:10.2f}{2[0]:10.5f}{2[1]:10.2f}\n'.format(i+38,*[x.c,y.c]))
    f.close()

    return (xax, yax)

def prod37_script(output):
    '''Analysis script for running angle_test on a production 37+2 fiber harness
    '''

    output_prefix = output.split('.txt')[0]
    numlist = np.arange(39) - 2
    numlist[2:] += 1
    numlist = np.array([0])


    '''first let's get information from the direct beam that we'll need to
    reduce the rest of the data'''
    direct_N, _ = SFR.kegstand('direct_*.fits',
                            '{}_N.pdf'.format(output_prefix),
                            '{}_EE.pdf'.format(output_prefix))

    scale_factor = np.arctan(1/(2*direct_N))/np.arctan(1/(2*4.97))

    direct_list = glob('direct_*.fits')
    dd = {}
    for direct_image in direct_list:
        
        z = float(direct_image.split('_')[1][0:4])
        data = pyfits.open(direct_image)[0].data
        r, f, _ = ADE.fast_annulize(data,300)
        flux = np.cumsum(f)
        
# <<<<<<< HEAD
#         rf5 = SFR.get_radius(data,None,0.5)
#         rf4 = rf5 * 4.97/4.0
#         rf3 = rf5 * 4.97/3
        
#         ff5, ff4, ff3 = np.interp([rf5,rf4,rf3],r,flux)
#         dd[z] = {'5': [rf5,ff5], '4': [rf4,ff4], '3': [rf3,ff3]}

#     xax, yax, fax, srcx, srcy = angle_test('direct_*.fits',label='Source')

#     fitlist = []
#     tputlist = []

#     for fiber in numlist:
        
#         '''angle stuff'''
#         name = 'fiber_'.format(fiber)
#         print name
#         xax, yax, fax, xfit, yfit = angle_test(name+'*.fits',axtup=(xax,yax),label=name)
#         fitlist.append((xfit,yfit))

#         '''throughput stuff'''
#         tput_stack = np.zeros(3)
#         fiber_list = glob('fiber_*.fits'.format(fiber))
#         for fiber_image in fiber_list:
# =======
        rf5 = SFR.get_radius(data,None,0.2)/0.0044
        rf4 = rf5 * direct_N/(4.0 / scale_factor)
        rf32 = rf5 * direct_N/(3.2 / scale_factor)
        
        df5, df4, df32 = np.interp([rf5,rf4,rf32],r,flux)
        dd[z] = {'5': [rf5,df5], '4': [rf4,df4], '3.2': [rf32,df32]}

    xax, yax, srcx, srcy = angle_test('direct_*.fits',label='Source')

    fitlist = np.zeros((len(numlist),),
                       dtype = [('xslope','f4'),
                                ('xinter','f4'),
                                ('yslope','f4'),
                                ('yinter','f4'),
                                ('xtheta','f8'),
                                ('ytheta','f8')])
    tputlist = []

    for i, fiber in enumerate(numlist):
        
        '''angle stuff'''
        name = 'fiber{:}'.format(fiber)
        print name
        print 'angles'
        xax, yax, xfit, yfit = angle_test(name+'_*.fits',axtup=(xax,yax),label=name)
        xtheta = np.arctan(xfit.c[0])*180/np.pi/scale_factor
        ytheta = np.arctan(yfit.c[0])*180/np.pi/scale_factor
        
        fitlist[i] = (xfit.c[0],xfit.c[1],yfit.c[0],yfit.c[1],xtheta,ytheta)

        '''throughput stuff'''
        tput_stack = np.zeros(3)
        fiber_list = glob('fiber{}_*.fits'.format(fiber))
        print '\tthroughput'
        for fiber_image in fiber_list:
            print '\t{}'.format(fiber_image)
#>>>>>>> 7de560c90df2c2def35eedf5163f22be3b40d2e3
            z = float(fiber_image.split('_')[1][0:4])
            data = pyfits.open(fiber_image)[0].data
            r, f, _ = ADE.fast_annulize(data,300)
            flux = np.cumsum(f)
            
            rf5, df5 = dd[z]['5']
            rf4, df4 = dd[z]['4']
# <<<<<<< HEAD
#             rf3, df3 = dd[z]['3']
            
#             ff5, ff4, ff3 = np.interp([rf5,rf4,rf3],r,flux)

#             tput = np.array([ff5,ff4,ff3])/np.array([df5,df4,df3])
#             print tput
#             tput_stack = np.vstack((tput_stack,tput))
#             print tput_stack

#         print tput_stack
# =======
            rf32, df32 = dd[z]['3.2']
            
            ff5, ff4, ff32 = np.interp([rf5,rf4,rf32],r,flux)

            tput = np.array([ff5,ff4,ff32])/np.array([df5,df4,df32])
            tput_stack = np.vstack((tput_stack,tput))

#>>>>>>> 7de560c90df2c2def35eedf5163f22be3b40d2e3
        final_tput = np.mean(tput_stack[1:],axis=0)
        tputlist.append(final_tput)

    xax.get_figure().show()
    yax.get_figure().show()

# <<<<<<< HEAD
#     # centeroutput = output.split('.txt')[0]+'_centers.txt'
#     # center_helper(centeroutput)

#     print fitlist
#     print tputlist

#     f = open(output,'w')
#     f.write('# File generated on {}\n'.format(datetime.now().isoformat(' ')))
#     f.write('# {:<10} - Fiber number\n'.format('fibnum') +
#             '# {:<10} - Slope of x = mx z + bx\n'.format('mx') +
#             '# {:<10} - Theta_x = arctan(mx) [deg]\n'.format('theta_x') +
#             '# {:<10} - Intercept of x = mx z + bx [mm]\n'.format('bx') +
#             '# {:<10} - Slope of y = my z + by\n'.format('my') +
#             '# {:<10} - Theta_y = arctan(my) [deg]\n'.format('theta_y') +
#             '# {:<10} - Intercept of y = my z + by [mm]\n'.format('by') +
#             '# {:<10} - Throughput within f/5\n'.format('tput5') +
#             '# {:<10} - Throughput within f/4\n'.format('tput4') +
#             '# {:<10} - Throughput within f/3\n'.format('tput3'))
# =======
    centeroutput = output.split('.txt')[0]+'_centers.txt'
    center_helper(centeroutput)

    mean_xtheta = np.mean(fitlist['xtheta'])
    mean_ytheta = np.mean(fitlist['ytheta'])

    f = open(output,'w')
    f.write('# File generated on {}\n'.format(datetime.now().isoformat(' ')))
    f.write('#\n# Lens scale factor = {:7.4f}\n'.format(scale_factor) +
            '# Average theta_x = {:8.5f} degrees\n'.format(mean_xtheta) +
            '# Average theta_y = {:8.5f} degress\n#\n'.format(mean_ytheta) +
            '# {:<10} - Fiber number\n'.format('fibnum') +
            '# {:<10} - Slope of x = mx z + bx\n'.format('mx') +
            '# {:<10} - Theta_x = arctan(mx) - average_theta_x [deg]\n'.format('theta_x') +
            '# {:<10} - Intercept of x = mx z + bx [mm]\n'.format('bx') +
            '# {:<10} - Slope of y = my z + by\n'.format('my') +
            '# {:<10} - Theta_y = arctan(my) - average_theta_y [deg]\n'.format('theta_y') +
            '# {:<10} - Intercept of y = my z + by [mm]\n'.format('by') +
            '# {:<10} - Throughput within f/5\n'.format('tput5') +
            '# {:<10} - Throughput within f/4\n'.format('tput4') +
            '# {:<10} - Throughput within f/3.2\n'.format('tput32') +
            '#\n')
#>>>>>>> 7de560c90df2c2def35eedf5163f22be3b40d2e3
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}\n'.\
                format('fibum','mx','theta_x','bx','my','theta_y','by','tput5','tput4','tput32'))
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format(*range(10)))
    f.write('#'+'-'*100+'\n')
    f.write('{0:>11}{1[0]:10.5f}{4:10.5f}{1[1]:10.2f}{2[0]:10.5f}{5:10.5f}{2[1]:10.2f}{3:10.2f}{3:10.2f}{3:10.2f}\n'.\
                format(0,srcx.c,
                       srcy.c,
                       100,
# <<<<<<< HEAD
#                        *np.arctan([srcx.c[0],srcy.c[0]])*180/np.pi))
#     for (x,y), (tf5, tf4, tf3) in zip(fitlist,tputlist):
#             f.write('{0:11}{1[0]:10.5f}{3:10.5f}{1[1]:10.2f}{2[0]:10.5f}{4:10.5f}{2[1]:10.2f}{5:10.2f}{6:10.2f}{7:10.2f}\n'.\
#                         format(0,
#                                x.c,
#                                y.c,
#                                np.arctan(x.c[0])*180/np.pi,
#                                np.arctan(y.c[0])*180/np.pi,
#                                tf5,tf4,tf3))
# =======
                       *np.arctan([srcx.c[0],srcy.c[0]])*180/np.pi/scale_factor))
    for i, fit, (tf5, tf4, tf32) in zip(numlist,fitlist,tputlist):
            f.write('{0:11}{1:10.5f}{2:10.5f}{3:10.2f}{4:10.5f}{5:10.5f}{6:10.2f}{7:10.2f}{8:10.2f}{9:10.2f}\n'.\
                        format(i,
                               fit['xslope'],
                               fit['xtheta'] - mean_xtheta,
                               fit['xinter'],
                               fit['yslope'],
                               fit['ytheta'] - mean_ytheta,
                               fit['yinter'],
                               tf5,tf4,tf32))
#>>>>>>> 7de560c90df2c2def35eedf5163f22be3b40d2e3
    f.close()

    return (xax, yax)

def center_helper(output):
    '''Takes the result of angle_test for a bunch of fibers and creates a file
    with the computed center of each fiber at each zpos
    '''
    
    numlist = np.arange(19) + 1
    
    f = open(output,'w')
    f.write('# File {} generated on {}\n#\n'.format(output,datetime.now().isoformat(' ')) +
            '# {:<10} - Fiber number\n'.format('fnum') +
            '# {:<10} - X center of beam profile (actually Y) [px]\n'.format('x') +
            '# {:<10} - Y center of beam profile (actually X) [px]\n'.format('y') +
            '# {:<10} - Reading of Z micrometer [mm]\n'.format('zpos') +
            '#{:>10}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}\n'.format('zpos = ',0,2,3,4,6,8) +
            '#{0:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}\n'.\
                format('fnum','x','y') +
            '#{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}\n'.format(*range(13)) +
            '#'+'-'*130+'\n')

    for num in numlist:
        z, x, y = np.loadtxt('fiber{}_centers.txt'.format(num),unpack=True)
        f.write('{:11n}'.format(num))
        for i in range(z.size):
            f.write('{:10.4f}{:10.4f}'.format(x[i],y[i]))
        f.write('\n')

    f.close()
    return
    

def A30_anal(totalnum,output):
    '''Takes the result of many A30_script runs and combines the respective output files,
    averaging all the quantities.
    '''
    
    centoutput = output.split('.txt')[0] + '_centers.txt'
    numlist = range(totalnum)
    
    mxlist = []
    bxlist = []
    mylist = []
    bylist = []
    centlist = []
    
    for num in numlist:
        
        name = 'A30_{0}/A30_{0}.txt'.format(num+1)
        centname = name.split('.txt')[0] + '_centers.txt'
        print name
        fiber, mx, bx, my, by = np.loadtxt(name,unpack=True)
        centdata = np.loadtxt(centname)
        mxlist.append(mx)
        bxlist.append(bx)
        mylist.append(my)
        bylist.append(by)
        centlist.append(centdata)
        
    bigmx = np.vstack(mxlist)
    bigbx = np.vstack(bxlist)
    bigmy = np.vstack(mylist)
    bigby = np.vstack(bylist)
    bigcent = np.dstack(centlist)
    bigcent = bigcent[:,1:,:]
    
    meanmx = np.mean(bigmx,axis=0)
    meanbx = np.mean(bigbx,axis=0)
    meanmy = np.mean(bigmy,axis=0)
    meanby = np.mean(bigby,axis=0)
    
    stdmx = np.std(bigmx,axis=0)
    stdbx = np.std(bigbx,axis=0)
    stdmy = np.std(bigmy,axis=0)
    stdby = np.std(bigby,axis=0)
    stdcent = np.std(bigcent,axis=2)
    
    f = open(output,'w')
    f.write('# File generated on {}\n#\n'.format(datetime.now().isoformat(' ')) +
            '# Data shown is a combination of {} data sets. Each data set consists of\n'.format(totalnum) +
            '# all 30 fibers measured at 6 different z-stage positions.\n#\n' +
            '# {:<10} - Fiber number\n'.format('fibnum') +
            '# {:<10} - Slope of x = mx*z + bx\n'.format('mx') +
            '# {:<10} - Intercept of x = mx*z + bx [mm]\n'.format('bx') +
            '# {:<10} - Slope of y = my*z + by\n'.format('my') +
            '# {:<10} - Intercept of y = my*z + by [mm]\n#\n'.format('by') +
            '#{:>6}{:^20}{:^20}{:^20}{:^20}\n'.format('fibum','     mx','     bx','     my','     by') +
            '#{:6}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format('','mean','std',
                                                                              'mean','std',
                                                                              'mean','std',
                                                                              'mean','std') +
            '#{:>6}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format(*range(9))+
            '#'+'-'*86+'\n')
    for i in range(meanmx.size):
        f.write('{:7.0f}{:10.5f}{:10.5f}{:10.4f}{:10.4f}{:10.5f}{:10.5f}{:10.4f}{:10.4f}\n'.format(fiber[i],
                                                                                                 meanmx[i],
                                                                                                 stdmx[i],
                                                                                                 meanbx[i],
                                                                                                 stdbx[i],
                                                                                                 meanmy[i],
                                                                                                 stdmy[i],
                                                                                                 meanby[i],
                                                                                                 stdby[i]))
    f.close()
    
    f2 = open(centoutput,'w')
    f2.write('# File {} generated on {}\n#\n'.format(output,datetime.now().isoformat(' ')) +
             '# {:<10} - Fiber number\n'.format('fnum') +
             '# {:<10} - stddev of X center of beam profile (actually Y) [px]\n'.format('dx') +
             '# {:<10} - stddev of Y center of beam profile (actually X) [px]\n'.format('dy') +
             '# {:<10} - Reading of Z micrometer [mm]\n'.format('zpos') +
             '#{:>10}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}|{:^19.2f}\n'.format('zpos = ',0,2,3,4,6,8) +
             '#{0:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}|{1:>9}{2:>10}\n'.\
                   format('fnum','dx','dy') +
             '#{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}|{:>9}{:>10}\n'.format(*range(13)) +
             '#'+'-'*130+'\n')
    for i in range(fiber.size - 1):
        f2.write('{:11n}'.format(i+38))
        f2.write(str('{:10.4f}'*12).format(*stdcent[i,:]))
        f2.write('\n')
    f2.write(str('\n{:>11}'+'{:10.4f}'*12).format('mean',*np.mean(stdcent,axis=0)))
    f2.close()

    return bigcent
        
def A19_script(output):
    '''Analysis script for running angle_test on block A19 in MIFU007
    '''

    numlist = np.arange(19) + 1
    xax, yax, srcx, srcy = angle_test('source*.fits',label='Source')

    fitlist = []
    
    for fiber in numlist:
        
        name = 'fiber{:}_'.format(fiber)
        print name
        xax, yax, xfit, yfit = angle_test(name+'*.fits',axtup=(xax,yax),label=name.split('_')[0])
        fitlist.append((xfit,yfit))

    xax.get_figure().show()
    yax.get_figure().show()

    centeroutput = output.split('.txt')[0]+'_centers.txt'
    center_helper(centeroutput)

    f = open(output,'w')
    f.write('# File generated on {}\n'.format(datetime.now().isoformat(' ')))
    f.write('# {:<10} - Fiber number\n'.format('fibnum') +
            '# {:<10} - Slope of x = mx z + bx\n'.format('mx') +
            '# {:<10} - Intercept of x = mx z + bx [mm]\n'.format('bx') +
            '# {:<10} - Slope of y = my z + by\n'.format('my') +
            '# {:<10} - Intercept of y = my z + by [mm]\n'.format('by'))
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format('fibum','mx','bx','my','by'))
    f.write('#{:>10}{:>10}{:>10}{:>10}{:>10}\n'.format(*range(5)))
    f.write('#'+'-'*50+'\n')
    f.write('{0:>11}{1[0]:10.5f}{1[1]:10.2f}{2[0]:10.5f}{2[1]:10.2f}\n'.format(0,*[srcx.c,srcy.c]))
    for i, (x,y) in enumerate(fitlist):
            f.write('{0:11}{1[0]:10.5f}{1[1]:10.2f}{2[0]:10.5f}{2[1]:10.2f}\n'.format(i+1,*[x.c,y.c]))
    f.close()

    return (xax, yax)


def ds9_test(region_file, parfile):
    
    f = open(region_file,'r')
    coords = [float(i) for i in f.readlines()[-1].split('(')[1].split(')')[0].split(',')]
    
    xc = np.array([coords[::2]])
    yc = np.array([coords[1::2]])
    xdiff = xc - xc.T
    ydiff = yc - yc.T
    
    idx = ([0,1,3,4],[1,0,4,3])
    
    theta = np.arctan(ydiff[idx]/xdiff[idx]).mean()*180/np.pi
    
    center = np.array([xc.mean(), yc.mean()])
    
    print 'hex center: {}'.format(center)
    
    cfg = ConfigParser.ConfigParser()
    cfg.read('manga.cfg')
    map_file = cfg.get('METROLOGY','ifu-metrology-file')
    pos, imx, imy = np.loadtxt(map_file,skiprows=1,usecols=(3,6,7),unpack=True)
    cidx = np.where(pos == 0)
    fcenter = np.array([imx[cidx][0], imy[cidx][0]])
    
    print "central fiber at: {}".format(fcenter)
    
    kx = cfg.getfloat('IFU_MOTOR_CAL','kx')
    ky = cfg.getfloat('IFU_MOTOR_CAL','ky')
    alpha = cfg.getfloat('IFU_MOTOR_CAL','alpha')
    
    xoff, yoff = fcenter - center
    mdx = kx * (np.cos(alpha)*xoff - np.sin(alpha)*yoff)
    mdy = ky * (np.sin(alpha)*xoff + np.cos(alpha)*yoff)
    print 'diff[px] is {}, {}'.format(xoff, yoff)
    print 'diff[mm] is {}, {}'.format(mdx, mdy)
    
    pf = open(parfile,'r')
    lines = pf.readlines()
    of = open('test.par','w')
    for line in lines:
        if line[0] == '#':
            of.write(line)

    of.write('\n')

    yan = yanny(parfile,np=True)
    
    yan['BUNDLEMAP']['xpmm'] -= mdx
    yan['BUNDLEMAP']['ypmm'] -= mdy
    
    yan.write(of)
    
    return center, theta

def hex_parameters(region_file):
    
    f = open(region_file,'r')
    coords = [float(i) for i in f.readlines()[-1].split('(')[1].split(')')[0].split(',')]
    
    xc = np.array([coords[::2]])
    yc = np.array([coords[1::2]])
    xdiff = xc - xc.T
    ydiff = yc - yc.T
    
    idx = ([0,1,3,4],[1,0,4,3])
    
    theta = np.arctan(ydiff[idx]/xdiff[idx]).mean()*180/np.pi
    
    center = np.array([xc.mean(), yc.mean()])

    return center, theta

def hex_accuracy_data(numtries,output):
    
    '''find out where we are'''
    x0 = md.proc_apt_query('x1')
    y0 = md.proc_apt_query('y1')
    
    rawimage = md.proc_gige_loop_save_image()
    os.system('manga_copy < {:} > {:}'.format(rawimage,'hextest_OG.fits'))
    os.system('rm {:}'.format(rawimage))

    dx = 0.4 * np.random.ranf(numtries) - 0.2
    dy = 0.4 * np.random.ranf(numtries) - 0.2
    
    f = open(output,'w')
    for i in range(numtries):
        
        md.proc_apt_move_absolute('x1',x0+dx[i])
        md.proc_apt_move_absolute('y1',y0+dy[i])
        md.proc_apt_move_block('x1')
        md.proc_apt_move_block('y1')
        
        name = 'hextest_{}.fits'.format(i)
        rawimage = md.proc_gige_loop_save_image()
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        
        f.write('{:10}{:20.8f}{:20.8f}\n'.format(i,dx[i],dy[i]))
        
    md.proc_apt_move_absolute('x1',x0)
    md.proc_apt_move_absolute('y1',y0)
    md.proc_apt_move_block('x1')
    md.proc_apt_move_block('y1')

    f.close()

def hex_accuracy_anal(infile):

    num, dx, dy = np.loadtxt(infile,unpack=True)
    
    # iraf.geotran('hextest_OG.fits','hextest_OG.trans.fits','/usr/users/eigenbrot/research/MaNGA/manga/ADE_trunk/python/database','20130109')

    # os.system('ds9 -colorbar no -view panner no -view info no -view magnifier no -log -cmap value 6.11 0.75315 {} -regions -system image {} &'.format('hextest_OG.trans.fits','hex.reg'))
    
    # raw_input('center up the hex')
    # os.system('xpaset -p ds9 regions system image')
    # os.system('xpaset -p ds9 regions save {}'.format('hex_OG.reg'))
    # os.system('killall ds9')
    
    OG_center, OG_theta = hex_parameters('hex_OG.reg')
    x = np.array([])
    y = np.array([])
    theta = np.array([])

    for i in range(num.size):
        
        # iraf.geotran('hextest_{:n}.fits'.format(num[i]),
        #              'hextest_{:n}.trans.fits'.format(num[i]),
        #              '/usr/users/eigenbrot/research/MaNGA/manga/ADE_trunk/python/database',
        #              '20130109')

        # os.system('ds9 -colorbar no -view panner no -view info no -view magnifier no -log -cmap value 6.11 0.75315 {} -regions -system image {} &'.format('hextest_{:n}.trans.fits'.format(num[i]),'hex.reg'))
        # raw_input('center up the hex')
        # os.system('xpaset -p ds9 regions system image')
        # os.system('xpaset -p ds9 regions save {}'.format('hex_{:n}.reg'.format(num[i])))
        # os.system('killall ds9')

        cent, thet = hex_parameters('hex_{:n}.reg'.format(num[i]))
        x = np.append(x,cent[0] - OG_center[0])
        y = np.append(y,cent[1] - OG_center[1])
        theta = np.append(theta,thet)

    x *= -0.00285
    y *= 0.00285

    diff_x = x - dx
    diff_y = y - dy

    return diff_x, diff_y, theta
        

def make_hex(output):
    
    x0 = 800. - 300.*np.cos(np.pi/3.)
    y0 = 577. - 300.*np.sin(np.pi/3.)
    hexlist = [x0,y0]
    for i in range(6):
        
        xi = x0 + 300*np.cos(np.pi/3. * i)
        yi = y0 + 300*np.sin(np.pi/3. * i)
        hexlist += [xi, yi]
        x0 = xi
        y0 = yi

    f = open(output,'w')
    f.write('# Region file format: DS9 version 4.1\n')
    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write('image\n')
    f.write('polygon({},{},{},{},{},{},{},{},{},{},{},{})\n'.format(*hexlist))
    f.close()


def z2stage_data(prefix):
    
    md.proc_gige_loop_cmds(1,'0DDDD')
    
    zstart = 25.171
    zend = 34.271
    zstep = 0.75
    zpos = np.arange(zstart,zend,zstep)
    
    for z in zpos:
        
        name = '{}_{:05.2f}.fits'.format(prefix,z)
        
        md.proc_apt_move_absolute('ts1','z2',z)
        md.proc_apt_move_block('ts1','z2')
        
        rawimage = md.proc_gige_loop_save_image(1)
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        
    return

def z2stage_manual(prefix,zpos):
    
    name = '{}_{:05.2f}.fits'.format(prefix,zpos)
    
    rawimage = md.proc_gige_loop_save_image(1)
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))

    return
    
def iso_cent(data, fig = None, tol=1.):
    
    max_val = np.max(data)
    
    xlist = np.array([])
    ylist = np.array([])
    pivotlist = np.array([])
    idxdict = {}
    countlist = np.array([])

    for pivot in np.arange(0,max_val):
        
        hidx = data >= pivot
        lidx = data < pivot
        count = np.sum(hidx)
        if count < 31000:
            break
        idxdict[pivot] = [hidx,lidx]
        pivotlist = np.append(pivotlist, pivot)
        countlist = np.append(countlist,count)
    
    tmpdata = data.copy()
    for pivot in pivotlist:

        tmpdata[idxdict[pivot][0]] = 1
        tmpdata[idxdict[pivot][1]] = 0
      
        x, y = centroid(tmpdata)
        
        xlist = np.append(xlist,x)
        ylist = np.append(ylist,y)

        
    # xints = np.array(np.round(xlist),dtype=np.int)
    # yints = np.array(np.round(ylist),dtype=np.int)
    # xmode = np.argmax(np.bincount(xints))
    # ymode = np.argmax(np.bincount(yints))

    # xidx = np.where(xints == xmode)[0]
    # yidx = np.where(yints == ymode)[0]
    
    # xcleanidx = find_chunk(xidx)
    # ycleanidx = find_chunk(yidx)
    
    # xidx = xidx[xcleanidx]
    # yidx = yidx[ycleanidx]

    # xidx = find_chunk(xlist)
    # yidx = find_chunk(ylist)

    idx = find_chunk(xlist,ylist,tol=tol)
    xidx = idx
    yidx = idx

    xcent = np.mean(xlist[xidx])
    ycent = np.mean(ylist[yidx])

    if fig:
        xax = fig.add_subplot(311)
        yax = fig.add_subplot(312)
        cax = fig.add_subplot(313)
    
    else:
        xax = plt.figure().add_subplot(111)
        yax = plt.figure().add_subplot(111)
        cax = plt.figure().add_subplot(111)
    
    xax.plot(pivotlist,xlist)
    xax.axvline(x=pivotlist[xidx.min()])
    xax.axvline(x=pivotlist[xidx.max()])
    yax.axvline(x=pivotlist[yidx.min()])
    yax.axvline(x=pivotlist[yidx.max()])
    yax.plot(pivotlist,ylist)
    
    xax.set_xlabel('Pivot value')
    xax.set_ylabel('X center [px]')
    
    yax.set_xlabel('Pivot value')
    yax.set_ylabel('Y center [px]')
    
    cax.plot(pivotlist,countlist)
    cax.axvline(x=pivotlist[yidx.min()])
    cax.axvline(x=pivotlist[yidx.max()])
    cax.set_xlabel('Pivot value')
    cax.set_ylabel('Num Pix')
    
    # xax.figure.show()
    # yax.figure.show()
    # cax.figure.show()
    
    return xcent, ycent#, xlist, ylist, pivotlist

def find_chunk(x, y, tol=1.):
    
#    dd = np.diff(x)
    best_size = 0
    best_idx = []
    idx = []
    size = 0
    valx = x[0]
    valy = y[0]

    # if np.all(dd == 1):
    #     return range(len(x))
    
    for i, (xx, yy) in enumerate(zip(x,y)):
        
        if np.abs(xx - valx) <= tol and np.abs(yy - valy) <= tol:
            idx.append(i)
            size += 1

        elif size > best_size:
            best_size = size
            best_idx = idx
            size = 0
            idx = []
            valx = xx
            valy = yy

        else:
            size = 0
            idx = []
            valx = xx
            valy = yy

    best_idx.append(best_idx[-1] + 1)
    
    return np.array(best_idx)

def center_fiber_gross(threshold = 1e6):
    
    name = 'tmp.fits'
#    searchposlist = [16,12,18,10,20,8,22,6,24]
    searchposlist = np.arange(2,20,2)
    for pos in searchposlist:
        
        md.proc_apt_move_absolute('ts1','x2',pos)
        md.proc_apt_move_block('ts1','x2')
        rawimage = md.proc_gige_loop_save_image(1)
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        data = pyfits.open(name)[0].data
        print np.sum(data)
        if np.sum(data) >= threshold:
            return pos
        
    return -1

def center_fiber_fine(threshold = 15):
    
    name = 'tmp.fits'
    kx = -4.576e-3
    ky = 5.5086e-3
    rawimage = md.proc_gige_loop_save_image(1)
    os.system('manga_copy < {:} > {:}'.format(rawimage,name))
    os.system('rm {:}'.format(rawimage))
    data = pyfits.open(name)[0].data
    xf, yf = centroid(data)
    x0,y0 = [i/2. for i in data.shape]
    
    x_move = kx*(x0 - xf)
    y_move = ky*(y0 - yf)
    
    #raw_input('{}, {}, {}, {}, {}, {}'.format(x0,y0,xf,yf,x_move/kx,y_move/ky))
    
    while ((x_move/kx)**2 + (y_move/ky)**2)**0.5 > threshold:
        xm = md.proc_apt_query('ts1','x2')
        ym = md.proc_apt_query('ts1','y2')
        
        md.proc_apt_move_absolute('ts1','x2',xm + x_move)
        md.proc_apt_move_absolute('ts1','y2',ym + y_move)
        md.proc_apt_move_block('ts1','x2')
        md.proc_apt_move_block('ts1','y2')
        
        rawimage = md.proc_gige_loop_save_image(1)
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        data = pyfits.open(name)[0].data
        xf, yf = centroid(data)
        print xf,yf, x0, y0
        x_move = kx*(x0 - xf)
        y_move = ky*(y0 - yf)
        
        #raw_input('{}, {}, {}, {}, {}, {}'.format(x0,y0,xf,yf,x_move/kx,y_move/ky))    
    return xf, yf

def compare_results(parlist, labellist, capture=False):
    
    fig = plt.figure(figsize=(8,6))
    fig.subplots_adjust(hspace=0.00001)
    ax1 = fig.add_subplot(211)
    ax1.set_xlabel('')
    ax1.set_xticks([])
    ax1.set_ylabel('Throughput')
    ax2 = fig.add_subplot(212)
    ax2.set_xlabel('Fiber number')
    ax2.set_ylabel('Difference ({} - {})'.format(*labellist))
    
    tput_list = []
    for par, label in zip(parlist, labellist):
        
        yan = yanny(par,np=True)
        fibers = yan['BUNDLEMAP']['fnum']
        tputs = yan['BUNDLEMAP']['tput']
        if np.mean(tputs) < 1.0:
            tputs *= 100
        ax1.plot(fibers,tputs,'.',label=label)
        if len(tput_list) > 0:
            ax2.plot(fibers, tput_list[0] - tputs,'.',label='{} - {}'.format(labellist[0],label))
        tput_list.append(tputs)
    
    ax1.legend(loc=0,numpoints=1,scatterpoints=1,frameon=False,fontsize=9)
    ax1.set_ylim(85,97)
    
    ax2.set_ylim(-5,5)
    if len(tput_list) > 2:
        ax2.legend(loc=0,numpoints=1,scatterpoints=1,frameon=False,fontsize=9)
    
    diff = np.mean(np.sqrt((tput_list[0] - tput_list[1])**2))
    print diff
    #ax1.plot(fibers,tput_list[1]+diff,'.')
    #ax2.plot(tput_list[0],tput_list[1],'.')
    #ax2.plot(tput_list[0],tput_list[0])
    
    if capture:
        plt.show()
    # else:
    #     fig.show()
    return tput_list, fig

def compare_sb(imagelist, labellist):
    
    fig = plt.figure(figsize=(8,6))
    ax = fig.add_subplot(111)
    ax.set_xlabel('radius [px]')
    ax.set_ylabel('Counts [ADU]')
    
    for image, label in zip(imagelist, labellist):
        center = Imager.iso_cent(image)
        r, sb, e = Imager.annulize(image, center)
        print 'Spacing is {} +/- {}'.format(np.mean(np.diff(r)), np.std(np.diff(r)))
        print 'or {}'.format(np.diff(r)[0])
        r1 = r - r[0]
        r2 = r1 + np.diff(r)[0]#np.mean(np.diff(r))
        areas = np.pi*(r2**2. - r1**2.)
        sb /= areas
        ax.plot(r, sb/np.mean(sb), label=label)
        
    ax.legend(loc=0)
    fig.show()
    
    return

def rereduce(folder,fnum, output):
    
    DI = Imager.DirectImages(md)
    direct = glob('{}/direct*.fits'.format(folder))[0]
    print 'direct is {}'.format(direct)
    
    radius = DI.get_radius(direct,EEcut=0.45)
    rf = radius * 4.97/fnum
    print 'radius at f/{} is {} px'.format(fnum,rf)
    center = Imager.iso_cent(direct)
    r, sb, e = Imager.annulize(direct, center)
    flux = np.cumsum(sb)
    direct_ADU = np.interp(rf,r,flux)
    print 'direct ADU is {}'.format(direct_ADU)
    
    fibers = glob('{}/fiber*.fits'.format(folder))
    results = np.zeros((len(fibers),),
                        dtype=[('finblock','i'),
                               ('fnum','i'),
                               ('tput','f4')])
    for i, fiber in enumerate(fibers):
        fnum = int(fiber.split('_')[1])
        if fnum == -2:
            slitnum = len(fibers)
        elif fnum == -1:
            slitnum = 1
        elif fnum > 0:
            slitnum = fnum + 1
        print 'doing fiber {}, slitnum {}'.format(fnum,slitnum)
        center = Imager.iso_cent(fiber)
        r, sb, e = Imager.annulize(fiber,center)
        flux = np.cumsum(sb)
        fiber_ADU = np.interp(rf,r,flux)
        tput = fiber_ADU/direct_ADU
        results[i]['finblock'] = slitnum
        results[i]['tput'] = tput
        results[i]['fnum'] = fnum
        
    
    results = np.sort(results,order='finblock')
    par = yanny(filename=None,np=True,debug=True)
    struct = par.dtype_to_struct(results.dtype,structname='BUNDLEMAP',enums=dict())
    par['symbols']['struct'] = struct['struct']
    par['symbols']['enum'] = struct['enum']
    par['symbols']['BUNDLEMAP'.upper()] = struct['BUNDLEMAP'.upper()]
    par['BUNDLEMAP'.upper()] = results
    f = open(output,'w')
    par.write(f)
    f.close()
    return
    
def compare_cart_CCD(harness_list,output):
    
    PD_base = '/Users/SDSSfiber/SDSSbench/data/shared_results/UWisc'
    CCD_base = '/Users/SDSSfiber/SDSSbench/data/*'
    CTECH_base = '/Users/SDSSfiber/SDSSbench/data/shared_results/C-Tech'
    
    pp = PDF(output)

    manum = np.array([])
#     rmsarr = np.array([])
#     meanarr = np.array([])
    mediandict = {'PI': np.array([]),
                  'CP': np.array([]),
                  'CI': np.array([])}
    medrmsdict = {'PI': np.array([]),
                  'CP': np.array([]),
                  'CI': np.array([])}
    bigPD = np.array([])
    bigCCD = np.array([])
    bigCTECH = np.array([])
    
    for harness in harness_list:
        
        PDdirlist = glob('{}/{}*postcoat*'.format(PD_base,harness))
        CCDdirlist = glob('{}/{}*imaging*'.format(CCD_base,harness))
        CTECHdirlist = glob('{}/[Mm][Aa]{}*'.format(CTECH_base,harness[2:]))
        PDdirlist.sort()
        CCDdirlist.sort()
        CTECHdirlist.sort()

        PDfile = '{}/{}-results.par'.format(PDdirlist[-1],harness)
        CCDfile = '{}/{}-results.par'.format(CCDdirlist[-1],harness)
        CTECHfile = '{}/{}-results.par'.format(CTECHdirlist[-1],harness)

        print 'Found PD data: {}'.format(PDfile)
        print 'Found CCD data: {}'.format(CCDfile)
        print 'Found C-Tech data: {}'.format(CTECHfile)

        res, fig = compare_results([PDfile,CCDfile,CTECHfile],['PD','CCD','C-Tech'])
        PD, CCD, CTECH = res
        diffPI = PD - CCD
        diffCP = CTECH - PD
        diffCI = CTECH - CCD
        rms = np.sqrt(np.mean(diffPI**2))
        mean = np.mean(diffPI)
        medianPI = np.median(diffPI)
        medrmsPI = np.sqrt(np.mean((diffPI - medianPI)**2))
        medianCP = np.median(diffCP)
        medrmsCP = np.sqrt(np.mean((diffCP - medianCP)**2))
        medianCI = np.median(diffCI)
        medrmsCI = np.sqrt(np.mean((diffCI - medianCI)**2))

        fig.suptitle('{:}\nmean (PD - CCD) = {:5.2f}, rms diff (about 0) = {:5.2f}'.format(harness,mean,rms))
        pp.savefig(fig)
        
#         rmsarr = np.append(rmsarr, rms)
#         meanarr = np.append(meanarr,mean)
        mediandict['PI'] = np.append(mediandict['PI'],medianPI)
        medrmsdict['PI'] = np.append(medrmsdict['PI'],medrmsPI)
        mediandict['CP'] = np.append(mediandict['CP'],medianCP)
        medrmsdict['CP'] = np.append(medrmsdict['CP'],medrmsCP)
        mediandict['CI'] = np.append(mediandict['CI'],medianCI)
        medrmsdict['CI'] = np.append(medrmsdict['CI'],medrmsCI)
#         medianarr = np.append(medianarr,median)
#         medrmsarr = np.append(medrmsarr,medrms)
        manum = np.append(manum, int(harness[2:]))
        bigPD = np.append(bigPD, PD)
        bigCCD = np.append(bigCCD, CCD)
        bigCTECH = np.append(bigCTECH,CTECH)

#     ax = plt.figure().add_subplot(111)
#     ax.set_xlabel('ma number')
#     ax.set_ylabel('PD - CCD RMS')
#     ax.set_title('Generated on {}'.format(time.asctime()))
#     ax.plot(manum,rmsarr,'.')
#     pp.savefig(ax.figure)
    
    ax2, ax3 = make_comp_plots(mediandict['PI'],medrmsdict['PI'],manum,bigPD,bigCCD,'PD - CCD')
    pp.savefig(ax2.figure)
    pp.savefig(ax3.figure)
    
    ax4, ax5 = make_comp_plots(mediandict['CP'],medrmsdict['CP'],manum,bigCTECH,bigPD,'C-Tech - PD')
    pp.savefig(ax4.figure)
    pp.savefig(ax5.figure)
    
    ax6, ax7 = make_comp_plots(mediandict['CI'],medrmsdict['CI'],manum,bigCTECH,bigCCD,'C-Tech - CCD')
    pp.savefig(ax6.figure)
    pp.savefig(ax7.figure)
    
    print np.sqrt(np.mean((bigPD - bigCCD)**2))
    print 'Average median difference is {:5.2f} +/- {:5.3f}'.format(np.mean(mediandict['PI']), np.std(mediandict['PI']))
    pp.close()

    plt.close('all')

    return bigPD, bigCCD
        

def compare_cart_UWC(harness_list,output):
    
    CTECH_base = '/Users/SDSSfiber/SDSSbench/data/shared_results/C-Tech'
    UW_base = '/Users/SDSSfiber/SDSSbench/data/shared_results/UWisc'

    pp = PDF(output)

    manum = np.array([])
    rmsarr = np.array([])
    meanarr = np.array([])
    medianarr = np.array([])
    medrmsarr = np.array([])
    bigCTECH = np.array([])
    bigUW = np.array([])
    
    for harness in harness_list:
        
        CTECHdirlist = glob('{}/[Mm][Aa]{}*'.format(CTECH_base,harness[2:]))
        UWdirlist = glob('{}/[Mm][Aa]{}*'.format(UW_base,harness[2:]))
        CTECHdirlist.sort()
        UWdirlist.sort()

        CTECHfile = '{}/{}-results.par'.format(CTECHdirlist[-1],harness)
        UWfile = '{}/{}-results.par'.format(UWdirlist[0],harness)
        
        if 'postcoat' in UWfile:
            UWfile = '{}/{}-results.par'.format(UWdirlist[1],harness)

        print 'Found CTECH data: {}'.format(CTECHfile)
        print 'Found UW data: {}'.format(UWfile)

        res, fig = compare_results([CTECHfile,UWfile],['CTECH','UW'])
        CTECH, UW = res
        diff = CTECH - UW
        rms = np.sqrt(np.mean(diff**2))
        mean = np.mean(diff)
        median = np.median(diff)
        medrms = np.sqrt(np.mean((diff - median)**2))

        fig.suptitle('{:}\nmean diff = {:5.2f}, rms diff (about 0) = {:5.2f}'.format(harness,mean,rms))
        pp.savefig(fig)
        
        rmsarr = np.append(rmsarr, rms)
        meanarr = np.append(meanarr,mean)
        medianarr = np.append(medianarr,median)
        medrmsarr = np.append(medrmsarr,medrms)
        manum = np.append(manum, int(harness[2:]))
        bigCTECH = np.append(bigCTECH, CTECH)
        bigUW = np.append(bigUW, UW)

    ax = plt.figure().add_subplot(111)
    ax.set_xlabel('ma number')
    ax.set_ylabel('CTECH - UW RMS')
    ax.set_title('Generated on {}'.format(time.asctime()))
    ax.plot(manum,rmsarr,'.')
    pp.savefig(ax.figure)
    
    ax2, ax3 = make_comp_plots(medianarr,medrmsarr,manum,bigCTECH,bigUW,'C-Tech - UWisc')
    pp.savefig(ax2.figure)
    pp.savefig(ax3.figure)
    
    print np.sqrt(np.mean((bigCTECH - bigUW)**2))
    print 'Average median difference is {:5.2f} +/- {:5.3f}'.format(np.mean(medianarr), np.std(medianarr))
    pp.close()

    return bigCTECH, bigUW
        

def make_comp_plots(medianarr,medrmsarr,manum,big1,big2,label):
    
    ax = plt.figure().add_subplot(111)
    ax.set_title('Generated on {}\nAverage median difference is {:5.2f} +/- {:5.3f}'.\
                  format(time.asctime(),np.mean(medianarr), np.std(medianarr)))
    ax.set_xlabel('Median difference ({})'.format(label))
    ax.set_ylabel('RMS about median difference')
    ax.set_xlim(-4,4)
    ax.set_ylim(0,2.5)
    ax.plot(medianarr,medrmsarr,'.')
    ax.add_patch(Rectangle((-1,0),2,1,color='g',alpha=0.2))
    idx = np.where((np.abs(medianarr) > 1) | (medrmsarr > 1))
    ax.plot(medianarr[idx],medrmsarr[idx],'r.')
    for i in idx[0]:
        ax.text(medianarr[i],medrmsarr[i]+0.03,'ma{:03n}'.format(manum[i]),color='r',fontsize=7)
    
    ax2 = plt.figure().add_subplot(111)
    ax2.set_title('Generated on {}\nAverage median difference is {:5.2f} +/- {:5.3f}'.\
                  format(time.asctime(),np.mean(medianarr), np.std(medianarr)))
    ax2.set_xlabel('Difference ({})'.format(label))
    ax2.set_ylabel('N')
    n, binedge, _ = ax2.hist(big1 - big2, bins=200,histtype='stepfilled',alpha=0.4,facecolor='g')
    ax3 = ax2.twinx()
    ax3.set_ylabel('CDF (normalized)')
    bins = 0.5*(binedge[1:] + binedge[:-1])
    cdf = np.cumsum(n)
    ax3.plot(bins,cdf*1.0/cdf.max())
    
    return ax, ax2
    
def manual_push(directory):
    
    datlist = glob('{}/*.dat'.format(directory))
    datlist.sort()
    parlist = glob('{}/*.par'.format(directory))
    parlist.sort()
    
    print parlist, datlist
    
    dir = os.path.basename(directory)
    dirsplit = dir.split('_')
    if len(dirsplit) > 2:
        pushdir = '~/SDSSbench/data/shared_results/UWisc/{}'.format('_'.join(dirsplit[:2]))
    else:
        pushdir = '~/SDSSbench/data/shared_results/UWisc/{}'.format(dir)
        
    print pushdir
        
    os.mkdir(os.path.expanduser(pushdir))
    os.chdir(os.path.expanduser(pushdir))
    os.system('cp {} {}'.format(parlist[-1],pushdir))
    os.system('cp {} {}'.format(datlist[-1],pushdir))
    os.system('cp {}/*ifu*.log.map {}'.format(directory,pushdir))
    os.system('cp {}/*_fibers.fits {}'.format(directory,pushdir))
    os.system('cp {}/*_face.fits {}'.format(directory,pushdir))
    os.system('git add .')

def kstats():
    
    config_list = glob(os.path.expanduser('~/SDSSbench/data/*/*/manga.cfg'))
    print 'found {} records'.format(len(config_list))
    
    kx_arr = np.array([])
    ky_arr = np.array([])
    k_arr = np.array([])
    for config in config_list:
        print config
        cf = ConfigParser.RawConfigParser()
        cf.read(config)
        try:
            section = "PP_MOTOR_CAL"
            k_arr = np.append(k_arr,cf.getfloat(section,'k'))
        except:
            pass
        try:
            section = "IFU_MOTOR_CAL"
            kx_arr = np.append(kx_arr, cf.getfloat(section, "kx"))
            ky_arr = np.append(kx_arr, cf.getfloat(section, "ky"))
        except ConfigParser.NoSectionError:
            try:
                for i in [1,2,3]:
                    section = "MINNIE_{}_MOTOR_CAL".format(i)
                    kx_arr = np.append(kx_arr, cf.getfloat(section, "kx"))
                    ky_arr = np.append(kx_arr, cf.getfloat(section, "ky"))
            except ConfigParser.NoSectionError:
                pass
            
    print 'recorded {} motor constants'.format(kx_arr.size)
    
    ky_arr[np.where(ky_arr > 0)] *= -1.
    fig = plt.figure()
    axx = fig.add_subplot(121)
    axy = fig.add_subplot(122)
    axx.set_xlabel('kx')
    axx.set_ylabel('N')
    axy.set_xlabel('ky')
    axy.set_ylabel('N')
    
    axx.hist(kx_arr,bins=100)
    axy.hist(ky_arr,bins=100)
    
    fig.show()
    
    print '<kx> = {:5.7f} +/- {:5.7f}'.format(np.mean(kx_arr),np.std(kx_arr))
    print '<ky> = {:5.7f} +/- {:5.7f}'.format(np.mean(ky_arr),np.std(ky_arr))
    print '<ppk> = {:5.7f} +/- {:5.7f}'.format(np.mean(k_arr),np.std(k_arr))
    return kx_arr, ky_arr, k_arr
    
    
def percent(harness):
    
    basedir = '/usr/users/eigenbrot/research/MaNGA/metro_trunk'
    parfile = glob('{0}/{1}/{1}*.par'.format(basedir,harness))[0]
    print 'got {}'.format(parfile)
    
    f = open(parfile,'r')
    header = [l for l in f.readlines() if l[0] == '#']
    header[1] = header[1][:-1] + ', modified to have percentage throughputs on {}\n'.format(time.asctime())
    f.close()
    
    par = yanny(parfile,np=True)
    par['BUNDLEMAP']['tput'] *= 100.
    f = open(parfile,'w')
    f.write(''.join(header))
    par.write(f)
    f.close()
    
    return
    
def check_for_order(dir):
    
    datlist = glob('{}/*.dat'.format(dir))
    
    idx = 0 
    if len(datlist) > 1:
        print datlist
        idx = raw_input("datlist")
        
    datfile = datlist[idx]
    
    with open(datfile, 'r') as f:
        lines = f.readlines()
    
    vgnums = []
    for i, l in enumerate(lines):
        if l[0:4] == 'Thru' and i > 30:
            vgnums.append(int(l.split()[2]))
    
    if (np.diff(vgnums) == np.ones(len(vgnums) - 1)).all():
        return True
    else:
        return False
    
def check_all_orders(toplevel):
    
    dirlist = glob('{}/*'.format(toplevel))
    
    for d in dirlist:
        try:
            order = check_for_order(d)
            if order is False:
                print order, d
                raw_input('')
        except:
            pass
        
    return

if __name__ == '__main__':
    PDdir = sys.argv[1]
    CCDdir = sys.argv[2]
     
    PDfile = glob('{}/*results.par'.format(PDdir))[0]
    CCDfile = glob('{}/*results.par'.format(CCDdir))[0]
      
    compare_results([PDfile,CCDfile],['PD','CCD'],True)


cart5_mlist = ['ma101', 'ma102', 'ma103', 'ma104', 'ma105', 'ma106', 'ma107', 'ma108', 'ma109', 'ma110', 'ma111', 'ma112', 'ma114', 'ma115', 'ma116', 'ma117', 'ma118', 'ma119', 'ma120', 'ma121', 'ma135']    
cart4_mlist = ['ma080', 'ma081', 'ma082', 'ma083', 'ma084', 'ma085', 'ma086', 'ma087', 'ma088', 'ma089', 'ma090', 'ma091', 'ma092', 'ma093', 'ma094', 'ma095', 'ma096', 'ma097', 'ma098', 'ma099', 'ma100']
cart3_mlist = ['ma059', 'ma060', 'ma061', 'ma062', 'ma063', 'ma064', 'ma065', 'ma066', 'ma067', 'ma068', 'ma069', 'ma070', 'ma071', 'ma072', 'ma073', 'ma074', 'ma075', 'ma076', 'ma077', 'ma078', 'ma079']
#mt.compare_results(['/Users/SDSSfiber/SDSSbench/tests/20140822/PD_ma051/ma051-results.par','/Users/SDSSfiber/SDSSbench/tests/20140903/imaging_ND/ma051-results.par','/Users/SDSSfiber/SDSSbench/tests/20140821/imaging_ma051/ma051-results.par'],['PD','ND','imaging'])
#mt.compare_results(['/Users/SDSSfiber/SDSSbench/tests/20140804/full_harness_prod/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140903/imaging_ND_ma053_smallest/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140807/imaging_smaller/ma053-results.par'],['PD','ND','imaging'])
#mt.compare_results(['/Users/SDSSfiber/SDSSbench/tests/20140804/full_harness_prod/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140807/imaging_smaller/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140818/imaging_offset/ma053-results.par'],['PD','Image','offset'])
#mt.compare_results(['/Users/SDSSfiber/SDSSbench/tests/20140804/full_harness_prod/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140807/imaging_smaller/ma053-results.par','/Users/SDSSfiber/SDSSbench/tests/20140806/imaging_center/ma053-results.par'],['PD','smaller','small'])
