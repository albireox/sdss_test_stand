#! /usr/bin/env python

import numpy as np
import ConfigParser
import Tkinter as tk
import tkFileDialog as tkFD
import os
import sys
import glob
import time
import pyfits
from pyraf import iraf
from yanny import yanny
import scipy.optimize as spo
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_pdf import PdfPages as PDF
import matplotlib.pyplot as plt
import manga_devices as md
glob = glob.glob
tau = np.pi*2.

class MetrologyWindow:
    '''This class is an interactive wrapper for updating a pre-computed
    metrology solution so that the fiber centers are referenced to the center
    of the hex-ferrule rather than the central IFU fiber.

    It's primary purpose is to load the correct files, make sure they are
    good, and then launch ds9 and promt the user to define a region file that
    locates the hex ferrule.

    This program is intended to be run directly from the command line,
    independent of the rest of the test stand software.
    '''

    def __init__(self, master, loadhex=False):
        '''Let's set some stuff up!
        '''

        '''Like constants!'''
        self.kx = 0.00285
        self.ky = 0.00285
        self.alpha = 0.0
        self.database_soln = '20140109'
        self.cart_median_pack = 0.1554
        self.cart_median_pack_std = 0.0016
        self.hex_x_std = 0.00205
        self.hex_y_std = 0.00447
        self.hex_rot_std = 0.002590
        self.fiber_find_std = 0.0003
        if os.getlogin() == 'SDSSfiber':
            self.wiki_path = '/Users/SDSSfiber/SDSSbench/wiki/wiki/cart6'
            self.metro_trunk = '/Users/SDSSfiber/SDSSbench/wiki/metro_trunk'
            self.data_path = '/Users/SDSSfiber/SDSSbench/wiki/data_trunk/UWisc'
        else:
            self.wiki_path = '/usr/users/eigenbrot/research/MaNGA/manga_wiki/wiki_str/spare_cart'
            self.metro_trunk = '/usr/users/eigenbrot/research/MaNGA/metro_trunk'
            self.data_path = '/usr/users/eigenbrot/research/MaNGA/data_trunk/UWisc'

        self.wiki_file = 'spare_cart.txt'
        self.wiki_string = ''

        '''Variables!'''
        self.master = master
        self.Step1 = tk.Frame(master)
        self.Step2 = tk.Frame(master)
        self.Step3 = tk.Frame(master)
        self.red_dir = tk.StringVar()
        self.face_image = tk.StringVar()
        self.fiber_image = tk.StringVar()
        self.reg_file = tk.StringVar()
        self.par_file = tk.StringVar()
        self.hex_center = tk.StringVar()
        self.fiber_center = tk.StringVar()
        self.px_diff = tk.StringVar()
        self.mm_diff = tk.StringVar()
        self.warning_text = tk.StringVar()
        self.theta_rot_str = tk.StringVar()
        self.conc_str = tk.StringVar()
        self.theta_hex_str = tk.StringVar()
        self.ferrule_str = tk.StringVar()
        self.k_str = tk.StringVar()
        self.k_str.set('({:8.5f}, {:8.5f})'.format(self.kx,self.ky))
        self.cart_num = tk.IntVar()
        self.cart_num.set(99)
        
        '''The difference between self.hex_reg and self.reg_file is that
        self.hex_reg is a default name that gets used to save region
        files. self.reg_file will always be the name of the region file about
        to be used for reduction.'''

        self.hex_reg = 'hex.reg'
        self.hex_template = 'hex_temp.reg'
        self.bin_num_dict = { 1: 5, 2: 15, 3: 25, 4: 40, 5: 50, 6: 30}
        self.map_file = ''
        self.conc_x = 0
        self.conc_y = 0
        self.theta_rot = 0
        self.theta_hex = 0
        self.fcenter = np.array([])
        self.yanny = None
        self.median_hex_plot = []
        self.is_minnie = False
        self.face_list = []
        self.face_ferrule = {}
        self.fiber_ferrule = {}
        self.minnie_ferrules = []
        self.current_ferrule_code = None
        self.current_facenum = None
#        self.err_plots = []
        self.median_hist_plots = []
        self.rank = 0
        self.median_pack = 0.0
        self.median_hex_x = np.array([])
        self.median_hex_y = np.array([])
        self.fiber_hex_shift_x = 0.0
        self.fiber_hex_shift_y = 0.0
        self.fiber_hex_rot = 0.0
        self.auto = False

        self.master.protocol('WM_DELETE_WINDOW',self.master.quit)
        self.master.wm_title('Metrology solution calculator')

        '''Colors!'''
        step1_color = {}
        step2_color = {}
        step3_color = {}
        instructfont = (None, 13, 'bold')
        
        '''Instruction windows!'''
        ###STEP 1###
        '''Step 1 asks the user to identify the directory that contains a
        completed test stand run. It then loads the necessary files from that
        run
        '''
        tk.Label(self.Step1, text='1. Select results directory:',
                 font=instructfont,fg='red',**step1_color).grid(column=0,row=0,columnspan=2)
        tk.Entry(self.Step1,width=50,textvariable=self.red_dir,**step1_color).grid(column=0,row=1)
        tk.Button(self.Step1,text='...',command=self.choose_dir,**step1_color).grid(column=1,row=1)
        tk.Label(self.Step1, text='Enter cartridge number:',justify='right',**step1_color).grid(column=0,row=2,sticky='e')
        tk.Entry(self.Step1,width=10,textvariable=self.cart_num,**step1_color).grid(column=1,row=2)
        
        tk.Label(self.Step1,text='Found face image:',font=instructfont,justify='left',**step1_color).grid(column=0,row=3,sticky='w')
        tk.Label(self.Step1,textvariable=self.face_image,justify='left',**step1_color).grid(column=0,row=4,columnspan=2,sticky='w')
        tk.Label(self.Step1,text='Found fiber image:',font=instructfont,justify='left',**step1_color).grid(column=0,row=5,sticky='w')
        tk.Label(self.Step1,textvariable=self.fiber_image,justify='left',**step1_color).grid(column=0,row=6,columnspan=2,sticky='w')
        tk.Label(self.Step1,text='Found results file:',font=instructfont,justify='left',**step1_color).grid(column=0,row=7,sticky='w')
        tk.Label(self.Step1,textvariable=self.par_file,justify='left',**step1_color).grid(column=0,row=8,columnspan=2,sticky='w')
        
        self.step1OK = tk.Button(self.Step1,text='Looks good!',command=self.goStep2,**step1_color)
        self.step1OK_minnie = tk.Button(self.Step1,text='Looks good!',command=self.setup_minnie,**step1_color)

        ###STEP 2###
        '''Step 2 opens the face image in ds9 and shows the user how to define
        a proper hexagon region.
        '''
        if os.path.exists(os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/ds9_ex.gif')):
            self.ds9_ex = tk.PhotoImage(file=os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/ds9_ex.gif'))
        else:
            self.ds9_ex = tk.PhotoImage(file=os.path.expanduser('~/research/MaNGA/test_stand_software/manga/ADE_trunk/python/ds9_ex.gif'))

        tk.Label(self.Step2, text='2. Place ds9 hex region exactly on top of hex channel:',
                 font=instructfont,fg='red',**step2_color).grid(column=0,row=0,columnspan=2)
        tk.Label(self.Step2, text='Drag any of the marked points to scale region',**step2_color).grid(column=0,row=1,columnspan=2,sticky='w')
        tk.Label(self.Step2, text='Hold <shift> while clicking to rotate region',**step2_color).grid(column=0,row=2,columnspan=2,sticky='w')
        tk.Label(self.Step2, image = self.ds9_ex).grid(column=0,row=3,columnspan=2)
        tk.Label(self.Step2, text='when done click "save ds9 region" to load region below. Then hit continue.', **step2_color).grid(column=0,row=4,columnspan=2,sticky='w')
        tk.Entry(self.Step2,width=50,textvariable=self.reg_file,**step2_color).grid(column=0,row=5)
        tk.Button(self.Step2,text='...',command=self.choose_reg,**step2_color).grid(column=1,row=5,sticky='w')
        tk.Button(self.Step2,text='Save ds9 region',command=self.save_reg,**step2_color).grid(column=1,row=6,sticky='w')
        tk.Button(self.Step2,text='Continue',command=self.goStep3,**step2_color).grid(column=0,row=6)
        self.warning = tk.Label(self.Step2,textvariable=self.warning_text,fg='red',**step2_color)
        self.warning.grid(column=0,row=7,columnspan=2)
        self.warning.grid_forget()

        ###STEP 3###
        '''Step 3 computes the difference in position between the center of
        the hexagon and the center of the central fiber. It then shows the
        user the results and asks the user to confirm they are not crazy
        before finally updating information the .par file generated by the
        test stand.
        '''
        tk.Label(self.Step3, text='3. Check Updated Solution',
                 font=instructfont,fg='red',**step3_color).grid(column=0,row=0,columnspan=2)
        tk.Label(self.Step3,textvariable=self.ferrule_str,font=instructfont,**step3_color).grid(column=0,row=1,columnspan=2)
        tk.Label(self.Step3,text='Hex-ferrule concentricity error [mm]',font=instructfont,justify='left',**step3_color).grid(column=0,row=2,sticky='w')
        tk.Label(self.Step3,text='Hex-pin angle [deg]',font=instructfont,justify='left',**step3_color).grid(column=1,row=2,sticky='w')
        tk.Label(self.Step3,text='Hex Center [px]',font=instructfont,justify='left',**step3_color).grid(column=0,row=4,sticky='w')
        tk.Label(self.Step3,text='Central fiber [px]',font=instructfont,justify='left',**step3_color).grid(column=1,row=4,sticky='w')
        tk.Label(self.Step3,text='Difference [px]',font=instructfont,justify='left',**step3_color).grid(column=0,row=6,sticky='w')
        tk.Label(self.Step3,text='Hex-CCD angle [deg]',font=instructfont,justify='left',**step3_color).grid(column=1,row=6,sticky='w')
        tk.Label(self.Step3,text='Motor constant [mm/px]',font=instructfont,justify='left',**step3_color).grid(column=0,row=8,sticky='w')
        tk.Label(self.Step3,text='Difference [mm]',font=instructfont,justify='left',**step3_color).grid(column=1,row=8,sticky='w')
        
        tk.Label(self.Step3,textvariable=self.conc_str,justify='left',**step3_color).grid(column=0,row=3,sticky='w')
        tk.Label(self.Step3,textvariable=self.theta_rot_str,justify='left',**step3_color).grid(column=1,row=3,sticky='w')
        tk.Label(self.Step3,textvariable=self.hex_center,justify='left',**step3_color).grid(column=0,row=5,sticky='w')
        tk.Label(self.Step3,textvariable=self.fiber_center,justify='left',**step3_color).grid(column=1,row=5,sticky='w')
        tk.Label(self.Step3,textvariable=self.px_diff,justify='left',**step3_color).grid(column=0,row=7,sticky='w')
        tk.Label(self.Step3,textvariable=self.theta_hex_str,justify='left',**step3_color).grid(column=1,row=7,sticky='w')
        tk.Label(self.Step3,textvariable=self.k_str,justify='left',**step3_color).grid(column=0,row=9,sticky='w')
        tk.Label(self.Step3,textvariable=self.mm_diff,justify='left',**step3_color).grid(column=1,row=9,sticky='w')

        self.writeOutButton = tk.Button(self.Step3,text='Write and exit',command=self.write_out,**step3_color)
        self.nextMinnieButton = tk.Button(self.Step3,text='Next IFU',command=self.next_minnie,**step3_color)
        self.writeOutButton.grid(column=0,row=11,columnspan=2)
        self.nextMinnieButton.grid(column=0,row=11,columnspan=2)
        self.nextMinnieButton.grid_forget()
        
        '''if the user started the program with a previously made hex file,
        save them some trouble.'''
        if loadhex:
            self.auto = True
            self.reg_file.set(os.getcwd()+'/'+loadhex)
            self.choose_dir(os.getcwd())
            if self.is_minnie:
                self.setup_minnie()
            else:
                self.goStep2()
            self.goStep3()
        else:
            self.Step1.pack()

    def choose_dir(self,name=False):
        '''A simple function to spawn a folder-select dialog and then check
        that the selected folder contains the needed files.
        '''

        if not name:
            name = tkFD.askdirectory(title='Choose folder with test stand results',
                                     initialdir=os.getcwd())
        
        OK = True
        self.red_dir.set(name)
        
        face = glob(name+'/*_face.fits')
        if len(face) < 1:
            face = ['NOT FOUND']
            OK = False
        elif len(face) == 3:
            self.is_minnie = True
            self.face_list = face
        elif len(face) > 1 and len(face) != 3:
            face = ['FOUND TOO MANY']
            OK = False

        fibers = glob(name+'/*_fibers.fits')
        if len(fibers) < 1:
            fibers = ['NOT FOUND']
            OK = False
        elif len(fibers) == 3:
            self.is_minnie = True
            self.fiber_image_list = fibers
        elif len(fibers) > 1 and len(fibers) != 3:
            fibers = ['FOUND TOO MANY']
            OK = False
            
        par = glob(name+'/*results.par')
        if len(par) < 1:
            par = ['NOT FOUND']
            OK = False
        elif len(par) > 1:
            par = ['FOUND TOO MANY']
            OK = False

        '''Show the user what we've found'''
        self.face_image.set('\n'.join(face))
        self.fiber_image.set('\n'.join(fibers))
        self.par_file.set(par[0])

        if OK:
            if self.is_minnie:
                self.step1OK_minnie.grid(column=0,row=9,columnspan=2)
            else:
                self.step1OK.grid(column=0,row=9,columnspan=2)

        return

    def goStep2(self):
        '''Shows step 2 to the user'''
        self.Step1.pack_forget()
        self.Step2.pack()
        
        if not self.is_minnie:
            '''first we'll figure out how big the IFU is'''
            self.yanny = yanny(self.par_file.get(),np=True)
        
        if self.is_minnie:
            self.rank = 1
        else:
            nifu = int(self.yanny['nifu'])
            self.rank = int(((12 * nifu - 3)**0.5 - 3)/6.)

        '''generate good starting ds9 hexagon'''
        self.make_hex(self.rank)

        '''get the ferrule ID and load in the UWash parameters for this ID'''
        if not self.is_minnie:
            for i in range(self.yanny['BUNDLEMAP']['frlCode'].size):
                self.current_ferrule_code = self.yanny['BUNDLEMAP']['frlCode'][i]
                if self.current_ferrule_code != 0:
                    break
                i += 1
        
        if os.path.exists(os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/hexferrules.par')):
            hpar = yanny(os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/hexferrules.par'),np=True)
        else:
            hpar = yanny(os.path.expanduser('~/research/MaNGA/test_stand_software/manga/ADE_trunk/python/hexferrules.par'),np=True)           
        try:
            hidx = np.where(hpar['HEXQA']['frlcode'] == self.current_ferrule_code)[0][0]
            self.conc_x = hpar['HEXQA'][hidx]['concxerr']
            self.conc_y = hpar['HEXQA'][hidx]['concyerr']
            self.theta_rot = hpar['HEXQA'][hidx]['roterr']*tau/360
            self.conc_x_std = hpar['HEXQA'][hidx]['concxstd']
            self.conc_y_std = hpar['HEXQA'][hidx]['concystd']
            self.theta_rot_std = hpar['HEXQA'][hidx]['rotstd']*tau/360
        except IndexError:
            print "Could not find ferrule # {} in hexferrules.par. Dying.".\
                format(self.current_ferrule_code)
            self.master.quit()
            self.master.destroy()
            return 

        '''record these quantities to the yanny file'''
        self.yanny['concxerr'] = self.conc_x
        self.yanny['concyerr'] = self.conc_y
        self.yanny['roterr'] = self.theta_rot*360/tau

        self.conc_str.set('({:8.4f}, {:8.4f})'.format(self.conc_x,self.conc_y))
        self.theta_rot_str.set('{:8.4f} deg'.format(self.theta_rot*360/tau))
        self.ferrule_str.set('Loaded data from ferrule # {}'.format(self.current_ferrule_code))

        '''remove distortions from the face and fiber images and run a
        metrology solution on the fiber image'''
        if os.path.exists(os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/database')):
            database = os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/python/database')
        else:
            database = os.path.expanduser('~/research/MaNGA/test_stand_software/manga/ADE_trunk/python/database')
        trans_face_name = self.face_image.get().split('.fits')[0] + '.trans.fits'
        trans_fiber_name = self.fiber_image.get().split('.fits')[0] + '.trans.fits'
        self.map_file = trans_fiber_name.split('.trans.fits')[0] + '.map'
        self.map_file = self.red_dir.get()+'/'+os.path.basename(self.map_file)

        iraf.geotran(','.join([self.face_image.get(),self.fiber_image.get()]),
                     ','.join([trans_face_name,trans_fiber_name]),
                     database,
                     self.database_soln)

        self.find_fibers(trans_fiber_name,self.map_file)

        if not self.auto:
            '''in case the user hasn't closed the main test stand software; we
            don't want gige_loop throwing images to ds9 while we're trying to draw
            a hexagon.
            '''
            os.system('killall gige_loop')
            
            '''launch ds9. All the options make it look nicer'''
            os.system('ds9 -cd {} -colorbar no -view panner no -view info no -view magnifier no -log -cmap value 2.55 0.33 {} -orient xy -regions -system image {} &'.format(self.red_dir.get(),trans_face_name,os.path.join(self.red_dir.get(),self.hex_template)))

    def choose_reg(self):
        '''load a region file'''
        self.reg_file.set(tkFD.askopenfilename(title='Choose ds9 region file',initialdir=self.red_dir.get()))
        os.system('xpaset -p ds9 regions delete all')
        os.system('xpaset -p ds9 regions system image')
        os.system('xpaset -p ds9 regions {}'.format(self.reg_file.get()))

    def save_reg(self):
        '''save whatever region file is displayed in ds9'''
        os.system('xpaset -p ds9 regions system image')
        os.system('xpaset -p ds9 regions save {}'.format(os.path.join(self.red_dir.get(),self.hex_reg)))
        self.reg_file.set(os.path.join(self.red_dir.get(),self.hex_reg))

    def goStep3(self):
        '''show step 3 to the user. This function also reads the central fiber
        location from the map file and computes the distance between this and
        the hex center, in both px and mm.
        '''
        try:
            f = open(self.reg_file.get(),'r')
        except Exception as e:
            self.warning_text.set('Could not find region file')
            self.warning.grid()
            return
        
        '''grab the locations of the hexagon verticies'''
        try:
            coords = [float(i) for i in f.readlines()[-1].split('(')[1].split(')')[0].split(',')]
        except Exception as e:
            self.warning_text.set('Region file is funky: {}'.format(e))
            self.warning.grid()
            return
        if len(coords) != 12:
            print 'bad'
            self.warning_text.set('ds9 region is not valid')
            self.warning.grid()
            return
        
        '''the format of coords is [x1, y1, x2, y2, x3, y3....]'''
        xc = np.array([coords[::2]])
        yc = np.array([coords[1::2]])
        xdiff = xc - xc.T
        ydiff = yc - yc.T
        
        '''We assume that the user set up the points as instructed in step
        2. If they didn't then we're all fuct.
        '''
        idx = ([0,1,3,4],[1,0,4,3])
        
        self.theta_hex = np.arctan(ydiff[idx]/xdiff[idx]).mean()
        hcenter = np.array([xc.mean(), yc.mean()])
        
        self.theta_hex_str.set('{:8.4f} deg'.format(self.theta_hex*360/tau))
        self.hex_center.set('({:8.4f}, {:8.4f})'.format(*hcenter))
        
        '''read in the fiber locations and find location of central fiber'''
        print 'using mapfile: {}'.format(self.map_file)
        imx, imy = np.loadtxt(self.map_file,skiprows=1,usecols=(3,4),unpack=True)

        print "fcenter is", self.fcenter
        self.fiber_center.set('({:8.4f}, {:8.4f})'.format(*self.fcenter))
        
        '''transfrom from image to fiber coordinates'''
        fx = imx - self.fcenter[0]
        fy = imy - self.fcenter[1]

        self.compute_solution(fx,fy,self.fcenter,hcenter)
        self.compute_packing_error()
        self.median_hex_x, self.median_hex_y = self.make_perfection(self.rank,self.cart_median_pack/2.)
        
        '''make a nice plot'''
        median_xdiff, median_ydiff = self.plot_median_spacing_errors()
        self.median_hist_plots.append(self.plot_error_histogram(median_xdiff, median_ydiff, 'Packing error [mm]'))
        
        '''write a formatted wiki-string for the cartridge wiki page'''
        wf = open('{}/{}'.format(self.wiki_path,self.wiki_file),'a')
        wf.write(self.wiki_string)
        wf.close()

        if len(self.minnie_ferrules) == 0:
            self.nextMinnieButton.grid_remove()
            self.writeOutButton.grid(column=0,row=11,columnspan=2)

        self.Step2.pack_forget()
        self.Step3.pack()

    def compute_solution(self,fx,fy,fcenter,hcenter):
        '''The function actually computes the transform from fiber coordinates
        (relative to the central fiber) to ferrule coordinates (relative to
        the center of the ferrule. It transforms to the hex frame in between.
        '''

        '''now match the positions to the correct fiber numbers'''
        ferrule_idx = np.where(self.yanny['BUNDLEMAP']['frlCode'] == self.current_ferrule_code)
        bundlemap = self.yanny['BUNDLEMAP'][ferrule_idx]

        fxmm = self.kx * (np.cos(self.alpha)*fx - np.sin(self.alpha)*fy)
        fymm = self.ky * (np.sin(self.alpha)*fx + np.cos(self.alpha)*fy)

        sort_fx = np.array([])
        sort_fy = np.array([])
        for fiber in bundlemap:
            '''The -1 here is very important because it gets us out of the
            motor coordinate frame that the results.par file is in'''
            refxmm = -1*fiber['xpmm']
            refymm = fiber['ypmm']
            refrmm = ((fxmm - refxmm)**2 + (fymm - refymm)**2)**0.5
            ridx = np.where(np.abs(refrmm) == np.min(np.abs(refrmm)))
            sort_fx = np.append(sort_fx,fx[ridx][0])
            sort_fy = np.append(sort_fy,fy[ridx][0])

        '''first, transform from fiber coordinates (in px) to hex coordinates
        (in mm)'''
        '''shift'''
        xoff, yoff = fcenter - hcenter
        self.px_diff.set('({:8.4f}, {:8.4f})'.format(xoff,yoff))
        hxpx = sort_fx - xoff
        hypx = sort_fy - xoff

        tmp_x_std = (self.fiber_find_std**2 + self.hex_x_std**2)**0.5
        tmp_y_std = (self.fiber_find_std**2 + self.hex_y_std**2)**0.5
        
        '''now to mm'''
        hxmm = self.kx * (np.cos(self.alpha)*hxpx - np.sin(self.alpha)*hypx)
        hymm = self.ky * (np.sin(self.alpha)*hxpx + np.cos(self.alpha)*hypx)

        '''now rotate from detector frame to hex frame'''
        self.hx = hxmm*np.cos(-self.theta_hex) - hymm*np.sin(-self.theta_hex)
        self.hy = hxmm*np.sin(-self.theta_hex) + hymm*np.cos(-self.theta_hex)

        tmp_x_std = ((np.cos(-self.theta_hex)*tmp_x_std)**2 +
                     (np.sin(-self.theta_hex)*tmp_y_std)**2 +
                     ((-hxmm*np.sin(self.theta_hex) + hymm*np.cos(self.theta_hex))*self.hex_rot_std)**2)**0.5

        tmp_y_std = ((np.sin(-self.theta_hex)*tmp_x_std)**2 +
                     (np.cos(-self.theta_hex)*tmp_y_std)**2 +
                     ((-hxmm*np.cos(self.theta_hex) - hymm*np.sin(self.theta_hex))*self.hex_rot_std)**2)**0.5

        '''Now shift origin to ferrule center. Yes this is correct, the
        coordinate system that UWash used is rather weird'''
        sx = self.hx + self.conc_y
        sy = self.hy - self.conc_x

        tmp_x_std = (tmp_x_std**2 + self.conc_y_std**2)**0.5
        tmp_y_std = (tmp_y_std**2 + self.conc_x_std**2)**0.5

        '''finally rotate to the pin-ferrule axis'''
        xpmm = sx*np.cos(self.theta_rot) - sy*np.sin(self.theta_rot)
        ypmm = sx*np.sin(self.theta_rot) + sy*np.cos(self.theta_rot)

        '''compute the uncertainty'''
        self.total_x_std = np.median(((np.cos(self.theta_rot)*tmp_x_std)**2 +
                                      (np.sin(self.theta_rot)*tmp_y_std)**2 +
                                      ((-sx*np.sin(self.theta_rot) - sx*np.cos(self.theta_rot))*self.theta_rot_std)**2)**0.5)
        self.total_y_std = np.median(((np.sin(self.theta_rot)*tmp_x_std)**2 +
                                      (np.cos(self.theta_rot)*tmp_y_std)**2 +
                                      ((sx*np.cos(self.theta_rot) - sx*np.sin(self.theta_rot))*self.theta_rot_std)**2)**0.5)
        

        '''and update the par file'''
        ferrule_idx = np.where(self.yanny['BUNDLEMAP']['frlCode'] == self.current_ferrule_code)
        self.yanny['BUNDLEMAP']['xpmm'][ferrule_idx] = xpmm
        self.yanny['BUNDLEMAP']['ypmm'][ferrule_idx] = ypmm


    def plot_median_spacing_errors(self):
        '''Yet another plotting function. This one compares the measured fiber
        locations to a perfect hexagon that has a median spacing equal to the
        measured median spacing. These distances are computed and plotted in a
        hex-centered frame (but still rotated)'''

        ferrule_idx = np.where(self.yanny['BUNDLEMAP']['frlCode'] == self.current_ferrule_code)
        bundlemap = self.yanny['BUNDLEMAP'][ferrule_idx]

        rxmm, rymm, pxmm, pymm = self.minimize_offrot(self.hx, 
                                                      self.hy, 
                                                      self.median_hex_x, 
                                                      self.median_hex_y)

        xdiff = rxmm - pxmm
        ydiff = rymm - pymm
        rdiff = (xdiff**2 + ydiff**2)**0.5

        cdf = np.cumsum(rdiff/rdiff.sum())
        diff90 = np.interp(0.9,cdf,rdiff)
        median_diff, median_diff_std = median_bootstrap(rdiff*1000.)

        median_pack = self.median_pack * 1000.
        total_xoff = (self.fiber_hex_shift_x - self.conc_y) * 1000.
        total_yoff = (self.fiber_hex_shift_y + self.conc_x) * 1000.
        total_rot = (self.fiber_hex_rot + self.theta_rot) * 360/tau

        median_pack_std = self.median_pack_std * 1000.
        total_xoff_std = (self.total_x_std**2 + self.fiber_hex_shift_x_std**2)**0.5 * 1000.
        total_yoff_std = (self.total_y_std**2 + self.fiber_hex_shift_y_std**2)**0.5 * 1000.
        total_rot_std = 360/tau*(self.hex_rot_std**2 + self.theta_rot_std**2 + self.fiber_hex_rot_std**2)**0.5
        fiber_hex_rot_std = self.fiber_hex_rot_std * 360 / tau

        '''find sig figs'''
        xsigfig = np.abs(int(np.floor(np.log10(total_xoff_std))))
        ysigfig = np.abs(int(np.floor(np.log10(total_yoff_std))))
        rotsigfig = np.abs(int(np.floor(np.log10(total_rot_std))))
        mediansigfig = np.abs(int(np.floor(np.log10(median_diff_std))))
        packsigfig = np.abs(int(np.floor(np.log10(median_pack_std))))
        fibhexsigfig = np.abs(int(np.floor(np.log10(fiber_hex_rot_std))))
        cartpacksigfig = np.abs(int(np.floor(np.log10(self.cart_median_pack_std*1000))))
        xfmt = '{{:1.{}f}}'.format(xsigfig)
        yfmt = '{{:1.{}f}}'.format(ysigfig)
        rotfmt = '{{:1.{}f}}'.format(rotsigfig)
        medianfmt = '{{:1.{}f}}'.format(mediansigfig)
        packfmt = '{{:1.{}f}}'.format(packsigfig)
        fibhexfmt = '{{:1.{}f}}'.format(fibhexsigfig)
        cartfmt = '{{:1.{}f}}'.format(cartpacksigfig)
        
        print np.array([self.fiber_hex_shift_x,self.fiber_hex_shift_y,self.conc_y, self.conc_x])
        print np.array([self.total_x_std, self.fiber_hex_shift_x_std]) * 1000.
        print np.array([self.total_y_std, self.fiber_hex_shift_y_std]) * 1000.        
        print 360/tau*np.array([self.hex_rot_std, self.theta_rot_std, self.fiber_hex_rot_std])
        print total_rot, total_rot_std, rotsigfig, rotfmt

        fig = plt.figure()
        ax = fig.add_subplot('111',aspect='equal')
        ax.set_xlabel('X distance from measured hex center [mm]')
        ax.set_ylabel('Y distance from measured hex center [mm]')        
        ax.axvline(x=0,ls=':',alpha=0.3)
        ax.axhline(y=0,ls=':',alpha=0.3)

        theta = np.arange(0,tau,0.01)
        for (mx,my,px,py,fiber) in zip(rxmm,rymm,pxmm,pymm,bundlemap):
            ax.plot(0.075*np.cos(theta)+px,0.075*np.sin(theta)+py,color='r',alpha=0.7)
            ax.plot(0.075*np.cos(theta)+mx, 0.075*np.sin(theta)+my,'k')
            ax.plot(0.06*np.cos(theta)+mx, 0.06*np.sin(theta)+my,'k')
            ax.text(px,py,fiber['fnum'],va='center',ha='center',fontdict={'size':10,'color':'red'})
            ax.text(mx,my,fiber['fnum'],va='center',ha='center',fontdict={'size':10})

        ax.quiver(pxmm,pymm,xdiff,ydiff,angles='xy',scale_units='xy',scale=1/10.,width=0.005)

        if self.is_minnie:
            ax.set_title('Harness {}, face {}, cartridge {}, Packing errors\nGenerated on {}\n'.format(
                    self.par_file.get().split('-')[0].split('/')[-1],self.current_facenum,self.cart_num.get(),time.asctime()),
                         fontdict={'fontsize':10})
        else:
            ax.set_title('Harness {}, cartridge {}, Packing errors\nGenerated on {}\n'.format(
                    self.par_file.get().split('-')[0].split('/')[-1],self.cart_num.get(),time.asctime()),
                         fontdict={'fontsize':10})
        ax.text(0.05,0.95,'Arrow magnitudes are enhanced by 10',fontdict={'size':8},transform=ax.transAxes)
        ax.text(1.01,0.9,('Median packing\nerror:\n {0} +/- {0} $\mu$m'.\
                              format(medianfmt)).format(median_diff,median_diff_std),
                fontdict={'fontsize':8},transform=ax.transAxes)
        
        ax.text(1.01,0.75,
                ('IFU offset from\nferrule-pin center:\n ({0}, {1}) +/- ({0}, {1}) $\mu$m'.format(xfmt,yfmt)).\
                    format(total_xoff,total_yoff,total_xoff_std,total_yoff_std),
                fontdict={'fontsize':8},transform=ax.transAxes)
        ax.text(1.01,0.65,
                ('IFU rotation from\nferrule-pin axis:\n {0} +/- {0} deg'.format(rotfmt)).\
                    format(total_rot,total_rot_std),
                fontdict={'fontsize':8},transform=ax.transAxes)
        ax.text(1.01,0.55,
                ('IFU rotation within\nhex channel:\n {0} +/- {0} deg'.format(fibhexfmt)).\
                    format(self.fiber_hex_rot*360/tau,fiber_hex_rot_std),
                fontdict={'fontsize':8},transform=ax.transAxes)
        ax.text(1.01,0.45,
                ('Harness median fiber\nspacing:\n {0} +/- {0} $\mu$m'.format(packfmt)).format(median_pack,median_pack_std),
                fontdict={'fontsize':8},transform=ax.transAxes)
        ax.text(1.01,0.35,
                ('Cartridge median fiber\nspacing:\n {0} +/- {0} $\mu$m'.format(cartfmt)).format(self.cart_median_pack*1000.,self.cart_median_pack_std*1000.),
                fontdict={'fontsize':8},transform=ax.transAxes)
        ax.text(-0.3,0.5,'Pin on\nthis side',transform=ax.transAxes,fontdict={'fontsize':12})
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        lim = max([xlim[1],ylim[1]])
        ax.set_xlim(-lim,lim)
        ax.set_ylim(-lim,lim)
        
        canvas = FigureCanvasTkAgg(fig, master=self.Step3)
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=10,columnspan=2)
        
        self.median_hex_plot.append(fig)

        '''save png's for the wiki. I hate that this is here rather than
        write_out(), but minnie bundles suck'''
        harness = self.par_file.get().split('-')[0].split('/')[-1]
        pngdir = '{}/{}'.format(self.data_path,os.path.basename(self.red_dir.get()))
        if self.is_minnie:
            png_hex_name = '{}/{}_{}_map.png'.format(pngdir,harness,self.current_facenum)
            self.wiki_string += '||Mini Bundle-{} || ma{} || '.format(self.current_facenum,harness[-3:])
        else:
            png_hex_name = '{}/{}_map.png'.format(pngdir,harness)
            self.wiki_string += '||{} fiber IFU || ma{} || '.format(self.yanny['nifu'],harness[-3:])

        '''if the save location doesn't exist yet it's because we're not ready
        to save the pngs. So don't do it'''
        if os.path.exists(pngdir):
            fig.savefig(png_hex_name,dpi=100)
        else:
            print 'Directory {} does not exist, so no image was saved'.format(pngdir)


        '''Start building the wiki formatted string'''
        total_xoff_str = ('{}'.format(xfmt)).format(total_xoff)
        total_yoff_str = ('{}'.format(yfmt)).format(total_yoff)
        total_rot_str = ('{}'.format(rotfmt)).format(total_rot)
        median_diff_str = ('{}'.format(medianfmt)).format(median_diff)
        harness_median_str = ('{}'.format(medianfmt)).format(median_pack)
        cart_median_str = ('{}'.format(cartfmt)).format(self.cart_median_pack*1000)

        self.wiki_string += '{}, {}, {} || {} || {} || [[Image(source:data/manga/teststand/trunk/UWisc/{}, 200px)]] '.\
            format(total_xoff_str, 
                   total_yoff_str, 
                   total_rot_str,
                   harness_median_str,
                   median_diff_str,
                   '/'.join(png_hex_name.split('/')[-2:]))
        
        return xdiff, ydiff

    def minimize_offrot(self, xpmm, ypmm, ideal_x, ideal_y):
        '''Given fiber measurements and an ideal hexagon, determine the offset
        and rotation needed to minimize the sum of the errors between measured
        and ideal fiber locations'''

        '''The fitting function depends on (x,y) shift and rotation. We'll
        start with 0's for all'''
        p0 = [0.0,0.0,0.0]

        soln, cov, _, _, _ = spo.leastsq(self.rot_func, p0, args=(xpmm,ypmm,ideal_x,ideal_y),full_output=True)
        print soln, cov
        xshift, yshift, rot_angle = soln
        print 'fit between IFU and hex is ({}, {}) and {} deg'.format(xshift,yshift,rot_angle*360./tau)
        self.fiber_hex_shift_x = xshift
        self.fiber_hex_shift_y = yshift
        self.fiber_hex_rot = rot_angle

        '''rotate and shift the fibers'''
        rot_x = (xpmm-xshift)*np.cos(rot_angle) - (ypmm-yshift)*np.sin(rot_angle)
        rot_y = (xpmm-xshift)*np.sin(rot_angle) + (ypmm-yshift)*np.cos(rot_angle)

        '''Match the rotated fibers with their perfect pairs'''
        pxmm = np.array([])
        pymm = np.array([])
        
        for (fx, fy) in zip(rot_x, rot_y):
            dist_from_fiber = ((fx - ideal_x)**2 + (fy - ideal_y)**2)**0.5
            ridx = np.where(np.abs(dist_from_fiber) == np.min(np.abs(dist_from_fiber)))
            px = ideal_x[ridx][0]
            py = ideal_y[ridx][0]
            pxmm = np.append(pxmm,px)
            pymm = np.append(pymm,py)

        residual = np.sum(((rot_x - pxmm)**2 + (rot_y - pymm)**2)**0.5)/(rot_x.size - 3)
        cov *= residual

        self.fiber_hex_shift_x_std = cov[0,0]
        self.fiber_hex_shift_y_std = cov[1,1]
        self.fiber_hex_rot_std = cov[2,2]

        return rot_x, rot_y, pxmm, pymm

    def rot_func(self, p, xpmm, ypmm, ideal_x, ideal_y):
        '''given a shift and rotation, compute the sum of the differences
        between shifted and rotated fibers and the ideal fiber locations.
        '''

        xdiff = np.array([])
        ydiff = np.array([])
        
        rot_x = (xpmm-p[0])*np.cos(p[2]) - (ypmm-p[1])*np.sin(p[2])
        rot_y = (xpmm-p[0])*np.sin(p[2]) + (ypmm-p[1])*np.cos(p[2])

        for (fx, fy) in zip(rot_x, rot_y):
            dist_from_fiber = ((fx - ideal_x)**2 + (fy - ideal_y)**2)**0.5
            ridx = np.where(np.abs(dist_from_fiber) == np.min(np.abs(dist_from_fiber)))
            px = ideal_x[ridx][0]
            py = ideal_y[ridx][0]
            xdiff = np.append(xdiff, fx - px)
            ydiff = np.append(ydiff, fy - py)

        return (xdiff**2 + ydiff**2)**0.5

    def compute_packing_error(self):
        '''This function computes the the median spacing between all of the
        fibers and plots a histogram of the spacings. It also populates the
        yanny with some more header info.
        '''

        ferrule_idx = np.where(self.yanny['BUNDLEMAP']['frlCode'] == self.current_ferrule_code)
        bundlemap = self.yanny['BUNDLEMAP'][ferrule_idx]

        xpmm = bundlemap['xpmm']
        ypmm = bundlemap['ypmm']
        
        xdiff = np.array([xpmm]).T - np.array([xpmm])
        ydiff = np.array([ypmm]).T - np.array([ypmm])
        
        rdiff = ((xdiff**2 + ydiff**2)**0.5)[np.triu_indices(xpmm.size,1)]
        
        close_idx = np.where(rdiff < 0.145*3**0.5)
        good_rdiff = rdiff[close_idx]
        good_rdiff.sort()
        
        self.median_pack, self.median_pack_std = median_bootstrap(good_rdiff)
        std_pack = np.std(good_rdiff)

        self.yanny['medianPack'] = self.median_pack
        self.yanny['stdPack'] = self.median_pack_std

        # n, binedge = np.histogram(good_rdiff,good_rdiff.size)
        # bins = 0.5*(binedge[1:] + binedge[:-1])
        # cdf = np.cumsum(n*1.0/np.sum(n))
                
        # ax = plt.figure().add_subplot(111)
        # ax.set_xlabel('Fiber center-to-center distance [mm]')
        # ax.set_ylabel('num')
        # if self.is_minnie:
        #     ax.set_title('Harness {} face {}. Generated on {}\n'.format(
        #             self.par_file.get().split('-')[0].split('/')[-1],self.current_facenum,time.asctime()))
        # else:
        #     ax.set_title('Harness {}. Generated on {}\n'.format(
        #             self.par_file.get().split('-')[0].split('/')[-1],time.asctime()))
        # ax.plot(bins,cdf)    
        # ax.text(0.65,0.85,'median separation: {:7.4f} mm'.format(self.median_pack),transform=ax.transAxes,fontdict={'size':8})
        # self.err_plots.append(ax.get_figure())

        # canvas = FigureCanvasTkAgg(ax.get_figure(), master=self.Step3)
        # canvas.show()
        # canvas.get_tk_widget().grid(column=0,row=10,columnspan=2)

    def plot_error_histogram(self,xdiff,ydiff,x_label):
        '''Generates a histogram of radial errors. These are computed based on
        the x and y errors of the fibers and you can label the x axis whatever
        you want. This function returns the resulting figure for future
        use.'''

        rdiff = (xdiff**2 + ydiff**2)**0.5
        rdiff.sort()

        median_sep, median_sep_std = median_bootstrap(rdiff*1000.)

        medsigfig = np.abs(int(np.floor(np.log10(median_sep_std))))
        medfmt = '{{:1.{}f}}'.format(medsigfig)

        n, binedge = np.histogram(rdiff,rdiff.size)
        bins = 0.5*(binedge[1:] + binedge[:-1])

        cdf = np.cumsum(n*1.0/np.sum(n))
        diff50 = np.interp(0.50,cdf,bins)
        diff90 = np.interp(0.90,cdf,bins)
        diff67 = np.interp(0.67,cdf,bins)
        
        ax = plt.figure().add_subplot(111)
        ax.set_xlabel(x_label)
        ax.set_ylabel('Normalized Cumulative Distribution')
        
        if self.is_minnie:
            ax.set_title('Harness {}, face {}, cartridge {}\nGenerated on {}'.format(
                    self.par_file.get().split('-')[0].split('/')[-1],self.current_facenum,self.cart_num.get(),time.asctime()))
        else:
            ax.set_title('Harness {}, cartridge {}\nGenerated on {}'.format(
                    self.par_file.get().split('-')[0].split('/')[-1],self.cart_num.get(),time.asctime()))
        ax.plot(bins,cdf,'k')
        ax.axvline(x=diff50, color='r', ls=':')
        ax.axvline(x=diff90, color='g', ls=':')
        ax.axvline(x=diff67, color='b', ls=':')
        ax.text(0.65,0.75,('50% have error <= {0} +/- {0} $\mu$m'.format(medfmt)).\
                    format(diff50*1000.,median_sep_std),
                transform=ax.transAxes,fontdict={'size':8},color='r')
        ax.text(0.65,0.70,('67% have error <= {0} +/- {0} $\mu$m'.format(medfmt)).\
                    format(diff67*1000.,median_sep_std),
                transform=ax.transAxes,fontdict={'size':8},color='b')
        ax.text(0.65,0.65,('90% have error <= {0} +/- {0} $\mu$m'.format(medfmt)).\
                    format(diff90*1000.,median_sep_std),
                transform=ax.transAxes,fontdict={'size':8},color='g')

        # canvas = FigureCanvasTkAgg(ax.get_figure(), master=self.Step3)
        # canvas.show()
        # canvas.get_tk_widget().grid(column=0,row=10,columnspan=2)
        
        harness = self.par_file.get().split('-')[0].split('/')[-1]
        pngdir = '{}/{}'.format(self.data_path,os.path.basename(self.red_dir.get()))
        if self.is_minnie:
            png_hist_name = '{}/{}_{}_hist.png'.format(pngdir,harness,self.current_facenum)
        else:
            png_hist_name = '{}/{}_hist.png'.format(pngdir,harness)
            
        '''if the save location doesn't exist yet it's because we're not ready
        to save the pngs. So don't do it'''
        if os.path.exists(pngdir):
            ax.get_figure().savefig(png_hist_name,dpi=100)
        else:
            print 'Directory {} does not exist, so no image was saved'.format(pngdir)        
        
        self.wiki_string += '|| [[Image(source:data/manga/teststand/trunk/UWisc/{}, 200px)]] ||\n'.format('/'.join(png_hist_name.split('/')[-2:]))

        return ax.get_figure()

    def write_out(self):
        '''This function creates a new yanny .par file with the updated
        metrology information. In keeping with MaNGA conventions the new file
        has its file name incremented by 1.
        '''

        mjd = int(time.time()/86400.0 + 40587.0)
        harness = self.par_file.get().split('-')[0]
        outputfile = '{}-{}-1.par'.format(harness,mjd)
        self.yanny['mjd'] = mjd

        parfile = self.par_file.get()

        pf = open(parfile,'r')

        '''increment the name by 1'''
        if os.path.exists('{}/{}'.format(self.red_dir.get(),outputfile)):
            outputfile = '-'.join(outputfile.split('-')[:-1] + ['{}.par'.format(outputfile.split('-')[-1]+1)])
        median_plot_name = outputfile.split('.par')[0] + '_median_hex.pdf'
        median_hist_name = outputfile.split('.par')[0] + '_median_hist.pdf'

        '''copy header information to the new file'''
        lines = pf.readlines()
        of = open(outputfile,'w')
        for line in lines:
            if '# File generated on' in line:
                of.write('# File generated on {}\n'.format(time.asctime()))
            elif '# 9) xpmm:' in line:
                of.write('# 9) xpmm: The x coordinate of the fiber relative to the ferrule centroid (sky-view frame: +x=E,+y=N)\n')
            elif '# 10) ypmm:' in line:
                of.write('# 10) ypmm: The y coordinate of the fiber relative to the ferrule centroid (sky-view frame: +x=E,+y=N)\n')
            elif line[0] == '#':
                of.write(line)
            if '# validMetro' in line:
                '''Write info about the new header stuff'''
                of.write('# medianPak: Median fiber center-to-center distance [mm]\n'
                         +'# stdPak: Standard deviation of the center-center distances [mm]\n'
                         +'# conc[x|y]err: Distance between hex center and ferrule center [mm]. Taken from hexferrules.par\n'
                         +'# roterr: Angle between hex and ferrule center-pin axis [deg]. Taken from hexferrules.par\n')
        of.write('\n')
        
        '''first reverse the order of the yanny file to put it in slit order
        or something'''
        self.yanny['BUNDLEMAP'] = self.yanny['BUNDLEMAP'][::-1]
        '''write the data'''
        self.yanny.write(of)
        of.close()

        '''move the resulting file to the SVN controlled metrology directory'''
        metrodir = '{}/{}'.format(self.metro_trunk,os.path.basename(self.par_file.get())[0:5].lower())
        if not os.path.exists(metrodir):
            print "making {}".format(metrodir)
            os.system('mkdir {}'.format(metrodir))
        print "cp {} {}".format(outputfile,metrodir)
        os.system('cp {} {}'.format(outputfile,metrodir))

        '''and save the plots'''
        mpp = PDF(median_plot_name)
        for mplot in self.median_hex_plot:
            mpp.savefig(mplot)
        mpp.close()
                                    
        mhpp = PDF(median_hist_name)
        for mhplot in self.median_hist_plots:
            mhpp.savefig(mhplot)
        mhpp.close()

        # '''write a formatted wiki-string for the cartridge wiki page'''
        # wf = open('{}/{}'.format(self.wiki_path,self.wiki_file),'a')
        # wf.write('{}: || {}, {}, {} || {} || {} || [[Image({}, 200px)]] || [[Image({}, 200px)]] ||'.\
        #              format(harness.split('/')[-1],
        #                     self.total_xoff_str,
        #                     self.total_yoff_str,
        #                     self.total_rot_str,
        #                     self.median_diff_str,
        #                     self.cart_median_str,
        #                     self.png_hex_name.split('/')[-1],
        #                     self.png_hist_name.split('/')[-1]))
        # wf.close()

        '''close down everything'''
        os.system('killall ds9')
        self.master.quit()
        self.master.destroy()

    def setup_minnie(self):
        '''This function handles running metrology for 3-ifu minnie
        bundles. It matches each ferrule code the correct IFU face image and
        then runs steps 2 and 3 for each IFU.
        '''
        self.yanny = yanny(self.par_file.get(),np=True)

        '''get all the ferrule ID's, we ignore the first one because it is
        zero (sky fibers)'''
        self.minnie_ferrules = list(np.unique(self.yanny['BUNDLEMAP']['frlCode'])[1:])

        base_face = self.red_dir.get()+'/'+os.path.basename(self.face_list[0]).split('_')[0]
        for ferrule in self.minnie_ferrules:
            '''find out which image to use. There is probably a more elegant
            way to do this'''
            idx = np.where(self.yanny['BUNDLEMAP']['frlCode'] == ferrule)
            fiber = self.yanny['BUNDLEMAP']['fnum'][idx][0]
            if fiber < 8:
                facenum = 1
            elif 8 <= fiber < 15:
                facenum = 2
            elif fiber >= 15:
                facenum = 3
            
            self.face_ferrule[ferrule] = ['{}_IFU{}_face.fits'.format(base_face,facenum), facenum]
            self.fiber_ferrule[ferrule]= ['{}_IFU{}_fibers.fits'.format(base_face,facenum), facenum]
            
        self.writeOutButton.grid_forget()
        self.nextMinnieButton.grid(column=0,row=11,columnspan=2)
        self.next_minnie()

    def next_minnie(self):
        '''this function runs metrology on the next IFU face in a minnie
        bundle
        '''

        ferrule = self.minnie_ferrules.pop()
        print ferrule, self.minnie_ferrules
        self.face_image.set(self.face_ferrule[ferrule][0])
        self.fiber_image.set(self.fiber_ferrule[ferrule][0])
        self.current_ferrule_code = ferrule
        self.current_facenum = self.face_ferrule[ferrule][1]
        self.hex_reg = 'hex{}.reg'.format(self.current_facenum)
        self.reg_file.set('')
        self.wiki_string = ''
        self.Step3.pack_forget()
        self.goStep2()

    def make_hex(self,rank):
        '''This function generates a perfect hexagon region file. It
        approximates the size to be close to the size of the ferrule in
        question, but purposefully get's it wrong to ensure the user has to
        actually think about what they're doing.
        '''

        scale =  rank/1.3

        x0 = 810. - 90.*scale*np.cos(tau/6.)
        y0 = 587. - 90.*scale*np.sin(tau/6.)
        hexlist = [x0,y0]
        for i in range(6):
        
            xi = x0 + 90*scale*np.cos(tau/6. * i)
            yi = y0 + 90*scale*np.sin(tau/6. * i)
            hexlist += [xi, yi]
            x0 = xi
            y0 = yi

        f = open(os.path.join(self.red_dir.get(),self.hex_template),'w')
        f.write('# Region file format: DS9 version 4.1\n')
        f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
        f.write('image\n')
        f.write('polygon({},{},{},{},{},{},{},{},{},{},{},{})\n'.format(*hexlist))
        f.close()

    def make_perfection(self, rank, radius=0.075):
        '''The function generates the X and Y centers of a bundle that is
        perfect in every way. In other words, the central fiber is perfectly
        centered in the hex, which is perfectly centered in the ferrule, and
        the fibers are all packed perfectly.  '''

        xlist = np.array([],dtype=np.float64)
        ylist = np.array([],dtype=np.float64)

        offset = (rank % 2)*0.5
        row_lengths = np.concatenate((
                np.arange(rank) + rank + 1,
                [rank*2 + 1],
                (np.arange(rank) + rank + 1)[::-1]))
        y_offsets = radius*(np.arange(rank*2 + 1)*1.732 - rank*1.732)
        
        for (row,y) in zip(row_lengths,y_offsets):
            for x in (np.arange(row) - row/2 + offset)*2*radius:
                xlist = np.append(xlist,x)
                ylist = np.append(ylist,y)
            offset = abs(offset - 0.5)
        
        return xlist, ylist        

    def find_fibers(self,input_image, output):
    
        f = md.proc_manga_sobel(input_image)
        f = md.proc_manga_hough(f,22)   # jwp changed this from 21 to 22
        f = md.proc_manga_blobs(f)
        spikef = md.proc_manga_spikes(f)
        print "jwp-----------> spikef", spikef
        aop, flist = md.proc_manga_ifu(spikef)
        print "jwp-----------> aop", aop, "flist", flist

        self.fcenter = np.array([flist[0][4], flist[0][5]])
        print 'Center fiber {} is at {}, {}'.format(flist[0][1], flist[0][4], flist[0][5])

        spike_x, spike_y = np.loadtxt(spikef,usecols=(3,4),skiprows=1,unpack=True)
        if spike_x.size != 1 + 3*self.rank + 3*self.rank**2:
            print 'Adding fibers to spike file...'
            '''This is probably a very sloppy way to do this'''
            for item in flist:
                ix, pos, modelx, modely, imagex, imagey, ex, ey, er = item
                if er == 0.0:
                    print '\tadded fiber at {} {}'.format(imagex,imagey)
                    spike_x = np.append(spike_x, imagex)
                    spike_y = np.append(spike_y, imagey)

        f = open(output,'wb')
        f.write("{}: fibers: {:>3s} {:>8s} {:>8s}\n".format(
                'geotran_metrology', "ix", "image-x", "image-y"))
        for ix, fiber in enumerate(zip(spike_x, spike_y)):
            imagex, imagey = fiber
            f.write("{}: fibers: {:3d} {:8.3f} {:8.3f}\n".format(
                    'geotran_metrology', ix, imagex, imagey))
            
        f.close()
        os.system('rm *.sobel*')
        
def generate_grid(input_image, xcent, ycent):
    '''generate a grid of perfect dot positions'''

    data = pyfits.open(input_image)[0].data
    dims = data.shape

    distance = 0.125/0.00285 #spacing of dots in px
    
    xvec = np.arange(0,dims[1],distance)
    yvec = np.arange(0,dims[0],distance)

    print xvec.size, yvec.size

    xidx = np.argmin(np.abs(xvec - xcent))
    yidx = np.argmin(np.abs(yvec - ycent))

    xoff = xvec[xidx] - xcent
    yoff = yvec[yidx] - ycent

    xvec -= xoff
    yvec -= yoff

    f = open('out.reg','w')
    f.write('# Region file format: DS9 version 4.1\nglobal color=red dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\nphysical\n')
    for x in xvec:
        for y in yvec:
            f.write('circle({}, {}, 12)\n'.format(x,y))

    f.close()

    xx, yy = np.meshgrid(xvec,yvec)

    return xx.flatten(), yy.flatten()

def make_iraf_input(data_region, modelx, modely,output='iraf.in',center=[779.889,622.841]):
    '''Make a file with columns modelx, modely, datax, datay. Ripe for use
    with IRAF's geomap.
    '''

    modelr = ((modelx - center[0])**2 + (modely - center[1])**2)**0.5

    f = open(data_region,'r')
    lines = f.readlines()
    
    datax = np.array([])
    datay = np.array([])

    reg = open('model.reg','w')
    reg.write('# Region file format: DS9 version 4.1\nglobal color=red dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\nimage\n')

    reg2 = open('fibers.reg','w')
    reg2.write('# Region file format: DS9 version 4.1\nglobal color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\nimage\n')


    iraf = open(output,'w')

    for line in lines:
        if 'circle(' in line:
            dx = float(line.split(',')[0].split('(')[1])
            dy = float(line.split(',')[1])
            datax = np.append(datax, dx)
            datay = np.append(datay, dy)
            datar = ((dx - center[0])**2 + (dy - center[1])**2)**0.5
            
            ridx = np.where(modelr <= datar)
            xidx = np.where(np.abs(modelx - dx) == np.min(np.abs(modelx - dx)))
            yidx = np.where(np.abs(modely[xidx] - dy) == np.min(np.abs(modely[xidx] - dy)))
            print yidx

            mx = modelx[xidx][yidx][0]
            my = modely[xidx][yidx][0]

            print mx, my

            reg.write('circle({}, {}, 12)\n'.format(mx, my))
            reg2.write('circle({}, {}, 12)\n'.format(dx,dy))
            iraf.write('{:15.8f} {:15.8f} {:15.8f} {:15.8f}\n'.format(mx, my, dx, dy))

    reg.close()
    reg2.close()
    iraf.close()
    f.close()

def median_bootstrap(data, N=1000):
    '''Compute the median and use bootstrapping to estimate the error. Return
    both.
    '''
    
    result_arr = np.array([])
    for i in range(N):
        idx = np.random.randint(0,data.size,data.size)
        result_arr = np.append(result_arr, np.median(data[idx]))

    return np.mean(result_arr), np.std(result_arr)

def plot_fibers(x,y,radius=0.075):
    
    fig = plt.figure()
    ax = fig.add_subplot(111,aspect='equal')
    ax.axvline(x=0,ls=':',alpha=0.3)
    ax.axhline(y=0,ls=':',alpha=0.3)
    ax.set_xlabel('X distance from ferrule center [mm]')
    ax.set_ylabel('Y distance from ferrule center [mm]')
    theta = np.arange(0,tau,0.01)
    
    for (xpmm,ypmm) in zip(x,y):
        ax.plot(radius*np.cos(theta)+xpmm, radius*np.sin(theta)+ypmm,'k')
        ax.plot(radius*0.8*np.cos(theta)+xpmm, radius*0.8*np.sin(theta)+ypmm,'k')
        ax.text(1.02,0.5,'E',ha='center',va='center',transform=ax.transAxes)
        ax.text(0.5,1.02,'N',ha='center',va='center',transform=ax.transAxes)

    fig.show()

def main():
    '''This program is intended to be run as stand alone from the rest of the
    test stand software'''
    if os.getlogin() == 'SDSSfiber':
        root = tk.Tk()
    else:
        root = tk.Toplevel()
    
    if len(sys.argv) == 2:
        '''this is a big assumption'''
        Metro = MetrologyWindow(root,sys.argv[1])
    elif len(sys.argv) > 2:
        print "The request was made but it was not good"
        return
    else:
        Metro = MetrologyWindow(root)
    root.mainloop()

if __name__ == '__main__':
    sys.exit(main())
