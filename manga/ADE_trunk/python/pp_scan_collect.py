#! /usr/bin/env python
# file: $RCSfile$
# rcsid: $Id$
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# Collects data for the plug plate metrology
# ********************************************************************

import inspect
import sys

import manga_devices as md

tag = inspect.stack()[0][3] # our name

print "{}: sys.argc: {}".format(tag, len(sys.argv))
print "{}: sys.argv: {}".format(tag, sys.argv)

md.proc_pp_fiber_acquire(1)

for ix in range(1, len(sys.argv)):
    print "{}: ix {} argv {}".format(tag, ix, sys.argv[ix])
    fiber = int(sys.argv[ix])
    print "{}: move to plug plate fiber {}".format(tag, fiber)
    md.proc_pp_fiber_acquire(fiber)
    md.proc_gige_loop_save_image()
    sys.stdout.flush()

md.proc_pp_fiber_acquire(1)
