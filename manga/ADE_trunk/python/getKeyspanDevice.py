"""Find Keyspan USB-serial adapter devices
"""
import glob

def getKeyspanDevice(portNum):
    """Return the path to a USB Keyspan USB-serial device given its port number
    """
    portNum = int(portNum)
    searchStr = "/dev/cu.USA*%d.%d" % (portNum, portNum)
    devList = glob.glob(searchStr)
    if len(devList) != 1:
        if not devList:
            raise RuntimeError("No Keyspan USB device found for port %d using search string %r" % (portNum, searchStr))
        raise RuntimeError("Found %d devices matching search string %r" % (searchStr))
    return devList[0]
