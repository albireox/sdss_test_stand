#! /usr/bin/env python

import numpy as np
import glob
import os
import sys
import time
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from matplotlib.collections import PatchCollection
from yanny import yanny
glob = glob.glob
tau = np.pi*2.

def main():

    if len(sys.argv) == 1:
        return "The request was made, but it was not good"
    
    for harness in sys.argv[1:]:
        do_harness(harness)

def do_harness(harness):
    
    print "Running harness {}".format(harness)

    basepath = '/usr/users/eigenbrot/research/MaNGA/data_trunk'
    tput_file = '/usr/users/eigenbrot/research/MaNGA/manga_wiki/wiki_str/spare_cart/spare_cart_tput.txt'
    
    try:
        precoat_folder = glob('{}/UWisc/*{}'.format(basepath,harness[1:]))[0]
        precoat = True
    except IndexError:
        try:
            precoat_folder = glob('{}/UWisc/*{}_precoat'.format(basepath,harness[1:]))[0]
            precoat = True
        except IndexError:
            precoat = False

    for pcoatname in ['2','postcoat','imaging']:
        try:
            postcoat_folder = glob('{}/UWisc/*{}_{}'.format(basepath,harness[1:],pcoatname))[0]
            break
        except IndexError:
            pass

    else:
        raise NameError("Couldn't find postcoat directory!")

    CTech_folder = glob('{}/C-Tech/*{}*'.format(basepath,harness.upper()))
    if len(CTech_folder) == 0:
        CTech_folder = glob('{}/C-Tech/*{}*'.format(basepath,harness.lower()))
    if len(CTech_folder) > 1:
            CTech_folder = glob('{}/C-Tech/*{}_*'.format(basepath,harness.upper()))[0]
    else:
        CTech_folder = CTech_folder[0]

    (CTech_par, minnie), CTech_dat = get_folder(CTech_folder,harness)
    if precoat:
        (pre_par, minnie), pre_dat = get_folder(precoat_folder,harness)
    (post_par, minnie), post_dat = get_folder(postcoat_folder,harness)

    if minnie:
        final_string = ''
        for ferrule in minnie.keys():
            print ferrule, minnie[ferrule]
            CTech_string = format_info(CTech_par, CTech_dat, ferrule, minnie[ferrule])
            if precoat:
                pre_string = format_info(pre_par, pre_dat, ferrule, minnie[ferrule])
            else:
                pre_string = 'NA'
            post_string = format_info(post_par, post_dat, ferrule, minnie[ferrule])
            image_name, ax = plot_throughputs(post_par,ferrule,minnie[ferrule])
            ax.get_figure().savefig('{}/{}'.format(postcoat_folder,image_name),dpi=200)
            final_string += '||Mini Bundle-{} || ma{} || {} || {} || {} || None || [[Image(source:data/manga/teststand/trunk/UWisc/{}/{}, 200px)]] ||\n'.\
                format(minnie[ferrule],harness[2:],
                       CTech_string,pre_string,post_string,os.path.basename(postcoat_folder),image_name)
    else:
        CTech_string = format_info(CTech_par, CTech_dat)
        if precoat:
            pre_string = format_info(pre_par, pre_dat)
        else:
            pre_string = 'NA'
        post_string = format_info(post_par, post_dat)
        numfibers = post_par['nifu']

        image_name, ax = plot_throughputs(post_par)
        ax.get_figure().savefig('{}/{}'.format(postcoat_folder,image_name),dpi=200)
        final_string = '||{} fiber IFU || ma{} || {} || {} || {} || None || [[Image(source:data/manga/teststand/trunk/UWisc/{}/{}, 200px)]] ||\n'.\
            format(numfibers,harness[2:],CTech_string,pre_string,post_string,os.path.basename(postcoat_folder),image_name)

    print final_string
    f = open(tput_file,'a')
    f.write(final_string)
    f.close()
    return

def get_folder(folder,harness):
    parlist = glob('/usr/users/eigenbrot/research/MaNGA/metro_trunk/{}/*.par'.format(harness))
    datfile = glob('{}/*.dat'.format(folder))[0]

    '''grab the most recent yanny file'''
    betterparlist = [p for p in parlist if 'results.par' not in p]
    betterparlist.sort()
    if len(betterparlist) == 0:
        parfile = parlist[0]
    else:
        parfile = betterparlist[-1]
    
    print parlist
    print betterparlist

    print 'using yanny file: {}\nusing dat file: {}'.format(parfile,datfile)
    
    return get_data(parfile), datfile

def get_data(yanny_file):

    par = yanny(yanny_file,np=True)
    ferrule_list = list(np.unique(par['BUNDLEMAP']['frlCode'])[1:])
    print ferrule_list
    if len(ferrule_list) == 1:
        return par, False
    else:
        ferrule_dict = {}
        for ferrule in ferrule_list:
            idx = np.where(par['BUNDLEMAP']['frlCode'] == ferrule)
            fiber = par['BUNDLEMAP']['fnum'][idx][0]
            if fiber < 8:
                facenum = 1
            elif 8 <= fiber < 15:
                facenum = 2
            elif fiber >= 15:
                facenum = 3
            ferrule_dict[ferrule] = facenum

        return par, ferrule_dict

def plot_throughputs(par, frlcode=None, facenum=None):

    low = 93
    high = 97
    
    if frlcode:
        idx = np.where(par['BUNDLEMAP']['frlCode'] == frlcode)
        skyidx = np.where(par['BUNDLEMAP']['fnum'] == facenum - 3 - 1)
        idx = np.r_[idx[0],skyidx[0][0]]
        prebundlemap = par['BUNDLEMAP'][idx]
    else:
        prebundlemap = par['BUNDLEMAP']
    
    ifu_idx = np.where(prebundlemap['fibertype'] == 'IFU')
    bundlemap = prebundlemap[ifu_idx]
    sky_idx = np.where(prebundlemap['fibertype'] == 'SKY')
    skymap = prebundlemap[sky_idx]


    lim = np.max(bundlemap['xpmm']) + 0.30 
    ax = plt.figure().add_subplot(111)
    patchlist = []
    tputlist = []

    for fiber in bundlemap:
        x_coord = fiber['xpmm']
        y_coord = fiber['ypmm']
        tput = fiber['tput']
        if tput < 1:
            tput *= 100.
        circ = Circle((x_coord, y_coord),radius=0.075)
        patchlist.append(circ)
        tputlist.append(tput)
        ax.text(x_coord,y_coord,'{:}\n{:5.2f}'.format(fiber['fnum'],tput),
                fontsize=6,ha='center',va='center')

    for i, sky in enumerate(skymap):
        x_coord = -lim + 0.150 + i*0.30
        if i < 4:
            y_coord = lim - 0.150
        else:
            y_coord = -lim + 0.15
        tput = sky['tput']
        if tput < 1.:
            tput *= 100.
        circ = Circle((x_coord, y_coord),radius=0.075)
        patchlist.append(circ)
        tputlist.append(tput)
        ax.text(x_coord,y_coord,'{:}\n{:5.2f}'.format(sky['fnum'],tput),
                fontsize=6,ha='center',va='center')
        

    collection = PatchCollection(patchlist,cmap=plt.get_cmap('RdYlGn'),
                                 norm=matplotlib.colors.Normalize(vmin=low,vmax=high),
                                 alpha=0.3)

    collection.set_array(np.array(tputlist))
    ax.add_collection(collection)
    cbar = ax.get_figure().colorbar(collection)
    cbar.ax.set_ylabel('Throughput')
    ax.set_xlim(-lim,lim)
    ax.set_ylim(-lim,lim)
    ax.set_xlabel('X distance from ferrule center [mm]')
    ax.set_ylabel('Y distance from ferrule center [mm]')
    if frlcode:
        ax.set_title('Harness {} face {}\ngenerated on {}'.\
                         format(par['harName'],facenum,time.asctime()))
        savename = '{}_{}_tputhex.png'.format(par['harName'],facenum)
    else:
        ax.set_title('Harness {}\ngenerated on {}'.format(par['harName'],time.asctime()))
    #    ax.get_figure().show()
        savename = '{}_tputhex.png'.format(par['harName'])

    #ax.get_figure().savefig(savename,dpi=200)

    return savename, ax

def format_info(par,datfile,frlcode=None, facenum=None):

    f = open(datfile,'r')
    lines = f.readlines()
    dateline = lines[0]
    year, month, day = dateline.split('_')[-1].split('-')[0:3]
    
    if frlcode:
        idx = np.where(par['BUNDLEMAP']['frlCode'] == frlcode)
        skyidx = np.where(par['BUNDLEMAP']['fnum'] == facenum - 3 - 1)
        print idx, skyidx
        idx = np.r_[idx[0],skyidx[0][0]]
        print idx
        bundlemap = par['BUNDLEMAP'][idx]
    else:
        bundlemap = par['BUNDLEMAP']

    mean = np.mean(bundlemap['tput'])
    minimum = np.min(bundlemap['tput'])
    maximum = np.max(bundlemap['tput'])

    if maximum < 1.:
        mean *= 100.
        minimum *= 100.
        maximum *= 100.

    return '{:4.2f}, {:4.2f}, {:4.2f}, {:02n}/{:02n}/{:4n}'.\
        format(mean,minimum,maximum,int(month),int(day),int(year))


if __name__ == '__main__':
    sys.exit(main())
