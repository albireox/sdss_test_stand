#!/usr/bin/env python
"""Keithley model 6485 Picoammeter RS-232 interface

History:
2008-03-19 ROwen    First pass (totally raw) copied from the IStep interface
2008-05-27 ROwen    Added measure method
"""
__all__ = ["PicoammeterDevice"]
import re
import sys
import Tkinter
#import pychecker.checker # uncomment to run pychecker
import RO.AddCallback
import RO.Comm.TkSerial

__version__ = "1.0a1"

_DefTimeLim = 2.0 # default time limit in seconds

def isQuery(cmdStr):
    return "?" in cmdStr

def parseReading(readStr):
    """Parse a reading and return (current, state, isOK)"""
    readData = readStr.split(",")
    if len(readData) != 3:
        raise RuntimeError("Cannot parse %r as a reading" % (readStr,))
    value = float(readData[0])
    status = int(float(readData[2]) + 0.5)
    isOK = status & 0x81
    return (value, status, isOK)

class BaseCmdObj(RO.AddCallback.BaseMixin):
    """Base class for information about a command.
    
    Subclasses must override all class variables or leave them all alone
    """
    RunningState = None
    SuccessState = -2
    TimeoutState = -25
    CommErrorState = -26
    _StateDescr = {
        RunningState: "running",
        SuccessState: "succeeded",
        TimeoutState: "failed: timed out",
        CommErrorState:   "failed: communication error",
    }
    _Tk = None
    def __init__(self, descr, callFunc=None, logFunc=None, timeLim=_DefTimeLim):
        """Create a new command info object
        
        Inputs:
        - callFunc  callback function (receives one argument: this object)
        - logFunc   function to log state changes (None if none)
        - timeLim   time limit for command to finish (sec)
        """
        if not self._Tk:
            self._Tk = Tkinter.Frame()
        if logFunc != None and not callable(logFunc):
            raise RuntimeError("Log function %r not callable" % (logFunc,))
        self._logFunc = logFunc
        RO.AddCallback.BaseMixin.__init__(self, callFunc=callFunc, callNow=False)
        self._timeoutID = None
        if timeLim != None:
            self._timeLimMS = int(0.5 + (1000 * timeLim))
            self._setTimeout(self._timeLimMS)
        else:
            self._timeLimMS = None

        self.reply = None
        self.state = self.RunningState
        self.descr = str(descr)
        if self._logFunc:
            self._logFunc(self.getStateStr() + "\n")
    
    def didFail(self):
        """Return True if the command failed"""
        return self.isDone() and not self.didSucceed()
    
    def didSucceed(self):
        """Return True if command succeeded (possibly with warnings)"""
        return self.state == self.SuccessState

    def getStateStr(self):
        """Return a textual description of the state"""
        reasonStr = self._StateDescr.get(self.state)
        if reasonStr == None:
            reasonStr = "failed: unknown state %s" % (self.state,)
        return "%s %s" % (self.descr, reasonStr)
    
    def isDone(self):
        """Return True if the command is done (succeeded or failed)"""
        return self.state != self.RunningState
    
    def setState(self, state):
        """Set state of command object.
        
        - state  new state; if None then state is unchanged (useful for logging)
        """
        if self.isDone():
            raise RuntimeError("Already finished")
        self.state = int(state)
        self._setTimeout()
        self._doCallbacks()
        if self._logFunc and self.isDone():
            self._logFunc(self.getStateStr() + "\n")
    
    def _setTimeout(self, timeLimMS=None):
        """Start, restart or cancel the time limit.
        
        Cancels the existing timeout timer, if any.
        Then if timeLimMS != None, starts a new timeout timer.
        
        Inputs:
        - timeLimMS desired time limit; None if no time limit
        """
        if self._timeoutID != None:
            self._Tk.after_cancel(self._timeoutID)
        if timeLimMS != None:
            self._timeoutID = self._Tk.after(timeLimMS, self._timeout)
    
    def _timeout(self):
        """Time out the command"""
        self._timeoutID = None
        if self.isDone():
            return
        self.setState(state=self.TimeoutState)
    
    def __str__(self):
        return self.descr


class CmdObj(BaseCmdObj):
    """Information about a command.
    
    Note: the time limit applies to each individual line of data received.
    Thus the default short time limit should be applicable for all commands.
    On the other hand, if a command outputs a lot of data then it is permitted
    to take considerably longer than the time limit to finish.
    
    Note: a warning is treated as success; to detect a warning use:
        "warning:" in str(self)
    """
    def __init__(self, cmdStr, callFunc=None, logFunc=None, timeLim=_DefTimeLim):
        """Create a new command info object.
        Inputs:
        - cmdStr: command string (any case)
        - callFunc  callback function; called when command finishes or reply received; receives one argumet: this object
        - timeLim   time limit (sec); if None then no limit
        """
        self.cmdStr = cmdStr.upper()
        self.reply = []
        descr = "Command %r" % (self.cmdStr,)
        BaseCmdObj.__init__(self, descr=descr, callFunc=callFunc, logFunc=logFunc, timeLim=timeLim)
    
    def setReply(self, replyStr):
        """Set the reply string and terminate the command"""
        self.reply = replyStr
        # refresh time limit timer, if any
        self._setTimeout()
        self.setState(self.SuccessState)
            
    def getCmdStr(self):
        return self.cmdStr
 

class NullCmdObj(CmdObj):
    def __init__(self):
        CmdObj.__init__(self, cmdStr="", timeLim=None)
        self.setState(self.SuccessState)


class CmdSeqObj(BaseCmdObj):
    def __init__(self, cmdSeq, name=None, failCmd=None, callFunc=None, logFunc=None):
        """Create a command sequence objects.

        Inputs:
        - cmdSeq    a sequence, each element of which is either a command string
                    or else a pair of items: (cmdStr, timeLim);
                    if cmdSeq is a null sequence then the sequence is immediately considered done
        - name      a short name for the sequence
        - callFunc  a callback function; it will be called every time a command
                    of the sequence finishes
        """
        if name:
            descr = "%s sequence" % (name,)
        else:
            descr = "Command sequence"
        name = name or ""
        self._cmdStrTimeoutList = []
        self._nextCmdInd = 0
        self._currCmd = NullCmdObj()
        self._failCmdObj = None
        self.reportedDone = False
        self._failCmdStr = failCmd
        for cmdData in cmdSeq:
            if RO.SeqUtil.isSequence(cmdData):
                cmdStr, timeLim = cmdData
            else:
                cmdStr = cmdData
                timeLim = _DefTimeLim
            self._cmdStrTimeoutList.append((str(cmdStr), timeLim))
        BaseCmdObj.__init__(self, descr=descr, callFunc=callFunc, logFunc=logFunc)
    
    def didFail(self):
        return self._currCmd.didFail()
    
    def didSucceed(self):
        return self._nextCmdInd >= len(self._cmdStrTimeoutList) and self._currCmd.didSucceed()
    
    def getStateStr(self):
        if self.didSucceed():
            return "%s succeeded" % (self,)
        elif self.didFail():
            return "%s: %s" % (self, self._currCmd.getStateStr())
        return "%s running" % (self,)

    def isDone(self):
        return self.didSucceed() or self.didFail()
    
    def getFailCmd(self, callFunc=None, logFunc=None):
        """Return fail command object, or None if fail command should not be executed.
        """
        if not self.didFail():
            return None
        if not self._failCmdStr:
            return None
        if self._failCmdObj:
            return None
        self._failCmdObj = CmdObj(self._failCmdStr, callFunc=None, logFunc=logFunc)
        return self._failCmdObj
    
    def getNextCmd(self, callFunc=None):
        """Return next command as a CmdObj.
        Raise RuntimeError if all done.
        """
        if self.isDone():
            raise RuntimeError("Command sequence is finished")
        if not self._currCmd.isDone():
            raise RuntimeError("Current command is not finished")
        cmdStr, timeLim = self._cmdStrTimeoutList[self._nextCmdInd]
        self._nextCmdInd += 1
        
        newCmd = CmdObj(cmdStr, callFunc=callFunc, timeLim=timeLim)
        self._currCmd = newCmd
        return newCmd
    
    def reportDone(self):
        if not self.isDone():
            raise RuntimeError("Not done")
        if not self.reportedDone:
            self.reportedDone = True
            if self._logFunc:
                self._logFunc(self.getStateStr() + "\n")
    
    def __str__(self):
        return "%s (%d/%d)" %  (self.descr, self._nextCmdInd, len(self._cmdStrTimeoutList))
    
    
class NullCmdSeqObj(CmdSeqObj):
    def __init__(self):
        CmdSeqObj.__init__(self, ())
        self.reportedDone
        

class PicoammeterDevice(RO.AddCallback.BaseMixin):
    LogReads = 0x01
    LogWrites = 0x02
    LogCommands = 0x04 # log start and end of all commands
    LogSequences = 0x08 # log start and end of all sequences
    def __init__(self,
        serPortName,
        callFunc = None,
        logFunc = None,
        logMask = 0,
    ):
        """Create a PicoammeterDevice
        
        Inputs:
        - serPortName   name of serial port
        - callFunc  function to call when the state changes
        - logFunc   log function (None if none); must take one argument: the string to log
                    the string will have a final \n
        - logMask   what to log
        """
        RO.AddCallback.BaseMixin.__init__(self)
        self._currCmd = NullCmdObj()
        self._currCmdSeq = NullCmdSeqObj()
        if logFunc == None:
            logMask = 0
        elif not callable(logFunc):
            raise RuntimeError("Log function %r is not callable" % (logFunc,))
        self._logFunc = logFunc
        self._logMask = int(logMask)

        self.serPortName = serPortName,
        self.conn = None
        self.connect()

        self.addCallback(callFunc, callNow=False)
        
    def connect(self):
        """Open a new connection (a no-op if already connected)"""
        if self.conn != None and self.isConnected():
            return
        self.conn = RO.Comm.TkSerial.TkSerial(
            portName = self.serPortName,
            baud = 9600,
            handshake = "xonxoff",
            translation = "cr",
            readCallback = self._read,
        )
    
    def didFail(self):
        """Return True if any of these failed: current command, move or sequence"""
        return self._currCmd.didFail() or self._currCmdSeq.didFail()
    
    def didSucceed(self):
        """Return True if all of these succeeded: current command, move and sequence"""
        return self._currCmd.didSucceed() and self._currCmdSeq.didSucceed()
    
    def getStateStr(self):
        """Return status as a string"""
        if not self._currCmdSeq.isDone():
            return self._currCmdSeq.getStateStr()
        return self._currCmd.getStateStr()
    
    def isDone(self):
        """Return True if all of these are done: current command, move and sequence"""
        return self._currCmd.isDone() and self._currCmdSeq.isDone()
    
    def isConnected(self):
        return self.conn.isOpen()

    def measure(self):
        """Start one current measurement"""
        self.startCmd("READ?")
    
    def startCmd(self, cmdStr, timeLim=_DefTimeLim, _checkSeq=True):
        """Start a command.
        
        Inputs:
        - cmdStr    command string; will be uppercased;
                    for command BX (begin move) you may add a desired stop code after a space, e.g. "BX 23"
        - timeLim   time limit for command (sec); for BX the time limit
                    is applied to completion of the move but a shorter limit
                    is used for starting the move
                    if None then no limit
        - _checkSeq  (for internal use only) set False if this command is part of a sequence
        """
        if not self._currCmd.isDone():
            raise RuntimeError("Already running %r" % (self._currCmd.cmdStr,))
        if not self.isConnected():
            raise RuntimeError("Disconnected")
            
        if _checkSeq:
            if not self._currCmdSeq.isDone():
                raise RuntimeError("Executing a command sequence")

        if self._logMask & self.LogCommands:
            logFunc = self._logFunc
        else:
            logFunc = None
        self._startCmdObj(CmdObj(cmdStr, callFunc=self._callFunc, logFunc=logFunc, timeLim=timeLim))
    
    def startCmdList(self, cmdList, name=None, failCmd=None):
        """Execute a sequence of commands; halt at first failure.
        
        Inputs:
        - cmdList   sequence of command to execute; each element must be either a command string
                    or else a pair: (cmdStr, timeLim)
        - name      a name for the sequence (optional)
        - failCmd   command to execute if any command in cmdList fails; None if no fail command
        
        Returns:
        - cmdStatus an CmdObjState representing the state of the last command executed
        - a list of list of replies, one entry per command executed
        """
        if not self.isDone():
            raise RuntimeError("Busy")
        if not self.isConnected():
            raise RuntimeError("Disconnected")

        if self._logMask & self.LogSequences:
            logFunc = self._logFunc
        else:
            logFunc = None
        self._currCmdSeq = CmdSeqObj(cmdList, name=name, failCmd=failCmd, logFunc=logFunc)
        self._nextCmd()
    
    def _callFunc(self, dumObj=None):
        self._doCallbacks()
        self._nextCmd()
    
    def _nextCmd(self, dum=None):
        if not self._currCmd.isDone():
            return

        if self._currCmdSeq.isDone():
            if self._logMask & self.LogSequences:
                self._currCmdSeq.reportDone()

        if self._currCmdSeq.didFail():
            # start fail command, if appropriate
            # (there is one and it has not been run)
            if self._logMask & self.LogCommands:
                logFunc = self._logFunc
            else:
                logFunc = None
            failCmdObj = self._currCmdSeq.getFailCmd(callFunc=self._callFunc, logFunc=logFunc)
            if failCmdObj:
                self._startCmdObj(failCmdObj)
            return

        if self._currCmdSeq.isDone():
            return

        cmdObj = self._currCmdSeq.getNextCmd(callFunc=self._callFunc)
        self._startCmdObj(cmdObj)
    
    def _read(self, dumConn=None):
        """Handle data from the I-Step controller
        """
        try:
            reply = self.conn.readLine()
        except Exception, e:
            if not self._currCmd.isDone():
                self._currCmd.setState(self._currCmd.CommErrorState)
            raise
        if not reply:
            return
        
        if self._logMask & self.LogReads:
            self._logFunc(reply + "\n")
        
        # output data
        if not self._currCmd.isDone():
            self._currCmd.setReply(reply)
        else:
            sys.stderr.write("Warning: ignoring unexpected data %r\n" % (reply,))
    
    def _startCmdObj(self, cmdObj):
        """Low-level routine to start a command.
        You must be sure the command may safely be started before calling this.
        """
        self._currCmd = cmdObj
        
        cmdStr = cmdObj.getCmdStr()
        if self._logMask & self.LogWrites:
            self._logFunc(cmdStr + "\n")
        try:
            self.conn.writeLine(cmdStr)
        except Exception, e:
            sys.stderr.write("Communication error: %s\n" % (e,))
            self._currCmd.setState(self._currCmd.CommErrorState)
            return
        
        if isQuery(cmdStr):
            self._doCallbacks()
        else:
            cmdObj.setState(cmdObj.SuccessState)


if __name__ == "__main__":
    import Tkinter
    import RO.Wdg
    import getKeyspanDevice

    KeyspanPort = 2
    
    root = Tkinter.Tk()
    logWdg = RO.Wdg.LogWdg(root)
    logWdg.pack(side="top", expand=True, fill="both")

    logMask = PicoammeterDevice.LogReads | PicoammeterDevice.LogWrites | PicoammeterDevice.LogCommands | PicoammeterDevice.LogSequences
#    logMask = PicoammeterDevice.LogReads | PicoammeterDevice.LogWrites | PicoammeterDevice.LogSequences
    devPath = getKeyspanDevice.getKeyspanDevice(KeyspanPort)
    ctrllr = PicoammeterDevice(devPath, logFunc=logWdg.addOutput, logMask=logMask)

    cmdWdg = RO.Wdg.CmdWdg(root, ctrllr.startCmd)
    cmdWdg.pack(side="top", expand=True, fill="x")

    root.mainloop()
