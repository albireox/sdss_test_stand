#!/usr/bin/env python
"""I-Step stepper motor controller interface

History:
2008-04-24 ROwen
"""
__all__ = ["IStepDevice", "UseDefTimeLim"]
import re
import sys
import Tkinter
#import pychecker.checker # uncomment to run pychecker
import RO.AddCallback
import RO.Comm.TkSerial

__version__ = "1.0b2"

UseDefTimeLim = True
_DefCmdTimeLim = 1.0 # seconds
_DefMoveTimeLim = 5.0 # seconds

MoveRE = re.compile(r"\b(B[CXY])(\d*)\b", re.IGNORECASE)

def isMoveCmd(cmdStr):
    """Return True if the cmdStr contains a begin statement
    """
    return bool(MoveRE.search(cmdStr))

def cmdObjFromCmdStr(cmdStr, callFunc=None, logFunc=None, timeLim=UseDefTimeLim):
    """Create a command object from a command string;
    the object is a CmdObj unless the command begins a move, in which case it is a MoveObj
    """
    moveMatches = MoveRE.findall(cmdStr)
    if not moveMatches:
        # not a move command
        if timeLim == UseDefTimeLim:
            timeLim = _DefCmdTimeLim
        return CmdObj(cmdStr, callFunc=callFunc, logFunc=logFunc, timeLim=timeLim)
    
    elif len(moveMatches) > 1:
        # command contains more than one starts
        raise RuntimeError("Command %r contains more than one move command" % (cmdStr,))
    
    # is a move command; find and strip the move start code
    modCmdStr = MoveRE.sub(r"\1", cmdStr)
    desEndCodeStr = moveMatches[0][1]
    if desEndCodeStr:
        try:
            desEndCode = int(desEndCodeStr)
        except Exception:
            raise RuntimeError("End code not integer in %s%s" % (moveMatches[0], moveMatches[1]))
    else:
        desEndCode = None
    cmdObj = CmdObj(modCmdStr, callFunc=callFunc)
    if timeLim == UseDefTimeLim:
        timeLim = _DefMoveTimeLim
    return MoveObj(cmdObj=cmdObj, callFunc=callFunc, logFunc=logFunc, timeLim=timeLim, desEndCode=desEndCode)

class BaseCmdObj(RO.AddCallback.BaseMixin):
    """Base class for information about an IStep command.
    
    Subclasses must override all class variables or leave them all alone
    """
    RunningState = None
    SuccessState = -2
    TimeoutState = -25
    CommErrorState = -26
    _StateDescr = {
        RunningState: "running",
        SuccessState: "succeeded",
        TimeoutState: "failed: timed out",
        CommErrorState:   "failed: communication error",
    }
    _Tk = None
    def __init__(self, descr, callFunc=None, logFunc=None, timeLim=None):
        """Create a new command info object
        
        Inputs:
        - callFunc  callback function (receives one argument: this object)
        - logFunc   function to log state changes (None if none)
        - timeLim   time limit for command to finish (sec)
        """
        if not self._Tk:
            self._Tk = Tkinter.Frame()
        if logFunc != None and not callable(logFunc):
            raise RuntimeError("Log function %r not callable" % (logFunc,))
        self._logFunc = logFunc
        RO.AddCallback.BaseMixin.__init__(self, callFunc=callFunc, callNow=False)
        self._timeoutID = None
        if timeLim != None:
            self._timeLimMS = int(0.5 + (1000 * timeLim))
            self._setTimeout(self._timeLimMS)
        else:
            self._timeLimMS = None

        self.state = self.RunningState
        self.descr = str(descr)
        if self._logFunc:
            self._logFunc(self.getStateStr() + "\n")
    
    def didFail(self):
        """Return True if the command failed"""
        return self.isDone() and not self.didSucceed()
    
    def didSucceed(self):
        """Return True if command succeeded (possibly with warnings)"""
        return self.state == self.SuccessState

    def getStateStr(self):
        """Return a textual description of the state"""
        reasonStr = self._StateDescr.get(self.state)
        if reasonStr == None:
            reasonStr = "failed: unknown state %s" % (self.state,)
        return "%s %s" % (self.descr, reasonStr)
    
    def isDone(self):
        """Return True if the command is done (succeeded or failed)"""
        return self.state != self.RunningState
    
    def isMoveObj(self):
        """Return True if the object is a move object"""
        return False
    
    def setState(self, state):
        """Set state of command object.
        
        - state  new state; if None then state is unchanged (useful for logging)
        """
        if self.isDone():
            raise RuntimeError("Already finished")
        self.state = int(state)
        self._setTimeout()
        self._doCallbacks()
        if self._logFunc and self.isDone():
            self._logFunc(self.getStateStr() + "\n")
    
    def _setTimeout(self, timeLimMS=None):
        """Start, restart or cancel the time limit.
        
        Cancels the existing timeout timer, if any.
        Then if timeLimMS != None, starts a new timeout timer.
        
        Inputs:
        - timeLimMS desired time limit; None if no time limit
        """
        if self._timeoutID != None:
            self._Tk.after_cancel(self._timeoutID)
        if timeLimMS != None:
            self._timeoutID = self._Tk.after(timeLimMS, self._timeout)
    
    def _timeout(self):
        """Time out the command"""
        self._timeoutID = None
        if self.isDone():
            return
        self.setState(state=self.TimeoutState)
    
    def __str__(self):
        return self.descr


class CmdObj(BaseCmdObj):
    """Information about an IStep command.
    
    Note: the time limit applies to each individual line of data received.
    Thus the default short time limit should be applicable for all commands.
    On the other hand, if a command outputs a lot of data then it is permitted
    to take considerably longer than the time limit to finish.
    
    Note: a warning is treated as success; to detect a warning use:
        "warning:" in str(self)
    """
    RunningState = None
    SuccessState = -1
    TimeoutState = -25
    CommErrorState = -26
    _SuccessStateSet = set((SuccessState, 21, 22)) # success, possibly with a warning
    _StateDescr = {
        RunningState: "running",
        SuccessState: "succeeded",
        21: "succeeded with a warning: parameter out of range; truncated to be in range",
        22: "succeeded with a warning: unrecognized sub-command in data field; field skipped",
        0: "failed: empty command or check-sum error",
        1: "failed: unrecognized sub-command",
        3: "failed: save to EEPROM failed",
        10: "failed: required optional hardware is missing",
        15: "failed: operation failed e.g. motor moved during enable",
        16: "failed: reqeusted parameter cannot be changed while moving",
        17: "failed: command not supported",
        19: "failed: invalid data (e.g. ID out of range)",
        20: "failed: missing data field",
        TimeoutState: "failed: timed out",
        CommErrorState:  "failed: communication error",
    }
    def __init__(self, cmdStr, callFunc=None, logFunc=None, timeLim=_DefCmdTimeLim):
        """Create a new command info object.
        Inputs:
        - cmdStr: command string (any case)
        - callFunc  callback function; called when command finishes or reply received; receives one argumet: this object
        - timeLim   time limit (sec); if None then no limit
        """
        self.cmdStr = cmdStr.upper()
        self.replies = []
        descr = "Command %r" % (self.cmdStr,)
        BaseCmdObj.__init__(self, descr=descr, callFunc=callFunc, logFunc=logFunc, timeLim=timeLim)
    
    def addReply(self, replyStr):
        """Append a reply string to the replies for this command"""
        self.replies.append(replyStr)
        # refresh time limit timer, if any
        self._setTimeout(self._timeLimMS)
        self._doCallbacks()
            
    def didSucceed(self):
        """Return True if command succeeded (possibly with warnings)"""
        return self.state in self._SuccessStateSet
    
    def getCmdStr(self):
        return self.cmdStr
 

class NullCmdObj(CmdObj):
    def __init__(self):
        CmdObj.__init__(self, cmdStr="", timeLim=None)
        self.setState(self.SuccessState)


class BeginObj(BaseCmdObj):
    """State of begin message"""
    RunningState = None
    SuccessState = 1
    TimeoutState = -25
    _StateDescr = {
        RunningState: "running",
        1: "succeeded",
        9: "failed: motion is common and other axis failed to move",
        10: "failed: continuous path mode error: length of first vector is too short",
        11: "failed: continue path mode error: length of last vector is too short",
        12: "failed: continue path mode error: LI>UI",
        14: "failed: arbitrary path generation error",
        20: "failed: motor disabled",
        21: "failed: required motion mode is invalid",
        22: "failed: common mode error: either the two axes are not in the same motion mode or common motion not allowed",
        23: "failed: requested position outside soft limits",
        24: "failed: controller could not calculate motion",
        25: "failed: common motion error: begin command has been issued for one axis instead of both",
        90: "failed: memory error; restore memory and issue T7 command",
        TimeoutState: "failed: timed out",
    }
    def __init__(self, callFunc=None, logFunc=None, timeLim=_DefCmdTimeLim):
        BaseCmdObj.__init__(self, descr="Move begin", callFunc=callFunc, logFunc=logFunc, timeLim=timeLim, )


class EndObj(BaseCmdObj):
    """State of move end message"""
    RunningState = None
    SuccessState = 1
    TimeoutState = -25
    _StateDescr = {
        RunningState: "running",
        1: "suceeded",
        2: "failed: motion killed by user",
        9: "failed: motion is common and the other axis failed to complete the motion",
        14: "failed: arbitrary path generation error",
        20: "failed: motor disabled",
        22: "failed: emergency stop",
        23: "failed: hit low limit swtich",
        24: "failed: hit high limit switch",
        25: "failed: error limit ER or EA exceeded",
        26: "failed: driver fault: overheated or could not develop required current",
        TimeoutState: "failed: timed out",
    }
    def __init__(self, callFunc=None, logFunc=None, timeLim=_DefMoveTimeLim, desEndCode=None):
        if desEndCode not in (None, self.RunningState, self.SuccessState):
            desEndCode = int(desEndCode)
            if desEndCode not in self._StateDescr:
                raise RuntimeError("Unknown stop code %r" % (desEndCode,))
            self._StateDescr = self._StateDescr.copy()
            self._StateDescr[self.SuccessState] = "failed: move ended normally but should have ended with code %s" % (desEndCode,)
            self._StateDescr[desEndCode] = self._StateDescr[desEndCode].replace("failed:", "succeeded:") + ", as desired"
            self.SuccessState = int(desEndCode)
        BaseCmdObj.__init__(self, descr="Move end", callFunc=callFunc, logFunc=logFunc, timeLim=timeLim)


class MoveObj(BaseCmdObj):
    """State of a move"""
    def __init__(self, cmdObj, callFunc=None, logFunc=None, timeLim=_DefMoveTimeLim, desEndCode=None):
        if timeLim == None:
            cmdTimeLim = None
        else:
            cmdTimeLim = _DefCmdTimeLim
        self.cmdObj = cmdObj
        self.begObj = BeginObj(callFunc=self._callFunc, timeLim=cmdTimeLim)
        self.endObj = EndObj(callFunc=self._callFunc, timeLim=timeLim, desEndCode=desEndCode)
        descr = "Move %r" % (cmdObj.cmdStr,)
        BaseCmdObj.__init__(self, descr=descr, callFunc=callFunc, logFunc=logFunc, timeLim=cmdTimeLim)
    
    def didFail(self):
        return self.cmdObj.didFail() or self.begObj.didFail() or self.endObj.didFail()
    
    def didSucceed(self):
        return self.cmdObj.didSucceed() and self.begObj.didSucceed() and self.endObj.didSucceed()
    
    def getStateStr(self):
        if not self.isDone():
            return "%r running" % (self.descr,)
        elif self.didSucceed():
            return "%s succeeded" % (self.descr,)
        elif self.cmdObj.didFail():
            return "%s failed: %s" % (self.descr, self.cmdObj.getStateStr())
        elif self.begObj.didFail():
            return "%s failed: %s" % (self.descr, self.begObj.getStateStr())
        elif self.endObj.didFail():
            return "%s failed: %s" % (self.descr, self.endObj.getStateStr())
        return "%s status unknown (internal bug)" % (self.descr,)

    def isDone(self):
        return self.didSucceed() or self.didFail()

    def isMoveObj(self):
        """Return True if the object is a move object"""
        return True
    
    def _callFunc(self, dumObj=None):
        if self.isDone():
            # make sure begin and end timers are off
            self.begObj._setTimeout()
            self.endObj._setTimeout()
        self._doCallbacks()
        if self._logFunc and self.isDone():
            self._logFunc(self.getStateStr() + "\n")


class NullMoveObj(MoveObj):
    def __init__(self):
        MoveObj.__init__(self, cmdObj=NullCmdObj(), timeLim=None)
        self.begObj.setState(self.begObj.SuccessState)
        self.endObj.setState(self.endObj.SuccessState)


class CmdSeqObj(BaseCmdObj):
    def __init__(self, cmdSeq, name=None, failCmd=None, callFunc=None, logFunc=None):
        """Create a command sequence objects.

        Inputs:
        - cmdSeq    a sequence, each element of which is either a command string
                    or else a pair of items: (cmdStr, timeLim);
                    if cmdSeq is a null sequence then the sequence is immediately considered done
        - name      a short name for the sequence
        - callFunc  a callback function; it will be called every time a command
                    of the sequence finishes
        """
        if name:
            descr = "%s sequence" % (name,)
        else:
            descr = "Command sequence"
        name = name or ""
        self._cmdStrTimeoutList = []
        self._nextCmdInd = 0
        self.currCmd = NullCmdObj()
        self._failCmdObj = None
        self.reportedDone = False
        if failCmd:
            if isMoveCmd(failCmd):
                raise RuntimeError("Fail command=%s; cannot start a move" % (failCmd,))
            self._failCmdStr = failCmd
        for cmdData in cmdSeq:
            if RO.SeqUtil.isSequence(cmdData):
                cmdStr, timeLim = cmdData
            else:
                cmdStr = cmdData
                timeLim = UseDefTimeLim
            self._cmdStrTimeoutList.append((str(cmdStr), timeLim))
        BaseCmdObj.__init__(self, descr=descr, callFunc=callFunc, logFunc=logFunc)
    
    def didFail(self):
        return self.currCmd.didFail()
    
    def didSucceed(self):
        return self._nextCmdInd >= len(self._cmdStrTimeoutList) and self.currCmd.didSucceed()
    
    def getStateStr(self):
        if self.didSucceed():
            return "%s succeeded" % (self,)
        elif self.didFail():
            return "%s: %s" % (self, self.currCmd.getStateStr())
        return "%s running" % (self,)

    def isDone(self):
        return self.didSucceed() or self.didFail()
    
    def getFailCmd(self, callFunc=None, logFunc=None):
        """Return fail command object, or None if fail command should not be executed.
        """
        if not self.didFail():
            return None
        if not self._failCmdStr:
            return None
        if self._failCmdObj:
            return None
        self._failCmdObj = cmdObjFromCmdStr(self._failCmdStr, callFunc=None, logFunc=logFunc)
        return self._failCmdObj
    
    def getNextCmd(self, callFunc=None):
        """Return next command as a CmdObj or MoveObj.
        Raise RuntimeError if all done.
        """
        if self.isDone():
            raise RuntimeError("Command sequence is finished")
        if not self.currCmd.isDone():
            raise RuntimeError("Current command is not finished")
        cmdStr, timeLim = self._cmdStrTimeoutList[self._nextCmdInd]
        self._nextCmdInd += 1
        
        newCmd = cmdObjFromCmdStr(cmdStr, callFunc=callFunc, timeLim=timeLim)
        self.currCmd = newCmd
        return newCmd
    
    def reportDone(self):
        if not self.isDone():
            raise RuntimeError("Not done")
        if not self.reportedDone:
            self.reportedDone = True
            if self._logFunc:
                self._logFunc(self.getStateStr() + "\n")
    
    def __str__(self):
        return "%s (%d/%d)" %  (self.descr, self._nextCmdInd, len(self._cmdStrTimeoutList))
    
    
class NullCmdSeqObj(CmdSeqObj):
    def __init__(self):
        CmdSeqObj.__init__(self, ())
        self.reportedDone
        

class IStepDevice(RO.AddCallback.BaseMixin):
    LimitMargin = 500 # margin between each soft and hard limit
    FullRange = 40000 # range of motion between soft limits
    LogReads = 0x01
    LogWrites = 0x02
    LogCommands = 0x04 # log start and end of all commands
    LogSequences = 0x08 # log start and end of all sequences
    def __init__(self,
        serPortName,
        callFunc = None,
        logFunc = None,
        logMask = 0,
    ):
        """Create an IStepDevice
        
        Inputs:
        - serPortName   name of serial port
        - callFunc  function to call when the state changes
        - logFunc   log function (None if none); must take one argument: the string to log
                    the string will have a final \n
        - logMask   what to log
        """
        RO.AddCallback.BaseMixin.__init__(self)
        self._currCmd = NullCmdObj()
        self._currMove = NullMoveObj()
        self._currCmdSeq = NullCmdSeqObj()
        if logFunc == None:
            logMask = 0
        elif not callable(logFunc):
            raise RuntimeError("Log function %r is not callable" % (logFunc,))
        self._logFunc = logFunc
        self._logMask = int(logMask)

        self.serPortName = serPortName
        self.conn = None
        self.connect()

        self.addCallback(callFunc, callNow=False)
        
    def connect(self):
        """Open a new connection (a no-op if already connected)"""
        if self.conn != None and self.isConnected():
            return
        self.conn = RO.Comm.TkSerial.TkSerial(
            portName = self.serPortName,
            handshake = "none",
            translation = "crlf",
            readCallback = self._read,
        )
    
    def didFail(self):
        """Return True if any of these failed: current command, move or sequence"""
        return self._currCmd.didFail() or self._currMove.didFail() or self._currCmdSeq.didFail()
    
    def didSucceed(self):
        """Return True if all of these succeeded: current command, move and sequence"""
        return self._currCmd.didSucceed() and self._currMove.didSucceed() and self._currCmdSeq.didSucceed()

    def isConnected(self):
        return self.conn.isOpen()
    
    def isDone(self):
        """Return True if all of these are done: current command, move and sequence"""
        return self._currCmd.isDone() and self._currMove.isDone() and self._currCmdSeq.isDone()
    
    def getConfig(self):
        """Query the IStep for its configuration"""
        self.startCmd("RXER EA EC EF CL P0 P1 P2 P3 P4 PH PL SF SR TH SN")

    def getStateStr(self):
        """Return status as a string"""
        if not self._currCmdSeq.isDone():
            return self._currCmdSeq.getStateStr()
        return self._currCmd.getStateStr()
    
    def getStatus(self):
        """Query the IStep for its status"""
        self.startCmd("R")

    def moveAbs(self, absPos):
        """Start moving to a specified absolute position"""
        self.startCmd("SXAP%d BX" % (absPos,))
    
    def moveRel(self, relPos):
        """Start moving by a specified relative amount"""
        self.startCmd("SXRP%d BX" % (relPos,))
    
    def home(self):
        maxDist = (self.FullRange + (2 * self.LimitMargin)) * 1.1
        cmdSet = (
             # zero position and position error, set low limit, set move beyond low limit,
             # and set motion mode to linear point-to-point,
             # enable motor and move into reverse limit (234 indicates desired end code: hit reverse limit)
            "SXZP PL-%d AP-%d SXMM0 SXMO1 BX23" % (maxDist + 10, maxDist),
            # set high limit and move away from limit
            "SXPH%d RP%d BX" % (maxDist, self.LimitMargin), # set high limit
            # zero position and position error, set low limit and high limit and disable motor
            "SXZP PL0 PH%d SXMO0" % (self.FullRange,),
        )
        self.startCmdList(cmdSet, name="Home", failCmd="SXMO0")
        
    def startCmd(self, cmdStr, timeLim=UseDefTimeLim, _checkSeq=True):
        """Start a command.
        
        Inputs:
        - cmdStr    command string; will be uppercased;
                    for command BX (begin move) you may add a desired stop code after a space, e.g. "BX 23"
        - timeLim   time limit for command (sec); for BX the time limit
                    is applied to completion of the move but a shorter limit
                    is used for starting the move
                    if None then no limit
        - _checkSeq  (for internal use only) set False if this command is part of a sequence
        """
        if not self._currCmd.isDone():
            raise RuntimeError("Already running %r" % (self._currCmd.cmdStr,))
        if not self.isConnected():
            raise RuntimeError("Disconnected")
        
        if self._currMove.isDone():
            self._currMove = NullMoveObj()
        elif isMoveCmd(cmdStr):
            raise RuntimeError("Already moving!")
            
        if _checkSeq:
            if not self._currCmdSeq.isDone():
                raise RuntimeError("Executing a command sequence")

        if self._logMask & self.LogCommands:
            logFunc = self._logFunc
        else:
            logFunc = None
        self._startCmdObj(cmdObjFromCmdStr(cmdStr, callFunc=self._callFunc, logFunc=logFunc, timeLim=timeLim))
    
    def startCmdList(self, cmdList, name=None, failCmd=None):
        """Execute a sequence of commands; halt at first failure.
        
        Inputs:
        - cmdList   sequence of command to execute; each element must be either a command string
                    or else a pair: (cmdStr, timeLim)
        - name      a name for the sequence (optional)
        - failCmd   command to execute if any command in cmdList fails; None if no fail command
        
        Returns:
        - cmdStatus an CmdObjState representing the state of the last command executed
        - a list of list of replies, one entry per command executed
        """
        if not self.isDone():
            raise RuntimeError("Busy")
        if not self.isConnected():
            raise RuntimeError("Disconnected")

        if self._logMask & self.LogSequences:
            logFunc = self._logFunc
        else:
            logFunc = None
        self._currCmdSeq = CmdSeqObj(cmdList, name=name, failCmd=failCmd, logFunc=logFunc)
        self._nextCmd()
    
    def _callFunc(self, dumObj=None):
        self._doCallbacks()
        self._nextCmd()
    
    def _nextCmd(self, dum=None):
        if not self._currCmd.isDone() or not self._currMove.isDone():
            return

        if self._currCmdSeq.isDone():
            if self._logMask & self.LogSequences:
                self._currCmdSeq.reportDone()

        if self._currCmdSeq.didFail():
            # start fail command, if appropriate
            # (there is one and it has not been run)
            if self._logMask & self.LogCommands:
                logFunc = self._logFunc
            else:
                logFunc = None
            failCmdObj = self._currCmdSeq.getFailCmd(callFunc=self._callFunc, logFunc=logFunc)
            if failCmdObj:
                self._startCmdObj(failCmdObj)
            return

        if self._currCmdSeq.isDone():
            return

        cmdObj = self._currCmdSeq.getNextCmd(callFunc=self._callFunc)
        self._startCmdObj(cmdObj)
    
    def _read(self, dumConn=None):
        """Handle data from the I-Step controller
        """
        try:
            reply = self.conn.readLine()
        except Exception, e:
            if not self._currCmd.isDone():
                self._currCmd.setState(self._currCmd.CommErrorState)
            raise
        if not reply:
            return
        
        if self._logMask & self.LogReads:
            self._logFunc(reply + "\n")
        
        if reply == "0>":
            # command succeeded
            if not self._currCmd.isDone():
                self._currCmd.setState(self._currCmd.SuccessState)
            else:
                sys.stderr.write("Warning: ignoring unexpected prompt %r\n" % (reply,))
        elif reply.startswith("0?"):
            # command failed
            if not self._currCmd.isDone():
                errState = int(reply[2:])
                self._currCmd.setState(errState)
            else:
                sys.stderr.write("Warning: ignoring unexpcted error prompt %r\n" % (reply,))
        elif reply.startswith("0BX"):
            # begin move
            if not self._currMove.begObj.isDone():
                moveState = int(reply[3:])
                self._currMove.begObj.setState(moveState)
            else:
                sys.stderr.write("Warning: ignoring unexpected begin message %r\n" % (reply,))
        elif reply.startswith("0EX"):
            # end move
            if not self._currMove.endObj.isDone():
                moveState = int(reply[3:])
                self._currMove.endObj.setState(moveState)
            else:
                sys.stderr.write("Warning: ignoring unexpected end message %r\n" % (reply,))
        else:
            # output data
            if not self._currCmd.isDone():
                self._currCmd.addReply(reply)
            else:
                sys.stderr.write("Warning: ignoring unexpected data %r\n" % (reply,))
    
    def _startCmdObj(self, cmdOrMoveObj):
        """Low-level routine to start a command.
        You must be sure the command may safely be started before calling this.
        """
        if cmdOrMoveObj.isMoveObj():
            cmdObj = cmdOrMoveObj.cmdObj
        else:
            cmdObj = cmdOrMoveObj

        self._currCmd = cmdObj
        if cmdOrMoveObj.isMoveObj():
            self._currMove = cmdOrMoveObj

        cmdStr = cmdObj.getCmdStr()
        if self._logMask & self.LogWrites:
            self._logFunc(cmdStr + "\n")
        try:
            self.conn.writeLine(cmdStr)
        except Exception, e:
            sys.stderr.write("Communication error: %s\n" % (e,))
            self._currCmd.setState(self._currCmd.CommErrorState)
        
        self._doCallbacks()


if __name__ == "__main__":
    import Tkinter
    import RO.Wdg
    import getKeyspanDevice

    KeyspanPort = 1
    
    root = Tkinter.Tk()
    logWdg = RO.Wdg.LogWdg(root)
    logWdg.pack(side="top", expand=True, fill="both")

    logMask = IStepDevice.LogReads | IStepDevice.LogWrites | IStepDevice.LogCommands | IStepDevice.LogSequences
#    logMask = IStepDevice.LogReads | IStepDevice.LogWrites | IStepDevice.LogSequences
    devPath = getKeyspanDevice.getKeyspanDevice(KeyspanPort)
    ctrllr = IStepDevice(devPath, logFunc=logWdg.addOutput, logMask=logMask)

    def startCmd(cmdStr):
        """Start a command
        
        Inputs:
        - cmdStr    command to send to istep (will be forced to uppercase)
        - timeLim   time limit, in seconds; if None then a default is used
                    (which depends on whether the command is a move)
        """
        cmdStr = cmdStr.upper()
        cmdVerbData = cmdStr.split(None, 1)
        if not cmdVerbData:
            return
        cmdVerb = cmdVerbData[0].upper()
        if len(cmdVerbData) > 1:
            cmdData = cmdVerbData[1]
        else:
            cmdData = ""
        if cmdVerb == "HOME":
            ctrllr.home()
        elif cmdVerb == "GETCONFIG":
            ctrllr.getConfig()
        elif cmdVerb == "GETSTATUS":
            ctrllr.getStatus()
        elif cmdVerb == "MOVEABS":
            try:
                pos = int(cmdData)
            except Exception, e:
                logWdg.addOutput("Invalid position %s: %s" % (cmdData, e,))
            ctrllr.moveAbs(pos)
        elif cmdVerb == "MOVEREL":
            try:
                pos = int(cmdData)
            except Exception, e:
                logWdg.addOutput("Invalid position %s: %s" % (cmdData, e,))
            ctrllr.moveRel(pos)
        else:
            ctrllr.startCmd(cmdStr)
        
    cmdWdg = RO.Wdg.CmdWdg(root, startCmd)
    cmdWdg.pack(side="top", expand=True, fill="x")

    root.mainloop()
