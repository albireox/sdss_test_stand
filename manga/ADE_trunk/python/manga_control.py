#! /usr/bin/env python
# file: $RCSfile: manga_control.py,v $
# rcsid: $Id: manga_control.py,v 1.1 2013/04/27 11:58:20 jwp Exp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# manga device control widget
# ********************************************************************

import inspect
import sys
import Tkinter as tk
import datetime

import ConfigParser

import manga_devices as md
from ADEFiberTester import CommonData

def wdump(w):
    """Shows various things about a widget."""
    print "w {}".format(w)
    print "w winfo_class {}".format(w.winfo_class())
    if w.winfo_class() == "Button":
        print "---------->boo!"
    d = w.config()
    if "text" in d:
        print d["text"][4]
    return

def walk(w0):
    """Descends a widget hierarchy and displays widget properties."""
    print "{}:".format(inspect.stack()[0][3])
    wdump(w0)
    for w1 in w0.winfo_children():
        wdump(w1)
        for w2 in w1.winfo_children():
            wdump(w2)
            for w3 in w2.winfo_children():
                wdump(w3)
                for w4 in w3.winfo_children():
                    wdump(w4)
    return

def paint_tree(w0, color):
    """Descends a widget hierarchy and paints the background"""
    #tag = inspect.stack()[0][3]
    #print "{}: w0 {} color {}".format(tag, w0, color)
    w0.config(bg=color)
    w0.config(highlightbackground=color)
    for w1 in w0.winfo_children():
        w1.config(bg=color)
        w1.config(highlightbackground=color)
        for w2 in w1.winfo_children():
            w2.config(bg=color)
            w2.config(highlightbackground=color)
            for w3 in w2.winfo_children():
                w3.config(bg=color)
                w3.config(highlightbackground=color)
                for w4 in w3.winfo_children():
                    w4.config(bg=color)
                    w4.config(highlightbackground=color)
    return

# this has to be here 
# to be able to use the tkinter Variables below.
root = tk.Tk()

root.wm_title("UWisc - Fiber Test Stand Widget Ver. 1.0.0.0.0 (b)")

FRONT = 0
BACK = 1
stages = [("x1", "y1"), ("x2", "y2")]

RAW = 0
FITS = 1

OLD = 0
NEW = 1

# we enumerate the exposure times to avoid floating point representation issues.
etimes = [5, 10, 50, 100, 500, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000]
E0005 = 0
E0010 = 1
E0050 = 2
E0100 = 3
E0500 = 4
E1000 = 5
E2000 = 6
E3000 = 7
E4000 = 8
E5000 = 9
E6000 = 10
E7000 = 11
E8000 = 12
E9000 = 13
E10000 = 14

class App:

    stage = tk.IntVar()
    stage.set(FRONT)
    xstage = tk.StringVar()
    xstage.set("x1")
    ystage = tk.StringVar()
    ystage.set("y1")
    xinvert = tk.IntVar()
    xinvert.set(1)
    yinvert = tk.IntVar()
    yinvert.set(-1)
    jogsize = tk.DoubleVar()
    jogsize.set(0.100)
    x = tk.DoubleVar()
    y = tk.DoubleVar()
    xsave = tk.DoubleVar()
    ysave = tk.DoubleVar()
    xdiff = tk.DoubleVar()
    ydiff = tk.DoubleVar()
    ammeter = tk.DoubleVar()
    dlpselect = tk.IntVar()
    dlpselect.set(NEW)

    # cache the stage settings for when we switch back and forth
    x1cache = 0.0
    y1cache = 0.0
    x1savecache = 0.0
    y1savecache = 0.0
    x1invertcache = 1
    y1invertcache = -1
    x2cache = 0.0
    y2cache = 0.0
    x2savecache = 0.0
    y2savecache = 0.0
    x2invertcache = 1
    y2invertcache = -1

    def diffed(self, *args):
        """Keeps the diff entries up to date."""
        self.xdiff.set(self.x.get() - self.xsave.get())
        self.ydiff.set(self.y.get() - self.ysave.get())

    logfile = tk.StringVar()
    logfile.set("not logging")
    fp = 0

    sendtods9 = tk.IntVar()
    sendtods9.set(1)
    beeper = tk.IntVar()
    beeper.set(0)
    imageformat = tk.IntVar()
    imageformat.set(RAW)
    imagefile = tk.StringVar()
    imagefile.set("N/A")
    etime = tk.IntVar()
    etime.set(E0005)

    def proc_cfg_put(self):

        configfile = "manga_control.cfg"
        config = ConfigParser.ConfigParser()
        config.read(configfile)

        section = "DLP"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "dlpselect", self.dlpselect.get())

        section = "IMAGE"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "sendtods9", self.sendtods9.get())
        config.set(section, "beeper", self.beeper.get())
        config.set(section, "imageformat", self.imageformat.get())
        config.set(section, "etime", self.etime.get())

        section = "CHOICES"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "stage", self.stage.get())
        config.set(section, "jogsize", self.jogsize.get())
        config.set(section, "xinvert", self.xinvert.get())
        config.set(section, "yinvert", self.yinvert.get())

        config.set(section, "x1cache", self.x1cache)
        config.set(section, "y1cache", self.y1cache)
        config.set(section, "x1savecache", self.x1savecache)
        config.set(section, "y1savecache", self.y1savecache)
        config.set(section, "x1invertcache", self.x1invertcache)
        config.set(section, "y1invertcache", self.y1invertcache)
        config.set(section, "x2cache", self.x2cache)
        config.set(section, "y2cache", self.y2cache)
        config.set(section, "x2savecache", self.x2savecache)
        config.set(section, "y2savecache", self.y2savecache)
        config.set(section, "x2invertcache", self.x2invertcache)
        config.set(section, "y2invertcache", self.y2invertcache)

        section = "PADDLE"
        if not config.has_section(section):
            config.add_section(section)

        section = "AMMETER"
        if not config.has_section(section):
            config.add_section(section)

        section = "SESSION"
        if not config.has_section(section):
            config.add_section(section)

        with open(configfile, "wb") as fp:
            config.write(fp)

    def proc_cfg_get(self):
        """Ingests the configuratin file."""

        configfile = "manga_control.cfg"
        config = ConfigParser.ConfigParser()
        config.read(configfile)

        section = "DLP"
        if config.has_section(section):
            i = config.getint(section, "dlpselect")
            self.dlpselect.set(i)

        section = "IMAGE"
        if config.has_section(section):
            i = config.getint(section, "sendtods9")
            self.sendtods9.set(i)
            i = config.getint(section, "beeper")
            self.beeper.set(i)
            i = config.getint(section, "imageformat")
            self.imageformat.set(i)
            i = config.getint(section, "etime")
            self.etime.set(i)

        section = "CHOICES"
        if config.has_section(section):
            i = config.getint(section, "stage")
            self.stage.set(i)
            i = config.getfloat(section, "jogsize")
            self.jogsize.set(i)
            i = config.getint(section, "xinvert")
            self.xinvert.set(i)
            i = config.getint(section, "yinvert")
            self.yinvert.set(i)

            f = config.getfloat(section, "x1cache")
            self.x1cache = f
            f = config.getfloat(section, "y1cache")
            self.y1cache = f
            f = config.getfloat(section, "x1savecache")
            self.x1savecache = f
            f = config.getfloat(section, "y1savecache")
            self.y1savecache = f
            i = config.getint(section, "x1invertcache")
            self.x1invertcache = i
            i = config.getint(section, "y1invertcache")
            self.y1invertcache = i
            f = config.getfloat(section, "x2cache")
            self.x2cache = f
            f = config.getfloat(section, "y2cache")
            self.y2cache = f
            f = config.getfloat(section, "x2savecache")
            self.x2savecache = f
            f = config.getfloat(section, "y2savecache")
            self.y2savecache = f
            i = config.getint(section, "x2invertcache")
            self.x2invertcache = i
            i = config.getint(section, "y2invertcache")
            self.y2invertcache = i

        section = "PADDLE"
        if config.has_section(section):
            pass

        section = "AMMETER"
        if config.has_section(section):
            pass

        section = "SESSION"
        if config.has_section(section):
            pass

    def proc_status(self):
        tag = inspect.stack()[0][3] # our name

        print "{}:   etime {}".format(tag, self.etime.get())
        print "{}: jogsize {}".format(tag, self.jogsize.get())
        print "{}:   stage {}".format(tag, self.stage.get())
        print "{}:  xstage {}".format(tag, self.xstage.get())
        print "{}:  ystage {}".format(tag, self.ystage.get())
        print "{}: xinvert {}".format(tag, self.xinvert.get())
        print "{}: yinvert {}".format(tag, self.yinvert.get())
        print "{}:       x {}".format(tag, self.x.get())
        print "{}:       y {}".format(tag, self.y.get())
        print "{}:   xsave {}".format(tag, self.xsave.get())
        print "{}:   ysave {}".format(tag, self.ysave.get())
        print "{}:   xdiff {}".format(tag, self.xdiff.get())
        print "{}:   ydiff {}".format(tag, self.ydiff.get())
        print "{}:      x1cache {}".format(tag, self.x1cache)
        print "{}:      y1cache {}".format(tag, self.y1cache)
        print "{}:  x1savecache {}".format(tag, self.x1savecache)
        print "{}:  y1savecache {}".format(tag, self.y1savecache)
        print "{}:x1invertcache {}".format(tag, self.x1invertcache)
        print "{}:y1invertcache {}".format(tag, self.y1invertcache)
        print "{}:      x2cache {}".format(tag, self.x2cache)
        print "{}:      y2cache {}".format(tag, self.y2cache)
        print "{}:  x2savecache {}".format(tag, self.x2savecache)
        print "{}:  y2savecache {}".format(tag, self.y2savecache)
        print "{}:x2invertcache {}".format(tag, self.x2invertcache)
        print "{}:y2invertcache {}".format(tag, self.y2invertcache)


        return

    def proc_open(self):
        """Opens a log file; closes an open file if possible."""
        tag = inspect.stack()[0][3] # our name

        # if a logfile is open, close it
        if self.fp != 0:
            self.proc_log("{}: old logfile {}".format(tag, self.logfile.get()))
            try:
                self.fp.close()
            except Exception as e:
                print >> sys.stderr, "{}: Can't close logfile {}".format(tag, self.logfile.get())
                print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
                print >> sys.stderr, "{}: Exception: {}".format(tag, e)

        # get the new file name
        try:
            now = datetime.datetime.now()
            logfile = now.strftime("manga_control.%Y%m%d.%H%M%S.log")
            print "{}: logfile {}".format(tag, logfile)
        except Exception as e:
            print >> sys.stderr, "{}: Can't get filename".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        else:
            try:
                self.fp = open(logfile, "w")
                self.logfile.set(logfile)
                self.proc_log("{}: new logfile {}".format(tag, self.logfile.get()))
            except Exception as e:
                print >> sys.stderr, "{}: Can't open logfile {}".format(tag, self.logfile.get())
                print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
                print >> sys.stderr, "{}: Exception: {}".format(tag, e)
                self.fp = 0

        return

    def proc_log(self, msg):
        """Timestamps a log string and writes it to stdout and the log file."""
        tag = inspect.stack()[0][3] # our name

        now = datetime.datetime.now()
        out = now.ctime()
        m = "{} {}".format(out, msg)
        print m

        try:
            self.fp.write(m + "\n")
        except Exception as e:
            print >> sys.stderr, "{}: Can't write to logfile".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

        return

    def proc_update(self):
        """Queries the motor stages for their current positions."""
        #tag = inspect.stack()[0][3] # our name

        x = md.proc_apt_query(teststand,self.xstage.get())
        self.x.set(x)

        y = md.proc_apt_query(teststand,self.ystage.get())
        self.y.set(y)

        return

    def proc_home(self):
        tag = inspect.stack()[0][3] # our name

        xstage = self.xstage.get()
        ystage = self.ystage.get()

        self.proc_log("{}: {},{} --> home".format(tag, xstage, ystage))

        md.proc_apt_move_home(teststand,xstage)
        md.proc_apt_move_home(teststand,ystage)

        md.proc_apt_move_block(teststand,xstage)
        md.proc_apt_move_block(teststand,ystage)

        self.proc_update()

        return

    def proc_home_all(self):
        
        md.proc_apt_move_home(teststand,'x1')
        md.proc_apt_move_home(teststand,'y1')
        md.proc_apt_move_home(teststand,'x2')
        md.proc_apt_move_home(teststand,'y2')

        md.proc_apt_move_block(teststand,'x1')
        md.proc_apt_move_block(teststand,'y1')
        md.proc_apt_move_block(teststand,'x2')
        md.proc_apt_move_block(teststand,'y2')

        self.proc_update()

        return

    def proc_up(self):
        tag = inspect.stack()[0][3] # our name

        old = self.y.get()
        new = old + (self.yinvert.get() * self.jogsize.get())
        self.y.set(new)

        self.proc_log("{}: {} --> {} mm".format(tag, self.ystage.get(), self.y.get()))

        md.proc_apt_move_absolute(teststand,self.ystage.get(), self.y.get())
        md.proc_apt_move_block(teststand,self.ystage.get())

        #self.proc_update()

        return

    def proc_down(self):
        tag = inspect.stack()[0][3] # our name

        old = self.y.get()
        new = old - (self.yinvert.get() * self.jogsize.get())
        self.y.set(new)

        self.proc_log("{}: {} --> {} mm".format(tag, self.ystage.get(), self.y.get()))

        md.proc_apt_move_absolute(teststand,self.ystage.get(), self.y.get())
        md.proc_apt_move_block(teststand,self.ystage.get())

        #self.proc_update()

        return

    def proc_right(self):
        tag = inspect.stack()[0][3] # our name

        old = self.x.get()
        new = old + (self.xinvert.get() * self.jogsize.get())
        self.x.set(new)

        self.proc_log("{}: {} --> {} mm".format(tag, self.xstage.get(), self.x.get()))

        md.proc_apt_move_absolute(teststand,self.xstage.get(), self.x.get())
        md.proc_apt_move_block(teststand,self.xstage.get())

        #self.proc_update()

        return

    def proc_left(self):
        tag = inspect.stack()[0][3] # our name

        old = self.x.get()
        new = old - (self.xinvert.get() * self.jogsize.get())
        self.x.set(new)

        self.proc_log("{}: {} --> {} mm".format(tag, self.xstage.get(), self.x.get()))

        md.proc_apt_move_absolute(teststand,self.xstage.get(), self.x.get())
        md.proc_apt_move_block(teststand,self.xstage.get())

        #self.proc_update()

        return

    def proc_xscale(self, event):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: {} --> {} mm".format(tag, self.xstage.get(), self.x.get()))

        md.proc_apt_move_absolute(teststand,self.xstage.get(), self.x.get())
        md.proc_apt_move_block(teststand,self.xstage.get())

        #self.proc_update()

        return

    def proc_yscale(self, event):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: {} --> {} mm".format(tag, self.ystage.get(), self.y.get()))

        md.proc_apt_move_absolute(teststand,self.ystage.get(), self.y.get())
        md.proc_apt_move_block(teststand,self.ystage.get())

        #self.proc_update()

        return

    def proc_xentry(self, event):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: {} --> {} mm".format(tag, self.xstage.get(), self.x.get()))

        md.proc_apt_move_absolute(teststand,self.xstage.get(), self.x.get())
        md.proc_apt_move_block(teststand,self.xstage.get())

        return

    def proc_yentry(self, event):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: {} --> {} mm".format(tag, self.ystage.get(), self.y.get()))

        md.proc_apt_move_absolute(teststand,self.ystage.get(), self.y.get())
        md.proc_apt_move_block(teststand,self.ystage.get())

        return

    def proc_save_position(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: xsave, ysave <-- {}, {}".format(tag, self.x.get(), self.y.get()))

        self.xsave.set(self.x.get())
        self.ysave.set(self.y.get())
        if self.stage.get() == FRONT:
            self.x1savecache = self.xsave.get()
            self.y1savecache = self.ysave.get()
        else:
            self.x2savecache = self.xsave.get()
            self.y2savecache = self.ysave.get()

        return

    def proc_restore_position(self):
        tag = inspect.stack()[0][3] # our name

        x = self.xsave.get()
        y = self.ysave.get()

        self.x.set(x)
        self.y.set(y)

        self.proc_log("{}: {} --> {} mm".format(tag, self.xstage.get(), self.x.get()))
        self.proc_log("{}: {} --> {} mm".format(tag, self.ystage.get(), self.y.get()))

        md.proc_apt_move_absolute(teststand,self.xstage.get(), self.x.get())
        md.proc_apt_move_absolute(teststand,self.ystage.get(), self.y.get())

        md.proc_apt_move_block(teststand,self.xstage.get())
        md.proc_apt_move_block(teststand,self.ystage.get())

        #self.proc_update()

        return

    def proc_stage(self, new):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: use stages {}".format(tag, stages[new]))

        if new == FRONT:
            # cache the back end
            self.x2cache = self.x.get()
            self.y2cache = self.y.get()

            # reload the front end
            self.xstage.set(stages[FRONT][0])
            self.ystage.set(stages[FRONT][1])
            self.x.set(self.x1cache)
            self.y.set(self.y1cache)
            self.xsave.set(self.x1savecache)
            self.ysave.set(self.y1savecache)
            self.xinvert.set(self.x1invertcache)
            self.yinvert.set(self.y1invertcache)
        else:
            # cache the front end
            self.x1cache = self.x.get()
            self.y1cache = self.y.get()

            # reload the back end
            self.xstage.set(stages[BACK][0])
            self.ystage.set(stages[BACK][1])
            self.x.set(self.x2cache)
            self.y.set(self.y2cache)
            self.xsave.set(self.x2savecache)
            self.ysave.set(self.y2savecache)
            self.xinvert.set(self.x2invertcache)
            self.yinvert.set(self.y2invertcache)

        self.proc_update()

        return

    def proc_xinvert(self):
        tag = inspect.stack()[0][3] # our name

        if self.stage.get() == FRONT:
            self.x1invertcache = self.xinvert.get()
        else:
            self.x2invertcache = self.xinvert.get()

        self.proc_log("{}: xinvert {}".format(tag, self.xinvert.get()))

        return

    def proc_yinvert(self):
        tag = inspect.stack()[0][3] # our name

        if self.stage.get() == FRONT:
            self.y1invertcache = self.yinvert.get()
        else:
            self.y2invertcache = self.yinvert.get()

        self.proc_log("{}: yinvert {}".format(tag, self.yinvert.get()))

        return

    def proc_jogsize(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: new jogsize {}".format(tag, self.jogsize.get()))

        return

    def proc_ammeter(self):
        tag = inspect.stack()[0][3] # our name

        reading = md.proc_rbd_query()
        self.ammeter.set(reading)
        self.proc_log("{}: ammeter {} nA".format(tag, self.ammeter.get()))

        return

    def proc_mark(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log(
"{}: stage {},{}: pos {:6.2f} {:6.2f} save {:6.2f} {:6.2f} diff {:6.2f} {:6.2f}".format(tag,
                self.xstage.get(),
                self.ystage.get(),
                self.x.get(),
                self.y.get(),
                self.xsave.get(),
                self.ysave.get(),
                self.xdiff.get(),
                self.ydiff.get()))

        return

    def proc_sendtods9(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: sendtods9: {}".format(tag, self.sendtods9.get()))

        if self.sendtods9.get():
            md.proc_gige_loop_cmds('input', "I")
        else:
            md.proc_gige_loop_cmds('input', "Ii")

        return

    def proc_beeper(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: beeper: {}".format(tag, self.beeper.get()))

        if self.beeper.get():
            md.proc_gige_loop_cmds('input', "B")
        else:
            md.proc_gige_loop_cmds('input', "Bb")

        return

    def proc_raw_fits(self):
        tag = inspect.stack()[0][3] # our name

        if self.imageformat.get() == RAW:
            self.proc_log("{}: image format RAW".format(tag))
            md.proc_gige_loop_cmds('input', "r")
        else:
            self.proc_log("{}: image format FITS".format(tag))
            md.proc_gige_loop_cmds('input', "f")

        return

    def proc_etime(self):
        tag = inspect.stack()[0][3] # our name

        etime = self.etime.get()
        t = etimes[etime]

        self.proc_log("{}: new etime {} {} {:.3f} s".format(tag, etime, t, t / 1000.0))

        if etime == E0005:
            md.proc_gige_loop_cmds('input', "0")
        elif etime == E0010:
            md.proc_gige_loop_cmds('input', "1DDDDDDDDDddddddddd")
        elif etime == E0050:
            md.proc_gige_loop_cmds('input', "1DDDDDDDDDddddd")
        elif etime == E0100:
            md.proc_gige_loop_cmds('input', "1DDDDDDDDD")
        elif etime == E0500:
            md.proc_gige_loop_cmds('input', "1DDDDD")
        elif etime == E1000:
            md.proc_gige_loop_cmds('input', "1")
        elif etime == E2000:
            md.proc_gige_loop_cmds('input', "2")
        elif etime == E3000:
            md.proc_gige_loop_cmds('input', "3")
        elif etime == E4000:
            md.proc_gige_loop_cmds('input', "4")
        elif etime == E5000:
            md.proc_gige_loop_cmds('input', "5")
        elif etime == E6000:
            md.proc_gige_loop_cmds('input', "6")
        elif etime == E7000:
            md.proc_gige_loop_cmds('input', "7")
        elif etime == E8000:
            md.proc_gige_loop_cmds('input', "8")
        elif etime == E9000:
            md.proc_gige_loop_cmds('input', "9")
        elif etime == E10000:
            md.proc_gige_loop_cmds('input', "9UUUUUUUUUU")

        return

    def proc_snap(self):
        tag = inspect.stack()[0][3] # our name

        self.proc_log("{}: save image".format(tag))

        filename = md.proc_gige_loop_save_image()
        self.proc_log("{}: saved image: {}".format(tag, filename))
        #self.imagefile.set(filename)

        return

    def proc_quit(self):
        tag = inspect.stack()[0][3] # our name

        #self.proc_cfg_put()

        if self.fp != 0:
            self.proc_log("{}: quit".format(tag))

        quit()

    def back_all_on(self):
        md.proc_dlp("3+")
        md.proc_dlp("4+")
        md.proc_dlp("5+")
        md.proc_dlp("6+")
        return

    def back_all_off(self):
        md.proc_dlp("3-")
        md.proc_dlp("4-")
        md.proc_dlp("5-")
        md.proc_dlp("6-")
        return

    def proc_dlp_select(self, old, new):
        print "{}:".format("proc_dlp_select")
        if self.dlpselect.get() == OLD:
            print "{}:".format("proc_dlp_select old")
            new.pack_forget()
            old.pack(side="top")
        else:
            print "{}:".format("proc_dlp_select new")
            old.pack_forget()
            new.pack(side="top")
        return

    def front_metro(self):
        
        md.proc_apt_move_absolute(teststand,'x1', CommonData.x1MetroPos)
        md.proc_apt_move_absolute(teststand,'y1', CommonData.y1MetroPos)
        md.proc_apt_move_block(teststand,'x1')
        md.proc_apt_move_block(teststand,'y1')
        self.proc_update()
        
    def back_metro(self):
        
        md.proc_apt_move_absolute(teststand,'x2', CommonData.x2MetroPos)
        md.proc_apt_move_absolute(teststand,'y2', CommonData.y2MetroPos)
        md.proc_apt_move_block(teststand,'x2')
        md.proc_apt_move_block(teststand,'y2')
        self.proc_update()

    def pp_metro(self):
        
        md.proc_apt_move_absolute(teststand,'x1', CommonData.x1PPMetroPos)
        md.proc_apt_move_absolute(teststand,'y1', CommonData.y1PPMetroPos)
        md.proc_apt_move_block(teststand,'x1')
        md.proc_apt_move_block(teststand,'y1')
        self.proc_update()

    def spot_cal(self):
        
        md.proc_apt_move_absolute(teststand,'x1', CommonData.x1MetroPos + CommonData.x1Offset)
        md.proc_apt_move_absolute(teststand,'y1', CommonData.y1MetroPos + CommonData.y1Offset)
        md.proc_apt_move_block(teststand,'x1')
        md.proc_apt_move_block(teststand,'y1')
        self.proc_update()

    def pp_spot(self):
        
        md.proc_apt_move_absolute(teststand,'x1', CommonData.x1PPMetroPos + CommonData.x1PPOffset)
        md.proc_apt_move_absolute(teststand,'y1', CommonData.y1PPMetroPos + CommonData.y1PPOffset)
        md.proc_apt_move_block(teststand,'x1')
        md.proc_apt_move_block(teststand,'y1')
        self.proc_update()

    def back_start(self):
        
        md.proc_apt_move_absolute(teststand,'x2', CommonData.startSearchPos)
        md.proc_apt_move_absolute(teststand,'y2', CommonData.y2StartPos)
        md.proc_apt_move_block(teststand,'x2')
        md.proc_apt_move_block(teststand,'y2')
        self.proc_update()

    def __init__(self, master):

        apt = tk.Frame(master)

        apt_paddle = tk.Frame(apt)
        apt_paddle_update = tk.Button(apt_paddle, text="Update", command=self.proc_update)
        apt_paddle_home = tk.Button(apt_paddle, text="Home", command=self.proc_home)
        apt_paddle_home_all = tk.Button(apt_paddle, text='Home All',command=self.proc_home_all)
        apt_paddle_up = tk.Button(apt_paddle, text="Up", command=self.proc_up)
        apt_paddle_down = tk.Button(apt_paddle, text="Down", command=self.proc_down)
        apt_paddle_left = tk.Button(apt_paddle, text="Left", command=self.proc_left)
        apt_paddle_right = tk.Button(apt_paddle, text="Right", command=self.proc_right)
        apt_paddle_x = tk.Scale(apt_paddle,
            label="X", orient="horizontal",
            from_=0, to=50, resolution=0.1,
            variable=self.x)
        apt_paddle_y = tk.Scale(apt_paddle,
            label="Y", orient="vertical",
            from_=0, to=25, resolution=0.1,
            variable=self.y)
        apt_paddle_x.bind("<ButtonRelease-1>", self.proc_xscale)
        apt_paddle_y.bind("<ButtonRelease-1>", self.proc_yscale)
        apt_paddle_y.grid(column=0, row=0, columnspan=1, rowspan=3)
        apt_paddle_x.grid(column=1, row=3, columnspan=3, rowspan=1)
        apt_paddle_up.grid(column=2, row=0, columnspan=1, rowspan=1)
        apt_paddle_down.grid(column=2, row=2, columnspan=1, rowspan=1)
        apt_paddle_left.grid(column=1, row=1, columnspan=1, rowspan=1)
        apt_paddle_right.grid(column=3, row=1, columnspan=1, rowspan=1)
        apt_paddle_home.grid(column=0, row=3, columnspan=1, rowspan=1)
        apt_paddle_home_all.grid(column=0, row=4, columnspan=1, rowspan=1)
        apt_paddle_update.grid(column=2, row=1, columnspan=1, rowspan=1)

        apt_choices = tk.Frame(apt)
        apt_choices_stages = tk.Frame(apt_choices)
        apt_choices_stages_front = tk.Radiobutton(apt_choices_stages,
            text="Front End",
            variable=self.stage,
            value=FRONT,
            command=(lambda: self.proc_stage(FRONT)))
        apt_choices_stages_back = tk.Radiobutton(apt_choices_stages,
            text="Back End",
            variable=self.stage,
            value=BACK,
            command=(lambda: self.proc_stage(BACK)))
        apt_choices_stages_front.pack(side="top", anchor=tk.W)
        apt_choices_stages_back.pack(side="top", anchor=tk.W)
        apt_choices_flips = tk.Frame(apt_choices)
        apt_choices_flips_xinvert = tk.Checkbutton(apt_choices_flips,
            text="Invert X",
            variable=self.xinvert,
            offvalue=1, onvalue= -1,
            command=self.proc_xinvert)
        apt_choices_flips_yinvert = tk.Checkbutton(apt_choices_flips,
            text="Invert Y",
            variable=self.yinvert, offvalue=1, onvalue= -1,
            command=self.proc_yinvert)
        apt_choices_flips_xinvert.pack(side="top")
        apt_choices_flips_yinvert.pack(side="top")

        apt_choices_entries = tk.Frame(apt_choices)
        apt_choices_entries_xentrylabel = tk.Label(apt_choices_entries, text="X")
        apt_choices_entries_yentrylabel = tk.Label(apt_choices_entries, text="Y")
        apt_choices_entries_xentry = tk.Entry(apt_choices_entries, width=8, textvar=self.x)
        apt_choices_entries_yentry = tk.Entry(apt_choices_entries, width=8, textvar=self.y)
        apt_choices_entries_save = tk.Button(apt_choices_entries, text="Remember", command=self.proc_save_position)
        apt_choices_entries_xentry.bind("<Return>", self.proc_xentry)
        apt_choices_entries_yentry.bind("<Return>", self.proc_yentry)

        apt_choices_entries_xsavelabel = tk.Label(apt_choices_entries, text="X-Save")
        apt_choices_entries_ysavelabel = tk.Label(apt_choices_entries, text="Y-Save")
        apt_choices_entries_xsave = tk.Entry(apt_choices_entries, width=8, textvar=self.xsave)
        apt_choices_entries_ysave = tk.Entry(apt_choices_entries, width=8, textvar=self.ysave)
        apt_choices_entries_restore = tk.Button(apt_choices_entries, text="Go Back", command=self.proc_restore_position)

        apt_choices_entries_xdifflabel = tk.Label(apt_choices_entries, text="X-Diff")
        apt_choices_entries_ydifflabel = tk.Label(apt_choices_entries, text="Y-Diff")
        apt_choices_entries_xdiff = tk.Entry(apt_choices_entries, width=8, textvar=self.xdiff)
        apt_choices_entries_ydiff = tk.Entry(apt_choices_entries, width=8, textvar=self.ydiff)
        apt_choices_entries_mark = tk.Button(apt_choices_entries, text="Log Position", command=self.proc_mark)
        
        apt_choices_entries_frontmetro = tk.Button(apt_choices_entries, text="Front Metro", command=self.front_metro)
        apt_choices_entries_backmetro = tk.Button(apt_choices_entries, text="Back Metro", command=self.back_metro)
        apt_choices_entries_spotcal = tk.Button(apt_choices_entries, text="Spot cal", command=self.spot_cal)
        apt_choices_entries_backstart = tk.Button(apt_choices_entries, text="Back starting", command=self.back_start)
        apt_choices_entries_ppmetro = tk.Button(apt_choices_entries, text="PP Front Metro", command=self.pp_metro)
        apt_choices_entries_ppspot = tk.Button(apt_choices_entries, text="PP Spot", command=self.pp_spot)

        apt_choices_entries_xentrylabel.grid(column=0, row=0)
        apt_choices_entries_xentry.grid(column=1, row=0)
        apt_choices_entries_yentrylabel.grid(column=0, row=1)
        apt_choices_entries_yentry.grid(column=1, row=1)
        apt_choices_entries_xsavelabel.grid(column=0, row=2)
        apt_choices_entries_xsave.grid(column=1, row=2)
        apt_choices_entries_ysavelabel.grid(column=0, row=3)
        apt_choices_entries_ysave.grid(column=1, row=3)
        apt_choices_entries_xdifflabel.grid(column=0, row=4)
        apt_choices_entries_xdiff.grid(column=1, row=4)
        apt_choices_entries_ydifflabel.grid(column=0, row=5)
        apt_choices_entries_ydiff.grid(column=1, row=5)

        apt_choices_entries_save.grid(column=2, row=0, rowspan=2, sticky=tk.NSEW)
        apt_choices_entries_restore.grid(column=2, row=2, rowspan=2, sticky=tk.NSEW)
        apt_choices_entries_mark.grid(column=2, row=4, rowspan=2, sticky=tk.NSEW)
        apt_choices_entries_frontmetro.grid(column=1,row=6,sticky='w')
        apt_choices_entries_backmetro.grid(column=2,row=6,sticky='w')
        apt_choices_entries_spotcal.grid(column=1,row=7,sticky='w')
        apt_choices_entries_backstart.grid(column=2,row=7,sticky='w')
        apt_choices_entries_ppmetro.grid(column=1, row=8, sticky='w')
        apt_choices_entries_ppspot.grid(column=2, row=8, sticky='w')

        apt_choices_jogs = tk.Frame(apt_choices)
        apt_choices_jogs_title = tk.Label(apt_choices_jogs, text="Jog Size")
        apt_choices_jogs_e0 = tk.Radiobutton(apt_choices_jogs,
            text="0.005 mm", variable=self.jogsize, value=0.005, command=self.proc_jogsize)
        apt_choices_jogs_e1 = tk.Radiobutton(apt_choices_jogs,
            text="0.010 mm", variable=self.jogsize, value=0.010, command=self.proc_jogsize)
        apt_choices_jogs_e2 = tk.Radiobutton(apt_choices_jogs,
            text="0.050 mm", variable=self.jogsize, value=0.050, command=self.proc_jogsize)
        apt_choices_jogs_e3 = tk.Radiobutton(apt_choices_jogs,
            text="0.100 mm", variable=self.jogsize, value=0.100, command=self.proc_jogsize)
        apt_choices_jogs_e4 = tk.Radiobutton(apt_choices_jogs,
            text="0.500 mm", variable=self.jogsize, value=0.500, command=self.proc_jogsize)
        apt_choices_jogs_e5 = tk.Radiobutton(apt_choices_jogs,
            text="1.000 mm", variable=self.jogsize, value=1.000, command=self.proc_jogsize)
        apt_choices_jogs_e6 = tk.Radiobutton(apt_choices_jogs,
            text="4.000 mm", variable=self.jogsize, value=4.000, command=self.proc_jogsize)
        apt_choices_jogs_title.pack(side="top")
        apt_choices_jogs_e0.pack(side="top")
        apt_choices_jogs_e1.pack(side="top")
        apt_choices_jogs_e2.pack(side="top")
        apt_choices_jogs_e3.pack(side="top")
        apt_choices_jogs_e4.pack(side="top")
        apt_choices_jogs_e5.pack(side="top")
        apt_choices_jogs_e6.pack(side="top")

        apt_choices_jogs.pack(side="right", expand="true", fill="both")
        apt_choices_entries.pack(side="bottom", expand="true", fill="both")
        apt_choices_stages.pack(side="left", expand="true", fill="both")
        apt_choices_flips.pack(side="left", expand="true", fill="both")

        apt_dlp = tk.Frame(apt)
        apt_dlp_dlp1 = tk.Frame(apt_dlp)
        apt_dlp_dlp2 = tk.Frame(apt_dlp)

        apt_dlp_select = tk.Frame(apt_dlp)
        cmd = (lambda: self.proc_dlp_select(apt_dlp_dlp1, apt_dlp_dlp2))
        apt_dlp_select_old = tk.Radiobutton(
            apt_dlp_select, text="Old", variable=self.dlpselect, value=OLD, command=cmd)
        apt_dlp_select_new = tk.Radiobutton(
            apt_dlp_select, text="New", variable=self.dlpselect, value=NEW, command=cmd)
        apt_dlp_select_old.pack(side="left")
        apt_dlp_select_new.pack(side="left")

        for i in range(1, 9):
            #print i
            f = tk.Frame(apt_dlp_dlp1)
            l = tk.Label(f, text="LED {}".format(i))
            cmd = lambda j = i: md.proc_dlp("{}+".format(j))
            b1 = tk.Button(f, text=" On", command=cmd)
            cmd = lambda j = i: md.proc_dlp("{}-".format(j))
            b2 = tk.Button(f, text="Off", command=cmd)
            l.pack(side="left")
            b1.pack(side="left")
            b2.pack(side="left")
            f.pack(side="top")
        f = tk.Frame(apt_dlp_dlp1)
        l = tk.Label(f, text="     All")
        b1 = tk.Button(f, text=" On", command=(lambda: md.proc_dlp("all+")))
        b2 = tk.Button(f, text="Off", command=(lambda: md.proc_dlp("all-")))
        b2.pack(side="right")
        b1.pack(side="right")
        l.pack(side="right")
        f.pack(side="top")

        f = tk.Frame(apt_dlp_dlp2)
        l = tk.Label(f, text="Front LEDs")
        b1 = tk.Button(f, text=" On", command=(lambda: md.proc_dlp("1+")))
        b2 = tk.Button(f, text="Off", command=(lambda: md.proc_dlp("1-")))
        l.pack(side="left")
        b1.pack(side="left")
        b2.pack(side="left")
        f.pack(side="top")

        f = tk.Frame(apt_dlp_dlp2)
        l = tk.Label(f, text="Brightness")
        b1 = tk.Button(f, text=" Hi", command=(lambda: md.proc_dlp("2+")))
        b2 = tk.Button(f, text=" Lo", command=(lambda: md.proc_dlp("2-")))
        l.pack(side="left")
        b1.pack(side="left")
        b2.pack(side="left")
        f.pack(side="top")

        f = tk.Frame(apt_dlp_dlp2)
        l = tk.Label(f, text=" Back LEDs")
        b1 = tk.Button(f, text=" On", command=self.back_all_on)
        b2 = tk.Button(f, text="Off", command=self.back_all_off)
        l.pack(side="left")
        b1.pack(side="left")
        b2.pack(side="left")
        f.pack(side="top")

        f = tk.Frame(apt_dlp_dlp2)
        l = tk.Label(f, text="Brightness")
        b1 = tk.Button(f, text=" Hi", command=(lambda: md.proc_dlp("7+")))
        b2 = tk.Button(f, text=" Lo", command=(lambda: md.proc_dlp("7-")))
        l.pack(side="left")
        b1.pack(side="left")
        b2.pack(side="left")
        f.pack(side="top")

        for i in range(1, 5):
            #print i
            f = tk.Frame(apt_dlp_dlp2)
            l = tk.Label(f, text="Back LED {}".format(i))
            cmd = lambda j = i + 2: md.proc_dlp("{}+".format(j))
            b1 = tk.Button(f, text=" On", command=cmd)
            cmd = lambda j = i + 2: md.proc_dlp("{}-".format(j))
            b2 = tk.Button(f, text="Off", command=cmd)
            l.pack(side="left")
            b1.pack(side="left")
            b2.pack(side="left")
            f.pack(side="top")

        f = tk.Frame(apt_dlp_dlp2)
        l = tk.Label(f, text="       Shutter")
        b1 = tk.Button(f, text=" In", command=(lambda: md.proc_dlp("8+")))
        b2 = tk.Button(f, text=" Out", command=(lambda: md.proc_dlp("8-")))
        l.pack(side="left")
        b1.pack(side="left")
        b2.pack(side="left")
        f.pack(side="top")

        apt_dlp_select.pack(side="top")
        apt_dlp_dlp2.pack(side="top", expand="true", fill="both")

        apt_image = tk.Frame(apt)
        apt_image_sendtods9 = tk.Checkbutton(apt_image,
            text="Connect to DS9",
            variable=self.sendtods9, offvalue=0, onvalue=1,
            command=self.proc_sendtods9)
        apt_image_beeper = tk.Checkbutton(apt_image,
            text="Beep When Saving",
            variable=self.beeper, offvalue=0, onvalue=1,
            command=self.proc_beeper)

        apt_image_format = tk.Frame(apt_image)
        apt_image_format_raw = tk.Radiobutton(apt_image_format,
            text="Raw", variable=self.imageformat, value=RAW, command=self.proc_raw_fits)
        apt_image_format_fits = tk.Radiobutton(apt_image_format,
            text="FITS", variable=self.imageformat, value=FITS, command=self.proc_raw_fits)
        apt_image_format_snap = tk.Button(apt_image_format, text="Snap", command=self.proc_snap)
        apt_image_format_file = tk.Frame(apt_image_format)
        apt_image_format_file_label = tk.Label(apt_image_format_file, text="Save: ")
        apt_image_format_file_value = tk.Label(apt_image_format_file, text="N/A", textvariable=self.imagefile)
        apt_image_format_file_label.pack(side="left")
        apt_image_format_file_value.pack(side="left")

        apt_image_format_file.pack(side="bottom")
        apt_image_format_fits.pack(side="left")
        apt_image_format_raw.pack(side="left")
        apt_image_format_snap.pack(side="left")

        apt_image_etime = tk.Frame(apt_image)

        apt_image_etime_0005 = tk.Radiobutton(apt_image_etime,
            text="  5 ms", value=E0005, variable=self.etime, command=self.proc_etime)
        apt_image_etime_0010 = tk.Radiobutton(apt_image_etime,
            text=" 10 ms", value=E0010, variable=self.etime, command=self.proc_etime)
        apt_image_etime_0050 = tk.Radiobutton(apt_image_etime,
            text=" 50 ms", value=E0050, variable=self.etime, command=self.proc_etime)
        apt_image_etime_0100 = tk.Radiobutton(apt_image_etime,
            text="100 ms", value=E0100, variable=self.etime, command=self.proc_etime)
        apt_image_etime_0500 = tk.Radiobutton(apt_image_etime,
            text="500 ms", value=E0500, variable=self.etime, command=self.proc_etime)

        apt_image_etime_1000 = tk.Radiobutton(apt_image_etime,
            text="1 s", value=E1000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_2000 = tk.Radiobutton(apt_image_etime,
            text="2 s", value=E2000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_3000 = tk.Radiobutton(apt_image_etime,
            text="3 s", value=E3000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_4000 = tk.Radiobutton(apt_image_etime,
            text="4 s", value=E4000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_5000 = tk.Radiobutton(apt_image_etime,
            text="5 s", value=E5000, variable=self.etime, command=self.proc_etime)

        apt_image_etime_6000 = tk.Radiobutton(apt_image_etime,
            text=" 6 s", value=E6000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_7000 = tk.Radiobutton(apt_image_etime,
            text=" 7 s", value=E7000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_8000 = tk.Radiobutton(apt_image_etime,
            text=" 8 s", value=E8000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_9000 = tk.Radiobutton(apt_image_etime,
            text=" 9 s", value=E9000, variable=self.etime, command=self.proc_etime)
        apt_image_etime_10000 = tk.Radiobutton(apt_image_etime,
            text="10 s", value=E10000, variable=self.etime, command=self.proc_etime)

        apt_image_etime_0005.grid(column=0, row=0, sticky=tk.W)
        apt_image_etime_0010.grid(column=0, row=1, sticky=tk.W)
        apt_image_etime_0050.grid(column=0, row=2, sticky=tk.W)
        apt_image_etime_0100.grid(column=0, row=3, sticky=tk.W)
        apt_image_etime_0500.grid(column=0, row=4, sticky=tk.W)
        apt_image_etime_1000.grid(column=1, row=0, sticky=tk.W)
        apt_image_etime_2000.grid(column=1, row=1, sticky=tk.W)
        apt_image_etime_3000.grid(column=1, row=2, sticky=tk.W)
        apt_image_etime_4000.grid(column=1, row=3, sticky=tk.W)
        apt_image_etime_5000.grid(column=1, row=4, sticky=tk.W)
        apt_image_etime_6000.grid(column=2, row=0, sticky=tk.W)
        apt_image_etime_7000.grid(column=2, row=1, sticky=tk.W)
        apt_image_etime_8000.grid(column=2, row=2, sticky=tk.W)
        apt_image_etime_9000.grid(column=2, row=3, sticky=tk.W)
        apt_image_etime_10000.grid(column=2, row=4, sticky=tk.W)

        apt_image_sendtods9.pack(side="top", anchor="w")
        apt_image_beeper.pack(side="top", anchor="w")
        apt_image_format.pack(side="top")
        apt_image_etime.pack(side="top")

        apt_ammeter = tk.Frame(apt)
        apt_ammeter_button = tk.Button(apt_ammeter,
            text="Measure Current", command=self.proc_ammeter)
        apt_ammeter_label = tk.Label(apt_ammeter, text="Ammeter: ")
        apt_ammeter_value = tk.Label(apt_ammeter, text="N/A", textvariable=self.ammeter)
        apt_ammeter_units = tk.Label(apt_ammeter, text=" nA")
        apt_ammeter_button.pack(side="left", expand="true", fill="x")
        apt_ammeter_label.pack(side="left", expand="true", fill="x")
        apt_ammeter_value.pack(side="left", expand="true", fill="x")
        apt_ammeter_units.pack(side="left", expand="false", fill="x")

        apt_session = tk.Frame(apt)
        apt_session_newlog = tk.Button(apt_session, text="New Log File", command=self.proc_open)
        apt_session_logfilelabel = tk.Label(apt_session, text="Log File: ")
        apt_session_logfilevalue = tk.Label(apt_session, textvariable=self.logfile)
        apt_session_quit = tk.Button(apt_session, text="Quit", command=self.proc_quit)
        apt_session_newlog.pack(side="left")
        apt_session_logfilelabel.pack(side="left")
        apt_session_logfilevalue.pack(side="left")
        apt_session_quit.pack(side="right")

#        apt_session.pack(side="bottom", expand="true", fill="both")
#        apt_dlp.pack(side="left", expand="true", fill="both")
#        apt_choices.pack(side="top", expand="true", fill="both")
#        apt_paddle.pack(side="top", expand="true", fill="both")
#        apt_ammeter.pack(side="top", expand="true", fill="both")
#        apt_image.pack(side="top", expand="true", fill="both")

        apt_dlp.grid(column=0, row=0, sticky=tk.NSEW)
        apt_image.grid(column=0, row=1, rowspan=2, sticky=tk.NSEW)
        apt_choices.grid(column=1, row=0, sticky=tk.NSEW)
        apt_paddle.grid(column=1, row=1, sticky=tk.NSEW)
        apt_ammeter.grid(column=1, row=2, sticky=tk.NSEW)
        apt_session.grid(column=0, row=3, columnspan=2, sticky=tk.NSEW)

        apt.pack(expand="true", fill="both")

        #walk(apt)

        paint_tree(apt, "white")
        paint_tree(apt_dlp, "lemonchiffon")
        paint_tree(apt_choices, "honeydew")
        paint_tree(apt_paddle, "lavender")
        paint_tree(apt_ammeter, "thistle")
        paint_tree(apt_session, "pink")
        paint_tree(apt_image, "lightcyan")

        self.x.trace("w", self.diffed)
        self.y.trace("w", self.diffed)
        self.xsave.trace("w", self.diffed)
        self.ysave.trace("w", self.diffed)
        
        self.proc_cfg_get()

        #self.proc_open()
        #self.proc_sendtods9()
        self.proc_beeper()
        #self.proc_etime()
        self.proc_stage(self.stage.get())
        self.proc_update()
        self.proc_status()

if __name__ == '__main__':
    
    try:
        teststand = sys.argv[1]
    except IndexError:
        teststand = CommonData.teststand
    if teststand != 'ts1' and teststand != 'ts2':
        print "The request was made but it was not good"
        sys.exit(1)
    app = App(root)

    root.mainloop()
