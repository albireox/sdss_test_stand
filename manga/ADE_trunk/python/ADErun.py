#!/usr/bin/env python
"""Fiber tester

To Do:
- Parse picoammeter data as a float
- Initialize picoammeter
- Support remapping linear stage

Questions:
- Do I have to restrict the fit to points near the peak? It would probably be safer.

History:
2008-05-27 ROwen    Incomplete
"""
import re
import sys
import RO.Wdg
#import pychecker.checker # uncomment to run pychecker
import ADEFiberTester

def main():
    import Tkinter
    
    root = Tkinter.Tk()
    root.wm_title('UWash - Fiber tester master control')

    try:
        root.tk.call('console','hide')
    except Tkinter.TclError:
        pass
    
    scriptWdg = RO.Wdg.ScriptModuleWdg(
        master = root,
        module = ADEFiberTester,
        dispatcher = None,
    )
    scriptWdg.pack(expand=True, fill="both")

    root.mainloop()

if __name__ == "__main__":
    sys.exit(main())

