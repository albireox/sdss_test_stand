#!/usr/bin/env python
"""Fiber tester

History:
2008-05-30 ROwen    Version 1
2008-05-30 ROwen    Version 1.1; add CommonData; handle broken fibers better.
2008-06-02 ROwen    Version 1.1.1: increased range a bit; center after each fiber.
2008-06-26 ROwen    Version 1.1.2: changed log file directory to allow bundling as an Mac application.
2008-06-27 ROwen    Version 1.2:
                    - Improved algorithm for deciding where to move the linear stage.
                    - Improved detecting <return> key.
                    - Added support for Command-Q to quit (with verification if running).
                    - Log window now grows when window is resized.
2008-08-01 ROwen    Version 1.2.1: increased ammeter sensitivity by 10x due to new smaller source fiber.                    
2008-08-06 ROwen    Version 1.3:
                    - Added voice prompts.
                    - Lowered threshold for finding fiber.
					- List minimum throughput at the end.
					- Improved FiberData to handle broken fibers gracefully.
					- List v-groove number in fiber throughput (Thru) data.
					- Cache motor position so we don't have to enable the motor if making a null move.
					- Fixed bug whereby broken fibers were not shown at the end.
					- Fixed bug whereby instructional widget stayed red once it went red.
2008-12-11 ROwen    Version 1.4: added test for fibers being in order.
2008-01-23 ROwen    Version 1.5: improved handling of communication errors.
2009-03-23 ROwen    Version 1.5.1:
                    - Updated FiberSep from 0.240 to 0.260 for BOSS production fibers.
                    - Added lower limit for source current as a sanity check, to avoid odd behavior
                      from a disconnected BNC cable or a burned out light bulb
                    - Increased YRangeSteps from 30,000 to 39,000 since the new v-groove blocks are wider.
2009-06-11 ROwen    Version 1.5.2: reduce minimum source current to 20.0.
2010-05-11 ROwen    Version 1.5.4: If beam angle is so far off that no current was measured at desired
                    beam center, set measured throughput to nan instead of raising an exception.
2010-05-11 ROwen    Version 1.5.5: If beam angle is too far off, set throughput to 0, not nan.
2010-07-30 ROwen    Version 1.5.6: Updated some parameters:
                    Changed SlitRadius from 1051.6 to 1052.41 (APOGEE final figure).
                    Corrected FiberApDist from 75.181 to 78.32 (it was wrong).
2010-08-04 ROwen    Version 1.5.7: Corrected FiberApDist from 28.32 to 80.86 (I had left out the thickness
                    of the stop) and improved documentation of the value.
"""
import bisect
import math
import os.path
import SimpleDialog
import sys
import time

import macspeechX
import numpy
import RO.Constants
import RO.OS
import RO.PhysConst
import RO.ScriptRunner
import RO.Wdg
#import pychecker.checker # uncomment to run pychecker

# jwp
import subprocess

__version__ = "1.5.7"

class CommonData(object):
    """Information about the system and whole harness.
    """
    # constants
	# jwp
    NumFibers = 19      # number of fibers in a harness
    YRangeSteps = 39000 # range of travel of stage while measuring a fiber, in microsteps
    NumMeas = 20        # number of measurements per fiber
    BigStepSize = 2.0 # size of step while searching for fiber
    SmallStepSize = 0.5 # size of step while measuring profile
    StepsPerMM = -1968.5 # scale of linear stage, in microsteps/mm

    FiberApDist = 2.54 + 1.0 + 77.32 # distance between end of v-groove block and precision aperture inside detector, in mm
        # the stop at the front of the stage is 0.1" = 2.54 mm thick
        # the gap between the stage and the detector is 1.0 mm
        # the precision aperture is 77.32 mm from the front of the detector
    # FiberSep = 0.350        # distance between adjacent fibers in v-groove block, in mm
    # SlitRadius = 1052.41    # radius of slit, in mm
    # FiberSep = 0.240        # distance between adjacent fibers in v-groove block, in mm
	# jwp
    FiberSep = 0.204        # distance between adjacent fibers in v-groove block, in mm
    SlitRadius = 640.1    # radius of slit, in mm
    DeltaAngle = FiberSep / SlitRadius  # angular separation between fibers, in radians
    
    BeamEdgeFrac = 0.8  # fractional drop from max for edge of beam
    
    MinMeanThroughput = 90.0    # minimum throughput averaged for all fibers, in %
    MinSingleThroughput = 87.0  # minimum throughput for any one fiber, in %
    MaxFracSourceChange = 0.005   # maximum reasonable fractional change in source current
    MinSourceCurrent = 10.0     # mininum source current in nanoamps; be generous, because
        # this is mainly used to defend against a disconnected cable or burned out light bulb

    LogFileDir = os.path.join(RO.OS.getHomeDir(), "Desktop", "FiberMeas")

    def __init__(self):
        self.harnessID = None
        self.sourceCurrentList = []
        self.meanSourceCurrent = numpy.nan
        self.meanYPeak = numpy.nan
    
    def setharnessID(self, harnessID):
        self.harnessID = harnessID
    
    def addSourceMeas(self, sourceCurrent):
        """Add a source current measurement and update mean source current accordingly.
        """
        self.sourceCurrentList.append(float(sourceCurrent))
        self.meanSourceCurrent = numpy.mean(self.sourceCurrentList)

    def setMeanYPeak(self, meanYPeak):
        """Set mean Y peak position for all fibers"""
        self.meanYPeak = float(meanYPeak)
    
    def getFracSourceChange(self):
        """Get fractional change in source intensity from start to finish.
        
        Returns (last_meas - first_meas) / average_meas
        
        Note that this pretty much assumes there were only two source measurements;
        otherwise the returned value doesn't make much sense.
        """
        return (self.sourceCurrentList[-1] - self.sourceCurrentList[0]) / self.meanSourceCurrent


class FiberData(object):
    """Information about one fiber in a harness.
    
    Units:
    - position is in mm
    - current is in nanoamps
    - angle is in radians
    """
    def __init__(self, num, commonData):
        self.num = int(num)
        self.commonData = commonData
        self.measPosCurrList = [] # list of (position, current)
        self.fitBeamBegPos = numpy.nan  # fit stage position at which end of beam is measured
        self.fitBeamEndPos = numpy.nan  # fit stage position at which start of beam is measured
        self.fitBeamCtrPos = numpy.nan  # fit stage position at which beam is allegedly centered (mean of beg and end)
        self.desBeamCtrPos = numpy.nan  # desired stage position at which beam is centered
        self.desAngle = numpy.nan
        self.fiberApDist = numpy.nan
        self.vgNum = numpy.nan
        self.maxCurr = numpy.nan
        self.isBroken = False
    
    def addMeas(self, pos, curr):
        """Add a measurement of current vs position.
        
        Note: the data is re-sorted by increasing position after every add
        """
        self.measPosCurrList.append((float(pos), float(curr)))
        self.measPosCurrList.sort()
        if not (curr < self.maxCurr): # use not < because max starts out nan
            self.maxCurr = curr
    
    def computeBeamWidth(self):
        """Compute beam width, in degrees"""
        return math.atan((self.fitBeamEndPos - self.fitBeamBegPos) / self.commonData.FiberApDist) / RO.PhysConst.RadPerDeg

    def computeFitCurr(self, pos):
        """Compute current at a specified y position, using linear interpolation.
        
        Return 0.0 if fiber is broken.
        """
        if self.isBroken:
            return 0.0
        posList, currList = zip(*self.measPosCurrList)
        endInd = bisect.bisect_left(posList, pos)
        if endInd <= 0 or endInd >= len(posList):
            raise RuntimeError("pos=%s out of range [%s, %s]" % (pos, posList[0], posList[-1]))
        begInd = endInd - 1
        
        slope = (currList[endInd] - currList[begInd]) / (posList[endInd] - posList[begInd])
        retVal = currList[begInd] + (slope * (pos - posList[begInd]))
        return retVal
    
    def computeThroughput(self, pos):
        """Compute throughput (in %) at a specified y position.
        """
        fitCurr = self.computeFitCurr(pos)
        return fitCurr * 100.0 / self.commonData.meanSourceCurrent
    
    def computeAngle(self, pos):
        """Compute angle from v-groove to detector for a given pos, in degrees.
        setVGBlockInfo must have been called.
        
        Returns nan if fiber is broken.
        
        Inputs:
        - pos  y position, in mm
        """
        if self.isBroken:
            return numpy.nan

        return math.atan((pos - self.commonData.meanYPeak) / self.commonData.FiberApDist) / RO.PhysConst.RadPerDeg
    
    def fitBeam(self):
        """Find edges of beam (and center = average of edge positions).
        
        If there is no data then sets isBroken True.
        
        The data looks like a plateau with fairly sharp cutoffs.
        """
        if len(self.measPosCurrList) < 3:
            raise RO.ScriptRunner.ScriptError("Too few measurements to fit")

        posList, currList = zip(*self.measPosCurrList)
        maxCurr = numpy.max(currList)
        if maxCurr < (self.commonData.meanSourceCurrent * 0.1):
            # throughput is less than 10%; fiber is essentially broken
            # use the edges as the peak: it's as good as anything and obviously bad
            self.fitBeamBegPos = posList[0]
            self.fitBeamEndPos = posList[-1]
        else:        
            threshCurr = self.commonData.BeamEdgeFrac * maxCurr
            if currList[0] >= threshCurr:
                raise RO.ScriptRunner.ScriptError("Did not find left side of peak")
            foundBeg = False
            foundEnd = False
            for ind, curr in enumerate(currList):
                if not foundBeg:
                    if curr >= threshCurr:
                        foundBeg = True
                        self.fitBeamBegPos = self.interpolatePosFromCurrent(threshCurr, ind)
                        #print "beg ind=%s; self.fitBeamBegPos=%s" % (ind, self.fitBeamBegPos)
                elif curr < threshCurr:
                    foundEnd = True
                    self.fitBeamEndPos = self.interpolatePosFromCurrent(threshCurr, ind)
                    #print "end ind=%s; self.fitBeamEndPos=%s" % (ind, self.fitBeamEndPos)
                    break
            if not foundEnd:
                raise RO.ScriptRunner.ScriptError("Did not find right side of peak")

        self.fitBeamCtrPos = (self.fitBeamBegPos + self.fitBeamEndPos) / 2.0
        #print "fitBeamBegPos=%s; fitBeamEndPos=%s; fitBeamCtrPos=%0.1f" % (self.fitBeamBegPos, self.fitBeamEndPos, self.fitBeamCtrPos)

        self.isBroken = False

    def getVGNumSortKey(self):
        """Return a value for sorting by vg number"""
        return -self.fitBeamCtrPos
        
    def interpolatePosFromCurrent(self, current, endInd):
        """Interpolate between two points, going from current to position.
        
        Returns nan if the fiber is broken or the current is the same at both points.
        """
        if self.isBroken:
            return numpy.nan

        posList, currList = zip(*self.measPosCurrList)
        if endInd <= 0 or endInd >= len(posList):
            raise RuntimeError("pos=%s out of range [%s, %s]" % (pos, posList[0], posList[-1]))
        begInd = endInd - 1
        
        try:
            slope = (posList[endInd] - posList[begInd]) / (currList[endInd] - currList[begInd])
        except ZeroDivisionError:
            return numpy.nan
        retVal = posList[begInd] + (slope * (current - currList[begInd]))
        return retVal
    
    def setVGBlockInfo(self, vgNum):
        """Set v-groove block position and compute desBeamCtrPos
        
        Inputs:
        - vgNum: v-groove groove number, starting from 1
        
        Note: fiber vgNum 1 is closest to the user when running the fiber tester
        """
        self.vgNum = int(vgNum)
        relVGNum = self.vgNum - (self.commonData.NumFibers + 1) / 2.0
        vgPos = relVGNum * self.commonData.FiberSep # position on v-groove block relative to center
        self.desAngle = relVGNum * self.commonData.DeltaAngle # desired angle of beam
        desRelBeamPos = math.tan(self.desAngle) * self.commonData.FiberApDist # desired beam pos at aperture relative to end of fiber
        self.desBeamCtrPos = self.commonData.meanYPeak - (vgPos + desRelBeamPos)
        #print "vgNum=%d, fiberNum=%d, desAngle=%0.5f, desBeamCtrPos=%0.2f" % (self.vgNum, self.num, self.desAngle, self.desBeamCtrPos)


class ScriptClass(object):
    """Fiber Tester script.
    
    At init time enable picoammeter
    When quitting Python disable picoammeter
    
    The algorithm:
    - Measure Isource
    - For fiber 1 through NumFibers:
        - Wait for fiber to be installed
        - Measure fiber I(x) at various x
        - Fit a parabola to I(x)
        - Compute xpeak and record I(xpeak)
    - Measure Isource again; make sure it has not changed too much
    - Sort fibers by increasing xpeak
    - Compute xctr for each fiber
    - Compute I(xctr) for each fiber
    - Display:
      - Throughput at xctr
      - Pass/fail
      - Throughput at xpeak and xpeak - xctr
      - Average Io and %change
      and write all this (and possibly each measurement) to a file with the harness # and date
    """
    # constants
    # StagePort = 1 # "/dev/cu.USA28X1a2P1.1"
    # AmmeterPort = 2 # "/dev/cu.USA28X1a2P2.2"
    
    tagGood = "good"
    tagWarning = "warning"
    tagError = "error"
    
    def __init__(self, sr):
        """The setup script; run once when the script runner
        window is created.
        """
        self.sr = sr
        self.sr.debug = False
        self.helpURL = None

        self.stage = None
        self.ammeter = None
        
        self.createWdg()
        
        self.initAll()
    
    def createWdg(self):
        """Create and grid the widgets.
        """
        gr = RO.Wdg.Gridder(self.sr.master, sticky="ew")
        
        self.harnessIDWdg = RO.Wdg.StrEntry(
            self.sr.master,
            label = "Harness ID",
            width = 5,
            helpText = "Harness identification number",
            helpURL = self.helpURL,
        )
        gr.gridWdg(self.harnessIDWdg.label, self.harnessIDWdg, sticky="w")
        
        self.instructWdg = RO.Wdg.StrLabel(
            master = self.sr.master,
#            readOnly = True,
            width = 20,
            helpText = "Instructions",
            helpURL = self.helpURL,
        )
        self.userContinueBtn = RO.Wdg.Button(
            self.sr.master,
            text = "Continue",
            callFunc = self.handleReturn,
            helpText = "Press or type <return> to continue",
            helpURL = self.helpURL,
        )
        gr.gridWdg(False, (self.instructWdg, self.userContinueBtn), colSpan=2, sticky="w")
        self.userContinueBtn.grid_remove()

        # table of measurements
        self.logWdg = RO.Wdg.LogWdg(
            master = self.sr.master,
            height = 30,
            width = 6 * 8,
            helpText = "Log of throughput measurements",
            helpURL = self.helpURL,
            relief = "sunken",
            bd = 2,
        )
        self.logWdg.text.tag_configure(self.tagError, foreground="red")
        self.logWdg.text.tag_configure(self.tagWarning, foreground="blue")
        self.logWdg.text.tag_configure(self.tagGood, foreground="dark green")
        gr.gridWdg(False, self.logWdg, sticky="nsew", colSpan = 10)
        self.sr.master.grid_rowconfigure(gr.getNextRow() - 1, weight=1)
        self.sr.master.grid_columnconfigure(9, weight=1)
        
        self.sr.master.winfo_toplevel().bind("<Return>", self.handleReturn)
        self.logWdg.text.bind("<Return>", self.handleReturn) # work around read-only bindings

        self.sr.master.winfo_toplevel().bind("<Command-q>", self.doQuit)
        self.logWdg.text.bind("<Command-q>", self.doQuit) # work around read-only bindings
    
    def doQuit(self, evt=None):
        if self.sr.isExecuting():
            d = SimpleDialog.SimpleDialog(self.sr.master.winfo_toplevel(),
                text="Quit now, while running?",
                buttons=["Yes", "No"],
                default=0,
                cancel=1,
                title="Really Quit?",
            )
            if d.go():
                return
            
        self.sr.master.quit()

    def doClear(self, wdg=None):
        self.logWdg.clearOutput()
    
    def end(self, sr):
        self.userContinueBtn.grid_remove()
        if self.logFile:
            self.logFile.close()
    
    def getEntryNum(self, wdg, errStr=None):
        """Return the numeric value of a widget, or raise ScriptError if blank.
        """
        numVal = wdg.getNumOrNone()
        if numVal != None:
            return numVal
        if errStr == None:
            errStr = wdg.label + " not specified"
        raise self.sr.ScriptError(errStr)

    def handleReturn(self, evt=None):
        if not self.sr.isExecuting():
            self.sr.start()
        elif self.sr._userWaitID != None:
            self.sr.resumeUser()
        
    def initAll(self):
        """Initialize and/or create variables and log
        """
        # initialize widgets
        self.doClear()
        self.instructWdg.clear()
        self.userContinueBtn.grid_remove()

        # initialize shared variables
        self.commonData = CommonData()
        self.fiberDataList = [] # list of FiberData object
        self.logFileName = None
        self.logFile = None
        self.currPosSteps = None
    
    def logFiberFit(self, fiberData):
        """Log the fit of current vs position for a fiber"""
        peakCurr = fiberData.computeFitCurr(fiberData.fitBeamCtrPos)
        # sanity check throughput; if it's too low now then the harness certainly fails
        # (but if it passes now, the throughput may still be too low once we figure out where the beam center should be).
        fiberOK = peakCurr >= self.commonData.MinSingleThroughput
        self.logMsg("\tFiber#\tyPeak\tIPeak\tbeamWid") # header
        dataStr = "Fit\t%d\t%0.2f\t%0.1f\t%0.1f" % \
            (fiberData.num, fiberData.fitBeamCtrPos, peakCurr, fiberData.computeBeamWidth())
        if fiberOK:
            self.logMsg(dataStr)
        else:
            self.logMsg(dataStr, tag=self.tagError)
        self.logFile.write(dataStr + "\n")

    def logFiberMeas(self, fiberData, pos, curr):
        """Log a fiber measurement.
        """
        measNum = len(fiberData.measPosCurrList)
    
        dataStr = "Meas\t%d\t%d\t%0.2f\t%0.1f" % (measNum, fiberData.num, pos, curr)
        self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")
    
    def logThroughputs(self, fiberDataList):
        """Log throughput information for all fibers.

        Inputs:
        - fiberDataList: list of fiber data
        """
        self.logMsg("\tVGrv#\tFiber#\tThru\tAngErr\tThruLoss")
        thruList = []
        badReasonList = []
        nBadFibers = 0
        badOrder = False
        for fiberData in fiberDataList:
            try:
                thru = fiberData.computeThroughput(fiberData.desBeamCtrPos)
            except Exception:
                thru = 0.0
            thruList.append(thru)
            maxThru = fiberData.computeThroughput(fiberData.fitBeamCtrPos)
            thruErr = maxThru - thru
            desAngle = fiberData.computeAngle(fiberData.desBeamCtrPos)
            fitAngle = fiberData.computeAngle(fiberData.fitBeamCtrPos)
            angErrDeg = (fitAngle - desAngle)
            dataStr = "Thru\t%d\t%d\t%0.1f\t%0.2f\t%0.1f" % (fiberData.vgNum, fiberData.num, thru, angErrDeg, thruErr)
            if thru < self.commonData.MinSingleThroughput:
                nBadFibers += 1
                self.logMsg(dataStr, tag=self.tagError)
            else:
                self.logMsg(dataStr)
            self.logFile.write(dataStr + "\n")
            if fiberData.num != fiberData.vgNum:
                badOrder = True
        
        minThru = numpy.min(thruList)
        dataStr = "MinThru\t%0.1f" % (minThru,)
        self.logMsg("\tMin(%)")
        if nBadFibers > 0:
            self.logMsg(dataStr, tag=self.tagError)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

        badAve = False
        aveThru = numpy.mean(thruList)
        stdDevThru = numpy.std(thruList)
        dataStr = "AveThru\t%0.1f\t%0.1f" % (aveThru, stdDevThru)
        self.logMsg("\tAve(%)\tSDev(%)")
        if aveThru < self.commonData.MinMeanThroughput:
            badAve = True
            self.logMsg(dataStr, tag=self.tagError)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

        if nBadFibers > 0:
            badReasonList.append("%d fibers have throughput < %0.1f" % (nBadFibers, self.commonData.MinSingleThroughput))
        if badAve:
            badReasonList.append("Average throughput < %0.1f" % (self.commonData.MinMeanThroughput,))
        if badOrder:
            badReasonList.append("Fibers are out of order")
        
        self.logMsg("")
        if badReasonList:
            dataStr = "Harness is Bad:"
            self.logMsg(dataStr, tag=self.tagError)
            for reasonStr in badReasonList:
                self.logMsg("- " + reasonStr, tag=self.tagError)
            self.setInstructWdg(dataStr, severity=RO.Constants.sevError)
        else:
            dataStr = "Harness is Good"
            self.logMsg(dataStr, tag=self.tagGood)
            self.setInstructWdg(dataStr)
    
    def logAveSource(self):
        """Log average source current and percent change
        """
        aveSrc = self.commonData.meanSourceCurrent
        pctChange = self.commonData.getFracSourceChange() * 100.0
        dataStr = "AveSrc\t%0.1f\t%0.1f" % (aveSrc, pctChange)
        if abs(pctChange) > self.commonData.MaxFracSourceChange * 100.0:
            self.logMsg(dataStr + " excessive change!", tag=self.tagWarning)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")
    
    def logMsg(self, msgStr, tag=None):
        """Append a message to the log, adding a final \n"""
        print "logMsg begin"
        if tag:
            tags = (tag,)
        else:
            tags = ()
        self.logWdg.addOutput(msgStr + "\n", tags=tags)
        if self.sr.debug:
            try:
                print msgStr
            except Exception:
                print repr(msgStr)
        print "logMsg end"
    
    def posFromSteps(self, posSteps):
        """Convert position in microsteps to position in mm"""
        return (float(posSteps) - (float(self.stage.FullRange) / 2.0)) / self.commonData.StepsPerMM
    
    def stepsFromPos(self, posMM):
        """Convert position in mm to position in microsteps.
        
        Raise RangeError if out of range"""
        posSteps = int(0.5 + (posMM * self.commonData.StepsPerMM) + (float(self.stage.FullRange) / 2.0))
        if (posSteps < 0) or (posSteps > self.stage.FullRange):
            raise RangeError("pos=%s mm = %s steps is out of range" % posMM, posSteps)
        return posSteps
    
    def recordUserParams(self):
        """Record user-set parameters
        """
        harnessID = self.harnessIDWdg.getString()
        if not harnessID:
            raise self.sr.ScriptError("Must specify a harness number")
        self.commonData.setharnessID(harnessID)
    
    def getSearchStepsList(self, startPos):
        """Return an array of search positions, in steps"""
        searchStepsList = [self.stepsFromPos(startPos)]
        multFac = 1
        oldLen = 0
        while oldLen < len(searchStepsList):
            oldLen = len(searchStepsList)
            deltaPos = multFac * self.commonData.BigStepSize
            try:
                searchStepsList.append(self.stepsFromPos(startPos + deltaPos))
            except Exception:
                pass
            try:
                searchStepsList.append(self.stepsFromPos(startPos - deltaPos))
            except Exception:
                pass
            multFac += 1
        #print "searchStepsList(%r) = %s" % (startPos, searchStepsList)
        return searchStepsList
    
    def run(self, sr):
        """Run the focus script.
        """
        self.initAll()
        self.recordUserParams()
        
        print "set up the devices"

        dateStr = time.strftime("%Y-%m-%d %H-%M-%S", time.gmtime())
        self.logFileName = "Harness %s %s.dat" % (self.commonData.harnessID, dateStr)
        logFilePath = os.path.join(self.commonData.LogFileDir, self.logFileName)
        self.logFile = file(logFilePath, "w")
        self.writeLogFileHeader()
        
        print "run.self.waitMeasureSource call"
        yield self.waitMeasureSource()
        print "run.self.waitMeasureSource done"
        
        yDelta = int((self.commonData.YRangeSteps / float(self.commonData.NumMeas - 1)) + 0.5)
        yCtr = 0.5 * self.stage.FullRange
        yStart = int(yCtr - (0.5 * self.commonData.YRangeSteps) + 0.5)
        yEnd = yStart + ((self.commonData.NumMeas - 1) * yDelta) + 1
        
        startSearchPos = 0
        for fiberNum in range(1, self.commonData.NumFibers + 1):
            #reply = subprocess.call(["sh", "/Users/mabadmin/manga/manga-fiber-acquire.sh", "f19", str(fiberNum)])
            reply = subprocess.call(["manga-fiber-acquire", "f19", str(fiberNum)])
            yield self.waitForUser("Set up Fiber %d" % (fiberNum,), "Fiber %d" % (fiberNum,))
            self.setInstructWdg("Measuring Fiber %d" % (fiberNum,))
            
            # find v-groove block (any light above 20% of input)
            foundPos = None
            foundCurr = None
            minCurrent = 0.2 * self.commonData.sourceCurrentList[0]
            for searchSteps in self.getSearchStepsList(startSearchPos):
                searchPos = self.posFromSteps(searchSteps)
                yield self.waitMoveStage(searchSteps)
                yield self.waitMeasureCurrent()
                curr = self.sr.value
                if curr >= minCurrent:
                    foundPos = searchPos
                    foundCurr = curr
                    self.sr.showMsg("Found fiber %s at %0.1f mm" % (fiberNum, searchPos,))
                    break
            
            # if no fiber found, go on to next fiber
            if foundPos == None:
                self.sr.showMsg("Fiber %s is broken" % (fiberNum,))
                fiberData = FiberData(fiberNum, self.commonData)
                fiberData.isBroken = True
                self.logFiberFit(fiberData)
                self.fiberDataList.append(fiberData)
                yield self.waitMoveStage(self.stepsFromPos(startSearchPos))
                continue
            
            fiberData = FiberData(fiberNum, self.commonData)
            fiberData.addMeas(foundPos, foundCurr)
            self.logMsg("\tMeas#\tFiber#\tPos(mm)\tI(nA)")
            self.logFiberMeas(fiberData, foundPos, foundCurr)

            # walk until signal drops to 50% of maximum
            for measDir in (1, -1):
                curr = foundCurr
                measPos = foundPos
                while curr > 0.5 * fiberData.maxCurr:
                    measPos += self.commonData.SmallStepSize * measDir
                    try:
                        measSteps = self.stepsFromPos(measPos)
                    except Exception:
                        # can't go any further in this direction
                        break
                    yield self.waitMoveStage(measSteps)
                    yield self.waitMeasureCurrent()
                    curr = self.sr.value
                    trueMeasPos = self.posFromSteps(measSteps)
                    fiberData.addMeas(trueMeasPos, curr)
                    self.logFiberMeas(fiberData, trueMeasPos, curr)
                
            # fit center of beam and measure current there
            # measuring there is not essential but
            # it moves the stage to a good position to find the next fiber
            # and displays the likely best current on the ammeter
            fiberData.fitBeam()
            yield self.waitMoveStage(int(self.stepsFromPos(fiberData.fitBeamCtrPos)))
            yield self.waitMeasureCurrent()
            fitBeamCtrCurr = self.sr.value
            fiberData.addMeas(fiberData.fitBeamCtrPos, fitBeamCtrCurr)
            startSearchPos = fiberData.fitBeamCtrPos

            # fit again, though fit should not change much from measuring current at center
            fiberData.fitBeam()
            self.logFiberFit(fiberData)
            
            self.fiberDataList.append(fiberData)

        yield self.waitMeasureSource()
        self.logAveSource()
        
        # set v-groove position and compute optimal peak position for each fiber
        self.fiberDataList.sort(key=FiberData.getVGNumSortKey)
        fitBeamCtrPosList = [fd.fitBeamCtrPos for fd in self.fiberDataList if not fd.isBroken]
        self.commonData.setMeanYPeak(numpy.mean(fitBeamCtrPosList))
        for ind, fiberData in enumerate(self.fiberDataList):
            fiberData.setVGBlockInfo(ind + 1)
        
        # report results -- including updating self.instructWdg
        self.logThroughputs(self.fiberDataList)
    
    def setInstructWdg(self, msgStr, severity=RO.Constants.sevNormal):
        """Set instructions widget message"""
        self.instructWdg.set(msgStr, severity=severity)
    
    def waitForUser(self, msgStr, speechStr=None):
        """Display an instruction and wait for the user to press the Continue button.
        """
        self.setInstructWdg(msgStr)
        if speechStr:
            macspeechX.SpeakString(speechStr)
        self.userContinueBtn.grid()
        self.userContinueBtn.focus_set()
        yield self.sr.waitUser()
        self.userContinueBtn.grid_remove()
        self.instructWdg.clear()
    
    def waitMeasureCurrent(self):
        """Take a current measurement, in nanoamps, and put it in script.sr.value
        Halts the script if no measurement can be taken.
        """
        print "waitMeasureCurrent begin"
        reply = subprocess.check_output(["rbd_query"])
        # reply should look like
        # rbd_query: 1354554580 377764 &S=,Range=020nA,+12.037,nA
        print reply[:-1]
        s1 = reply.split()
        print s1
        s2 = s1[3].split(",")
        print s2
        reading = s2[2]
        print reading
        units = s2[3]
        print units
        reading = float(reading)
        print reading

        #self.ammeter.measure()
        #yield self.waitAmmeter()
        #measStr = self.sr.value
        #measCurr = measStr.split(",", 1)[0][:-1]
        #if not measCurr:
        #    raise ScriptError("No measured current returned")
        #self.sr.value = float(measCurr) * 1.0e9 # convert amps to nanoamps
        self.sr.value = reading
        print "waitMeasureCurrent end"
    
    #def waitAmmeter(self):
    #   _WaitAmmeter(self.sr, self.ammeter)
    
    def waitConfigAmmeter(self):
        """Initialize and configure the ammeter"""
        self.ammeter.startCmdList(
            cmdList = (
#                "*RST",
                "SYST:ZCH OFF", # disable zero check
                "SENS:CURR:NPLC 1", # set meas rate to # power line cycles (p4-5)
                "CONF:CURR", # I'm not sure; the choices are CURR and DC (p12-2)
                "CURR:RANGE:AUTO OFF",  # disable autoranging
                "CURR:RANGE 2E-7",  # set range to 0.2 uA (p4-4)
                "STAT:QUEUE:NEXT?", # get last error code
            ),
            name = "Initialize",
        )
        yield self.waitAmmeter()
        errReply = self.sr.value
        if not errReply:
            raise self.sr.ScriptError("Final status code not returned")
        errCode, errStr = errReply.split(",", 1)
        if errCode != "0":
            raise self.sr.ScriptError("Ammeter init failed with error: %s" % (errReply,))
        self.sr.value = None
    
    def waitConfigStage(self):
        """Home the stage and enable the motor"""
        
        print "waitConfigStage begin"
        reply = subprocess.check_output(["apt_move_home", "x2", "-v", "-b"])
        print "reply", reply
        # os.system("apt_move_home x2 -v -b")
        
        self.stage.home()
        yield self.waitStage()
        self.stage.startCmd("SXMO1") # enable motor
        yield self.waitStage()
        yield self.waitMoveStage(self.stepsFromPos(0.0))
        self.currPosSteps = None
        print "waitConfigStage end"
    
    def waitMeasureSource(self):
        """Measure and log source current (prompting the user)"""
        print "waitMeasureSource begin"
        
        print "waitMeasureSource.self.waitForUser call"
        yield self.waitForUser("Set up Source Meas.", "Source")
        print "waitMeasureSource.self.waitForUser done"
        print "waitMeasureSource.waitMeasureCurrent call"
        yield self.waitMeasureCurrent()
        print "waitMeasureSource.waitMeasureCurrent done"
        curr = self.sr.value
        curr = 30	# jwp bypass source measurement
        self.commonData.addSourceMeas(curr)
        dataStr = "Src\t%0.1f" % (curr,)
        self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")
        if curr < self.commonData.MinSourceCurrent:
            raise RO.ScriptRunner.ScriptError("Source current too low: %0.1f < %0.1f nA" % \
                (curr, self.commonData.MinSourceCurrent))
        print "waitMeasureSource end"
    
    def waitMoveStage(self, posSteps):
        """Move the linear stage to the specified absolute position.
        
        Inputs:
        - posSteps: desired absolute position, in microsteps
        """
        print "waitMoveStage begin"
        aptSteps = (posSteps / 1968.5) + 20  # jwp our estimated zero point offset
        print "posSteps", posSteps
        print "aptSteps", aptSteps
        # os.system("apt_move_absolute x2 -v -b " + str(aptSteps))
        reply = subprocess.check_output(["apt_move_absolute", "x2", "-v", "-b", str(aptSteps)])
        
        if self.currPosSteps == posSteps:
            self.sr.waitMS(0)
            return
        
        self.stage.moveAbs(posSteps)
        _WaitDevice(self.sr, self.stage)
        self.currPosSteps = posSteps
        # aptSteps = posSteps / 1968.5
        # print "posSteps", posSteps
        # print "aptSteps", aptSteps
        # os.system("apt_move_absolute x2 -v -b " + str(aptSteps))
        print "waitMoveStage end"
    
    def waitStage(self):
        print "waitStage begin"
        _WaitDevice(self.sr, self.stage)
        print "waitStage end"
        
        
    def writeLogFileHeader(self):
        """Write header info to log file"""
        self.logFile.write("# %s\n" % self.logFileName)
        self.logFile.write("""

# Units are as follows
# Throughput is in %%
# Current is in nanoamps
# Position is in mm
# Angle is in degrees

# Each line starts with a keyword indicating its contents:
# Src:  source intensity measurement
# AveSrc: average source intensity and percent change
# Meas: fiber intensity measurement
# Fit:  fiber fit
# Thru: throughput of one fiber
# AveThru: average and standard deviation of throughput for all fibers
# MinThru: minimum throughput for all fibers
# Fields are:
Src\tISrc
AveSrc\taveISrc\tsrcPctChange
Meas\tfiberNum\tmeasNum\tpos\tIFib
Fit\tfiberNum\tposPeak\tIPeak\tbeamWid
Thru\tvgrooveNum\tfiberNum\tthru\tangErr\thruLoss
AveThru\tthru\tstdDev
MinThru\tthru
# Where:
# - fiberNum is the number of the fiber in the anchor block (1 is closest to mounting hole)
# - vgrooveNum is the number of the fiber in the v-groove block (1 is right? when facing front of block)
# - srcPctChange is the percent in source intensity from initial to final measurement
# - beamWid is the beam width in degrees (edge to edge at the %0.0f%% level)
# - angErr is error in output angle = angle of fit peak - angle of desired peak
# - thruLoss is loss of throughput due to angle error = peak throughput - throughput at desired angle
""" % (self.commonData.BeamEdgeFrac * 100.0,))


class _WaitDevice(RO.ScriptRunner._WaitBase):
    """Wait for a device command.
    
    The device must support methods:
    - addCallback(callFunc)
    - removeCallback(callFunc, doRaise)
    - isDone()
    - didFail()
    - getStateStr()

    Inputs:
    - scriptRunner: the script runner
    - one or more command variables (RO.KeyVariable.CmdVar objects)
    - checkFail: check for command failure?
        if True (the default) command failure will halt your script
    """
    def __init__(self, scriptRunner, dev, checkFail=True):
        self.dev = dev
        self.checkFail = bool(checkFail)
        self.addedCallback = False
        RO.ScriptRunner._WaitBase.__init__(self, scriptRunner)

        if self.getState() != 0:
            # no need to wait; commands are already done or one has failed
            # schedule a callback for asap
            #print "_WaitDevice: no need to wait"
            self.master.after(1, self.devCallback)
        else:
            # need to wait; add self as callback to device
            self.dev.addCallback(self.devCallback)
            self.addedCallback = True
    
    def cancelWait(self):
        """Call when aborting early.
        """
#       print "_WaitDevice.cancelWait"
        self.cleanup()
    
    def cleanup(self):
        """Called when ending for any reason.
        """
#       print "_WaitDevice.cleanup"
        if self.addedCallback:
            didRemove = self.dev.removeCallback(self.devCallback, doRaise=False)
            self.addedCallback = False
            if not didRemove:
                sys.stderr.write("_WaitDevice cleanup could not remove callback from device\n")
    
    def devCallback(self, *args, **kargs):
        """If device command finished then continue or fail, as appropriate
        """
        if not self.dev.isDone():
            return
        if self.dev.didFail():
            reason = self.dev.getStateStr()
            self.fail(reason)
        else:
            self._continue()

    def getState(self):
        """Return the current state as a numeric value.
        See the state constants defined in RO.ScriptRunner.
        See also getFullState.
        """
        if not self.dev.isDone():
            return RO.ScriptRunner.Running
        elif self.dev.didFail():
            return RO.ScriptRunner.Failed
        else:
            return RO.ScriptRunner.Done


class _WaitAmmeter(_WaitDevice):
    """Wait for an ammeter command"""
    def devCallback(self, *args, **kargs):
        """If ammeter command finished then continue or fail, as appropriate.
        If it succeeds then sr.value is set to the last command reply.
        """
        if not self.dev.isDone():
            return
        if self.dev.didFail():
            reason = self.dev.getStateStr()
            self.fail(reason)
        else:
            self._continue(self.dev._currCmd.reply)
    

if __name__ == "__main__":
    import Tkinter
    
    root = Tkinter.Tk()
    
    sr = RO.ScriptRunner.ScriptRunner(
        master = root,
        name = "FiberTester",
        scriptClass = ScriptClass,
        startNow = True,
    )

    root.mainloop()

