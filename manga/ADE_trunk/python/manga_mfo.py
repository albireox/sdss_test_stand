#! /usr/bin/env python
# file: $RCSfile: manga_mfo.py,v $
# rcsid: $Id: manga_mfo.py,v 1.4 2013/08/16 20:16:24 jwp Exp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# general manga fiber operations
# ********************************************************************

import ConfigParser
import inspect
import math
import matplotlib.pyplot as plt
import numpy
import os
import subprocess
Popen = subprocess.Popen
PIPE = subprocess.PIPE
import sys

def ifu_count(rank):
    """Returns the number of members in an IFU of the given rank."""
    n = (3 * rank) * (rank + 1) + 1
    return n

def read_metrology_map(mapfile):
    """Reads in a metrology map file."""
    tag = inspect.stack()[0][3] # our name

    path = find_file(mapfile)

    print "{}: mapfile {}".format(tag, path)

    metrology_map = {}

    try:
        with open(path, "r") as f:
            lines = f.readlines()
    except Exception as e:
        print >> sys.stderr, "{}: Can't open error map file {}".format(tag, path)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return metrology_map

    for line in lines:
        tokens = line.split()
        if tokens[1] == "residuals:":
            if (tokens[2] == "f"):
                pass    # header line
            else:
                fiber = int(tokens[2])
                pos = int(tokens[3])
                xm = float(tokens[4])
                ym = float(tokens[5])
                xs = float(tokens[6])
                ys = float(tokens[7])
                ex = float(tokens[8])
                ey = float(tokens[9])
                er = float(tokens[10])
                metrology_map[fiber] = (fiber, pos, xm, ym, xs, ys, ex, ey, er)

    return metrology_map

####################################################################
############# map-file related functions/definitions ###############
####################################################################

CENTER_X = 1624 / 2 
CENTER_Y = 1234 / 2

searchlist = [
    ".",
    "~",
    "~/manga",
    "~/manga-archive/manta",
    "~/git/MaNGA/manga/ADE_Trunk/python/ifu_maps",
    "~/Backups/duplicate data/manga-archive",
    "/Volumes/Jeff\'s Vault/Backups/manga-archive/manta",
    "/Volumes/Jeff\'s Vault/Backups/duplicate data/manga",
    "/Volumes/Jeff\'s Vault/workspaces/python/workspace-indigo/python"
]
    

def find_file(filename):
    """Searches the usual places for the given file."""
    tag = inspect.stack()[0][3] # our name

    for d in searchlist:
        d = os.path.expanduser(d)
        p = os.path.join(d, filename)
        #print "{}: p {}".format(tag, p)
        if os.path.isfile(p):
            print "{}: path {}".format(tag, p)
            return p

    return None

def read_ifu_fiber_map(filename):
    """Reads the IFU fiber map file."""
    tag = inspect.stack()[0][3] # our name

    ifu_fiber_map = []
    
    path = find_file(filename)

    try:
        with open(path, "r") as f:
            lines = f.readlines()
    except Exception as e:
        print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, path)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return None

    lines.pop(0)    # lose the header line
    for line in lines:
        a = line.split()
        cfiber = int(a[0])
        sfiber = int(a[1])
        ifu_fiber_map.append((cfiber, sfiber))

    return ifu_fiber_map

def read_pp_fiber_map(filename):
    """Reads the fiber-to-hole mapping file."""
    tag = inspect.stack()[0][3] # our name

    pp_fiber_map = {}

    if filename is None:
        return pp_fiber_map

    path = find_file(filename)

    try:
        with open(path, "r") as f:
            lines = f.readlines()
    except Exception as e:
        print >> sys.stderr, "{}: Can't open fiber map file {}".format(tag, path)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return pp_fiber_map

    lines.pop(0)    # lose the header line
    for line in lines:
        tokens = line.split()
        if len(tokens) < 2:
            continue
        f = int(tokens[0])
        h = int(tokens[1])
        pp_fiber_map[f] = h

    return pp_fiber_map

def pp_hole_xy(h):
    """ Computes the ideal plate coordinates (mm) of the given plate hole number 0-34."""
    
    x = 10.5 + 7 * (h % 2) + 17 * (h/2 % 2)
    y = 10.75 - 16 * (h/4) # minus from the orientation of the y stage
    return (h, x, y)

def map_list_and_cfiber(manga_map, cfiber):
    """Finds a given c-tech fiber number in a given map."""

    for m in manga_map:
        if m[0] == cfiber:
            return m
    return (cfiber, -1)

def map_rank_and_cfiber(rank, cfiber):
    """Returns the fiber map entry for the given rank and c-tech fiber number."""

    if rank == 1:
        m = map_list_and_cfiber(ifu_fiber_map_rank_1, cfiber)
    elif rank == 2:
        m = map_list_and_cfiber(ifu_fiber_map_rank_2, cfiber)
    elif rank == 3:
        m = map_list_and_cfiber(ifu_fiber_map_rank_3, cfiber)
    elif rank == 4:
        m = map_list_and_cfiber(ifu_fiber_map_rank_4, cfiber)
    elif rank == 5:
        m = map_list_and_cfiber(ifu_fiber_map_rank_5, cfiber)
    elif rank == 6:
        m = map_list_and_cfiber(ifu_fiber_map_rank_6, cfiber)
    else:
        m = (cfiber, -1)
    return m

pp_drill_map = map(pp_hole_xy, range(0, 35))
# ifu_fiber_map_rank_1 = read_ifu_fiber_map("ifu-fiber-map-rank-1.map")
# ifu_fiber_map_rank_2 = read_ifu_fiber_map("ifu-fiber-map-rank-2.map")
# ifu_fiber_map_rank_3 = read_ifu_fiber_map("ifu-fiber-map-rank-3.map")
# ifu_fiber_map_rank_4 = read_ifu_fiber_map("ifu-fiber-map-rank-4.map")
# ifu_fiber_map_rank_5 = read_ifu_fiber_map("ifu-fiber-map-rank-5.map")
# ifu_fiber_map_rank_6 = read_ifu_fiber_map("ifu-fiber-map-rank-6.map")

###################################################
############## plotting commands ##################
###################################################


# the master list of 169 (rank=7) fibers, in AB space


def ifu_ab2xy(fab):
    """Converts a AB-space 3-tuple into an XY-space tuple"""

    f, a, b = fab
    x = 2 * a + b
    y = b * numpy.sqrt(3)

    return (f, x, y)

def plot_pp_fiber_map(mapfile, pp_drill_map):
    """Produces a nice plot of the plug plate."""
#    tag = inspect.stack()[0][3] # our name

    pp_fiber_map = read_pp_fiber_map(mapfile)

    fig = plt.figure()
    fig.add_subplot(111, aspect='equal')

    # the spot size scale factor in points^2.
    # This value is about right.
    # multiply this times mm to get a spot of roughly the right size.
    a = 240;
#    print; print "{}: a: {}".format(tag, a)

    flist = []
    xlist = []
    ylist = []
    for f in range(1, 21):
        h = pp_fiber_map[f - 1]
        _, x, y = pp_drill_map[h]
        print "f", f, "h", h, "x", x, "y", y
        flist.append(f)
        xlist.append(x)
        ylist.append(y)

    s = 2.15 * 2.15 * a
    plt.scatter(xlist, ylist, s=s, c="Aquamarine")
    for f in flist:
        h = pp_fiber_map[f - 1]
        _, x, y = pp_drill_map[h]
        plt.text(x, y, f, size='x-small', horizontalalignment='center', verticalalignment='center')

    plt.axis([-5, 30, -5, 20])
    plt.grid(True, which='both')
    title = "MaNGA UWisc Fiber Test Stand Plug Plate Fiber Map"
    plt.title(title)
    plt.xlabel("X (mm)")
    plt.ylabel("Y (mm)")
    plt.tight_layout()
    #plt.show()
    _, tail = os.path.split(mapfile)
    root, _ = os.path.splitext(tail)
    filename = "manga-figure-" + root
    fig.savefig(filename + ".png")


def read_spike_map(mapfile):
    """Reads in a spike map file."""
    tag = inspect.stack()[0][3] # our name

    print "{}: mapfile {}".format(tag, mapfile)

    spike_map = []

    try:
        with open(mapfile, "r") as f:
            lines = f.readlines()
    except Exception as e:
        print >> sys.stderr, "{}: Can't open error map file {}".format(tag, mapfile)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return spike_map

    ix = 0
    for line in lines:
        #print "line", line.strip()
        tokens = line.split()
        #print "tokens", tokens
        if tokens[0] == "manga_spikes:":
            if (tokens[1] == "ix"):
                pass    # header line
            else:
                f = int(tokens[2])
                xc = float(tokens[3])
                yc = float(tokens[4])
                spike_map.append((ix, f, xc, yc))
                ix += 1

    return spike_map

def plot_spike_map(mapfile):
    """Produces a nice plot of a spike file"""
    tag = inspect.stack()[0][3] # our name

    print "{}: read spike file {}".format(tag, mapfile)

    spike_map = read_spike_map(mapfile)

    _, fv, xc, yc = zip(*spike_map)

    title = mapfile

    fig = plt.figure()
    plt.subplot(1, 1, 1)
    plt.title(title)
    plt.axis('equal')
    #plt.axis([-5, 30, -5, 20])
    plt.xlabel("X (mm)")
    plt.ylabel("Y (mm)")
    plt.grid(True, which='both')

    plt.scatter(xc, yc, s=300, c='w')

    for f, x, y in zip(fv, xc, yc):
        plt.text(x, y, int(f), horizontalalignment='center', verticalalignment='center', size='small')

    plt.tight_layout()
    fig.savefig(title + ".png")
    plt.show()

def read_ifu_map(mapfile):
    """Reads in a ifu map file (made by manga_ifu)."""
    tag = inspect.stack()[0][3] # our name

    print "{}: mapfile {}".format(tag, mapfile)

    # defaults
    c = 1.0
    theta = 0.0
    sx = CENTER_X
    sy = CENTER_Y
    aop = (c, theta, sx, sy)

    ifu_map = []

    try:
        with open(mapfile, "r") as f:
            lines = f.readlines()
    except Exception as e:
        print >> sys.stderr, "{}: Can't open error map file {}".format(tag, mapfile)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return aop, ifu_map

    ix = 0
    for line in lines:
        tokens = line.split()
        if tokens[0] != "manga_ifu:":
            continue;
        if tokens[1] == "solution:":
            if tokens[2] == "aop:":
                if tokens[3] == "c:":
                    c = float(tokens[4])
                elif tokens[3] == "theta:":
                    theta = math.radians(float(tokens[4]))
                elif tokens[3] == "sx:":
                    sx = float(tokens[4])
                elif tokens[3] == "sy:":
                    sy = float(tokens[4])
        elif tokens[1] == "residuals:":
            if (tokens[2] == "ix"):
                pass    # header line
            else:
                pos = int(tokens[3])
                modelx = float(tokens[4])
                modely = float(tokens[5])
                imagex = float(tokens[6])
                imagey = float(tokens[7])
                ex = float(tokens[8])
                ey = float(tokens[9])
                er = float(tokens[10])
                ifu_map.append((ix, pos, modelx, modely, imagex, imagey, ex, ey, er))
                ix += 1

    aop = (c, theta, sx, sy)

    return aop, ifu_map

def plot_ifu_map(mapfile):
    """Produces a nice plot of an ifu file (made by manga_ifu)"""
    tag = inspect.stack()[0][3] # our name

    print "{}: read ifu file {}".format(tag, mapfile)

    aop, ifu_map = read_ifu_map(mapfile)
    print "ifu_map", ifu_map

    ix, fv, modelx, modely, imagex, imagey, ex, ey, er = zip(*ifu_map)
    # refer the model to the origin
    x0 = modelx[0]
    modelx = [x - x0 for x in modelx]
    y0 = modely[0]
    modely = [y - y0 for y in modely]

    title = mapfile

    fig = plt.figure()
    plt.subplot(1, 1, 1)
    plt.title(title)
    plt.axis('equal')
    #plt.axis([-5, 30, -5, 20])
    plt.xlabel("X (Pixels)")
    plt.ylabel("Y (Pixels)")
    plt.grid(True, which='both')

    plt.scatter(modelx, modely, s=800, c='w')

    for f, x, y in zip(fv, modelx, modely):
        plt.text(x, y, int(f), horizontalalignment='center', verticalalignment='center', size='small')

    Q = plt.quiver(modelx, modely, ex, ey, pivot='tail', width=0.002)
    plt.quiverkey(Q, 0.1, 0.1, 1.000, r'$1\ pixel$', labelpos='S')

    plt.tight_layout()
    fig.savefig(title + ".png")
    plt.show()


def plot_metrology_map(mapfile):
    """Produces a nice plot of a metrology file"""
    tag = inspect.stack()[0][3] # our name

    print "{}: read metrology map file {}".format(tag, mapfile)

    metrology_map = read_metrology_map(mapfile)

    f, pos, mx1, my1, mx2, my2, ex, ey, er = zip(*metrology_map)
    print len(pos)
    if len(pos) > 35:
        psize = 600
        qkey = (0.005, r'$5\ \mu$')
    else:
        psize = 900
        qkey = (0.005, r'$5\ \mu$')

    title = mapfile

    fig = plt.figure()
    plt.subplot(1, 1, 1)
    plt.title(title)
    plt.axis('equal')
    #plt.axis([-5, 30, -5, 20])
    plt.xlabel("X (mm)")
    plt.ylabel("Y (mm)")
    plt.grid(True, which='both')

    plt.scatter(mx1, my1, s=psize, c='w')

    Q = plt.quiver(mx1, my1, ex, ey, pivot='tail', width=0.002)
    plt.quiverkey(Q, 0.1, 0.1, qkey[0], qkey[1], labelpos='S')

    for pos, x, y in zip(pos, mx1, my1):
        plt.text(x, y, int(pos), horizontalalignment='center', verticalalignment='center', size='small')

    plt.tight_layout()
    fig.savefig(title + ".png")
    plt.show()


def ifu_outline_scale(s):
    """Scales up the rank=1 outline for higher ranks."""
    new_ab = []
    for ab in ifu_outline:
        a, b = ab
        new_ab.append((s * a, s * b))
    return new_ab

def get_vgb_colors(rank):
    """Returns a list of v-groove block colors for the given rank."""
    manga_colors = []
    n = ifu_count(rank)
    for sfiber in range(0, n):
        m = map_rank_and_sfiber(rank, sfiber)
        v = get_vgb_number(rank, m[0])
        manga_colors.append(ifu_vgb_color_list[v])
    return manga_colors

def get_vgb_number(rank, cfiber):
    """Given a rank and a c-tech fiber number, returns v-groove block number (zero-based)."""

    if rank == 1: # 7 fibers
        vgb_number = 0
    elif rank == 2: # 19 fibers
        vgb_number = 0
    elif rank == 3: # 37 fibers (guessing as we don't have one)
        vgb_number = 0
    elif rank == 4: # 61 fibers, 30 then 31
        if cfiber <= 30:
            vgb_number = 0
        else:
            vgb_number = 1
    elif rank == 5: # 91 fibers, 30 then 30 then 31 (guessing as we don't have one)
        if cfiber <= 30:
            vgb_number = 0
        elif cfiber <= 60:
            vgb_number = 1
        else:
            vgb_number = 2
    elif rank == 6: # 127 fibers, 30 then 30 then 30 then 37
        if cfiber <= 30:
            vgb_number = 0
        elif cfiber <= 60:
            vgb_number = 1
        elif cfiber <= 90:
            vgb_number = 2
        else:
            vgb_number = 3
    else:
        vgb_number = 0

    return vgb_number    

def map_list_and_sfiber(manga_map, sfiber):
    """Finds a given spiral fiber number in a given map."""

    for m in manga_map:
        if m[1] == sfiber:
            return m
    return (-1, sfiber)

def map_rank_and_sfiber(rank, sfiber):
    """Returns the fiber map entry for the given rank and spiral fiber number."""

    if rank == 1:
        m = map_list_and_sfiber(ifu_fiber_map_rank_1, sfiber)
    elif rank == 2:
        m = map_list_and_sfiber(ifu_fiber_map_rank_2, sfiber)
    elif rank == 3:
        m = map_list_and_sfiber(ifu_fiber_map_rank_3, sfiber)
    elif rank == 4:
        m = map_list_and_sfiber(ifu_fiber_map_rank_4, sfiber)
    elif rank == 5:
        m = map_list_and_sfiber(ifu_fiber_map_rank_5, sfiber)
    elif rank == 6:
        m = map_list_and_sfiber(ifu_fiber_map_rank_6, sfiber)
    else:
        m = (-1, sfiber)
    return m

def plot_ifu_axes(rank):
    """Produces a plot showing our coordinate systems."""
    #tag = inspect.stack()[0][3] # our name

    fig = plt.figure()
    fig.add_subplot(111, aspect='equal')

    n = ifu_count(rank)

    _, x, y = zip(*ifu_xy_list[:n])
    plt.scatter(x, y, s=550, c='w')

    x1 = 0
    y1 = 0
    x2 = 2 * rank + 2
    x2 = min(x2, 11)
    y2 = 0
    plt.arrow(x1, y1, x2, y2, width=0.02, head_width=0.5, overhang=1, label='X,A')
    plt.text(x2 + 1, -0.3, "X, A")
    x2 = 0
    y2 = 2 * rank + 2
    y2 = min(y2, 12)
    plt.arrow(x1, y1, x2, y2, width=0.02, head_width=0.5, overhang=1, label='Y')
    plt.text(-0.25, y2 + 1, "Y")
    x2 = rank + 1
    y2 = x2 * math.sqrt(3)
    plt.arrow(x1, y1, x2, y2, width=0.02, head_width=0.5, overhang=1, label='B')
    plt.text(x2 + 0.5, y2 + 0.7, "B")
    x1 = -13
    y1 = 12
    x2 = x1
    y2 = y1 - 1
    plt.text(x1, y1, "x = 2*a+b")
    plt.text(x2, y2, "y = b*sqrt(3)")

    plt.axis([-14, 14, -14, 14])
    plt.grid(True, which='both')
    title = "Cartesian (XY) and Hexagonal (AB) Coordinate Systems"
    title += "\nIdeal Rank=" + str(rank) + " IFU"
    plt.title(title)
    plt.xlabel("X, A (Fiber Radii)")
    plt.ylabel("Y (Fiber Radii)")
    #plt.tight_layout()
    #plt.show()
    filename = "manga-figure-example-rank-" + str(rank)
    fig.savefig(filename + ".png")

def plot_ifu_ab_map(rank, scheme):
    """Produces a nice AB-space plot of an IFU of the given rank."""
    #tag = inspect.stack()[0][3] # our name

    fig = plt.figure()
    fig.add_subplot(111, aspect='equal')

    n = ifu_count(rank)
    manga_colors = get_vgb_colors(rank)

    f, a, b = zip(*ifu_ab_list[:n])
    plt.scatter(a, b, s=410, c=manga_colors)

    for f, a, b in ifu_ab_list[:n]:
        if scheme != "s":
            m = map_rank_and_sfiber(rank, f)
            f = m[0]
        plt.text(a, b, f, size='x-small', horizontalalignment='center', verticalalignment='center')
    for s in range(1, rank + 1):
        new_ab = ifu_outline_scale(s)
        a, b = zip(*new_ab)
        plt.plot(a, b)
    plt.axis([-8, 8, -8, 8])
    plt.grid(True, which='both')
    title = "Ideal Rank=" + str(rank) + " IFU (" + ifu_scheme_names[scheme] + " Numbering)"
    if rank < 4:
        title += "\nSingle V-Groove Block"
    else:
        title += "\nColors Represent V-Groove Blocks"
    title += "\nFace-On View, Pin to the Left"
    plt.title(title)
    plt.xlabel("Hexagonal Coordinate A (Fiber Diameter = 1)")
    plt.ylabel("Hexagonal Coordinate B")
    plt.tight_layout()
    #plt.show()
    filename = "manga-figure-rank-" + str(rank) + "-ab-" + str(scheme)
    fig.savefig(filename + ".png")

def plot_ifu_xy_map(rank, scheme):
    """Produces a nice XY-space plot of an IFU of the given rank."""
    #tag = inspect.stack()[0][3] # our name

    fig = plt.figure()
    fig.add_subplot(111, aspect='equal')

    n = ifu_count(rank)
    colors = get_vgb_colors(rank)

    f, x, y = zip(*ifu_xy_list[:n])
    plt.scatter(x, y, s=550, c=colors)

    for f, x, y in ifu_xy_list[:n]:
        if scheme != "s":
            m = map_rank_and_sfiber(rank, f)
            f = m[0]
        plt.text(x, y, f, size='x-small', horizontalalignment='center', verticalalignment='center')
    for s in range(1, rank + 1):
        new_ab = ifu_outline_scale(s)
        new_xy = []
        for ab in new_ab:
            xy = ifu_ab2xy((0, ab[0], ab[1]))
            new_xy.append((xy[1], xy[2]))
        x, y = zip(*new_xy)
        plt.plot(x, y)
    plt.axis([-14, 14, -14, 14])
    plt.grid(True, which='both')
    title = "Ideal Rank=" + str(rank) + " IFU (" + ifu_scheme_names[scheme] + " Numbering)"
    if rank < 4:
        title += "\nSingle V-Groove Block"
    else:
        title += "\nColors Represent V-Groove Blocks"
    title += "\nFace-On View, Pin to the Left"
    plt.title(title)
    plt.xlabel("X (Fiber Radius = 1)")
    plt.ylabel("Y")
    plt.tight_layout()
    #plt.show()
    filename = "manga-figure-rank-" + str(rank) + "-xy-" + str(scheme)
    fig.savefig(filename + ".png")

def plot_pp_drill_map(pp_drill_map):
    """Produces a nice plot of the plug plate."""
    tag = inspect.stack()[0][3] # our name

    print "{}:".format(tag)

    fig = plt.figure()
    fig.add_subplot(111, aspect='equal')

    # the spot size scale factor in points^2.
    # This value is about right.
    # multiply this times mm to get a spot of roughly the right size.
    a = 240;
#    print; print "{}: a: {}".format(tag, a)

    # clone the list because we want to pop some elements
    tmplist = list(pp_drill_map)
    tmplist.pop(24)
    tmplist.pop(18)
    tmplist.pop(17)
    tmplist.pop(11)
    tmplist.pop(10)

    h, x, y = zip(*tmplist)
    s = 2.15 * 2.15 * a
    plt.scatter(x, y, s=s, c="Plum")
    for h, x, y in tmplist:
        plt.text(x, y, h, size='x-small', horizontalalignment='center', verticalalignment='center')

    h = "B"
    x = 12
    y = 6.25
    s = 2.819 * 2.819 * a
    plt.scatter(x, y, s=s, c="Plum")
    plt.text(x, y, h, size='x-small', horizontalalignment='center', verticalalignment='center')

    h = "P"
    x = 14.75
    y = 6.25
    s = 0.79 * 0.79 * a
    plt.scatter(x, y, s=s, c="Plum")
    plt.text(x, y, h, size='x-small', horizontalalignment='center', verticalalignment='center')

    title = "MaNGA UWisc Fiber Test Stand Plug Plate Drill Map"
    plt.title(title)
    plt.axis([-5, 30, -5, 20])
    plt.grid(True, which='both')
    plt.xlabel("X (mm)")
    plt.ylabel("Y (mm)")
    plt.tight_layout()
    #plt.show()
    filename = "manga-figure-pp-drill-map"
    fig.savefig(filename + ".png")

######################################################
################# DS9 stuff      #####################
######################################################
def get_vgb_list(rank, cfiber):
    """Given a rank and a c-tech fiber number,
    returns a list of c-tech fiber numbers in its v-groove block."""

    n = ifu_count(rank)

    if rank == 1: # 7 fibers
        vgb_list = range(int((cfiber-1)/n)*n + 1, int((cfiber-1)/n)*n + n + 1)
    elif rank == 2:   # 19 fibers
        vgb_list = range(1, n + 1)
    elif rank == 3:   # 37 fibers (guessing as we don't have one)
        vgb_list = range(1, n + 1)
    elif rank == 4:   # 61 fibers, 30 then 31
        if cfiber <= 30:
            vgb_list = range(1, 30 + 1)
        else:
            vgb_list = range(31, n + 1)
    elif rank == 5:   # 91 fibers, 31 then 30 then 30 (guessing as we don't have one)
        if cfiber <= 30:
            vgb_list = range(1, 30 + 1)
        elif cfiber <= 60:
            vgb_list = range(31, 60 + 1)
        else:
            vgb_list = range(61, n + 1)
    elif rank == 6:   # 127 fibers, 37 then 30 then 30 then 30
        if cfiber <= 30:
            vgb_list = range(1, 30 + 1)
        elif cfiber <= 60:
            vgb_list = range(31, 60 + 1)
        elif cfiber <= 90:
            vgb_list = range(61, 90 + 1)
        else:
            vgb_list = range(91, n + 1)
    else:
        vgb_list = []

    return vgb_list


def clear_regions_in_ds9(ds9name='input'):
    """Clears the regions in DS9."""
    tag = inspect.stack()[0][3] # our name

    cmd = "xpaset -p {} regions delete all".format(ds9name).split()
    print "cmd", cmd
    subprocess.call(cmd)

def place_region_in_ds9(xs, ys, r, text=False, ds9name='input'):
    """Builds a DS9 regions file to display a single region in ds9."""
    tag = inspect.stack()[0][3] # our name

    print "{}: xs {} ys {} r {}".format(tag, xs, ys, r)
    f = open("/tmp/ds9_{}.reg".format(ds9name), "w")
    f.write("circle {} {} 21\n".format(xs + 1, ys + 1))
    if text:
        f.write("text {} {} # text={{{}}}\n".format(xs + 1,ys + 1,text))
    f.close()

def place_vgb_in_ds9(rank, cfiber, spotx, spoty, mapfile, csmap, ds9name='input'):
    """Builds a DS9 regions file to display the fibers in a v-groove block."""
    tag = inspect.stack()[0][3] # our name

    print "{}: rank {} cfiber {} mapfile {}".format(tag, rank, cfiber, mapfile)

    ifu_metrology_map = read_metrology_map(mapfile)

    manga_vgb_list = get_vgb_list(rank, cfiber)
    
    # we refer things to the chosen fiber
    _, sfiber = map_list_and_cfiber(csmap, cfiber)
    ix, pos, _, _, imx0, imy0, _, _, _ = ifu_metrology_map[sfiber]
    print "{}: ix {} pos {} imx0 {} imy0 {}".format(tag, ix, pos, imx0, imy0)

    f = open("/tmp/ds9_{}.reg".format(ds9name), "w")
    for cfiber in manga_vgb_list:

        # look up the desired fiber
        _, sfiber = map_list_and_cfiber(csmap, cfiber)
        #print "{}: use rank {} cfiber {} sfiber {}".format(tag, rank, cfiber, sfiber)
        ix, pos, _, _, imx, imy, _, _, _ = ifu_metrology_map[sfiber]
        #print "{}: ix {} pos {} imx {} imy {}".format(tag, ix, pos, imx, imy)
        xs = spotx + (imx - imx0) + 1
        ys = spoty + (imy - imy0) + 1

        f.write("circle {} {} 21\n".format(xs, ys))
        f.write("text {} {} # text={{{}}}\n".format(xs, ys, cfiber))
    f.close()

def show_regions_in_ds9(ds9name='input'):
    """Sends a prepared DS9 regions file to DS9."""

    clear_regions_in_ds9(ds9name)

    cmd = "xpaset -p {} preserve regions yes".format(ds9name).split()
    print "cmd", cmd
    subprocess.call(cmd)

    cmd = "xpaset -p {} preserve pan yes".format(ds9name).split()
    print "cmd", cmd
    subprocess.call(cmd)

    # we want "cat /tmp/ds9.reg | xpaset ds9 regions"
    cmd1 = "cat /tmp/ds9_{}.reg".format(ds9name).split()
    cmd2 = "xpaset {} regions".format(ds9name).split()
    print "cmd1", cmd1, "cmd2", cmd2
    p1 = Popen(cmd1, stdout=PIPE)
    p2 = Popen(cmd2, stdin=p1.stdout)
    output = p2.communicate()[0]
    print "p2.communicate", output
    
def save_ds9_image(outputname, ds9name='input'):
    """Save the current ds9 image as a jpg. Any regions will be
    burned on the image.
    """
    
    cmd = 'xpaset -p {} saveimage {}.jpeg 100'.format(ds9name,outputname).split()
    print 'cmd',cmd
    subprocess.call(cmd)

ifu_ab_list = [
               (0, 0, 0),
                (1, 1, 0),
                (2, 0, 1),
                (3, -1, 1),
                (4, -1, 0),
                (5, 0, -1),
                (6, 1, -1),
                (7, 2, 0),
                (8, 1, 1),
                (9, 0, 2),
                (10, -1, 2),
                (11, -2, 2),
                (12, -2, 1),
                (13, -2, 0),
                (14, -1, -1),
                (15, 0, -2),
                (16, 1, -2),
                (17, 2, -2),
                (18, 2, -1),
                (19, 3, 0),
                (20, 2, 1),
                (21, 1, 2),
                (22, 0, 3),
                (23, -1, 3),
                (24, -2, 3),
                (25, -3, 3),
                (26, -3, 2),
                (27, -3, 1),
                (28, -3, 0),
                (29, -2, -1),
                (30, -1, -2),
                (31, 0, -3),
                (32, 1, -3),
                (33, 2, -3),
                (34, 3, -3),
                (35, 3, -2),
                (36, 3, -1),
                (37, 4, 0),
                (38, 3, 1),
                (39, 2, 2),
                (40, 1, 3),
                (41, 0, 4),
                (42, -1, 4),
                (43, -2, 4),
                (44, -3, 4),
                (45, -4, 4),
                (46, -4, 3),
                (47, -4, 2),
                (48, -4, 1),
                (49, -4, 0),
                (50, -3, -1),
                (51, -2, -2),
                (52, -1, -3),
                (53, 0, -4),
                (54, 1, -4),
                (55, 2, -4),
                (56, 3, -4),
                (57, 4, -4),
                (58, 4, -3),
                (59, 4, -2),
                (60, 4, -1),
                (61, 5, 0),
                (62, 4, 1),
                (63, 3, 2),
                (64, 2, 3),
                (65, 1, 4),
                (66, 0, 5),
                (67, -1, 5),
                (68, -2, 5),
                (69, -3, 5),
                (70, -4, 5),
                (71, -5, 5),
                (72, -5, 4),
                (73, -5, 3),
                (74, -5, 2),
                (75, -5, 1),
                (76, -5, 0),
                (77, -4, -1),
                (78, -3, -2),
                (79, -2, -3),
                (80, -1, -4),
                (81, 0, -5),
                (82, 1, -5),
                (83, 2, -5),
                (84, 3, -5),
                (85, 4, -5),
                (86, 5, -5),
                (87, 5, -4),
                (88, 5, -3),
                (89, 5, -2),
                (90, 5, -1),
                (91, 6, 0),
                (92, 5, 1),
                (93, 4, 2),
                (94, 3, 3),
                (95, 2, 4),
                (96, 1, 5),
                (97, 0, 6),
                (98, -1, 6),
                (99, -2, 6),
                (100, -3, 6),
                (101, -4, 6),
                (102, -5, 6),
                (103, -6, 6),
                (104, -6, 5),
                (105, -6, 4),
                (106, -6, 3),
                (107, -6, 2),
                (108, -6, 1),
                (109, -6, 0),
                (110, -5, -1),
                (111, -4, -2),
                (112, -3, -3),
                (113, -2, -4),
                (114, -1, -5),
                (115, 0, -6),
                (116, 1, -6),
                (117, 2, -6),
                (118, 3, -6),
                (119, 4, -6),
                (120, 5, -6),
                (121, 6, -6),
                (122, 6, -5),
                (123, 6, -4),
                (124, 6, -3),
                (125, 6, -2),
                (126, 6, -1)
            ]

# the ifu in cartesian coordinates
ifu_xy_list = map(ifu_ab2xy, ifu_ab_list)

# this list gives the corner coordinates (AB space) of a rank=1 ifu.
# we scale it up for higher ranks.
ifu_outline = [(1, 0), (0, 1), (-1, 1), (-1, 0), (0, -1), (1, -1), (1, 0)]

# the color of each successive v-groove block
ifu_vgb_color_list = [ "Cyan", "Magenta", "Yellow", "Lime" ]

# the dictionary of numbering schemes
ifu_scheme_names = {'c':"C-Tech", 's':"Spiral" }

class MFO:
    """ The class is how this module interacts with the rest of the test-stand software.
    It primarily exists to allow the setting of mapfiles
    
    """
    
    def __init__(self):
        
        self.find_file = find_file
        self.clear_regions_in_ds9 = clear_regions_in_ds9
        self.show_regions_in_ds9 = show_regions_in_ds9
        self.save_ds9_image = save_ds9_image
        self.read_metrology_map = read_metrology_map
        self.map_list_and_cfiber = map_list_and_cfiber
        self.read_spike_map = read_spike_map
        
        self.pp_map = None
        self.ifu_map = None
        self.CENTER_X = CENTER_X
        self.CENTER_Y = CENTER_Y
        self.pp_drill_map = map(pp_hole_xy, range(0, 9))
        
        self.configfile = 'manga.cfg'
        
    def set_configfile(self, path):
        """Sets the file that contains the results of the calibration in manga_devices
        """
        
        self.configfile = path
        
        return
            
    def set_pp_map(self, mapfile):
        """Sets the pp_map
        """
        
        self.pp_map = read_pp_fiber_map(mapfile)
        
        return
    
    def set_ifu_map(self, mapfile):
        """Sets the ifu_map
        """
        
        self.ifu_map = read_ifu_fiber_map(mapfile)
        
        return
    
    def get_pp_map(self):
        """Returns a mapping of pp fiber-to-hole number
        """
        
        return self.pp_map
    
    def get_pp_drill_map(self):
        
        return self.pp_drill_map
    
######################################################
################ fiber acquisition ###################
######################################################

    def pp_fiber_locate(self, fiber, mapfile=None):
        """Finds a given plug plate fiber."""
        tag = inspect.stack()[0][3] # our name
    
        config = ConfigParser.RawConfigParser()
        config.read(self.configfile)
        section = "SPOT"
        spotx = config.getfloat(section, "spotx")
        spoty = config.getfloat(section, "spoty")
        print "{}: spotx {} spoty {}".format(tag, spotx, spoty)
        section = "PP_MOTOR_CAL"
        k = config.getfloat(section, "k")
        alpha = math.radians(config.getfloat(section, "alpha"))
        Jx = config.getfloat(section, "Jx")
        Jy = config.getfloat(section, "Jy")
        print "{}: k {} alpha {} deg Jx {} Jy {}".format(tag, k, math.degrees(alpha), Jx, Jy)
        if mapfile is None:
            section = "METROLOGY"
            mapfile = config.get(section, "plug-plate-metrology-file")
    
        print "{}: use metrology map file {}".format(tag, mapfile)
        pp_metrology_map = read_metrology_map(mapfile)
        #print "pp_metrology_map", pp_metrology_map
    
        # we refer things to hole 0
        # The try statement is left in so someone can still use this on pre-production harnesses
        #  for production harnesses the first hole in the plug plate is -1
        try:
            ix, pos, _, _, mx0, my0, _, _, _ = pp_metrology_map[-1]
        except KeyError:
            ix, pos, _, _, mx0, my0, _, _, _ = pp_metrology_map[1]
        print "{}: ix {} pos {} mx0 {} my0 {}".format(tag, ix, pos, mx0, my0)
    
        # where do we want the fiber?
        x = spotx
        y = spoty
        
        # look up the desired fiber
        try:
            ix, pos, _, _, mx1, my1, _, _, _ = pp_metrology_map[fiber]
            print "{}: ix {} pos {} mx1 {} my1 {}".format(tag, ix, pos, mx1, my1)
        except KeyError:
            hole = abs(fiber) - 1
            _, mx, my = self.pp_drill_map[hole]
            place_region_in_ds9(x, y, 21, text="!{}!".format(fiber))
            print "Fiber {} not in metrology file! Moving to hole {} instead".format(fiber, hole)
            return (-1*mx, -1*my)
        
    
        # move hole 0 to the spot
        mx = k * (math.cos(alpha) * x - math.sin(alpha) * y) + Jx
        my = k * (math.sin(alpha) * x + math.cos(alpha) * y) + Jy
        #print "{}: hole {}: mx {} my {}".format(tag, 0, mx, my)
    
        # offset to the desired hole
        mx -= (mx1 - mx0)
        my += (my1 - my0)
    
        mx *= -1    # go back to the left-handed coordinate system
        print "{}: hole {}: mx {} my {}".format(tag, pos, mx, my)
        
        # prepare the DS9 regions file
        place_region_in_ds9(x, y, 21, text=str(fiber))
    
        return (mx, my)
    
    def ifu_fiber_locate(self, rank, cfiber, mapfile=None, mapname="ifu-metrology-file", motorname='IFU_MOTOR_CAL'):
        """Finds a given fiber number in an IFU of the given rank."""
        tag = inspect.stack()[0][3] # our name
    
        config = ConfigParser.RawConfigParser()
        config.read(self.configfile)
    
        section = "SPOT"
        spotx = config.getfloat(section, "spotx")
        spoty = config.getfloat(section, "spoty")
        print "{}: spotx {} spoty {}".format(tag, spotx, spoty)
    
        kx = config.getfloat(motorname, "kx")
        ky = config.getfloat(motorname, "ky")
        alpha = math.radians(config.getfloat(motorname, "alpha"))
        Jx = config.getfloat(motorname, "Jx")
        Jy = config.getfloat(motorname, "Jy")
        print "{}:   kx: {:12.6f}".format(tag, kx)
        print "{}:   ky: {:12.6f}".format(tag, ky)
        print "{}:alpha: {:12.6f} deg".format(tag, math.degrees(alpha))
        print "{}:   Jx: {:12.6f}".format(tag, Jx)
        print "{}:   Jy: {:12.6f}".format(tag, Jy)
    
        if mapfile is None:
            section = "METROLOGY"
            mapfile = config.get(section, mapname)
        print "{}: read metrology map file {}".format(tag, mapfile)
        ifu_metrology_map = read_metrology_map(mapfile)
    
        print cfiber, ifu_metrology_map
        # we refer things to the central fiber
        ix, pos, _, _, x0, y0, _, _, _ = ifu_metrology_map[0]
        print "{}: ix {} pos {} x0 {} y0 {}".format(tag, ix, pos, x0, y0)
    
        # look up the desired fiber
        _, sfiber = map_list_and_cfiber(self.ifu_map, cfiber)
        print "{}: use rank {} cfiber {} sfiber {}".format(tag, rank, cfiber, sfiber)
        ix, pos, _, _, x, y, _, _, _ = ifu_metrology_map[sfiber]
        print "{}: ix {} pos {} x {} y {}".format(tag, ix, pos, x, y)
    
        # where is the point Q on the CCD
        # such that moving fiber 0 to Q
        # will result in the desired fiber being on the spot?
    
        Qx = spotx - (x - x0)
        Qy = spoty - (y - y0)
        print "{}: Qx {} Qy {}".format(tag, Qx, Qy)
        mx = kx * (math.cos(alpha) * Qx - math.sin(alpha) * Qy) + Jx
        my = ky * (math.sin(alpha) * Qx + math.cos(alpha) * Qy) + Jy
        print "{}: mx {} my {}".format(tag, mx, my)
    
        # prepare the DS9 regions file
        place_vgb_in_ds9(rank, cfiber, spotx, spoty, mapfile, self.ifu_map)
    
        return (mx, my)

#     def ifu_fiber_locate(self, rank, cfiber, mapfile=None):
#         """Finds a given fiber number in an IFU of the given rank."""
#         tag = inspect.stack()[0][3] # our name
#     
#         config = ConfigParser.RawConfigParser()
#         config.read(self.configfile)
#     
#     #    section = "IFU_AOP"
#     #    c = config.getfloat(section, "c")
#     #    theta = math.radians(config.getfloat(section, "theta"))
#     #    sx = config.getfloat(section, "sx")
#     #    sy = config.getfloat(section, "sy")
#     #    print "{}: c {} theta {} deg sx {} sy {}".format(tag, c, math.degrees(theta), sx, sy)
#     
#         section = "SPOT"
#         spotx = config.getfloat(section, "spotx")
#         spoty = config.getfloat(section, "spoty")
#         print "{}: spotx {} spoty {}".format(tag, spotx, spoty)
#     
#         section = "IFU_MOTOR_CAL"
#         k = config.getfloat(section, "k")
#         alpha = math.radians(config.getfloat(section, "alpha"))
#         Jx = config.getfloat(section, "Jx")
#         Jy = config.getfloat(section, "Jy")
#         print "{}: k {} alpha {} deg Jx {} Jy {}".format(tag, k, math.degrees(alpha), Jx, Jy)
#     
#         if mapfile is None:
#             section = "METROLOGY"
#             mapfile = config.get(section, "ifu-metrology-file")
#         print "{}: read metrology map file {}".format(tag, mapfile)
#         ifu_metrology_map = read_metrology_map(mapfile)
#     #    print "ifu_metrology_map:"
#     #    for ix, item in enumerate(ifu_metrology_map):
#     #        print "{}: ix {} item {}".format(tag, ix, item)
#     
#         # we refer things to the central fiber
#         ix, pos, _, _, imx0, imy0, _, _, _ = ifu_metrology_map[0]
#         print "{}: ix {} pos {} imx0 {} imy0 {}".format(tag, ix, pos, imx0, imy0)
#     
#         # look up the desired fiber
#         _, sfiber = map_list_and_cfiber(self.ifu_map, cfiber)
#         print "{}: use rank {} cfiber {} sfiber {}".format(tag, rank, cfiber, sfiber)
#         ix, pos, _, _, imx, imy, _, _, _ = ifu_metrology_map[sfiber]
#         print "{}: ix {} pos {} imx {} imy {}".format(tag, ix, pos, imx, imy)
#     
#         dx = spotx - (imx - imx0)
#         dy = spoty - (imy - imy0)
#         print "{}: dx {} dy {}".format(tag, dx, dy)
#         mx = k * (math.cos(alpha) * dx - math.sin(alpha) * dy) + Jx
#         my = k * (math.sin(alpha) * dx + math.cos(alpha) * dy) + Jy
#         print "{}: mx {} my {}".format(tag, mx, my)
#     
#         mx *= -1    # go back to left-handed coordinate system
#     
#         # prepare the DS9 regions file
#         place_vgb_in_ds9(rank, cfiber, spotx, spoty, mapfile, self.ifu_map)
#     
#         return (mx, my)
