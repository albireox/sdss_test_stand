#!/usr/bin/env python
"""Fiber tester

History:
2008-05-30 ROwen    Version 1
2008-05-30 ROwen    Version 1.1; add CommonData; handle broken fibers better.
2008-06-02 ROwen    Version 1.1.1: increased range a bit; center after each fiber.
2008-06-26 ROwen    Version 1.1.2: changed log file directory to allow bundling as an Mac application.
2008-06-27 ROwen    Version 1.2:
                    - Improved algorithm for deciding where to move the linear stage.
                    - Improved detecting <return> key.
                    - Added support for Command-Q to quit (with verification if running).
                    - Log window now grows when window is resized.
2008-08-01 ROwen    Version 1.2.1: increased ammeter sensitivity by 10x due to new smaller source fiber.
2008-08-06 ROwen    Version 1.3:
                    - Added voice prompts.
                    - Lowered threshold for finding fiber.
					- List minimum throughput at the end.
					- Improved FiberData to handle broken fibers gracefully.
					- List v-groove number in fiber throughput (Thru) data.
					- Cache motor position so we don't have to enable the motor if making a null move.
					- Fixed bug whereby broken fibers were not shown at the end.
					- Fixed bug whereby instructional widget stayed red once it went red.
2008-12-11 ROwen    Version 1.4: added test for fibers being in order.
2008-01-23 ROwen    Version 1.5: improved handling of communication errors.
2009-03-23 ROwen    Version 1.5.1:
                    - Updated FiberSep from 0.240 to 0.260 for BOSS production fibers.
                    - Added lower limit for source current as a sanity check, to avoid odd behavior
                      from a disconnected BNC cable or a burned out light bulb
                    - Increased YRangeSteps from 30,000 to 39,000 since the new v-groove blocks are wider.
2009-06-11 ROwen    Version 1.5.2: reduce minimum source current to 20.0.
2010-05-11 ROwen    Version 1.5.4: If beam angle is so far off that no current was measured at desired
                    beam center, set measured throughput to nan instead of raising an exception.
2010-05-11 ROwen    Version 1.5.5: If beam angle is too far off, set throughput to 0, not nan.
2010-07-30 ROwen    Version 1.5.6: Updated some parameters:
                    Changed SlitRadius from 1051.6 to 1052.41 (APOGEE final figure).
                    Corrected FiberApDist from 75.181 to 78.32 (it was wrong).
2010-08-04 ROwen    Version 1.5.7: Corrected FiberApDist from 28.32 to 80.86 (I had left out the thickness
                    of the stop) and improved documentation of the value.
"""
import bisect
import math
import pwd
import os
import SimpleDialog
import sys
import time
import ConfigParser
import Config as ADEC
import manga_devices
import Imager
import Tkinter as tk
import time
import pyfits
from yanny import yanny
import macspeechX
import numpy
import RO.Constants
import RO.OS
import RO.PhysConst
import RO.ScriptRunner
import RO.Wdg
#import pychecker.checker # uncomment to run pychecker

# jwp
import subprocess

__version__ = "1.5.7"

class CommonData(object):
    """Information about the system and whole harness.
    """
    # constants
    # jwp
    """here be defaults used by the GUI"""
    NumFibers = 127      # number of fibers in a harness
    StartFiber = 1      # the C-tech fiber you want to start at
    startSlit = 0       # the output slit to start at
    HexRank = 2         # the rank of the head hexagon (i.e. 19 fiber bundles have rank=2)
    FiberSep = 0.204        # distance between adjacent fibers in v-groove block, in mm
    sessionID = ''
    harnessType = None
    goodMetro = True
    ferruleCode = 0
    FiberList = []


    if pwd.getpwuid(os.getuid())[0] == 'SDSSfiber2':
        location = 'UWash'
        teststand = 'ts2'
    elif pwd.getpwuid(os.getuid())[0] == 'SDSSfiber':
        location = 'UWisc'
        teststand = 'ts1'
    else:
        print 'WARNING: Unrecognized location. Setting constants to UWisc'
        location = 'UWisc'


    #Some constants that are unlikely to ever change

    BigStepSize = 1.0 # size of step while searching for fiber
    Y2SmallStepSize = 0.25
    SmallStepSize = 0.5 # size of step while measuring profile
    X2minLimit = 5.0 # hardware limit of X2, in mm
    X2maxLimit = 30.0 # hardware limit of X2, in mm
    X2minLimit = 5.5 # hardware limit of X2, in mm
    X2maxLimit = 25.5 # hardware limit of X2, in mm
    Y2minLimit = 0.0
    Y2maxLimit = 24.5
    startSearchPos = 11.7
    #YstartSearchPos = 0.0

    vg1num = 0
    vg1space = 0.0
    vg2num = 0
    vg2space = 0.0
    vg3num = 0
    vg3space = 0.0
    vg4num = 0
    vg4space = 0.0

    # Read in constants from the user-editable config file
    defaults = ConfigParser.ConfigParser()
    defaults.read(os.path.expanduser("~/SDSSbench/manga_defaults.ini")) #Yes, magic number. Whatever

    slitHeight = defaults.getfloat("Misc", "slitHeight")
    x2MetroPos = defaults.getfloat("Metrology", "x2MetroPos")
    y2MetroPos = defaults.getfloat("Metrology", "y2MetroPos")
    x1MetroPos = defaults.getfloat("Metrology", "x1MetroPos")
    y1MetroPos = defaults.getfloat("Metrology", "y1MetroPos")
    y2StartPos = defaults.getfloat("Starting Positions", "y2StartPos")
    startSearchPos = defaults.getfloat("Starting Positions", "startSearchPos")
    defaultSearchPos = defaults.getfloat("Starting Positions", "startSearchPos")
    x1PPMetroPos = defaults.getfloat("Metrology", "x1PPMetroPos")
    y1PPMetroPos = defaults.getfloat("Metrology", "y1PPMetroPos")

    x1HomePos = defaults.getfloat("Misc", "x1HomePos")
    y1HomePos = defaults.getfloat("Misc", "y1HomePos")
    x1Offset = defaults.getfloat("Metrology", "x1Offset")
    y1Offset = defaults.getfloat("Metrology", "y1Offset")
    x1MinnieMove = defaults.getfloat("Misc", "x1MinnieMove")
    x1PPOffset = defaults.getfloat("Metrology", "x1PPOffset")
    y1PPOffset = defaults.getfloat("Metrology", "y1PPOffset")

    MinMeanThroughput = defaults.getfloat("Throughput Requirements", "MinMeanThroughput")   # minimum throughput averaged for all fibers, in %
    MinSingleThroughput = defaults.getfloat("Throughput Requirements", "MinSingleThroughput")  # minimum throughput for any one fiber, in %
    MaxFracSourceChange = defaults.getfloat("Throughput Requirements", "MaxFracSourceChange")   # maximum reasonable fractional change in source current
    MinSourceCurrent = 10.0     # mininum source current in nanoamps; be generous, because
        # this is mainly used to defend against a disconnected cable or burned out light bulb

    # bench 1
#     if location == 'UWisc':
#         teststand = 'ts1'
#         x2MetroPos = 48.5
#         y2MetroPos = 11.23
#         x1MetroPos = 25.27
#         y1MetroPos = 11.35
#         y2StartPos = 7.74
#         y2StartPos = 1.4#1.42#0.641
#         x1PPMetroPos = 12.57
#         y1PPMetroPos = 22.2

        #for the fiducial cable
#         y1MetroPos = 12.45
#         x1MetroPos = 25.243
#         y2StartPos = 9.5
        #y2StartPos = 2.3


    # bench 2

#     else:
#         teststand = 'ts2'
#         x2MetroPos = 48.5
#         y2MetroPos = 12.275
#         x1MetroPos = 25.67
#         y1MetroPos = 11.9
#         y2StartPos = 0.5
#         x1PPMetroPos = 13.57
#         y1PPMetroPos = 22.60

    ResultsArray = numpy.array([])

    FiberApDist = 4.3 + 77.32 # distance between end of v-groove block and precision aperture inside detector, in mm
        # the face of the vgroove block is 4.3mm from the precision aperture
        ## the stop at the front of the stage is 0.1" = 2.54 mm thick
        ## the gap between the stage and the detector is 1.0 mm
        # the precision aperture is 77.32 mm from the front of the detector
    # FiberSep = 0.350        # distance between adjacent fibers in v-groove block, in mm
    # SlitRadius = 1052.41    # radius of slit, in mm
    # FiberSep = 0.240        # distance between adjacent fibers in v-groove block, in mm
    # jwp

    SlitRadius = 640.1    # radius of slit, in mm
    DeltaAngle = FiberSep / SlitRadius  # angular separation between fibers, in radians

    BeamEdgeFrac = 0.8  # fractional drop from max for edge of beam

    LogFileDir = os.getcwd()

    def __init__(self):
        self.harnessID = None
        self.sourceCurrentList = []
        self.meanSourceCurrent = numpy.nan
        self.meanXPeak = numpy.nan

    def setharnessID(self, harnessID):
        self.harnessID = harnessID

    def addSourceMeas(self, sourceCurrent):
        """Add a source current measurement and update mean source current accordingly.
        """
        self.sourceCurrentList.append(float(sourceCurrent))
        self.meanSourceCurrent = numpy.mean(self.sourceCurrentList)

    def setMeanXPeak(self, meanXPeak):
        """Set mean Y peak position for all fibers"""
        self.meanXPeak = float(meanXPeak)

    def getFracSourceChange(self):
        """Get fractional change in source intensity from start to finish.

        Returns (last_meas - first_meas) / average_meas

        Note that this pretty much assumes there were only two source measurements;
        otherwise the returned value doesn't make much sense.
        """
        return (self.sourceCurrentList[-1] - self.sourceCurrentList[0]) / self.meanSourceCurrent


class FiberData(object):
    """Information about one fiber in a harness.

    Units:
    - position is in mm
    - current is in nanoamps
    - angle is in radians
    """
    def __init__(self, num, slitnum, commonData):
        self.num = int(num)
        self.slitnum = int(slitnum)
        self.commonData = commonData
        self.measPosCurrList = [] # list of (position, current)
        self.fitBeamBegPos = numpy.nan  # fit stage position at which end of beam is measured
        self.fitBeamEndPos = numpy.nan  # fit stage position at which start of beam is measured
        self.fitBeamCtrPos = numpy.nan  # fit stage position at which beam is allegedly centered (mean of beg and end)
        self.desBeamCtrPos = numpy.nan  # desired stage position at which beam is centered
        self.desAngle = numpy.nan
        self.fiberApDist = numpy.nan
        self.vgNum = numpy.nan
        self.maxCurr = numpy.nan
        self.maxCurrPos = 0
        self.isBroken = False

    def addMeas(self, pos, curr):
        """Add a measurement of current vs position.

        Note: the data is re-sorted by increasing position after every add
        """
        self.measPosCurrList.append((float(pos), float(curr)))
        self.measPosCurrList.sort()
        if not (curr < self.maxCurr): # use not < because max starts out nan
            self.maxCurr = curr
            self.maxCurrPos = pos

    def computeBeamWidth(self):
        """Compute beam width, in degrees"""
        return math.atan((self.fitBeamEndPos - self.fitBeamBegPos) / self.commonData.FiberApDist) / RO.PhysConst.RadPerDeg

    def computeFitCurr(self, pos):
        """Compute current at a specified y position, using linear interpolation.

        Return 0.0 if fiber is broken.
        """
        if self.isBroken:
            return 0.0
        posList, currList = zip(*self.measPosCurrList)

        """ADE note: Can probably replace all following lines with call to
            np.interp

            Edit: We'll keep it this way b/c it will raise an error when trying
            to extrapolate rather than interpolate. numpy.interp will silently
            extrapolate"""
        endInd = bisect.bisect_left(posList, pos)
        if endInd <= 0 or endInd >= len(posList):
            raise RuntimeError("pos=%s out of range [%s, %s]".format(pos, posList[0], posList[-1]))
        begInd = endInd - 1

        slope = (currList[endInd] - currList[begInd]) / (posList[endInd] - posList[begInd])
        retVal = currList[begInd] + (slope * (pos - posList[begInd]))
        return retVal

    def computeThroughput(self, pos):
        """Compute throughput (in %) at a specified y position.
        """
        fitCurr = self.computeFitCurr(pos)
        return fitCurr * 100.0 / self.commonData.meanSourceCurrent

    def fitBeam(self):
        """Find edges of beam (and center = average of edge positions).

        If there is no data then sets isBroken True.

        The data look like a plateau with fairly sharp cutoffs.
        """
        #if len(self.measPosCurrList) < 1:
         #   raise RO.ScriptRunner.ScriptError("Too few measurements to fit")

        posList, currList = zip(*self.measPosCurrList)
        maxCurr = numpy.max(currList)
        if maxCurr < (self.commonData.meanSourceCurrent * 0.1):
            # throughput is less than 10%; fiber is essentially broken
            # use the edges as the peak: it's as good as anything and obviously bad
            self.fitBeamBegPos = posList[0]
            self.fitBeamEndPos = posList[-1]
        else:
            threshCurr = self.commonData.BeamEdgeFrac * maxCurr
            if currList[0] >= threshCurr:
                raise RO.ScriptRunner.ScriptError("I can't find the edge of the fiber. Make sure the X2 limits are set correctly.")
                return False

            foundBeg = False
            foundEnd = False
            for ind, curr in enumerate(currList):
                if not foundBeg:
                    if curr >= threshCurr:
                        foundBeg = True
                        self.fitBeamBegPos = self.interpolatePosFromCurrent(threshCurr, ind)
                        #print "beg ind=%s; self.fitBeamBegPos=%s" % (ind, self.fitBeamBegPos)
                elif curr < threshCurr:
                    foundEnd = True
                    self.fitBeamEndPos = self.interpolatePosFromCurrent(threshCurr, ind)
                    #print "end ind=%s; self.fitBeamEndPos=%s" % (ind, self.fitBeamEndPos)
                    break
            if not foundEnd:
                raise RO.ScriptRunner.ScriptError("Did not find right side of peak")

        self.fitBeamCtrPos = (self.fitBeamBegPos + self.fitBeamEndPos) / 2.0
        #print "fitBeamBegPos=%s; fitBeamEndPos=%s; fitBeamCtrPos=%0.1f" % (self.fitBeamBegPos, self.fitBeamEndPos, self.fitBeamCtrPos)

        self.isBroken = False

        return True

    def getVGNumSortKey(self):
        """Return a value for sorting by vg number"""
        return (self.slitnum, self.fitBeamCtrPos)

    def interpolatePosFromCurrent(self, current, endInd):
        """Interpolate between two points, going from current to position.

        Returns nan if the fiber is broken or the current is the same at both points.
        """
        if self.isBroken:
            return numpy.nan

        posList, currList = zip(*self.measPosCurrList)
        if endInd <= 0 or endInd >= len(posList):
            raise RuntimeError("pos={} out of range [{}, {}]".format(endInd, posList[0], posList[-1]))
        begInd = endInd - 1

        try:
            slope = (posList[endInd] - posList[begInd]) / (currList[endInd] - currList[begInd])
        except ZeroDivisionError:
            return numpy.nan
        retVal = posList[begInd] + (slope * (current - currList[begInd]))
        return retVal

    def setVGBlockInfo(self, vgNum):
        """Set v-groove block position and compute desBeamCtrPos

        Inputs:
        - vgNum: v-groove groove number, starting from 1

        Note: fiber vgNum 1 is closest to the user when running the fiber tester
        """
        self.vgNum = int(vgNum)


class ScriptClass(object):
    """Fiber Tester script.

    At init time enable picoammeter
    When quitting Python disable picoammeter

    The algorithm:
    - Measure Isource
    - For fiber 1 through NumFibers:
        - Wait for fiber to be installed
        - Measure fiber I(x) at various x
        - Fit a parabola to I(x)
        - Compute xpeak and record I(xpeak)
    - Measure Isource again; make sure it has not changed too much
    - Sort fibers by increasing xpeak
    - Compute xctr for each fiber
    - Compute I(xctr) for each fiber
    - Display:
      - Throughput at xctr
      - Pass/fail
      - Throughput at xpeak and xpeak - xctr
      - Average Io and %change
      and write all this (and possibly each measurement) to a file with the harness # and date
    """

    tagGood = "good"
    tagWarning = "warning"
    tagError = "error"

    def __init__(self, sr):
        """The setup script; run once when the script runner
        window is created.
        """
        self.sr = sr
        self.sr.debug = False
        self.helpURL = None
        self.DoAcq = tk.BooleanVar()
        self.DoAcq.set(True)
        self.DoTput = tk.BooleanVar()
        self.DoTput.set(True)

        self.commonData = CommonData()
        self.sr.master.winfo_toplevel().wm_title('{} - Fiber tester master control'.format(self.commonData.location))
        self.createWdg()
        self.cw = ADEC.ConfigWindow(self.sr.master, self.sessionWdg)
        self.md = manga_devices.MangaDevices()

        self.initAll()

    def createWdg(self):
        """Create and grid the widgets."""

        gr = RO.Wdg.Gridder(self.sr.master, sticky="ew")

        self.sessionWdg = RO.Wdg.StrEntry(
            self.sr.master,
            label = 'Session ID',
            width = 20,
            helpText = 'Prefix for all generated files')
        gr.gridWdg(self.sessionWdg.label,self.sessionWdg,col=0,row=0,colSpan=2,sticky='w')

        self.bigButton = RO.Wdg.Button(
            self.sr.master,
            text = 'PRESETS',
            callFunc = self.launch_presets,
            helpText = 'Start a run with a standard productin harness',
            helpURL = self.helpURL)
        gr.gridWdg(self.bigButton,sticky='w',col=0,row=1)

        self.big19label = RO.Wdg.Label(self.sr.master)
        self.big19label.set('<- Quickrun a production harness')
        gr.gridWdg(self.big19label,col=2,row=1,sticky='w')

        self.configButton = RO.Wdg.Button(
            self.sr.master,
            text = 'Configuration',
            callFunc = self.getConfiguration,
            helpText = 'Open Configuration Utility',
            helpURL = self.helpURL)
        gr.gridWdg(self.configButton,sticky='w',col=0,row=2)

        self.homeButton = RO.Wdg.Button(
            self.sr.master,
            text = 'Home Stages',
            callFunc = self.homeAll,
            helpText = 'Initialize all stages',
            helpURL = self.helpURL)
        gr.gridWdg(self.homeButton,sticky='w',col=0,row=3)

        self.Acqcheck = RO.Wdg.Checkbutton(
            self.sr.master,
            var = self.DoAcq,
            helpText = 'Perform metrology/acquisition',
            helpURL = self.helpURL)
        gr.gridWdg(self.Acqcheck,col=1,row=2,sticky='w')

        self.Tputcheck = RO.Wdg.Checkbutton(
            self.sr.master,
            var = self.DoTput,
            helpText = 'Perform throughput measurements',
            helpURL = self.helpURL)
        gr.gridWdg(self.Tputcheck,col=1,row=3,sticky='w')

        self.Acqlabel = RO.Wdg.Label(
            self.sr.master
            )
        self.Acqlabel.set('Do Acquisition/metrology')
        gr.gridWdg(self.Acqlabel,col=2,row=2,sticky='e')

        self.Tputlabel = RO.Wdg.Label(
            self.sr.master
            )
        self.Tputlabel.set('Do Throughput Measurements')
        gr.gridWdg(self.Tputlabel,col=2,row=3,sticky='e')

        self.instructWdg = RO.Wdg.StrLabel(
            master = self.sr.master,
#            readOnly = True,
            width = 20,
            helpText = "Instructions",
            helpURL = self.helpURL,
        )
        self.userContinueBtn = RO.Wdg.Button(
            self.sr.master,
            text = "Continue",
            callFunc = self.handleReturn,
            helpText = "Press or type <return> to continue",
            helpURL = self.helpURL,
        )
        gr.gridWdg(False, (self.instructWdg, self.userContinueBtn), colSpan=2, sticky="w",row=4)
        self.userContinueBtn.grid_remove()

        # table of measurements
        self.logWdg = RO.Wdg.LogWdg(
            master = self.sr.master,
            height = 27,
            width = 6 * 8,
            helpText = "Log of throughput measurements",
            helpURL = self.helpURL,
            relief = "sunken",
            bd = 2,
        )
        self.logWdg.text.tag_configure(self.tagError, foreground="red")
        self.logWdg.text.tag_configure(self.tagWarning, foreground="blue")
        self.logWdg.text.tag_configure(self.tagGood, foreground="dark green")
        gr.gridWdg(False, self.logWdg, sticky="nsew", colSpan = 10)
        self.sr.master.grid_rowconfigure(gr.getNextRow() - 1, weight=1)
        self.sr.master.grid_columnconfigure(9, weight=1)

        self.sr.master.winfo_toplevel().bind("<Return>", self.handleReturn)
        self.logWdg.text.bind("<Return>", self.handleReturn) # work around read-only bindings

        self.sr.master.winfo_toplevel().bind("<Command-q>", self.doQuit)
        self.logWdg.text.bind("<Command-q>", self.doQuit) # work around read-only bindings

        self.sr.master.winfo_toplevel().protocol("WM_DELETE_WINDOW",self.doQuit)


    def doQuit(self, evt=None):
        if self.sr.isExecuting():
            d = SimpleDialog.SimpleDialog(self.sr.master.winfo_toplevel(),
                text="Quit now, while running?",
                buttons=["Yes", "No"],
                default=0,
                cancel=1,
                title="Really Quit?",
            )
            if d.go():
                return

        self.md.clear_regions_in_ds9('input')
        self.md.clear_regions_in_ds9('output')
        self.md.proc_gige_loop_cmds('input', 'q')
        self.md.proc_gige_loop_cmds('output', 'q')
        subprocess.Popen([os.path.expanduser('~/git/MaNGA/manga/ADE_trunk/cleanup.sh')],
                         stdin=None,stdout=None,stderr=None)
        self.sr.master.quit()

    def launch_presets(self, evt=None):

        self.bb = ADEC.BigButton(self.sr.master,self)
        self.sr.master.wait_window(self.bb.top)

        """This is the second way this might work. See Config.BigButton for the other"""
        self.sr.start()

    def doClear(self, wdg=None):
        self.logWdg.clearOutput()

    def end(self, sr):
        self.userContinueBtn.grid_remove()

    def getEntryNum(self, wdg, errStr=None):
        """Return the numeric value of a widget, or raise ScriptError if blank.
        """
        numVal = wdg.getNumOrNone()
        if numVal != None:
            return numVal
        if errStr == None:
            errStr = wdg.label + " not specified"
        raise self.sr.ScriptError(errStr)

    def getConfiguration(self, evt=None):
        """Opens the configuration window and sets the name for the config .INI file
        to be written
        """

        self.cw.show()
        self.cw.set_sessionID(self.sessionWdg.getString())


    def handleReturn(self, evt=None):
        if not self.sr.isExecuting():
            self.sr.start()
        elif self.sr._userWaitID != None:
            self.sr.resumeUser()

    def initAll(self):
        """Initialize and/or create variables and log
        """
        # initialize widgets
        self.doClear()
        self.instructWdg.clear()
        self.userContinueBtn.grid_remove()

        # clear ds9
        self.md.clear_regions_in_ds9('input')
        self.md.clear_regions_in_ds9('output')

        # initialize shared variables
        self.commonData = CommonData()
        self.fiberDataList = [] # list of FiberData object
        self.logFileName = None
        self.logFile = None
        self.currPosSteps = None

        #just to be safe
        os.chdir(os.path.expanduser('~'))

    def logFiberFit(self, fiberData):
        """Log the fit of current vs position for a fiber"""
        # sanity check throughput; if it's too low now then the harness certainly fails
        # (but if it passes now, the throughput may still be too low once we figure out where the beam center should be).
        fiberOK = fiberData.tput_list[0] >= self.commonData.MinSingleThroughput
        self.logMsg("\tFiber#\tPos(mm)\tttput3\tttput4\tttput5") # header
        dataStr = "Fit\t{}\t{:0.2f}\t{:0.2f}\t{:0.2f}\t{:0.2f}".\
            format(fiberData.fibnum, fiberData.x2_center, fiberData.tput_list[0], fiberData.tput_list[1], fiberData.tput_list[2])
        if fiberOK:
            self.logMsg(dataStr, tag=self.tagGood)
        else:
            self.logMsg(dataStr, tag=self.tagError)
        self.logFile.write(dataStr + "\n")

    def logFiberMeas(self, fiberData, pos, curr):
        """Log a fiber measurement.
        """
        measNum = len(fiberData.measPosCurrList)

        dataStr = "Meas\t{}\t{}\t{:0.2f}\t{:0.2f}".format(measNum, fiberData.fibnum, pos, curr)
        self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

    def logThroughputs(self, fiberDataList, badFiberList):
        """Log throughput information for all fibers.

        Inputs:
        - fiberDataList: list of fiber data
        """
        self.logMsg("\tFiber#\tVGrv#\tThru3\tThru4\tThru5")
        thruList = numpy.empty(3)
        badReasonList = []
        nBadFibers = 0
        badOrder = False
        lastNum = 0
        nextNum = numpy.inf
        lastVG = 0
        nextVG = numpy.inf
        for i, fiberData in enumerate(fiberDataList):
            gbu = 0
            try:
                fiberData.compute_throughput(self.commonData.directImages.avg_flux)
                thru = fiberData.tput_list
            except Exception:
                print Exception, type(Exception)
                thru = [0.0, 0,0]
            thruList = numpy.vstack((thruList,thru))
            dataStr = "Thru\t{}\t{}\t{:0.2f}\t{:0.2f}\t{:0.2f}".format(fiberData.fibnum, fiberData.vgNum, thru[0], thru[1], thru[2])
            if thru[0] < self.commonData.MinSingleThroughput:
                gbu += 1
                nBadFibers += 1
                badFiberList.append((i,fiberData.fibnum))
                self.logMsg(dataStr, tag=self.tagError)
            else:
                self.logMsg(dataStr)
            self.logFile.write(dataStr + "\n")

            try:
                nextNum = fiberDataList[i+1].fibnum
                nextVG = fiberDataList[i+1].vgNum
            except IndexError:
                #We're asking about the last fiber
                nextNum += 1
                nextVG += 1

            if fiberData.vgNum < lastVG or fiberData.vgNum > nextVG:
                badOrder = True
                gbu += 2

            lastNum = fiberData.fibnum
            lastVG = fiberData.vgNum

            idx = numpy.where(self.commonData.ResultsArray['fnum'] == fiberData.fibnum)[0][0]
            try:
                print 'previous tput is', self.commonData.ResultsArray[idx]['tput']
            except exception:
                pass
            self.commonData.ResultsArray[idx]['gbu'] = gbu
            self.commonData.ResultsArray[idx]['tput'] = thru[0]

        thruList = thruList[1:]
        minThru = numpy.min(thruList,axis=0)
        dataStr = "MinThru\t{:0.2f}\t{:0.2f}\t{:0.2f}".format(*minThru)
        self.logMsg("\tMin(%)")
        if nBadFibers > 0:
            self.logMsg(dataStr, tag=self.tagError)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

        badAve = False
        aveThru = numpy.mean(thruList,axis=0)
        stdDevThru = numpy.std(thruList,axis=0)
        dataStr = "AveThru\t{:0.2f}\t{:0.2f}\t{:0.2f}".format(*aveThru)
        self.logMsg("\tAve(%)")
        if aveThru[0] < self.commonData.MinMeanThroughput:
            badAve = True
            self.logMsg(dataStr, tag=self.tagError)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

        if nBadFibers > 0:
            badReasonList.append("{} fibers have throughput < {:0.1f}".format(nBadFibers, self.commonData.MinSingleThroughput))
        if badAve:
            badReasonList.append("Average throughput < {:0.1f}".format(self.commonData.MinMeanThroughput))
        if badOrder:
            badReasonList.append("Fibers are out of order")

        self.logMsg("")
        if badReasonList:
            dataStr = "Harness is Bad:"
            self.logMsg(dataStr, tag=self.tagError)
            for reasonStr in badReasonList:
                self.logMsg("- " + reasonStr, tag=self.tagError)
            self.sr.showMsg(dataStr, severity=RO.Constants.sevError)
        else:
            dataStr = "Harness is Good"
            self.logMsg(dataStr, tag=self.tagGood)
            self.sr.showMsg(dataStr)

    def logAveSource(self):
        """Log average source current and percent change
        """
        aveSrc = self.commonData.directImages.avg_flux
        pctChange = self.commonData.directImages.flux_change * 100.0
        dataStr = "AveSrc\t{:0.2e}\t{:0.2e}\t{:0.2e}".format(*aveSrc)
        if numpy.any(abs(pctChange) > self.commonData.MaxFracSourceChange * 100.0):
            self.logMsg(dataStr + " excessive change!", tag=self.tagWarning)
        else:
            self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")

    def logMsg(self, msgStr, tag=None):
        """Append a message to the log, adding a final \n"""
        if tag:
            tags = (tag,)
        else:
            tags = ()
        self.logWdg.addOutput(msgStr + "\n", tags=tags)
        if self.sr.debug:
            try:
                print msgStr
            except Exception:
                print repr(msgStr)

    def recordUserParams(self):
        """
        Reads in a configuration file created by a ConfigWindow object and sets
        relevant values in self.CommonData

        """
        options = self.cw.get_options()

        if not options:
            self.setInstructWdg("Configure the Session ->",RO.Constants.sevError)
            self.getConfiguration()
            return False

        harnessID = options.get('Global Info','harnessID')
        self.commonData.setharnessID(harnessID)

        savepath = options.get('Global Info','savepath')
        self.commonData.LogFileDir = savepath
        self.md.set_workingdir(savepath)

        calibfile = options.get('Global Info','calibration_file')
        self.md.set_configfile(calibfile)

        startFiber = options.getint('Harness Info','startfiber')

        harnessType = options.get('Harness Info','harnessType')
        self.commonData.harnessType = harnessType

        slitnum = 0

        if harnessType == 'IFU':

            hexRank = options.getint('Harness Info','rank')
            self.commonData.HexRank = hexRank

            mapfile = options.get('Harness Info','mapfile')
            self.md.set_ifu_map(mapfile)

            bigskylist = []
            spacelist = [0,0,0,0]
            slitDict = {}
            fibers_per_slitlist = [0,0,0,0]
            numFibers = 0
            for i, (namedata, spacedata, name) in enumerate(zip([self.commonData.vg1num,
                                                 self.commonData.vg2num,
                                                 self.commonData.vg3num,
                                                 self.commonData.vg4num],
                                                [self.commonData.vg1space,
                                                 self.commonData.vg2space,
                                                 self.commonData.vg3space,
                                                 self.commonData.vg4space],
                                                ['VG1','VG2','VG3','VG4'])):
                try:
                    num = options.getint('Harness Info','{}_numfibers'.format(name))
                    namedata = num
                    spacedata = options.getfloat('Harness Info','{}_spacing'.format(name))
                    spacelist[i] = spacedata
                    fibers_per_slitlist[i] = num
                    vglist = range(startFiber + numFibers, num + numFibers + startFiber)
                    slitDict[int(name[2]) - 1] = vglist
                    numFibers += num
                    if startFiber in vglist:
                        slitnum = int(name[2]) - 1

                except ConfigParser.NoOptionError:
                    continue

            fiberList = range(startFiber, startFiber + numFibers)


        elif harnessType == 'single' or harnessType == 'MCAL':
            numFibers = options.getint('Harness Info','numFibers')

            fiberSep = options.getfloat('Harness Info','fiberSep')
            self.commonData.FiberSep = fiberSep

            mapfile = options.get('Harness Info','plugfile')
            self.md.set_pp_map(mapfile)

            bigskylist = []
            slitDict = {0: range(startFiber, numFibers + 1)}
            spacelist = [fiberSep]
            fibers_per_slitlist = [numFibers]
            fiberList = range(startFiber, numFibers + 1)


        elif harnessType == 'PROD':

            hexRank = options.getint('Harness Info','rank')
            self.commonData.HexRank = hexRank

            ifu_mapfile = options.get('Harness Info','ifu_mapfile')
            self.md.set_ifu_map(ifu_mapfile)

            startFiber = options.getint('Harness Info','startfiber')

            sky_mapfile = options.get('Harness Info','sky_mapfile')
            self.md.set_pp_map(sky_mapfile)

            self.commonData.ferruleCode = options.getint('Harness Info','ferrulecode')

            spacelist = [0,0,0,0]
            fiberList = []
            slitDict = {}
            fibers_per_slitlist = [0,0,0,0]
            bigskylist = []
            numFibers = 0
            numSky = 0
            for i, (namedata, spacedata, name) in enumerate(zip([self.commonData.vg1num,
                                                 self.commonData.vg2num,
                                                 self.commonData.vg3num,
                                                 self.commonData.vg4num],
                                                [self.commonData.vg1space,
                                                 self.commonData.vg2space,
                                                 self.commonData.vg3space,
                                                 self.commonData.vg4space],
                                                ['VG1','VG2','VG3','VG4'])):
                try:
                    num = options.getint('Harness Info','{}_numfibers'.format(name))
                    spacedata = options.getfloat('Harness Info','{}_spacing'.format(name))
                    spacelist[i] = spacedata
                    vgnumsky = options.getint('Harness Info','{}_numsky'.format(name))
                    namedata = num + vgnumsky
                    fibers_per_slitlist[i] = num + vgnumsky

                    skylist = range(-1*(vgnumsky + numSky),-1*numSky)[::-1]
                    numlist = range(numFibers + startFiber, numFibers + num + startFiber)
                    vglist = skylist[0:len(skylist)/2] + numlist + skylist[len(skylist)/2:]
                    slitDict[int(name[2]) - 1] = vglist
                    fiberList += vglist
                    if startFiber in vglist:
                        slitnum = int(name[2]) - 1

                    numSky += vgnumsky
                    numFibers += num
                    bigskylist += skylist

                except ConfigParser.NoOptionError:
                    continue

        elif harnessType == 'MINNIE':

            self.commonData.HexRank = options.getint('Harness Info','rank')

            ifu_mapfile = options.get('Harness Info','ifu_mapfile')
            self.md.set_ifu_map(ifu_mapfile)

            sky_mapfile = options.get('Harness Info','sky_mapfile')
            self.md.set_pp_map(sky_mapfile)

            ferrulelist = []
            for i in [1,2,3]:
                ferrulelist.append(options.getint('Harness Info','ifu{}_ferrulecode'.format(i)))
            self.commonData.MINNIE_ferrulelist = ferrulelist

            spacelist = []
            fiberList = []
            slitDict = {}
            fibers_per_slitlist = []
            numFibers = 0
            numSky = 0
            minnielist = []
            self.commonData.vg1space = options.getfloat('Harness Info','VG1_spacing')
            spacelist.append(self.commonData.vg1space)
            for i in range(1,4):
                numFibers += options.getint('Harness Info','ifu{}_numfibers'.format(i))
                numSky += options.getint('Harness Info','ifu{}_numsky'.format(i))
                minnielist.append(numFibers)
            self.commonData.MinnieList = minnielist

            self.commonData.vg1num = numFibers + numSky
            fibers_per_slitlist.append(numFibers + numSky)

            numlist = range(1, numFibers + 1)
            skylist = range(-1*numSky,0)[::-1]
            vglist = skylist[0:len(skylist)/2] + numlist + skylist[len(skylist)/2:]
            slitDict[0] = vglist
            fiberList += vglist
            slitnum = 0
            bigskylist = skylist

        print fiberList
        print spacelist
        self.commonData.startSlit = slitnum
        self.commonData.y2StartPos += slitnum*self.commonData.slitHeight
        self.commonData.StartFiber = startFiber
        self.commonData.FiberList = fiberList
        self.commonData.SkyList = bigskylist
        self.commonData.slitDict = slitDict
        self.commonData.spaceList = spacelist
        self.commonData.fibersPerSlit = fibers_per_slitlist
        self.commonData.NumFibers = numFibers

        self.createResults()

        print "starting at slitNum {}, which is at y2 = {} mm".format(slitnum + 1,self.commonData.y2StartPos)

        return True

    def getSearchPosList(self, startPos):
        """Return an array of search positions, in mm"""
        print "generating search list..."
        searchStepsList = [startPos]
        multFac = 1
        minBool = True
        maxBool = True
        while minBool or maxBool:
            deltaPos = multFac * self.commonData.BigStepSize
            if startPos + deltaPos > self.commonData.X2maxLimit:
                maxBool = False
            else:
                searchStepsList.append(startPos + deltaPos)
            if startPos - deltaPos < self.commonData.X2minLimit:
                minBool = False
            else:
                searchStepsList.append(startPos - deltaPos)
            multFac += 1
#        print "searchStepsList(%r) = %s" % (startPos, searchStepsList)
        return searchStepsList

    def checkMetro(self):
        """Checks to make sure there is a valid .cfg file and
        a valid metrology map.
        """

        calibfile = self.md.get_configfile()

        if not os.path.exists(calibfile):
            return False

        config = ConfigParser.RawConfigParser()
        config.read(calibfile)

        try:
            if self.commonData.harnessType == 'IFU':
                if os.path.exists(config.get('METROLOGY', "ifu-metrology-file")):
                    return True
                return False

            elif self.commonData.harnessType == 'single':
                if os.path.exists(config.get('METROLOGY', "plug-plate-metrology-file")):
                    return True
                return False

            elif self.commonData.harnessType == 'PROD':
                if os.path.exists(config.get('METROLOGY', "plug-plate-metrology-file"))\
                    and os.path.exists(config.get('METROLOGY', "ifu-metrology-file")):
                    return True
                elif os.path.exists(config.get('METROLOGY', "ifu-metrology-file"))\
                    and len(self.commonData.SkyList) < 1:
                    return True
                return False

            elif self.commonData.harnessType == 'MINNIE':
                if os.path.exists(config.get('METROLOGY', 'minnie-map-1'))\
                    and os.path.exists(config.get('METROLOGY','minnie-map-2'))\
                    and os.path.exists(config.get('METROLOGY','minnie-map-3')):
                    return True
        except Exception as e:
            if os.path.exists(config.get('METROLOGY', "ifu-metrology-file"))\
                and len(self.commonData.SkyList) < 1:
                return True
            else:
                return False

        return False

    def checkMap(self):
        """Checks to make sure a valid IFU or plug plate fiber map
        has been loaded. Exit and warn the user if not.
        """

        print "Checking map..."

        if self.commonData.harnessType == 'PROD':
            try:
                if len(self.md.CMfo.ifu_map) == 0 or len(self.md.CMfo.pp_map) == 0:
                    return False
            except TypeError:
                return False

        return True

    def homeAll(self, evt=None):
        """Initializes all of the ThorLabs stages.
        """

        stages = ['x1','y1','x2','y2']

        for stage in stages:
            self.md.proc_apt_move_home(self.commonData.teststand,stage)

        for stage in stages:
            self.md.proc_apt_move_block(self.commonData.teststand,stage)

        return

    def run(self, sr):
        """Run the focus script.
        """
        self.initAll()
        self.commonData.sessionID = self.sessionWdg.getString()

        recorded = self.recordUserParams()
        if not recorded:
            return

        if not self.checkMap():
            macspeechX.SpeakString("Did not load a valid fiber map")
            self.sr.showMsg("Did not load a valid fiber map!",RO.Constants.sevError)
            self.md.proc_dlp('8-')
            return

        if self.DoAcq.get():
            print "'ere I am: {}".format(self.commonData.harnessType)
            if self.commonData.harnessType == 'single':
                yield self.SH_metrology()
            elif self.commonData.harnessType == 'IFU':
                yield self.IFU_metrology()
            elif self.commonData.harnessType == 'MCAL':
                yield self.MCAL_metrology()
            elif self.commonData.harnessType == 'PROD':
                yield self.PROD_metrology()
            elif self.commonData.harnessType == 'MINNIE':
                yield self.MINNIE_metrology()

            self.sr.showMsg('Metrology completed')

        if not self.DoTput.get():
            macspeechX.SpeakString("All done! Did you forget to check the throughput box above?")
            self.md.proc_dlp('8-')
            return

        if not self.checkMetro():
            macspeechX.SpeakString("Sorry, I could not find a valid metrology solution.")
            self.sr.showMsg("No metrology solution found!",RO.Constants.sevError)
            self.md.proc_dlp('8-')
            return

        self.dateStr = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
        self.logFileName = "{}_{}.dat".format(self.commonData.sessionID, self.dateStr)
        self.logFilePath = os.path.join(self.commonData.LogFileDir, self.logFileName)
        self.logFile = file(self.logFilePath, "w")
        self.writeLogFileHeader()

        mjd = int(time.time()/86400.0 + 40587.0)
        self.yannyFilePath = '{}/{}-results.par'.format(self.commonData.LogFileDir,
                                                     ''.join(self.commonData.harnessID.split('.')).lower())
        print self.yannyFilePath

        yield self.measureHarness(self.commonData.FiberList)

        if self.commonData.harnessType == 'PROD':
            self.recordMetrology()
        elif self.commonData.harnessType == 'MINNIE':
            self.recordMinnieMetrology()

        if self.logFile:
            self.logFile.close()
        self.writeYanny()
        self.md.proc_dlp('8-')
        self.pushResults()
        macspeechX.SpeakString("Moving stages to home")
        yield self.MoveStage(('x1','y1'), (self.commonData.x1HomePos,self.commonData.y1HomePos))
        macspeechX.SpeakString("All done!")


    def measureHarness(self,fiberList,retest=False):
        '''The core function for measuring a harness. This handles aquiring each fiber in the harness and
        measuring its profile. It also finds slit blocks and logs throughtputs.

        In the event a retest is needed, measureHarness is called recursively.
        '''
        print "fiberList is {}".format(fiberList)
        print 'workingdir is {}'.format(self.md.workingdir)
        self.md.proc_dlp('8-')
        self.commonData.directImages = Imager.DirectImages(self.md)
        yield self.waitMeasureSource()
        self.md.clear_regions_in_ds9('output')
        #self.md.proc_gige_loop_cmds('input', "0u")
        self.md.proc_gige_loop_cmds('input', "0")

        y2Opt = True
        slitNum = self.commonData.startSlit
        finslit = 1

        yield self.MoveStage('y2',self.commonData.y2StartPos)

        for i, fiberNum in enumerate(fiberList):

            self.md.clear_regions_in_ds9('output')
            if fiberNum == fiberList[0]:
                yield self.waitForUser("Move to stage to fiber-mode", "Move the stage to fiber mode")

            self.sr.showMsg("Measuring Fiber {}".format(fiberNum))

            if self.commonData.harnessType == 'single':
                yield self.SH_getFiber(fiberNum)

            elif self.commonData.harnessType == 'IFU':
                yield self.IFU_getFiber(fiberNum)

            elif self.commonData.harnessType == 'MCAL':
                yield self.MCAL_getFiber(fiberNum)

            elif self.commonData.harnessType == 'PROD':
                if fiberNum < 0:
                    yield self.SH_getFiber(fiberNum)
                else:
                    yield self.IFU_getFiber(fiberNum)
            elif self.commonData.harnessType == 'MINNIE':
                if fiberNum < 0:
                    yield self.SH_getFiber(fiberNum)
                else:
                    yield self.MINNIE_getFiber(fiberNum)

            print "{} in {} ?".format(fiberNum,self.commonData.slitDict[slitNum])
            if fiberNum not in self.commonData.slitDict[slitNum]:
                macspeechX.SpeakString('Moving to slit number {}'.format(slitNum+2))
                new_y2_pos = self.md.proc_apt_query(self.commonData.teststand,'y2') + self.commonData.slitHeight
                theory_y2_pos = self.commonData.y2StartPos + (self.commonData.slitHeight * (slitNum + 1))
                if abs(new_y2_pos - theory_y2_pos) >= 1:
                    yield self.waitForUser("{:1n} {:4.1f} {:4.1f}".format(slitNum, new_y2_pos, theory_y2_pos), "Confirm why 2 mismatch")
                    new_y2_pos = theory_y2_pos
                yield self.MoveStage('y2',new_y2_pos)
                self.commonData.startSearchPos = self.commonData.defaultSearchPos
                slitNum += 1
                finslit = 1
                y2Opt = True

            #record some info about the fiber
            if not retest:
                self.recordFiberInfo(i, fiberNum, slitNum)

            self.sr.showMsg("Finding rough x2 location")
            macspeechX.SpeakString("Finding rough x 2 location")
            foundPos = None
            name = '/tmp/tmp.fits'
            self.logMsg("\ngross x2 movement:\n\tMeas#\tFiber#\tPos(mm)\tFlux(ADU)")
            x2meas = 1
            for pos in self.getSearchPosList(self.commonData.startSearchPos):
                self.md.proc_apt_move_absolute(self.commonData.teststand,'x2',pos)
                self.md.proc_apt_move_block(self.commonData.teststand,'x2')
                rawimage = self.md.proc_gige_loop_save_image('output')
                os.system('manga_copy < {:} > {:}'.format(rawimage,name))
                os.system('rm {:}'.format(rawimage))
                data = pyfits.open(name)[0].data
                self.logMsg("Meas\t{:}\t{:}\t{:0.2f}\t{:0.2e}".format(x2meas,
                                                        fiberNum,
                                                        pos,
                                                        numpy.sum(data)))
                yield self.sr.waitMS(110)
                if numpy.sum(data) >= 1e5:
                    foundPos = pos
                    break
                x2meas += 1

            # if no fiber found, go on to next fiber
            if not foundPos:
                self.sr.showMsg("Fiber {} is broken".format(fiberNum,))
                fiberData = Imager.FiberImages(fiberNum, slitNum, -99., self.md)
                fiberData.isBroken = True
                fiberData.tput_list = [0,0,0]
                self.logFiberFit(fiberData)
                self.fiberDataList.append(fiberData)
                yield self.sr.waitMS(101)
                yield self.MoveStage('x2',self.commonData.startSearchPos)
                continue

            macspeechX.SpeakString("Centering fiber {}".format(fiberNum))
            self.sr.showMsg('Centering fiber {}'.format(fiberNum))
#             if fiberNum == fiberList[0]:
#                     yield self.waitForUser('Center output', 'Center output')
            yield self.center_fiber_fine()
            x2_center = self.md.proc_apt_query(self.commonData.teststand,'x2')
            print "Setting up fiber data"
            fiberData = Imager.FiberImages(fiberNum, slitNum, x2_center, self.md)
            yield self.sr.waitMS(101)
            # walk until signal drops to 50% of maximum
            macspeechX.SpeakString("Measuring fiber {}".format(fiberNum))
            self.sr.showMsg('Measuring fiber {}'.format(fiberNum))
            yield self.measureFiber(fiberData)

            self.commonData.startSearchPos = self.md.proc_apt_query(self.commonData.teststand,'x2') +\
                                    self.commonData.spaceList[slitNum]

            # fit again, though fit should not change much from measuring current at center
            self.logFiberFit(fiberData)

            #If we're retesting, delete the old copy of fiberData from the fiberDataList
            if retest:
                for i, fdat in enumerate(self.fiberDataList):
                    if fdat.fibnum == fiberData.fibnum:
                        print 'Replacing old fiberData'
                        self.fiberDataList[i] = fiberData
            else:
                self.fiberDataList.append(fiberData)
            finslit += 1
            yield self.sr.waitMS(101)

        self.md.clear_regions_in_ds9('input')
        self.md.clear_regions_in_ds9('output')
        yield self.waitMeasureSource(True)
        self.commonData.directImages.compute_avg_flux()
        self.logAveSource()

        # set v-groove position
        vgsortlist = [fiber.getVGNumSortKey() for fiber in self.fiberDataList]
        vgidx = sorted(range(len(vgsortlist)), key=lambda k:vgsortlist[k])
        for id, idx in enumerate(vgidx):
            self.fiberDataList[idx].setVGBlockInfo(id + 1)
            self.commonData.ResultsArray[idx]['finblock'] = id + 1

        # report results -- including updating self.instructWdg
        badFiberList = []
        self.logThroughputs(self.fiberDataList,badFiberList)
        print badFiberList
        if len(badFiberList) != 0:
            ids, badlist = zip(*badFiberList)
            retest = tk.BooleanVar()
            retest.set(False)
            self.sr.master.wait_window(FiberRetest(self.sr.master,badlist,retest).top)
            print retest.get()
            if retest.get() == 1:
                yield self.measureHarness(badlist,retest=True)

    def recordFiberInfo(self,i,fiberNum,slitNum):
        '''Write various data to the ResultsArray (i.e., yanny file).
        '''

        self.commonData.ResultsArray[i]['harblock'] = slitNum + 1
        self.commonData.ResultsArray[i]['blocksize'] = len(self.commonData.slitDict[slitNum])
        self.commonData.ResultsArray[i]['fnum'] = fiberNum

        if self.commonData.harnessType == 'single':
            return

        if fiberNum < 0:
            self.commonData.ResultsArray[i]['ifuinblock'] = 0
            self.commonData.ResultsArray[i]['fibertype'] = 'SKY'
            mx ,my = self.md.CMfo.pp_fiber_locate(fiberNum)
            self.commonData.ResultsArray[i]['xpmm'] = mx
            self.commonData.ResultsArray[i]['ypmm'] = my
        else:
            if self.commonData.harnessType == 'MINNIE':
                ifu_num = [j + 1 for j, n in enumerate(self.commonData.MinnieList) if n >= fiberNum][0]
                print "Recording minnie params, ifu_num is {}".format(ifu_num)
                mx, my = self.md.CMfo.ifu_fiber_locate(self.commonData.HexRank,fiberNum,
                                                       mapname='minnie-map-{}'.format(ifu_num),
                                                       motorname = 'MINNIE_{}_MOTOR_CAL'.format(ifu_num))
                self.commonData.ResultsArray[i]['ifuinblock'] = ifu_num
                self.commonData.ResultsArray[i]['frlCode'] = self.commonData.MINNIE_ferrulelist[ifu_num - 1]
            else:
                mx ,my = self.md.CMfo.ifu_fiber_locate(self.commonData.HexRank,fiberNum)
                self.commonData.ResultsArray[i]['ifuinblock'] = 1
                self.commonData.ResultsArray[i]['frlCode'] = self.commonData.ferruleCode

            self.commonData.ResultsArray[i]['fibertype'] = 'IFU'
            self.commonData.ResultsArray[i]['xpmm'] = mx
            self.commonData.ResultsArray[i]['ypmm'] = my

    def OptimizeY2(self,slitNum):
        '''Each time we move to a new slit we need to peak-up in y2. This function does that
        in exactly the same way fibers are found; by building a profile of measured brightness
        and then finding the center.
        '''

        self.logFile.write('# y2 optimization\n')
        self.sr.showMsg("Doing y2 optimization")
        macspeechX.SpeakString('Optimizing why 2 location') # Ha ha!
        y2_optimize_data = FiberData(slitNum+1, slitNum, self.commonData)
        foundCurr = self.MeasureCurrent()
        y2foundPos = self.md.proc_apt_query(self.commonData.teststand,'y2')
        y2_optimize_data.addMeas(y2foundPos, foundCurr)
        self.logMsg("\ny2 optimization:\n\tMeas#\tSlit#\tPos(mm)\tI(nA)")
        self.logFiberMeas(y2_optimize_data, y2foundPos, foundCurr)
        yield self.sr.waitMS(101)
        for measDir in (1, -1):
            curr = foundCurr
            y2Pos = y2foundPos
            while curr > 0.5 * y2_optimize_data.maxCurr:
                y2Pos += self.commonData.Y2SmallStepSize * measDir
                if y2Pos > self.commonData.Y2maxLimit or y2Pos < self.commonData.Y2minLimit:
                    break
                yield self.sr.waitMS(101)
                yield self.MoveStage('y2',y2Pos)
                curr = self.MeasureCurrent()
                y2_optimize_data.addMeas(y2Pos,curr)
                self.logFiberMeas(y2_optimize_data, y2Pos, curr)

        goodfit = y2_optimize_data.fitBeam()
        y2message = "Found optimal y2 location at {:.2f} mm\n".format(y2_optimize_data.fitBeamCtrPos)
        if not goodfit:
            y2message = "Could not fit peak, using max current location at {} mm".\
                        format(y2_optimize_data.maxCurrPos)
            yield self.MoveStage('y2',y2_optimize_data.maxCurrPos)
        else:
            yield self.MoveStage('y2',y2_optimize_data.fitBeamCtrPos)
            self.sr.showMsg("Found slit {} at y = {:0.2f} mm".format(slitNum + 1,
                                                                      y2_optimize_data.fitBeamCtrPos))

        self.logMsg(y2message)
        dataStr = "Y2Fit\t{}\t{:0.2f}\t{:0.1f}\t{:0.1f}\n".\
            format(y2_optimize_data.slitnum, y2_optimize_data.fitBeamCtrPos,
                   y2_optimize_data.computeFitCurr(y2_optimize_data.fitBeamCtrPos),
                   y2_optimize_data.computeBeamWidth())
        self.logFile.write(dataStr)
        self.logFile.write('##\n\n')

    def measureFiber(self,fiberData):
        '''Take an image of the fiber and compute the flux and throughput within
        a few different apertures
        '''
        zpos = self.md.proc_apt_query(self.commonData.teststand,'z2')
        fiberData.take_image(zpos)
        yield self.sr.waitThread(fiberData.get_center,zpos)
        yield self.sr.waitThread(fiberData.set_apertures,self.commonData.directImages)
        Imager.make_ds9_regions(fiberData,self.commonData.harnessID)
        self.md.CMfo.show_regions_in_ds9('output')
        #saveimage = '{}/{}_ap'.format(self.commonData.LogFileDir,fiberData.basename)
        #self.md.CMfo.save_ds9_image(saveimage,'output')
        yield self.sr.waitThread(fiberData.compute_flux)
        yield self.sr.waitThread(fiberData.compute_throughput,self.commonData.directImages.flux_list[1])

    def recordMetrology(self):
        """Takes the ResultArray and computes the positions of the fibers, relative to the central fiber
        """

        ifu_map = numpy.array(self.md.CMfo.ifu_map)
        cent_fiber = ifu_map[numpy.where(ifu_map[:,1] == 0)[0][0]][0]
        print 'cent_fiber is {}'.format(cent_fiber)
        cent_idx = numpy.where(self.commonData.ResultsArray['fnum'] == cent_fiber)
        print cent_idx
        mx0 = self.commonData.ResultsArray[cent_idx]['xpmm']
        my0 = self.commonData.ResultsArray[cent_idx]['ypmm']
        try:
            self.commonData.ResultsArray['xpmm'] -= mx0
            self.commonData.ResultsArray['ypmm'] -= my0
        except ValueError:
            self.sr.showMsg("Center fiber not measured! Metrology information has offset.", RO.Constants.sevError)
            self.commonData.goodMetro = False

    def recordMinnieMetrology(self):
        """Computes and records metrology information for 3-IFU minnie bundle harnesses.
        This has a lot of shit hardcoded into it because I hate minnie bundles.
        """

        for i in range(3):
            ifu_idx = numpy.where((self.commonData.ResultsArray['fnum'] > i*7) &
                               (self.commonData.ResultsArray['fnum'] <= (i+1)*7))[0]
            print ifu_idx
            try:
                cent_idx = numpy.where(self.commonData.ResultsArray['fnum'] == i*7 + 4)[0][0]
                print cent_idx
                mx0 = self.commonData.ResultsArray[cent_idx]['xpmm']
                my0 = self.commonData.ResultsArray[cent_idx]['ypmm']
                print mx0, my0
                print self.commonData.ResultsArray[ifu_idx]['xpmm']
                self.commonData.ResultsArray['xpmm'][ifu_idx] = self.commonData.ResultsArray[ifu_idx]['xpmm'] - mx0
                self.commonData.ResultsArray['ypmm'][ifu_idx] = self.commonData.ResultsArray[ifu_idx]['ypmm'] - my0
                print self.commonData.ResultsArray[ifu_idx]['xpmm']
            except IndexError:
                self.sr.showMsg("Center fiber not measured! Metrology information has offset.", RO.Constants.sevError)
                self.commonData.goodMetro = False

    def pushResults(self, evt=None):
        """Asks the user if they want to push the results from a run up to the shared
        git repo. Assembles the files and does the push if the answer is yes.
        """

        push = tk.BooleanVar()
        push.set(False)
        push_dialog = PushDialog(self.sr.master,push)
        self.sr.master.wait_window(push_dialog.top)
        if push.get():
            push_dir = os.path.expanduser('~/SDSSbench/data/shared_results/{}/{}'.\
                                          format(self.commonData.location,self.commonData.sessionID))
            if os.path.exists(push_dir):
                push_dir += '_2'
                n = 3
                while os.path.exists(push_dir):
                    push_dir = '_'.join(push_dir.split('_')[:-1]) + '_{}'.format(n)
                    print push_dir
                    n += 1

            os.system('mkdir {}'.format(push_dir))
            os.chdir(self.commonData.LogFileDir)
            os.system('cp {} {}'.format(self.yannyFilePath,push_dir))
            os.system('cp {} {}'.format(self.logFilePath,push_dir))
            os.system('cp {} {}'.format(self.md.get_configfile,push_dir))
            os.system('cp manga_ifu_metrology*.log.map {}'.format(push_dir))
            if self.commonData.harnessType == 'MINNIE':
                os.system('cp *_face.fits {}'.format(push_dir))
                os.system('cp *_fibers.fits {}'.format(push_dir))
            else:
                try:
                    os.system('cp {} {}'.format(self.ifuFaceImagePath,push_dir))
                    os.system('cp {} {}'.format(self.ifuFiberImagePath,push_dir))
                except AttributeError:
                    print "couldn't find either the IFU face or fibers image"
            os.chdir(push_dir)
            os.system('git pull origin master')
            os.system('git add .')
            os.system('git commit -a -m "{}: added data for {}"'.format(self.commonData.location,self.commonData.sessionID))
            os.system('git push origin master')

        '''get back to where we were'''
        os.chdir(os.path.expanduser('~'))

    def SH_getFiber(self,fiberNum):
        """
        Places the input spot on the desired plug plate fiber. self.commonData.HexRank must be
        set.

        """
        self.sr.showMsg("Acquiring sky fiber number {}".\
                        format(fiberNum))
        yield self.sr.waitMS(101)
        macspeechX.SpeakString("Moving to fiber {}".format(fiberNum))
        self.md.proc_pp_fiber_acquire(self.commonData.teststand,fiberNum)


    def IFU_getFiber(self,fiberNum):
        """
        Places the input spot on the desired IFU fiber. self.commonData.HexRank must be
        set.

        """
        self.sr.showMsg("Acquiring IFU fiber number {}".\
                        format(fiberNum))

        yield self.sr.waitMS(101)
        macspeechX.SpeakString("Moving to fiber {}".format(fiberNum))
        self.md.proc_ifu_fiber_acquire(self.commonData.teststand,self.commonData.HexRank, fiberNum)
        #yield self.waitForUser("Ensure spot is centered on fiber","Please align fiber")

    def MINNIE_getFiber(self,fiberNum):
        """
        Places the input spot on the desired minnie bundle fiber. self.commonData.HexRank must be
        set.

        """
        self.sr.showMsg("Acquiring IFU fiber number {}".\
                        format(fiberNum))

        yield self.sr.waitMS(101)
        macspeechX.SpeakString("Moving to fiber {}".format(fiberNum))
        ifu_num = [i + 1 for i, n in enumerate(self.commonData.MinnieList) if n >= fiberNum][0]
        print "Using minnie-map-{} and MINNIE_{}_MOTOR_CAL for fiber number {}".format(ifu_num, ifu_num, fiberNum)
        self.md.proc_ifu_fiber_acquire(self.commonData.teststand,self.commonData.HexRank, fiberNum, mapname='minnie-map-{}'.format(ifu_num),
                                       motorname = 'MINNIE_{}_MOTOR_CAL'.format(ifu_num))

    def MCAL_getFiber(self,fiberNum):
        """ 'Gets' the next fiber on a pinned single fiber calibration harness. Really
        just asks the user to plug the next fiber in

        """
        yield self.waitForUser("Plug in fiber {}".format(fiberNum), "Please plug in fiber number {}".format(fiberNum))

    def SH_metrology(self):
        """
        Do a metrology sequence on a Single Fiber Plug Plate. This function sets up all of the
        correct LED and stage states and then performs a motor calibration followed by a
        metrology run. The metrology run produces a file that describes the true motor coordinates
        of each fiber in the plug plate.

        """

        macspeechX.SpeakString('Doing fiber metrology')
        self.md.set_pp_metro(self.commonData.x1PPMetroPos, self.commonData.y1PPMetroPos)
        self.md.set_pp_fiber_limits(self.commonData.StartFiber, self.commonData.NumFibers)
        print "fiberlist:", self.md.fiberlist
        #yield self.waitForUser("Un-plug the source lamp","Please remove the source lamp and ensure the stage is in fiber mode")
        yield self.waitForUser("Move stage to fiber mode","Please put the stage in fiber mode")
        self.md.proc_dlp('8+')
        yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                                    self.commonData.y2MetroPos,
                                                    self.commonData.x1PPMetroPos,
                                                    self.commonData.y1PPMetroPos))
        self.md.proc_gige_loop_cmds('input', "1DDDDD")
        self.md.proc_dlp('3+')
        self.md.proc_dlp('4+')
        self.md.proc_dlp('5+')
        self.md.proc_dlp('6+')
        self.md.proc_dlp('7+')
        self.md.proc_dlp('1-')
        self.md.proc_dlp('2-')
        yield self.waitForUser("Ensure correct fiber focus", "Please focus the fibers")

        yield self.sr.waitThread(self.md.proc_pp_motor_cal, **{'teststand': self.commonData.teststand})
        yield self.MoveStage(('x1','y1'),(self.commonData.x1PPMetroPos,
                                          self.commonData.y1PPMetroPos))
        yield self.sr.waitThread(self.md.proc_pp_metrology, **{'teststand': self.commonData.teststand})
        # yield self.waitForUser("Plug in the source lamp","Please plug in the source lamp")
        self.md.proc_dlp('8-')
        self.md.proc_gige_loop_cmds('input', "0D")
        yield self.MoveStage(('x1','y1'),(self.commonData.x1PPMetroPos + self.commonData.x1PPOffset,
                                          self.commonData.y1PPMetroPos + self.commonData.y1PPOffset))
        yield self.sr.waitThread(self.md.proc_spot_cal)
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')

    def IFU_metrology(self):
        """
        Do a metrology sequence on an IFU. This function sets up all of the
        correct LED and stage states and then performs a motor calibration followed by a
        metrology run. The metrology run produces a file that describes the true motor
        coordinates of each fiber in the IFU.
        """

        macspeechX.SpeakString('Doing fiber metrology')
        self.sr.showMsg('Doing fiber metrology')
        #yield self.waitForUser("Un-plug the source lamp","Please remove the source lamp and ensure the stage is in fiber mode")
        yield self.waitForUser("Move stage to fiber mode","Please put the stage in fiber mode")
        #yield self.waitForUser("Plug in the source lamp","Please plug in the source lamp")
        self.md.proc_dlp('8-')
        self.md.proc_gige_loop_cmds('input', "0D")
        yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos + self.commonData.x1Offset,
                                          self.commonData.y1MetroPos + self.commonData.y1Offset))
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        yield self.waitForUser("Focus input spot","Please focus input spot")
        macspeechX.SpeakString("Finding input spot")
        self.sr.showMsg("Finding input spot")
        yield self.sr.waitThread(self.md.proc_spot_cal)
        yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                              self.commonData.y2MetroPos,
                                              self.commonData.x1MetroPos,
                                              self.commonData.y1MetroPos))
        self.md.proc_dlp('8+')
        self.md.proc_gige_loop_cmds('input', "1DDDDD")
        self.md.proc_dlp('1-')
        self.md.proc_dlp('3+')
        self.md.proc_dlp('4+')
        self.md.proc_dlp('5+')
        self.md.proc_dlp('6+')
        self.md.proc_dlp('7+')

        yield self.waitForUser("Ensure correct fiber focus", "Please focus the fibers")

        macspeechX.SpeakString("Computing motor solution")
        self.sr.showMsg("Computing motor solution")
        yield self.sr.waitThread(self.md.proc_ifu_metrology, **{'teststand': self.commonData.teststand})
#         yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos,
#                                           self.commonData.y1MetroPos))
        yield self.sr.waitThread(self.md.proc_ifu_motor_cal,self.commonData.HexRank, **{'teststand': self.commonData.teststand})
        yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos,
                                              self.commonData.y1MetroPos))


        yield self.sr.waitThread(self.md.proc_ifu_metrology, **{'teststand': self.commonData.teststand})

        rawimage = self.md.proc_gige_loop_save_image('input')
        self.ifuFiberImagePath = '{}/{}_fibers.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID)
        os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFiberImagePath))
        os.system('rm {:}'.format(rawimage))

        '''turn on the front illumination'''
        self.md.proc_dlp('1+')
        self.md.proc_dlp('2+')
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        rawimage = self.md.proc_gige_loop_save_image('input')
        self.ifuFaceImagePath = '{}/{}_face.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID)
        os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFaceImagePath))
        os.system('rm {:}'.format(rawimage))
        self.md.proc_dlp('1-')
        self.md.proc_dlp('2-')

        self.md.proc_gige_loop_cmds('input', "0")
        os.system('manga-clean')
        self.md.proc_dlp('8-')
        macspeechX.SpeakString("Metrology complete")

    def PROD_metrology(self):
        """This is different from just doing IFU and SH separately because we don't need
        to measure the spot twice
        """
        macspeechX.SpeakString('Doing fiber metrology')
        self.sr.showMsg('Doing fiber metrology')
        """SPOT"""
        yield self.waitForUser("Move stage to fiber mode","Please put the stage in fiber mode")
        self.md.proc_dlp('8-')
        self.md.proc_gige_loop_cmds('input', "0")
        yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos + self.commonData.x1Offset,
                                          self.commonData.y1MetroPos + self.commonData.y1Offset))
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        yield self.waitForUser("Focus input spot","Please focus input spot")
        macspeechX.SpeakString("Finding input spot")
        self.sr.showMsg("Finding input spot")
        yield self.sr.waitThread(self.md.proc_spot_cal)

        """IFU"""
        macspeechX.SpeakString("Finding eye ef you fibers")
        self.sr.showMsg('Finding IFU fibers')
        yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                              self.commonData.y2MetroPos,
                                              self.commonData.x1MetroPos,
                                              self.commonData.y1MetroPos))
        self.md.proc_dlp('8+')
        self.md.proc_dlp('1-')
        self.md.proc_dlp('3+')
        self.md.proc_dlp('4+')
        self.md.proc_dlp('5+')
        self.md.proc_dlp('6+')
        self.md.proc_dlp('7+')
        self.md.proc_gige_loop_cmds('input', "0UU")
        yield self.waitForUser("Ensure correct fiber focus", "Please focus the fibers")
        macspeechX.SpeakString("Computing motor solution")
        self.sr.showMsg("Computing motor solution")
        yield self.sr.waitThread(self.md.proc_ifu_metrology, **{'teststand': self.commonData.teststand})
        yield self.sr.waitThread(self.md.proc_ifu_motor_cal,self.commonData.HexRank, **{'teststand': self.commonData.teststand})
        yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos,
                                              self.commonData.y1MetroPos))
        yield self.sr.waitThread(self.md.proc_ifu_metrology, **{'teststand': self.commonData.teststand})
        self.md.proc_gige_loop_cmds('input', "1")
        rawimage = self.md.proc_gige_loop_save_image('input')
        self.ifuFiberImagePath = '{}/{}_fibers.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID)
        os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFiberImagePath))
        os.system('rm {:}'.format(rawimage))

        self.md.proc_dlp('1+')
        self.md.proc_dlp('2+')
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        rawimage = self.md.proc_gige_loop_save_image('input')
        self.ifuFaceImagePath = '{}/{}_face.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID)
        os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFaceImagePath))
        os.system('rm {:}'.format(rawimage))
        self.md.proc_dlp('1-')
        self.md.proc_dlp('2-')
        self.md.proc_gige_loop_cmds('input', "0UU")


        """PLUGPLATE"""
        if len(self.commonData.SkyList) > 0:
            self.md.proc_dlp('3+')
            self.md.proc_dlp('4+')
            self.md.proc_dlp('5+')
            self.md.proc_dlp('6+')
            macspeechX.SpeakString("Finding sky fibers")
            self.sr.showMsg('Finding sky fibers')
            self.md.set_pp_metro(self.commonData.x1PPMetroPos, self.commonData.y1PPMetroPos)
            self.md.set_pp_fiberlist(self.commonData.SkyList)
            print "fiberlist:", self.md.fiberlist
            self.md.proc_dlp('8+')
            yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                                        self.commonData.y2MetroPos,
                                                        self.commonData.x1PPMetroPos,
                                                        self.commonData.y1PPMetroPos))
            yield self.sr.waitThread(self.md.proc_pp_motor_cal, **{'teststand': self.commonData.teststand})
            yield self.MoveStage(('x1','y1'),(self.commonData.x1PPMetroPos,
                                              self.commonData.y1PPMetroPos))
            yield self.sr.waitThread(self.md.proc_pp_metrology, **{'teststand': self.commonData.teststand})
            self.md.proc_dlp('3-')
            self.md.proc_dlp('4-')
            self.md.proc_dlp('5-')
            self.md.proc_dlp('6-')
        os.system('manga-clean')
        self.md.proc_gige_loop_cmds('input', '0')
        self.md.proc_dlp('8-')
        macspeechX.SpeakString("Metrology complete")

    def MINNIE_metrology(self):
        """Does motor calibration and metrology for a minnie bundle style calibration harness.
        This is essentially just IFU metrology three times, one for each bundle
        """

        macspeechX.SpeakString('Doing fiber metrology')
        self.sr.showMsg('Doing fiber metrology')
        """SPOT"""
        yield self.waitForUser("Move stage to fiber mode","Please put the stage in fiber mode")
        self.md.proc_dlp('8-')
        self.md.proc_gige_loop_cmds('input', "0D")
        yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos + self.commonData.x1Offset,
                                          self.commonData.y1MetroPos + self.commonData.y1Offset))
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        yield self.waitForUser("Focus input spot","Please focus input spot")
        macspeechX.SpeakString("Finding input spot")
        self.sr.showMsg("Finding input spot")
        yield self.sr.waitThread(self.md.proc_spot_cal)

        """IFUs"""
        macspeechX.SpeakString("Finding eye ef you fibers")
        self.sr.showMsg('Finding IFU fibers')
        self.md.proc_dlp('8+')
        self.md.proc_gige_loop_cmds('input', "1DDDDD")
        self.md.proc_dlp('1-')
        self.md.proc_dlp('3+')
        self.md.proc_dlp('4+')
        self.md.proc_dlp('5+')
        self.md.proc_dlp('6+')
        self.md.proc_dlp('7+')
        yield self.MoveStage(('x2','y2'), (self.commonData.x2MetroPos,
                                                  self.commonData.y2MetroPos))
        for i, dir in zip([1,2,3],[-1,0,1]):
            self.md.proc_dlp('3+')
            self.md.proc_dlp('4+')
            self.md.proc_dlp('5+')
            self.md.proc_dlp('6+')
            self.md.proc_dlp('7+')
            yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos + self.commonData.x1MinnieMove*dir,
                                              self.commonData.y1MetroPos))

            yield self.waitForUser("Ensure correct fiber focus", "Please focus the fibers")
            macspeechX.SpeakString("Computing motor solution")
            self.sr.showMsg("Computing motor solution")
            yield self.sr.waitThread(self.md.proc_ifu_metrology,**{'mapname':'minnie-map-{}'.format(i)})
            yield self.sr.waitThread(self.md.proc_ifu_motor_cal,self.commonData.HexRank,
                                                            **{'mapname':'minnie-map-{}'.format(i),
                                                               'sectionname':'MINNIE_{}_MOTOR_CAL'.format(i)})
            yield self.MoveStage(('x1','y1'),(self.commonData.x1MetroPos + self.commonData.x1MinnieMove*dir,
                                              self.commonData.y1MetroPos))
            yield self.sr.waitThread(self.md.proc_ifu_metrology,**{'mapname':'minnie-map-{}'.format(i)})
            rawimage = self.md.proc_gige_loop_save_image('input')
            self.ifuFiberImagePath = '{}/{}_IFU{}_fibers.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID,i)
            os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFiberImagePath))
            os.system('rm {:}'.format(rawimage))

            '''turn on the front illumination'''
            self.md.proc_dlp('1+')
            self.md.proc_dlp('2+')
            self.md.proc_dlp('3-')
            self.md.proc_dlp('4-')
            self.md.proc_dlp('5-')
            self.md.proc_dlp('6-')
            rawimage = self.md.proc_gige_loop_save_image('input')
            self.ifuFaceImagePath = '{}/{}_IFU{}_face.fits'.format(self.commonData.LogFileDir,self.commonData.harnessID,i)
            os.system('manga_copy < {:} > {:}'.format(rawimage,self.ifuFaceImagePath))
            os.system('rm {:}'.format(rawimage))
            self.md.proc_dlp('1-')
            self.md.proc_dlp('2-')


        """PLUGPLATE"""
        if len(self.commonData.SkyList) > 0:
            self.md.proc_dlp('3+')
            self.md.proc_dlp('4+')
            self.md.proc_dlp('5+')
            self.md.proc_dlp('6+')
            macspeechX.SpeakString("Finding sky fibers")
            self.sr.showMsg('Finding sky fibers')
            self.md.set_pp_metro(self.commonData.x1PPMetroPos, self.commonData.y1PPMetroPos)
            self.md.set_pp_fiberlist(self.commonData.SkyList)
            print "fiberlist:", self.md.fiberlist
            self.md.proc_dlp('8+')
            yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                                        self.commonData.y2MetroPos,
                                                        self.commonData.x1PPMetroPos,
                                                        self.commonData.y1PPMetroPos))
            yield self.sr.waitThread(self.md.proc_pp_motor_cal)
            yield self.MoveStage(('x1','y1'),(self.commonData.x1PPMetroPos,
                                              self.commonData.y1PPMetroPos))
            yield self.sr.waitThread(self.md.proc_pp_metrology)
            self.md.proc_dlp('3-')
            self.md.proc_dlp('4-')
            self.md.proc_dlp('5-')
            self.md.proc_dlp('6-')

        self.md.proc_gige_loop_cmds('input', "0")
        os.system('manga-clean')
        self.md.proc_dlp('8-')
        macspeechX.SpeakString("Metrology complete")


    def MCAL_metrology(self):
        """ Really simply metrology here, just asks the user to ensure the spot is lined up
        on the fiber.

        """
        yield self.MoveStage(('x2','y2','x1','y1'),(self.commonData.x2MetroPos,
                                              self.commonData.y2MetroPos,
                                              self.commonData.x1MetroPos,
                                              self.commonData.y1MetroPos))
        self.md.proc_dlp('1+')
        self.md.proc_gige_loop_cmds('input', '1DDDDD')
        yield self.waitForUser('Place fiber on spot','Please ensure the spot is centered on the fiber')
        self.md.proc_dlp('1-')
        self.md.proc_dlp('3-')
        self.md.proc_dlp('4-')
        self.md.proc_dlp('5-')
        self.md.proc_dlp('6-')
        self.md.proc_gige_loop_cmds('input', "0")

    def setInstructWdg(self, msgStr, severity=RO.Constants.sevNormal):
        """Set instructions widget message"""
        self.instructWdg.set(msgStr, severity=severity)

    def waitForUser(self, msgStr, speechStr=None):
        """Display an instruction and wait for the user to press the Continue button.
        """
        self.setInstructWdg(msgStr)
        if speechStr:
            macspeechX.SpeakString(speechStr)
        self.userContinueBtn.grid()
        self.userContinueBtn.focus_set()
        yield self.sr.waitUser()
        self.userContinueBtn.grid_remove()
        self.instructWdg.clear()

    def MeasureCurrent(self):
        """Take a current measurement, in nanoamps, and return it
        """

        thresh = 0.05
        diff = numpy.inf
        I1 = numpy.inf

        while diff > thresh:
            I2 = self.md.proc_rbd_query()
            diff = numpy.abs(I2 - I1)
            print "Current is {} nA, diff is {} nA".format(I2,diff)
            I1 = I2

        if self.md.proc_rbd_query() == -1:
            cancel = tk.BooleanVar()
            cancel.set(False)
            self.sr.master.wait_window(AmmeterWarning(self.sr.master,cancel).top)
            if cancel.get():
                raise RO.ScriptRunner.ScriptError('Test stopped by user')
            else:
                return self.MeasureCurrent()

        return self.md.proc_rbd_query()


    def waitMeasureSource(self, end=False):
        """Measure and log source current (prompting the user)"""

        yield self.waitForUser("Set up Source Meas.", "Please move the stage to source mode")
        yield self.sr.waitMS(101)
        zpos = self.md.proc_apt_query(self.commonData.teststand,'z2')

        suffix = 'end' if end else ''
        self.commonData.directImages.take_image(zpos, suffix=suffix)

        saveimage = '{}/{}_ap_2'.format(self.commonData.LogFileDir,self.commonData.directImages.basename)
        if not end:
            macspeechX.SpeakString("Finding apertures")
            self.sr.showMsg('Finding apertures')
            yield self.sr.waitThread(self.commonData.directImages.find_apertures)
            saveimage = saveimage[:-2]
        Imager.make_ds9_regions(self.commonData.directImages,self.commonData.harnessID)
        self.md.CMfo.show_regions_in_ds9('output')
        self.md.CMfo.save_ds9_image(saveimage,'output')
        macspeechX.SpeakString("Measuring direct flux")
        self.sr.showMsg('Computing direct flux')
        yield self.sr.waitThread(self.commonData.directImages.compute_flux)
        self.logMsg("\t{:9}{:9}{:9}".format(*'f/3.2 f/4 f/5'.split()))
        dataStr = "Src\t{:0.2e}\t{:0.2e}\t{:0.2e}".format(*self.commonData.directImages.flux_list[-1])
        self.logMsg(dataStr)
        self.logFile.write(dataStr + "\n")
#         if curr < self.commonData.MinSourceCurrent:
#             raise RO.ScriptRunner.ScriptError("Source current too low: {:0.1f} < {:0.1f} nA".\
#                 format(curr, self.commonData.MinSourceCurrent))

    def center_fiber_fine(self,threshold = 5):
        """Make the final movement of a far-field fiber image to the center
        of the output CCD. Return the center of the beam profile"""
        name = '/tmp/tmp.fits'
#         kx = -4.576e-3
#         ky = 5.5086e-3
#16mm
#         kx = -1.21e-3
#         ky = 1.62e-3
#6mm
#         kx = -0.43e-3
#         ky = 0.46e-3
#25mm
#         kx = -2.09e-3
#         ky = 2.00e-3
#2 lenses 25/16
#         kx = 0.0059
#         ky = 0.0058
#2 lenses 16/25
#         kx = 3.9e-3
#         ky = 3.9e-3
#16/25 z-stage
#         kx = 2.1e-3
#         ky = -2.1e-3
#L1 and L2, z=20.607
        kx = 3.95e-3
        ky = -3.95e-3
        rawimage = self.md.proc_gige_loop_save_image('output')
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        data = pyfits.open(name)[0].data
        xf, yf = self.centroid(data)
        x0, y0 = self.commonData.directImages.center#[i/2. for i in data.shape]
#         xf, yf = self.centroid(data)
#         x0,y0 = [i/2. for i in data.shape]#self.commonData.directImages.center#[600.,930.]#[i/2. for i in data.shape]

        x_move = kx*(x0 - xf)
        y_move = ky*(y0 - yf)
        print 'initial: ',xf,yf, x0, y0

        #raw_input('{}, {}, {}, {}, {}, {}'.format(x0,y0,xf,yf,x_move/kx,y_move/ky))

        while ((x0 - xf)**2 + (y0 - yf)**2)**0.5 > threshold:
            xm = self.md.proc_apt_query(self.commonData.teststand,'x2')
            ym = self.md.proc_apt_query(self.commonData.teststand,'y2')

            yield self.MoveStage(('x2','y2'), (xm + x_move, ym + y_move))

            rawimage = self.md.proc_gige_loop_save_image('output')
            os.system('manga_copy < {:} > {:}'.format(rawimage,name))
            os.system('rm {:}'.format(rawimage))
            data = pyfits.open(name)[0].data
            xf, yf = self.centroid(data)
            print xf,yf, x0, y0
            x_move = kx*(x0 - xf)
            y_move = ky*(y0 - yf)

            #raw_input('{}, {}, {}, {}, {}, {}'.format(x0,y0,xf,yf,x_move/kx,y_move/ky))

    def centroid(self,image):
        '''takes in an array and returns a list containing the
        center of mass of that array

        Originally from ADEUtils.py
        '''

        '''just to make sure we don't mess up the original data somehow'''
        data = numpy.copy(image) * 1.0

        size = data.shape
        totalMass = numpy.sum(data)

        xcom = numpy.sum(numpy.sum(data,1) * 1.*numpy.arange(size[0]))/totalMass
        ycom = numpy.sum(numpy.sum(data,0) * 1.*numpy.arange(size[1]))/totalMass

        return (xcom,ycom)

    def MoveStage(self, stages, aptSteps):
        """Move one of the ThorLabs stages

        Inputs:
        - stages: a string or tuple of strings with the stages you want to move as strings
        - aptSteps: a float or tuple of floats with each stage's move location, in mm
        """

        if type(stages) != tuple:
            stages = (stages,)
            aptSteps = (aptSteps,)

        #first, check to make sure we're not trying to move the same stage twice
        for stage in stages:
            if stages.count(stage) != 1:
                raise RO.ScriptRunner.ScriptError("Oops! Trying to move {} to two different locations".\
                                                  format(stage))

        for stage, pos in zip(stages, aptSteps):
            self.md.proc_apt_move_absolute(self.commonData.teststand,stage,pos)
        for stage in stages:
            yield self.sr.waitThread(self.md.proc_apt_move_block,self.commonData.teststand,stage)

    def createResults(self):
        """set up the numpy record array that will be used to hold
        all the throughput and metrology data for a future writing to
        a yanny file.
        """

        totalnumfibers = len(self.commonData.FiberList)
        self.commonData.ResultsArray = numpy.zeros((totalnumfibers,),
                                                   dtype = [('harblock','i'),
                                                            ('finblock','i'),
                                                            ('ifuinblock','i'),
                                                            ('blocksize','i'),
                                                            ('fibertype','a7'),
                                                            ('fnum','i'),
                                                            ('gbu','i'),
                                                            ('frlCode','i'),
                                                            ('xpmm','f8'),
                                                            ('ypmm','f8'),
                                                            ('tput','f4')])

    def writeYanny(self):
        """Writes the throughput results and IFU metrology to a SDSS yanny file
        """

        mjd = int(time.time()/86400.0 + 40587.0)
        par = yanny(filename=None,np=True,debug=True)

        struct = par.dtype_to_struct(self.commonData.ResultsArray.dtype,structname='BUNDLEMAP',enums=dict())
        par['symbols']['struct'] = struct['struct']
        par['symbols']['enum'] = struct['enum']
        par['symbols']['BUNDLEMAP'.upper()] = struct['BUNDLEMAP'.upper()]
        par['BUNDLEMAP'.upper()] = self.commonData.ResultsArray

        par['harName'] = self.commonData.harnessID
        par['mjd'] = mjd
        par['nifu'] = len(self.commonData.FiberList) - len(self.commonData.SkyList)
        par['nsky'] = len(self.commonData.SkyList)
        par['ntotal'] = len(self.commonData.FiberList)
        if self.commonData.goodMetro:
            par['validMetro'] = True
        else:
            par['validMetro'] = False

        f = open(self.yannyFilePath,'w')
        f.write("""# MaNGA harness metrology file
# File generated on {}
# Test done on {} bench
#
# Uses SDSS-style Yanny .par formatting
#
# The purpose of this file is to track the metrology and status of all fibers associated with a
# harness of a given name (harName).  Each harness (e.g., MA123) can be associated with one or more
# IFU ferrules and some number of sky fibers.  For science harnesses, there is a single IFU ferrule per
# harness, plus some sky fibers.  For photometric standard calibration harnesses there are 3 individual
# IFU ferrules per harness, plus some sky fibers.
#
# The data structure is as follows.
#
# Header keywords:
# harName: The harness name (e.g., ma123)
# mjd: The mjd from which this file is valid
# nifu: The number of ifu fibers in the harness
# nsky: The number of sky fibers in the harness
# ntotal: The total number of fibers in the harness (ntotal=nifu+nsky)
# validMetro: Is the metrology solution valid? False indicates that there is a systematic offset in xpmm and ypmm
#
# Data table entries:
# 1) harblock: Which v-groove block of a harness the fiber is in (1 indexed)
# 2) finblock: Fiber number within the v-groove block (1-blocksize)
# 3) ifuinblock: IFU number within the v-groove block (0 for skies)
# 4) blocksize: Number of fibers in the block
# 5) fibertype: Either SKY or IFU
# 6) fnum: The numbering scheme internal to a given harness.  Positive valued for IFU fibers,
#    e.g., fnum=1-127 for a 127-fiber IFU, fnum=1-21 for calibration harnesses.
#    Negative valued for sky fibers (e.g. -1 through -8).
# 7) gbu: additive fiber flag 0=good, 1=low throughput, 2=out of order
# 8) frlCode: The code laser etched on the ferrule itself during production
# 9) xpmm: The x coordinate of the fiber relative to THE CENTRAL FIBER (test-stand frame)
# 10) ypmm: The y coordinate of the fiber relative to THE CENTRAL FIBER (test-stand frame)
# 11) tput: The throughput of the fiber as measured on the Sloan Test Stand

""".format(time.asctime(),self.commonData.location))
        par.write(f)

    def writeLogFileHeader(self):
        """Write header info to log file"""
        self.logFile.write("# {}\n".format(self.logFileName))
        self.logFile.write("""
# File generated at {}
# Test done on {} bench
#
# Units are as follows
# Throughput is in %%
# Current is in nanoamps
# Position is in mm
# Angle is in degrees

# Each line starts with a keyword indicating its contents:
# Src:  source intensity measurement
# AveSrc: average source intensity and percent change
# Meas: fiber intensity measurement
# Fit:  fiber fit
# Thru: throughput of one fiber
# AveThru: average and standard deviation of throughput for all fibers
# MinThru: minimum throughput for all fibers
# Fields are:
Src\tISrc
AveSrc\taveISrc\tsrcPctChange
Meas\tfiberNum\tmeasNum\tpos\tIFib
Fit\tfiberNum\tposPeak\tIPeak\tbeamWid
Thru\tvgrooveNum\tfiberNum\tthru\tangErr\thruLoss
AveThru\tthru\tstdDev
MinThru\tthru
# Where:
# - fiberNum is the number of the fiber in the anchor block (1 is closest to mounting hole)
# - vgrooveNum is the number of the fiber in the v-groove block (1 is right? when facing front of block)
# - srcPctChange is the percent in source intensity from initial to final measurement
# - beamWid is the beam width in degrees (edge to edge at the %0.0f%% level)
# - angErr is error in output angle = angle of fit peak - angle of desired peak
# - thruLoss is loss of throughput due to angle error = peak throughput - throughput at desired angle
""".format(time.asctime(),self.commonData.location,self.commonData.BeamEdgeFrac * 100.0))


class PushDialog:
    '''This class defines a window that asks the user if they want to
    push the results of a test to the shared git repo.
    '''

    def __init__(self, master, pushvar):
        top = self.top = tk.Toplevel(master)
        top.wm_title('Submit results?')
        self.pushvar = pushvar

        PushFrame = tk.Frame(top)
        tk.Label(PushFrame, text='Do you want to push the results to the shared repository?',
                 wraplength=140, anchor='w', justify='left').grid(column=0,row=0,columnspan=2)
        tk.Button(PushFrame, text='Yes',
                  command=self.yes).grid(column=0,row=1)
        tk.Button(PushFrame, text='No',
                  command=self.no).grid(column=1,row=1)
        PushFrame.pack()

    def yes(self):
        self.pushvar.set(True)
        self.top.destroy()

    def no(self):
        self.pushvar.set(False)
        self.top.destroy()

class AmmeterWarning:
    '''This class defines a window that alerts the user if the ammeter is not responding.
    The user has the option to cancel the test or try the ammeter again (usually after
    power-cycling the ammeter). The time of error is displayed because if it has been a long
    time since the ammeter failed it is possible the lamp has drifted an unacceptable amount
    and the test should be restarted.
    '''

    def __init__(self,master,cancel):
        self.cancel = cancel
        top = self.top = tk.Toplevel(master)
        top.wm_title('Ammeter Warning')

        colorargs = {'bg':'red','highlightbackground':'red'}
        WarningFrame = tk.Frame(top, **colorargs)
        tk.Label(WarningFrame, text='WARNING, ammeter is not responding. Please power cycle the ammeter and Try Again.',
                 wraplength=300,justify='left', **colorargs).grid(columnspan=2)
        tk.Label(WarningFrame, text='This message generated at {}'.format(time.asctime()), **colorargs).grid(columnspan=2)
        tk.Button(WarningFrame, text='Try Again', command=self.top.destroy, **colorargs).grid(row=2)
        tk.Button(WarningFrame, text='Stop Test', command=self.stop, **colorargs).grid(row=3)
        WarningFrame.pack()

class FiberRetest:
    '''This class defines a window that alerts the user to any fibers that tested as "bad".
    Often this is because the fibers are dirty, so the user is given the option to re-test just
    the bad fibers (presumably after cleaning). In this case a whole new data run is initiated
    with only the bad fibers. The experiment log will record all tests done this way, while the
    yanny file only conaints the most recent test for each fiber.
    '''

    def __init__(self,master,badfibers,retest):
        self.retest = retest
        top = self.top = tk.Toplevel(master)
        top.wm_title('Bad Fibers!')

        colorargs = {'bg':'red','highlightbackground':'red'}
        WarningFrame = tk.Frame(top, **colorargs)
        tk.Label(WarningFrame, text='WARNING, the following fibers tested bad:',
                 wraplength=300,justify='left', **colorargs).grid(columnspan=2)
        tk.Label(WarningFrame, text=str('{}\n'*len(badfibers)).format(*badfibers),
                justify='left',**colorargs).grid()
        tk.Label(WarningFrame, text='You can retest just these fibers now if you want', **colorargs).grid(columnspan=2)
        tk.Button(WarningFrame, text='Retest', command=self.test_again, **colorargs).grid(row=3,column=0)
        tk.Button(WarningFrame, text='End Test', command=self.stop, **colorargs).grid(row=3,column=1)
        WarningFrame.pack()

    def test_again(self):
        self.retest.set(True)
        self.top.destroy()

    def stop(self):
        self.retest.set(False)
        self.top.destroy()


if __name__ == "__main__":

    import Tkinter

    root = Tkinter.Tk()

    sr = RO.ScriptRunner.ScriptRunner(
        master = root,
        name = "FiberTester",
        scriptClass = ScriptClass,
        startNow = True,
    )

    root.mainloop()
