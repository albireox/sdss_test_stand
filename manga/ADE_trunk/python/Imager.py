import os
import time
import numpy as np
import pyfits
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages as PDF

EXP = '0<<'	# 3 ms
EXP = '0D'	# 1 ms

def iso_cent(image, tol=1.):

    data = pyfits.open(image)[0].data
    max_val = np.max(data)

    xlist = np.array([])
    ylist = np.array([])
    pivotlist = np.array([])
    idxdict = {}
    countlist = np.array([])

    for pivot in np.arange(0,max_val):

        hidx = data >= pivot
        lidx = data < pivot
        count = np.sum(hidx)
        if count < 31000:
            break
        idxdict[pivot] = [hidx,lidx]
        pivotlist = np.append(pivotlist, pivot)
        countlist = np.append(countlist,count)

    tmpdata = data.copy()
    for pivot in pivotlist:

        tmpdata[idxdict[pivot][0]] = 1
        tmpdata[idxdict[pivot][1]] = 0

        x, y = centroid(tmpdata)

        xlist = np.append(xlist,x)
        ylist = np.append(ylist,y)

    idx = find_chunk(xlist,ylist,tol=tol)
    xidx = idx
    yidx = idx

    xcent = np.mean(xlist[xidx])
    ycent = np.mean(ylist[yidx])

    #backwards because of the way ds9 and the stages are set up
    return [xcent, ycent]

def centroid(image):
    '''takes in an array and returns a list containing the
    center of mass of that array

    Originally from ADEUtils.py
    '''

    '''just to make sure we don't mess up the original data somehow'''
    data = np.copy(image) * 1.0

    size = data.shape
    totalMass = np.sum(data)

    xcom = np.sum(np.sum(data,1) * 1.*np.arange(size[0]))/totalMass
    ycom = np.sum(np.sum(data,0) * 1.*np.arange(size[1]))/totalMass

    return (xcom,ycom)

def find_chunk(x, y, tol=1.):

#    dd = np.diff(x)
    best_size = 0
    best_idx = []
    idx = []
    size = 0
    valx = x[0]
    valy = y[0]

    # if np.all(dd == 1):
    #     return range(len(x))

    for i, (xx, yy) in enumerate(zip(x,y)):

        if np.abs(xx - valx) <= tol and np.abs(yy - valy) <= tol:
            idx.append(i)
            size += 1

        elif size > best_size:
            best_size = size
            best_idx = idx
            size = 0
            idx = []
            valx = xx
            valy = yy

        else:
            size = 0
            idx = []
            valx = xx
            valy = yy

    best_idx.append(best_idx[-1] + 1)

    return np.array(best_idx)

def annulize(image,center,num_an=200):
    '''
    Description:
        annulize takes in a FITS image and computes the total power
        contained within progressivly larger annuli. The number of
        annuli used is set by the user and, unlike annulize_sb, it
        is the width of the annuli that remains constant, not the area.

    Inputs:
        data      - ndarray
                    The data to be annulized
        num_an    - Int
                    The number of annuli to use
        distances - ndarray
                    This is expected to be a transformed distance array

    Output:
        r_vec - ndarray
                vector of radii where each entry is the radius of the
                middle of the corresponding annulus.
        fluxes- ndarray
                each entry is the flux contained within that annulus.
        errors- ndarray
                The standard deviation of the pixels in each annulus.
    '''
    data = pyfits.open(image)[0].data
    dims = data.shape
    vecvec = np.indices(dims,dtype=np.float32)
    distances = ((center[0] - vecvec[0,])**2 + (center[1] - vecvec[1,])**2)**0.5

    rlimit = distances.max()
    rstep = rlimit/num_an

    'initialize future output arrays. I think that maybe with python'
    'these can be left empty, but w/e'
    fluxes = np.zeros(num_an, dtype=np.float32)
    r_vec = np.zeros(num_an, dtype=np.float32)
    area_array = np.zeros(dims, dtype=np.float32) + 1
    errors = np.zeros(num_an, dtype=np.float32)
    r1 = 0.0
    r2 = rstep

    for i in range(num_an):
        idx = np.where((distances <= r2) & (distances > r1))

        '''The correction is designed to correct annuli that are not
        entirely contained in the data array, but I still can't decide
        if it screws up the data or not'''
        correction = np.pi*(r2**2 - r1**2)/np.sum(area_array[idx])
        fluxes[i] = np.sum(data[idx])#*correction
        errors[i] = np.std(data[idx])

        r_mid = (r1 + r2)*0.5
        r_vec[i] = r_mid
        r1 = r2
        r2 = r1 + rstep

    return(r_vec,fluxes,errors)

def make_ds9_regions(fiberImages, harness_ID):
    '''Take a FiberImages or DirectImages object and make a ds9
    region file that shows apertures for f/3, f/4, and f/5.
    '''

    center = fiberImages.center
    radii = fiberImages.aperture_list

    f = open("/tmp/ds9_output.reg", "w")
    f.write('circle {1} {0} {2}\ncircle {1} {0} {3}\ncircle {1} {0} {4}\n'.
            format(*center+radii))
    f.write('text {} {} # text={{{}}}\n'.format(center[1],center[0]+radii[0]+20,'f/3.2'))
    f.write('text {} {} # text={{{}}}\n'.format(center[1],center[0]+radii[1]+20,'f/4'))
    f.write('text {} {} # text={{{}}}\n'.format(center[1],center[0]+radii[2]+20,'f/5'))
    f.write('text 812 1200 # text={{{}, {}}}\n'.format(harness_ID, fiberImages.basename))
    f.write('text 812 100 # text={{{}}}\n'.format(time.asctime()))
    f.close()

class FiberImages(object):

    def __init__(self, fibernum, slitnum, x2_center, manga_devices):

        self.fibnum = int(fibernum)
        self.slitnum = int(slitnum)
        self.x2_center = x2_center
        self.zpos_dict = {} # {z_position: image_file_name}
        self.basename = 'fiber_{:03n}'.format(self.fibnum)
        self.md = manga_devices

        self.angle = np.nan
        self.aperture_list = [] #radius, in px, for f3, f4, f5
        self.flux_list = [] #flux at f3, f4, f5
        self.tput_list = [] #[tput3, tput4, tput5]
        self.center = []
        self.isBroken = False

    def take_image(self, zpos):
        '''Take an image, rename it to something sensible, and update
        the zpos_dict with this information'''

        name = '{:}/{:}_{:04n}.fits'.format(self.md.workingdir,self.basename,int(zpos*100))
        self.md.proc_gige_loop_cmds('output', EXP)
        rawimage = self.md.proc_gige_loop_save_image('output')
        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))
        data = pyfits.open(name)[0].data
        if 255 in data:
            print('WARNING! IMAGE IS SATURATED')
        self.zpos_dict[zpos] = name

    def compute_flux(self):
        '''For all images in zpos_dict, compute the amount of light within
        the three different beam speads specified by aperture_list'''

        for zpos in self.zpos_dict:
            image = self.zpos_dict[zpos]
            r, sb, e = annulize(image, self.center)
            flux = np.cumsum(sb)
            self.flux_list = np.interp(self.aperture_list,r,flux)

    def compute_throughput(self,direct_flux):
        '''Given a FiberImages object for the sources measurements,
        compute the throughput at three different beam speeds'''

        self.tput_list = (self.flux_list/direct_flux)*100.

    def set_apertures(self,directImages):
        '''Given a FiberImages object set the current aperture list to
        match those of that object'''
        self.aperture_list = directImages.aperture_list

    def get_center(self, zpos):
        '''Find the center of the profile at a given zpos.
        We do this in a separate method because we want to draw
        the apertures before computing the flux.
        '''

        self.center = iso_cent(self.zpos_dict[zpos])

    def getVGNumSortKey(self):
        """Return a value for sorting by vg number"""
        return (self.slitnum, self.x2_center)

    def setVGBlockInfo(self, vgNum):
        """Set v-groove block position and compute desBeamCtrPos

        Inputs:
        - vgNum: v-groove groove number, starting from 1

        Note: fiber vgNum 1 is closest to the user when running the fiber tester
        """
        self.vgNum = int(vgNum)

class DirectImages(object):

    def __init__(self, manga_devices):

        self.zpos_dict = {} # {z_position: image_file_name}
        self.basename = 'direct'
        self.md = manga_devices

        self.angle = np.nan
        self.aperture_list = np.array([]) #radius, in px, for f3, f4, f5
        self.flux_list = np.empty(3) #flux at f3, f4, f5
        self.center = []

    def take_image(self, zpos, suffix=''):
        """Take an image, rename it, and update zpos_dict."""

        if suffix != '' and not suffix.startswith('_'):
            suffix = '_' + suffix

        name = '{:}/{:}_{:04n}{:}.fits'.format(self.md.workingdir,
                                               self.basename,
                                               int(zpos * 100),
                                               suffix)

        self.md.proc_gige_loop_cmds('output', EXP)
        rawimage = self.md.proc_gige_loop_save_image('output')

        os.system('manga_copy < {:} > {:}'.format(rawimage,name))
        os.system('rm {:}'.format(rawimage))

        data = pyfits.open(name)[0].data
        if 255 in data:
            print('WARNING! IMAGE IS SATURATED')

        self.zpos_dict[zpos] = name

    def compute_flux(self):
        '''For all images in zpos_dict, compute the amount of light within
        the three different beam speads specified by aperture_list'''

        for zpos in self.zpos_dict:
            image = self.zpos_dict[zpos]
            data = pyfits.open(image)[0].data
            center = iso_cent(image)
            r, sb, e = annulize(image,center)
            flux = np.cumsum(sb)
            tmplist = [self.aperture_list[0],self.aperture_list[0],self.aperture_list[0]]
            self.flux_list = np.vstack((self.flux_list,np.interp(tmplist,r,flux)))
            #self.flux_list = np.vstack((self.flux_list,np.array([np.sum(data),np.sum(data),np.sum(data)])))
            print '$$$$$'
            print self.flux_list

    def compute_avg_flux(self):
        '''Compute the average of beginning and ending direct measurements
        '''

        self.avg_flux = np.mean(self.flux_list[1:],axis=0)
        self.flux_change = np.diff(self.flux_list[1:],axis=0)/self.flux_list[1]

        print '*****'
        print self.flux_list, self.avg_flux

    def find_apertures(self,EEcut=0.45):
        '''Given a direct image find the radii, in px, corresponding to
        f/3, f/4, and f/5'''

        for zpos in self.zpos_dict:

            radius = self.get_radius(self.zpos_dict[zpos],EEcut=EEcut)
            rf5 = radius * 4.97/5.0
            rf4 = radius * 4.97/4.0
            rf3 = radius * 4.97/3.2

        self.aperture_list = [rf3, rf4, rf5]
        print "direct center:", self.center

    def get_radius(self,image,EEcut=0.2):
        """Takes in a 2D numpy array and computes the radius of the beam profile.
        This function uses a parabola fit to find the true beam radius.

        """

        center = iso_cent(image)
        self.center = center
        r, sb, err = annulize(image,center)

        EE = np.cumsum(sb/sb.sum())

        cutr = np.where(EE >= EEcut)[0][0]

        EEfit = np.poly1d(np.polyfit(r[:cutr],EE[:cutr],2))

        fitr = np.linspace(r.min(),r.max(),500)
        fitEE = EEfit(fitr)

        r1 = np.interp(1.0,fitEE,fitr)

        pp = PDF('direct.pdf')

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(r,EE,'.')#,markersize=1)
        ax.plot(fitr,fitEE,'-',alpha=0.4)
        ax.set_xlabel('r [px]')
        ax.set_ylabel('EE')
        ax.axvline(r1,linestyle='-',alpha=0.4)
        ax.axhline(1.0,linestyle=':',color='k',alpha=0.2)
        ax.set_ylim(0,1.1)
        ax.set_xlim(0,1.5*r1)
        ax.set_title('{}\nr: {:3.2f} px'.format(image,r1))
        pp.savefig(fig)
        pp.close()

        return r1
