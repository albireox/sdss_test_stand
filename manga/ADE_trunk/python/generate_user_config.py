#! /usr/bin/env python
# 
# Copyright Arthur D. Eigenbrot
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# uwisc-manga fiber test stand location INI generation script
# ********************************************************************

import os
import pwd
import sys
import ConfigParser

def main(filename, location):
    
    defaults = ConfigParser.ConfigParser()
    defaults.add_section("Metrology")
    defaults.add_section("Starting Positions")
    defaults.add_section("Misc")
    defaults.add_section("Throughput Requirements")
    
    if location == "UWisc":
        defaults.set("Metrology", "x2MetroPos", 48.5)
        defaults.set("Metrology", "y2MetroPos", 11.23)
        defaults.set("Metrology", "x1MetroPos", 25.27)
        defaults.set("Metrology", "y1MetroPos", 11.35)
        defaults.set("Starting Positions", "y2StartPos", 1.4)
        defaults.set("Metrology", "x1PPMetroPos", 12.57)
        defaults.set("Metrology", "y1PPMetroPos", 22.2)
    
    else:
        defaults.set("Metrology", "x2MetroPos", 48.5)
        defaults.set("Metrology", "y2MetroPos", 12.275)
        defaults.set("Metrology", "x1MetroPos", 25.67)
        defaults.set("Metrology", "y1MetroPos", 11.9)
        defaults.set("Starting Positions", "y2StartPos", 0.5)
        defaults.set("Metrology", "x1PPMetroPos", 13.57)
        defaults.set("Metrology", "y1PPMetroPos", 22.60)
        
    defaults.set("Metrology", "x1Offset", 0.70)
    defaults.set("Metrology", "y1Offset", 0.00)
    defaults.set("Metrology", "x1PPOffset", -1.5)
    defaults.set("Metrology", "y1PPOffset", 1.5)
    
    defaults.set("Starting Positions","startSearchPos", 11.7)
    
    defaults.set("Misc", "x1HomePos", 0.0)
    defaults.set("Misc", "y1HomePos", 25.0)
    defaults.set("Misc", "x1MinnieMove", 8.0)
    defaults.set("Misc", "slitHeight", 6.34)
    
    defaults.set("Throughput Requirements", "MinMeanThroughput", 87.0)    # minimum throughput averaged for all fibers, in %
    defaults.set("Throughput Requirements", "MinSingleThroughput", 85.0)  # minimum throughput for any one fiber, in %
    defaults.set("Throughput Requirements", "MaxFracSourceChange", 0.005)   # maximum reasonable fractional change in source current)
    
    with open(filename, 'w') as f:
        f.write("#Location is {}\n\n".format(location))
        defaults.write(f)
        
    return

if __name__ == "__main__":
    
    if pwd.getpwuid(os.getuid())[0] == 'SDSSfiber2':
        location = 'C-Tech'
    elif pwd.getpwuid(os.getuid())[0] == 'SDSSfiber':
        location = 'UWisc'
    else:
        print 'WARNING: Unrecognized location. Setting constants to UWisc'
        location = 'UWisc'
    
    try:
        main(sys.argv[1], location)
    except IndexError:
        main(os.path.expanduser("~/SDSSbench/manga_defaults.ini"),location)

