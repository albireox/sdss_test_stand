#! /usr/bin/env python
# file: $RCSfile$
# rcsid: $Id$
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# Reduces the plug plate metrology run
# ********************************************************************

import ConfigParser
import fileinput
import inspect
import math
import os
import re
import sys

import manga_devices as md

IMAGE_DIR = "/Users/mabadmin/manga"
#IMAGE_DIR = "/Users/mabadmin/manga-archive/manta"
#IMAGE_DIR = "/Volumes/Jeff's Vault/Backups/manga-archive/manta"

tag = inspect.stack()[0][3] # our name

print "{}: sys.argc: {}".format(tag, len(sys.argv))
print "{}: sys.argv: {}".format(tag, sys.argv)

configfile = "manga.cfg"
config = ConfigParser.ConfigParser()
config.read(configfile)

section = "MOTORCAL"
k = config.getfloat(section, "k")
alpha = config.getfloat(section, "alpha")
alpha_r = math.radians(alpha)
Jx = config.getfloat(section, "Jx")
Jy = config.getfloat(section, "Jy")


print "k: {}".format(k)
print "alpha: {}".format(alpha)
print "alpha_r: {}".format(alpha_r)
print "Jx: {}".format(Jx)
print "Jy: {}".format(Jy)

aoplist = []

for line in fileinput.input():
    line = line.strip()
    #print "line", line
    tokens = line.split()
    #print "tokens", tokens
    if tokens and tokens[0] == "proc_pp_fiber_acquire:":
        mx0 = float(tokens[4])
        my0 = float(tokens[6])
        mx1 = float(tokens[8])
        my1 = float(tokens[10])
        print "{}: mx0 {} my0 {} mx1 {} m1y {}".format(tag, mx0, my0, mx1, my1)
    elif tokens and tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
        if tokens[1] == "cachefile":
            continue
        image_file = tokens[2]
        print "{}: image_file {}".format(tag, image_file)
        image_path = os.path.join(IMAGE_DIR, image_file)
        print "{}: image_path {}".format(tag, image_file)
        if os.path.isfile(image_path):
            sx, sy = md.proc_manga_spot(image_path)
            dmx = k * (math.cos(alpha_r) * sx - math.sin(alpha_r) * sy)
            dmy = k * (math.sin(alpha_r) * sx + math.cos(alpha_r) * sy)
            print "{}: sx {} sy {} dmx {} dmy {}".format(tag, sx, sy, dmx, dmy)
            mx2 = mx1 + dmx
            my2 = my1 + dmy
            aoplist.append((mx0, my0, mx1, my1, sx, sy, mx2, my2))
            for ix, item in enumerate(aoplist):
                print "{}: ix {} item {}".format(tag, ix, item)
        else:
            print >> sys.stderr, "{}: file {} not found".format(tag, image_path)

for ix, item in enumerate(aoplist):
    print "{}: ix {} item {}".format(tag, ix, item)

with open("foo.txt", "wb") as fp:
    fp.write("tag1 tag2 ix mx0 my0 mx2 my2\n")
    for ix, item in enumerate(aoplist):
        mx0, my0, mx1, my1, sx, sy, mx2, my2 = item
        fp.write("{}: ix {}  {}  {}  {}  {}\n".format(tag, ix, mx0, my0, mx2, my2))
