#! /usr/bin/env python
# 
# Copyright Arthur D. Eigenbrot
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# uwisc-manga fiber test stand configuration utility
# ********************************************************************


import sys
import os
import Tkinter as tk
import tkFileDialog as tkFD
import ConfigParser
from datetime import datetime

class Defaults:
    """Holds all the default values for most harnesses and saves some input time
    
    """
    def __init__(self):
        
        self.slit_spacing = 0.177
        
        self.plugfile = 'plug-plate-map-file-b.map'
        self.sky_map = 'plug-plate-sky-map.map'
        
        self.IFU7_vg1num = 21
        self.IFU7_vg1numsky = 3
        self.IFU7_ifu1num = 7
        self.IFU7_ifu1numsky = 1
        self.IFU7_ifu2num = 7
        self.IFU7_ifu2numsky = 1
        self.IFU7_ifu3num = 7
        self.IFU7_ifu3numsky = 1
        self.IFU7_vg1space = self.slit_spacing
        self.IFU7_map = 'minnie-fiber-bundle-map.map'
        
        self.IFU19_vg1num = 19
        self.IFU19_vg1numsky = 2
        self.IFU19_vg1space = self.slit_spacing
        self.IFU19_ifu_map = 'production-fiber-map-rank-2.map'
        
        self.IFU37_vg1num = 37
        self.IFU37_vg1numsky = 2
        self.IFU37_vg1space = self.slit_spacing
        self.IFU37_ifu_map = 'production-fiber-map-rank-3.map'
        
        self.IFU61_vg1num = 30
        self.IFU61_vg1space = self.slit_spacing
        self.IFU61_vg1numsky = 2
        self.IFU61_vg2num = 31
        self.IFU61_vg2space = self.slit_spacing
        self.IFU61_vg2numsky = 2
        self.IFU61_ifu_map = 'production-fiber-map-rank-4.map'
        
        self.IFU91_vg1num = 30
        self.IFU91_vg1space = self.slit_spacing
        self.IFU91_vg1numsky = 2
        self.IFU91_vg2num = 30
        self.IFU91_vg2space = self.slit_spacing
        self.IFU91_vg2numsky = 2
        self.IFU91_vg3num = 31
        self.IFU91_vg3space = self.slit_spacing
        self.IFU91_vg3numsky = 2
        self.IFU91_ifu_map = 'production-fiber-map-rank-5.map'
        
        self.IFU127_vg1num = 30
        self.IFU127_vg1space = self.slit_spacing
        self.IFU127_vg1numsky = 2
        self.IFU127_vg2num = 30
        self.IFU127_vg2space = self.slit_spacing
        self.IFU127_vg2numsky = 2
        self.IFU127_vg3num = 30
        self.IFU127_vg3space = self.slit_spacing
        self.IFU127_vg3numsky = 2
        self.IFU127_vg4num = 37
        self.IFU127_vg4space = self.slit_spacing
        self.IFU127_vg4numsky = 2
        self.IFU127_ifu_map = 'production-fiber-map-rank-6.map'
        
        self.savepath = ''
        self.calib_file = ''

class ConfigWindow:

    def __init__(self, master, sessionWdg):
        
        self.master = master
        apt = tk.Frame(master)
        self.sessionWdg = sessionWdg
        
        self.Defaults = Defaults()
        
        self.previousINI = False
        try:
            self.sessionID = sessionWdg.getString()
        except AttributeError:
            """This only happens when you run this module by itself,
            for testing purposes
            """ 
            self.sessionID = None
            
        self.harnessType = tk.StringVar()
        self.savepath = tk.StringVar()
        self.harnessID = tk.StringVar()
        self.ferruleCode = tk.IntVar()
        self.numfibers = tk.IntVar()
        self.startfiber = tk.IntVar()
        self.status = tk.StringVar()
        self.overwrite = tk.BooleanVar()
        self.calibFile = tk.StringVar()
        
        self.startfiber.set(1)
        self.calibFile.set(self.Defaults.calib_file)
        
        self.MCALspacing = tk.DoubleVar()
        
        self.SHspacing = tk.DoubleVar()
        self.plugfile = tk.StringVar()
        
        self.vg1num = tk.IntVar()
        self.vg2num = tk.IntVar()
        self.vg3num = tk.IntVar()
        self.vg4num = tk.IntVar()
        self.vg1numsky = tk.IntVar()
        self.vg2numsky = tk.IntVar()
        self.vg3numsky = tk.IntVar()
        self.vg4numsky = tk.IntVar()
        self.vg1space = tk.DoubleVar()
        self.vg2space = tk.DoubleVar()
        self.vg3space = tk.DoubleVar()
        self.vg4space = tk.DoubleVar()
        self.mapfile = tk.StringVar()
        self.ifu_mapfile = tk.StringVar()
        self.sky_mapfile = tk.StringVar()
        
        '''for the minnie bundles'''
        self.ifu1num = tk.IntVar()
        self.ifu2num = tk.IntVar()
        self.ifu3num = tk.IntVar()
        self.ifu1numsky = tk.IntVar()
        self.ifu2numsky = tk.IntVar()
        self.ifu3numsky = tk.IntVar()
        self.MINNIE_ferrule1 = tk.IntVar()
        self.MINNIE_ferrule2 = tk.IntVar()
        self.MINNIE_ferrule3 = tk.IntVar()
        
        self.savepath.set(self.Defaults.savepath)
        self.ferruleCode.set(0)
        self.overwrite.set(False)

        instructfont = (None,13,'bold')
        ### Main options ###
        main_colorargs = {'bg':'light goldenrod','highlightbackground':'light goldenrod'}
        apt_Mainoptions = tk.Frame(apt,**main_colorargs)

        tk.Label(apt_Mainoptions,text='1. Set Harness ID and Save folder:',
                 font=instructfont,fg='red',**main_colorargs).grid(column=0,row=0,stick='w',columnspan=2)
        
        tk.Label(apt_Mainoptions,text='Harness ID',**main_colorargs).grid(column=0,row=1,stick='w')
        
        tk.Entry(apt_Mainoptions,width=10,
                textvariable=self.harnessID).grid(column=1,row=1,sticky='w')
        
        tk.Label(apt_Mainoptions,text='Save folder',**main_colorargs).grid(column=0,row=2,sticky='w')
        tk.Entry(apt_Mainoptions,width=30,
                 textvariable=self.savepath).grid(column=1,row=2,sticky='w')
        tk.Button(apt_Mainoptions,text='...',
                  command=self.save_path,**main_colorargs).grid(column=2,row=2)
        
        tk.Label(apt_Mainoptions, text='Calibration File',
                       **main_colorargs).grid(column=0,row=3,sticky='w')
        tk.Entry(apt_Mainoptions,width=30,
                 textvariable=self.calibFile).grid(column=1,row=3,sticky='w')
                                   
        tk.Button(apt_Mainoptions,text='...',
                  command=self.calib_path,
                  **main_colorargs).grid(column=2,row=3,sticky='w')

        tk.Label(apt_Mainoptions, text='2. Select Harness Type:',
                 font=instructfont,fg='red',**main_colorargs).grid(column=0,row=4,sticky='w',columnspan=2)
                 
        tk.Radiobutton(apt_Mainoptions,text='MaNGA production harness',
                                value='PROD',
                                variable = self.harnessType,
                                command = self.PROD_options,
                                **main_colorargs).grid(column=0,row=5,columnspan=2,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text="Single fiber harness",
                                        value='single',
                                        variable = self.harnessType,
                                        command=self.SH_options,
                                        **main_colorargs).grid(column=0,row=6,columnspan=2,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='IFU',
                                value='IFU',
                                variable = self.harnessType,
                                command = self.IFU_options,
                                **main_colorargs).grid(column=0,row=7,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='MaNGA calibration harness',
                                value='MCAL',
                                variable = self.harnessType,
                                command = self.MCAL_options,
                                **main_colorargs).grid(column=0,row=8,columnspan=2,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='Minni Bundles',
                                value='MINNIE',
                                variable = self.harnessType,
                                command = self.MINNIE_options,
                                **main_colorargs).grid(column=0,row=9,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='Single fiber -> MTP',
                                value='SHMTP',
                                variable = self.harnessType,
                                **main_colorargs).grid(column=0,row=10,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='MTP -> MTP',
                                value='MTPMTP',
                                variable = self.harnessType,
                                **main_colorargs).grid(column=0,row=11,sticky='w')
        tk.Radiobutton(apt_Mainoptions,text='MTP -> V-groove',
                                value='MTPVG',
                                variable = self.harnessType,
                                **main_colorargs).grid(column=0,row=12,sticky='w')
        tk.Button(apt_Mainoptions,text='Save & Close',
                                    command=self.close,
                                    highlightcolor = 'green',
                                    **main_colorargs).grid(column=0,row=13,sticky='w')
        ####
        
        ### File dialog ###
        apt_Filedialog = tk.Frame(apt,**main_colorargs)
        tk.Button(apt_Filedialog,
                  text='Load Configuration File',
                  command=self.load_ini,
                  **main_colorargs).grid(sticky='w')
        tk.Checkbutton(apt_Filedialog,
                       text='overwrite',
                       variable = self.overwrite,
                       **main_colorargs).grid(column=1,row=0,sticky='w')
        
        ### Status bar ###
        apt_Status = tk.Frame(apt,bg='pink')
        tk.Label(apt_Status,textvariable=self.status,bg='pink',fg='red').grid(sticky='n')
        ####
        
        ### IFU options ###
        IFU_colorargs = {'bg':'green','highlightbackground':'green'}
        self.apt_IFUoptions = tk.Frame(apt,**IFU_colorargs)
        tk.Label(self.apt_IFUoptions,text='3. Select IFU Type:',
                 font=instructfont,fg='red',**IFU_colorargs).grid()
        
        num_buttons = [('19 fiber IFU',19,self.load_19defs),
                       ('37 fiber IFU',37,self.load_37defs),
                       ('61 fiber IFU',61,self.load_61defs),
                       ('91 fiber IFU',91,self.load_91defs),
                       ('127 fiber IFU',127,self.load_127defs)]
        for name, value, func in num_buttons:
            tk.Radiobutton(self.apt_IFUoptions,
                text=name,
                value=value,
                variable=self.numfibers,
                command = func,
                **IFU_colorargs).grid(sticky='w')

        tk.Label(self.apt_IFUoptions,
                 text = '\n4. Customize Harness\n(optional):',
                 font = instructfont, fg='red', justify = 'left',
                 **IFU_colorargs).grid(sticky='w')
            
        tk.Button(self.apt_IFUoptions,
                  text = 'customize...',
                  command = self.IFUcustom,
                  **IFU_colorargs).grid(sticky='w')
        ####
        
        ### IFU customization ###
        IFUcustom_colorargs = {'bg':'light green','highlightbackground':'light green'}
        self.apt_IFUcustom = tk.Frame(apt,**IFUcustom_colorargs)
        tk.Label(self.apt_IFUcustom,
                 text='IFU Customization',
                 **IFUcustom_colorargs).grid(column=0,row=0,columnspan=3,sticky='n')
        tk.Label(self.apt_IFUcustom,
                 text='V-Groove',**IFUcustom_colorargs).grid(column=0,row=1,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='Numfibers',**IFUcustom_colorargs).grid(column=1,row=1,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='Spacing (mm)',**IFUcustom_colorargs).grid(column=2,row=1,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='VG-1',**IFUcustom_colorargs).grid(column=0,row=2,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='VG-2',**IFUcustom_colorargs).grid(column=0,row=3,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='VG-3',**IFUcustom_colorargs).grid(column=0,row=4,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text='VG-4',**IFUcustom_colorargs).grid(column=0,row=5,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg1num).grid(column=1,row=2,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg2num).grid(column=1,row=3,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg3num).grid(column=1,row=4,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg4num).grid(column=1,row=5,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg1space).grid(column=2,row=2,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg2space).grid(column=2,row=3,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg3space).grid(column=2,row=4,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width=5,textvariable=self.vg4space).grid(column=2,row=5,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text = 'map file:',
                 **IFUcustom_colorargs).grid(column=0,row=6,sticky = 'w')
        tk.Entry(self.apt_IFUcustom,
                 width = 20,
                 textvariable = self.mapfile).grid(column=0,row=7,columnspan=2,sticky='w')
        tk.Button(self.apt_IFUcustom,
                  text = '...',
                  command = self.load_IFU_mapfile,
                  **IFUcustom_colorargs).grid(column=2,row=7,sticky='w')
        tk.Label(self.apt_IFUcustom,
                 text = 'start fiber:',
                 **IFUcustom_colorargs).grid(column=0,row=8,sticky='w')
        tk.Entry(self.apt_IFUcustom,
                 width = 5,
                 textvariable = self.startfiber).grid(column=1,row=8,sticky='w')
        ####
                 
        ####Prodcution options###         
        PROD_colorargs = {'bg':'AntiqueWhite3','highlightbackground':'AntiqueWhite3'}
        self.apt_PRODoptions = tk.Frame(apt,**PROD_colorargs)
        tk.Label(self.apt_PRODoptions,text='3. Select Harness Type:',
                 font=instructfont,fg='red',**PROD_colorargs).grid()
        
        num_buttons = [('19 fiber IFU',19,self.load_19defs),
                       ('37 fiber IFU',37,self.load_37defs),
                       ('61 fiber IFU',61,self.load_61defs),
                       ('91 fiber IFU',91,self.load_91defs),
                       ('127 fiber IFU',127,self.load_127defs)]
        for name, value, func in num_buttons:
            tk.Radiobutton(self.apt_PRODoptions,
                text=name,
                value=value,
                variable=self.numfibers,
                command = func,
                **PROD_colorargs).grid(sticky='w')

        tk.Label(self.apt_PRODoptions,
                 text = '\n4. Customize Harness\n(optional):',
                 font = instructfont, fg='red', justify = 'left',
                 **PROD_colorargs).grid(sticky='w')
            
        tk.Button(self.apt_PRODoptions,
                  text = 'customize...',
                  command = self.PRODcustom,
                  **PROD_colorargs).grid(sticky='w')
        ####
        
        ### Production customization ###
        PRODcustom_colorargs = {'bg':'AntiqueWhite2','highlightbackground':'AntiqueWhite2'}
        self.apt_PRODcustom = tk.Frame(apt,**PRODcustom_colorargs)
        tk.Label(self.apt_PRODcustom,
                 text='Harness Customization',
                 **PRODcustom_colorargs).grid(column=0,row=0,columnspan=3,sticky='n')
        tk.Label(self.apt_PRODcustom,
                 text='V-Groove',**PRODcustom_colorargs).grid(column=0,row=1,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='Numfibers',**PRODcustom_colorargs).grid(column=1,row=1,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='Numsky',**PRODcustom_colorargs).grid(column=2,row=1,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='Spacing (mm)',**PRODcustom_colorargs).grid(column=3,row=1,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='VG-1',**PRODcustom_colorargs).grid(column=0,row=2,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='VG-2',**PRODcustom_colorargs).grid(column=0,row=3,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='VG-3',**PRODcustom_colorargs).grid(column=0,row=4,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text='VG-4',**PRODcustom_colorargs).grid(column=0,row=5,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg1num).grid(column=1,row=2,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg2num).grid(column=1,row=3,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg3num).grid(column=1,row=4,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg4num).grid(column=1,row=5,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg1numsky).grid(column=2,row=2,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg2numsky).grid(column=2,row=3,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg3numsky).grid(column=2,row=4,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg4numsky).grid(column=2,row=5,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg1space).grid(column=3,row=2,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg2space).grid(column=3,row=3,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg3space).grid(column=3,row=4,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width=5,textvariable=self.vg4space).grid(column=3,row=5,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text = 'IFU map file:',
                 **PRODcustom_colorargs).grid(column=0,row=6,sticky = 'w')
        tk.Entry(self.apt_PRODcustom,
                 width = 30,
                 textvariable = self.ifu_mapfile).grid(column=0,row=7,columnspan=4,sticky='w')
        tk.Button(self.apt_PRODcustom,
                  text = '...',
                  command = self.load_IFU_mapfile,
                  **PRODcustom_colorargs).grid(column=3,row=7,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text = 'Plug-plate map file:',
                 **PRODcustom_colorargs).grid(column=0,row=8,sticky = 'w')
        tk.Entry(self.apt_PRODcustom,
                 width = 30,
                 textvariable = self.sky_mapfile).grid(column=0,row=9,columnspan=4,sticky='w')
        tk.Button(self.apt_PRODcustom,
                  text = '...',
                  command = self.load_SH_plugfile,
                  **PRODcustom_colorargs).grid(column=3,row=9,sticky='w')
        tk.Label(self.apt_PRODcustom,
                 text = 'start fiber:',
                 **PRODcustom_colorargs).grid(column=0,row=10,sticky='w')
        tk.Entry(self.apt_PRODcustom,
                 width = 5,
                 textvariable = self.startfiber).grid(column=1,row=10,sticky='w')
        ####
        
        ####Prodcution options###         
        MINNIE_colorargs = {'bg':'AntiqueWhite3','highlightbackground':'AntiqueWhite3'}
        self.apt_MINNIEoptions = tk.Frame(apt,**MINNIE_colorargs)
        tk.Label(self.apt_MINNIEoptions,text='3. MINNIE OPTIONS LOADED:',
                 font=instructfont,fg='red',**MINNIE_colorargs).grid()

        tk.Label(self.apt_MINNIEoptions,
                 text = '\n4. Customize Harness\n(optional):',
                 font = instructfont, fg='red', justify = 'left',
                 **MINNIE_colorargs).grid(sticky='w')
            
        tk.Button(self.apt_MINNIEoptions,
                  text = 'customize...',
                  command = self.MINNIEcustom,
                  **MINNIE_colorargs).grid(sticky='w')
        ####
        
        ### Production customization ###
        MINNIEcustom_colorargs = {'bg':'AntiqueWhite2','highlightbackground':'AntiqueWhite2'}
        self.apt_MINNIEcustom = tk.Frame(apt,**MINNIEcustom_colorargs)
        tk.Label(self.apt_MINNIEcustom,
                 text='Harness Customization',
                 **MINNIEcustom_colorargs).grid(column=0,row=0,columnspan=3,sticky='n')
        tk.Label(self.apt_MINNIEcustom,
                 text='IFU',**MINNIEcustom_colorargs).grid(column=0,row=1,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text='Numfibers',**MINNIEcustom_colorargs).grid(column=1,row=1,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text='Numsky',**MINNIEcustom_colorargs).grid(column=2,row=1,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text='IFU-1',**MINNIEcustom_colorargs).grid(column=0,row=2,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text='IFU-2',**MINNIEcustom_colorargs).grid(column=0,row=3,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text='IFU-3',**MINNIEcustom_colorargs).grid(column=0,row=4,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu1num).grid(column=1,row=2,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu2num).grid(column=1,row=3,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu3num).grid(column=1,row=4,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu1numsky).grid(column=2,row=2,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu2numsky).grid(column=2,row=3,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width=5,textvariable=self.ifu3numsky).grid(column=2,row=4,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text = 'Slit spacing:',
                 **MINNIEcustom_colorargs).grid(column=0,row=5,sticky = 'w')
        tk.Entry(self.apt_MINNIEcustom,
                 width = 5,
                 textvariable = self.vg1space).grid(column=1,row=5,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text = 'IFU map file:',
                 **MINNIEcustom_colorargs).grid(column=0,row=6,sticky = 'w')
        tk.Entry(self.apt_MINNIEcustom,
                 width = 30,
                 textvariable = self.ifu_mapfile).grid(column=0,row=7,columnspan=4,sticky='w')
        tk.Button(self.apt_MINNIEcustom,
                  text = '...',
                  command = self.load_IFU_mapfile,
                  **MINNIEcustom_colorargs).grid(column=3,row=7,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text = 'Plug-plate map file:',
                 **MINNIEcustom_colorargs).grid(column=0,row=8,sticky = 'w')
        tk.Entry(self.apt_MINNIEcustom,
                 width = 30,
                 textvariable = self.sky_mapfile).grid(column=0,row=9,columnspan=4,sticky='w')
        tk.Button(self.apt_MINNIEcustom,
                  text = '...',
                  command = self.load_SH_plugfile,
                  **MINNIEcustom_colorargs).grid(column=3,row=9,sticky='w')
        tk.Label(self.apt_MINNIEcustom,
                 text = 'start fiber:',
                 **MINNIEcustom_colorargs).grid(column=0,row=10,sticky='w')
        tk.Entry(self.apt_MINNIEcustom,
                 width = 5,
                 textvariable = self.startfiber).grid(column=1,row=10,sticky='w')
        ####
        
        ### Single fiber harness options ###
        SH_colorargs = {'bg':'deep sky blue','highlightbackground':'deep sky blue'}
        self.apt_SHoptions = tk.Frame(apt,**SH_colorargs)
        tk.Label(self.apt_SHoptions,text='Number of fibers',
                 **SH_colorargs).grid()
        tk.Entry(self.apt_SHoptions,width=10,textvariable=self.numfibers).grid()
        tk.Button(self.apt_SHoptions,
                  text='customize...',
                  command = self.SHcustom,
                  **SH_colorargs).grid(sticky='n')
        ####
        
        ### Single fiber harness customization ###
        SHcustom_colorargs = {'bg':'light blue','highlightbackground':'light blue'}
        self.apt_SHcustom = tk.Frame(apt,**SHcustom_colorargs)
        tk.Label(self.apt_SHcustom,
                 text='Single Harness Customization',
                 **SHcustom_colorargs).grid(column=0,row=0,columnspan=3,sticky='w')
        tk.Label(self.apt_SHcustom,
                 text='V-Groove spacing:',
                 **SHcustom_colorargs).grid(column=0,row=1,sticky='w')
        tk.Label(self.apt_SHcustom,
                 text='Plug file:',
                 **SHcustom_colorargs).grid(column=0,row=2,sticky='w')
        tk.Entry(self.apt_SHcustom,
                 width = 5,textvariable=self.SHspacing).grid(column=1,row=1,sticky='w')
        tk.Entry(self.apt_SHcustom,
                 width = 20,textvariable = self.plugfile).grid(column=0,columnspan=2,row=3,sticky='w')
        tk.Button(self.apt_SHcustom,
                  text='...',
                  command=self.load_SH_plugfile,
                  **SHcustom_colorargs).grid(column=2,row=3,sticky='w')
        tk.Label(self.apt_SHcustom,
                 text = 'start fiber:',
                 **SHcustom_colorargs).grid(column=0,row=4,sticky='e')
        tk.Entry(self.apt_SHcustom,
                 width = 5,
                 textvariable = self.startfiber).grid(column=1,row=4,sticky='w')
        ####

        ### MaNGA calibration options
        MCAL_colorargs = {'bg':'magenta','highlightbackground':'magenta'}
        self.apt_MCALoptions = tk.Frame(apt,**MCAL_colorargs)
        tk.Label(self.apt_MCALoptions,text='Number of fibers',
                 **MCAL_colorargs).grid()
        tk.Entry(self.apt_MCALoptions,width=10,textvariable=self.numfibers).grid()
        tk.Button(self.apt_MCALoptions,
                  text='customize...',
                  command = self.MCALcustom,
                  **MCAL_colorargs).grid(sticky='n')
        ####
        
        ### MaNGA calibration customization ###
        MCALcustom_colorargs = {'bg':'violet','highlightbackground':'violet'}
        self.apt_MCALcustom = tk.Frame(apt,**MCALcustom_colorargs)
        tk.Label(self.apt_MCALcustom,
                 text='MaNGA Cakibration Harness Customization',
                 **MCALcustom_colorargs).grid(column=0,row=0,columnspan=3,sticky='w')
        tk.Label(self.apt_MCALcustom,
                 text='V-Groove spacing:',
                 **MCALcustom_colorargs).grid(column=0,row=1,sticky='w')
        tk.Entry(self.apt_MCALcustom,
                 width = 5,textvariable=self.MCALspacing).grid(column=1,row=1,sticky='w')
        tk.Label(self.apt_MCALcustom,
                 text = 'start fiber:',
                 **MCALcustom_colorargs).grid(column=0,row=4,sticky='e')
        tk.Entry(self.apt_MCALcustom,
                 width = 5,
                 textvariable = self.startfiber).grid(column=1,row=4,sticky='w')
        ###

        self.apt_Blankframe = tk.Frame(apt,bg='yellow')
        tk.Label(self.apt_Blankframe,text='Harness options',bg='yellow').grid()
        
        self.apt_Blankcustom = tk.Frame(apt,bg='light yellow')
        tk.Label(self.apt_Blankcustom,text='Custom options',bg='light yellow').grid()

        apt_Status.pack(side='bottom',expand='true',fill='x')
        apt_Filedialog.pack(side='top',expand='true',fill='x')
        apt_Mainoptions.pack(side='left',expand='true',fill='both')
        
        apt.grid(column=10,row=0,rowspan=20,columnspan=20,sticky='nw')
        apt.grid_remove()
        self.apt = apt
        
    def show(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt.grid()

    def load_ini(self):
        filename = self.load_filename()
        if filename == '':
            return
        self.previousINI = filename
        input_options = ConfigParser.ConfigParser()
        input_options.read(filename)
        
        self.overwrite.set(True)
        
        self.harnessID.set(input_options.get('Global Info','harnessID'))
        self.savepath.set(input_options.get('Global Info','savePath'))
        self.calibFile.set(input_options.get('Global Info','calibration_file'))

        self.startfiber.set(input_options.get('Harness Info','startFiber'))
        harnesstype = input_options.get('Harness Info','harnessType')
        self.harnessType.set(harnesstype)
        
        if harnesstype == 'IFU':
            self.mapfile.set(input_options.get('Harness Info','mapfile'))
            for numfunc, spacefunc, name in zip([self.vg1num.set,
                                                 self.vg2num.set,
                                                 self.vg3num.set,
                                                 self.vg4num.set],
                                                [self.vg1space.set,
                                                 self.vg2space.set,
                                                 self.vg3space.set,
                                                 self.vg4space.set],
                                                ['VG1','VG2','VG3','VG4']):
                try:
                    numfunc(input_options.getint('Harness Info','{}_numfibers'.format(name)))
                    spacefunc(input_options.getfloat('Harness Info','{}_spacing'.format(name)))
                except ConfigParser.NoOptionError:
                    continue
            self.IFU_options()
            
        elif harnesstype == 'PROD':
            self.ifu_mapfile.set(input_options.get('Harness Info','ifu_mapfile'))
            self.sky_mapfile.set(input_options.get('Harness Info','sky_mapfile'))
            for numfunc, spacefunc, skyfunc, name in zip([self.vg1num.set,
                                                 self.vg2num.set,
                                                 self.vg3num.set,
                                                 self.vg4num.set],
                                                [self.vg1space.set,
                                                 self.vg2space.set,
                                                 self.vg3space.set,
                                                 self.vg4space.set],
                                                [self.vg1numsky.set,
                                                 self.vg2numsky.set,
                                                 self.vg3numsky.set,
                                                 self.vg4numsky.set],
                                                ['VG1','VG2','VG3','VG4']):
                try:
                    numfunc(input_options.getint('Harness Info','{}_numfibers'.format(name)))
                    spacefunc(input_options.getfloat('Harness Info','{}_spacing'.format(name)))
                    skyfunc(input_options.getint('Harness Info','{}_numsky'.format(name)))
                except ConfigParser.NoOptionError:
                    continue
            self.PROD_options()
            
        elif harnesstype == 'MINNIE':
            self.ifu_mapfile.set(input_options.get('Harness Info','ifu_mapfile'))
            self.sky_mapfile.set(input_options.get('Harness Info','sky_mapfile'))
            self.vg1space.set(input_options.get('Harness Info','VG1_spacing'))
            for numfunc, skyfunc, name in zip([self.ifu1num.set,
                                      self.ifu2num.set,
                                      self.ifu3num.set],
                                     [self.ifu1numsky.set,
                                      self.ifu2numsky.set,
                                      self.ifu3numsky.set],
                                     ['IFU1','IFU2','IFU3']):
                numfunc(input_options.getint('Harness Info','{}_numfibers'.format(name)))
                skyfunc(input_options.getint('Harness Info','{}_numsky'.format(name)))
            self.MINNIE_options()
            
            
        elif harnesstype == 'single':
            self.SH_options()
            self.SHspacing.set(input_options.getfloat('Harness Info','fibersep'))
            self.plugfile.set(input_options.get('Harness Info','plugfile'))
            
        self.numfibers.set(input_options.getint('Harness Info','numfibers'))
        print self.numfibers.get()

    def load_filename(self):
        return tkFD.askopenfilename(title='Load file',initialdir=self.savepath.get())
        
    def save_path(self):
        name = tkFD.askdirectory(title='Choose a save directory',initialdir=self.savepath.get())
        self.savepath.set(name)
        if name != '':
            self.calibFile.set(name+'/manga.cfg')
    
    def IFU_options(self):
        self.numfibers.set(0)
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_SHoptions.pack_forget()
        self.apt_SHcustom.pack_forget()
        self.apt_MCALcustom.pack_forget()
        self.apt_MCALoptions.pack_forget()
        self.apt_PRODcustom.pack_forget()
        self.apt_PRODoptions.pack_forget()
        self.apt_MINNIEcustom.pack_forget()
        self.apt_MINNIEoptions.pack_forget()
        self.apt_IFUoptions.pack(side='left',expand='true',fill='y')
        
    def IFUcustom(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_Blankcustom.pack_forget()
        self.apt_IFUcustom.pack(side='left',expand='true',fill='y')
        
    def PROD_options(self):
        self.numfibers.set(0)
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_SHoptions.pack_forget()
        self.apt_SHcustom.pack_forget()
        self.apt_MCALcustom.pack_forget()
        self.apt_MCALoptions.pack_forget()
        self.apt_IFUcustom.pack_forget()
        self.apt_IFUoptions.pack_forget()
        self.apt_MINNIEcustom.pack_forget()
        self.apt_MINNIEoptions.pack_forget()
        self.apt_PRODoptions.pack(side='left',expand='true',fill='y')
        
    def PRODcustom(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_Blankcustom.pack_forget()
        self.apt_PRODcustom.pack(side='left',expand='true',fill='y')
        
    def MINNIE_options(self):
        self.numfibers.set(0)
        self.load_7defs()
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_SHoptions.pack_forget()
        self.apt_SHcustom.pack_forget()
        self.apt_MCALcustom.pack_forget()
        self.apt_MCALoptions.pack_forget()
        self.apt_IFUcustom.pack_forget()
        self.apt_IFUoptions.pack_forget()
        self.apt_PRODcustom.pack_forget()
        self.apt_PRODoptions.pack_forget()
        self.apt_MINNIEoptions.pack(side='left',expand='true',fill='y')
        
    def MINNIEcustom(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_Blankcustom.pack_forget()
        self.apt_MINNIEcustom.pack(side='left',expand='true',fill='y')
        
    def load_IFU_mapfile(self):
        filename = self.load_filename()
        self.mapfile.set(filename)
        
    def load_7defs(self):
        
        self.vg1num.set(self.Defaults.IFU7_vg1num)
        self.vg1space.set(self.Defaults.IFU7_vg1space)
        self.vg1numsky.set(self.Defaults.IFU7_vg1numsky)
        self.numfibers.set(21)
        self.vg2num.set(0)
        self.vg2space.set(0.0)
        self.vg3num.set(0)
        self.vg3space.set(0.0)
        self.vg4num.set(0)
        self.vg4space.set(0.0)
        self.ifu1num.set(self.Defaults.IFU7_ifu1num)
        self.ifu1numsky.set(self.Defaults.IFU7_ifu1numsky)
        self.ifu2num.set(self.Defaults.IFU7_ifu2num)
        self.ifu2numsky.set(self.Defaults.IFU7_ifu2numsky)
        self.ifu3num.set(self.Defaults.IFU7_ifu3num)
        self.ifu3numsky.set(self.Defaults.IFU7_ifu3numsky)
        self.ifu_mapfile.set(self.Defaults.IFU7_map)
        self.mapfile.set(self.Defaults.IFU7_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        
    def load_19defs(self):
        
        self.vg1num.set(self.Defaults.IFU19_vg1num)
        self.vg1space.set(self.Defaults.IFU19_vg1space)
        self.vg1numsky.set(self.Defaults.IFU19_vg1numsky)
        self.vg2num.set(0)
        self.vg2space.set(0.0)
        self.vg2numsky.set(0)
        self.vg3num.set(0)
        self.vg3space.set(0.0)
        self.vg3numsky.set(0)
        self.vg4num.set(0)
        self.vg4space.set(0.0)
        self.vg4numsky.set(0)
        self.ifu_mapfile.set(self.Defaults.IFU19_ifu_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        self.mapfile.set(self.Defaults.IFU19_ifu_map)
        
    def load_37defs(self):
        self.vg1num.set(self.Defaults.IFU37_vg1num)
        self.vg1space.set(self.Defaults.IFU37_vg1space)
        self.vg1numsky.set(self.Defaults.IFU37_vg1numsky)
        self.vg2num.set(0)
        self.vg2space.set(0.0)
        self.vg2numsky.set(0)
        self.vg3num.set(0)
        self.vg3space.set(0.0)
        self.vg3numsky.set(0)
        self.vg4num.set(0)
        self.vg4space.set(0.0)
        self.vg4numsky.set(0)
        self.ifu_mapfile.set(self.Defaults.IFU37_ifu_map)
        self.mapfile.set(self.Defaults.IFU37_ifu_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        
    def load_61defs(self):
        
        self.vg1num.set(self.Defaults.IFU61_vg1num)
        self.vg1space.set(self.Defaults.IFU61_vg1space)
        self.vg1numsky.set(self.Defaults.IFU61_vg1numsky)
        self.vg2num.set(self.Defaults.IFU61_vg2num)
        self.vg2space.set(self.Defaults.IFU61_vg2space)
        self.vg2numsky.set(self.Defaults.IFU61_vg2numsky)
        self.vg3num.set(0)
        self.vg3space.set(0.0)
        self.vg3numsky.set(0)
        self.vg4num.set(0)
        self.vg4space.set(0.0)
        self.vg4numsky.set(0)
        self.ifu_mapfile.set(self.Defaults.IFU61_ifu_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        self.mapfile.set(self.Defaults.IFU61_ifu_map)
        
    def load_91defs(self):
        self.vg1num.set(self.Defaults.IFU91_vg1num)
        self.vg1space.set(self.Defaults.IFU91_vg1space)
        self.vg1numsky.set(self.Defaults.IFU91_vg1numsky)
        self.vg2num.set(self.Defaults.IFU91_vg2num)
        self.vg2space.set(self.Defaults.IFU91_vg2space)
        self.vg2numsky.set(self.Defaults.IFU91_vg2numsky)
        self.vg3num.set(self.Defaults.IFU91_vg3num)
        self.vg3space.set(self.Defaults.IFU91_vg3space)
        self.vg3numsky.set(self.Defaults.IFU91_vg3numsky)
        self.vg4num.set(0)
        self.vg4space.set(0.0)
        self.vg4numsky.set(0)
        self.ifu_mapfile.set(self.Defaults.IFU91_ifu_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        self.mapfile.set(self.Defaults.IFU91_ifu_map)

    def load_127defs(self):
        
        self.vg1num.set(self.Defaults.IFU127_vg1num)
        self.vg1space.set(self.Defaults.IFU127_vg1space)
        self.vg1numsky.set(self.Defaults.IFU127_vg1numsky)
        self.vg2num.set(self.Defaults.IFU127_vg2num)
        self.vg2space.set(self.Defaults.IFU127_vg2space)
        self.vg2numsky.set(self.Defaults.IFU127_vg2numsky)
        self.vg3num.set(self.Defaults.IFU127_vg3num)
        self.vg3space.set(self.Defaults.IFU127_vg3space)
        self.vg3numsky.set(self.Defaults.IFU127_vg3numsky)
        self.vg4num.set(self.Defaults.IFU127_vg4num)
        self.vg4space.set(self.Defaults.IFU127_vg4space)
        self.vg4numsky.set(self.Defaults.IFU127_vg4numsky)
        self.ifu_mapfile.set(self.Defaults.IFU127_ifu_map)
        self.sky_mapfile.set(self.Defaults.sky_map)
        self.mapfile.set(self.Defaults.IFU127_ifu_map)
        
    def SH_options(self):
        self.numfibers.set(0)
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_IFUcustom.pack_forget()
        self.apt_IFUoptions.pack_forget()
        self.apt_MCALcustom.pack_forget()
        self.apt_MCALoptions.pack_forget()
        self.apt_PRODcustom.pack_forget()
        self.apt_PRODoptions.pack_forget()
        self.apt_MINNIEcustom.pack_forget()
        self.apt_MINNIEoptions.pack_forget()
        self.apt_SHoptions.pack(side='left',expand='true',fill='y')
        self.load_SHdefs()
        
    def SHcustom(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_Blankcustom.pack_forget()
        self.apt_SHcustom.pack(side='left',expand='true',fill='y')
        
    def load_SH_plugfile(self):
        filename = self.load_filename()
        self.plugfile.set(filename)
        
    def MCAL_options(self):
        self.numfibers.set(0)
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_IFUcustom.pack_forget()
        self.apt_IFUoptions.pack_forget()
        self.apt_SHcustom.pack_forget()
        self.apt_SHoptions.pack_forget()
        self.apt_PRODcustom.pack_forget()
        self.apt_PRODoptions.pack_forget()
        self.apt_MINNIEcustom.pack_forget()
        self.apt_MINNIEoptions.pack_forget()
        self.apt_MCALoptions.pack(side='left',expand='true',fill='y')
        
    def MCALcustom(self):
        self.apt.winfo_toplevel().wm_geometry("")
        self.apt_Blankcustom.pack_forget()
        self.apt_MCALcustom.pack(side='left',expand='true',fill='y')
    
    def load_SHdefs(self):
        
        self.SHspacing.set(self.Defaults.slit_spacing)
        self.plugfile.set(self.Defaults.plugfile)
            
    def calib_path(self):
        
        name = tkFD.asksaveasfilename(title='Choose a calibration file')
        self.calibFile.set(name)
    
    def close(self):
        
        if self.harnessID.get() == '':
            self.status.set('Please set the harness ID')
            return
            
        if self.harnessType.get() == '':
            self.status.set('Please select a harness type')
            return
        
        if self.numfibers.get() < 1:
            self.status.set("Warning: the number of fibers ({}) doesn't make sense".\
                            format(self.numfibers.get()))
            return
        
        if self.savepath.get() == '':
            self.status.set('Warning: the save path is not good')
            return
        
        self.writeoutput()
        self.status.set("")
        self.apt.grid_remove()
        
    def writeoutput(self):
        
        self.sessionID = self.sessionWdg.getString()
        
        if self.overwrite.get():
            if self.previousINI:
                inifile = self.previousINI
        elif self.previousINI:
            inifile = tkFD.asksaveasfilename(title='Save a new INI file',initialdir=self.savepath.get())
        else:
            if not self.sessionID:
                self.sessionID = self.harnessID.get()
            inifile = '{}/{}.ini'.format(self.savepath.get(),
                                            self.sessionID)
        
        options = ConfigParser.ConfigParser()
        options.add_section('Global Info')
        options.set('Global Info','harnessID',value=self.harnessID.get())
        options.set('Global Info','savepath',value=self.savepath.get())
        options.set('Global Info','calibration_file',value=self.calibFile.get())
        
        options.add_section('Harness Info')
        options.set('Harness Info','harnessType',value=self.harnessType.get())
        
        if self.harnessType.get() == 'IFU':
            rank_dict = {1:0,7:1,19:2,37:3,61:4,91:5,127:6}
            rank = rank_dict[self.numfibers.get()]
            options.set('Harness Info','rank',value=str(rank))
            
            for num, space, name in zip([self.vg1num.get(),
                                         self.vg2num.get(),
                                         self.vg3num.get(),
                                         self.vg4num.get()],
                                        [self.vg1space.get(),
                                         self.vg2space.get(),
                                         self.vg3space.get(),
                                         self.vg4space.get()],
                                        ['VG1','VG2','VG3','VG4']):
                if num != 0:
                    options.set('Harness Info','{}_numfibers'.format(name),value=str(num))
                    options.set('Harness Info','{}_spacing'.format(name),value=str(space))
            options.set('Harness Info','mapfile',value=self.mapfile.get())
            
        elif self.harnessType.get() == 'PROD':
            rank_dict = {1:0,7:1,19:2,37:3,61:4,91:5,127:6}
            rank = rank_dict[self.numfibers.get()]
            options.set('Harness Info','rank',value=str(rank))
            
            for num, sky, space, name in zip([self.vg1num.get(),
                                              self.vg2num.get(),
                                             self.vg3num.get(),
                                             self.vg4num.get()],
                                            [self.vg1numsky.get(),
                                             self.vg2numsky.get(),
                                             self.vg3numsky.get(),
                                             self.vg4numsky.get()],
                                            [self.vg1space.get(),
                                             self.vg2space.get(),
                                             self.vg3space.get(),
                                             self.vg4space.get()],
                                             ['VG1','VG2','VG3','VG4']):
                if num != 0:
                    options.set('Harness Info','{}_numfibers'.format(name),value=str(num))
                    options.set('Harness Info','{}_spacing'.format(name),value=str(space))
                    options.set('Harness Info','{}_numsky'.format(name),value=str(sky))
            options.set('Harness Info','ifu_mapfile',value=self.ifu_mapfile.get())
            options.set('Harness Info','sky_mapfile',value=self.sky_mapfile.get())
            options.set('Harness Info', 'ferrulecode',value=str(self.ferruleCode.get()))
        
        elif self.harnessType.get() == 'MINNIE':
            options.set('Harness Info','rank',value=str(1))
            options.set('Harness Info','VG1_spacing',str(self.vg1space.get()))
            options.set('Harness Info','ifu_mapfile',value=self.ifu_mapfile.get())
            options.set('Harness Info','sky_mapfile',value=self.sky_mapfile.get())
            options.set('Harness Info','ifu1_ferrulecode',value=str(self.MINNIE_ferrule1.get()))
            options.set('Harness Info','ifu2_ferrulecode',value=str(self.MINNIE_ferrule2.get()))
            options.set('Harness Info','ifu3_ferrulecode',value=str(self.MINNIE_ferrule3.get()))
            for num, sky, name in zip([self.ifu1num.get(),
                                      self.ifu2num.get(),
                                      self.ifu3num.get()],
                                     [self.ifu1numsky.get(),
                                      self.ifu2numsky.get(),
                                      self.ifu3numsky.get()],
                                     ['IFU1','IFU2','IFU3']):
                options.set('Harness Info','{}_numfibers'.format(name),str(num))
                options.set('Harness Info','{}_numsky'.format(name),str(sky))
        
        elif self.harnessType.get() == 'single':
            options.set('Harness Info','fiberSep',value=str(self.SHspacing.get()))
            options.set('Harness Info','plugFile',value=self.plugfile.get())
            options.set('Harness Info','numFibers',value=str(self.numfibers.get()))
        
        elif self.harnessType.get() == 'MCAL':
            options.set('Harness Info','fiberSep',value=str(self.MCALspacing.get()))
            options.set('Harness Info','numFibers',value=str(self.numfibers.get()))
            
        options.set('Harness Info','startFiber',value=str(self.startfiber.get()))
        options.set('Harness Info','numfibers',value=str(self.numfibers.get()))
            
        f = open(inifile,'w')
        f.write('# Configuration file generated on {}\n'.format(
            datetime.now().isoformat(' ')))
        f.write('# Slayer rules!\n')
        options.write(f)
        f.close()
        
        self.options = options
    
    def get_options(self):
        try:
            return self.options
        except AttributeError:
            return False
        
    def set_sessionID(self,sessionID):
        self.sessionID = sessionID

class BigButton:
    def __init__(self, master, mainscript):
        top = self.top = tk.Toplevel(master)
        top.wm_title('Preset Control')
        self.harnessID = tk.StringVar()
        self.ferruleCode = tk.IntVar()
        self.MINNIE_ferrule1 = tk.IntVar()
        self.MINNIE_ferrule2 = tk.IntVar()
        self.MINNIE_ferrule3 = tk.IntVar()
        self.mainscript = mainscript
        self.CW = self.mainscript.cw
        self.harnesstype = 'PROD'
#        self.numfibers = numfibers

        #self.def_dict = {19: self.CW.load_19defs, 127: self.CW.load_127defs}
        
        self.harness_frame = tk.Frame(top)
        tk.Label(self.harness_frame,
                 text = 'Select number of IFU fibers:').grid(column=0,row=0,columnspan=3,sticky='news')
        tk.Button(self.harness_frame,
                  text = 'MINNIE',
                  command = self.choose7).grid(column=0,row=1,sticky='w')
        tk.Button(self.harness_frame, 
                  text='19', 
                  command = self.choose19).grid(column=1,row=1,sticky='w')
        tk.Button(self.harness_frame,
                  text = '37',
                  command = self.choose37).grid(column=2,row=1,sticky='w')
        tk.Button(self.harness_frame,
                  text = '61',
                  command = self.choose61).grid(column=0,row=2,sticky='w')
        tk.Button(self.harness_frame,
                  text = '91',
                  command = self.choose91).grid(column=1,row=2,sticky='w')
        tk.Button(self.harness_frame,
                  text = '127',
                  command = self.choose127).grid(column=2,row=2,sticky='w')
        
        self.ID_frame = tk.Frame(top)
        tk.Label(self.ID_frame,text='Please enter:\nharness ID').pack()
        tk.Entry(self.ID_frame, textvariable=self.harnessID).pack()
        tk.Label(self.ID_frame,text='ferrule code').pack()
        tk.Entry(self.ID_frame, textvariable=self.ferruleCode).pack()
        tk.Button(self.ID_frame, text='OK', command=self.doEverything).pack()
        
        self.MINNIE_frame = tk.Frame(top)
        tk.Label(self.MINNIE_frame,text='Please enter harness ID:',justify='left').grid(column=0,row=0,columnspan=3)
        tk.Entry(self.MINNIE_frame, textvariable=self.harnessID).grid(column=0,row=1,columnspan=3)
        tk.Label(self.MINNIE_frame,text=' and ferrule\ncode for IFUs:',justify='left').grid(column=0,row=2,rowspan=3)
        tk.Label(self.MINNIE_frame,text='1').grid(column=1,row=2)
        tk.Label(self.MINNIE_frame,text='2').grid(column=1,row=3)
        tk.Label(self.MINNIE_frame,text='3').grid(column=1,row=4)
        tk.Entry(self.MINNIE_frame, textvariable=self.MINNIE_ferrule1,width=5).grid(column=2,row=2)
        tk.Entry(self.MINNIE_frame, textvariable=self.MINNIE_ferrule2,width=5).grid(column=2,row=3)
        tk.Entry(self.MINNIE_frame, textvariable=self.MINNIE_ferrule3,width=5).grid(column=2,row=4)
        tk.Button(self.MINNIE_frame, text='OK', command=self.doEverything).grid(column=1,row=5)
        
        self.harness_frame.pack()
        self.top.bind("<Return>", self.doEverything)
        
    def choose7(self):
        self.CW.load_7defs()
        self.CW.numfibers.set(7)
        self.harness_frame.pack_forget()
        self.harnesstype = 'MINNIE'
        self.MINNIE_frame.pack()
        
    def choose19(self):
        self.CW.load_19defs()
        self.CW.numfibers.set(19)
        self.harness_frame.pack_forget()
        self.ID_frame.pack()
        
    def choose37(self):
        self.CW.load_37defs()
        self.CW.numfibers.set(37)
        self.harness_frame.pack_forget()
        self.ID_frame.pack()
        
    def choose61(self):
        self.CW.load_61defs()
        self.CW.numfibers.set(61)
        self.harness_frame.pack_forget()
        self.ID_frame.pack()

    def choose91(self):
        self.CW.load_91defs()
        self.CW.numfibers.set(91)
        self.harness_frame.pack_forget()
        self.ID_frame.pack()
    
    def choose127(self):
        self.CW.load_127defs()
        self.CW.numfibers.set(127)
        self.harness_frame.pack_forget()
        self.ID_frame.pack()
        
        
    def doEverything(self, evt=None):
        

        self.CW.harnessID.set(self.harnessID.get())

        saveroot = os.path.expanduser('~/SDSSbench/data')
        now = datetime.now()
        dateroot = saveroot+'/{:4n}{:02n}{:02n}'.format(now.year,now.month,now.day)
        defaultsave = dateroot+'/'+self.mainscript.sessionWdg.get()
        print 'sesasionID is {}'.format(self.mainscript.sessionWdg.get())
        print "Defaultsave is {}".format(defaultsave)

        if not os.path.exists(dateroot):
            os.makedirs(dateroot)
        n=2
        while os.path.exists(defaultsave):
            if defaultsave[-2] == '_':
                defaultsave = defaultsave[:-1] + str(n)
            else:
                defaultsave += '_{}'.format(n)
            n += 1
        
        os.makedirs(defaultsave)

        self.CW.savepath.set(defaultsave)
        self.CW.calibFile.set(defaultsave+'/manga.cfg')
        self.CW.harnessType.set(self.harnesstype)
        self.CW.ferruleCode.set(self.ferruleCode.get())
        self.CW.MINNIE_ferrule1.set(self.MINNIE_ferrule1.get())
        self.CW.MINNIE_ferrule2.set(self.MINNIE_ferrule2.get())
        self.CW.MINNIE_ferrule3.set(self.MINNIE_ferrule3.get())
        self.CW.close()
        
        self.mainscript.DoAcq.set(True)
        self.mainscript.DoTput.set(True)
        
        self.top.destroy()
        

def main():
    configroot = tk.Tk()
    frame = tk.Frame(configroot)
    cw = ConfigWindow(frame,None)
    #configroot.attributes('-topmost',True)
    frame.pack()
    cw.show() 
    configroot.mainloop()
    
        
if __name__ == '__main__':
    sys.exit(main())
