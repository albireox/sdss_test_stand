#! /usr/bin/env python
# file: $RCSfile: manga_devices.py,v $
# rcsid: $Id: manga_devices.py,v 1.5 2013/08/27 16:33:28 jwp Exp jwp $
# Copyright Jeffrey W Percival
# ********************************************************************
# Do not use this software without permission.
# Do not use this software without attribution.
# Do not remove or alter any of the lines above.
# ********************************************************************
# manga device python wrappers
# ********************************************************************

import datetime
import inspect
import math
import os
import subprocess
import sys
import time
import ConfigParser
import manga_mfo as mfo

DEVICE_API_WRAPPERS = 0

def proc_dlp(cmd):
    """Operates the DLP digital IO device."""
    tag = inspect.stack()[0][3] # our name

    print "{}: cmd {}".format(tag, cmd)

    try:
        out = subprocess.check_output(["dlp_led", cmd])
        print "{}: out {}".format(tag, out)
    except Exception as e:
        print >> sys.stderr, "{}: Can't control LED".format(tag)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return

def proc_rbd_query():
    """Operates the RBD ammeter device."""
    tag = inspect.stack()[0][3] # our name

    print "{}: query ammeter".format(tag)

    reading = 0

    try:
        # reply should look like
        # rbd_query: 1354554580 377764 &S=,Range=020nA,+12.037,nA
        out = subprocess.check_output(["rbd_query"])
        out = " ".join(out.split()) # squeeze out extra white space
        print "{}: out {}".format(tag, out)
        out = out.strip()
        out = out.replace(",", " ")
        out = out.split()
        reading = float(out[5])
        units = out[6]
        print "{}: reading {} {}".format(tag, reading, units)
    except Exception as e:
        print >> sys.stderr, "{}: Can't read RBD ammeter".format(tag)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return -1

    return reading

def proc_apt_query(teststand, stage):
    tag = inspect.stack()[0][3] # our name

    print "{}: query teststand {} stage {}".format(tag, teststand, stage)

    reading = 0

    try:
        out = subprocess.check_output(["apt_query", teststand, stage])
        out = " ".join(out.split()) # squeeze out extra white space
        print "{}: out {}".format(tag, out)
        s1 = out.split()
        reading = float(s1[6])
        print "{}: teststand {} stage {} reading {} mm".format(tag, teststand, stage, reading)
    except Exception as e:
        print >> sys.stderr, "{}: Can't query teststand {} stage {}".format(tag, teststand, stage)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return reading

def proc_apt_move_home(teststand, stage):
    tag = inspect.stack()[0][3] # our name

    print "{}: teststand {} stage {} --> home".format(tag, teststand, stage)

    try:
        out = subprocess.check_output(["apt_move_home", teststand, stage])
        out = " ".join(out.split()) # squeeze out extra white space
        print "{}: out {}".format(tag, out)
    except Exception as e:
        print >> sys.stderr, "{}: Can't home teststand {} stage {}".format(tag, teststand, stage)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return


def proc_apt_move_absolute(teststand, stage, pos):
    tag = inspect.stack()[0][3] # our name

    print "{}: teststand {} stage {} --> {}".format(tag, teststand, stage, pos)

    try:
        out = subprocess.check_output(["apt_move_absolute", teststand, stage, str(pos)])
        out = " ".join(out.split()) # squeeze out extra white space
        print "{}: out {}".format(tag, out)
    except Exception as e:
        print >> sys.stderr, "{}: Can't move teststand {} stage {}".format(tag, teststand, stage)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return

def proc_apt_move_block(teststand, stage):
    tag = inspect.stack()[0][3] # our name

    print "{}: block on teststand {} stage {}".format(tag, teststand, stage)

    try:
        out = subprocess.check_output(["apt_move_block", teststand, stage])
        out = " ".join(out.split()) # squeeze out extra white space
        print "{}: out {}".format(tag, out)
    except Exception as e:
        print >> sys.stderr, "{}: Can't block on teststand {} stage {}".format(tag, teststand, stage)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return

def proc_gige_loop_notify():
    """Sends the kill signal to the gige_loop program."""
    tag = inspect.stack()[0][3] # our name

    print "{}:".format(tag)

    pid = 0

    # find the gige_loop PID
    try:
        out = subprocess.check_output(["ps", "-ao", "comm,pid"])
        #print "{}: out\n{}".format(tag, out)
        lines = out.split("\n")
        for line in lines:
            tokens = line.split()
            if tokens and tokens[0] == "gige_loop":
                pid = tokens[1]
    except Exception as e:
        print >> sys.stderr, "{}: Can't get pid".format(tag)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)
        return

    if pid == 0:
        print >> sys.stderr, "{}: Can't get pid".format(tag)
        return

    cmd = "kill -USR1 {}".format(pid)
    print "{}: cmd {}".format(tag, cmd)

    try:
        out = subprocess.check_output(cmd.split())
        print "{}: out {}".format(tag, out)
        time.sleep(1)
    except Exception as e:
        print >> sys.stderr, "{}: Can't send signal".format(tag)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return

def proc_gige_loop_cmds(camera, cmds):
    """Sends a command string to gige_loop."""
    tag = inspect.stack()[0][3] # our name

    print "{}:".format(tag)

    filename = "/tmp/gige_loop.cmds.{}.txt".format(camera)

    try:
        with open(filename, "wb") as f:
            print >> sys.stderr, "{}: Cmds {}".format(tag,cmds)
            f.write(cmds)
    except Exception as e:
        print >> sys.stderr, "{}: Can't open file {} for writing".format(tag, filename)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    proc_gige_loop_notify()

    return

def proc_gige_loop_save_image(camera, destdir=False):
    """Saves a gige_loop image and returns the filename."""
    tag = inspect.stack()[0][3] # our name

    cachefile = "/tmp/gige_loop.save.{}.txt".format(camera)
    if not os.path.exists(cachefile):
        os.system('touch {}'.format(cachefile))
    f = open(cachefile, 'r')
    oldfile = f.readline().strip()
    f.close()

    print "{}: cachefile {}".format(tag, cachefile)
    #print '{}: oldfile {}'.format(tag,oldfile)
    # peek at the current modification time, so we can watch for changes
    mt1 = 0;
    try:
        mt1 = os.stat(cachefile).st_mtime
    except Exception as e:
        print >> sys.stderr, "{}: Can't stat file {}".format(tag, cachefile)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    # invoke the save action
    proc_gige_loop_cmds(camera, "s")

    # wait for the action to end
    for i in range(0, 10):
        mt2 = os.stat(cachefile).st_mtime
        print >> sys.stderr, "{}: waiting for save {} {} > {}".format(tag, i, mt2, mt1)
        if mt2 > mt1:
            print >> sys.stderr, "{}: wait is done".format(tag)
            break
        time.sleep(1)

    savefile = "/dev/null"  # default

    # try to read the save-file name
    try:
        with open(cachefile, "r") as fp:
            out = fp.readline()
            savefile = out.strip()
            
    except Exception as e:
        print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, cachefile)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    if savefile == oldfile:
        print "Trying again..."
        return proc_gige_loop_save_image(camera, destdir)
        
    if destdir:
        newlocation = destdir + '/' + savefile
        os.system('mv {} {}'.format(savefile,newlocation))
        savefile = newlocation
        
    print "{}: savefile {}".format(tag, savefile)
        
    return savefile

IMAGE_PROCESSING_API = 0

def proc_manga_sim(prefix):
    tag = inspect.stack()[0][3] # our name

    print "{}: prefix {}".format(tag, prefix)

    # generate the expected output filename
    ofile = prefix + ".raw"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Sim file {} already exists".format(tag, ofile)
    else:
        print "{}: Sim file {}".format(tag, ofile)
        f1 = open(ofile, "w")

        try:
            cmd = ["manga_sim"]
            out = subprocess.call(cmd, stdout=f1)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't sim file {}".format(tag, ofile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

        f1.close()

    return ofile

def proc_manga_copy(image_file):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {}".format(tag, image_file)

    # generate the expected output filename
    root, ext = os.path.splitext(image_file)
    if ext == ".fits":
        ofile = root + ".copy.fits"
    else:
        ofile = root + ".fits"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Copy file {} already exists".format(tag, ofile)
    else:
        print "{}: Copy file {} <-- {}".format(tag, ofile, image_file)
        try:
            cmd = ["manga_copy", image_file]
            out = subprocess.call(cmd)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't copy file {}".format(tag, image_file)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return ofile

def proc_manga_spot(image_file, tilesize=48):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {} tilesize {}".format(tag, image_file, tilesize)

    # generate the expected output filename
    root, _ = os.path.splitext(image_file)
    ofile = root + ".spot.out"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Spot file {} already exists".format(tag, ofile)
    else:
        image_path = mfo.find_file(image_file)
        if image_path is None:
            print >> sys.stderr, "{}: Can't find image file {}".format(tag, image_file)
            return (0, 0)
        print "{}: Run Spot transform on file {}".format(tag, image_path)
        try:
            cmd = ["manga_spot", "-tilesize", str(tilesize), image_path]
            out = subprocess.call(cmd)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't run Spot transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return (0, 0)

    # the output looks like:
    # manga_spot: tilesize 42 xc 807.537983 yc 560.135524
    with open(ofile, "r") as fp:
        line = fp.readline()
        line = line.strip()
        #print "{}: line {}".format(tag, line)
        tokens = line.split()
        if tokens and tokens[0] == "manga_spot:":
            xc = float(tokens[4])
            yc = float(tokens[6])
    print "{}: xc {} yc {}".format(tag, xc, yc)

    return (xc, yc)

def proc_manga_sobel(image_file):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {}".format(tag, image_file)

    # generate the expected output filename
    root, _ = os.path.splitext(image_file)
    ofile = root + ".sobel.fits"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Sobel file {} already exists".format(tag, ofile)
    else:
        print "{}: Run Sobel transform on file {}".format(tag, image_file)
        try:
            cmd = ["manga_sobel", image_file]
            out = subprocess.call(cmd)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't run Sobel transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return ofile

def proc_manga_hough(image_file, r0=21):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {} r0 {}".format(tag, image_file, r0)

    # generate the expected output filename
    root, _ = os.path.splitext(image_file)
    ofile = root + ".hough.fits"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Hough file {} already exists".format(tag, ofile)
    else:
        print "{}: Run Hough transform on file {}".format(tag, image_file)
        try:
            cmd = ["manga_hough", "-r0", str(r0), image_file]
            out = subprocess.call(cmd)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't run Hough transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return ofile

def proc_manga_blobs(image_file):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {}".format(tag, image_file)

    # generate the expected output filename
    root, _ = os.path.splitext(image_file)
    ofile = root + ".blobs.fits"
    print "{}: ofile {}".format(tag, ofile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Blobs file {} already exists".format(tag, ofile)
    else:
        print "{}: Run Blobs transform on file {}".format(tag, image_file)
        try:
            cmd = ["manga_blobs", image_file]
            out = subprocess.call(cmd)
            print "{}: out {}".format(tag, out)
        except Exception as e:
            print >> sys.stderr, "{}: Can't run Blobs transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return ofile

def proc_manga_spikes(image_file):
    tag = inspect.stack()[0][3] # our name

    print "{}: image_file {}".format(tag, image_file)

    # generate the expected output filename
    root, _ = os.path.splitext(image_file)
    ofile = root + ".spikes.out"
    print "{}: ofile {}".format(tag, ofile)
    # we keep the error output because it has good stuff
    efile = root + ".spikes.err"
    print "{}: efile {}".format(tag, efile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: Spikes file {} already exists".format(tag, ofile)
    else:
        print "{}: Run Spikes transform on file {}".format(tag, image_file)
        try:
            f2 = open(efile, "wb")
            cmd = ["manga_spikes", image_file]
            out = subprocess.call(cmd, stderr=f2)
            print "{}: out {}".format(tag, out)
            f2.close()
        except Exception as e:
            print >> sys.stderr, "{}: Can't run Spikes transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    return ofile

def proc_manga_ifu(spikefilename):
    """Invokes manga_ifu on a spikes file."""
    tag = inspect.stack()[0][3] # our name

    print "{}: spikefilename {}".format(tag, spikefilename)

    # generate the expected output filename
    root, _ = os.path.splitext(spikefilename)
    ofile = root + ".ifu.out"
    print "{}: ofile {}".format(tag, ofile)
    # we keep the error output because it has good stuff
    efile = root + ".ifu.err"
    print "{}: efile {}".format(tag, efile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: IFU file {} already exists".format(tag, ofile)
    else:
        print "{}: Run IFU transform on file {}".format(tag, spikefilename)
        try:
            f2 = open(efile, "wb")
            cmd = ["manga_ifu", spikefilename]
            out = subprocess.call(cmd, stderr=f2)
            print "{}: out {}".format(tag, out)
            f2.close()
        except Exception as e:
            print >> sys.stderr, "{}: Can't run IFU transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    c = theta = sx = sy = 0
    ylist = []
    with open(ofile, "r") as fp:
        lines = fp.readlines()
        for line in lines:
            line = line.strip()
            #print "line", line
            tokens = line.split()
            if tokens:
                if tokens[0] == "manga_ifu:":
                    if tokens[1] == "solution:":
                        if tokens[2] == "aop:":
                            token = tokens[3]
                            value = tokens[4]
                            #print "token {} v {}".format(token, value)
                            if token == "c:": c = float(value)
                            if token == "theta:": theta = math.radians(float(value))
                            if token == "sx:": sx = float(value)
                            if token == "sy:": sy = float(value)
                    elif tokens[1] == "residuals:":
                        if tokens[2] == "ix":
                            pass    # header line
                        else:
                            ix = int(tokens[2])
                            f = int(tokens[3])
                            xy_x = float(tokens[4])
                            xy_y = float(tokens[5])
                            im_x = float(tokens[6])
                            im_y = float(tokens[7])
                            dx = float(tokens[8])
                            dy = float(tokens[9])
                            dr = float(tokens[10])
                            ylist.append((ix, f, xy_x, xy_y, im_x, im_y, dx, dy, dr))

    print "{}: aop:     c: {:12.6f}".format(tag, c)
    print "{}: aop: theta: {:12.6f} deg".format(tag, math.degrees(theta))
    print "{}: aop:    sx: {:12.6f}".format(tag, sx)
    print "{}: aop:    sy: {:12.6f}".format(tag, sy)
#    for ix, item in enumerate(ylist):
#        print "{}: ylist: ix {} item {}".format(tag, ix, item)
    flist = sorted(ylist, key=lambda tup: tup[1])
    for ix, item in enumerate(flist):
        print "{}: flist: ix {} item {}".format(tag, ix, item)

    aop = (c, theta, sx, sy)
    return (aop, flist)

def proc_manga_aop(filename):
    """Runs the motor_aop program to solve the given mapping."""
    tag = inspect.stack()[0][3] # our name

    # generate the expected output filename
    root, _ = os.path.splitext(filename)
    ofile = root + ".aop.out"
    print "{}: ofile {}".format(tag, ofile)
    # we keep the error output because it has good stuff
    efile = root + ".aop.err"
    print "{}: efile {}".format(tag, efile)

    # see if it's already been run
    if os.path.isfile(ofile):
        print "{}: AOP file {} already exists".format(tag, ofile)
    else:
        print "{}: Run AOP transform on file {}".format(tag, filename)
        try:
            f2 = open(efile, "wb")
            cmd = ["manga_aop", filename]
            out = subprocess.call(cmd, stderr=f2)
            print "{}: out {}".format(tag, out)
            f2.close()
        except Exception as e:
            print >> sys.stderr, "{}: Can't run AOP transform".format(tag)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    c = theta = sx = sy = 0
    with open(ofile, "r") as fp:
        lines = fp.readlines()
        for line in lines:
            line = line.strip()
            #print "line", line
            tokens = line.split()
            if tokens:
                if tokens[0] == "manga_aop:":
                    if tokens[1] == "solution:":
                        if tokens[2] == "aop:":
                            token = tokens[3]
                            value = tokens[4]
                            #print "token {} v {}".format(token, value)
                            if token == "c:": c = float(value)
                            if token == "theta:": theta = math.radians(float(value))
                            if token == "sx:": sx = float(value)
                            if token == "sy:": sy = float(value)

    print "{}: aop:     c: {:12.6f}".format(tag, c)
    print "{}: aop: theta: {:12.6f} deg".format(tag, math.degrees(theta))
    print "{}: aop:    sx: {:12.6f}".format(tag, sx)
    print "{}: aop:    sy: {:12.6f}".format(tag, sy)

    return (c, theta, sx, sy)

def proc_manga_motor_cal(mx1, my1, sx1, sy1, mx2, my2, sx2, sy2):
    """Runs the motor cal program to determine the motor constants."""
    tag = inspect.stack()[0][3] # our name

    cmd = "manga_motor_cal -v".format()

    args1 = " -mx1 {} -my1 {} -sx1 {} -sy1 {}".format(-1 * mx1, my1, sx1, sy1)

    args2 = " -mx2 {} -my2 {} -sx2 {} -sy2 {}".format(-1 * mx2, my2, sx2, sy2)

    cmd = (cmd + args1 + args2).split()
    print "{}: cmd {}".format(tag, cmd)

    # defaults
    k = 1
    alpha = 0
    Jx = 0
    Jy = 0

    try:
        out = subprocess.check_output(cmd)
        print "{}: out {}".format(tag, out)
        tokens = out.split()
        if tokens:
# manga_motor_cal: solution: k   1.00000000 mm/pix alpha  36.86989765 deg Jx    1.200 mm Jy   -0.600 mm
            k = float(tokens[3])
            alpha = math.radians(float(tokens[6]))
            Jx = float(tokens[9])
            Jy = float(tokens[12])
    except Exception as e:
        print >> sys.stderr, "{}: Can't run motor cal transform".format(tag)
        print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
        print >> sys.stderr, "{}: Exception: {}".format(tag, e)

    print "{}: aop:     k: {:12.6f}".format(tag, k)
    print "{}: aop: alpha: {:12.6f}".format(tag, math.degrees(alpha))
    print "{}: aop:    Jx: {:12.6f}".format(tag, Jx)
    print "{}: aop:    Jy: {:12.6f}".format(tag, Jy)

    return (k, alpha, Jx, Jy)

class MangaDevices:
    """Main class for interacting with manga_devices
    """

    def __init__(self, cmfo=None):
        
        #access to the device API commands
        self.proc_dlp = proc_dlp
        self.proc_rbd_query = proc_rbd_query
        self.proc_apt_query = proc_apt_query
        self.proc_apt_move_home = proc_apt_move_home
        self.proc_apt_move_absolute = proc_apt_move_absolute
        self.proc_apt_move_block = proc_apt_move_block
        self.proc_gige_loop_notify = proc_gige_loop_notify
        self.proc_gige_loop_cmds = proc_gige_loop_cmds
        self.proc_gige_loop_save_image = proc_gige_loop_save_image

        #If we weren't given a preloaded manga_MFO then we'll make one
        if cmfo == None:
            cmfo = mfo.MFO()
        self.CMfo = cmfo
       
        self.clear_regions_in_ds9 = self.CMfo.clear_regions_in_ds9
       
        #Set the defaults that might be changed later
        self.configfile = 'manga.cfg'
        self.set_ifu_map('production-fiber-map-rank-3.map')
        self.pp_xoffset = 15
        self.pp_yoffset = 6
        self.fiberlist = [-1,-2]
        self.workingdir = os.getcwd()
        
        return
    
#     def proc_gige_loop_save_image(self,camera):
#         
#         return proc_gige_loop_save_image(camera,self.workingdir)
    
    def set_configfile(self, path):
        """Set the file that will contain the results of all the configuration routines
        """
        
        self.configfile = path
        
        """"also set the config file in mfo. We might want to do this in *_fiber_acquire
        but I think this is OK for now because all mfo does is read the config file, so the
        instance here should always be a slave to MangaDevices
        """
        self.CMfo.set_configfile(path)
        
        return

    def get_configfile(self):
        
        return self.configfile

    def set_pp_metro(self, x, y):
        """Set the starting location for pp_metrology. This needs to be the location of
        fiber 1 in the plug plate
        """
        
        """These numbers are hard-coded in because they are the location in
        relative plug-plate space of hole #0"""
        self.pp_xoffset = x - 10.5
        self.pp_yoffset = y - 10.75
        
        return

    def set_pp_fiber_limits(self, start, end):
        """Sets the starting and ending pp fibers. These are used by pp_metrology,
        and setting them correctly can greatly reduce your metrology time.
        """
        
        self.fiberlist = range(end,start+1)[::-1]
        
        return
    
    def set_pp_fiberlist(self,list):
        
        self.fiberlist = list
        
        return

    def set_ifu_map(self, mapfile):
        """Sets the ifu-map file used by mfo
        """
        
        self.CMfo.set_ifu_map(mapfile)
        
        return
    
    def set_pp_map(self, mapfile):
        """Sets the pp map file used by mfo
        """
        
        self.CMfo.set_pp_map(mapfile)
        
        return
    
    def set_workingdir(self, directory):
        """Sets the working directory for md. This is primarily used to control
        where gige_loop saves images
        """
        
        self.workingdir = directory
        
        return

    def proc_spot_cal(self, logfile=None):
        """Runs the Spot Position calibration sequence."""
        tag = inspect.stack()[0][3] # our name
    
        print "{}: Run the Spot Position calibration".format(tag)
    
        print "{}: logfile {}".format(tag, logfile)
    
        if logfile is None:
            # we must collect the data
            now = datetime.datetime.now()
            logfile = now.strftime("manga_spot_cal.%Y%m%d.%H%M%S.log")
            logfile = self.workingdir + '/' + logfile
            print "{}: logfile {}".format(tag, logfile)
            fp = open(logfile, "wb")
            orig_stdout = sys.stdout
            sys.stdout = fp
            print "{}: start spot cal".format(tag)
            proc_gige_loop_save_image('input', self.workingdir)
            sys.stdout.flush()
            sys.stdout = orig_stdout
            fp.close()
    
        try:
            with open(logfile, "r") as fp:
                lines = fp.readlines()
        except Exception as e:
            print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return
    
        for line in lines:
            line = line.strip()
            #print "line", line
            tokens = line.split()
            #print "tokens", tokens
            if tokens and tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
                if tokens[1] == "cachefile":
                    continue
                image_file = tokens[2]
                print "{}: image_file {}".format(tag, image_file)
                # find out where the fiber image landed on the sensor
                spotx, spoty = proc_manga_spot(image_file)
                print "{}: spotx {} spoty {}".format(tag, spotx, spoty)
    
        # update the config file
        config = ConfigParser.ConfigParser()
        config.read(self.configfile);
        section = "SPOT"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "spotx", spotx)
        config.set(section, "spoty", spoty)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
    
        return
    
    def proc_ifu_motor_cal(self, rank, logfile=None, mapname="ifu-metrology-file", sectionname='IFU_MOTOR_CAL',
                           teststand='ts1'):
        """Runs the IFU motor calibration sequence."""
        tag = inspect.stack()[0][3] # our name
    
        print "{}: rank {}".format(tag, rank)
        
        print "{}: open the config file {}".format(tag, self.configfile)
        config = ConfigParser.ConfigParser()
        config.read(self.configfile)
    
        # choose which fibers to tune up on
        # start by assuming rank=6
        
        cfiber0 = 84
        cfiber1 = 7
        cfiber2 = 1
        cfiber3 = 119
        cfiber4 = 64
        cfiber5 = 36
        cfiber6 = 30
        if rank == 5:
            cfiber0 = 79
            cfiber1 = 33
            cfiber2 = 28
            cfiber3 = 3
            cfiber4 = 74
            cfiber5 = 63
            cfiber6 = 58
        if rank == 4:
            cfiber0 = 36
            cfiber1 = 5
            cfiber2 = 1
            cfiber3 = 58
            cfiber4 = 32
            cfiber5 = 28
            cfiber6 = 21
        if rank == 3:
            cfiber0 = 23
            cfiber1 = 1
            cfiber2 = 37
            cfiber3 = 20
            cfiber4 = 10
            cfiber5 = 7
            cfiber6 = 4
        if rank == 2:
            cfiber0 = 12
            cfiber1 = 1
            cfiber2 = 19
            cfiber3 = 14
            cfiber4 = 7
            cfiber5 = 5
            cfiber6 = 3
        if rank == 1:
            cfiber0 = 4
            cfiber1 = 1
            cfiber2 = 2
            cfiber3 = 3
            cfiber4 = 7
            cfiber5 = 6
            cfiber6 = 5
    
        mapfile = config.get('METROLOGY', mapname)
        ifu_metrology_map = self.CMfo.read_metrology_map(mapfile)
        _, _, _, _, x0, y0, _, _, _ = ifu_metrology_map[0]
        print "{}: x0 {} y0 {}".format(tag, x0, y0)
    
        # assume the operator has centered up the IFU
        m0x = proc_apt_query(teststand,"x1")
        m0y = proc_apt_query(teststand,"y1")
    
        print "{}: estimate k and J".format(tag)
    
        kx = -0.00296314526182
        ky = 0.00294697537848
        alpha = math.radians(0.0);
        Jx = m0x - (kx * x0)
        Jy = m0y - (ky * y0)
        print "{}:   kx: {:12.6f}".format(tag, kx)
        print "{}:   ky: {:12.6f}".format(tag, ky)
        print "{}:alpha: {:12.6f} deg".format(tag, math.degrees(alpha))
        print "{}:   Jx: {:12.6f}".format(tag, Jx)
        print "{}:   Jy: {:12.6f}".format(tag, Jy)
    
        # remember our estimate of the motor cal
        if not config.has_section(sectionname):
            config.add_section(sectionname)
        config.set(sectionname, "kx", kx)
        config.set(sectionname, "ky", ky)
        config.set(sectionname, "alpha", math.degrees(alpha))
        config.set(sectionname, "Jx", Jx)
        config.set(sectionname, "Jy", Jy)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
    
        # find out the spot location
        section = "SPOT"
        spotx = config.getfloat(section, "spotx")
        spoty = config.getfloat(section, "spoty")
        print "{}: spotx {} spoty {}".format(tag, spotx, spoty)
    
        if logfile is None:
            # we must collect the data
            now = datetime.datetime.now()
            logfile = now.strftime("manga_ifu_motor_cal.%Y%m%d.%H%M%S.log")
            logfile = self.workingdir + '/' + logfile
            print "{}: logfile {}".format(tag, logfile)
            fp = open(logfile, "wb")
            sys.stdout = fp
    
            print "{}: acquire corner cfiber {}".format(tag, cfiber1)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber1, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
            print "{}: acquire corner cfiber {}".format(tag, cfiber2)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber2, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
            print "{}: acquire corner cfiber {}".format(tag, cfiber3)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber3, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
            print "{}: acquire corner cfiber {}".format(tag, cfiber4)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber4, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
            print "{}: acquire corner cfiber {}".format(tag, cfiber5)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber5, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
            print "{}: acquire corner cfiber {}".format(tag, cfiber6)
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber6, mapname=mapname, motorname=sectionname)
            self.proc_gige_loop_save_image('input', self.workingdir)
    
            self.proc_ifu_fiber_acquire(teststand, rank, cfiber0, mapname=mapname, motorname=sectionname)
    
            sys.stdout = sys.__stdout__
            fp.close()
    
        try:
            with open(logfile, "r") as fp:
                lines = fp.readlines()
        except Exception as e:
            print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return
    
        Qxlist = []
        Qylist = []
        mxlist = []
        mylist = []
    
        Qx = Qy = mx = my = 0
        n = 0
        for line in lines:
            line = line.strip()
            tokens = line.split()
            if not tokens:
                continue
            if tokens[0] == "proc_ifu_motor_cal:":
                cfiber = int(tokens[4])
                _, sfiber = self.CMfo.map_list_and_cfiber(self.CMfo.ifu_map, cfiber)
                print "{}: cfiber {} sfiber {}".format(tag, cfiber, sfiber)
                _, _, _, _, x, y, _, _, _ = ifu_metrology_map[sfiber]
                print "{}: n {} x {} y {}".format(tag, n, x, y)
                Qx = spotx - (x - x0)
                Qy = spoty - (y - y0)
                print "{}: n {} Qx {} Qy {}".format(tag, n, Qx, Qy)
            elif tokens[0] == "proc_ifu_fiber_acquire:":
                mx = float(tokens[8])
                my = float(tokens[10])
                print "{}: n {} mx {} my {}".format(tag, n, mx, my)
            elif tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
                if tokens[1] == "cachefile":
                    continue
                image_file = tokens[2]
                print "{}: image_file {}".format(tag, image_file)
                # measure the IFU position
                root, _ = os.path.splitext(image_file)
                #print "root {} ext {}".format(root, ext)
                f = "{}.sobel.hough.blobs.spikes.out".format(root)
                if not os.path.isfile(f):
                    f = "{}.sobel.hough.blobs.fits".format(root)
                    if not os.path.isfile(f):
                        f = "{}.sobel.hough.fits".format(root)
                        if not os.path.isfile(f):
                            f = "{}.sobel.fits".format(root)
                            if not os.path.isfile(f):
                                f = "{}.fits".format(root)
                                if not os.path.isfile(f):
                                    f = "{}.raw".format(root)
                                    f = proc_manga_copy(f)
                                    if f is None:
                                        print >> sys.stderr, "Can't find image file {}".format(f)
                                        return None
                                f = proc_manga_sobel(f)
                            f = proc_manga_hough(f, 21)
                        f = proc_manga_blobs(f)
                    f = proc_manga_spikes(f)
                spike_map = self.CMfo.read_spike_map(f)
                print "{}: spike_map: {}".format(tag, spike_map)
    
                # find the fiber closest to the spot
                xc_save = 0
                yc_save = 0
                dr_save = 1000  # choose a value larger than any expected one
                for ix, f, xc, yc in spike_map:
                    dx = xc - spotx
                    dy = yc - spoty
                    dr = math.sqrt(dx * dx + dy * dy)
                    if (dr < dr_save):
                        xc_save = xc
                        yc_save = yc
                        dr_save = dr
                        print "{}: ix {} f {} xc {} yc {} dr {}".format(tag, ix, f, xc, yc, dr)
    
                # check if the fiber we found is too far from the expected position
                if dr_save > 21*(1 + 0.11 * (rank - 1)):
                    print "{}: error finding corner fiber: distance {} is too large".format(tag, dr_save)
                    sys.exit(1)
                
                dx = xc_save - spotx
                dy = yc_save - spoty
                print "{}: n {} dx {} dy {}".format(tag, n, dx, dy)
    
                dmx = dx * kx
                dmy = dy * ky
                print "{}: n {} dmx {} dmy {}".format(tag, n, dmx, dmy)
    
                mx -= dmx
                my -= dmy
                print "{}: n {} mx {} my {}".format(tag, n, mx, my)
    
                Qxlist.append(Qx)
                Qylist.append(Qy)
                mxlist.append(mx)
                mylist.append(my)
                n += 1
    
        print "{}: do {}-point lls for x".format(tag, n)
        # do the least squares fit in x
        sum_x = 0
        sum_y = 0
        sum_xx = 0
        sum_xy = 0
        n = 0
        for Q, m in zip(Qxlist, mxlist):
            sum_x += Q
            sum_y += m
            sum_xx += Q * Q
            sum_xy += Q * m
            n += 1
    
        kx = (sum_xy - (sum_x * sum_y) / n) / (sum_xx - sum_x * sum_x / n)
        Jx = sum_y / n - (kx * sum_x) / n
        print "{}: lls kx {} Jx {}".format(tag, kx, Jx)
    
        print "{}: do {}-point lls for y".format(tag, n)
        # do the least squares fit in y
        sum_x = 0
        sum_y = 0
        sum_xx = 0
        sum_xy = 0
        n = 0
        for Q, m in zip(Qylist, mylist):
            sum_x += Q
            sum_y += m
            sum_xx += Q * Q
            sum_xy += Q * m
            n += 1
    
        ky = (sum_xy - (sum_x * sum_y) / n) / (sum_xx - sum_x * sum_x / n)
        Jy = sum_y / n - (ky * sum_x) / n
        print "{}: lls ky {} Jy {}".format(tag, ky, Jy)
    
        # lock in our measurement of the motor cal
        if not config.has_section(sectionname):
            config.add_section(sectionname)
        config.set(sectionname, "kx", kx)
        config.set(sectionname, "ky", ky)
        config.set(sectionname, "alpha", math.degrees(0))
        config.set(sectionname, "Jx", Jx)
        config.set(sectionname, "Jy", Jy)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
    
        self.clear_regions_in_ds9('input')
    
        return

#     def proc_ifu_motor_cal(self, teststand="ts2", logfile=None):
#         """Calibrates the motor constants using the IFU."""
#         tag = inspect.stack()[0][3] # our name
#     
#         print "{}: Run the IFU motor calibration".format(tag)
#     
#         print "{}: logfile {}".format(tag, logfile)
#     
#         xstage = "x1"
#         ystage = "y1"
#     
#         if logfile is None:
#             # we must collect the data
#             xstage = "x1"
#             ystage = "y1"
#             now = datetime.datetime.now()
#             logfile = now.strftime("manga_ifu_motor_cal.%Y%m%d.%H%M%S.log")
#             logfile = self.workingdir + '/' + logfile
#             print "{}: logfile {}".format(tag, logfile)
#             fp = open(logfile, "wb")
#             orig_stdout = sys.stdout
#             sys.stdout = fp
#             mx0 = proc_apt_query(teststand, xstage)
#             my0 = proc_apt_query(teststand, ystage)
#             print "{}: start ifu motor cal mx0 {} my0 {}".format(tag, mx0, my0)
#             pos = 0
#             for dmx in [-0.5, 0, 0.5]:
#                 for dmy in [-0.5, 0, 0.5]:
#                     mx = mx0 + dmx;
#                     my = my0 + dmy;
#                     print "{}: move to motor pos {} mx {} my {}".format(
#                         tag, pos, mx, my)
#                     #proc_apt_move_absolute(teststand, xstage, mx - BACKLASH)
#                     #proc_apt_move_absolute(teststand, ystage, my - BACKLASH)
#                     #proc_apt_move_block(teststand, xstage)
#                     #proc_apt_move_block(teststand, ystage)
#                     proc_apt_move_absolute(teststand, xstage, mx)
#                     proc_apt_move_absolute(teststand, ystage, my)
#                     proc_apt_move_block(teststand, xstage)
#                     proc_apt_move_block(teststand, ystage)
#                     proc_gige_loop_save_image(self.workingdir)
#                     sys.stdout.flush()
#                     pos += 1
#             proc_apt_move_absolute(teststand, xstage, mx0)
#             proc_apt_move_absolute(teststand, ystage, my0)
#             sys.stdout = orig_stdout
#             fp.close()
#     
#         try:
#             with open(logfile, "r") as fp:
#                 lines = fp.readlines()
#         except Exception as e:
#             print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
#             print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
#             print >> sys.stderr, "{}: Exception: {}".format(tag, e)
#             return
#     
#         hlist = []
#         pos = mx = my = sx = sy = 0
#     
#         for line in lines:
#             line = line.strip()
#             tokens = line.split()
#             if not tokens:
#                 continue
#             if tokens[0] == "proc_ifu_motor_cal:":
#                 if tokens[1] == "move":
#                     print "line", line
#                     print "tokens", tokens
#                     pos = int(tokens[5])
#                     mx = float(tokens[7])
#                     my = float(tokens[9])
#                     print "{}: pos {} mx {} my {}".format(tag, pos, mx, my)
#             elif tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
#                 if tokens[1] == "cachefile":
#                     continue
#                 image_file = tokens[2]
#                 print "{}: image_file {}".format(tag, image_file)
#                 # measure the IFU position
#                 #print "head {} tail {}".format(head, tail)
#                 root, _ = os.path.splitext(image_file)
#                 #print "root {} ext {}".format(root, ext)
#                 f = "{}.sobel.hough.blobs.spikes.out".format(root)
#                 if not os.path.isfile(f):
#                     f = "{}.sobel.hough.blobs.fits".format(root)
#                     if not os.path.isfile(f):
#                         f = "{}.sobel.hough.fits".format(root)
#                         if not os.path.isfile(f):
#                             f = "{}.sobel.fits".format(root)
#                             if not os.path.isfile(f):
#                                 f = "{}.fits".format(root)
#                                 if not os.path.isfile(f):
#                                     f = "{}.raw".format(root)
#                                     f = proc_manga_copy(f)
#                                     if f is None:
#                                         print >> sys.stderr, "Can't find image file {}".format(f)
#                                         return None
#                                 f = proc_manga_sobel(f)
#                             f = proc_manga_hough(f, 21)
#                         f = proc_manga_blobs(f)
#                     f = proc_manga_spikes(f)
#                 aop, flist = proc_manga_ifu(f)
#                 print "{}: aop: {}".format(tag, aop)
#                 c, theta, sx, sy = aop
#                 print "{}: aop:     c: {:12.6f}".format(tag, c)
#                 print "{}: aop: theta: {:12.6f} deg".format(tag, math.degrees(theta))
#                 print "{}: aop:    sx: {:12.6f}".format(tag, sx)
#                 print "{}: aop:    sy: {:12.6f}".format(tag, sy)
#                 print "{}: flist[0]: {}".format(tag, flist[0])
#                 _, _, _, _, sx, sy, _, _, _ = flist[0]
#                 mx *= -1     # left handed motor space...
#                 print "{}: pos {} sx {} sy {} mx {} my {}".format(tag, pos, sx, sy, mx, my)
#                 hlist.append((pos, pos, sx, sy, mx, my))
#                 for ix, item in enumerate(hlist):
#                     print "{}: partial ix {} item {}".format(tag, ix, item)
#     
#         for ix, item in enumerate(hlist):
#             print "{}: final ix {} item {}".format(tag, ix, item)
#     
#         # write the data into a file for the aop solver
#         ofile = logfile + ".txt"
#         with open(ofile, "wb") as fp:
#             fp.write("{}: residuals: {:>3s} {:>12s} {:>12s} {:>12s} {:>12s}\n".format(
#                     tag, "pos", "sx", "sy", "mx", "my"))
#             for ix, item in enumerate(hlist):
#                 pos, _, sx, sy, mx, my = item
#                 fp.write("{}: residuals: {:3d} {:12.6f} {:12.6f} {:12.6f} {:12.6f}\n".format(
#                         tag, pos, sx, sy, mx, my))
#     
#         # invoke the aop solver on this file
#         aop = proc_manga_aop(ofile)
#         print "{}: aop: {}".format(tag, aop)
#         k, alpha, Jx, Jy = aop
#         print "{}: aop:     k: {:12.6f}".format(tag, k)
#         print "{}: aop: alpha: {:12.6f} deg".format(tag, math.degrees(alpha))
#         print "{}: aop:    Jx: {:12.6f}".format(tag, Jx)
#         print "{}: aop:    Jy: {:12.6f}".format(tag, Jy)
#     
#     #    ofile = logfile + ".map"
#     #    with open(ofile, "wb") as fp:
#     #        fp.write("{}: residuals: {:>3s} {:>3s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s}\n".format(
#     #                    tag, "ix", "pos", "xy", "yr", "xs", "ys", "ex", "ey", "er"))
#     #        for ix, item in enumerate(flist):
#     #            print "{}: flist ix {} item {}".format(tag, ix, item)
#     #            ix, pos, xr, yr, xs, ys, ex, ey, er = item
#     #            xm = xr * c * mfo.PX
#     #            ym = yr * c * mfo.PY
#     #            
#     #            ex *= c * mfo.PX
#     #            ey *= c * mfo.PY
#     #            er = math.sqrt(ex * ex + ey * ey)
#     #            fp.write("{}: residuals: {:3d} {:3d} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f}\n".format(
#     #                    tag, ix, pos, xm, ym, xs, ys, ex, ey, er))
#     
#         # update the config file
#         config = ConfigParser.ConfigParser()
#         config.read(self.configfile);
#         section = "IFU_MOTOR_CAL"
#         if not config.has_section(section):
#             config.add_section(section)
#         config.set(section, "k", k)
#         config.set(section, "alpha", math.degrees(alpha))
#         config.set(section, "Jx", Jx)
#         config.set(section, "Jy", Jy)
#     #    section = "METROLOGY"
#     #    if not config.has_section(section):
#     #        config.add_section(section)
#     #    config.set(section, "ifu-metrology-file", ofile)
#     #    section = "IFU_AOP"
#     #    if not config.has_section(section):
#     #        config.add_section(section)
#     #    config.set(section, "c", c)
#     #    config.set(section, "theta", math.degrees(theta))
#     #    config.set(section, "sx", sx)
#     #    config.set(section, "sy", sy)
#         with open(self.configfile, "wb") as fp:
#             config.write(fp)
    
    def proc_ifu_metrology(self, logfile=None, mapname="ifu-metrology-file",teststand='ts1'):
        """Runs the IFU metrology sequence on a raw file."""
        tag = inspect.stack()[0][3] # our name
    
        print "{}: logfile {}".format(tag, logfile)
    
        if logfile is None:
            # we must collect the data
            now = datetime.datetime.now()
            logfile = now.strftime("manga_ifu_metrology.%Y%m%d.%H%M%S.log")
            logfile = self.workingdir + '/' + logfile
            print "{}: logfile {}".format(tag, logfile)
            fp = open(logfile, "wb")
            orig_stdout = sys.stdout
            sys.stdout = fp
            proc_gige_loop_save_image('input', self.workingdir)
            sys.stdout.flush()
            sys.stdout = orig_stdout
            fp.close()
    
        try:
            with open(logfile, "r") as fp:
                lines = fp.readlines()
        except Exception as e:
            print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return
    
        for line in lines:
            line = line.strip()
            #print "line", line
            tokens = line.split()
            #print "tokens", tokens
            if tokens and tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
                if tokens[1] == "cachefile":
                    continue
                image_file = tokens[2]
                print "{}: image_file {}".format(tag, image_file)
                # measure the IFU position
                #print "head {} tail {}".format(head, tail)
                root, ext = os.path.splitext(image_file)
                #print "root {} ext {}".format(root, ext)
                f = "{}.sobel.hough.blobs.spikes.out".format(root)
                if not os.path.isfile(f):
                    f = "{}.sobel.hough.blobs.fits".format(root)
                    if not os.path.isfile(f):
                        f = "{}.sobel.hough.fits".format(root)
                        if not os.path.isfile(f):
                            f = "{}.sobel.fits".format(root)
                            if not os.path.isfile(f):
                                f = "{}.fits".format(root)
                                if not os.path.isfile(f):
                                    f = "{}.raw".format(root)
                                    f = proc_manga_copy(f)
                                    if f is None:
                                        print >> sys.stderr, "Can't find image file {}".format(f)
                                        return None
                                f = proc_manga_sobel(f)
                            f = proc_manga_hough(f, 21)
                        f = proc_manga_blobs(f)
                    f = proc_manga_spikes(f)
                aop, flist = proc_manga_ifu(f)
                print "{}: aop: {}".format(tag, aop)
                c, theta, sx, sy = aop
                print "{}: aop:     c: {:12.6f}".format(tag, c)
                print "{}: aop: theta: {:12.6f} deg".format(tag, math.degrees(theta))
                print "{}: aop:    sx: {:12.6f}".format(tag, sx)
                print "{}: aop:    sy: {:12.6f}".format(tag, sy)
    
        ofile = logfile + ".map"
        with open(ofile, "wb") as fp:
            fp.write("{}: residuals: {:>3s} {:>3s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s}\n".format(
                        tag, "f", "pos", "model-x", "model-y", "image-x", "image-y", "ex-pix", "ey-pix", "er-pix"))
            for ix, item in enumerate(flist):
                print "{}: flist ix {} item {}".format(tag, ix, item)
                ix, pos, modelx, modely, imagex, imagey, ex, ey, er = item
                fp.write("{}: residuals: {:3d} {:3d} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f}\n".format(
                        tag, ix, pos, modelx, modely, imagex, imagey, ex, ey, er))
    
        # update the config file
        config = ConfigParser.ConfigParser()
        config.read(self.configfile);
        section = "METROLOGY"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, mapname, ofile)
#         section = "IFU_AOP"
#         if not config.has_section(section):
#             config.add_section(section)
#         config.set(section, "c", c)
#         config.set(section, "theta", math.degrees(theta))
#         config.set(section, "sx", sx)
#         config.set(section, "sy", sy)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
    
    def proc_pp_motor_cal(self, teststand="ts1", logfile=None):
        """Runs the Plug Plate motor calibration sequence."""
        tag = inspect.stack()[0][3] # our name
    
        print "{}: Run the Plug Plate motor calibration".format(tag)
    
        print "{}: logfile {}".format(tag, logfile)
    
        if logfile is None:
            # we must collect the data
            xstage = "x1"
            ystage = "y1"
            now = datetime.datetime.now()
            logfile = now.strftime("manga_pp_motor_cal.%Y%m%d.%H%M%S.log")
            logfile = self.workingdir + '/' + logfile
            print "{}: logfile {}".format(tag, logfile)
            fp = open(logfile, "wb")
            orig_stdout = sys.stdout
            sys.stdout = fp
            # assume the operator has centered up a fiber
            mx0 = proc_apt_query(teststand, xstage)
            my0 = proc_apt_query(teststand, ystage)
            print "{}: start pp motor cal mx0 {} my0 {}".format(tag, mx0, my0)
            pos = 0
            for dmx in [-0.5, 0, 0.5]:
                for dmy in [-0.5, 0, 0.5]:
                    mx = mx0 + dmx;
                    my = my0 + dmy;
                    print "{}: move to motor pos {} mx {} my {}".format(
                        tag, pos, mx, my)
                    proc_apt_move_absolute(teststand, xstage, mx)
                    proc_apt_move_absolute(teststand, ystage, my)
                    proc_apt_move_block(teststand, xstage)
                    proc_apt_move_block(teststand, ystage)
                    proc_gige_loop_save_image('input', self.workingdir)
                    sys.stdout.flush()
                    pos += 1
            proc_apt_move_absolute(teststand, xstage, mx0)
            proc_apt_move_absolute(teststand, ystage, my0)
            sys.stdout = orig_stdout
            fp.close()
    
        try:
            with open(logfile, "r") as fp:
                lines = fp.readlines()
        except Exception as e:
            print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return
    
        hlist = []
        pos = mx = my = sx = sy = 0
    
        for line in lines:
            line = line.strip()
            tokens = line.split()
            if not tokens:
                continue;
            if tokens[0] == "proc_pp_motor_cal:":
                if tokens[1] == "move":
                    print "line", line
                    print "tokens", tokens
                    pos = int(tokens[5])
                    mx = float(tokens[7])
                    my = float(tokens[9])
                    print "{}: pos {} mx {} my {}".format(tag, pos, mx, my)
            elif tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
                if tokens[1] == "cachefile":
                    continue
                image_file = tokens[2]
                print "{}: image_file {}".format(tag, image_file)
                # find out where the fiber image landed on the sensor
                sx, sy = proc_manga_spot(image_file)
                print "{}: pos {} sx {} sy {}".format(tag, pos, sx, sy)
                # refer the spot to the center
                #print "{}: pos {} CENTER_X {} CENTER_Y {}".format(tag, pos, mfo.CENTER_X, mfo.CENTER_Y)
                #sx -= mfo.CENTER_X
                #sy -= mfo.CENTER_Y
                mx *= -1     # left handed motor space...
                print "{}: pos {} sx {} sy {} mx {} my {}".format(tag, pos, sx, sy, mx, my)
                hlist.append((pos, pos, sx, sy, mx, my))
                for ix, item in enumerate(hlist):
                    print "{}: partial ix {} item {}".format(tag, ix, item)
    
        for ix, item in enumerate(hlist):
            print "{}: final ix {} item {}".format(tag, ix, item)
    
        ofile = logfile + ".txt"
        with open(ofile, "wb") as fp:
            fp.write("{}: residuals: {:>3s} {:>12s} {:>12s} {:>12s} {:>12s}\n".format(
                    tag, "pos", "sx", "sy", "mx", "my"))
            for ix, item in enumerate(hlist):
                pos, _, sx, sy, mx, my = item
                fp.write("{}: residuals: {:3d} {:12.6f} {:12.6f} {:12.6f} {:12.6f}\n".format(
                        tag, pos, sx, sy, mx, my))
    
        # invoke the aop solver on this file
        k, alpha, Jx, Jy = proc_manga_aop(ofile)
        print "{}: aop:     k: {:12.6f}".format(tag, k)
        print "{}: aop: alpha: {:12.6f} deg".format(tag, math.degrees(alpha))
        print "{}: aop:    Jx: {:12.6f}".format(tag, Jx)
        print "{}: aop:    Jy: {:12.6f}".format(tag, Jy)
    
        # update the config file
        config = ConfigParser.ConfigParser()
        config.read(self.configfile);
        section = "PP_MOTOR_CAL"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "k", k)
        config.set(section, "alpha", math.degrees(alpha))
        config.set(section, "Jx", Jx)
        config.set(section, "Jy", Jy)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
            
        return
    
    def proc_pp_metrology(self, teststand="ts1", logfile=None):
        """Runs the Plug Plate metrology sequence."""
        tag = inspect.stack()[0][3] # our name
    
        config = ConfigParser.RawConfigParser()
        config.read(self.configfile)
        section = "PP_MOTOR_CAL"
        k = config.getfloat(section, "k")
        alpha = math.radians(config.getfloat(section, "alpha"))
        Jx = config.getfloat(section, "Jx")
        Jy = config.getfloat(section, "Jy")
        print "{}: aop:     k: {:12.6f}".format(tag, k)
        print "{}: aop: alpha: {:12.6f} deg".format(tag, math.degrees(alpha))
        print "{}: aop:    Jx: {:12.6f}".format(tag, Jx)
        print "{}: aop:    Jy: {:12.6f}".format(tag, Jy)
        pp_map = self.CMfo.get_pp_map()
        pp_drill_map = self.CMfo.get_pp_drill_map()
    
        if logfile is None:
            # we must collect the data
            xstage = "x1"
            ystage = "y1"
            now = datetime.datetime.now()
            logfile = now.strftime("manga_pp_metrology.%Y%m%d.%H%M%S.log")
            logfile = self.workingdir + '/' + logfile
            print "{}: logfile {}".format(tag, logfile)
            fp = open(logfile, "wb")
            orig_stdout = sys.stdout
            sys.stdout = fp
            # remember where we started
            mx0 = proc_apt_query(teststand, xstage)
            my0 = proc_apt_query(teststand, ystage)
            print "{}: start pp metrology mx0 {} my0 {}".format(tag, mx0, my0)
            pos = 0
            #fiberlist = range(self.pp_start_fiber, self.pp_end_fiber + 1)
#             if fiberlist[0] != 1:
#                 fiberlist = [1] + fiberlist
            for fiber in self.fiberlist:
                print "{}: move to plug plate fiber {}".format(tag, fiber)
                hole = pp_map[fiber]
                print "{}: move to plug plate hole {}".format(tag, hole)
                _, mx, my = pp_drill_map[hole]
                # offset from the schematic positions to a rough real position
                mx += self.pp_xoffset
                my += self.pp_yoffset
                print "{}: move to motor pos {} mx {} my {}".format(tag, pos, mx, my)
                proc_apt_move_absolute(teststand, xstage, mx)
                proc_apt_move_absolute(teststand, ystage, my)
                proc_apt_move_block(teststand, xstage)
                proc_apt_move_block(teststand, ystage)
                proc_gige_loop_save_image('input', self.workingdir)
                sys.stdout.flush()
            proc_apt_move_absolute(teststand, xstage, mx0)
            proc_apt_move_absolute(teststand, ystage, my0)
            proc_apt_move_block(teststand, xstage)
            proc_apt_move_block(teststand, ystage)
            sys.stdout = orig_stdout
            fp.close()
    
        try:
            with open(logfile, "r") as fp:
                lines = fp.readlines()
        except Exception as e:
            print >> sys.stderr, "{}: Can't open file {} for reading".format(tag, logfile)
            print >> sys.stderr, "{}: Exception: {}".format(tag, sys.exc_info()[0])
            print >> sys.stderr, "{}: Exception: {}".format(tag, e)
            return
    
        hlist = []
        fiber = hole = mx0 = my0 = mx1 = my1 = 0    # these need this outer scope
    
        for line in lines:
            line = line.strip()
            #print "line", line
            tokens = line.split()
            #print "tokens", tokens
            if not tokens:
                continue
            if tokens[0] == "proc_pp_metrology:":
                if len(tokens) is 7 and tokens[5] == "fiber":
                    fiber = int(tokens[6])
                    print "{}: new measurement: fiber {}".format(tag, fiber)
                elif len(tokens) is 7 and tokens[5] == "hole":
                    hole = int(tokens[6])
                    print "{}: new measurement: hole {}".format(tag, hole)
                elif len(tokens) is 10 and tokens[6] == "mx":
                    # get the motor coords at the time of the exposure
                    mx1 = float(tokens[7])
                    my1 = float(tokens[9])
                    print "{}: new measurement: mx1 {:8.3f} my1 {:8.3f}".format(tag, mx1, my1)
            elif tokens[0] == "proc_gige_loop_save_image:" and len(tokens) == 3:
                if tokens[1] == "cachefile":
                    continue
                image_file = tokens[2]
                print "{}: image_file {}".format(tag, image_file)
                print "{}: image_file {}".format(tag, image_file)
                # find out where the fiber image landed on the sensor
                sx, sy = proc_manga_spot(image_file)
                print "{}: sx {:8.3f} sy {:8.3f}".format(tag, sx, sy)
                # refer this position to the center of the sensor
                dsx = self.CMfo.CENTER_X - sx
                dsy = self.CMfo.CENTER_Y - sy
                print "{}: dsx {:8.3f} dsy {:8.3f}".format(tag, dsx, dsy)
                # convert pixel vector to motor vector
                dmx = k * (math.cos(alpha) * dsx - math.sin(alpha) * dsy)
                dmy = k * (math.sin(alpha) * dsx + math.cos(alpha) * dsy)
                print "{}: dmx {:8.3f} dmy {:8.3f}".format(tag, dmx, dmy)
                # compute the motor position of the spot
                mx2 = mx1 - dmx     # watch out for handedness!
                my2 = my1 + dmy
                print "{}: mx2 {:8.3f} my2 {:8.3f}".format(tag, mx2, my2)
                if hole == 0:
                    # memorize the reference hole
                    mx0 = mx2
                    my0 = my2
                    print "{}: new reference mx0 {:8.3f} my0 {:8.3f}".format(tag, mx0, my0)
                # now reassign mx1,my1 as the model, shifted according to the first hole
                _, mx1, my1 = pp_drill_map[hole]
                mx1 += mx0
                my1 += my0
                ex = mx2 - mx1
                ey = my2 - my1
                er = math.sqrt(ex * ex + ey * ey)
                print "{}: ex {:8.3f} ey {:8.3f} er {:8.3f}".format(tag, ex, ey, er)
                hlist.append((fiber, hole, mx1, my1, mx2, my2, ex, ey, er))
                for ix, item in enumerate(hlist):
                    print "{}: partial ix {} item {}".format(tag, ix, item)
    
        for ix, item in enumerate(hlist):
            print "{}: final ix {} item {}".format(tag, ix, item)
    
        ofile = logfile + ".map"
        with open(ofile, "wb") as fp:
            fp.write("{}: residuals:  {:>3s} {:>3s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s} {:>8s}\n".format(
                        tag, "f", "pos", "model-x", "model-y", "fiber-x", "fiber-y", "ex-mm", "ey-mm", "er-mm"))
            for ix, item in enumerate(hlist):
                f, pos, mx1, my1, mx2, my2, ex, ey, er = item
                fp.write("{}: residuals:  {:3d} {:3d} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f} {:8.3f}\n".format(
                        tag, f, pos, mx1, my1, mx2, my2, ex, ey, er))
    
        # update the config file
        config = ConfigParser.ConfigParser()
        config.read(self.configfile);
        section = "METROLOGY"
        if not config.has_section(section):
            config.add_section(section)
        config.set(section, "plug-plate-metrology-file", ofile)
        with open(self.configfile, "wb") as fp:
            config.write(fp)
    
        return
    
    RUN_TIME_API = 0
    
    def proc_ifu_fiber_acquire(self, teststand, rank, cfiber, mapname="ifu-metrology-file", motorname='IFU_MOTOR_CAL'):
        """Acquires the given fiber number in an IFU."""
        tag = inspect.stack()[0][3] # our name
    
        xstage = "x1"
        ystage = "y1"
    
        self.CMfo.clear_regions_in_ds9('input')
    
        mx, my = self.CMfo.ifu_fiber_locate(rank, cfiber, mapname=mapname, motorname=motorname)
    
        print "{}: acquire rank {} cfiber {} at mx {} my {}".format(tag, rank, cfiber, mx, my)
        
        proc_apt_move_absolute(teststand, xstage, mx)
        proc_apt_move_absolute(teststand, ystage, my)
        proc_apt_move_block(teststand, xstage)
        proc_apt_move_block(teststand, ystage)
    
        self.CMfo.show_regions_in_ds9('input')
    
        return
    
    def proc_pp_fiber_acquire(self, teststand, fiber):
        """Acquires the given fiber in the plug plate."""
        tag = inspect.stack()[0][3] # our name
    
        xstage = "x1"
        ystage = "y1"
        
        self.CMfo.clear_regions_in_ds9('input')
    
        mx, my = self.CMfo.pp_fiber_locate(fiber)
    
        #this will happen when moving to a hole with no fiber in it
        if mx < 0:
            mx = -1 * mx + self.pp_xoffset
            my = -1 * my + self.pp_yoffset
    
        print "{}: fiber {} mx {} my {}".format(tag, fiber, mx, my)
    
        if mx > 49 or my > 24 or mx < 0 or my < 0:
            print "{}: location mx {} my {} is out of range".format(tag, mx, my)
            return
    
        proc_apt_move_absolute(teststand, xstage, mx)
        proc_apt_move_absolute(teststand, ystage, my)
        proc_apt_move_block(teststand, xstage)
        proc_apt_move_block(teststand, ystage)
        
        self.CMfo.show_regions_in_ds9('input')
    
        return
