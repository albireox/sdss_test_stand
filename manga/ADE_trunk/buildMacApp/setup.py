"""Build the Mac fiber tester application

Usage:
% python setup.py [--quiet] py2app

History:
2008-06-26 ROwen
"""
import os
from plistlib import Plist
import subprocess
import sys
from setuptools import setup

# add FiberTester directory to sys.path before importing FiberTester
ftDir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "python")
sys.path.append(ftDir)
import FiberTester

appName = "FiberTester"
mainProg = os.path.join(ftDir, "run.py")
appPath = os.path.join("dist", "%s.app" % (appName,))
contentsDir = os.path.join(appPath, "Contents")
fullVersStr = FiberTester.__version__
shortVersStr = fullVersStr.split(None, 1)[0]

# packages to include recursively
inclPackages = ()

plist = Plist(
    CFBundleName                = appName,
    CFBundleShortVersionString  = shortVersStr,
    CFBundleGetInfoString       = "%s %s" % (appName, fullVersStr),
    CFBundleExecutable          = appName,
    LSPrefersPPC                = False,
)

setup(
    app = [mainProg],
    setup_requires = ["py2app"],
    options = dict(
        py2app = dict (
            plist = plist,
#            iconfile = iconFile,
#            includes = inclModules,
            packages = inclPackages,
        )
    ),
)

print "*** Creating disk image ***"
appName = "FiberTester_%s_Mac" % shortVersStr
destFile = os.path.join("dist", appName)
args=("hdiutil", "create", "-srcdir", appPath, destFile)
retCode = subprocess.call(args=args)

print "*** Built %s ***" % (appName,)
