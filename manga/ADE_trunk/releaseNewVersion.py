#!/usr/bin/env python
"""A script to release a new version of the fiber tester from a subversion working copy

To use:
    ./releaseNewVersion.py
"""
from __future__ import with_statement
import os
import re
import shutil
import sys
import subprocess

ftDir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "python")
sys.path.append(ftDir)
import FiberTester

PkgName = "FiberTester"
versionNum = FiberTester.__version__
queryStr = "Version is %s; is this OK? (y/[n]) " % (versionNum,)
getOK = raw_input(queryStr)
if not getOK.lower().startswith("y"):
    sys.exit(0)

print "Status of subversion repository:"
subprocess.call(["svn", "status"])

getOK = raw_input("Is the subversion repository up to date? (y/[n]) ")
if not getOK.lower().startswith("y"):
    sys.exit(0)

print "Subversion repository OK"

exportRoot = os.environ["HOME"]
exportDirName = "%s_%s_Source" % (PkgName, versionNum)
exportPath = os.path.abspath(os.path.join(exportRoot, exportDirName))
if os.path.exists(exportPath):
    print "Export directory %r already exists" % (exportPath,)
    getOK = raw_input("Should I delete the old %r? (yes/[n]) " % (exportPath,))
    if not getOK.lower() == "yes":
        sys.exit(0)
    print "Deleting %r" % (exportPath,)
    shutil.rmtree(exportPath)

print "Exporting subversion repository to %r" % (exportPath,)

status = subprocess.call(["svn", "export", ".", exportPath])
if status != 0:
    print "Svn export failed!"
    sys.exit(0)

if sys.platform == "darwin":
    print "Building Mac version"
    macBuildDir = os.path.join(exportPath, "buildMacApp")
    status = subprocess.call(["python", "setup.py", "-q", "py2app"], cwd=macBuildDir)
    if status != 0:
        print "Mac build failed!"
