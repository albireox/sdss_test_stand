lib:
	echo "Compiling libraries..."
	(cd ./manga/ADE_trunk/c/ftdi/lib; make lib)
	(cd ./manga/ADE_trunk/c/apt/lib; make lib)
	(cd ./manga/ADE_trunk/c/gige/lib; make lib)
	(cd ./manga/ADE_trunk/c/manga/lib; make lib)

exe:
	echo "Compiling executables..."
	(cd ./manga/ADE_trunk/c/ftdi; make exe)
	(cd ./manga/ADE_trunk/c/apt; make exe)
	(cd ./manga/ADE_trunk/c/gige; make exe)
	(cd ./manga/ADE_trunk/c/manga; make exe)

install:
	echo "Installing executables..."
	(cd ./manga/ADE_trunk/c/ftdi; make install)
	(cd ./manga/ADE_trunk/c/apt; make install)
	(cd ./manga/ADE_trunk/c/gige; make install)
	(cd ./manga/ADE_trunk/c/manga; make install)

clean:
	echo "Cleaning source folders..."

	(cd ./manga/ADE_trunk/c/ftdi/lib; make clean)
	(cd ./manga/ADE_trunk/c/apt/lib; make clean)
	(cd ./manga/ADE_trunk/c/gige/lib; make clean)
	(cd ./manga/ADE_trunk/c/manga/lib; make clean)

	(cd ./manga/ADE_trunk/c/ftdi; make clean)
	(cd ./manga/ADE_trunk/c/apt; make clean)
	(cd ./manga/ADE_trunk/c/gige; make clean)
	(cd ./manga/ADE_trunk/c/manga; make clean)

imaging:
	git checkout imaging
	make lib exe install

apogee:
	git checkout apogee
	make lib exe install
